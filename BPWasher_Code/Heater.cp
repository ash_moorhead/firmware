#line 1 "C:/Software/BPWasher_Code/Heater.c"
#line 1 "c:/software/bpwasher_code/bpwasher_objects.h"
typedef enum {_taLeft, _taCenter, _taRight} TTextAlign;
typedef enum {_tavTop, _tavMiddle, _tavBottom} TTextAlignVertical;

typedef struct Screen TScreen;

typedef struct Button {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 char *Caption;
 TTextAlign TextAlign;
 TTextAlignVertical TextAlignVertical;
 const char *FontName;
 unsigned int Font_Color;
 char VerticalText;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TButton;

typedef struct Button_Round {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 char *Caption;
 TTextAlign TextAlign;
 TTextAlignVertical TextAlignVertical;
 const char *FontName;
 unsigned int Font_Color;
 char VerticalText;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char Corner_Radius;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TButton_Round;

typedef struct CButton_Round {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 const char *Caption;
 TTextAlign TextAlign;
 TTextAlignVertical TextAlignVertical;
 const char *FontName;
 unsigned int Font_Color;
 char VerticalText;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char Corner_Radius;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TCButton_Round;

typedef struct Label {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char *Caption;
 const char *FontName;
 unsigned int Font_Color;
 char VerticalText;
 char Visible;
 char Active;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TLabel;

typedef struct Image {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 const char *Picture_Name;
 char Visible;
 char Active;
 char Picture_Type;
 char Picture_Ratio;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TImage;

typedef const struct CImage {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 const char *Picture_Name;
 char Visible;
 char Active;
 char Picture_Type;
 char Picture_Ratio;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TCImage;

typedef struct Circle {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Radius;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TCircle;

typedef struct Box {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TBox;

typedef struct Line {
 TScreen* OwnerScreen;
 char Order;
 unsigned int First_Point_X;
 unsigned int First_Point_Y;
 unsigned int Second_Point_X;
 unsigned int Second_Point_Y;
 char Pen_Width;
 char Visible;
 unsigned int Color;
} TLine;

typedef struct CheckBox {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Checked;
 unsigned char Bit;
 char Transparent;
 char *Caption;
 TTextAlign TextAlign;
 const char *FontName;
 unsigned int Font_Color;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char Rounded;
 char Corner_Radius;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)(char Output, char Checked);
 void (*OnPressPtr)();

} TCheckBox;

struct Screen {
 unsigned int Color;
 unsigned int Width;
 unsigned int Height;
 unsigned int ObjectsCount;
 unsigned int ButtonsCount;
 TButton * const code *Buttons;
 unsigned int Buttons_RoundCount;
 TButton_Round * const code *Buttons_Round;
 unsigned int CButtons_RoundCount;
 TCButton_Round * const code *CButtons_Round;
 unsigned int LabelsCount;
 TLabel * const code *Labels;
 unsigned int ImagesCount;
 TImage * const code *Images;
 unsigned int CImagesCount;
 TCImage * const code *CImages;
 unsigned int CirclesCount;
 TCircle * const code *Circles;
 unsigned int BoxesCount;
 TBox * const code *Boxes;
 unsigned int LinesCount;
 TLine * const code *Lines;
 unsigned int CheckBoxesCount;
 TCheckBox * const code *CheckBoxes;
};

extern TScreen* CurrentScreen;

extern TScreen Keypad;
extern TButton_Round Key_7;
extern TButton_Round Key_8;
extern TButton_Round Key_9;
extern TButton_Round Key_clear;
extern TButton_Round Key_4;
extern TButton_Round Key_5;
extern TButton_Round Key_6;
extern TButton_Round Key_1;
extern TButton_Round Key_2;
extern TButton_Round Key_3;
extern TButton_Round Key_dp;
extern TButton_Round Key_0;
extern TButton_Round Key_enter;
extern TCButton_Round Key_return_button;
extern TButton kp_display;
extern TCImage Image8;
extern TButton_Round pos_neg;
extern TLabel negative_label;


extern TLabel lblKeyboardTitle;
extern TButton * const code Screen1_Buttons[1];
extern TButton_Round * const code Screen1_Buttons_Round[14];
extern TCButton_Round * const code Screen1_CButtons_Round[1];
extern TLabel * const code Screen1_Labels[1];
extern TCImage * const code Screen1_CImages[1];

extern TScreen _main;
extern TLabel Diagram3_Label1;
extern TCircle indicator;
extern TButton Screen_PV;
extern TLine Line2;
extern TButton_Round Main_Screen_settings;
extern TImage Main_screen_settings_image;
extern TLine Line1;
extern TButton_Round Main_Screen_start;
extern TImage start_stop;
extern TImage Diagram3_thermred;
extern TImage Diagram3_thermBlack;
extern TLabel Label1;
extern TLabel Label2;
extern TButton * const code Screen2_Buttons[1];
extern TButton_Round * const code Screen2_Buttons_Round[2];
extern TLabel * const code Screen2_Labels[3];
extern TImage * const code Screen2_Images[4];
extern TCircle * const code Screen2_Circles[1];
extern TLine * const code Screen2_Lines[2];

extern TScreen Settings;
extern TButton_Round Settings_temp;
extern TButton_Round Settings_PID;
extern TButton_Round Settings_CAL;
extern TButton_Round Settings_Exit;
extern TImage therm_settings;
extern TButton_Round ToolBox;
extern TImage imgToolBox;
extern TButton_Round Settings_drain;
extern TButton_Round * const code Screen3_Buttons_Round[6];

extern TScreen PID;
extern TCButton_Round Prop_button;
extern TButton_Round P_value;
extern TButton_Round I_value;
extern TButton_Round D_value;
extern TButton_Round I_button;
extern TButton_Round D_button;
extern TButton_Round PID_conf_button;
extern TButton_Round PID_ret_button;
extern TImage Image9;
extern TButton_Round * const code Screen4_Buttons_Round[7];
extern TCButton_Round * const code Screen4_CButtons_Round[1];
extern TImage * const code Screen4_Images[1];

extern TScreen Calibration;
extern TCButton_Round cal_offset_button;
extern TButton_Round cal_offset_value;
extern TButton_Round cal_factor_value;
extern TButton_Round cal_fact_button;
extern TButton_Round cal_confirm_button;
extern TButton_Round cal_ret_button;
extern TImage Image10;
extern TLabel cal_message_label;
extern TLabel cal_temp_label;
extern TButton_Round cal_start_button;
extern TButton_Round * const code Screen5_Buttons_Round[6];
extern TCButton_Round * const code Screen5_CButtons_Round[1];
extern TLabel * const code Screen5_Labels[2];
extern TImage * const code Screen5_Images[1];

extern TScreen Diags;
extern TLine Line3;
extern TLabel Lbl_inputs;
extern TLabel Lbl_outputs;
extern TCircle Circle1;
extern TLabel Lbl_water;
extern TCircle Circle2;
extern TLabel Label3;
extern TCircle Circle3;
extern TLabel Label4;
extern TCircle Circle4;
extern TLabel Label5;
extern TCircle Circle5;
extern TLabel Label6;
extern TCircle Circle6;
extern TLabel Label7;
extern TCheckBox CheckBox1;
extern TCheckBox CheckBox2;
extern TCheckBox CheckBox3;
extern TCheckBox CheckBox4;
extern TCheckBox CheckBox5;
extern TCheckBox CheckBox6;
extern TButton Button1;
extern TButton * const code Screen6_Buttons[1];
extern TLabel * const code Screen6_Labels[8];
extern TCircle * const code Screen6_Circles[6];
extern TLine * const code Screen6_Lines[1];
extern TCheckBox * const code Screen6_CheckBoxes[6];

extern TScreen SplashLand;
extern TImage Image1;
extern TImage Image2;
extern TBox Box1;
extern TBox Box2;
extern TLabel Label8;
extern TLabel * const code Screen7_Labels[1];
extern TImage * const code Screen7_Images[2];
extern TBox * const code Screen7_Boxes[2];

extern TScreen ErrorLog;
extern TLabel Log_Labels[10];
extern TLabel Log_Title;
extern TButton_Round ErrorLog_OK;
extern TButton_Round * const code Screen8_Buttons_Round[1];
extern TLabel * const code Screen8_Labels[];

extern TScreen Config;
extern TLabel config_fill_value_label;
extern TLabel config_drain_value_label;
extern TButton_Round config_fill_button;
extern TButton_Round config_drain_button;
extern TButton_Round config_confirm_button;
extern TButton_Round config_ret_button;
extern TImage Image3;
extern TButton_Round * const code Screen9_Buttons_Round[4];
extern TLabel * const code Screen9_Labels[2];
extern TImage * const code Screen9_Images[1];



void cal_confirm_buttonClick();
void cal_fact_buttonClick();
void cal_offset_buttonClick();
void CalibrationScreenVisible(char visibility);
void cal_ret_buttonClick();
void cal_start_buttonClick();
void config_ret_buttonClick();
void D_buttonClick();
void ErrorLog_OKClick();
void I_buttonClick();
void Key_0Click();
void Key_1click();
void Key_2Click();
void Key_3Click();
void Key_4Click();
void Key_5Click();
void Key_6Click();
void Key_7Click();
void Key_8Click();
void Key_9Click();
void Key_clearClick();
void Key_dpClick();
void Key_enterClick();
void Key_return_buttonClick();
void Main_Screen_settingsClick();
void Main_Screen_startClick();
void Main_Screen_sv_downClick();
void Main_Screen_sv_upClick();
void PID_conf_buttonClick();
void PID_ret_buttonClick();
void pos_negClick();
void Prop_buttonClick();
void Settings_CALClick();
void Settings_ExitClick();
void Settings_PIDClick();

void Settings_LogClick();
void Diag_Clicked();
void Exit_Pressed();
void config_fill_buttonClick();
void config_drain_buttonClick();
void Config_Clicked();
void config_confirm_buttonClick();




extern char Key_7_Caption[];
extern char Key_8_Caption[];
extern char Key_9_Caption[];
extern char Key_clear_Caption[];
extern char Key_4_Caption[];
extern char Key_5_Caption[];
extern char Key_6_Caption[];
extern char Key_1_Caption[];
extern char Key_2_Caption[];
extern char Key_3_Caption[];
extern char Key_dp_Caption[];
extern char Key_0_Caption[];
extern char Key_enter_Caption[];
extern const char Key_return_button_Caption[];
extern char kp_display_Caption[];
extern const char Image8_Caption[];
extern char pos_neg_Caption[];


extern char Diagram3_Label1_Caption[];
extern char indicator_Caption[];
extern char Screen_PV_Caption[];
extern char Line2_Caption[];
extern char Main_Screen_settings_Caption[];
extern char Main_screen_settings_image_Caption[];
extern char Line1_Caption[];
extern char Main_Screen_start_Caption[];
extern char start_stop_Caption[];
extern char Diagram3_thermred_Caption[];
extern char Diagram3_thermBlack_Caption[];
extern char Label1_Caption[];
extern char Settings_temp_Caption[];
extern char Settings_PID_Caption[];
extern char Settings_CAL_Caption[];
extern char Settings_Exit_Caption[];
extern char therm_settings_Caption[];
extern char ToolBox_Caption[];
extern char imgToolBox_Caption[];
extern const char Prop_button_Caption[];
extern char P_value_Caption[];
extern char I_value_Caption[];
extern char D_value_Caption[];
extern char I_button_Caption[];
extern char D_button_Caption[];
extern char PID_conf_button_Caption[];
extern char PID_ret_button_Caption[];
extern char Image9_Caption[];
extern const char cal_offset_button_Caption[];
extern char cal_offset_value_Caption[];
extern char cal_factor_value_Caption[];
extern char cal_fact_button_Caption[];
extern char cal_confirm_button_Caption[];
extern char cal_ret_button_Caption[];
extern char cal_message_label_Caption[];
extern char cal_temp_label_Caption[];
extern char Image10_Caption[];
extern char cal_start_button_Caption[];
extern char cal_start_button_CaptionStop[];
extern char Line3_Caption[];
extern char Lbl_inputs_Caption[];
extern char Lbl_outputs_Caption[];
extern char Circle1_Caption[];
extern char Lbl_water_Caption[];
extern char Circle2_Caption[];
extern char Label3_Caption[];
extern char Circle3_Caption[];
extern char Label4_Caption[];
extern char Circle4_Caption[];
extern char Label5_Caption[];
extern char Circle5_Caption[];
extern char Label6_Caption[];
extern char Circle6_Caption[];
extern char Label7_Caption[];
extern char CheckBox1_Caption[];
extern char CheckBox2_Caption[];
extern char CheckBox3_Caption[];
extern char CheckBox4_Caption[];
extern char CheckBox5_Caption[];
extern char CheckBox6_Caption[];
extern char Image1_Caption[];
extern char Image2_Caption[];
extern char Box1_Caption[];
extern char Box2_Caption[];
extern char Label8_Caption[];
extern char Button1_Caption[];
extern char Log_Title_Caption[];
extern char ErrorLog_OK_Caption[];
extern char config_drain_button_Caption[];
extern char config_fill_button_Caption[];
extern char config_drain_value_label_Caption[];
extern char config_fill_value_label_Caption[];
extern char config_confirm_button_Caption[];
extern char config_ret_button_Caption[];
extern char Image3_Caption[];


void DrawScreen(TScreen *aScreen);
void DrawButton(TButton *aButton);
void DrawRoundButton(TButton_Round *Around_button);
void DrawCRoundButton(TCButton_Round *ACround_button);
void DrawLabel(TLabel *ALabel);
void DrawImage(TImage *AImage);
void DrawCImage(TCImage *ACimage);
void DrawCircle(TCircle *ACircle);
void DrawBox(TBox *ABox);
void DrawLine(TLine *Aline);
void DrawCheckBox(TCheckBox *ACheckBox);
void Check_TP();
void Start_TP();
void Process_TP_Press(unsigned int X, unsigned int Y);
void Process_TP_Up(unsigned int X, unsigned int Y);
void Process_TP_Down(unsigned int X, unsigned int Y);
#line 1 "c:/software/bpwasher_code/bpwasher_resources.h"
const code char Tahoma12x16_Regular[];
const code char Tahoma16x19_Regular[];
const code char Tahoma19x23_Regular[];
const code char Tahoma23x23_Bold[];
const code char Tahoma21x25_Regular[];
const code char Tahoma25x25_Bold[];
const code char Tahoma23x29_Regular[];
const code char Tahoma29x29_Bold[];
const code char Tahoma26x33_Regular[];
const code char Tahoma34x42_Regular[];
const code char Tahoma44x45_Bold[];
const code char Tahoma42x52_Regular[];
const code char Tahoma50x62_Regular[];
const code char Tahoma55x68_Regular[];
const code char Tahoma83x103_Regular[];
const code char Tahoma11x13_Regular[];
const code char returnsarrow_bmp[3366];
const code char Settings_bmp[3126];
const code char powergreen_bmp[4054];
const code char Thermometer_red_bmp[3206];
const code char Thermometer_black_bmp[3206];
const code char waterdripsmallland_bmp[68998];
const code char MPBE_Logo_small_jpg[26126];
#line 1 "c:/users/public/documents/mikroelektronika/mikroc pro for arm/include/built_in.h"
#line 1 "c:/software/bpwasher_code/constants.h"
#line 126 "c:/software/bpwasher_code/constants.h"
void cal_confirm_buttonClick();
void cal_fact_buttonClick();
void cal_offset_buttonClick();
void cal_ret_buttonClick();
void D_buttonClick();
void I_buttonClick();
void Key_0Click();
void Key_1click();
void Key_2Click();
void Key_3Click();
void Key_4Click();
void Key_5Click();
void Key_6Click();
void Key_7Click();
void Key_8Click();
void Key_9Click();
void Key_clearClick();
void Key_dpClick();
void Key_enterClick();
void Key_return_buttonClick();
void Main_Screen_settingsClick();
void Main_Screen_startClick();


void PID_conf_buttonClick();
void PID_ret_buttonClick();
void pos_negClick();
void Prop_buttonClick();
void Settings_CALClick();
void Settings_ExitClick();
void Settings_PIDClick();

void Settings_LogClick();

typedef struct params {
 float finit_value;
 float fvalue;
 float fValueMax;
 float fValueMin;
 TScreen *CallingDisplay;
 char *DisplayToUpdate;
 char nDecimalPlaces;
 } stParams;

extern stParams *ValueToSet;
extern stParams fProportional;
extern stParams fIntegral;
extern stParams fDerivative;
extern stParams fCal_offset;
extern stParams fCal_factor;
extern stParams fPassword;
extern stParams fMeasuredHigh;
extern stParams fMeasuredLow;
extern stParams fExtraFillTime;
extern stParams fDrainTime;
extern float fSetTemp;
extern float fCyclesComplete;
extern float fCyclesFail;
extern char bGlobalTempAlarm;
extern char bAtTemp;
extern char bFlash;
extern char bTemp;
extern int nSec_count;
extern char bRefresh;
extern float ActualTemp;

extern char nState;
extern char CyclesComplete[7];
extern char CyclesFail[7];
extern char sErrorLog[];






void ServiceHeater(void);
void button_up();
void button_down();
void set_and_format_value(stParams *StpValue, float fValue);
void Display_Keyboard(char *caption, stParams *StpValue);
void String_to_num(char nNumber);
void ret_prev_screen();
void enter_pressed();
void ResetKeypad();
void refresh_PV_display();
void heater_off_indicator();
void heater_on_indicator();

void GetInputStates();
void ConfigureInputs(void);
void SetOutput(char nOutputToDrive, char CallingChkBox);
void ClearAllOutputs();
char InputOn(char nInputIndex);




void ErrorLog_NewError(char *errMsg, char bIncrementCount);
void Recipe_State();
void StatusHalfSecondsCount();
void ResetRecipe();
float GetCurrentTemp();
float GetDisplayTemp();
void UpDateMemory();
#line 18 "C:/Software/BPWasher_Code/Heater.c"
float ActualTemp = 20.0;

signed int nRecent = 0;

float fCurrentDisplay = 0;

float LastReadings[ 25 ];

char bGlobalTempAlarm;

int SRC_Ticks = 0;
float SRC_Power = 0;
unsigned char SRC_State =  0 ;

char bAtTemp =  0 ;



signed int nFudgeTemp = 20.0 ;
#line 43 "C:/Software/BPWasher_Code/Heater.c"
 float GetCurrentTemp()
 {
 return ActualTemp;
 }
#line 53 "C:/Software/BPWasher_Code/Heater.c"
 float GetDisplayTemp()
 {
 return fCurrentDisplay;
 }
#line 66 "C:/Software/BPWasher_Code/Heater.c"
unsigned char nSeconds = 0;

void AddTempError(float TempError)
 {
 int i;

 if (++nSeconds >=  5 )
 {
 for (i =  25  - 1; i > 0; i--)
 LastReadings[i] = LastReadings[i - 1];

 LastReadings[0] = (TempError /  5 );

 nSeconds = 0;
 if (++nRecent >  25 )
 nRecent =  25 ;
 }
 }



float nTempLedDisplay[ 15 ];
int nTempIndex = 0;
int nTempDisplay = 0;
int nTempSetpoint = 0;
float Total;

void DisplayTemp(void)
 {
 char nIndex = 0;
 Total = 0;

 if (nTempIndex <  15 )
 {
 nTempLedDisplay[nTempIndex] = (ActualTemp);
 nTempIndex++;
 }
 else
 {

 for (nIndex = 0; nIndex <  15 -1; nIndex++)
 nTempLedDisplay[nIndex] = nTempLedDisplay[nIndex + 1];

 nTempLedDisplay[ 15 -1] = (ActualTemp);
 }

 for (nIndex = 0; nIndex < (nTempIndex); nIndex++)
 {
 Total += nTempLedDisplay[nIndex] ;
 }

 fCurrentDisplay = Total;
 fCurrentDisplay = fCurrentDisplay / (nTempIndex);
 fCurrentDisplay *= 10;
 modf (fCurrentDisplay , &fCurrentDisplay);
 fCurrentDisplay /= 10;

 Total = Total / (nTempIndex);

 bAtTemp =  0 ;
 if (fSetTemp)
 {
 nTempSetpoint = fSetTemp * 100;
 nTempDisplay = fCurrentDisplay *100;
 nTempDisplay = (nTempSetpoint) - (nTempDisplay );
 if (((nTempDisplay ) < 70) && ((nTempDisplay) > -179))
 bAtTemp =  1 ;

 }

 }

float fLastNumberDisplay = -1;


void refresh_PV_display()
 {
 DisplayTemp();
 if (fCurrentDisplay == fLastNumberDisplay && Screen_PV.Font_Color ==  (fSetTemp>0 ? (bAtTemp ? CL_LIME : 0xF904) : 0xC618) )
 return;

 Screen_PV.Font_Color = 0x0000;
 DrawButton(&Screen_PV);

 sprintf(Screen_PV_caption, "%3.1f", fCurrentDisplay);
 Screen_PV.Font_Color =  (fSetTemp>0 ? (bAtTemp ? CL_LIME : 0xF904) : 0xC618) ;
 DrawButton(&Screen_PV);
 fLastNumberDisplay = fCurrentDisplay;
 }
#line 161 "C:/Software/BPWasher_Code/Heater.c"
float GetDerivative(void)
 {
 float nRate = 0;
 int nPeriod;

 if (nRecent > 20)
 nPeriod = 20 /  5 ;
 else
 nPeriod = nRecent - 1;

 if (0 < nPeriod)
 nRate = LastReadings[0] - LastReadings[nPeriod];

 return nRate;
 }
#line 182 "C:/Software/BPWasher_Code/Heater.c"
float GetIntegral(void)
 {
 float nIntegral;
 int i;

 nIntegral = 0;
 for (i = 0; i < nRecent; i++)
 nIntegral += LastReadings[i];

 return nIntegral;
 }
#line 199 "C:/Software/BPWasher_Code/Heater.c"
float CalculatePowerDemand(void)
 {
 float fPowerDemand = 0;
 float TempError = 0;



 ActualTemp = ADC1_Get_Sample(1);
 ActualTemp += ADC1_Get_Sample(1);
 ActualTemp += ADC1_Get_Sample(1);
 ActualTemp /= 3;
 ActualTemp *=  1.8 / 4096 ;


 ActualTemp = 120 * (ActualTemp / 1.8);
 ActualTemp *= fCal_factor.fvalue;
 ActualTemp += fCal_offset.fvalue ;

 ActualTemp = nFudgeTemp;


 if (ActualTemp > 100.0)
 ActualTemp = 100.0;
 else if (ActualTemp < 0)
 ActualTemp = 0;

 if (fSetTemp == 0)
 return 0;

 TempError = fSetTemp - ActualTemp;
 AddTempError(TempError);

 fPowerDemand = TempError * fProportional.fvalue
 + GetIntegral() * fIntegral.fvalue
 + GetDerivative() * fDerivative.fvalue;

 if (fPowerDemand >  98 )
 fPowerDemand =  98 ;
 else if (fPowerDemand < 0)
 fPowerDemand = 0;

 return fPowerDemand;
 }




int nHeatOffDelay = 0;

void CheckHeaterAlarm(void)
 {
 char msg[100];




 if (SRC_State ==  1  || fabs(ActualTemp-fSetTemp)<5 || bGlobalTempAlarm)
 {



 nHeatOffDelay = 0;

 return;
 }


 if (++nHeatOffDelay <  200 )
 return;

 bGlobalTempAlarm = InputOn( 0x06 );

 nHeatOffDelay = bGlobalTempAlarm ? -3000
 : 0;

 if (bGlobalTempAlarm && nState!= 101 )
 {
 sprintf(msg, "Elm SSR Short S=%dT=%.0f/%.0f", SRC_State, ActualTemp, fSetTemp);
 ErrorLog_NewError(msg,  1 );
 nState =  100 ;
 }
 }
#line 290 "C:/Software/BPWasher_Code/Heater.c"
void ServiceHeater(void)
 {
 CheckHeaterAlarm();
 ++SRC_Ticks;

 if ( 1  == SRC_State)
 {
 if (SRC_Ticks >= SRC_Power)
 {
  GPIOD_ODR.B11 = 1 ;
 SRC_State =  0 ;
 heater_off_indicator();
 }
 }
 else
 {
 if (1 == SRC_Ticks)
 {

  GPIOD_ODR.B11 = 1 ;
 heater_off_indicator();
 }
 else if (SRC_Ticks >= 10)
 {
 SRC_Power = CalculatePowerDemand();
 if (0 < SRC_Power)
 {
  GPIOD_ODR.B11 = 0 ;
 SRC_State =  1 ;
 heater_on_indicator();
 }

 SRC_Ticks = 0;
 }
 }
}
