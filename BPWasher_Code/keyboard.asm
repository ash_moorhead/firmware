_set_and_format_value:
;keyboard.c,25 :: 		void set_and_format_value(stParams *StpValue, float fValue)
; fValue start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; StpValue start address is: 0 (R0)
VMOV.F32	S2, S0
; fValue end address is: 0 (R0)
; StpValue end address is: 0 (R0)
; StpValue start address is: 0 (R0)
; fValue start address is: 8 (R2)
;keyboard.c,27 :: 		StpValue->fvalue = fValue;
ADDS	R1, R0, #4
VSTR	#1, S2, [R1, #0]
;keyboard.c,28 :: 		if( (StpValue->DisplayToUpdate == config_drain_value_label_Caption) || (StpValue->DisplayToUpdate == config_fill_value_label_Caption))
ADDW	R1, R0, #20
LDR	R2, [R1, #0]
MOVW	R1, #lo_addr(_config_drain_value_label_Caption+0)
MOVT	R1, #hi_addr(_config_drain_value_label_Caption+0)
CMP	R2, R1
IT	EQ
BEQ	L__set_and_format_value46
ADDW	R1, R0, #20
LDR	R2, [R1, #0]
MOVW	R1, #lo_addr(_config_fill_value_label_Caption+0)
MOVT	R1, #hi_addr(_config_fill_value_label_Caption+0)
CMP	R2, R1
IT	EQ
BEQ	L__set_and_format_value45
IT	AL
BAL	L_set_and_format_value2
L__set_and_format_value46:
L__set_and_format_value45:
;keyboard.c,30 :: 		strcpy(formatString,"%.0f s");
MOVW	R1, #lo_addr(?lstr1_keyboard+0)
MOVT	R1, #hi_addr(?lstr1_keyboard+0)
STR	R0, [SP, #4]
MOVW	R0, #lo_addr(_formatString+0)
MOVT	R0, #hi_addr(_formatString+0)
BL	_strcpy+0
LDR	R0, [SP, #4]
;keyboard.c,31 :: 		} else
IT	AL
BAL	L_set_and_format_value3
L_set_and_format_value2:
;keyboard.c,33 :: 		sprintf(formatString,"%%3.%df", StpValue->nDecimalPlaces);
ADDW	R1, R0, #24
LDRB	R1, [R1, #0]
UXTB	R3, R1
MOVW	R2, #lo_addr(?lstr_2_keyboard+0)
MOVT	R2, #hi_addr(?lstr_2_keyboard+0)
MOVW	R1, #lo_addr(_formatString+0)
MOVT	R1, #hi_addr(_formatString+0)
STR	R0, [SP, #4]
PUSH	(R3)
PUSH	(R2)
PUSH	(R1)
BL	_sprintf+0
ADD	SP, SP, #12
LDR	R0, [SP, #4]
;keyboard.c,34 :: 		}
L_set_and_format_value3:
;keyboard.c,35 :: 		sprintf(StpValue->DisplayToUpdate, formatString, fValue);
MOVW	R2, #lo_addr(_formatString+0)
MOVT	R2, #hi_addr(_formatString+0)
ADDW	R1, R0, #20
; StpValue end address is: 0 (R0)
LDR	R1, [R1, #0]
VPUSH	#0, (S2)
; fValue end address is: 8 (R2)
PUSH	(R2)
PUSH	(R1)
BL	_sprintf+0
ADD	SP, SP, #12
;keyboard.c,36 :: 		}
L_end_set_and_format_value:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _set_and_format_value
_error:
;keyboard.c,38 :: 		void error(char *msg)
SUB	SP, SP, #8
STR	LR, [SP, #0]
STR	R0, [SP, #4]
;keyboard.c,40 :: 		ResetKeypad();
BL	_ResetKeypad+0
;keyboard.c,41 :: 		strcpy(&kp_display_Caption, ERROR);
MOVW	R1, #lo_addr(?lstr3_keyboard+0)
MOVT	R1, #hi_addr(?lstr3_keyboard+0)
MOVW	R0, #lo_addr(_kp_display_Caption+0)
MOVT	R0, #hi_addr(_kp_display_Caption+0)
BL	_strcpy+0
;keyboard.c,42 :: 		DrawButton(&kp_display);
MOVW	R0, #lo_addr(_kp_display+0)
MOVT	R0, #hi_addr(_kp_display+0)
BL	_DrawButton+0
;keyboard.c,44 :: 		lblKeyboardTitle.Font_Color = CL_BLACK;
MOVW	R2, #0
MOVW	R1, #lo_addr(_lblKeyboardTitle+24)
MOVT	R1, #hi_addr(_lblKeyboardTitle+24)
STRH	R2, [R1, #0]
;keyboard.c,45 :: 		DrawLabel(&lblKeyboardTitle);
MOVW	R0, #lo_addr(_lblKeyboardTitle+0)
MOVT	R0, #hi_addr(_lblKeyboardTitle+0)
BL	_DrawLabel+0
;keyboard.c,46 :: 		lblKeyboardTitle.Font_Color = LABEL_FONT_COLOUR;
MOVW	R2, #65504
MOVW	R1, #lo_addr(_lblKeyboardTitle+24)
MOVT	R1, #hi_addr(_lblKeyboardTitle+24)
STRH	R2, [R1, #0]
;keyboard.c,47 :: 		lblKeyboardTitle.Caption = msg;
LDR	R2, [SP, #4]
MOVW	R1, #lo_addr(_lblKeyboardTitle+16)
MOVT	R1, #hi_addr(_lblKeyboardTitle+16)
STR	R2, [R1, #0]
;keyboard.c,48 :: 		DrawLabel(&lblKeyboardTitle);
MOVW	R0, #lo_addr(_lblKeyboardTitle+0)
MOVT	R0, #hi_addr(_lblKeyboardTitle+0)
BL	_DrawLabel+0
;keyboard.c,49 :: 		}
L_end_error:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _error
_enter_pressed:
;keyboard.c,51 :: 		void enter_pressed()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;keyboard.c,53 :: 		if (ValueToSet == &fMeasuredHigh && (fNew_Value < 58 || fNew_Value > 62)) {
MOVW	R0, #lo_addr(_ValueToSet+0)
MOVT	R0, #hi_addr(_ValueToSet+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_fMeasuredHigh+0)
MOVT	R0, #hi_addr(_fMeasuredHigh+0)
CMP	R1, R0
IT	NE
BNE	L__enter_pressed53
MOVW	R0, #lo_addr(_fNew_Value+0)
MOVT	R0, #hi_addr(_fNew_Value+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #0
MOVT	R0, #17000
VMOV	S0, R0
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	LT
BLT	L__enter_pressed52
MOVW	R0, #lo_addr(_fNew_Value+0)
MOVT	R0, #hi_addr(_fNew_Value+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #0
MOVT	R0, #17016
VMOV	S0, R0
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	GT
BGT	L__enter_pressed51
IT	AL
BAL	L_enter_pressed8
L__enter_pressed52:
L__enter_pressed51:
L__enter_pressed49:
;keyboard.c,54 :: 		error("Unexpected value, contact MPBE");
MOVW	R0, #lo_addr(?lstr4_keyboard+0)
MOVT	R0, #hi_addr(?lstr4_keyboard+0)
BL	_error+0
;keyboard.c,55 :: 		}
L_enter_pressed8:
;keyboard.c,53 :: 		if (ValueToSet == &fMeasuredHigh && (fNew_Value < 58 || fNew_Value > 62)) {
L__enter_pressed53:
;keyboard.c,56 :: 		if (ValueToSet == &fMeasuredLow && (fNew_Value < 35 || fNew_Value > 39)) {
MOVW	R0, #lo_addr(_ValueToSet+0)
MOVT	R0, #hi_addr(_ValueToSet+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_fMeasuredLow+0)
MOVT	R0, #hi_addr(_fMeasuredLow+0)
CMP	R1, R0
IT	NE
BNE	L__enter_pressed56
MOVW	R0, #lo_addr(_fNew_Value+0)
MOVT	R0, #hi_addr(_fNew_Value+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #0
MOVT	R0, #16908
VMOV	S0, R0
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	LT
BLT	L__enter_pressed55
MOVW	R0, #lo_addr(_fNew_Value+0)
MOVT	R0, #hi_addr(_fNew_Value+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #0
MOVT	R0, #16924
VMOV	S0, R0
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	GT
BGT	L__enter_pressed54
IT	AL
BAL	L_enter_pressed13
L__enter_pressed55:
L__enter_pressed54:
L__enter_pressed47:
;keyboard.c,57 :: 		error("Unexpected value, contact MPBE");
MOVW	R0, #lo_addr(?lstr5_keyboard+0)
MOVT	R0, #hi_addr(?lstr5_keyboard+0)
BL	_error+0
;keyboard.c,58 :: 		}
L_enter_pressed13:
;keyboard.c,56 :: 		if (ValueToSet == &fMeasuredLow && (fNew_Value < 35 || fNew_Value > 39)) {
L__enter_pressed56:
;keyboard.c,59 :: 		if (ValueToSet == &fPassword)
MOVW	R0, #lo_addr(_ValueToSet+0)
MOVT	R0, #hi_addr(_ValueToSet+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_fPassword+0)
MOVT	R0, #hi_addr(_fPassword+0)
CMP	R1, R0
IT	NE
BNE	L_enter_pressed14
;keyboard.c,61 :: 		if (fNew_Value == 6776)
MOVW	R0, #lo_addr(_fNew_Value+0)
MOVT	R0, #hi_addr(_fNew_Value+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #49152
MOVT	R0, #17875
VMOV	S0, R0
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	NE
BNE	L_enter_pressed15
;keyboard.c,62 :: 		DrawScreen(&settings);
MOVW	R0, #lo_addr(_Settings+0)
MOVT	R0, #hi_addr(_Settings+0)
BL	_DrawScreen+0
IT	AL
BAL	L_enter_pressed16
L_enter_pressed15:
;keyboard.c,64 :: 		error("Incorrect password");
MOVW	R0, #lo_addr(?lstr6_keyboard+0)
MOVT	R0, #hi_addr(?lstr6_keyboard+0)
BL	_error+0
L_enter_pressed16:
;keyboard.c,65 :: 		}
IT	AL
BAL	L_enter_pressed17
L_enter_pressed14:
;keyboard.c,66 :: 		else if (!IS_ERROR)
MOVW	R0, #lo_addr(_kp_display_Caption+0)
MOVT	R0, #hi_addr(_kp_display_Caption+0)
LDRB	R0, [R0, #0]
CMP	R0, #101
IT	EQ
BEQ	L_enter_pressed18
;keyboard.c,69 :: 		set_and_format_value(ValueToSet, fNew_Value);
MOVW	R0, #lo_addr(_fNew_Value+0)
MOVT	R0, #hi_addr(_fNew_Value+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_ValueToSet+0)
MOVT	R0, #hi_addr(_ValueToSet+0)
LDR	R0, [R0, #0]
BL	_set_and_format_value+0
;keyboard.c,72 :: 		UpDateMemory();
BL	_UpDateMemory+0
;keyboard.c,75 :: 		ret_prev_screen();
BL	_ret_prev_screen+0
;keyboard.c,76 :: 		}
L_enter_pressed18:
L_enter_pressed17:
;keyboard.c,77 :: 		}
L_end_enter_pressed:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _enter_pressed
_ret_prev_screen:
;keyboard.c,79 :: 		void ret_prev_screen()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;keyboard.c,81 :: 		DrawScreen(ValueToSet->CallingDisplay);
MOVW	R0, #lo_addr(_ValueToSet+0)
MOVT	R0, #hi_addr(_ValueToSet+0)
LDR	R0, [R0, #0]
ADDS	R0, #16
LDR	R0, [R0, #0]
BL	_DrawScreen+0
;keyboard.c,82 :: 		}
L_end_ret_prev_screen:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _ret_prev_screen
_ResetKeypad:
;keyboard.c,84 :: 		void ResetKeypad()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;keyboard.c,86 :: 		*kp_display.Caption = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_kp_display+24)
MOVT	R0, #hi_addr(_kp_display+24)
LDR	R0, [R0, #0]
STRB	R1, [R0, #0]
;keyboard.c,87 :: 		fNew_Value = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fNew_Value+0)
MOVT	R0, #hi_addr(_fNew_Value+0)
VSTR	#1, S0, [R0, #0]
;keyboard.c,88 :: 		bDecimalPlacePressed = FALSE;
MOVS	R1, #0
MOVW	R0, #lo_addr(_bDecimalPlacePressed+0)
MOVT	R0, #hi_addr(_bDecimalPlacePressed+0)
STRB	R1, [R0, #0]
;keyboard.c,89 :: 		bNumberOfDecPlaces = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_bNumberOfDecPlaces+0)
MOVT	R0, #hi_addr(_bNumberOfDecPlaces+0)
STRB	R1, [R0, #0]
;keyboard.c,91 :: 		lblKeyboardTitle.Font_Color = CL_BLACK;
MOVW	R1, #0
MOVW	R0, #lo_addr(_lblKeyboardTitle+24)
MOVT	R0, #hi_addr(_lblKeyboardTitle+24)
STRH	R1, [R0, #0]
;keyboard.c,92 :: 		DrawLabel(&lblKeyboardTitle);
MOVW	R0, #lo_addr(_lblKeyboardTitle+0)
MOVT	R0, #hi_addr(_lblKeyboardTitle+0)
BL	_DrawLabel+0
;keyboard.c,94 :: 		lblKeyboardTitle.Font_Color = LABEL_FONT_COLOUR;
MOVW	R1, #65504
MOVW	R0, #lo_addr(_lblKeyboardTitle+24)
MOVT	R0, #hi_addr(_lblKeyboardTitle+24)
STRH	R1, [R0, #0]
;keyboard.c,95 :: 		lblKeyboardTitle.Caption = sTitle;
MOVW	R0, #lo_addr(_sTitle+0)
MOVT	R0, #hi_addr(_sTitle+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_lblKeyboardTitle+16)
MOVT	R0, #hi_addr(_lblKeyboardTitle+16)
STR	R1, [R0, #0]
;keyboard.c,96 :: 		DrawLabel(&lblKeyboardTitle);
MOVW	R0, #lo_addr(_lblKeyboardTitle+0)
MOVT	R0, #hi_addr(_lblKeyboardTitle+0)
BL	_DrawLabel+0
;keyboard.c,97 :: 		DrawButton(&kp_display);
MOVW	R0, #lo_addr(_kp_display+0)
MOVT	R0, #hi_addr(_kp_display+0)
BL	_DrawButton+0
;keyboard.c,98 :: 		}
L_end_ResetKeypad:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _ResetKeypad
_Display_Keyboard:
;keyboard.c,101 :: 		void Display_Keyboard(char *caption, stParams *StpValue)
; StpValue start address is: 4 (R1)
; caption start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; StpValue end address is: 4 (R1)
; caption end address is: 0 (R0)
; caption start address is: 0 (R0)
; StpValue start address is: 4 (R1)
;keyboard.c,103 :: 		ValueToSet =  StpValue;
MOVW	R2, #lo_addr(_ValueToSet+0)
MOVT	R2, #hi_addr(_ValueToSet+0)
STR	R1, [R2, #0]
;keyboard.c,104 :: 		nMaxNumberOfDecimalPlaces  = ValueToSet->nDecimalPlaces;
ADDW	R2, R1, #24
LDRB	R3, [R2, #0]
MOVW	R2, #lo_addr(_nMaxNumberOfDecimalPlaces+0)
MOVT	R2, #hi_addr(_nMaxNumberOfDecimalPlaces+0)
STRB	R3, [R2, #0]
;keyboard.c,105 :: 		fMin = ValueToSet->fValueMin;
ADDW	R2, R1, #12
VLDR	#1, S1, [R2, #0]
MOVW	R2, #lo_addr(_fMin+0)
MOVT	R2, #hi_addr(_fMin+0)
VSTR	#1, S1, [R2, #0]
;keyboard.c,106 :: 		fMax = ValueToSet->fValueMax;
ADDW	R2, R1, #8
; StpValue end address is: 4 (R1)
VLDR	#1, S0, [R2, #0]
MOVW	R2, #lo_addr(_fMax+0)
MOVT	R2, #hi_addr(_fMax+0)
VSTR	#1, S0, [R2, #0]
;keyboard.c,107 :: 		pos_neg.Visible = fMin < 0 ? 1 : 0;
VCMPE.F32	S1, #0
VMRS	#60, FPSCR
IT	GE
BGE	L_Display_Keyboard19
; ?FLOC___Display_Keyboard?T98 start address is: 4 (R1)
MOVS	R1, #1
SXTB	R1, R1
; ?FLOC___Display_Keyboard?T98 end address is: 4 (R1)
IT	AL
BAL	L_Display_Keyboard20
L_Display_Keyboard19:
; ?FLOC___Display_Keyboard?T98 start address is: 4 (R1)
MOVS	R1, #0
SXTB	R1, R1
; ?FLOC___Display_Keyboard?T98 end address is: 4 (R1)
L_Display_Keyboard20:
; ?FLOC___Display_Keyboard?T98 start address is: 4 (R1)
MOVW	R2, #lo_addr(_pos_neg+18)
MOVT	R2, #hi_addr(_pos_neg+18)
STRB	R1, [R2, #0]
; ?FLOC___Display_Keyboard?T98 end address is: 4 (R1)
;keyboard.c,108 :: 		Key_dp.Visible = nMaxNumberOfDecimalPlaces ? 1 : 0;
MOVW	R2, #lo_addr(_nMaxNumberOfDecimalPlaces+0)
MOVT	R2, #hi_addr(_nMaxNumberOfDecimalPlaces+0)
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Display_Keyboard21
; ?FLOC___Display_Keyboard?T99 start address is: 4 (R1)
MOVS	R1, #1
SXTB	R1, R1
; ?FLOC___Display_Keyboard?T99 end address is: 4 (R1)
IT	AL
BAL	L_Display_Keyboard22
L_Display_Keyboard21:
; ?FLOC___Display_Keyboard?T99 start address is: 4 (R1)
MOVS	R1, #0
SXTB	R1, R1
; ?FLOC___Display_Keyboard?T99 end address is: 4 (R1)
L_Display_Keyboard22:
; ?FLOC___Display_Keyboard?T99 start address is: 4 (R1)
MOVW	R2, #lo_addr(_Key_dp+18)
MOVT	R2, #hi_addr(_Key_dp+18)
STRB	R1, [R2, #0]
; ?FLOC___Display_Keyboard?T99 end address is: 4 (R1)
;keyboard.c,110 :: 		sTitle = caption; // store a copy so it can be repainted when clearing an error
MOVW	R2, #lo_addr(_sTitle+0)
MOVT	R2, #hi_addr(_sTitle+0)
STR	R0, [R2, #0]
; caption end address is: 0 (R0)
;keyboard.c,111 :: 		ResetKeypad();
BL	_ResetKeypad+0
;keyboard.c,113 :: 		DrawScreen(&Keypad);
MOVW	R0, #lo_addr(_Keypad+0)
MOVT	R0, #hi_addr(_Keypad+0)
BL	_DrawScreen+0
;keyboard.c,114 :: 		}
L_end_Display_Keyboard:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Display_Keyboard
_String_to_num:
;keyboard.c,122 :: 		void String_to_num(char nNumber)
; nNumber start address is: 0 (R0)
SUB	SP, SP, #20
STR	LR, [SP, #0]
; nNumber end address is: 0 (R0)
; nNumber start address is: 0 (R0)
;keyboard.c,126 :: 		if (IS_ERROR)  // this is true when the caption shows an error state
MOVW	R1, #lo_addr(_kp_display_Caption+0)
MOVT	R1, #hi_addr(_kp_display_Caption+0)
LDRB	R1, [R1, #0]
CMP	R1, #101
IT	NE
BNE	L_String_to_num23
;keyboard.c,127 :: 		ResetKeypad();
STRB	R0, [SP, #4]
BL	_ResetKeypad+0
LDRB	R0, [SP, #4]
L_String_to_num23:
;keyboard.c,129 :: 		if (nNumber == '-')
CMP	R0, #45
IT	NE
BNE	L__String_to_num63
; nNumber end address is: 0 (R0)
;keyboard.c,131 :: 		if (*kp_display_Caption == '-')
MOVW	R1, #lo_addr(_kp_display_Caption+0)
MOVT	R1, #hi_addr(_kp_display_Caption+0)
LDRB	R1, [R1, #0]
CMP	R1, #45
IT	NE
BNE	L_String_to_num25
;keyboard.c,132 :: 		strcpy(/*dest*/ kp_display_Caption, /*source*/ kp_display_Caption+1); // remove leading '-'
MOVW	R1, #lo_addr(_kp_display_Caption+1)
MOVT	R1, #hi_addr(_kp_display_Caption+1)
MOVW	R0, #lo_addr(_kp_display_Caption+0)
MOVT	R0, #hi_addr(_kp_display_Caption+0)
BL	_strcpy+0
IT	AL
BAL	L_String_to_num26
L_String_to_num25:
;keyboard.c,135 :: 		sprintf(sTempNumber, "-%s", kp_display_Caption);
MOVW	R3, #lo_addr(_kp_display_Caption+0)
MOVT	R3, #hi_addr(_kp_display_Caption+0)
MOVW	R2, #lo_addr(?lstr_7_keyboard+0)
MOVT	R2, #hi_addr(?lstr_7_keyboard+0)
ADD	R1, SP, #8
PUSH	(R3)
PUSH	(R2)
PUSH	(R1)
BL	_sprintf+0
ADD	SP, SP, #12
;keyboard.c,136 :: 		strcpy(kp_display_Caption, sTempNumber);
ADD	R1, SP, #8
MOVW	R0, #lo_addr(_kp_display_Caption+0)
MOVT	R0, #hi_addr(_kp_display_Caption+0)
BL	_strcpy+0
;keyboard.c,137 :: 		}
L_String_to_num26:
;keyboard.c,138 :: 		nNumber = NULL;
; nNumber start address is: 24 (R6)
MOVS	R6, #0
; nNumber end address is: 24 (R6)
;keyboard.c,139 :: 		}
IT	AL
BAL	L_String_to_num24
L__String_to_num63:
;keyboard.c,129 :: 		if (nNumber == '-')
UXTB	R6, R0
;keyboard.c,139 :: 		}
L_String_to_num24:
;keyboard.c,141 :: 		if (nNumber == '.' && bDecimalPlacePressed)
; nNumber start address is: 24 (R6)
CMP	R6, #46
IT	NE
BNE	L__String_to_num60
MOVW	R1, #lo_addr(_bDecimalPlacePressed+0)
MOVT	R1, #hi_addr(_bDecimalPlacePressed+0)
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__String_to_num59
; nNumber end address is: 24 (R6)
L__String_to_num58:
;keyboard.c,142 :: 		return;
IT	AL
BAL	L_end_String_to_num
;keyboard.c,141 :: 		if (nNumber == '.' && bDecimalPlacePressed)
L__String_to_num60:
; nNumber start address is: 24 (R6)
L__String_to_num59:
;keyboard.c,143 :: 		else if (nNumber == '.')
CMP	R6, #46
IT	NE
BNE	L_String_to_num31
;keyboard.c,144 :: 		bDecimalPlacePressed = TRUE;
MOVS	R2, #1
MOVW	R1, #lo_addr(_bDecimalPlacePressed+0)
MOVT	R1, #hi_addr(_bDecimalPlacePressed+0)
STRB	R2, [R1, #0]
IT	AL
BAL	L_String_to_num32
L_String_to_num31:
;keyboard.c,145 :: 		else if (bDecimalPlacePressed && nNumber)
MOVW	R1, #lo_addr(_bDecimalPlacePressed+0)
MOVT	R1, #hi_addr(_bDecimalPlacePressed+0)
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__String_to_num62
CMP	R6, #0
IT	EQ
BEQ	L__String_to_num61
L__String_to_num57:
;keyboard.c,146 :: 		if (++bNumberOfDecPlaces > nMaxNumberOfDecimalPlaces)
MOVW	R3, #lo_addr(_bNumberOfDecPlaces+0)
MOVT	R3, #hi_addr(_bNumberOfDecPlaces+0)
LDRB	R1, [R3, #0]
ADDS	R2, R1, #1
UXTB	R2, R2
STRB	R2, [R3, #0]
MOVW	R1, #lo_addr(_nMaxNumberOfDecimalPlaces+0)
MOVT	R1, #hi_addr(_nMaxNumberOfDecimalPlaces+0)
LDRB	R1, [R1, #0]
CMP	R2, R1
IT	LS
BLS	L_String_to_num36
; nNumber end address is: 24 (R6)
;keyboard.c,148 :: 		error("Too many decimal places");
MOVW	R1, #lo_addr(?lstr8_keyboard+0)
MOVT	R1, #hi_addr(?lstr8_keyboard+0)
MOV	R0, R1
BL	_error+0
;keyboard.c,149 :: 		return;
IT	AL
BAL	L_end_String_to_num
;keyboard.c,150 :: 		}
L_String_to_num36:
;keyboard.c,145 :: 		else if (bDecimalPlacePressed && nNumber)
; nNumber start address is: 24 (R6)
L__String_to_num62:
L__String_to_num61:
;keyboard.c,150 :: 		}
L_String_to_num32:
;keyboard.c,152 :: 		if (nNumber)
CMP	R6, #0
IT	EQ
BEQ	L_String_to_num37
;keyboard.c,154 :: 		sTempNumber[0] = nNumber;
ADD	R3, SP, #8
STRB	R6, [R3, #0]
; nNumber end address is: 24 (R6)
;keyboard.c,155 :: 		sTempNumber[1] = NULL;
ADDS	R2, R3, #1
MOVS	R1, #0
STRB	R1, [R2, #0]
;keyboard.c,157 :: 		strcat(kp_display_Caption, sTempNumber); // concatenate next entered digit to display caption
MOV	R1, R3
MOVW	R0, #lo_addr(_kp_display_Caption+0)
MOVT	R0, #hi_addr(_kp_display_Caption+0)
BL	_strcat+0
;keyboard.c,158 :: 		}
L_String_to_num37:
;keyboard.c,160 :: 		fNew_Value = atof(kp_display_Caption);  // convert entered value from string to number
MOVW	R0, #lo_addr(_kp_display_Caption+0)
MOVT	R0, #hi_addr(_kp_display_Caption+0)
BL	_atof+0
MOVW	R1, #lo_addr(_fNew_Value+0)
MOVT	R1, #hi_addr(_fNew_Value+0)
VSTR	#1, S0, [R1, #0]
;keyboard.c,162 :: 		if (strlen(kp_display_Caption) > 5)
MOVW	R0, #lo_addr(_kp_display_Caption+0)
MOVT	R0, #hi_addr(_kp_display_Caption+0)
BL	_strlen+0
CMP	R0, #5
IT	LE
BLE	L_String_to_num38
;keyboard.c,163 :: 		error("Too long");
MOVW	R1, #lo_addr(?lstr9_keyboard+0)
MOVT	R1, #hi_addr(?lstr9_keyboard+0)
MOV	R0, R1
BL	_error+0
IT	AL
BAL	L_String_to_num39
L_String_to_num38:
;keyboard.c,164 :: 		else if (fNew_Value > fMax)
MOVW	R1, #lo_addr(_fMax+0)
MOVT	R1, #hi_addr(_fMax+0)
VLDR	#1, S1, [R1, #0]
MOVW	R1, #lo_addr(_fNew_Value+0)
MOVT	R1, #hi_addr(_fNew_Value+0)
VLDR	#1, S0, [R1, #0]
VCMPE.F32	S0, S1
VMRS	#60, FPSCR
IT	LE
BLE	L_String_to_num40
;keyboard.c,165 :: 		error("Too high");
MOVW	R1, #lo_addr(?lstr10_keyboard+0)
MOVT	R1, #hi_addr(?lstr10_keyboard+0)
MOV	R0, R1
BL	_error+0
IT	AL
BAL	L_String_to_num41
L_String_to_num40:
;keyboard.c,166 :: 		else if (fNew_Value < fMin)
MOVW	R1, #lo_addr(_fMin+0)
MOVT	R1, #hi_addr(_fMin+0)
VLDR	#1, S1, [R1, #0]
MOVW	R1, #lo_addr(_fNew_Value+0)
MOVT	R1, #hi_addr(_fNew_Value+0)
VLDR	#1, S0, [R1, #0]
VCMPE.F32	S0, S1
VMRS	#60, FPSCR
IT	GE
BGE	L_String_to_num42
;keyboard.c,167 :: 		error("Too low");
MOVW	R1, #lo_addr(?lstr11_keyboard+0)
MOVT	R1, #hi_addr(?lstr11_keyboard+0)
MOV	R0, R1
BL	_error+0
IT	AL
BAL	L_String_to_num43
L_String_to_num42:
;keyboard.c,169 :: 		DrawButton(&kp_display);
MOVW	R0, #lo_addr(_kp_display+0)
MOVT	R0, #hi_addr(_kp_display+0)
BL	_DrawButton+0
L_String_to_num43:
L_String_to_num41:
L_String_to_num39:
;keyboard.c,170 :: 		}
L_end_String_to_num:
LDR	LR, [SP, #0]
ADD	SP, SP, #20
BX	LR
; end of _String_to_num
