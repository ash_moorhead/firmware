#define SWITCH_ON_I2C   TRUE   // TODO switch this back to TRUE before deploy in production
#define VERSION_MAJOR   "1"
#define VERSION_MINOR   "6"  // TODO update version & build number - CM
#define VERSION         VERSION_MAJOR "." VERSION_MINOR
#define BUILD           (atoi(VERSION_MAJOR)*1000 + atoi(VERSION_MINOR))
/*
 BUILD is stored in the EEPROM Memory so can be used so selectively load data when data format changes.
 it is also used to record in the error log when the software is upgraded.
 
    BUILD     VERSION
    n/a       < 1.3
    1         1.3
    1004      1.4
*/

#define FO_VERTICAL_COLUMN FO_VERTICAL
#define TRUE 1
#define FALSE 0
#define NULL '\0'
// Prototype port pin assignments
/*
#define HEATER_ON  GPIOD_ODR.B11 = 0    //  GPIOB_ODR.B11
#define HEATER_OFF  GPIOD_ODR.B11 = 1
#define RELAY1_ON  GPIOB_ODR.B12 = TRUE;   //user relay1
#define RELAY1_OFF  GPIOB_ODR.B12 = FALSE;
#define RELAY2_ON  GPIOB_ODR.B13 = TRUE;   // user relay2
#define RELAY2_OFF  GPIOB_ODR.B13 = FALSE;
#define CPU_MON_ON  GPIOB_ODR.B14 = FALSE;
#define CPU_MON_OFF  GPIOB_ODR.B14 = TRUE;
#define FLOAT_SW  GPIOB_IDR.B15
#define ELEMENT_CHECK  GPIOD_IDR.B0  */

//Inputs
#define LID_SHUT        0x00    // The lid closed relay
#define START_BTN       0x01
#define LID_OPEN_BTN    0x02    // Button pressed by user to request the lid catch is opened
//#define OVER_TEMP       0x03  // not used? - CM 14/07/17
#define WATER_LEVEL     0x04
//#define IMPELLOR_MAIN   0x05  // not used - CM 14/07/17
#define ELEMENT_FAULT   0x05    // added by Chris 14/07/17  TODO need to finalise which input and whether TRUE=fault or ok
#define HEATER_MAIN_ON  0x06
//Outputs
#define CYCLE_RUN_LIGHT      0x00
#define CYCLE_COMPLETE_LIGHT 0x01
#define IMPELLOR             0x02
#define LID_UNLOCK           0x03
#define WATER_IN             0x04
#define DRAIN_CLOSED         0x05
#define SWITCH_DELAY         20

// Production port pin assignments
#define HEATER_ON  GPIOD_ODR.B11 = 0          //  GPIOD_ODR.B8
#define HEATER_OFF  GPIOD_ODR.B11 = 1
#define RELAY1_ON  GPIOD_ODR.B10 = TRUE;   //user relay1
#define RELAY1_OFF  GPIOD_ODR.B10 = FALSE;
#define RELAY2_ON  GPIOD_ODR.B11 = TRUE;   // user relay2
#define RELAY2_OFF  GPIOD_ODR.B11 = FALSE;
#define CPU_MON_ON  GPIOD_ODR.B1 = FALSE;
#define CPU_MON_OFF  GPIOD_ODR.B1 = TRUE;
#define FLOAT_SW  GPIOD_IDR.B4
//#define ELEMENT_CHECK_OFF (!InputOn(HEATER_MAIN_ON)) //GPIOD_IDR.B2   (!(InputOn(HEATER_MAIN_ON)))

#define CAPTION_SIZE 7

#define ERRLOG_LINE_LEN 50 // including trailing zero
#define ERRLOG_LINE_N 10   // NOTE - if change this, also need to change "10" in BPWasher_objects.h, search for ERRLOG_LINE_N
#define ERRLOG_TOTAL_LEN (ERRLOG_LINE_LEN * ERRLOG_LINE_N) // 500 bytes

#define MEMORY_ACTIVE             0x00000
#define TEMP_MEMORY               MEMORY_ACTIVE + 1
#define P_MEMORY                  TEMP_MEMORY + CAPTION_SIZE
#define I_MEMORY                  P_MEMORY + CAPTION_SIZE
#define D_MEMORY                  I_MEMORY + CAPTION_SIZE
#define OFFSET_MEMORY             D_MEMORY + CAPTION_SIZE
#define FACTOR_MEMORY             OFFSET_MEMORY + CAPTION_SIZE
#define CYCLE_COMPLETE            FACTOR_MEMORY + CAPTION_SIZE
#define CYCLE_FAILED              CYCLE_COMPLETE + CAPTION_SIZE
#define BUILD_MEMORY              CYCLE_FAILED + CAPTION_SIZE  // BUILD_MEMORY is a word so 2 bytes long
#define ERRLOG_MEMORY             BUILD_MEMORY + 2
#define FILL_MEMORY               ERRLOG_MEMORY + ERRLOG_TOTAL_LEN
#define DRAIN_MEMORY              FILL_MEMORY + CAPTION_SIZE
#define UNUSED_NEXT_MEMORY        DRAIN_MEMORY + CAPTION_SIZE

#define MAGIC_NUMBER 0xAA

// RTC Backup Register storage
#define RTC_STORAGE_NSTATE    RTC_BKP0R    // RTC backup register 0

//Port Expansion
#define GPPUB  0x0DFF
#define IPOLB 0x03FF
#define GBINTENB 0x05FF
#define IOCONB 0x0902

//States
#define WAITING_START      0
#define PREWASH_FILL       1
#define PREWASH_HEAT       2
#define PREWASH_DRAIN      3
#define PRECOOL_INIT       4
#define PRECOOL_FILL       5
#define PRECOOL_WAIT       6
#define PRECOOL_DRAIN      7
#define SANTIZ_INIT        8
#define SANTIZ_FILL        9
#define SANTIZ_FILL_EXTRA  10
#define SANTIZ_HEAT        11
#define SANTIZ_WAIT        12
#define SANTIZ_DRAIN       13
#define POSTCOOL_INIT      14
#define POSTCOOL_FILL      15
#define POSTCOOL_FILL_EXT  16
#define POSTCOOL_WAIT      17
#define WAIT_UNLOCK        18
#define ERR_PRE_HOLDING_STATE    100
#define ERR_HOLDING_STATE        101

#define CAL_START                200
#define CAL_FILL                 201
#define CAL_HOLD_TEMP_LOW        202
#define CAL_ENTER_LOW_TEMP       203
#define CAL_HOLD_TEMP_HIGH       204
#define CAL_ENTER_HIGH_TEMP      205
#define CAL_STOP                 206

#define AUTO_RESTART FALSE
 /////////////////////////
// Events Code Declarations
void cal_confirm_buttonClick();
void cal_fact_buttonClick();
void cal_offset_buttonClick();
void cal_ret_buttonClick();
void D_buttonClick();
void I_buttonClick();
void Key_0Click();
void Key_1click();
void Key_2Click();
void Key_3Click();
void Key_4Click();
void Key_5Click();
void Key_6Click();
void Key_7Click();
void Key_8Click();
void Key_9Click();
void Key_clearClick();
void Key_dpClick();
void Key_enterClick();
void Key_return_buttonClick();
void Main_Screen_settingsClick();
void Main_Screen_startClick();
//void Main_Screen_sv_downClick();
//void Main_Screen_sv_upClick();
void PID_conf_buttonClick();
void PID_ret_buttonClick();
void pos_negClick();
void Prop_buttonClick();
void Settings_CALClick();
void Settings_ExitClick();
void Settings_PIDClick();
//void Settings_tempClick(); // no longer used - CM 10/07/17
void Settings_LogClick();
/////////////////////////
typedef struct params {
         float finit_value;
         float fvalue;
         float fValueMax;
         float fValueMin;
         TScreen *CallingDisplay;
         char *DisplayToUpdate;
         char  nDecimalPlaces;
        } stParams;

extern stParams *ValueToSet;
extern stParams fProportional;
extern stParams fIntegral;
extern stParams fDerivative;
extern stParams fCal_offset;
extern stParams fCal_factor;
extern stParams fPassword;
extern stParams fMeasuredHigh;
extern stParams fMeasuredLow;
extern stParams fExtraFillTime;
extern stParams fDrainTime;
extern float fSetTemp;
extern float fCyclesComplete;
extern float fCyclesFail;
extern char bGlobalTempAlarm;
extern char bAtTemp;
extern char bFlash;
extern char bTemp;
extern int nSec_count;
extern char bRefresh;
extern float ActualTemp;
extern char nState;
extern char CyclesComplete[7];
extern char CyclesFail[7];
extern char sErrorLog[];

float CalculatePowerDemand(void);
void ServiceHeater(void);
void button_up();
void button_down();
void set_and_format_value(stParams *StpValue, float fValue);
void Display_Keyboard(char *caption, stParams *StpValue);
void String_to_num(char nNumber); // was unsigned char, but return value is never used
void ret_prev_screen();
void enter_pressed();
void ResetKeypad();
void refresh_PV_display();
void heater_off_indicator();
void heater_on_indicator();
//InputUpDate
void GetInputStates();
void ConfigureInputs(void);
void SetOutput(char nOutputToDrive, char CallingChkBox);
void ClearAllOutputs();
char InputOn(char nInputIndex);

//Recipe
#define STATUS_NORMAL 0
#define STATUS_ERROR  1
void ErrorLog_NewError(char *errMsg, char bIncrementCount);
void Recipe_State();
void StatusHalfSecondsCount();
void ResetRecipe();
float GetCurrentTemp();
float GetDisplayTemp();
void UpDateMemory();