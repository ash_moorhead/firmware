#line 1 "C:/Software/BPWasher_Code/BPWasher_main.c"
#line 1 "c:/software/bpwasher_code/bpwasher_objects.h"
typedef enum {_taLeft, _taCenter, _taRight} TTextAlign;
typedef enum {_tavTop, _tavMiddle, _tavBottom} TTextAlignVertical;

typedef struct Screen TScreen;

typedef struct Button {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 char *Caption;
 TTextAlign TextAlign;
 TTextAlignVertical TextAlignVertical;
 const char *FontName;
 unsigned int Font_Color;
 char VerticalText;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TButton;

typedef struct Button_Round {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 char *Caption;
 TTextAlign TextAlign;
 TTextAlignVertical TextAlignVertical;
 const char *FontName;
 unsigned int Font_Color;
 char VerticalText;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char Corner_Radius;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TButton_Round;

typedef struct CButton_Round {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 const char *Caption;
 TTextAlign TextAlign;
 TTextAlignVertical TextAlignVertical;
 const char *FontName;
 unsigned int Font_Color;
 char VerticalText;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char Corner_Radius;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TCButton_Round;

typedef struct Label {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char *Caption;
 const char *FontName;
 unsigned int Font_Color;
 char VerticalText;
 char Visible;
 char Active;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TLabel;

typedef struct Image {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 const char *Picture_Name;
 char Visible;
 char Active;
 char Picture_Type;
 char Picture_Ratio;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TImage;

typedef const struct CImage {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 const char *Picture_Name;
 char Visible;
 char Active;
 char Picture_Type;
 char Picture_Ratio;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TCImage;

typedef struct Circle {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Radius;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TCircle;

typedef struct Box {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TBox;

typedef struct Line {
 TScreen* OwnerScreen;
 char Order;
 unsigned int First_Point_X;
 unsigned int First_Point_Y;
 unsigned int Second_Point_X;
 unsigned int Second_Point_Y;
 char Pen_Width;
 char Visible;
 unsigned int Color;
} TLine;

typedef struct CheckBox {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Checked;
 unsigned char Bit;
 char Transparent;
 char *Caption;
 TTextAlign TextAlign;
 const char *FontName;
 unsigned int Font_Color;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char Rounded;
 char Corner_Radius;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)(char Output, char Checked);
 void (*OnPressPtr)();

} TCheckBox;

struct Screen {
 unsigned int Color;
 unsigned int Width;
 unsigned int Height;
 unsigned int ObjectsCount;
 unsigned int ButtonsCount;
 TButton * const code *Buttons;
 unsigned int Buttons_RoundCount;
 TButton_Round * const code *Buttons_Round;
 unsigned int CButtons_RoundCount;
 TCButton_Round * const code *CButtons_Round;
 unsigned int LabelsCount;
 TLabel * const code *Labels;
 unsigned int ImagesCount;
 TImage * const code *Images;
 unsigned int CImagesCount;
 TCImage * const code *CImages;
 unsigned int CirclesCount;
 TCircle * const code *Circles;
 unsigned int BoxesCount;
 TBox * const code *Boxes;
 unsigned int LinesCount;
 TLine * const code *Lines;
 unsigned int CheckBoxesCount;
 TCheckBox * const code *CheckBoxes;
};

extern TScreen* CurrentScreen;

extern TScreen Keypad;
extern TButton_Round Key_7;
extern TButton_Round Key_8;
extern TButton_Round Key_9;
extern TButton_Round Key_clear;
extern TButton_Round Key_4;
extern TButton_Round Key_5;
extern TButton_Round Key_6;
extern TButton_Round Key_1;
extern TButton_Round Key_2;
extern TButton_Round Key_3;
extern TButton_Round Key_dp;
extern TButton_Round Key_0;
extern TButton_Round Key_enter;
extern TCButton_Round Key_return_button;
extern TButton kp_display;
extern TCImage Image8;
extern TButton_Round pos_neg;
extern TLabel negative_label;


extern TLabel lblKeyboardTitle;
extern TButton * const code Screen1_Buttons[1];
extern TButton_Round * const code Screen1_Buttons_Round[14];
extern TCButton_Round * const code Screen1_CButtons_Round[1];
extern TLabel * const code Screen1_Labels[1];
extern TCImage * const code Screen1_CImages[1];

extern TScreen _main;
extern TLabel Diagram3_Label1;
extern TCircle indicator;
extern TButton Screen_PV;
extern TLine Line2;
extern TButton_Round Main_Screen_settings;
extern TImage Main_screen_settings_image;
extern TLine Line1;
extern TButton_Round Main_Screen_start;
extern TImage start_stop;
extern TImage Diagram3_thermred;
extern TImage Diagram3_thermBlack;
extern TLabel Label1;
extern TLabel Label2;
extern TButton * const code Screen2_Buttons[1];
extern TButton_Round * const code Screen2_Buttons_Round[2];
extern TLabel * const code Screen2_Labels[3];
extern TImage * const code Screen2_Images[4];
extern TCircle * const code Screen2_Circles[1];
extern TLine * const code Screen2_Lines[2];

extern TScreen Settings;
extern TButton_Round Settings_temp;
extern TButton_Round Settings_PID;
extern TButton_Round Settings_CAL;
extern TButton_Round Settings_Exit;
extern TImage therm_settings;
extern TButton_Round ToolBox;
extern TImage imgToolBox;
extern TButton_Round Settings_drain;
extern TButton_Round * const code Screen3_Buttons_Round[6];

extern TScreen PID;
extern TCButton_Round Prop_button;
extern TButton_Round P_value;
extern TButton_Round I_value;
extern TButton_Round D_value;
extern TButton_Round I_button;
extern TButton_Round D_button;
extern TButton_Round PID_conf_button;
extern TButton_Round PID_ret_button;
extern TImage Image9;
extern TButton_Round * const code Screen4_Buttons_Round[7];
extern TCButton_Round * const code Screen4_CButtons_Round[1];
extern TImage * const code Screen4_Images[1];

extern TScreen Calibration;
extern TCButton_Round cal_offset_button;
extern TButton_Round cal_offset_value;
extern TButton_Round cal_factor_value;
extern TButton_Round cal_fact_button;
extern TButton_Round cal_confirm_button;
extern TButton_Round cal_ret_button;
extern TImage Image10;
extern TLabel cal_message_label;
extern TLabel cal_temp_label;
extern TButton_Round cal_start_button;
extern TButton_Round * const code Screen5_Buttons_Round[6];
extern TCButton_Round * const code Screen5_CButtons_Round[1];
extern TLabel * const code Screen5_Labels[2];
extern TImage * const code Screen5_Images[1];

extern TScreen Diags;
extern TLine Line3;
extern TLabel Lbl_inputs;
extern TLabel Lbl_outputs;
extern TCircle Circle1;
extern TLabel Lbl_water;
extern TCircle Circle2;
extern TLabel Label3;
extern TCircle Circle3;
extern TLabel Label4;
extern TCircle Circle4;
extern TLabel Label5;
extern TCircle Circle5;
extern TLabel Label6;
extern TCircle Circle6;
extern TLabel Label7;
extern TCheckBox CheckBox1;
extern TCheckBox CheckBox2;
extern TCheckBox CheckBox3;
extern TCheckBox CheckBox4;
extern TCheckBox CheckBox5;
extern TCheckBox CheckBox6;
extern TButton Button1;
extern TButton * const code Screen6_Buttons[1];
extern TLabel * const code Screen6_Labels[8];
extern TCircle * const code Screen6_Circles[6];
extern TLine * const code Screen6_Lines[1];
extern TCheckBox * const code Screen6_CheckBoxes[6];

extern TScreen SplashLand;
extern TImage Image1;
extern TImage Image2;
extern TBox Box1;
extern TBox Box2;
extern TLabel Label8;
extern TLabel * const code Screen7_Labels[1];
extern TImage * const code Screen7_Images[2];
extern TBox * const code Screen7_Boxes[2];

extern TScreen ErrorLog;
extern TLabel Log_Labels[10];
extern TLabel Log_Title;
extern TButton_Round ErrorLog_OK;
extern TButton_Round * const code Screen8_Buttons_Round[1];
extern TLabel * const code Screen8_Labels[];

extern TScreen Config;
extern TLabel config_fill_value_label;
extern TLabel config_drain_value_label;
extern TButton_Round config_fill_button;
extern TButton_Round config_drain_button;
extern TButton_Round config_confirm_button;
extern TButton_Round config_ret_button;
extern TImage Image3;
extern TButton_Round * const code Screen9_Buttons_Round[4];
extern TLabel * const code Screen9_Labels[2];
extern TImage * const code Screen9_Images[1];



void cal_confirm_buttonClick();
void cal_fact_buttonClick();
void cal_offset_buttonClick();
void CalibrationScreenVisible(char visibility);
void cal_ret_buttonClick();
void cal_start_buttonClick();
void config_ret_buttonClick();
void D_buttonClick();
void ErrorLog_OKClick();
void I_buttonClick();
void Key_0Click();
void Key_1click();
void Key_2Click();
void Key_3Click();
void Key_4Click();
void Key_5Click();
void Key_6Click();
void Key_7Click();
void Key_8Click();
void Key_9Click();
void Key_clearClick();
void Key_dpClick();
void Key_enterClick();
void Key_return_buttonClick();
void Main_Screen_settingsClick();
void Main_Screen_startClick();
void Main_Screen_sv_downClick();
void Main_Screen_sv_upClick();
void PID_conf_buttonClick();
void PID_ret_buttonClick();
void pos_negClick();
void Prop_buttonClick();
void Settings_CALClick();
void Settings_ExitClick();
void Settings_PIDClick();

void Settings_LogClick();
void Diag_Clicked();
void Exit_Pressed();
void config_fill_buttonClick();
void config_drain_buttonClick();
void Config_Clicked();
void config_confirm_buttonClick();




extern char Key_7_Caption[];
extern char Key_8_Caption[];
extern char Key_9_Caption[];
extern char Key_clear_Caption[];
extern char Key_4_Caption[];
extern char Key_5_Caption[];
extern char Key_6_Caption[];
extern char Key_1_Caption[];
extern char Key_2_Caption[];
extern char Key_3_Caption[];
extern char Key_dp_Caption[];
extern char Key_0_Caption[];
extern char Key_enter_Caption[];
extern const char Key_return_button_Caption[];
extern char kp_display_Caption[];
extern const char Image8_Caption[];
extern char pos_neg_Caption[];


extern char Diagram3_Label1_Caption[];
extern char indicator_Caption[];
extern char Screen_PV_Caption[];
extern char Line2_Caption[];
extern char Main_Screen_settings_Caption[];
extern char Main_screen_settings_image_Caption[];
extern char Line1_Caption[];
extern char Main_Screen_start_Caption[];
extern char start_stop_Caption[];
extern char Diagram3_thermred_Caption[];
extern char Diagram3_thermBlack_Caption[];
extern char Label1_Caption[];
extern char Settings_temp_Caption[];
extern char Settings_PID_Caption[];
extern char Settings_CAL_Caption[];
extern char Settings_Exit_Caption[];
extern char therm_settings_Caption[];
extern char ToolBox_Caption[];
extern char imgToolBox_Caption[];
extern const char Prop_button_Caption[];
extern char P_value_Caption[];
extern char I_value_Caption[];
extern char D_value_Caption[];
extern char I_button_Caption[];
extern char D_button_Caption[];
extern char PID_conf_button_Caption[];
extern char PID_ret_button_Caption[];
extern char Image9_Caption[];
extern const char cal_offset_button_Caption[];
extern char cal_offset_value_Caption[];
extern char cal_factor_value_Caption[];
extern char cal_fact_button_Caption[];
extern char cal_confirm_button_Caption[];
extern char cal_ret_button_Caption[];
extern char cal_message_label_Caption[];
extern char cal_temp_label_Caption[];
extern char Image10_Caption[];
extern char cal_start_button_Caption[];
extern char cal_start_button_CaptionStop[];
extern char Line3_Caption[];
extern char Lbl_inputs_Caption[];
extern char Lbl_outputs_Caption[];
extern char Circle1_Caption[];
extern char Lbl_water_Caption[];
extern char Circle2_Caption[];
extern char Label3_Caption[];
extern char Circle3_Caption[];
extern char Label4_Caption[];
extern char Circle4_Caption[];
extern char Label5_Caption[];
extern char Circle5_Caption[];
extern char Label6_Caption[];
extern char Circle6_Caption[];
extern char Label7_Caption[];
extern char CheckBox1_Caption[];
extern char CheckBox2_Caption[];
extern char CheckBox3_Caption[];
extern char CheckBox4_Caption[];
extern char CheckBox5_Caption[];
extern char CheckBox6_Caption[];
extern char Image1_Caption[];
extern char Image2_Caption[];
extern char Box1_Caption[];
extern char Box2_Caption[];
extern char Label8_Caption[];
extern char Button1_Caption[];
extern char Log_Title_Caption[];
extern char ErrorLog_OK_Caption[];
extern char config_drain_button_Caption[];
extern char config_fill_button_Caption[];
extern char config_drain_value_label_Caption[];
extern char config_fill_value_label_Caption[];
extern char config_confirm_button_Caption[];
extern char config_ret_button_Caption[];
extern char Image3_Caption[];


void DrawScreen(TScreen *aScreen);
void DrawButton(TButton *aButton);
void DrawRoundButton(TButton_Round *Around_button);
void DrawCRoundButton(TCButton_Round *ACround_button);
void DrawLabel(TLabel *ALabel);
void DrawImage(TImage *AImage);
void DrawCImage(TCImage *ACimage);
void DrawCircle(TCircle *ACircle);
void DrawBox(TBox *ABox);
void DrawLine(TLine *Aline);
void DrawCheckBox(TCheckBox *ACheckBox);
void Check_TP();
void Start_TP();
void Process_TP_Press(unsigned int X, unsigned int Y);
void Process_TP_Up(unsigned int X, unsigned int Y);
void Process_TP_Down(unsigned int X, unsigned int Y);
#line 1 "c:/software/bpwasher_code/sf_driver.h"
#line 27 "c:/software/bpwasher_code/sf_driver.h"
extern sfr sbit CS_Serial_Flash_bit;


static const unsigned short _SERIAL_FLASH_CMD_RDID = 0x9F;
static const unsigned short _SERIAL_FLASH_CMD_READ = 0x03;
static const unsigned short _SERIAL_FLASH_CMD_WRITE = 0x02;
static const unsigned short _SERIAL_FLASH_CMD_WREN = 0x06;
static const unsigned short _SERIAL_FLASH_CMD_RDSR = 0x05;
static const unsigned short _SERIAL_FLASH_CMD_ERASE = 0xC7;
static const unsigned short _SERIAL_FLASH_CMD_EWSR = 0x06;
static const unsigned short _SERIAL_FLASH_CMD_WRSR = 0x01;
static const unsigned short _SERIAL_FLASH_CMD_SER = 0xD8;


void SerialFlash_init();
void SerialFlash_WriteEnable();
unsigned char SerialFlash_IsWriteBusy();
void SerialFlash_WriteByte(unsigned char _data, unsigned long address);
void SerialFlash_WriteWord(unsigned int _data, unsigned long address);
unsigned char SerialFlash_ReadID(void);
unsigned char SerialFlash_ReadByte(unsigned long address);
unsigned int SerialFlash_ReadWord(unsigned long address);
unsigned char SerialFlash_WriteArray(unsigned long address, unsigned char* pData, unsigned int nCount);
void SerialFlash_ReadArray(unsigned long address, unsigned char* pData, unsigned int nCount);
void SerialFlash_ChipErase(void);
void SerialFlash_ResetWriteProtection();
void SerialFlash_SectorErase(unsigned long address);
#line 1 "c:/software/bpwasher_code/constants.h"
#line 126 "c:/software/bpwasher_code/constants.h"
void cal_confirm_buttonClick();
void cal_fact_buttonClick();
void cal_offset_buttonClick();
void cal_ret_buttonClick();
void D_buttonClick();
void I_buttonClick();
void Key_0Click();
void Key_1click();
void Key_2Click();
void Key_3Click();
void Key_4Click();
void Key_5Click();
void Key_6Click();
void Key_7Click();
void Key_8Click();
void Key_9Click();
void Key_clearClick();
void Key_dpClick();
void Key_enterClick();
void Key_return_buttonClick();
void Main_Screen_settingsClick();
void Main_Screen_startClick();


void PID_conf_buttonClick();
void PID_ret_buttonClick();
void pos_negClick();
void Prop_buttonClick();
void Settings_CALClick();
void Settings_ExitClick();
void Settings_PIDClick();

void Settings_LogClick();

typedef struct params {
 float finit_value;
 float fvalue;
 float fValueMax;
 float fValueMin;
 TScreen *CallingDisplay;
 char *DisplayToUpdate;
 char nDecimalPlaces;
 } stParams;

extern stParams *ValueToSet;
extern stParams fProportional;
extern stParams fIntegral;
extern stParams fDerivative;
extern stParams fCal_offset;
extern stParams fCal_factor;
extern stParams fPassword;
extern stParams fMeasuredHigh;
extern stParams fMeasuredLow;
extern stParams fExtraFillTime;
extern stParams fDrainTime;
extern float fSetTemp;
extern float fCyclesComplete;
extern float fCyclesFail;
extern char bGlobalTempAlarm;
extern char bAtTemp;
extern char bFlash;
extern char bTemp;
extern int nSec_count;
extern char bRefresh;
extern float ActualTemp;

extern char nState;
extern char CyclesComplete[7];
extern char CyclesFail[7];
extern char sErrorLog[];






void ServiceHeater(void);
void button_up();
void button_down();
void set_and_format_value(stParams *StpValue, float fValue);
void Display_Keyboard(char *caption, stParams *StpValue);
void String_to_num(char nNumber);
void ret_prev_screen();
void enter_pressed();
void ResetKeypad();
void refresh_PV_display();
void heater_off_indicator();
void heater_on_indicator();

void GetInputStates();
void ConfigureInputs(void);
void SetOutput(char nOutputToDrive, char CallingChkBox);
void ClearAllOutputs();
char InputOn(char nInputIndex);




void ErrorLog_NewError(char *errMsg, char bIncrementCount);
void Recipe_State();
void StatusHalfSecondsCount();
void ResetRecipe();
float GetCurrentTemp();
float GetDisplayTemp();
void UpDateMemory();
#line 106 "C:/Software/BPWasher_Code/BPWasher_main.c"
stParams fProportional;
stParams fIntegral;
stParams fDerivative;
stParams fCal_offset;
stParams fCal_factor;
stParams fPassword;
stParams fDrainTime;
stParams fExtraFillTime;

float fSetTemp;
float fCyclesComplete;
float fCyclesFail;

unsigned char KickDog =  1 ;

char sErrorLog[ ( 50  * 10 ) ];

char CyclesComplete[7] = "0";
char CyclesFail[7] = "0";

unsigned char nMemoryActive = 0, SerialFlashID = 0;

void UpDateMemory()
 {
 unsigned char nChar =  0xAA ;
 unsigned int build =  (atoi( "1" )*1000 + atoi( "6" )) ;
 SerialFlash_SectorErase(0x00000);
 SerialFlash_WriteByte(nChar,  0x00000 );


 SerialFlash_WriteArray( 0x00000  + 1  + 7 , &P_value_Caption,  7 );
 SerialFlash_WriteArray( 0x00000  + 1  + 7  + 7 , &I_value_Caption,  7 );
 SerialFlash_WriteArray( 0x00000  + 1  + 7  + 7  + 7 , &D_value_Caption,  7 );
 SerialFlash_WriteArray( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7 , &cal_factor_value_Caption,  7 );
 SerialFlash_WriteArray( 0x00000  + 1  + 7  + 7  + 7  + 7 , &cal_offset_value_Caption,  7 );
 SerialFlash_WriteArray( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7  + 7 , &CyclesComplete,  7 );
 SerialFlash_WriteArray( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7  + 7  + 7 , &CyclesFail,  7 );
 SerialFlash_WriteArray( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 2  + ( 50  * 10 ) , &config_fill_value_label_Caption,  7 );
 SerialFlash_WriteArray( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 2  + ( 50  * 10 )  + 7 , &config_drain_value_label_Caption,  7 );

 SerialFlash_WriteWord(build,  0x00000  + 1  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 7 );
 SerialFlash_WriteArray( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 2 , &sErrorLog,  ( 50  * 10 ) );
 }

void init_variables()
 {
 unsigned int stored_memory_build;

 SerialFlashID = SerialFlash_ReadID();
 nMemoryActive = SerialFlash_ReadByte( 0x00000 );
 if ( nMemoryActive ==  0xAA )
 {

 SerialFlash_ReadArray( 0x00000  + 1  + 7 , &P_value_Caption,  7 );
 SerialFlash_ReadArray( 0x00000  + 1  + 7  + 7 , &I_value_Caption,  7 );
 SerialFlash_ReadArray( 0x00000  + 1  + 7  + 7  + 7 , &D_value_Caption,  7 );
 SerialFlash_ReadArray( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7 , &cal_factor_value_Caption,  7 );
 SerialFlash_ReadArray( 0x00000  + 1  + 7  + 7  + 7  + 7 , &cal_offset_value_Caption,  7 );
 SerialFlash_ReadArray( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7  + 7 , &CyclesComplete,  7 );
 SerialFlash_ReadArray( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7  + 7  + 7 , &CyclesFail,  7 );
 stored_memory_build = SerialFlash_ReadWord( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 7 );



 }
 else
 stored_memory_build = 0;

 fSetTemp = 85;

 fProportional.fvalue = atof(P_value_Caption);
 fProportional.fValueMax = 500;
 fProportional.fValueMin = 1;
 fProportional.CallingDisplay = &PID;
 fProportional.DisplayToUpdate = P_value_Caption;
 fProportional.nDecimalPlaces = 1;

 fIntegral.fvalue = atof(I_value_Caption);
 fIntegral.fValueMax = 500;
 fIntegral.fValueMin = 0;
 fIntegral.CallingDisplay = &PID;
 fIntegral.DisplayToUpdate = I_value_Caption;
 fIntegral.nDecimalPlaces = 1;

 fDerivative.fvalue = atof(D_value_Caption);
 fDerivative.fValueMax = 500;
 fDerivative.fValueMin = 0;
 fDerivative.CallingDisplay = &PID;
 fDerivative.DisplayToUpdate = D_value_Caption;
 fDerivative.nDecimalPlaces = 1;

 fCal_offset.fvalue = atof(cal_offset_value_Caption);
 fCal_offset.fValueMax = 100;
 fCal_offset.fValueMin = -100;
 fCal_offset.CallingDisplay = &Calibration;
 fCal_offset.DisplayToUpdate = cal_offset_value_Caption;
 fCal_offset.nDecimalPlaces = 3;

 fCal_factor.fvalue = atof(cal_factor_value_Caption);
 fCal_factor.fValueMax = 100;
 fCal_factor.fValueMin = -100;
 fCal_factor.CallingDisplay = &Calibration;
 fCal_factor.DisplayToUpdate = cal_factor_value_Caption;
 fCal_factor.nDecimalPlaces = 3;

 fCyclesComplete = atof(CyclesComplete);
 fCyclesFail = atof(CyclesFail);

 fPassword.fvalue = 0;
 fPassword.fValueMax = 99999;
 fPassword.fValueMin = 0;
 fPassword.CallingDisplay = &_main;
 fPassword.DisplayToUpdate = 0;
 fPassword.nDecimalPlaces = 0;


 if ( nMemoryActive ==  0xAA  && stored_memory_build >= 1 )
 {
 SerialFlash_ReadArray( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 2 , &sErrorLog,  ( 50  * 10 ) );
 }
 else
 {

 memset(sErrorLog, 0,  ( 50  * 10 ) );
 }


 if ( nMemoryActive ==  0xAA  && stored_memory_build >= 1004 )
 {
 SerialFlash_ReadArray( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 2  + ( 50  * 10 ) , &config_fill_value_label_Caption,  7 );
 SerialFlash_ReadArray( 0x00000  + 1  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 7  + 2  + ( 50  * 10 )  + 7 , &config_drain_value_label_Caption,  7 );
 }
 else
 {

 sprintf(config_fill_value_label_Caption, "5s");
 sprintf(config_drain_value_label_Caption, "60s");
 }

 fExtraFillTime.fvalue = atof(config_fill_value_label_Caption);
 fExtraFillTime.fValueMax = 999;
 fExtraFillTime.fValueMin = 0;
 fExtraFillTime.CallingDisplay = &Config;
 fExtraFillTime.DisplayToUpdate = config_fill_value_label_Caption;
 fExtraFillTime.nDecimalPlaces = 0;

 fDrainTime.fvalue = atof(config_drain_value_label_Caption);
 fDrainTime.fValueMax = 999;
 fDrainTime.fValueMin = 0;
 fDrainTime.CallingDisplay = &Config;
 fDrainTime.DisplayToUpdate = config_drain_value_label_Caption;
 fDrainTime.nDecimalPlaces = 0;


 if (stored_memory_build !=  (atoi( "1" )*1000 + atoi( "6" ))  )
 {
 ErrorLog_NewError("Version "  "1"  "." "6"  " installed",  0 );
 }

 fSetTemp = 0;

 }
#line 271 "C:/Software/BPWasher_Code/BPWasher_main.c"
sbit CS_Serial_Flash_bit at GPIOD_ODR.B7;

char cSF_test_status;
#line 281 "C:/Software/BPWasher_Code/BPWasher_main.c"
void SF_Start()
 {
 SPI3_Init_Advanced(_SPI_FPCLK_DIV2, _SPI_MASTER | _SPI_8_BIT |
 _SPI_CLK_IDLE_LOW | _SPI_FIRST_CLK_EDGE_TRANSITION |
 _SPI_MSB_FIRST | _SPI_SS_DISABLE | _SPI_SSM_ENABLE | _SPI_SSI_1,
 &_GPIO_MODULE_SPI3_PC10_11_12);




 SerialFlash_init();
 SerialFlash_WriteEnable();
 Delay_ms(500);
 }

void init_Ports()
 {

 GPIO_Config(&GPIOD_BASE, _GPIO_PINMASK_7, _GPIO_CFG_DIGITAL_OUTPUT);
 GPIO_Digital_Output(&GPIOB_BASE, _GPIO_PINMASK_11);
 GPIO_Digital_Output(&GPIOB_BASE, _GPIO_PINMASK_12);
 GPIO_Digital_Output(&GPIOB_BASE, _GPIO_PINMASK_13);
 GPIO_Digital_Output(&GPIOB_BASE, _GPIO_PINMASK_14);
 GPIO_Digital_Input(&GPIOB_BASE, _GPIO_PINMASK_15);
 GPIO_Digital_Input(&GPIOD_BASE, _GPIO_PINMASK_0);
  GPIOD_ODR.B11 = 1 ;
 SF_Start();
 }




void flash_status()
 {


 indicator.Color = (indicator.Color== CL_BLACK) ? (fSetTemp>0 ? CL_LIME : 0xC618) : CL_BLACK ;
 DrawCircle(&indicator);
 }






void InitTimer2()
 {
 RCC_APB1ENR.TIM2EN = 1;
 TIM2_CR1.CEN = 0;
 TIM2_PSC = 1;
 TIM2_ARR = 34760;
 NVIC_IntEnable(IVT_INT_TIM2);
 TIM2_DIER.UIE = 1;
 TIM2_CR1.CEN = 1;
 }

int nADC_count = 0;
int nSec_count = 0;
int nRefresh_count = 0;
char bFlash =  0 ;
char bADC =  0 ;
char bRefresh =  0 ;

void Timer2_interrupt() iv IVT_INT_TIM2
 {
 TIM2_SR.UIF = 0;

 nADC_count++;
 nSec_count++;
 nRefresh_count++;

 if (20 <= nADC_count)
 {
 bADC =  1 ;
 nADC_count = 0;
 }

 if (100 <= nRefresh_count)
 {
 bRefresh =  1 ;
 nRefresh_count = 0;
 }


 if (500 <= nSec_count)
 {
 bFLASH =  1 ;
 nSec_count = 0;
 }
 }



void main() {
 char bCPU_Mon =  0 ;
 unsigned char nTest[2];
 unsigned int stored_state;


 PWREN_bit = 1;

 Start_TP();
 init_Ports();
 init_variables();
 InitTimer2();
 GPIO_Digital_Output(&GPIOD_BASE, _GPIO_PINMASK_10 | _GPIO_PINMASK_11 | _GPIO_PINMASK_1);
  GPIOD_ODR.B11 = 1 ;
 ADC_Set_Input_Channel(_ADC_CHANNEL_1);


 IWDG_KR = 0x5555;
 IWDG_PR = 0x0004;
 IWDG_KR = 0x5555;
 IWDG_RLR = 0xFFFF;
 IWDG_KR = 0xCCCC;

 I2C1_Init();
 ConfigureInputs();
 nTest[0] =0x00;
 nTest[1] =0x00;
 if ( 0 )
 {
 I2C1_Start();
 I2C1_Write( 0x20 , nTest, 2, END_MODE_STOP);
 ClearAllOutputs();
 }

 ResetRecipe();
 Delay_ms(2000);
 DrawScreen(&_main);



 if(WDGRSTF_bit){


 char msgWatchdogReset[100];
 stored_state = RTC_BKP0R;
 sprintf(msgWatchdogReset, "Watchdog Reset Occured, %d, %.0f", stored_state, getCurrentTemp());


 if(stored_state != 0){
 nState =  100 ;
 }

 ErrorLog_NewError(&msgWatchdogReset,  1 );

 RMVF_bit = 1;
 }

 while (1) {

 if(KickDog){
 IWDG_KR = 0xAAAA;
 }

 Check_TP();

 if ( 0 )
 GetInputStates();

 Recipe_State();

 if (bADC)
 {
 if (!bGlobalTempAlarm)
 {
 if (bCPU_Mon)
  GPIOD_ODR.B1 = 1 ; 
 else
  GPIOD_ODR.B1 = 0 ; 

 bCPU_Mon = !bCPU_Mon;
 }

 ServiceHeater();
 bADC =  0 ;
 }

 if (bRefresh)
 {
 refresh_PV_display();
 bRefresh =  0 ;
 }

 if (bFlash)
 {
 StatusHalfSecondsCount();
 flash_status();
 bFLASH =  0 ;
 }
 }
}
