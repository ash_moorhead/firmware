_heater_on_indicator:
;BPWasher_events_code.c,13 :: 		void heater_on_indicator()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,15 :: 		Diagram3_thermBlack.Visible = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_thermBlack+20)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+20)
STRB	R1, [R0, #0]
;BPWasher_events_code.c,16 :: 		DrawImage(&Diagram3_thermBlack);
MOVW	R0, #lo_addr(_Diagram3_thermBlack+0)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+0)
BL	_DrawImage+0
;BPWasher_events_code.c,17 :: 		Diagram3_thermred.Visible = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diagram3_thermred+20)
MOVT	R0, #hi_addr(_Diagram3_thermred+20)
STRB	R1, [R0, #0]
;BPWasher_events_code.c,18 :: 		DrawImage(&Diagram3_thermred);
MOVW	R0, #lo_addr(_Diagram3_thermred+0)
MOVT	R0, #hi_addr(_Diagram3_thermred+0)
BL	_DrawImage+0
;BPWasher_events_code.c,19 :: 		}
L_end_heater_on_indicator:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _heater_on_indicator
_heater_off_indicator:
;BPWasher_events_code.c,21 :: 		void heater_off_indicator()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,23 :: 		Diagram3_thermred.Visible = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_thermred+20)
MOVT	R0, #hi_addr(_Diagram3_thermred+20)
STRB	R1, [R0, #0]
;BPWasher_events_code.c,24 :: 		DrawImage(&Diagram3_thermred);
MOVW	R0, #lo_addr(_Diagram3_thermred+0)
MOVT	R0, #hi_addr(_Diagram3_thermred+0)
BL	_DrawImage+0
;BPWasher_events_code.c,25 :: 		Diagram3_thermBlack.Visible = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diagram3_thermBlack+20)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+20)
STRB	R1, [R0, #0]
;BPWasher_events_code.c,26 :: 		DrawImage(&Diagram3_thermBlack);
MOVW	R0, #lo_addr(_Diagram3_thermBlack+0)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+0)
BL	_DrawImage+0
;BPWasher_events_code.c,27 :: 		}
L_end_heater_off_indicator:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _heater_off_indicator
_Key_0Click:
;BPWasher_events_code.c,36 :: 		void Key_0Click()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,38 :: 		String_to_num('0');
MOVS	R0, #48
BL	_String_to_num+0
;BPWasher_events_code.c,39 :: 		}
L_end_Key_0Click:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_0Click
_Key_dpClick:
;BPWasher_events_code.c,41 :: 		void Key_dpClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,43 :: 		String_to_num('.');
MOVS	R0, #46
BL	_String_to_num+0
;BPWasher_events_code.c,44 :: 		}
L_end_Key_dpClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_dpClick
_Key_1click:
;BPWasher_events_code.c,46 :: 		void Key_1click()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,48 :: 		String_to_num('1');
MOVS	R0, #49
BL	_String_to_num+0
;BPWasher_events_code.c,49 :: 		}
L_end_Key_1click:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_1click
_Key_2Click:
;BPWasher_events_code.c,51 :: 		void Key_2Click()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,53 :: 		String_to_num('2');
MOVS	R0, #50
BL	_String_to_num+0
;BPWasher_events_code.c,54 :: 		}
L_end_Key_2Click:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_2Click
_Key_3Click:
;BPWasher_events_code.c,56 :: 		void Key_3Click()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,58 :: 		String_to_num('3');
MOVS	R0, #51
BL	_String_to_num+0
;BPWasher_events_code.c,59 :: 		}
L_end_Key_3Click:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_3Click
_Key_4Click:
;BPWasher_events_code.c,61 :: 		void Key_4Click()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,63 :: 		String_to_num('4');
MOVS	R0, #52
BL	_String_to_num+0
;BPWasher_events_code.c,64 :: 		}
L_end_Key_4Click:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_4Click
_Key_5Click:
;BPWasher_events_code.c,66 :: 		void Key_5Click() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,67 :: 		String_to_num('5');
MOVS	R0, #53
BL	_String_to_num+0
;BPWasher_events_code.c,68 :: 		}
L_end_Key_5Click:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_5Click
_Key_6Click:
;BPWasher_events_code.c,70 :: 		void Key_6Click()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,72 :: 		String_to_num('6');
MOVS	R0, #54
BL	_String_to_num+0
;BPWasher_events_code.c,73 :: 		}
L_end_Key_6Click:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_6Click
_Key_7Click:
;BPWasher_events_code.c,75 :: 		void Key_7Click()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,77 :: 		String_to_num('7');
MOVS	R0, #55
BL	_String_to_num+0
;BPWasher_events_code.c,78 :: 		}
L_end_Key_7Click:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_7Click
_Key_8Click:
;BPWasher_events_code.c,80 :: 		void Key_8Click()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,82 :: 		String_to_num('8');
MOVS	R0, #56
BL	_String_to_num+0
;BPWasher_events_code.c,83 :: 		}
L_end_Key_8Click:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_8Click
_Key_9Click:
;BPWasher_events_code.c,85 :: 		void Key_9Click()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,87 :: 		String_to_num('9');
MOVS	R0, #57
BL	_String_to_num+0
;BPWasher_events_code.c,88 :: 		}
L_end_Key_9Click:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_9Click
_pos_negClick:
;BPWasher_events_code.c,90 :: 		void pos_negClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,92 :: 		String_to_num('-');
MOVS	R0, #45
BL	_String_to_num+0
;BPWasher_events_code.c,93 :: 		}
L_end_pos_negClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _pos_negClick
_Key_clearClick:
;BPWasher_events_code.c,95 :: 		void Key_clearClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,97 :: 		ResetKeypad();
BL	_ResetKeypad+0
;BPWasher_events_code.c,98 :: 		}
L_end_Key_clearClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_clearClick
_Key_enterClick:
;BPWasher_events_code.c,100 :: 		void Key_enterClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,102 :: 		enter_pressed();
BL	_enter_pressed+0
;BPWasher_events_code.c,103 :: 		}
L_end_Key_enterClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Key_enterClick
_Main_Screen_settingsClick:
;BPWasher_events_code.c,109 :: 		void Main_Screen_settingsClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,111 :: 		Display_Keyboard("Enter password", &fPassword);
MOVW	R0, #lo_addr(?lstr1_BPWasher_events_code+0)
MOVT	R0, #hi_addr(?lstr1_BPWasher_events_code+0)
MOVW	R1, #lo_addr(_fPassword+0)
MOVT	R1, #hi_addr(_fPassword+0)
BL	_Display_Keyboard+0
;BPWasher_events_code.c,112 :: 		}
L_end_Main_Screen_settingsClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Main_Screen_settingsClick
_Main_Screen_startClick:
;BPWasher_events_code.c,115 :: 		void Main_Screen_startClick()    // actually this is the STOP button - CM 13/07/17
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,117 :: 		fSetTemp = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;BPWasher_events_code.c,118 :: 		ClearAllOutputs();
BL	_ClearAllOutputs+0
;BPWasher_events_code.c,119 :: 		ResetRecipe();
BL	_ResetRecipe+0
;BPWasher_events_code.c,120 :: 		HEATER_OFF;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOD_ODR+0)
MOVT	R0, #hi_addr(GPIOD_ODR+0)
STR	R1, [R0, #0]
;BPWasher_events_code.c,121 :: 		}
L_end_Main_Screen_startClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Main_Screen_startClick
_Diag_Clicked:
;BPWasher_events_code.c,127 :: 		void Diag_Clicked()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,129 :: 		DrawScreen(&Diags);
MOVW	R0, #lo_addr(_Diags+0)
MOVT	R0, #hi_addr(_Diags+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,130 :: 		}
L_end_Diag_Clicked:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Diag_Clicked
_Config_Clicked:
;BPWasher_events_code.c,132 :: 		void Config_Clicked()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,134 :: 		DrawScreen(&Config);
MOVW	R0, #lo_addr(_Config+0)
MOVT	R0, #hi_addr(_Config+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,135 :: 		fDrainTimeOld = fDrainTime.fValue;
MOVW	R0, #lo_addr(_fDrainTime+4)
MOVT	R0, #hi_addr(_fDrainTime+4)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fDrainTimeOld+0)
MOVT	R0, #hi_addr(_fDrainTimeOld+0)
VSTR	#1, S0, [R0, #0]
;BPWasher_events_code.c,136 :: 		fExtraFillTimeOld = fExtraFillTime.fValue;
MOVW	R0, #lo_addr(_fExtraFillTime+4)
MOVT	R0, #hi_addr(_fExtraFillTime+4)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fExtraFillTimeOld+0)
MOVT	R0, #hi_addr(_fExtraFillTimeOld+0)
VSTR	#1, S0, [R0, #0]
;BPWasher_events_code.c,137 :: 		}
L_end_Config_Clicked:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Config_Clicked
_Settings_LogClick:
;BPWasher_events_code.c,139 :: 		void Settings_LogClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,141 :: 		sprintf(Log_Title_Caption, "%.0f / %.0f = Complete / Faults", fCyclesComplete, fCyclesFail);
MOVW	R0, #lo_addr(_fCyclesFail+0)
MOVT	R0, #hi_addr(_fCyclesFail+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_fCyclesComplete+0)
MOVT	R0, #hi_addr(_fCyclesComplete+0)
VLDR	#1, S0, [R0, #0]
MOVW	R1, #lo_addr(?lstr_2_BPWasher_events_code+0)
MOVT	R1, #hi_addr(?lstr_2_BPWasher_events_code+0)
MOVW	R0, #lo_addr(_Log_Title_Caption+0)
MOVT	R0, #hi_addr(_Log_Title_Caption+0)
VPUSH	#0, (S1)
VPUSH	#0, (S0)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #16
;BPWasher_events_code.c,142 :: 		DrawScreen(&ErrorLog);
MOVW	R0, #lo_addr(_ErrorLog+0)
MOVT	R0, #hi_addr(_ErrorLog+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,143 :: 		}
L_end_Settings_LogClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Settings_LogClick
_Settings_PIDClick:
;BPWasher_events_code.c,144 :: 		void Settings_PIDClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,146 :: 		DrawScreen(&PID);
MOVW	R0, #lo_addr(_PID+0)
MOVT	R0, #hi_addr(_PID+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,148 :: 		fProportionalOld = fProportional.fValue;
MOVW	R0, #lo_addr(_fProportional+4)
MOVT	R0, #hi_addr(_fProportional+4)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fProportionalOld+0)
MOVT	R0, #hi_addr(_fProportionalOld+0)
VSTR	#1, S0, [R0, #0]
;BPWasher_events_code.c,149 :: 		fIntegralOld = fIntegral.fValue;
MOVW	R0, #lo_addr(_fIntegral+4)
MOVT	R0, #hi_addr(_fIntegral+4)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fIntegralOld+0)
MOVT	R0, #hi_addr(_fIntegralOld+0)
VSTR	#1, S0, [R0, #0]
;BPWasher_events_code.c,150 :: 		fDerivativeOld = fDerivative.fValue;
MOVW	R0, #lo_addr(_fDerivative+4)
MOVT	R0, #hi_addr(_fDerivative+4)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fDerivativeOld+0)
MOVT	R0, #hi_addr(_fDerivativeOld+0)
VSTR	#1, S0, [R0, #0]
;BPWasher_events_code.c,151 :: 		}
L_end_Settings_PIDClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Settings_PIDClick
_Settings_CALClick:
;BPWasher_events_code.c,153 :: 		void Settings_CALClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,155 :: 		DrawScreen(&Calibration);
MOVW	R0, #lo_addr(_Calibration+0)
MOVT	R0, #hi_addr(_Calibration+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,157 :: 		fCal_offsetOld = fCal_offset.fValue;
MOVW	R0, #lo_addr(_fCal_offset+4)
MOVT	R0, #hi_addr(_fCal_offset+4)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fCal_offsetOld+0)
MOVT	R0, #hi_addr(_fCal_offsetOld+0)
VSTR	#1, S0, [R0, #0]
;BPWasher_events_code.c,158 :: 		fCal_factorOld = fCal_factor.fValue;
MOVW	R0, #lo_addr(_fCal_factor+4)
MOVT	R0, #hi_addr(_fCal_factor+4)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fCal_factorOld+0)
MOVT	R0, #hi_addr(_fCal_factorOld+0)
VSTR	#1, S0, [R0, #0]
;BPWasher_events_code.c,159 :: 		}
L_end_Settings_CALClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Settings_CALClick
_Settings_ExitClick:
;BPWasher_events_code.c,161 :: 		void Settings_ExitClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,163 :: 		DrawScreen(&_Main);
MOVW	R0, #lo_addr(__main+0)
MOVT	R0, #hi_addr(__main+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,164 :: 		}
L_end_Settings_ExitClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Settings_ExitClick
_Prop_buttonClick:
;BPWasher_events_code.c,170 :: 		void Prop_buttonClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,172 :: 		Display_Keyboard("Enter P value", &fProportional);
MOVW	R0, #lo_addr(?lstr3_BPWasher_events_code+0)
MOVT	R0, #hi_addr(?lstr3_BPWasher_events_code+0)
MOVW	R1, #lo_addr(_fProportional+0)
MOVT	R1, #hi_addr(_fProportional+0)
BL	_Display_Keyboard+0
;BPWasher_events_code.c,173 :: 		}
L_end_Prop_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Prop_buttonClick
_I_buttonClick:
;BPWasher_events_code.c,175 :: 		void I_buttonClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,177 :: 		Display_Keyboard("Enter I Value", &fIntegral);
MOVW	R0, #lo_addr(?lstr4_BPWasher_events_code+0)
MOVT	R0, #hi_addr(?lstr4_BPWasher_events_code+0)
MOVW	R1, #lo_addr(_fIntegral+0)
MOVT	R1, #hi_addr(_fIntegral+0)
BL	_Display_Keyboard+0
;BPWasher_events_code.c,178 :: 		}
L_end_I_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _I_buttonClick
_D_buttonClick:
;BPWasher_events_code.c,180 :: 		void D_buttonClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,182 :: 		Display_Keyboard("Enter D Value", &fDerivative);
MOVW	R0, #lo_addr(?lstr5_BPWasher_events_code+0)
MOVT	R0, #hi_addr(?lstr5_BPWasher_events_code+0)
MOVW	R1, #lo_addr(_fDerivative+0)
MOVT	R1, #hi_addr(_fDerivative+0)
BL	_Display_Keyboard+0
;BPWasher_events_code.c,183 :: 		}
L_end_D_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _D_buttonClick
_PID_conf_buttonClick:
;BPWasher_events_code.c,185 :: 		void PID_conf_buttonClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,187 :: 		DrawScreen(&Settings);
MOVW	R0, #lo_addr(_Settings+0)
MOVT	R0, #hi_addr(_Settings+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,188 :: 		}
L_end_PID_conf_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _PID_conf_buttonClick
_PID_ret_buttonClick:
;BPWasher_events_code.c,190 :: 		void PID_ret_buttonClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,193 :: 		set_and_format_value(&fProportional, fProportionalOld);
MOVW	R0, #lo_addr(_fProportionalOld+0)
MOVT	R0, #hi_addr(_fProportionalOld+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fProportional+0)
MOVT	R0, #hi_addr(_fProportional+0)
BL	_set_and_format_value+0
;BPWasher_events_code.c,194 :: 		set_and_format_value(&fIntegral, fIntegralOld);
MOVW	R0, #lo_addr(_fIntegralOld+0)
MOVT	R0, #hi_addr(_fIntegralOld+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fIntegral+0)
MOVT	R0, #hi_addr(_fIntegral+0)
BL	_set_and_format_value+0
;BPWasher_events_code.c,195 :: 		set_and_format_value(&fDerivative, fDerivativeOld);
MOVW	R0, #lo_addr(_fDerivativeOld+0)
MOVT	R0, #hi_addr(_fDerivativeOld+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fDerivative+0)
MOVT	R0, #hi_addr(_fDerivative+0)
BL	_set_and_format_value+0
;BPWasher_events_code.c,196 :: 		DrawScreen(&PID);
MOVW	R0, #lo_addr(_PID+0)
MOVT	R0, #hi_addr(_PID+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,197 :: 		UpDateMemory();
BL	_UpDateMemory+0
;BPWasher_events_code.c,198 :: 		}
L_end_PID_ret_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _PID_ret_buttonClick
_cal_offset_buttonClick:
;BPWasher_events_code.c,203 :: 		void cal_offset_buttonClick() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,204 :: 		Display_Keyboard("Enter Cal Offset", &fCal_offset);
MOVW	R0, #lo_addr(?lstr6_BPWasher_events_code+0)
MOVT	R0, #hi_addr(?lstr6_BPWasher_events_code+0)
MOVW	R1, #lo_addr(_fCal_offset+0)
MOVT	R1, #hi_addr(_fCal_offset+0)
BL	_Display_Keyboard+0
;BPWasher_events_code.c,205 :: 		}
L_end_cal_offset_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _cal_offset_buttonClick
_cal_fact_buttonClick:
;BPWasher_events_code.c,207 :: 		void cal_fact_buttonClick() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,208 :: 		Display_Keyboard("Enter Cal Factor", &fCal_factor);
MOVW	R0, #lo_addr(?lstr7_BPWasher_events_code+0)
MOVT	R0, #hi_addr(?lstr7_BPWasher_events_code+0)
MOVW	R1, #lo_addr(_fCal_factor+0)
MOVT	R1, #hi_addr(_fCal_factor+0)
BL	_Display_Keyboard+0
;BPWasher_events_code.c,209 :: 		}
L_end_cal_fact_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _cal_fact_buttonClick
_cal_start_buttonClick:
;BPWasher_events_code.c,211 :: 		void cal_start_buttonClick() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,213 :: 		if (cal_start_button.Caption == cal_start_button_Caption)
MOVW	R0, #lo_addr(_cal_start_button+24)
MOVT	R0, #hi_addr(_cal_start_button+24)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_cal_start_button_Caption+0)
MOVT	R0, #hi_addr(_cal_start_button_Caption+0)
CMP	R1, R0
IT	NE
BNE	L_cal_start_buttonClick0
;BPWasher_events_code.c,216 :: 		CalibrationScreenVisible(FALSE);
MOVS	R0, #0
BL	_CalibrationScreenVisible+0
;BPWasher_events_code.c,217 :: 		cal_start_button.Caption = cal_start_button_CaptionStop;
MOVW	R1, #lo_addr(_cal_start_button_CaptionStop+0)
MOVT	R1, #hi_addr(_cal_start_button_CaptionStop+0)
MOVW	R0, #lo_addr(_cal_start_button+24)
MOVT	R0, #hi_addr(_cal_start_button+24)
STR	R1, [R0, #0]
;BPWasher_events_code.c,220 :: 		nState = CAL_START;
MOVS	R1, #200
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;BPWasher_events_code.c,222 :: 		} else {
IT	AL
BAL	L_cal_start_buttonClick1
L_cal_start_buttonClick0:
;BPWasher_events_code.c,224 :: 		CalibrationScreenVisible(TRUE);
MOVS	R0, #1
BL	_CalibrationScreenVisible+0
;BPWasher_events_code.c,225 :: 		cal_temp_label.Visible = FALSE;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_temp_label+27)
MOVT	R0, #hi_addr(_cal_temp_label+27)
STRB	R1, [R0, #0]
;BPWasher_events_code.c,226 :: 		cal_start_button.Caption = cal_start_button_Caption;
MOVW	R1, #lo_addr(_cal_start_button_Caption+0)
MOVT	R1, #hi_addr(_cal_start_button_Caption+0)
MOVW	R0, #lo_addr(_cal_start_button+24)
MOVT	R0, #hi_addr(_cal_start_button+24)
STR	R1, [R0, #0]
;BPWasher_events_code.c,229 :: 		nState = CAL_STOP;
MOVS	R1, #206
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;BPWasher_events_code.c,230 :: 		}
L_cal_start_buttonClick1:
;BPWasher_events_code.c,232 :: 		DrawScreen(&Calibration);
MOVW	R0, #lo_addr(_Calibration+0)
MOVT	R0, #hi_addr(_Calibration+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,233 :: 		}
L_end_cal_start_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _cal_start_buttonClick
_CalibrationScreenVisible:
;BPWasher_events_code.c,235 :: 		void CalibrationScreenVisible(char visibility){
; visibility start address is: 0 (R0)
SUB	SP, SP, #4
; visibility end address is: 0 (R0)
; visibility start address is: 0 (R0)
;BPWasher_events_code.c,237 :: 		cal_offset_button.Visible = visibility;
MOVW	R1, #lo_addr(_cal_offset_button+18)
MOVT	R1, #hi_addr(_cal_offset_button+18)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,238 :: 		cal_fact_button.Visible = visibility;
MOVW	R1, #lo_addr(_cal_fact_button+18)
MOVT	R1, #hi_addr(_cal_fact_button+18)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,239 :: 		cal_offset_value.Visible = visibility;
MOVW	R1, #lo_addr(_cal_offset_value+18)
MOVT	R1, #hi_addr(_cal_offset_value+18)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,240 :: 		cal_factor_value.Visible = visibility;
MOVW	R1, #lo_addr(_cal_factor_value+18)
MOVT	R1, #hi_addr(_cal_factor_value+18)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,241 :: 		cal_ret_button.Visible = visibility;
MOVW	R1, #lo_addr(_cal_ret_button+18)
MOVT	R1, #hi_addr(_cal_ret_button+18)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,242 :: 		Image10.Visible = visibility;
MOVW	R1, #lo_addr(_Image10+20)
MOVT	R1, #hi_addr(_Image10+20)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,243 :: 		cal_confirm_button.Visible = visibility;
MOVW	R1, #lo_addr(_cal_confirm_button+18)
MOVT	R1, #hi_addr(_cal_confirm_button+18)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,244 :: 		cal_message_label.Visible = !visibility;
CMP	R0, #0
MOVW	R2, #0
BNE	L__CalibrationScreenVisible35
MOVS	R2, #1
L__CalibrationScreenVisible35:
MOVW	R1, #lo_addr(_cal_message_label+27)
MOVT	R1, #hi_addr(_cal_message_label+27)
STRB	R2, [R1, #0]
;BPWasher_events_code.c,246 :: 		cal_offset_button.Active = visibility;
MOVW	R1, #lo_addr(_cal_offset_button+19)
MOVT	R1, #hi_addr(_cal_offset_button+19)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,247 :: 		cal_fact_button.Active = visibility;
MOVW	R1, #lo_addr(_cal_fact_button+19)
MOVT	R1, #hi_addr(_cal_fact_button+19)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,248 :: 		cal_offset_value.Active = visibility;
MOVW	R1, #lo_addr(_cal_offset_value+19)
MOVT	R1, #hi_addr(_cal_offset_value+19)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,249 :: 		cal_factor_value.Active = visibility;
MOVW	R1, #lo_addr(_cal_factor_value+19)
MOVT	R1, #hi_addr(_cal_factor_value+19)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,250 :: 		cal_ret_button.Active = visibility;
MOVW	R1, #lo_addr(_cal_ret_button+19)
MOVT	R1, #hi_addr(_cal_ret_button+19)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,251 :: 		Image10.Active = visibility;
MOVW	R1, #lo_addr(_Image10+21)
MOVT	R1, #hi_addr(_Image10+21)
STRB	R0, [R1, #0]
;BPWasher_events_code.c,252 :: 		cal_confirm_button.Active = visibility;
MOVW	R1, #lo_addr(_cal_confirm_button+19)
MOVT	R1, #hi_addr(_cal_confirm_button+19)
STRB	R0, [R1, #0]
; visibility end address is: 0 (R0)
;BPWasher_events_code.c,253 :: 		}
L_end_CalibrationScreenVisible:
ADD	SP, SP, #4
BX	LR
; end of _CalibrationScreenVisible
_cal_confirm_buttonClick:
;BPWasher_events_code.c,255 :: 		void cal_confirm_buttonClick() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,256 :: 		DrawScreen(&Settings);
MOVW	R0, #lo_addr(_Settings+0)
MOVT	R0, #hi_addr(_Settings+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,257 :: 		}
L_end_cal_confirm_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _cal_confirm_buttonClick
_cal_ret_buttonClick:
;BPWasher_events_code.c,259 :: 		void cal_ret_buttonClick() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,261 :: 		set_and_format_value(&fCal_offset, fCal_offsetOld);
MOVW	R0, #lo_addr(_fCal_offsetOld+0)
MOVT	R0, #hi_addr(_fCal_offsetOld+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fCal_offset+0)
MOVT	R0, #hi_addr(_fCal_offset+0)
BL	_set_and_format_value+0
;BPWasher_events_code.c,262 :: 		set_and_format_value(&fCal_factor, fCal_factorOld);
MOVW	R0, #lo_addr(_fCal_factorOld+0)
MOVT	R0, #hi_addr(_fCal_factorOld+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fCal_factor+0)
MOVT	R0, #hi_addr(_fCal_factor+0)
BL	_set_and_format_value+0
;BPWasher_events_code.c,263 :: 		DrawScreen(&Calibration);
MOVW	R0, #lo_addr(_Calibration+0)
MOVT	R0, #hi_addr(_Calibration+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,264 :: 		UpDateMemory();
BL	_UpDateMemory+0
;BPWasher_events_code.c,265 :: 		}
L_end_cal_ret_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _cal_ret_buttonClick
_Exit_Pressed:
;BPWasher_events_code.c,269 :: 		void Exit_Pressed()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,271 :: 		DrawScreen(&Settings);
MOVW	R0, #lo_addr(_Settings+0)
MOVT	R0, #hi_addr(_Settings+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,272 :: 		}
L_end_Exit_Pressed:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Exit_Pressed
_ErrorLog_OKClick:
;BPWasher_events_code.c,276 :: 		void ErrorLog_OKClick()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,278 :: 		DrawScreen(&Settings);
MOVW	R0, #lo_addr(_Settings+0)
MOVT	R0, #hi_addr(_Settings+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,279 :: 		}
L_end_ErrorLog_OKClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _ErrorLog_OKClick
_config_drain_buttonClick:
;BPWasher_events_code.c,284 :: 		void config_drain_buttonClick(){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,285 :: 		Display_Keyboard("Enter drain time", &fDrainTime);
MOVW	R0, #lo_addr(?lstr8_BPWasher_events_code+0)
MOVT	R0, #hi_addr(?lstr8_BPWasher_events_code+0)
MOVW	R1, #lo_addr(_fDrainTime+0)
MOVT	R1, #hi_addr(_fDrainTime+0)
BL	_Display_Keyboard+0
;BPWasher_events_code.c,286 :: 		}
L_end_config_drain_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _config_drain_buttonClick
_config_fill_buttonClick:
;BPWasher_events_code.c,288 :: 		void config_fill_buttonClick(){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,289 :: 		Display_Keyboard("Enter extra fill time", &fExtraFillTime);
MOVW	R0, #lo_addr(?lstr9_BPWasher_events_code+0)
MOVT	R0, #hi_addr(?lstr9_BPWasher_events_code+0)
MOVW	R1, #lo_addr(_fExtraFillTime+0)
MOVT	R1, #hi_addr(_fExtraFillTime+0)
BL	_Display_Keyboard+0
;BPWasher_events_code.c,290 :: 		}
L_end_config_fill_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _config_fill_buttonClick
_config_confirm_buttonClick:
;BPWasher_events_code.c,292 :: 		void config_confirm_buttonClick() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,293 :: 		UpDateMemory();
BL	_UpDateMemory+0
;BPWasher_events_code.c,294 :: 		DrawScreen(&Settings);
MOVW	R0, #lo_addr(_Settings+0)
MOVT	R0, #hi_addr(_Settings+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,295 :: 		}
L_end_config_confirm_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _config_confirm_buttonClick
_config_ret_buttonClick:
;BPWasher_events_code.c,297 :: 		void config_ret_buttonClick() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_events_code.c,298 :: 		set_and_format_value(&fDrainTime, fDrainTimeOld);
MOVW	R0, #lo_addr(_fDrainTimeOld+0)
MOVT	R0, #hi_addr(_fDrainTimeOld+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fDrainTime+0)
MOVT	R0, #hi_addr(_fDrainTime+0)
BL	_set_and_format_value+0
;BPWasher_events_code.c,299 :: 		set_and_format_value(&fExtraFillTime, fExtraFillTimeOld);
MOVW	R0, #lo_addr(_fExtraFillTimeOld+0)
MOVT	R0, #hi_addr(_fExtraFillTimeOld+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fExtraFillTime+0)
MOVT	R0, #hi_addr(_fExtraFillTime+0)
BL	_set_and_format_value+0
;BPWasher_events_code.c,300 :: 		UpDateMemory();
BL	_UpDateMemory+0
;BPWasher_events_code.c,301 :: 		DrawScreen(&Config);
MOVW	R0, #lo_addr(_Config+0)
MOVT	R0, #hi_addr(_Config+0)
BL	_DrawScreen+0
;BPWasher_events_code.c,302 :: 		}
L_end_config_ret_buttonClick:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _config_ret_buttonClick
