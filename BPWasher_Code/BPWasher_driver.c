#include "BPWasher_objects.h"
#include "BPWasher_resources.h"
#include "built_in.h"
#include "Constants.h"

// TFT module connections
unsigned int TFT_DataPort at GPIOE_ODR;
sbit TFT_RST at GPIOE_ODR.B8;
sbit TFT_RS at GPIOE_ODR.B12;
sbit TFT_CS at GPIOE_ODR.B15;
sbit TFT_RD at GPIOE_ODR.B10;
sbit TFT_WR at GPIOE_ODR.B11;
sbit TFT_BLED at GPIOE_ODR.B9;
// End TFT module connections

// Touch Panel module connections
sbit DriveX_Left at GPIOB_ODR.B1;
sbit DriveX_Right at GPIOB_ODR.B8;
sbit DriveY_Up at GPIOB_ODR.B9;
sbit DriveY_Down at GPIOB_ODR.B0;
// End Touch Panel module connections

// Global variables
unsigned int Xcoord, Ycoord;
const ADC_THRESHOLD = 1500;
char PenDown;
typedef unsigned long TPointer;
TPointer PressedObject;
int PressedObjectType;
unsigned int caption_length, caption_height;
unsigned int display_width, display_height;

int _object_count;
unsigned short object_pressed;
TButton *local_button;
TButton *exec_button;
int button_order;
TButton_Round *local_round_button;
TButton_Round *exec_round_button;
int round_button_order;
TCButton_Round *local_round_cbutton;
TCButton_Round *exec_round_cbutton;
int round_cbutton_order;
TLabel *local_label;
TLabel *exec_label;
int label_order;
TImage *local_image;
TImage *exec_image;
int image_order;
TCImage *local_cimage;
TCImage *exec_cimage;
int cimage_order;
TCircle *local_circle;
TCircle *exec_circle;
int circle_order;
TBox *local_box;
TBox *exec_box;
int box_order;
TCheckBox *local_checkbox;
TCheckBox *exec_checkbox;
int checkbox_order;


void Init_ADC() {
  ADC_Set_Input_Channel(_ADC_CHANNEL_8 | _ADC_CHANNEL_9);
  ADC1_Init();
  Delay_ms(100);
}
static void InitializeTouchPanel() {
  Init_ADC();
  TFT_Init_ILI9341_8bit(320, 240);

  TP_TFT_Init(320, 240, 8, 9);                                  // Initialize touch panel
  TP_TFT_Set_ADC_Threshold(ADC_THRESHOLD);                              // Set touch panel ADC threshold

  PenDown = 0;
  PressedObject = 0;
  PressedObjectType = -1;
}
#define  TP_XMIN 358
#define  TP_XMAX 3464
#define  TP_YMIN 212
#define  TP_YMAX 3678

unsigned int x_tp_min;
unsigned int x_tp_max;
unsigned int y_tp_min;
unsigned int y_tp_max;

void Calibrate() {

   x_tp_min = TP_XMIN;
   x_tp_max = TP_XMAX;
   y_tp_min = TP_YMIN;
   y_tp_max = TP_YMAX;
  /*TFT_Set_Pen(CL_WHITE, 3);
  TFT_Set_Font(TFT_defaultFont, CL_WHITE, FO_HORIZONTAL);
  TFT_Write_Text("Touch selected corners for calibration", 50, 80);
  TFT_Line(315, 239, 319, 239);
  TFT_Line(309, 229, 319, 239);
  TFT_Line(319, 234, 319, 239);
  TFT_Write_Text("first here", 210, 220);

  TP_TFT_Calibrate_Min();                      // Calibration of TP minimum
  Delay_ms(500);

  TFT_Set_Pen(CL_BLACK, 3);
  TFT_Set_Font(TFT_defaultFont, CL_BLACK, FO_HORIZONTAL);
  TFT_Line(315, 239, 319, 239);
  TFT_Line(309, 229, 319, 239);
  TFT_Line(319, 234, 319, 239);
  TFT_Write_Text("first here", 210, 220);

  TFT_Set_Pen(CL_WHITE, 3);
  TFT_Set_Font(TFT_defaultFont, CL_WHITE, FO_HORIZONTAL);
  TFT_Write_Text("now here ", 20, 5);
  TFT_Line(0, 0, 5, 0);
  TFT_Line(0, 0, 0, 5);
  TFT_Line(0, 0, 10, 10);

  TP_TFT_Calibrate_Max();  */                     // Calibration of TP maximum
  TP_TFT_Set_Calibration_Consts( x_tp_min, x_tp_max, y_tp_min, y_tp_max);
  Delay_ms(500);
}


/////////////////////////
  TScreen*  CurrentScreen;

  TScreen                Keypad;
  TButton_Round          Key_7;
char Key_7_Caption[2] = "7";

  TButton_Round          Key_8;
char Key_8_Caption[2] = "8";

  TButton_Round          Key_9;
char Key_9_Caption[2] = "9";

  TButton_Round          Key_clear;
char Key_clear_Caption[2] = "<";

  TButton_Round          Key_4;
char Key_4_Caption[2] = "4";

  TButton_Round          Key_5;
char Key_5_Caption[2] = "5";

  TButton_Round          Key_6;
char Key_6_Caption[2] = "6";

  TButton_Round          Key_1;
char Key_1_Caption[2] = "1";

  TButton_Round          Key_2;
char Key_2_Caption[2] = "2";

  TButton_Round          Key_3;
char Key_3_Caption[2] = "3";

  TButton_Round          Key_dp;
char Key_dp_Caption[2] = ".";

  TButton_Round          Key_0;
char Key_0_Caption[2] = "0";

  TButton_Round          Key_enter;
char Key_enter_Caption[6] = "Enter";

  TCButton_Round          Key_return_button =
         {
         &Keypad               , //   Key_return_button.OwnerScreen
         13                    , //   Key_return_button.Order
         1                     , //   Key_return_button.Left
         0                     , //   Key_return_button.Top
         80                    , //   Key_return_button.Width
         52                    , //   Key_return_button.Height
         1                     , //   Key_return_button.Pen_Width
         0x0000                , //   Key_return_button.Pen_Color
         1                     , //   Key_return_button.Visible
         1                     , //   Key_return_button.Active
         1                     , //   Key_return_button.Transparent
         &Key_return_button_Caption, //   Key_return_button.Caption
         _taCenter             , //   Key_return_button.TextAlign
         _tavMiddle            , //   Key_return_button.TextAlignVertical
         &Tahoma11x13_Regular  , //   Key_return_button.FontName
         0x0000                , //   Key_return_button.FontColor
         0                     , //   Key_return_button.VerticalText
         0                     , //   Key_return_button.Gradient
         0                     , //   Key_return_button.Gradient_Orientation
         0xF800                , //   Key_return_button.Gradient_Start_Color
         0xC618                , //   Key_return_button.Gradient_End_Color
         0xF800                , //   Key_return_button.Color
         3                     , //   Key_return_button.CornerRadius
         0                     , //   Key_return_button.PressColEnabled
         0xE71C                , //   Key_return_button.Press_Color
         0                     , //   Key_return_button.OnUpPtr
         0                     , //   Key_return_button.OnDownPtr
         ret_prev_screen       , //   Key_return_button.OnClickPtr     // This was OnPressPtr which was making it execute on KeyDown, making the Button 'underneath'
         0                       //   Key_return_button.OnPressPtr     // execute on the same KeyUp - CM 10/07/17
         };

const char Key_return_button_Caption[1] = "";

  TButton               kp_display;
char kp_display_Caption[7] = "     0";

  TCImage               Image8 = 
         {
         &Keypad               , //   Image8.OwnerScreen
         15                    , //   Image8.Order          
         21                    , //   Image8.Left           
         5                     , //   Image8.Top            
         40                    , //   Image8.Width          
         42                    , //   Image8.Height         
         &returnsarrow_bmp     , //   Image8.Picture_Name  
         1                     , //   Image8.Visible      
         0                     , //   Image8.Active         
         0                     , //   Image8.Picture_Type        
         1                     , //   Image8.Picture_Ratio       
         0                     , //   Image8.OnUpPtr
         0                     , //   Image8.OnDownPtr
         0                     , //   Image8.OnClickPtr
         0                       //   Image8.OnPressPtr
         };
  TButton_Round          pos_neg;
char pos_neg_Caption[4] = "+/-";

  TLabel                 lblKeyboardTitle;

  TButton                * const code Screen1_Buttons[1]=
         {
         &kp_display           
         };
  TButton_Round          * const code Screen1_Buttons_Round[14]=
         {
         &Key_7,               
         &Key_8,               
         &Key_9,               
         &Key_clear,           
         &Key_4,               
         &Key_5,               
         &Key_6,               
         &Key_1,               
         &Key_2,               
         &Key_3,               
         &Key_dp,              
         &Key_0,               
         &Key_enter,           
         &pos_neg              
         };
  TCButton_Round         * const code Screen1_CButtons_Round[1]=
         {
         &Key_return_button    
         };
  TLabel                 * const code Screen1_Labels[1]=
         {
         &lblKeyboardTitle
         };
  TCImage                * const code Screen1_CImages[1]=
         {
         &Image8               
         };

  TScreen                _main;
  TLabel                 Diagram3_Label1;
char Diagram3_Label1_Caption[3] = "�C";

  TCircle                indicator;
  TButton               Screen_PV;
char Screen_PV_Caption[6] = "999.9";

  TLine                  Line2;
  TButton_Round          Main_Screen_settings;
char Main_Screen_settings_Caption[1] = "";

  TImage               Main_screen_settings_image;
  TLine                  Line1;
  TButton_Round          Main_Screen_start;
char Main_Screen_start_Caption[5] = "STOP";

  TImage               start_stop;
  TImage               Diagram3_thermred;
  TImage               Diagram3_thermBlack;
  TLabel                 Label1;
char Label1_Caption[8] = "STATUS:";

  TLabel                 Label2;
char Label2_Caption[50] = "";

  TButton                * const code Screen2_Buttons[1]=
         {
         &Screen_PV            
         };
  TButton_Round          * const code Screen2_Buttons_Round[2]=
         {
         &Main_Screen_settings,
         &Main_Screen_start    
         };
  TLabel                 * const code Screen2_Labels[3]=
         {
         &Diagram3_Label1,     
         &Label1,              
         &Label2               
         };
  TImage                 * const code Screen2_Images[4]=
         {
         &Main_screen_settings_image,
         &start_stop,          
         &Diagram3_thermred,   
         &Diagram3_thermBlack  
         };
  TCircle                * const code Screen2_Circles[1]=
         {
         &indicator            
         };
  TLine                  * const code Screen2_Lines[2]=
         {
         &Line2,               
         &Line1                
         };

  TScreen                Settings;
  TButton_Round          Settings_temp;
char Settings_temp_Caption[] = "LOGS";

  TButton_Round          Settings_PID;
char Settings_PID_Caption[] = "PID";

  TButton_Round          Settings_CAL;
char Settings_CAL_Caption[] = "CAL";

  TButton_Round          Settings_Exit;
char Settings_Exit_Caption[] = "EXIT";

  TImage               therm_settings;
  TButton_Round          ToolBox;
char ToolBox_Caption[] = "I/O";

  TImage               imgToolBox;
  TButton_Round          Settings_drain;
  char Settings_drain_Caption[] = "CONFIG";

  TButton_Round          * const code Screen3_Buttons_Round[6]=
         {
         &Settings_temp,       
         &Settings_PID,        
         &Settings_CAL,        
         &Settings_Exit,       
         &ToolBox,
         &Settings_drain
         };

  TScreen                PID;
  const char Prop_button_Caption[2] = "P";
  TCButton_Round          Prop_button = 
         {
         &PID                  , //   Prop_button.OwnerScreen
         0                     , //   Prop_button.Order           
         6                     , //   Prop_button.Left            
         4                     , //   Prop_button.Top              
         74                    , //   Prop_button.Width            
         70                    , //   Prop_button.Height        
         2                     , //   Prop_button.Pen_Width          
         0xFFFF                , //   Prop_button.Pen_Color       
         1                     , //   Prop_button.Visible         
         1                     , //   Prop_button.Active           
         1                     , //   Prop_button.Transparent     
         &Prop_button_Caption  , //   Prop_button.Caption        
         _taCenter             , //   Prop_button.TextAlign        
         _tavMiddle            , //   Prop_button.TextAlignVertical        
         &Tahoma50x62_Regular  , //   Prop_button.FontName        
         0x0000                , //   Prop_button.FontColor       
         0                     , //   Prop_button.VerticalText       
         0                     , //   Prop_button.Gradient        
         0                     , //   Prop_button.Gradient_Orientation    
         0xFFFF                , //   Prop_button.Gradient_Start_Color    
         0xC618                , //   Prop_button.Gradient_End_Color    
         0x87F0                , //   Prop_button.Color           
         5                     , //   Prop_button.CornerRadius           
         1                     , //   Prop_button.PressColEnabled 
         0xE71C                , //   Prop_button.Press_Color     
         0                     , //   Prop_button.OnUpPtr
         0                     , //   Prop_button.OnDownPtr
         Prop_buttonClick      , //   Prop_button.OnClickPtr
         0                       //   Prop_button.OnPressPtr
         };


  TButton_Round          P_value;
char P_value_Caption[7] = "312";

  TButton_Round          I_value;
char I_value_Caption[7] = "5";

  TButton_Round          D_value;
char D_value_Caption[7] = "100";

  TButton_Round          I_button;
char I_button_Caption[2] = "I";

  TButton_Round          D_button;
char D_button_Caption[2] = "D";

  TButton_Round          PID_conf_button;
char PID_conf_button_Caption[3] = "OK";

  TButton_Round          PID_ret_button;
char PID_ret_button_Caption[1] = "";

  TImage               Image9;
  TButton_Round          * const code Screen4_Buttons_Round[7]=
         {
         &P_value,             
         &I_value,             
         &D_value,             
         &I_button,            
         &D_button,            
         &PID_conf_button,     
         &PID_ret_button       
         };
  TCButton_Round         * const code Screen4_CButtons_Round[1]=
         {
         &Prop_button          
         };
  TImage                 * const code Screen4_Images[1]=
         {
         &Image9               
         };

  TScreen                Calibration;
  TCButton_Round          cal_offset_button = 
         {
         &Calibration          , //   cal_offset_button.OwnerScreen
         0                     , //   cal_offset_button.Order           
         4                     , //   cal_offset_button.Left            
         4                     , //   cal_offset_button.Top
         95                    , //   cal_offset_button.Width            
         77                    , //   cal_offset_button.Height        
         2                     , //   cal_offset_button.Pen_Width          
         0xFFFF                , //   cal_offset_button.Pen_Color       
         1                     , //   cal_offset_button.Visible
         1                     , //   cal_offset_button.Active
         1                     , //   cal_offset_button.Transparent     
         &cal_offset_button_Caption, //   cal_offset_button.Caption        
         _taCenter             , //   cal_offset_button.TextAlign        
         _tavMiddle            , //   cal_offset_button.TextAlignVertical        
         &Tahoma16x19_Regular  , //   cal_offset_button.FontName        
         0xFFFF                , //   cal_offset_button.FontColor       
         0                     , //   cal_offset_button.VerticalText       
         0                     , //   cal_offset_button.Gradient        
         0                     , //   cal_offset_button.Gradient_Orientation    
         0xFFFF                , //   cal_offset_button.Gradient_Start_Color    
         0xC618                , //   cal_offset_button.Gradient_End_Color    
         0xF810                , //   cal_offset_button.Color           
         5                     , //   cal_offset_button.CornerRadius           
         1                     , //   cal_offset_button.PressColEnabled 
         0xE71C                , //   cal_offset_button.Press_Color     
         0                     , //   cal_offset_button.OnUpPtr
         0                     , //   cal_offset_button.OnDownPtr
         cal_offset_buttonClick, //   cal_offset_button.OnClickPtr
         0                       //   cal_offset_button.OnPressPtr
         };
const char cal_offset_button_Caption[12] = "CAL Off-Set";

  TButton_Round          cal_offset_value;
char cal_offset_value_Caption[7] = "0.6";

  TButton_Round          cal_factor_value;
char cal_factor_value_Caption[7] = "0.834";

  TButton_Round          cal_fact_button;
char cal_fact_button_Caption[11] = "CAL Factor";

  TButton_Round          cal_confirm_button;
char cal_confirm_button_Caption[3] = "OK";

  TButton_Round          cal_ret_button;
char cal_ret_button_Caption[1] = "";

  TImage               Image10;
    TButton_Round          cal_start_button;
char cal_start_button_Caption[] = "Start Calibration";
char cal_start_button_CaptionStop[] = "Stop Calibration";

  TLabel                 cal_message_label;
char cal_message_label_Caption[51] = " ";

  TLabel                 cal_temp_label;
char cal_temp_label_Caption[5] = "Text";

  TButton_Round          * const code Screen5_Buttons_Round[6]=
         {
         &cal_offset_value,    
         &cal_factor_value,    
         &cal_fact_button,     
         &cal_confirm_button,  
         &cal_ret_button,
         &cal_start_button
         };
  TCButton_Round         * const code Screen5_CButtons_Round[1]=
         {
         &cal_offset_button    
         };
  TLabel                 * const code Screen5_Labels[2]=
          {
          &cal_message_label,
          &cal_temp_label
          };
  TImage                 * const code Screen5_Images[1]=
         {
         &Image10              
         };

  TScreen                Diags;
  TLine                  Line3;
  TLabel                 Lbl_inputs;
char Lbl_inputs_Caption[7] = "Inputs";

  TLabel                 Lbl_outputs;
char Lbl_outputs_Caption[8] = "Outputs";

  TCircle                Circle1;
  TLabel                 Lbl_water;
char Lbl_water_Caption[9] = "Lid Shut";

  TCircle                Circle2;
  TLabel                 Label3;
char Label3_Caption[6] = "Start";

  TCircle                Circle3;
  TLabel                 Label4;
char Label4_Caption[9] = "Lid Open";

  TCircle                Circle4;
  TLabel                 Label5;
char Label5_Caption[10] = "Spare"; //"Over Temp";

  TCircle                Circle5;
  TLabel                 Label6;
char Label6_Caption[12] = "Water Level";

  TCircle                Circle6;
  TLabel                 Label7;
char Label7_Caption[14] = "Element Fault";

  TCheckBox                 CheckBox1;
char CheckBox1_Caption[11] = " Cycle Run";

  TCheckBox                 CheckBox2;
char CheckBox2_Caption[16] = " Cycle Complete";

  TCheckBox                 CheckBox3;
char CheckBox3_Caption[10] = " Impellor";

  TCheckBox                 CheckBox4;
char CheckBox4_Caption[12] = " Lid Unlock";

  TCheckBox                 CheckBox5;
char CheckBox5_Caption[10] = " Water In";

  TCheckBox                 CheckBox6;
char CheckBox6_Caption[7] = " Drain";

  TButton               Button1;
char Button1_Caption[5] = "Exit";
 TButton                * const code Screen6_Buttons[1]=
         {
         &Button1
         };
  TLabel                 * const code Screen6_Labels[8]=
         {
         &Lbl_inputs,          
         &Lbl_outputs,         
         &Lbl_water,           
         &Label3,              
         &Label4,              
         &Label5,              
         &Label6,              
         &Label7               
         };
  TCircle                * const code Screen6_Circles[6]=
         {
         &Circle1,             
         &Circle2,             
         &Circle3,             
         &Circle4,             
         &Circle5,             
         &Circle6              
         };
  TLine                  * const code Screen6_Lines[1]=
         {
         &Line3                
         };
  TCheckBox              * const code Screen6_CheckBoxes[6]=
         {
         &CheckBox1,           
         &CheckBox2,           
         &CheckBox3,           
         &CheckBox4,           
         &CheckBox5,           
         &CheckBox6            
         };

  TScreen                SplashLand;
  TImage               Image1;
  TImage               Image2;
  TBox                   Box1;
  TBox                   Box2;
  TLabel                 Label8;

#if SWITCH_ON_I2C
char Label8_Caption[20] = "Version " VERSION;
#else
char Label8_Caption[40] = "Version " VERSION "  !!! NO I2C !!!"; // // add warning on splash Screen if no I2C - CM
#endif
  TLabel                 * const code Screen7_Labels[1]=
         {
         &Label8               
         };
  TImage                 * const code Screen7_Images[2]=
         {
         &Image1,              
         &Image2               
         };
  TBox                   * const code Screen7_Boxes[2]=
         {
         &Box1,                
         &Box2                 
         };

  TScreen                ErrorLog;
  TLabel                 Log_Labels[ERRLOG_LINE_N];

  TLabel                 Log_Title;
char Log_Title_Caption[42] = "9999 / 9999 = Complete / Faults";

  TButton_Round          ErrorLog_OK;
char ErrorLog_OK_Caption[3] = "OK";

  TButton_Round          * const code Screen8_Buttons_Round[1]=
         {
         &ErrorLog_OK
         };
  TLabel                 * const code Screen8_Labels[ERRLOG_LINE_N+1]=
         {
         &Log_Title,
         &Log_Labels[0],
         &Log_Labels[1],
         &Log_Labels[2],
         &Log_Labels[3],
         &Log_Labels[4],
         &Log_Labels[5],
         &Log_Labels[6],
         &Log_Labels[7],
         &Log_Labels[8],
         &Log_Labels[9]
         };
         
TScreen                Config;
  TLabel                 config_fill_value_label;
char config_fill_value_label_Caption[7] = "5 s";

  TLabel                 config_drain_value_label;
char config_drain_value_label_Caption[7] = "60 s";

  TButton_Round          config_fill_button;
char config_fill_button_Caption[11] = "Extra Fill";

  TButton_Round          config_drain_button;
char config_drain_button_Caption[11] = "Drain";

  TButton_Round          config_confirm_button;
  char config_confirm_button_Caption[3] = "OK";

  TButton_Round          config_ret_button;
char config_ret_button_Caption[1] = "";

  TImage               Image3;
  TButton_Round          * const code Screen9_Buttons_Round[4]=
         {
         &config_fill_button,
         &config_drain_button,
         &config_confirm_button,
         &config_ret_button
         };
  TLabel                 * const code Screen9_Labels[2]=
         {
         &config_fill_value_label,
         &config_drain_value_label
         };
  TImage                 * const code Screen9_Images[1]=
         {
         &Image3
         };
         
         

static void InitializeObjects() {
  Keypad.Color                     = 0x0000;
  Keypad.Width                     = 320;
  Keypad.Height                    = 240;
  Keypad.ButtonsCount              = 1;
  Keypad.Buttons                   = Screen1_Buttons;
  Keypad.Buttons_RoundCount        = 14;
  Keypad.Buttons_Round             = Screen1_Buttons_Round;
  Keypad.CButtons_RoundCount       = 1;
  Keypad.CButtons_Round            = Screen1_CButtons_Round;
  Keypad.LabelsCount               = 1;
  Keypad.Labels                    = Screen1_Labels;
  Keypad.ImagesCount               = 0;
  Keypad.CImagesCount              = 1;
  Keypad.CImages                   = Screen1_CImages;
  Keypad.CirclesCount              = 0;
  Keypad.BoxesCount                = 0;
  Keypad.LinesCount                = 0;
  Keypad.CheckBoxesCount           = 0;
  Keypad.ObjectsCount              = 18;

  _main.Color                     = 0x0000;
  _main.Width                     = 320;
  _main.Height                    = 240;
  _main.ButtonsCount              = 1;
  _main.Buttons                   = Screen2_Buttons;
  _main.Buttons_RoundCount        = 2;
  _main.Buttons_Round             = Screen2_Buttons_Round;
  _main.CButtons_RoundCount       = 0;
  _main.LabelsCount               = 3;
  _main.Labels                    = Screen2_Labels;
  _main.ImagesCount               = 4;
  _main.Images                    = Screen2_Images;
  _main.CImagesCount              = 0;
  _main.CirclesCount              = 1;
  _main.Circles                   = Screen2_Circles;
  _main.BoxesCount                = 0;
  _main.LinesCount                = 2;
  _main.Lines                     = Screen2_Lines;
  _main.CheckBoxesCount           = 0;
  _main.ObjectsCount              = 13;

  Settings.Color                     = 0x0000;
  Settings.Width                     = 320;
  Settings.Height                    = 240;
  Settings.ButtonsCount              = 0;
  Settings.Buttons_RoundCount        = 6;
  Settings.Buttons_Round             = Screen3_Buttons_Round;
  Settings.CButtons_RoundCount       = 0;
  Settings.LabelsCount               = 0;
  Settings.ImagesCount               = 0;
  Settings.CImagesCount              = 0;
  Settings.CirclesCount              = 0;
  Settings.BoxesCount                = 0;
  Settings.LinesCount                = 0;
  Settings.CheckBoxesCount           = 0;
  Settings.ObjectsCount              = 6;

  PID.Color                     = 0x0000;
  PID.Width                     = 320;
  PID.Height                    = 240;
  PID.ButtonsCount              = 0;
  PID.Buttons_RoundCount        = 7;
  PID.Buttons_Round             = Screen4_Buttons_Round;
  PID.CButtons_RoundCount       = 1;
  PID.CButtons_Round            = Screen4_CButtons_Round;
  PID.LabelsCount               = 0;
  PID.ImagesCount               = 1;
  PID.Images                    = Screen4_Images;
  PID.CImagesCount              = 0;
  PID.CirclesCount              = 0;
  PID.BoxesCount                = 0;
  PID.LinesCount                = 0;
  PID.CheckBoxesCount           = 0;
  PID.ObjectsCount              = 9;

  Calibration.Color                     = 0x0000;
  Calibration.Width                     = 320;
  Calibration.Height                    = 240;
  Calibration.ButtonsCount              = 0;
  Calibration.Buttons_RoundCount        = 6;
  Calibration.Buttons_Round             = Screen5_Buttons_Round;
  Calibration.CButtons_RoundCount       = 1;
  Calibration.CButtons_Round            = Screen5_CButtons_Round;
  Calibration.LabelsCount               = 2;
  Calibration.Labels                    = Screen5_Labels;
  Calibration.ImagesCount               = 1;
  Calibration.Images                    = Screen5_Images;
  Calibration.CImagesCount              = 0;
  Calibration.CirclesCount              = 0;
  Calibration.BoxesCount                = 0;
  Calibration.LinesCount                = 0;
  Calibration.CheckBoxesCount           = 0;
  Calibration.ObjectsCount              = 10;

  Diags.Color                     = 0x041F;
  Diags.Width                     = 320;
  Diags.Height                    = 240;
  Diags.ButtonsCount              = 1;
  Diags.Buttons                   = Screen6_Buttons;
  Diags.Buttons_RoundCount        = 0;
  Diags.CButtons_RoundCount       = 0;
  Diags.LabelsCount               = 8;
  Diags.Labels                    = Screen6_Labels;
  Diags.ImagesCount               = 0;
  Diags.CImagesCount              = 0;
  Diags.CirclesCount              = 6;
  Diags.Circles                   = Screen6_Circles;
  Diags.BoxesCount                = 0;
  Diags.LinesCount                = 1;
  Diags.Lines                     = Screen6_Lines;
  Diags.CheckBoxesCount           = 6;
  Diags.CheckBoxes                = Screen6_CheckBoxes;
  Diags.ObjectsCount              = 22;

  SplashLand.Color                     = 0x5AEB;
  SplashLand.Width                     = 320;
  SplashLand.Height                    = 240;
  SplashLand.ButtonsCount              = 0;
  SplashLand.Buttons_RoundCount        = 0;
  SplashLand.CButtons_RoundCount       = 0;
  SplashLand.LabelsCount               = 1;
  SplashLand.Labels                    = Screen7_Labels;
  SplashLand.ImagesCount               = 2;
  SplashLand.Images                    = Screen7_Images;
  SplashLand.CImagesCount              = 0;
  SplashLand.CirclesCount              = 0;
  SplashLand.BoxesCount                = 2;
  SplashLand.Boxes                     = Screen7_Boxes;
  SplashLand.LinesCount                = 0;
  SplashLand.CheckBoxesCount           = 0;
  SplashLand.ObjectsCount              = 5;

  ErrorLog.Color                     = 0x0000;
  ErrorLog.Width                     = 320;
  ErrorLog.Height                    = 240;
  ErrorLog.ButtonsCount              = 0;
  ErrorLog.Buttons_RoundCount        = 1;
  ErrorLog.Buttons_Round             = Screen8_Buttons_Round;
  ErrorLog.CButtons_RoundCount       = 0;
  ErrorLog.LabelsCount               = ERRLOG_LINE_N+1;
  ErrorLog.Labels                    = Screen8_Labels;
  ErrorLog.ImagesCount               = 0;
  ErrorLog.CImagesCount              = 0;
  ErrorLog.CirclesCount              = 0;
  ErrorLog.BoxesCount                = 0;
  ErrorLog.LinesCount                = 0;
  ErrorLog.CheckBoxesCount           = 0;
  ErrorLog.ObjectsCount              = ERRLOG_LINE_N+2;

  Config.Color                     = 0x0000;
  Config.Width                     = 320;
  Config.Height                    = 240;
  Config.ButtonsCount              = 0;
  Config.Buttons_RoundCount        = 4;
  Config.Buttons_Round             = Screen9_Buttons_Round;
  Config.CButtons_RoundCount       = 0;
  Config.LabelsCount               = 2;
  Config.Labels                    = Screen9_Labels;
  Config.ImagesCount               = 1;
  Config.Images                    = Screen9_Images;
  Config.CImagesCount              = 0;
  Config.CirclesCount              = 0;
  Config.BoxesCount                = 0;
  Config.LinesCount                = 0;
  Config.CheckBoxesCount           = 0;
  Config.ObjectsCount              = 7;

  Key_7.OwnerScreen     = &Keypad;
  Key_7.Order           = 0;
  Key_7.Left            = 1;
  Key_7.Top             = 162;
  Key_7.Width           = 80;
  Key_7.Height          = 38;
  Key_7.Pen_Width       = 1;
  Key_7.Pen_Color       = 0x0000;
  Key_7.Visible         = 1;
  Key_7.Active          = 1;
  Key_7.Transparent     = 1;
  Key_7.Caption         = Key_7_Caption;
  Key_7.TextAlign       = _taCenter;
  Key_7.TextAlignVertical= _tavMiddle;
  Key_7.FontName        = Tahoma26x33_Regular;
  Key_7.PressColEnabled = 1;
  Key_7.Font_Color      = 0x0000;
  Key_7.VerticalText    = 0;
  Key_7.Gradient        = 0;
  Key_7.Gradient_Orientation = 0;
  Key_7.Gradient_Start_Color = 0x4A69;
  Key_7.Gradient_End_Color = 0xC618;
  Key_7.Color           = 0xC618;
  Key_7.Press_Color     = 0xE71C;
  Key_7.Corner_Radius   = 3;
  Key_7.OnUpPtr         = 0;
  Key_7.OnDownPtr       = 0;
  Key_7.OnClickPtr      = Key_7Click;
  Key_7.OnPressPtr      = 0;

  Key_8.OwnerScreen     = &Keypad;
  Key_8.Order           = 1;
  Key_8.Left            = 81;
  Key_8.Top             = 162;
  Key_8.Width           = 80;
  Key_8.Height          = 38;
  Key_8.Pen_Width       = 1;
  Key_8.Pen_Color       = 0x0000;
  Key_8.Visible         = 1;
  Key_8.Active          = 1;
  Key_8.Transparent     = 1;
  Key_8.Caption         = Key_8_Caption;
  Key_8.TextAlign       = _taCenter;
  Key_8.TextAlignVertical= _tavMiddle;
  Key_8.FontName        = Tahoma26x33_Regular;
  Key_8.PressColEnabled = 1;
  Key_8.Font_Color      = 0x0000;
  Key_8.VerticalText    = 0;
  Key_8.Gradient        = 0;
  Key_8.Gradient_Orientation = 0;
  Key_8.Gradient_Start_Color = 0x4A69;
  Key_8.Gradient_End_Color = 0xC618;
  Key_8.Color           = 0xC618;
  Key_8.Press_Color     = 0xE71C;
  Key_8.Corner_Radius   = 3;
  Key_8.OnUpPtr         = 0;
  Key_8.OnDownPtr       = 0;
  Key_8.OnClickPtr      = Key_8Click;
  Key_8.OnPressPtr      = 0;

  Key_9.OwnerScreen     = &Keypad;
  Key_9.Order           = 2;
  Key_9.Left            = 161;
  Key_9.Top             = 162;
  Key_9.Width           = 80;
  Key_9.Height          = 38;
  Key_9.Pen_Width       = 1;
  Key_9.Pen_Color       = 0x0000;
  Key_9.Visible         = 1;
  Key_9.Active          = 1;
  Key_9.Transparent     = 1;
  Key_9.Caption         = Key_9_Caption;
  Key_9.TextAlign       = _taCenter;
  Key_9.TextAlignVertical= _tavMiddle;
  Key_9.FontName        = Tahoma26x33_Regular;
  Key_9.PressColEnabled = 1;
  Key_9.Font_Color      = 0x0000;
  Key_9.VerticalText    = 0;
  Key_9.Gradient        = 0;
  Key_9.Gradient_Orientation = 0;
  Key_9.Gradient_Start_Color = 0x4A69;
  Key_9.Gradient_End_Color = 0xC618;
  Key_9.Color           = 0xC618;
  Key_9.Press_Color     = 0xE71C;
  Key_9.Corner_Radius   = 3;
  Key_9.OnUpPtr         = 0;
  Key_9.OnDownPtr       = 0;
  Key_9.OnClickPtr      = Key_9Click;
  Key_9.OnPressPtr      = 0;

  Key_clear.OwnerScreen     = &Keypad;
  Key_clear.Order           = 3;
  Key_clear.Left            = 243;
  Key_clear.Top             = 0;
  Key_clear.Width           = 78;
  Key_clear.Height          = 52;
  Key_clear.Pen_Width       = 1;
  Key_clear.Pen_Color       = 0x0000;
  Key_clear.Visible         = 1;
  Key_clear.Active          = 1;
  Key_clear.Transparent     = 1;
  Key_clear.Caption         = Key_clear_Caption;
  Key_clear.TextAlign       = _taCenter;
  Key_clear.TextAlignVertical= _tavMiddle;
  Key_clear.FontName        = Tahoma44x45_Bold;
  Key_clear.PressColEnabled = 1;
  Key_clear.Font_Color      = 0xFFFF;
  Key_clear.VerticalText    = 0;
  Key_clear.Gradient        = 0;
  Key_clear.Gradient_Orientation = 0;
  Key_clear.Gradient_Start_Color = 0x4A69;
  Key_clear.Gradient_End_Color = 0xF800;
  Key_clear.Color           = 0xF800;
  Key_clear.Press_Color     = 0xE71C;
  Key_clear.Corner_Radius   = 3;
  Key_clear.OnUpPtr         = 0;
  Key_clear.OnDownPtr       = 0;
  Key_clear.OnClickPtr      = Key_clearClick;
  Key_clear.OnPressPtr      = 0;

  Key_4.OwnerScreen     = &Keypad;
  Key_4.Order           = 4;
  Key_4.Left            = 1;
  Key_4.Top             = 123;
  Key_4.Width           = 80;
  Key_4.Height          = 38;
  Key_4.Pen_Width       = 1;
  Key_4.Pen_Color       = 0x0000;
  Key_4.Visible         = 1;
  Key_4.Active          = 1;
  Key_4.Transparent     = 1;
  Key_4.Caption         = Key_4_Caption;
  Key_4.TextAlign       = _taCenter;
  Key_4.TextAlignVertical= _tavMiddle;
  Key_4.FontName        = Tahoma26x33_Regular;
  Key_4.PressColEnabled = 1;
  Key_4.Font_Color      = 0x0000;
  Key_4.VerticalText    = 0;
  Key_4.Gradient        = 0;
  Key_4.Gradient_Orientation = 0;
  Key_4.Gradient_Start_Color = 0x4A69;
  Key_4.Gradient_End_Color = 0xC618;
  Key_4.Color           = 0xC618;
  Key_4.Press_Color     = 0xE71C;
  Key_4.Corner_Radius   = 3;
  Key_4.OnUpPtr         = 0;
  Key_4.OnDownPtr       = 0;
  Key_4.OnClickPtr      = Key_4Click;
  Key_4.OnPressPtr      = 0;

  Key_5.OwnerScreen     = &Keypad;
  Key_5.Order           = 5;
  Key_5.Left            = 81;
  Key_5.Top             = 123;
  Key_5.Width           = 80;
  Key_5.Height          = 38;
  Key_5.Pen_Width       = 1;
  Key_5.Pen_Color       = 0x0000;
  Key_5.Visible         = 1;
  Key_5.Active          = 1;
  Key_5.Transparent     = 1;
  Key_5.Caption         = Key_5_Caption;
  Key_5.TextAlign       = _taCenter;
  Key_5.TextAlignVertical= _tavMiddle;
  Key_5.FontName        = Tahoma26x33_Regular;
  Key_5.PressColEnabled = 1;
  Key_5.Font_Color      = 0x0000;
  Key_5.VerticalText    = 0;
  Key_5.Gradient        = 0;
  Key_5.Gradient_Orientation = 0;
  Key_5.Gradient_Start_Color = 0x4A69;
  Key_5.Gradient_End_Color = 0xC618;
  Key_5.Color           = 0xC618;
  Key_5.Press_Color     = 0xE71C;
  Key_5.Corner_Radius   = 3;
  Key_5.OnUpPtr         = 0;
  Key_5.OnDownPtr       = 0;
  Key_5.OnClickPtr      = Key_5Click;
  Key_5.OnPressPtr      = 0;

  Key_6.OwnerScreen     = &Keypad;
  Key_6.Order           = 6;
  Key_6.Left            = 161;
  Key_6.Top             = 123;
  Key_6.Width           = 80;
  Key_6.Height          = 38;
  Key_6.Pen_Width       = 1;
  Key_6.Pen_Color       = 0x0000;
  Key_6.Visible         = 1;
  Key_6.Active          = 1;
  Key_6.Transparent     = 1;
  Key_6.Caption         = Key_6_Caption;
  Key_6.TextAlign       = _taCenter;
  Key_6.TextAlignVertical= _tavMiddle;
  Key_6.FontName        = Tahoma26x33_Regular;
  Key_6.PressColEnabled = 1;
  Key_6.Font_Color      = 0x0000;
  Key_6.VerticalText    = 0;
  Key_6.Gradient        = 0;
  Key_6.Gradient_Orientation = 0;
  Key_6.Gradient_Start_Color = 0x4A69;
  Key_6.Gradient_End_Color = 0xC618;
  Key_6.Color           = 0xC618;
  Key_6.Press_Color     = 0xE71C;
  Key_6.Corner_Radius   = 3;
  Key_6.OnUpPtr         = 0;
  Key_6.OnDownPtr       = 0;
  Key_6.OnClickPtr      = Key_6Click;
  Key_6.OnPressPtr      = 0;

  Key_1.OwnerScreen     = &Keypad;
  Key_1.Order           = 7;
  Key_1.Left            = 2;
  Key_1.Top             = 84;
  Key_1.Width           = 80;
  Key_1.Height          = 38;
  Key_1.Pen_Width       = 1;
  Key_1.Pen_Color       = 0x0000;
  Key_1.Visible         = 1;
  Key_1.Active          = 1;
  Key_1.Transparent     = 1;
  Key_1.Caption         = Key_1_Caption;
  Key_1.TextAlign       = _taCenter;
  Key_1.TextAlignVertical= _tavMiddle;
  Key_1.FontName        = Tahoma26x33_Regular;
  Key_1.PressColEnabled = 1;
  Key_1.Font_Color      = 0x0000;
  Key_1.VerticalText    = 0;
  Key_1.Gradient        = 0;
  Key_1.Gradient_Orientation = 0;
  Key_1.Gradient_Start_Color = 0x4A69;
  Key_1.Gradient_End_Color = 0xC618;
  Key_1.Color           = 0xC618;
  Key_1.Press_Color     = 0xE71C;
  Key_1.Corner_Radius   = 3;
  Key_1.OnUpPtr         = 0;
  Key_1.OnDownPtr       = 0;
  Key_1.OnClickPtr      = Key_1click;
  Key_1.OnPressPtr      = 0;

  Key_2.OwnerScreen     = &Keypad;
  Key_2.Order           = 8;
  Key_2.Left            = 81;
  Key_2.Top             = 84;
  Key_2.Width           = 80;
  Key_2.Height          = 38;
  Key_2.Pen_Width       = 1;
  Key_2.Pen_Color       = 0x0000;
  Key_2.Visible         = 1;
  Key_2.Active          = 1;
  Key_2.Transparent     = 1;
  Key_2.Caption         = Key_2_Caption;
  Key_2.TextAlign       = _taCenter;
  Key_2.TextAlignVertical= _tavMiddle;
  Key_2.FontName        = Tahoma26x33_Regular;
  Key_2.PressColEnabled = 1;
  Key_2.Font_Color      = 0x0000;
  Key_2.VerticalText    = 0;
  Key_2.Gradient        = 0;
  Key_2.Gradient_Orientation = 0;
  Key_2.Gradient_Start_Color = 0x4A69;
  Key_2.Gradient_End_Color = 0xC618;
  Key_2.Color           = 0xC618;
  Key_2.Press_Color     = 0xE71C;
  Key_2.Corner_Radius   = 3;
  Key_2.OnUpPtr         = 0;
  Key_2.OnDownPtr       = 0;
  Key_2.OnClickPtr      = Key_2Click;
  Key_2.OnPressPtr      = 0;

  Key_3.OwnerScreen     = &Keypad;
  Key_3.Order           = 9;
  Key_3.Left            = 161;
  Key_3.Top             = 84;
  Key_3.Width           = 80;
  Key_3.Height          = 38;
  Key_3.Pen_Width       = 1;
  Key_3.Pen_Color       = 0x0000;
  Key_3.Visible         = 1;
  Key_3.Active          = 1;
  Key_3.Transparent     = 1;
  Key_3.Caption         = Key_3_Caption;
  Key_3.TextAlign       = _taCenter;
  Key_3.TextAlignVertical= _tavMiddle;
  Key_3.FontName        = Tahoma26x33_Regular;
  Key_3.PressColEnabled = 1;
  Key_3.Font_Color      = 0x0000;
  Key_3.VerticalText    = 0;
  Key_3.Gradient        = 0;
  Key_3.Gradient_Orientation = 0;
  Key_3.Gradient_Start_Color = 0x4A69;
  Key_3.Gradient_End_Color = 0xC618;
  Key_3.Color           = 0xC618;
  Key_3.Press_Color     = 0xE71C;
  Key_3.Corner_Radius   = 3;
  Key_3.OnUpPtr         = 0;
  Key_3.OnDownPtr       = 0;
  Key_3.OnClickPtr      = Key_3Click;
  Key_3.OnPressPtr      = 0;

  Key_dp.OwnerScreen     = &Keypad;
  Key_dp.Order           = 10;
  Key_dp.Left            = 1;
  Key_dp.Top             = 201;
  Key_dp.Width           = 80;
  Key_dp.Height          = 38;
  Key_dp.Pen_Width       = 1;
  Key_dp.Pen_Color       = 0x0000;
  Key_dp.Visible         = 1;
  Key_dp.Active          = 1;
  Key_dp.Transparent     = 1;
  Key_dp.Caption         = Key_dp_Caption;
  Key_dp.TextAlign       = _taCenter;
  Key_dp.TextAlignVertical= _tavMiddle;
  Key_dp.FontName        = Tahoma26x33_Regular;
  Key_dp.PressColEnabled = 1;
  Key_dp.Font_Color      = 0x0000;
  Key_dp.VerticalText    = 0;
  Key_dp.Gradient        = 0;
  Key_dp.Gradient_Orientation = 0;
  Key_dp.Gradient_Start_Color = 0x4A69;
  Key_dp.Gradient_End_Color = 0xC618;
  Key_dp.Color           = 0xC618;
  Key_dp.Press_Color     = 0xE71C;
  Key_dp.Corner_Radius   = 3;
  Key_dp.OnUpPtr         = 0;
  Key_dp.OnDownPtr       = 0;
  Key_dp.OnClickPtr      = Key_dpClick;
  Key_dp.OnPressPtr      = 0;

  Key_0.OwnerScreen     = &Keypad;
  Key_0.Order           = 11;
  Key_0.Left            = 81;
  Key_0.Top             = 201;
  Key_0.Width           = 80;
  Key_0.Height          = 38;
  Key_0.Pen_Width       = 1;
  Key_0.Pen_Color       = 0x0000;
  Key_0.Visible         = 1;
  Key_0.Active          = 1;
  Key_0.Transparent     = 1;
  Key_0.Caption         = Key_0_Caption;
  Key_0.TextAlign       = _taCenter;
  Key_0.TextAlignVertical= _tavMiddle;
  Key_0.FontName        = Tahoma26x33_Regular;
  Key_0.PressColEnabled = 1;
  Key_0.Font_Color      = 0x0000;
  Key_0.VerticalText    = 0;
  Key_0.Gradient        = 0;
  Key_0.Gradient_Orientation = 0;
  Key_0.Gradient_Start_Color = 0x4A69;
  Key_0.Gradient_End_Color = 0xC618;
  Key_0.Color           = 0xC618;
  Key_0.Press_Color     = 0xE71C;
  Key_0.Corner_Radius   = 3;
  Key_0.OnUpPtr         = 0;
  Key_0.OnDownPtr       = 0;
  Key_0.OnClickPtr      = Key_0Click;
  Key_0.OnPressPtr      = 0;

  Key_enter.OwnerScreen     = &Keypad;
  Key_enter.Order           = 12;
  Key_enter.Left            = 243;
  Key_enter.Top             = 85;
  Key_enter.Width           = 79;
  Key_enter.Height          = 154;
  Key_enter.Pen_Width       = 1;
  Key_enter.Pen_Color       = 0x0000;
  Key_enter.Visible         = 1;
  Key_enter.Active          = 1;
  Key_enter.Transparent     = 1;
  Key_enter.Caption         = Key_enter_Caption;
  Key_enter.TextAlign       = _taCenter;
  Key_enter.TextAlignVertical= _tavMiddle;
  Key_enter.FontName        = Tahoma29x29_Bold;
  Key_enter.PressColEnabled = 1;
  Key_enter.Font_Color      = 0x0000;
  Key_enter.VerticalText    = 0;
  Key_enter.Gradient        = 0;
  Key_enter.Gradient_Orientation = 0;
  Key_enter.Gradient_Start_Color = 0x4A69;
  Key_enter.Gradient_End_Color = 0x0400;
  Key_enter.Color           = 0x0400;
  Key_enter.Press_Color     = 0xE71C;
  Key_enter.Corner_Radius   = 3;
  Key_enter.OnUpPtr         = 0;
  Key_enter.OnDownPtr       = 0;
  Key_enter.OnClickPtr      = Key_enterClick;
  Key_enter.OnPressPtr      = 0;

  kp_display.OwnerScreen     = &Keypad;
  kp_display.Order           = 14;
  kp_display.Left            = 81;
  kp_display.Top             = 0;
  kp_display.Width           = 160;
  kp_display.Height          = 52;
  kp_display.Pen_Width       = 1;
  kp_display.Pen_Color       = 0x0000;
  kp_display.Visible         = 1;
  kp_display.Active          = 0;
  kp_display.Transparent     = 1;
  kp_display.Caption         = kp_display_Caption;
  kp_display.TextAlign       = _taRight;
  kp_display.TextAlignVertical= _tavMiddle;
  kp_display.FontName        = Tahoma34x42_Regular;
  kp_display.PressColEnabled = 1;
  kp_display.Font_Color      = 0x0000;
  kp_display.VerticalText    = 0;
  kp_display.Gradient        = 0;
  kp_display.Gradient_Orientation = 0;
  kp_display.Gradient_Start_Color = 0xFFFF;
  kp_display.Gradient_End_Color = 0xC618;
  kp_display.Color           = 0xFFFF;
  kp_display.Press_Color     = 0xE71C;
  kp_display.OnUpPtr         = 0;
  kp_display.OnDownPtr       = 0;
  kp_display.OnClickPtr      = 0;
  kp_display.OnPressPtr      = 0;

  pos_neg.OwnerScreen     = &Keypad;
  pos_neg.Order           = 16;
  pos_neg.Left            = 161;
  pos_neg.Top             = 201;
  pos_neg.Width           = 80;
  pos_neg.Height          = 38;
  pos_neg.Pen_Width       = 1;
  pos_neg.Pen_Color       = 0x0000;
  pos_neg.Visible         = 1;
  pos_neg.Active          = 1;
  pos_neg.Transparent     = 1;
  pos_neg.Caption         = pos_neg_Caption;
  pos_neg.TextAlign       = _taCenter;
  pos_neg.TextAlignVertical= _tavMiddle;
  pos_neg.FontName        = Tahoma26x33_Regular;
  pos_neg.PressColEnabled = 1;
  pos_neg.Font_Color      = 0x0000;
  pos_neg.VerticalText    = 0;
  pos_neg.Gradient        = 0;
  pos_neg.Gradient_Orientation = 0;
  pos_neg.Gradient_Start_Color = 0x4A69;
  pos_neg.Gradient_End_Color = 0xC618;
  pos_neg.Color           = 0xC618;
  pos_neg.Press_Color     = 0xE71C;
  pos_neg.Corner_Radius   = 3;
  pos_neg.OnUpPtr         = pos_negClick;
  pos_neg.OnDownPtr       = 0;
  pos_neg.OnClickPtr      = 0;
  pos_neg.OnPressPtr      = 0;

  lblKeyboardTitle.OwnerScreen     = &Keypad;
  lblKeyboardTitle.Order           = 17;  // need to keep orders in sync
  lblKeyboardTitle.Left            = 2;
  lblKeyboardTitle.Top             = 53;
  lblKeyboardTitle.Width           = 180;
  lblKeyboardTitle.Height          = 32;
  lblKeyboardTitle.Visible         = 1;
  lblKeyboardTitle.Active          = 1;
  lblKeyboardTitle.Caption         = 0;     // to be replaced with pointer to string later
  lblKeyboardTitle.FontName        = Tahoma23x29_Regular;
  lblKeyboardTitle.Font_Color      = 0xFFE0;
  lblKeyboardTitle.VerticalText    = 0;
  lblKeyboardTitle.OnUpPtr         = 0;
  lblKeyboardTitle.OnDownPtr       = 0;
  lblKeyboardTitle.OnClickPtr      = 0;
  lblKeyboardTitle.OnPressPtr      = 0;

  Diagram3_Label1.OwnerScreen     = &_main;
  Diagram3_Label1.Order           = 0;
  Diagram3_Label1.Left            = 285;
  Diagram3_Label1.Top             = 71;
  Diagram3_Label1.Width           = 21;
  Diagram3_Label1.Height          = 25;
  Diagram3_Label1.Visible         = 1;
  Diagram3_Label1.Active          = 1;
  Diagram3_Label1.Caption         = Diagram3_Label1_Caption;
  Diagram3_Label1.FontName        = Tahoma19x23_Regular;
  Diagram3_Label1.Font_Color      = 0xFFE0;
  Diagram3_Label1.VerticalText    = 0;
  Diagram3_Label1.OnUpPtr         = 0;
  Diagram3_Label1.OnDownPtr       = 0;
  Diagram3_Label1.OnClickPtr      = 0;
  Diagram3_Label1.OnPressPtr      = 0;

  indicator.OwnerScreen     = &_main;
  indicator.Order           = 1;
  indicator.Left            = 1;
  indicator.Top             = 1;
  indicator.Radius          = 17;
  indicator.Pen_Width       = 1;
  indicator.Pen_Color       = 0x0000;
  indicator.Visible         = 1;
  indicator.Active          = 1;
  indicator.Transparent     = 1;
  indicator.Gradient        = 0;
  indicator.Gradient_Orientation = 0;
  indicator.Gradient_Start_Color = 0;
  indicator.Gradient_End_Color = 0;
  indicator.Color           = 0xC618;
  indicator.PressColEnabled = 1;
  indicator.Press_Color     = 0xE71C;
  indicator.OnUpPtr         = 0;
  indicator.OnDownPtr       = 0;
  indicator.OnClickPtr      = 0;
  indicator.OnPressPtr      = 0;

  Screen_PV.OwnerScreen     = &_main;
  Screen_PV.Order           = 2;
  Screen_PV.Left            = 29;
  Screen_PV.Top             = 0;
  Screen_PV.Width           = 239;
  Screen_PV.Height          = 115;
  Screen_PV.Pen_Width       = 0;
  Screen_PV.Pen_Color       = 0x0000;
  Screen_PV.Visible         = 1;
  Screen_PV.Active          = 0;
  Screen_PV.Transparent     = 0;
  Screen_PV.Caption         = Screen_PV_Caption;
  Screen_PV.TextAlign       = _taRight;
  Screen_PV.TextAlignVertical= _tavMiddle;
  Screen_PV.FontName        = Tahoma83x103_Regular;
  Screen_PV.PressColEnabled = 1;
  Screen_PV.Font_Color      = 0x2986;
  Screen_PV.VerticalText    = 0;
  Screen_PV.Gradient        = 0;
  Screen_PV.Gradient_Orientation = 0;
  Screen_PV.Gradient_Start_Color = 0xFFFF;
  Screen_PV.Gradient_End_Color = 0xC618;
  Screen_PV.Color           = 0xC618;
  Screen_PV.Press_Color     = 0xE71C;
  Screen_PV.OnUpPtr         = 0;
  Screen_PV.OnDownPtr       = 0;
  Screen_PV.OnClickPtr      = 0;
  Screen_PV.OnPressPtr      = 0;

  Line2.OwnerScreen     = &_main;
  Line2.Order           = 3;
  Line2.First_Point_X   = 1;
  Line2.First_Point_Y   = 160;
  Line2.Second_Point_X  = 321;
  Line2.Second_Point_Y  = 160;
  Line2.Visible         = 1;
  Line2.Pen_Width       = 1;
  Line2.Color           = 0xFFFF;

  Main_Screen_settings.OwnerScreen     = &_main;
  Main_Screen_settings.Order           = 4;
  Main_Screen_settings.Left            = 1;
  Main_Screen_settings.Top             = 176;
  Main_Screen_settings.Width           = 103;
  Main_Screen_settings.Height          = 64;
  Main_Screen_settings.Pen_Width       = 1;
  Main_Screen_settings.Pen_Color       = 0x0000;
  Main_Screen_settings.Visible         = 1;
  Main_Screen_settings.Active          = 1;
  Main_Screen_settings.Transparent     = 1;
  Main_Screen_settings.Caption         = Main_Screen_settings_Caption;
  Main_Screen_settings.TextAlign       = _taCenter;
  Main_Screen_settings.TextAlignVertical= _tavMiddle;
  Main_Screen_settings.FontName        = Tahoma11x13_Regular;
  Main_Screen_settings.PressColEnabled = 0;
  Main_Screen_settings.Font_Color      = 0x0000;
  Main_Screen_settings.VerticalText    = 0;
  Main_Screen_settings.Gradient        = 0;
  Main_Screen_settings.Gradient_Orientation = 0;
  Main_Screen_settings.Gradient_Start_Color = 0xFFFF;
  Main_Screen_settings.Gradient_End_Color = 0xC618;
  Main_Screen_settings.Color           = 0xFFFF;
  Main_Screen_settings.Press_Color     = 0xE71C;
  Main_Screen_settings.Corner_Radius   = 5;
  Main_Screen_settings.OnUpPtr         = 0;
  Main_Screen_settings.OnDownPtr       = 0;
  Main_Screen_settings.OnClickPtr      = Main_Screen_settingsClick;
  Main_Screen_settings.OnPressPtr      = 0;

  Main_screen_settings_image.OwnerScreen     = &_main;
  Main_screen_settings_image.Order           = 5;
  Main_screen_settings_image.Left            = 33;
  Main_screen_settings_image.Top             = 187;
  Main_screen_settings_image.Width           = 40;
  Main_screen_settings_image.Height          = 39;
  Main_screen_settings_image.Picture_Type    = 0;
  Main_screen_settings_image.Picture_Ratio   = 1;
  Main_screen_settings_image.Picture_Name    = Settings_bmp;
  Main_screen_settings_image.Visible         = 1;
  Main_screen_settings_image.Active          = 0;
  Main_screen_settings_image.OnUpPtr         = 0;
  Main_screen_settings_image.OnDownPtr       = 0;
  Main_screen_settings_image.OnClickPtr      = 0;
  Main_screen_settings_image.OnPressPtr      = 0;

  Line1.OwnerScreen     = &_main;
  Line1.Order           = 6;
  Line1.First_Point_X   = 1;
  Line1.First_Point_Y   = 100;
  Line1.Second_Point_X  = 320;
  Line1.Second_Point_Y  = 100;
  Line1.Visible         = 1;
  Line1.Pen_Width       = 1;
  Line1.Color           = 0xFFFF;

  Main_Screen_start.OwnerScreen     = &_main;
  Main_Screen_start.Order           = 7;
  Main_Screen_start.Left            = 216;
  Main_Screen_start.Top             = 178;
  Main_Screen_start.Width           = 103;
  Main_Screen_start.Height          = 61;
  Main_Screen_start.Pen_Width       = 1;
  Main_Screen_start.Pen_Color       = 0x0000;
  Main_Screen_start.Visible         = 0;  // was 1 - CM 14/7/17
  Main_Screen_start.Active          = 0;  // was 1 - CM 04/8/17
  Main_Screen_start.Transparent     = 1;
  Main_Screen_start.Caption         = Main_Screen_start_Caption;
  Main_Screen_start.TextAlign       = _taCenter;
  Main_Screen_start.TextAlignVertical= _tavMiddle;
  Main_Screen_start.FontName        = Tahoma21x25_Regular;
  Main_Screen_start.PressColEnabled = 1;
  Main_Screen_start.Font_Color      = CL_Black;
  Main_Screen_start.VerticalText    = 0;
  Main_Screen_start.Gradient        = 0;
  Main_Screen_start.Gradient_Orientation = 0;
  Main_Screen_start.Gradient_Start_Color = 0xFFFF;
  Main_Screen_start.Gradient_End_Color = 0xFFFF;
  Main_Screen_start.Color           = CL_Red;      //0xFFFF
  Main_Screen_start.Press_Color     = 0xE71C;
  Main_Screen_start.Corner_Radius   = 5;
  Main_Screen_start.OnUpPtr         = 0;
  Main_Screen_start.OnDownPtr       = 0;
  Main_Screen_start.OnClickPtr      = Main_Screen_startClick;
  Main_Screen_start.OnPressPtr      = 0;

  start_stop.OwnerScreen     = &_main;
  start_stop.Order           = 8;
  start_stop.Left            = 151; //246
  start_stop.Top             = 186;  //186
  start_stop.Width           = 44;
  start_stop.Height          = 46;
  start_stop.Picture_Type    = 0;
  start_stop.Picture_Ratio   = 1;
  start_stop.Picture_Name    = powergreen_bmp;
  start_stop.Visible         = 0;  //1
  start_stop.Active          = 0;
  start_stop.OnUpPtr         = 0;
  start_stop.OnDownPtr       = 0;
  start_stop.OnClickPtr      = 0;
  start_stop.OnPressPtr      = 0;

  Diagram3_thermred.OwnerScreen     = &_main;
  Diagram3_thermred.Order           = 9;
  Diagram3_thermred.Left            = 280;
  Diagram3_thermred.Top             = 1;
  Diagram3_thermred.Width           = 40;
  Diagram3_thermred.Height          = 40;
  Diagram3_thermred.Picture_Type    = 0;
  Diagram3_thermred.Picture_Ratio   = 1;
  Diagram3_thermred.Picture_Name    = Thermometer_red_bmp;
  Diagram3_thermred.Visible         = 1;
  Diagram3_thermred.Active          = 1;
  Diagram3_thermred.OnUpPtr         = 0;
  Diagram3_thermred.OnDownPtr       = 0;
  Diagram3_thermred.OnClickPtr      = 0;
  Diagram3_thermred.OnPressPtr      = 0;

  Diagram3_thermBlack.OwnerScreen     = &_main;
  Diagram3_thermBlack.Order           = 10;
  Diagram3_thermBlack.Left            = 280;
  Diagram3_thermBlack.Top             = 1;
  Diagram3_thermBlack.Width           = 40;
  Diagram3_thermBlack.Height          = 40;
  Diagram3_thermBlack.Picture_Type    = 0;
  Diagram3_thermBlack.Picture_Ratio   = 1;
  Diagram3_thermBlack.Picture_Name    = Thermometer_black_bmp;
  Diagram3_thermBlack.Visible         = 1;
  Diagram3_thermBlack.Active          = 1;
  Diagram3_thermBlack.OnUpPtr         = 0;
  Diagram3_thermBlack.OnDownPtr       = 0;
  Diagram3_thermBlack.OnClickPtr      = 0;
  Diagram3_thermBlack.OnPressPtr      = 0;

  Label1.OwnerScreen     = &_main;
  Label1.Order           = 11;
  Label1.Left            = 0;
  Label1.Top             = 117;
  Label1.Width           = 84;
  Label1.Height          = 28;
  Label1.Visible         = 1;
  Label1.Active          = 1;
  Label1.Caption         = Label1_Caption;
  Label1.FontName        = Tahoma21x25_Regular;
  Label1.Font_Color      = 0x07FF;
  Label1.VerticalText    = 0;
  Label1.OnUpPtr         = 0;
  Label1.OnDownPtr       = 0;
  Label1.OnClickPtr      = 0;
  Label1.OnPressPtr      = 0;

  Label2.OwnerScreen     = &_main;
  Label2.Order           = 12;
  Label2.Left            = 90;
  Label2.Top             = 117;
  Label2.Width           = 236;
  Label2.Height          = 28;
  Label2.Visible         = 1;
  Label2.Active          = 1;
  Label2.Caption         = Label2_Caption;
  Label2.FontName        = Tahoma21x25_Regular;
  Label2.Font_Color      = 0x07FF;
  Label2.VerticalText    = 0;
  Label2.OnUpPtr         = 0;
  Label2.OnDownPtr       = 0;
  Label2.OnClickPtr      = 0;
  Label2.OnPressPtr      = 0;

  Settings_temp.OwnerScreen     = &Settings;
  Settings_temp.Order           = 0;
  Settings_temp.Left            = 0;
  Settings_temp.Top             = 0;
  Settings_temp.Width           = 106;
  Settings_temp.Height          = 120;
  Settings_temp.Pen_Width       = 1;
  Settings_temp.Pen_Color       = 0x0000;
  Settings_temp.Visible         = 1;
  Settings_temp.Active          = 1;
  Settings_temp.Transparent     = 1;
  Settings_temp.Caption         = Settings_temp_Caption;
  Settings_temp.TextAlign       = _taCenter;
  Settings_temp.TextAlignVertical= _tavMiddle;
  Settings_temp.FontName        = Tahoma34x42_Regular;
  Settings_temp.PressColEnabled = 0;
  Settings_temp.Font_Color      = 0x0000;
  Settings_temp.VerticalText    = 0;
  Settings_temp.Gradient        = 0;
  Settings_temp.Gradient_Orientation = 0;
  Settings_temp.Gradient_Start_Color = 0xC618;
  Settings_temp.Gradient_End_Color = 0xC618;
  Settings_temp.Color           = 0xFC00;
  Settings_temp.Press_Color     = 0xE71C;
  Settings_temp.Corner_Radius   = 5;
  Settings_temp.OnUpPtr         = 0;
  Settings_temp.OnDownPtr       = 0;
  Settings_temp.OnClickPtr      = Settings_LogClick;
  Settings_temp.OnPressPtr      = 0;

  Settings_PID.OwnerScreen     = &Settings;
  Settings_PID.Order           = 1;
  Settings_PID.Left            = 107;
  Settings_PID.Top             = 121;
  Settings_PID.Width           = 106;
  Settings_PID.Height          = 120;
  Settings_PID.Pen_Width       = 1;
  Settings_PID.Pen_Color       = 0x0000;
  Settings_PID.Visible         = 1;
  Settings_PID.Active          = 1;
  Settings_PID.Transparent     = 1;
  Settings_PID.Caption         = Settings_PID_Caption;
  Settings_PID.TextAlign       = _taCenter;
  Settings_PID.TextAlignVertical= _tavMiddle;
  Settings_PID.FontName        = Tahoma34x42_Regular;
  Settings_PID.PressColEnabled = 1;
  Settings_PID.Font_Color      = 0x0000;
  Settings_PID.VerticalText    = 0;
  Settings_PID.Gradient        = 0;
  Settings_PID.Gradient_Orientation = 0;
  Settings_PID.Gradient_Start_Color = 0xFFFF;
  Settings_PID.Gradient_End_Color = 0xC618;
  Settings_PID.Color           = 0x801F;
  Settings_PID.Press_Color     = 0xE71C;
  Settings_PID.Corner_Radius   = 5;
  Settings_PID.OnUpPtr         = 0;
  Settings_PID.OnDownPtr       = 0;
  Settings_PID.OnClickPtr      = Settings_PIDClick;
  Settings_PID.OnPressPtr      = 0;

  Settings_CAL.OwnerScreen     = &Settings;
  Settings_CAL.Order           = 2;
  Settings_CAL.Left            = 0;
  Settings_CAL.Top             = 121;
  Settings_CAL.Width           = 106;
  Settings_CAL.Height          = 120;
  Settings_CAL.Pen_Width       = 1;
  Settings_CAL.Pen_Color       = 0x0000;
  Settings_CAL.Visible         = 1;
  Settings_CAL.Active          = 1;
  Settings_CAL.Transparent     = 1;
  Settings_CAL.Caption         = Settings_CAL_Caption;
  Settings_CAL.TextAlign       = _taCenter;
  Settings_CAL.TextAlignVertical= _tavMiddle;
  Settings_CAL.FontName        = Tahoma34x42_Regular;
  Settings_CAL.PressColEnabled = 1;
  Settings_CAL.Font_Color      = 0x0000;
  Settings_CAL.VerticalText    = 0;
  Settings_CAL.Gradient        = 0;
  Settings_CAL.Gradient_Orientation = 0;
  Settings_CAL.Gradient_Start_Color = 0xFFFF;
  Settings_CAL.Gradient_End_Color = 0xC618;
  Settings_CAL.Color           = 0x0400;
  Settings_CAL.Press_Color     = 0xE71C;
  Settings_CAL.Corner_Radius   = 5;
  Settings_CAL.OnUpPtr         = 0;
  Settings_CAL.OnDownPtr       = 0;
  Settings_CAL.OnClickPtr      = Settings_CALClick;
  Settings_CAL.OnPressPtr      = 0;

  Settings_Exit.OwnerScreen     = &Settings;
  Settings_Exit.Order           = 3;
  Settings_Exit.Left            = 214;
  Settings_Exit.Top             = 121;
  Settings_Exit.Width           = 106;
  Settings_Exit.Height          = 120;
  Settings_Exit.Pen_Width       = 1;
  Settings_Exit.Pen_Color       = 0x0000;
  Settings_Exit.Visible         = 1;
  Settings_Exit.Active          = 1;
  Settings_Exit.Transparent     = 1;
  Settings_Exit.Caption         = Settings_Exit_Caption;
  Settings_Exit.TextAlign       = _taCenter;
  Settings_Exit.TextAlignVertical= _tavMiddle;
  Settings_Exit.FontName        = Tahoma34x42_Regular;
  Settings_Exit.PressColEnabled = 1;
  Settings_Exit.Font_Color      = 0x0000;
  Settings_Exit.VerticalText    = 0;
  Settings_Exit.Gradient        = 0;
  Settings_Exit.Gradient_Orientation = 0;
  Settings_Exit.Gradient_Start_Color = 0xFFFF;
  Settings_Exit.Gradient_End_Color = 0xC618;
  Settings_Exit.Color           = 0xF800;
  Settings_Exit.Press_Color     = 0xE71C;
  Settings_Exit.Corner_Radius   = 5;
  Settings_Exit.OnUpPtr         = 0;
  Settings_Exit.OnDownPtr       = 0;
  Settings_Exit.OnClickPtr      = Settings_ExitClick;
  Settings_Exit.OnPressPtr      = 0;

  ToolBox.OwnerScreen     = &Settings;
  ToolBox.Order           = 4;
  ToolBox.Left            = 214;
  ToolBox.Top             = 0;
  ToolBox.Width           = 106;
  ToolBox.Height          = 120;
  ToolBox.Pen_Width       = 1;
  ToolBox.Pen_Color       = 0x0000;
  ToolBox.Visible         = 1;
  ToolBox.Active          = 1;
  ToolBox.Transparent     = 1;
  ToolBox.Caption         = ToolBox_Caption;
  ToolBox.TextAlign       = _taCenter;
  ToolBox.TextAlignVertical= _tavMiddle;
  ToolBox.FontName        = Tahoma34x42_Regular;
  ToolBox.PressColEnabled = 0;
  ToolBox.Font_Color      = 0x0000;
  ToolBox.VerticalText    = 0;
  ToolBox.Gradient        = 0;
  ToolBox.Gradient_Orientation = 0;
  ToolBox.Gradient_Start_Color = 0xC618;
  ToolBox.Gradient_End_Color = 0xC618;
  ToolBox.Color           = 0x041F;
  ToolBox.Press_Color     = 0xE71C;
  ToolBox.Corner_Radius   = 5;
  ToolBox.OnUpPtr         = 0;
  ToolBox.OnDownPtr       = 0;
  ToolBox.OnClickPtr      = Diag_Clicked;
  ToolBox.OnPressPtr      = 0;
  
  Settings_drain.OwnerScreen     = &Settings;
  Settings_drain.Order           = 5;
  Settings_drain.Left            = 107;
  Settings_drain.Top             = 0;
  Settings_drain.Width           = 106;
  Settings_drain.Height          = 120;
  Settings_drain.Pen_Width       = 1;
  Settings_drain.Pen_Color       = 0x0000;
  Settings_drain.Visible         = 1;
  Settings_drain.Active          = 1;
  Settings_drain.Transparent     = 1;
  Settings_drain.Caption         = Settings_drain_Caption;
  Settings_drain.TextAlign       = _taCenter;
  Settings_drain.TextAlignVertical= _tavMiddle;
  Settings_drain.FontName        = Tahoma26x33_Regular;
  Settings_drain.PressColEnabled = 1;
  Settings_drain.Font_Color      = 0x0000;
  Settings_drain.VerticalText    = 0;
  Settings_drain.Gradient        = 0;
  Settings_drain.Gradient_Orientation = 0;
  Settings_drain.Gradient_Start_Color = 0xFFFF;
  Settings_drain.Gradient_End_Color = 0xC618;
  Settings_drain.Color           = 0xC618;
  Settings_drain.Press_Color     = 0xE71C;
  Settings_drain.Corner_Radius   = 5;
  Settings_drain.OnUpPtr         = 0;
  Settings_drain.OnDownPtr       = 0;
  Settings_drain.OnClickPtr      = Config_Clicked;
  Settings_drain.OnPressPtr      = 0;

  P_value.OwnerScreen     = &PID;
  P_value.Order           = 1;
  P_value.Left            = 86; //112;
  P_value.Top             = 3;
  P_value.Width           = 149; //101;
  P_value.Height          = 77;
  P_value.Pen_Width       = 1;
  P_value.Pen_Color       = 0x0000;
  P_value.Visible         = 1;
  P_value.Active          = 0;
  P_value.Transparent     = 0;
  P_value.Caption         = P_value_Caption;
  P_value.TextAlign       = _taRight;
  P_value.TextAlignVertical= _tavMiddle;
  P_value.FontName        = Tahoma55x68_Regular;
  P_value.PressColEnabled = 1;
  P_value.Font_Color      = 0xFFE0;
  P_value.VerticalText    = 0;
  P_value.Gradient        = 0;
  P_value.Gradient_Orientation = 0;
  P_value.Gradient_Start_Color = 0xFFFF;
  P_value.Gradient_End_Color = 0xC618;
  P_value.Color           = 0xC618;
  P_value.Press_Color     = 0xE71C;
  P_value.Corner_Radius   = 3;
  P_value.OnUpPtr         = 0;
  P_value.OnDownPtr       = 0;
  P_value.OnClickPtr      = 0;
  P_value.OnPressPtr      = 0;

  I_value.OwnerScreen     = &PID;
  I_value.Order           = 2;
  I_value.Left            = 86; //125;
  I_value.Top             = 84;
  I_value.Width           = 149; //101;
  I_value.Height          = 77;
  I_value.Pen_Width       = 1;
  I_value.Pen_Color       = 0x0000;
  I_value.Visible         = 1;
  I_value.Active          = 0;
  I_value.Transparent     = 0;
  I_value.Caption         = I_value_Caption;
  I_value.TextAlign       = _taRight;
  I_value.TextAlignVertical= _tavMiddle;
  I_value.FontName        = Tahoma55x68_Regular;
  I_value.PressColEnabled = 1;
  I_value.Font_Color      = 0xFFE0;
  I_value.VerticalText    = 0;
  I_value.Gradient        = 0;
  I_value.Gradient_Orientation = 0;
  I_value.Gradient_Start_Color = 0xFFFF;
  I_value.Gradient_End_Color = 0xC618;
  I_value.Color           = 0xC618;
  I_value.Press_Color     = 0xE71C;
  I_value.Corner_Radius   = 3;
  I_value.OnUpPtr         = 0;
  I_value.OnDownPtr       = 0;
  I_value.OnClickPtr      = 0;
  I_value.OnPressPtr      = 0;

  D_value.OwnerScreen     = &PID;
  D_value.Order           = 3;
  D_value.Left            = 86; //117;
  D_value.Top             = 162;
  D_value.Width           = 149; //118;
  D_value.Height          = 77;
  D_value.Pen_Width       = 1;
  D_value.Pen_Color       = 0x0000;
  D_value.Visible         = 1;
  D_value.Active          = 0;
  D_value.Transparent     = 0;
  D_value.Caption         = D_value_Caption;
  D_value.TextAlign       = _taRight;
  D_value.TextAlignVertical= _tavMiddle;
  D_value.FontName        = Tahoma55x68_Regular;
  D_value.PressColEnabled = 1;
  D_value.Font_Color      = 0xFFE0;
  D_value.VerticalText    = 0;
  D_value.Gradient        = 0;
  D_value.Gradient_Orientation = 0;
  D_value.Gradient_Start_Color = 0xFFFF;
  D_value.Gradient_End_Color = 0xC618;
  D_value.Color           = 0xC618;
  D_value.Press_Color     = 0xE71C;
  D_value.Corner_Radius   = 3;
  D_value.OnUpPtr         = 0;
  D_value.OnDownPtr       = 0;
  D_value.OnClickPtr      = 0;
  D_value.OnPressPtr      = 0;

  I_button.OwnerScreen     = &PID;
  I_button.Order           = 4;
  I_button.Left            = 7;
  I_button.Top             = 83;
  I_button.Width           = 74;
  I_button.Height          = 70;
  I_button.Pen_Width       = 2;
  I_button.Pen_Color       = 0xFFFF;
  I_button.Visible         = 1;
  I_button.Active          = 1;
  I_button.Transparent     = 1;
  I_button.Caption         = I_button_Caption;
  I_button.TextAlign       = _taCenter;
  I_button.TextAlignVertical= _tavMiddle;
  I_button.FontName        = Tahoma50x62_Regular;
  I_button.PressColEnabled = 1;
  I_button.Font_Color      = 0x0000;
  I_button.VerticalText    = 0;
  I_button.Gradient        = 0;
  I_button.Gradient_Orientation = 0;
  I_button.Gradient_Start_Color = 0xFFFF;
  I_button.Gradient_End_Color = 0xC618;
  I_button.Color           = 0x87FF;
  I_button.Press_Color     = 0xE71C;
  I_button.Corner_Radius   = 5;
  I_button.OnUpPtr         = 0;
  I_button.OnDownPtr       = 0;
  I_button.OnClickPtr      = I_buttonClick;
  I_button.OnPressPtr      = 0;

  D_button.OwnerScreen     = &PID;
  D_button.Order           = 5;
  D_button.Left            = 6;
  D_button.Top             = 163;
  D_button.Width           = 74;
  D_button.Height          = 70;
  D_button.Pen_Width       = 2;
  D_button.Pen_Color       = 0xFFFF;
  D_button.Visible         = 1;
  D_button.Active          = 1;
  D_button.Transparent     = 1;
  D_button.Caption         = D_button_Caption;
  D_button.TextAlign       = _taCenter;
  D_button.TextAlignVertical= _tavMiddle;
  D_button.FontName        = Tahoma50x62_Regular;
  D_button.PressColEnabled = 1;
  D_button.Font_Color      = 0x0000;
  D_button.VerticalText    = 0;
  D_button.Gradient        = 0;
  D_button.Gradient_Orientation = 0;
  D_button.Gradient_Start_Color = 0xFFFF;
  D_button.Gradient_End_Color = 0xC618;
  D_button.Color           = 0xFC08;
  D_button.Press_Color     = 0xE71C;
  D_button.Corner_Radius   = 5;
  D_button.OnUpPtr         = 0;
  D_button.OnDownPtr       = 0;
  D_button.OnClickPtr      = D_buttonClick;
  D_button.OnPressPtr      = 0;

  PID_conf_button.OwnerScreen     = &PID;
  PID_conf_button.Order           = 6;
  PID_conf_button.Left            = 239;
  PID_conf_button.Top             = 163;
  PID_conf_button.Width           = 74;
  PID_conf_button.Height          = 70;
  PID_conf_button.Pen_Width       = 2;
  PID_conf_button.Pen_Color       = 0xFFFF;
  PID_conf_button.Visible         = 1;
  PID_conf_button.Active          = 1;
  PID_conf_button.Transparent     = 1;
  PID_conf_button.Caption         = PID_conf_button_Caption;
  PID_conf_button.TextAlign       = _taCenter;
  PID_conf_button.TextAlignVertical= _tavMiddle;
  PID_conf_button.FontName        = Tahoma42x52_Regular;
  PID_conf_button.PressColEnabled = 1;
  PID_conf_button.Font_Color      = 0x0000;
  PID_conf_button.VerticalText    = 0;
  PID_conf_button.Gradient        = 0;
  PID_conf_button.Gradient_Orientation = 0;
  PID_conf_button.Gradient_Start_Color = 0xFFFF;
  PID_conf_button.Gradient_End_Color = 0xC618;
  PID_conf_button.Color           = 0x0400;
  PID_conf_button.Press_Color     = 0xE71C;
  PID_conf_button.Corner_Radius   = 5;
  PID_conf_button.OnUpPtr         = 0;
  PID_conf_button.OnDownPtr       = 0;
  PID_conf_button.OnClickPtr      = PID_conf_buttonClick;
  PID_conf_button.OnPressPtr      = 0;

  PID_ret_button.OwnerScreen     = &PID;
  PID_ret_button.Order           = 7;
  PID_ret_button.Left            = 239;
  PID_ret_button.Top             = 3;
  PID_ret_button.Width           = 74;
  PID_ret_button.Height          = 70;
  PID_ret_button.Pen_Width       = 2;
  PID_ret_button.Pen_Color       = 0xFFFF;
  PID_ret_button.Visible         = 1;
  PID_ret_button.Active          = 1;
  PID_ret_button.Transparent     = 1;
  PID_ret_button.Caption         = PID_ret_button_Caption;
  PID_ret_button.TextAlign       = _taCenter;
  PID_ret_button.TextAlignVertical= _tavMiddle;
  PID_ret_button.FontName        = Tahoma42x52_Regular;
  PID_ret_button.PressColEnabled = 1;
  PID_ret_button.Font_Color      = 0x0000;
  PID_ret_button.VerticalText    = 0;
  PID_ret_button.Gradient        = 0;
  PID_ret_button.Gradient_Orientation = 0;
  PID_ret_button.Gradient_Start_Color = 0xFFFF;
  PID_ret_button.Gradient_End_Color = 0xC618;
  PID_ret_button.Color           = 0xF800;
  PID_ret_button.Press_Color     = 0xE71C;
  PID_ret_button.Corner_Radius   = 5;
  PID_ret_button.OnUpPtr         = 0;
  PID_ret_button.OnDownPtr       = 0;
  PID_ret_button.OnClickPtr      = PID_ret_buttonClick;   // was PID_conf_buttonClick - CM 10/07/17
  PID_ret_button.OnPressPtr      = 0;

  Image9.OwnerScreen     = &PID;
  Image9.Order           = 8;
  Image9.Left            = 257;
  Image9.Top             = 16;
  Image9.Width           = 40;
  Image9.Height          = 42;
  Image9.Picture_Type    = 0;
  Image9.Picture_Ratio   = 1;
  Image9.Picture_Name    = returnsarrow_bmp;
  Image9.Visible         = 1;
  Image9.Active          = 0;
  Image9.OnUpPtr         = 0;
  Image9.OnDownPtr       = 0;
  Image9.OnClickPtr      = 0;
  Image9.OnPressPtr      = 0;

  cal_offset_value.OwnerScreen     = &Calibration;
  cal_offset_value.Order           = 1;
  cal_offset_value.Left            = 97; //126; //114
  cal_offset_value.Width           = 266; //101;
  cal_offset_value.TextAlign       = _taLeft; //_taRight;
  cal_offset_value.Top             = 4; //28;
  cal_offset_value.Height          = 77;
  cal_offset_value.Pen_Width       = 1;
  cal_offset_value.Pen_Color       = 0x0000;
  cal_offset_value.Visible         = 1;
  cal_offset_value.Active          = 0;
  cal_offset_value.Transparent     = 0;
  cal_offset_value.Caption         = cal_offset_value_Caption;
  cal_offset_value.TextAlignVertical= _tavMiddle;
  cal_offset_value.FontName        = Tahoma55x68_Regular;
  cal_offset_value.PressColEnabled = 1;
  cal_offset_value.Font_Color      = 0xFFE0;
  cal_offset_value.VerticalText    = 0;
  cal_offset_value.Gradient        = 0;
  cal_offset_value.Gradient_Orientation = 0;
  cal_offset_value.Gradient_Start_Color = 0xFFFF;
  cal_offset_value.Gradient_End_Color = 0xC618;
  cal_offset_value.Color           = 0xC618;
  cal_offset_value.Press_Color     = 0xE71C;
  cal_offset_value.Corner_Radius   = 3;
  cal_offset_value.OnUpPtr         = 0;
  cal_offset_value.OnDownPtr       = 0;
  cal_offset_value.OnClickPtr      = 0;
  cal_offset_value.OnPressPtr      = 0;

  cal_factor_value.OwnerScreen     = &Calibration;
  cal_factor_value.Order           = 2;
  cal_factor_value.Left            = 97; //126; //114
  cal_factor_value.Width           = 266; //101;
  cal_factor_value.TextAlign       = _taLeft; //_taRight;
  cal_factor_value.Top             = 90; //115;
  cal_factor_value.Height          = 77;
  cal_factor_value.Pen_Width       = 1;
  cal_factor_value.Pen_Color       = 0x0000;
  cal_factor_value.Visible         = 1;
  cal_factor_value.Active          = 0;
  cal_factor_value.Transparent     = 0;
  cal_factor_value.Caption         = cal_factor_value_Caption;
  cal_factor_value.TextAlignVertical= _tavMiddle;
  cal_factor_value.FontName        = Tahoma55x68_Regular;
  cal_factor_value.PressColEnabled = 1;
  cal_factor_value.Font_Color      = 0xFFE0;
  cal_factor_value.VerticalText    = 0;
  cal_factor_value.Gradient        = 0;
  cal_factor_value.Gradient_Orientation = 0;
  cal_factor_value.Gradient_Start_Color = 0xFFFF;
  cal_factor_value.Gradient_End_Color = 0xC618;
  cal_factor_value.Color           = 0xC618;
  cal_factor_value.Press_Color     = 0xE71C;
  cal_factor_value.Corner_Radius   = 3;
  cal_factor_value.OnUpPtr         = 0;
  cal_factor_value.OnDownPtr       = 0;
  cal_factor_value.OnClickPtr      = 0;
  cal_factor_value.OnPressPtr      = 0;

  cal_fact_button.OwnerScreen     = &Calibration;
  cal_fact_button.Order           = 3;
  cal_fact_button.Left            = 5;
  cal_fact_button.Top             = 90; //117;
  cal_fact_button.Width           = 95;
  cal_fact_button.Height          = 77;
  cal_fact_button.Pen_Width       = 2;
  cal_fact_button.Pen_Color       = 0xFFFF;
  cal_fact_button.Visible         = 1;
  cal_fact_button.Active          = 1;
  cal_fact_button.Transparent     = 1;
  cal_fact_button.Caption         = cal_fact_button_Caption;
  cal_fact_button.TextAlign       = _taCenter;
  cal_fact_button.TextAlignVertical= _tavMiddle;
  cal_fact_button.FontName        = Tahoma16x19_Regular;
  cal_fact_button.PressColEnabled = 1;
  cal_fact_button.Font_Color      = 0xFFFF;
  cal_fact_button.VerticalText    = 0;
  cal_fact_button.Gradient        = 0;
  cal_fact_button.Gradient_Orientation = 0;
  cal_fact_button.Gradient_Start_Color = 0xFFFF;
  cal_fact_button.Gradient_End_Color = 0xC618;
  cal_fact_button.Color           = 0x001F;
  cal_fact_button.Press_Color     = 0xE71C;
  cal_fact_button.Corner_Radius   = 5;
  cal_fact_button.OnUpPtr         = 0;
  cal_fact_button.OnDownPtr       = 0;
  cal_fact_button.OnClickPtr      = cal_fact_buttonClick;
  cal_fact_button.OnPressPtr      = 0;

  cal_confirm_button.OwnerScreen     = &Calibration;
  cal_confirm_button.Order           = 4;
  cal_confirm_button.Left            = 227;  //222
  cal_confirm_button.Top             = 157;
  cal_confirm_button.Width           = 90;  //95
  cal_confirm_button.Height          = 77;
  cal_confirm_button.Pen_Width       = 2;
  cal_confirm_button.Pen_Color       = 0xFFFF;
  cal_confirm_button.Visible         = 1;
  cal_confirm_button.Active          = 1;
  cal_confirm_button.Transparent     = 1;
  cal_confirm_button.Caption         = cal_confirm_button_Caption;
  cal_confirm_button.TextAlign       = _taCenter;
  cal_confirm_button.TextAlignVertical= _tavMiddle;
  cal_confirm_button.FontName        = Tahoma42x52_Regular;
  cal_confirm_button.PressColEnabled = 1;
  cal_confirm_button.Font_Color      = 0x0000;
  cal_confirm_button.VerticalText    = 0;
  cal_confirm_button.Gradient        = 0;
  cal_confirm_button.Gradient_Orientation = 0;
  cal_confirm_button.Gradient_Start_Color = 0xFFFF;
  cal_confirm_button.Gradient_End_Color = 0xC618;
  cal_confirm_button.Color           = 0x0400;
  cal_confirm_button.Press_Color     = 0xE71C;
  cal_confirm_button.Corner_Radius   = 5;
  cal_confirm_button.OnUpPtr         = 0;
  cal_confirm_button.OnDownPtr       = 0;
  cal_confirm_button.OnClickPtr      = cal_confirm_buttonClick;
  cal_confirm_button.OnPressPtr      = 0;

  cal_ret_button.OwnerScreen     = &Calibration;
  cal_ret_button.Order           = 5;
  cal_ret_button.Left            = 227;  //222
  cal_ret_button.Top             = 4;
  cal_ret_button.Width           = 90;  //95
  cal_ret_button.Height          = 77;
  cal_ret_button.Pen_Width       = 2;
  cal_ret_button.Pen_Color       = 0xFFFF;
  cal_ret_button.Visible         = 1;
  cal_ret_button.Active          = 1;
  cal_ret_button.Transparent     = 1;
  cal_ret_button.Caption         = cal_ret_button_Caption;
  cal_ret_button.TextAlign       = _taCenter;
  cal_ret_button.TextAlignVertical= _tavMiddle;
  cal_ret_button.FontName        = Tahoma42x52_Regular;
  cal_ret_button.PressColEnabled = 1;
  cal_ret_button.Font_Color      = 0x0000;
  cal_ret_button.VerticalText    = 0;
  cal_ret_button.Gradient        = 0;
  cal_ret_button.Gradient_Orientation = 0;
  cal_ret_button.Gradient_Start_Color = 0xFFFF;
  cal_ret_button.Gradient_End_Color = 0xC618;
  cal_ret_button.Color           = 0xF800;
  cal_ret_button.Press_Color     = 0xE71C;
  cal_ret_button.Corner_Radius   = 5;
  cal_ret_button.OnUpPtr         = 0;
  cal_ret_button.OnDownPtr       = 0;
  cal_ret_button.OnClickPtr      = cal_ret_buttonClick;
  cal_ret_button.OnPressPtr      = 0;

  Image10.OwnerScreen     = &Calibration;
  Image10.Order           = 6;
  Image10.Left            = 250;
  Image10.Top             = 21;
  Image10.Width           = 40;
  Image10.Height          = 42;
  Image10.Picture_Type    = 0;
  Image10.Picture_Ratio   = 1;
  Image10.Picture_Name    = returnsarrow_bmp;
  Image10.Visible         = 1;
  Image10.Active          = 0;
  Image10.OnUpPtr         = 0;
  Image10.OnDownPtr       = 0;
  Image10.OnClickPtr      = cal_ret_buttonClick;
  Image10.OnPressPtr      = 0;

  cal_start_button.OwnerScreen     = &Calibration;
  cal_start_button.Order           = 7;
  cal_start_button.Left            = 8;
  cal_start_button.Top             = 178;
  cal_start_button.Width           = 175;
  cal_start_button.Height          = 53;
  cal_start_button.Pen_Width       = 2;
  cal_start_button.Pen_Color       = 0xFFFF;
  cal_start_button.Visible         = 1;
  cal_start_button.Active          = 1;
  cal_start_button.Transparent     = 1;
  cal_start_button.Caption         = cal_start_button_Caption;
  cal_start_button.TextAlign       = _taCenter;
  cal_start_button.TextAlignVertical= _tavMiddle;
  cal_start_button.FontName        = Tahoma16x19_Regular;
  cal_start_button.PressColEnabled = 1;
  cal_start_button.Font_Color      = 0x0000;
  cal_start_button.VerticalText    = 0;
  cal_start_button.Gradient        = 0;
  cal_start_button.Gradient_Orientation = 0;
  cal_start_button.Gradient_Start_Color = 0xFFFF;
  cal_start_button.Gradient_End_Color = 0xC618;
  cal_start_button.Color           = 0xFC00;
  cal_start_button.Press_Color     = 0xE71C;
  cal_start_button.Corner_Radius   = 3;
  cal_start_button.OnUpPtr         = 0;
  cal_start_button.OnDownPtr       = 0;
  cal_start_button.OnClickPtr      = cal_start_buttonClick;
  cal_start_button.OnPressPtr      = 0;

  cal_message_label.OwnerScreen     = &Calibration;
  cal_message_label.Order           = 8;
  cal_message_label.Left            = 15;
  cal_message_label.Top             = 84;
  cal_message_label.Width           = 217;
  cal_message_label.Height          = 17;
  cal_message_label.Visible         = 0;
  cal_message_label.Active          = 1;
  cal_message_label.Caption         = cal_message_label_Caption;
  cal_message_label.FontName        = Tahoma12x16_Regular;
  cal_message_label.Font_Color      = 0xFFFF;
  cal_message_label.VerticalText    = 0;
  cal_message_label.OnUpPtr         = 0;
  cal_message_label.OnDownPtr       = 0;
  cal_message_label.OnClickPtr      = 0;
  cal_message_label.OnPressPtr      = 0;
  
  cal_temp_label.OwnerScreen     = &Calibration;
  cal_temp_label.Order           = 9;
  cal_temp_label.Left            = 244;
  cal_temp_label.Top             = 77;
  cal_temp_label.Width           = 38;
  cal_temp_label.Height          = 25;
  cal_temp_label.Visible         = 0;
  cal_temp_label.Active          = 0;
  cal_temp_label.Caption         = cal_temp_label_Caption;
  cal_temp_label.FontName        = Tahoma19x23_Regular;
  cal_temp_label.Font_Color      = 0xFFFF;
  cal_temp_label.VerticalText    = 0;
  cal_temp_label.OnUpPtr         = 0;
  cal_temp_label.OnDownPtr       = 0;
  cal_temp_label.OnClickPtr      = 0;
  cal_temp_label.OnPressPtr      = 0;
  
  Line3.OwnerScreen     = &Diags;
  Line3.Order           = 0;
  Line3.First_Point_X   = 160;
  Line3.First_Point_Y   = 0;
  Line3.Second_Point_X  = 160;
  Line3.Second_Point_Y  = 242;
  Line3.Visible         = 1;
  Line3.Pen_Width       = 3;
  Line3.Color           = 0x0273;

  Lbl_inputs.OwnerScreen     = &Diags;
  Lbl_inputs.Order           = 1;
  Lbl_inputs.Left            = 54;
  Lbl_inputs.Top             = 1;
  Lbl_inputs.Width           = 72;
  Lbl_inputs.Height          = 28;
  Lbl_inputs.Visible         = 1;
  Lbl_inputs.Active          = 1;
  Lbl_inputs.Caption         = Lbl_inputs_Caption;
  Lbl_inputs.FontName        = Tahoma25x25_Bold;
  Lbl_inputs.Font_Color      = 0xFFFF;
  Lbl_inputs.VerticalText    = 0;
  Lbl_inputs.OnUpPtr         = 0;
  Lbl_inputs.OnDownPtr       = 0;
  Lbl_inputs.OnClickPtr      = 0;
  Lbl_inputs.OnPressPtr      = 0;

  Lbl_outputs.OwnerScreen     = &Diags;
  Lbl_outputs.Order           = 2;
  Lbl_outputs.Left            = 206;
  Lbl_outputs.Top             = 1;
  Lbl_outputs.Width           = 87;
  Lbl_outputs.Height          = 28;
  Lbl_outputs.Visible         = 1;
  Lbl_outputs.Active          = 1;
  Lbl_outputs.Caption         = Lbl_outputs_Caption;
  Lbl_outputs.FontName        = Tahoma25x25_Bold;
  Lbl_outputs.Font_Color      = 0xFFFF;
  Lbl_outputs.VerticalText    = 0;
  Lbl_outputs.OnUpPtr         = 0;
  Lbl_outputs.OnDownPtr       = 0;
  Lbl_outputs.OnClickPtr      = 0;
  Lbl_outputs.OnPressPtr      = 0;

  Circle1.OwnerScreen     = &Diags;
  Circle1.Order           = 3;
  Circle1.Left            = 8;
  Circle1.Top             = 34;
  Circle1.Radius          = 12;
  Circle1.Pen_Width       = 1;
  Circle1.Pen_Color       = 0x0273;
  Circle1.Visible         = 1;
  Circle1.Active          = 1;
  Circle1.Transparent     = 1;
  Circle1.Gradient        = 1;
  Circle1.Gradient_Orientation = 0;
  Circle1.Gradient_Start_Color = 0xFFFF;
  Circle1.Gradient_End_Color = 0xC618;
  Circle1.Color           = 0xC618;
  Circle1.PressColEnabled = 1;
  Circle1.Press_Color     = 0xE71C;
  Circle1.OnUpPtr         = 0;
  Circle1.OnDownPtr       = 0;
  Circle1.OnClickPtr      = 0;
  Circle1.OnPressPtr      = 0;

  Lbl_water.OwnerScreen     = &Diags;
  Lbl_water.Order           = 4;
  Lbl_water.Left            = 38;
  Lbl_water.Top             = 35;
  Lbl_water.Width           = 59;
  Lbl_water.Height          = 21;
  Lbl_water.Visible         = 1;
  Lbl_water.Active          = 1;
  Lbl_water.Caption         = Lbl_water_Caption;
  Lbl_water.FontName        = Tahoma16x19_Regular;
  Lbl_water.Font_Color      = 0xFFFF;
  Lbl_water.VerticalText    = 0;
  Lbl_water.OnUpPtr         = 0;
  Lbl_water.OnDownPtr       = 0;
  Lbl_water.OnClickPtr      = 0;
  Lbl_water.OnPressPtr      = 0;

  Circle2.OwnerScreen     = &Diags;
  Circle2.Order           = 5;
  Circle2.Left            = 8;
  Circle2.Top             = 67;
  Circle2.Radius          = 12;
  Circle2.Pen_Width       = 1;
  Circle2.Pen_Color       = 0x0273;
  Circle2.Visible         = 1;
  Circle2.Active          = 1;
  Circle2.Transparent     = 1;
  Circle2.Gradient        = 1;
  Circle2.Gradient_Orientation = 0;
  Circle2.Gradient_Start_Color = 0xFFFF;
  Circle2.Gradient_End_Color = 0xC618;
  Circle2.Color           = 0xC618;
  Circle2.PressColEnabled = 1;
  Circle2.Press_Color     = 0xE71C;
  Circle2.OnUpPtr         = 0;
  Circle2.OnDownPtr       = 0;
  Circle2.OnClickPtr      = 0;
  Circle2.OnPressPtr      = 0;

  Label3.OwnerScreen     = &Diags;
  Label3.Order           = 6;
  Label3.Left            = 38;
  Label3.Top             = 68;
  Label3.Width           = 35;
  Label3.Height          = 21;
  Label3.Visible         = 1;
  Label3.Active          = 1;
  Label3.Caption         = Label3_Caption;
  Label3.FontName        = Tahoma16x19_Regular;
  Label3.Font_Color      = 0xFFFF;
  Label3.VerticalText    = 0;
  Label3.OnUpPtr         = 0;
  Label3.OnDownPtr       = 0;
  Label3.OnClickPtr      = 0;
  Label3.OnPressPtr      = 0;

  Circle3.OwnerScreen     = &Diags;
  Circle3.Order           = 7;
  Circle3.Left            = 8;
  Circle3.Top             = 101;
  Circle3.Radius          = 12;
  Circle3.Pen_Width       = 1;
  Circle3.Pen_Color       = 0x0273;
  Circle3.Visible         = 1;
  Circle3.Active          = 1;
  Circle3.Transparent     = 1;
  Circle3.Gradient        = 1;
  Circle3.Gradient_Orientation = 0;
  Circle3.Gradient_Start_Color = 0xFFFF;
  Circle3.Gradient_End_Color = 0xC618;
  Circle3.Color           = 0xC618;
  Circle3.PressColEnabled = 1;
  Circle3.Press_Color     = 0xE71C;
  Circle3.OnUpPtr         = 0;
  Circle3.OnDownPtr       = 0;
  Circle3.OnClickPtr      = 0;
  Circle3.OnPressPtr      = 0;

  Label4.OwnerScreen     = &Diags;
  Label4.Order           = 8;
  Label4.Left            = 38;
  Label4.Top             = 101;
  Label4.Width           = 65;
  Label4.Height          = 21;
  Label4.Visible         = 1;
  Label4.Active          = 1;
  Label4.Caption         = Label4_Caption;
  Label4.FontName        = Tahoma16x19_Regular;
  Label4.Font_Color      = 0xFFFF;
  Label4.VerticalText    = 0;
  Label4.OnUpPtr         = 0;
  Label4.OnDownPtr       = 0;
  Label4.OnClickPtr      = 0;
  Label4.OnPressPtr      = 0;

  Circle4.OwnerScreen     = &Diags;
  Circle4.Order           = 9;
  Circle4.Left            = 8;
  Circle4.Top             = 134;
  Circle4.Radius          = 12;
  Circle4.Pen_Width       = 1;
  Circle4.Pen_Color       = 0x0273;
  Circle4.Visible         = 1;
  Circle4.Active          = 1;
  Circle4.Transparent     = 1;
  Circle4.Gradient        = 1;
  Circle4.Gradient_Orientation = 0;
  Circle4.Gradient_Start_Color = 0xFFFF;
  Circle4.Gradient_End_Color = 0xC618;
  Circle4.Color           = 0xC618;
  Circle4.PressColEnabled = 1;
  Circle4.Press_Color     = 0xE71C;
  Circle4.OnUpPtr         = 0;
  Circle4.OnDownPtr       = 0;
  Circle4.OnClickPtr      = 0;
  Circle4.OnPressPtr      = 0;

  Label5.OwnerScreen     = &Diags;
  Label5.Order           = 10;
  Label5.Left            = 38;
  Label5.Top             = 135;
  Label5.Width           = 81;
  Label5.Height          = 21;
  Label5.Visible         = 1;
  Label5.Active          = 1;
  Label5.Caption         = Label5_Caption;
  Label5.FontName        = Tahoma16x19_Regular;
  Label5.Font_Color      = 0xFFFF;
  Label5.VerticalText    = 0;
  Label5.OnUpPtr         = 0;
  Label5.OnDownPtr       = 0;
  Label5.OnClickPtr      = 0;
  Label5.OnPressPtr      = 0;

  Circle5.OwnerScreen     = &Diags;
  Circle5.Order           = 11;
  Circle5.Left            = 8;
  Circle5.Top             = 168;
  Circle5.Radius          = 12;
  Circle5.Pen_Width       = 1;
  Circle5.Pen_Color       = 0x0273;
  Circle5.Visible         = 1;
  Circle5.Active          = 1;
  Circle5.Transparent     = 1;
  Circle5.Gradient        = 1;
  Circle5.Gradient_Orientation = 0;
  Circle5.Gradient_Start_Color = 0xFFFF;
  Circle5.Gradient_End_Color = 0xC618;
  Circle5.Color           = 0xC618;
  Circle5.PressColEnabled = 1;
  Circle5.Press_Color     = 0xE71C;
  Circle5.OnUpPtr         = 0;
  Circle5.OnDownPtr       = 0;
  Circle5.OnClickPtr      = 0;
  Circle5.OnPressPtr      = 0;

  Label6.OwnerScreen     = &Diags;
  Label6.Order           = 12;
  Label6.Left            = 38;
  Label6.Top             = 168;
  Label6.Width           = 86;
  Label6.Height          = 21;
  Label6.Visible         = 1;
  Label6.Active          = 1;
  Label6.Caption         = Label6_Caption;
  Label6.FontName        = Tahoma16x19_Regular;
  Label6.Font_Color      = 0xFFFF;
  Label6.VerticalText    = 0;
  Label6.OnUpPtr         = 0;
  Label6.OnDownPtr       = 0;
  Label6.OnClickPtr      = 0;
  Label6.OnPressPtr      = 0;

  Circle6.OwnerScreen     = &Diags;
  Circle6.Order           = 13;
  Circle6.Left            = 8;
  Circle6.Top             = 201;
  Circle6.Radius          = 12;
  Circle6.Pen_Width       = 1;
  Circle6.Pen_Color       = 0x0273;
  Circle6.Visible         = 1;
  Circle6.Active          = 1;
  Circle6.Transparent     = 1;
  Circle6.Gradient        = 1;
  Circle6.Gradient_Orientation = 0;
  Circle6.Gradient_Start_Color = 0xFFFF;
  Circle6.Gradient_End_Color = 0xC618;
  Circle6.Color           = 0xC618;
  Circle6.PressColEnabled = 1;
  Circle6.Press_Color     = 0xE71C;
  Circle6.OnUpPtr         = 0;
  Circle6.OnDownPtr       = 0;
  Circle6.OnClickPtr      = 0;
  Circle6.OnPressPtr      = 0;

  Label7.OwnerScreen     = &Diags;
  Label7.Order           = 14;
  Label7.Left            = 38;
  Label7.Top             = 201;
  Label7.Width           = 101;
  Label7.Height          = 21;
  Label7.Visible         = 1;
  Label7.Active          = 1;
  Label7.Caption         = Label7_Caption;
  Label7.FontName        = Tahoma16x19_Regular;
  Label7.Font_Color      = 0xFFFF;
  Label7.VerticalText    = 0;
  Label7.OnUpPtr         = 0;
  Label7.OnDownPtr       = 0;
  Label7.OnClickPtr      = 0;
  Label7.OnPressPtr      = 0;

  CheckBox1.OwnerScreen     = &Diags;
  CheckBox1.Order           = 15;
  CheckBox1.Left            = 171;
  CheckBox1.Top             = 34;
  CheckBox1.Width           = 105;
  CheckBox1.Height          = 24;
  CheckBox1.Pen_Width       = 1;
  CheckBox1.Pen_Color       = 0x0273;
  CheckBox1.Visible         = 1;
  CheckBox1.Active          = 1;
  CheckBox1.Checked         = 0;
  CheckBox1.Transparent     = 1;
  CheckBox1.Caption         = CheckBox1_Caption;
  CheckBox1.TextAlign       = _taLeft;
  CheckBox1.FontName        = Tahoma16x19_Regular;
  CheckBox1.PressColEnabled = 1;
  CheckBox1.Font_Color      = 0xFFFF;
  CheckBox1.Gradient        = 1;
  CheckBox1.Gradient_Orientation = 0;
  CheckBox1.Gradient_Start_Color = 0xFFFF;
  CheckBox1.Gradient_End_Color = 0xC618;
  CheckBox1.Color           = 0xC618;
  CheckBox1.Press_Color     = 0xE71C;
  CheckBox1.Rounded         = 1;
  CheckBox1.Corner_Radius   = 3;
  CheckBox1.OnUpPtr         = 0;
  CheckBox1.OnDownPtr       = 0;
  CheckBox1.OnClickPtr      = SetOutput;
  CheckBox1.OnPressPtr      = 0;
  CheckBox1.Bit             = 0x0;

  CheckBox2.OwnerScreen     = &Diags;
  CheckBox2.Order           = 16;
  CheckBox2.Left            = 171;
  CheckBox2.Top             = 67;
  CheckBox2.Width           = 145;
  CheckBox2.Height          = 24;
  CheckBox2.Pen_Width       = 1;
  CheckBox2.Pen_Color       = 0x0273;
  CheckBox2.Visible         = 1;
  CheckBox2.Active          = 1;
  CheckBox2.Checked         = 0;
  CheckBox2.Transparent     = 1;
  CheckBox2.Caption         = CheckBox2_Caption;
  CheckBox2.TextAlign       = _taLeft;
  CheckBox2.FontName        = Tahoma16x19_Regular;
  CheckBox2.PressColEnabled = 1;
  CheckBox2.Font_Color      = 0xFFFF;
  CheckBox2.Gradient        = 1;
  CheckBox2.Gradient_Orientation = 0;
  CheckBox2.Gradient_Start_Color = 0xFFFF;
  CheckBox2.Gradient_End_Color = 0xC618;
  CheckBox2.Color           = 0xC618;
  CheckBox2.Press_Color     = 0xE71C;
  CheckBox2.Rounded         = 1;
  CheckBox2.Corner_Radius   = 3;
  CheckBox2.OnUpPtr         = 0;
  CheckBox2.OnDownPtr       = 0;
  CheckBox2.OnClickPtr      = SetOutput;
  CheckBox2.OnPressPtr      = 0;
  CheckBox2.Bit             = 0x1;

  CheckBox3.OwnerScreen     = &Diags;
  CheckBox3.Order           = 17;
  CheckBox3.Left            = 171;
  CheckBox3.Top             = 99;
  CheckBox3.Width           = 93;
  CheckBox3.Height          = 24;
  CheckBox3.Pen_Width       = 1;
  CheckBox3.Pen_Color       = 0x0273;
  CheckBox3.Visible         = 1;
  CheckBox3.Active          = 1;
  CheckBox3.Checked         = 0;
  CheckBox3.Transparent     = 1;
  CheckBox3.Caption         = CheckBox3_Caption;
  CheckBox3.TextAlign       = _taLeft;
  CheckBox3.FontName        = Tahoma16x19_Regular;
  CheckBox3.PressColEnabled = 1;
  CheckBox3.Font_Color      = 0xFFFF;
  CheckBox3.Gradient        = 1;
  CheckBox3.Gradient_Orientation = 0;
  CheckBox3.Gradient_Start_Color = 0xFFFF;
  CheckBox3.Gradient_End_Color = 0xC618;
  CheckBox3.Color           = 0xC618;
  CheckBox3.Press_Color     = 0xE71C;
  CheckBox3.Rounded         = 1;
  CheckBox3.Corner_Radius   = 3;
  CheckBox3.OnUpPtr         = 0;
  CheckBox3.OnDownPtr       = 0;
  CheckBox3.OnClickPtr      = SetOutput;
  CheckBox3.OnPressPtr      = 0;
  CheckBox3.Bit             = 0x2;

  CheckBox4.OwnerScreen     = &Diags;
  CheckBox4.Order           = 18;
  CheckBox4.Left            = 171;
  CheckBox4.Top             = 132;
  CheckBox4.Width           = 108;
  CheckBox4.Height          = 24;
  CheckBox4.Pen_Width       = 1;
  CheckBox4.Pen_Color       = 0x0273;
  CheckBox4.Visible         = 1;
  CheckBox4.Active          = 1;
  CheckBox4.Checked         = 0;
  CheckBox4.Transparent     = 1;
  CheckBox4.Caption         = CheckBox4_Caption;
  CheckBox4.TextAlign       = _taLeft;
  CheckBox4.FontName        = Tahoma16x19_Regular;
  CheckBox4.PressColEnabled = 1;
  CheckBox4.Font_Color      = 0xFFFF;
  CheckBox4.Gradient        = 1;
  CheckBox4.Gradient_Orientation = 0;
  CheckBox4.Gradient_Start_Color = 0xFFFF;
  CheckBox4.Gradient_End_Color = 0xC618;
  CheckBox4.Color           = 0xC618;
  CheckBox4.Press_Color     = 0xE71C;
  CheckBox4.Rounded         = 1;
  CheckBox4.Corner_Radius   = 3;
  CheckBox4.OnUpPtr         = 0;
  CheckBox4.OnDownPtr       = 0;
  CheckBox4.OnClickPtr      = SetOutput;
  CheckBox4.OnPressPtr      = 0;
  CheckBox4.Bit             = 0x3;

  CheckBox5.OwnerScreen     = &Diags;
  CheckBox5.Order           = 19;
  CheckBox5.Left            = 171;
  CheckBox5.Top             = 164;
  CheckBox5.Width           = 97;
  CheckBox5.Height          = 24;
  CheckBox5.Pen_Width       = 1;
  CheckBox5.Pen_Color       = 0x0273;
  CheckBox5.Visible         = 1;
  CheckBox5.Active          = 1;
  CheckBox5.Checked         = 0;
  CheckBox5.Transparent     = 1;
  CheckBox5.Caption         = CheckBox5_Caption;
  CheckBox5.TextAlign       = _taLeft;
  CheckBox5.FontName        = Tahoma16x19_Regular;
  CheckBox5.PressColEnabled = 1;
  CheckBox5.Font_Color      = 0xFFFF;
  CheckBox5.Gradient        = 1;
  CheckBox5.Gradient_Orientation = 0;
  CheckBox5.Gradient_Start_Color = 0xFFFF;
  CheckBox5.Gradient_End_Color = 0xC618;
  CheckBox5.Color           = 0xC618;
  CheckBox5.Press_Color     = 0xE71C;
  CheckBox5.Rounded         = 1;
  CheckBox5.Corner_Radius   = 3;
  CheckBox5.OnUpPtr         = 0;
  CheckBox5.OnDownPtr       = 0;
  CheckBox5.OnClickPtr      = SetOutput;
  CheckBox5.OnPressPtr      = 0;
  CheckBox5.Bit             = 0x4;

  CheckBox6.OwnerScreen     = &Diags;
  CheckBox6.Order           = 20;
  CheckBox6.Left            = 171;
  CheckBox6.Top             = 197;
  CheckBox6.Width           = 72;
  CheckBox6.Height          = 24;
  CheckBox6.Pen_Width       = 1;
  CheckBox6.Pen_Color       = 0x0273;
  CheckBox6.Visible         = 1;
  CheckBox6.Active          = 1;
  CheckBox6.Checked         = 0;
  CheckBox6.Transparent     = 1;
  CheckBox6.Caption         = CheckBox6_Caption;
  CheckBox6.TextAlign       = _taLeft;
  CheckBox6.FontName        = Tahoma16x19_Regular;
  CheckBox6.PressColEnabled = 1;
  CheckBox6.Font_Color      = 0xFFFF;
  CheckBox6.Gradient        = 1;
  CheckBox6.Gradient_Orientation = 0;
  CheckBox6.Gradient_Start_Color = 0xFFFF;
  CheckBox6.Gradient_End_Color = 0xC618;
  CheckBox6.Color           = 0xC618;
  CheckBox6.Press_Color     = 0xE71C;
  CheckBox6.Rounded         = 1;
  CheckBox6.Corner_Radius   = 3;
  CheckBox6.OnUpPtr         = 0;
  CheckBox6.OnDownPtr       = 0;
  CheckBox6.OnClickPtr      = SetOutput;
  CheckBox6.OnPressPtr      = 0;
  CheckBox6.Bit             = 0x5;

  Button1.OwnerScreen     = &Diags;
  Button1.Order           = 21;
  Button1.Left            = 250;
  Button1.Top             = 196;
  Button1.Width           = 59;
  Button1.Height          = 39;
  Button1.Pen_Width       = 1;
  Button1.Pen_Color       = 0x0010;
  Button1.Visible         = 1;
  Button1.Active          = 1;
  Button1.Transparent     = 1;
  Button1.Caption         = Button1_Caption;
  Button1.TextAlign       = _taCenter;
  Button1.TextAlignVertical= _tavMiddle;
  Button1.FontName        = Tahoma16x19_Regular;
  Button1.PressColEnabled = 1;
  Button1.Font_Color      = 0x0000;
  Button1.VerticalText    = 0;
  Button1.Gradient        = 0;
  Button1.Gradient_Orientation = 1;
  Button1.Gradient_Start_Color = 0xFFFF;
  Button1.Gradient_End_Color = 0xFFFF;
  Button1.Color           = 0xFFFF;  //0xC618
  Button1.Press_Color     = 0xE71C;
  Button1.OnUpPtr         = 0;
  Button1.OnDownPtr       = 0;
  Button1.OnClickPtr      = Exit_Pressed;
  Button1.OnPressPtr      = 0;

  Image1.OwnerScreen     = &SplashLand;
  Image1.Order           = 0;
  Image1.Left            = 0;
  Image1.Top             = 27;
  Image1.Width           = 320;
  Image1.Height          = 214;
  Image1.Picture_Type    = 0;
  Image1.Picture_Ratio   = 1;
  Image1.Picture_Name    = waterdripsmallland_bmp;
  Image1.Visible         = 1;
  Image1.Active          = 1;
  Image1.OnUpPtr         = 0;
  Image1.OnDownPtr       = 0;
  Image1.OnClickPtr      = 0;
  Image1.OnPressPtr      = 0;

  Image2.OwnerScreen     = &SplashLand;
  Image2.Order           = 1;
  Image2.Left            = 44;
  Image2.Top             = 0;
  Image2.Width           = 236;
  Image2.Height          = 33;
  Image2.Picture_Type    = 1;
  Image2.Picture_Ratio   = 1;
  Image2.Picture_Name    = MPBE_Logo_small_jpg;
  Image2.Visible         = 1;
  Image2.Active          = 1;
  Image2.OnUpPtr         = 0;
  Image2.OnDownPtr       = 0;
  Image2.OnClickPtr      = 0;
  Image2.OnPressPtr      = 0;

  Box1.OwnerScreen     = &SplashLand;
  Box1.Order           = 2;
  Box1.Left            = 0;
  Box1.Top             = 0;
  Box1.Width           = 44;
  Box1.Height          = 32;
  Box1.Pen_Width       = 1;
  Box1.Pen_Color       = 0xFFFF;
  Box1.Visible         = 1;
  Box1.Active          = 1;
  Box1.Transparent     = 1;
  Box1.Gradient        = 0;
  Box1.Gradient_Orientation = 0;
  Box1.Gradient_Start_Color = 0xFFFF;
  Box1.Gradient_End_Color = 0xC618;
  Box1.Color           = 0xFFFF;
  Box1.PressColEnabled = 1;
  Box1.Press_Color     = 0xE71C;
  Box1.OnUpPtr         = 0;
  Box1.OnDownPtr       = 0;
  Box1.OnClickPtr      = 0;
  Box1.OnPressPtr      = 0;

  Box2.OwnerScreen     = &SplashLand;
  Box2.Order           = 3;
  Box2.Left            = 279;
  Box2.Top             = 0;
  Box2.Width           = 46;
  Box2.Height          = 32;
  Box2.Pen_Width       = 1;
  Box2.Pen_Color       = 0xFFFF;
  Box2.Visible         = 1;
  Box2.Active          = 1;
  Box2.Transparent     = 1;
  Box2.Gradient        = 0;
  Box2.Gradient_Orientation = 0;
  Box2.Gradient_Start_Color = 0xFFFF;
  Box2.Gradient_End_Color = 0xC618;
  Box2.Color           = 0xFFFF;
  Box2.PressColEnabled = 1;
  Box2.Press_Color     = 0xE71C;
  Box2.OnUpPtr         = 0;
  Box2.OnDownPtr       = 0;
  Box2.OnClickPtr      = 0;
  Box2.OnPressPtr      = 0;

  Label8.OwnerScreen     = &SplashLand;
  Label8.Order           = 4;
  Label8.Left            = 14;
  Label8.Top             = 207; // was 40, moved to bottom - CM
  Label8.Width           = 111;
  Label8.Height          = 25;
  Label8.Visible         = 1;
  Label8.Active          = 1;
  Label8.Caption         = Label8_Caption;
  Label8.FontName        = Tahoma23x23_Bold;
  Label8.Font_Color      = 0xFFFF;
#if !SWITCH_ON_I2C
  Label8.Font_Color      = CL_RED; // add warning on splash Screen if no I2C - CM
#endif
  Label8.VerticalText    = 0;
  Label8.OnUpPtr         = 0;
  Label8.OnDownPtr       = 0;
  Label8.OnClickPtr      = 0;
  Label8.OnPressPtr      = 0;

  ErrorLog_OK.OwnerScreen     = &ErrorLog;
  ErrorLog_OK.Order           = 0;
  ErrorLog_OK.Left            = 239;
  ErrorLog_OK.Top             = 4;
  ErrorLog_OK.Width           = 74;
  ErrorLog_OK.Height          = 59;
  ErrorLog_OK.Pen_Width       = 2;
  ErrorLog_OK.Pen_Color       = 0xFFFF;
  ErrorLog_OK.Visible         = 1;
  ErrorLog_OK.Active          = 1;
  ErrorLog_OK.Transparent     = 1;
  ErrorLog_OK.Caption         = ErrorLog_OK_Caption;
  ErrorLog_OK.TextAlign       = _taCenter;
  ErrorLog_OK.TextAlignVertical= _tavMiddle;
  ErrorLog_OK.FontName        = Tahoma42x52_Regular;
  ErrorLog_OK.PressColEnabled = 1;
  ErrorLog_OK.Font_Color      = 0x0000;
  ErrorLog_OK.VerticalText    = 0;
  ErrorLog_OK.Gradient        = 0;
  ErrorLog_OK.Gradient_Orientation = 0;
  ErrorLog_OK.Gradient_Start_Color = 0xFFFF;
  ErrorLog_OK.Gradient_End_Color = 0xC618;
  ErrorLog_OK.Color           = 0x0400;
  ErrorLog_OK.Press_Color     = 0xE71C;
  ErrorLog_OK.Corner_Radius   = 5;
  ErrorLog_OK.OnUpPtr         = 0;
  ErrorLog_OK.OnDownPtr       = 0;
  ErrorLog_OK.OnClickPtr      = ErrorLog_OKClick;
  ErrorLog_OK.OnPressPtr      = 0;

  Log_Title.OwnerScreen     = &ErrorLog;
  Log_Title.Order           = 1;
  Log_Title.Left            = 0;
  Log_Title.Top             = 25;
  Log_Title.Width           = 194;
  Log_Title.Height          = 17;
  Log_Title.Visible         = 1;
  Log_Title.Active          = 1;
  Log_Title.Caption         = Log_Title_Caption;
  Log_Title.FontName        = Tahoma12x16_Regular;
  Log_Title.Font_Color      = 0x07E0;
  Log_Title.VerticalText    = 0;
  Log_Title.OnUpPtr         = 0;
  Log_Title.OnDownPtr       = 0;
  Log_Title.OnClickPtr      = 0;
  Log_Title.OnPressPtr      = 0;

  Log_Labels[0].OwnerScreen     = &ErrorLog;
  Log_Labels[0].Order           = 2;
  Log_Labels[0].Left            = 0;
  Log_Labels[0].Top             = 69;
  Log_Labels[0].Width           = 352;
  Log_Labels[0].Height          = 17;
  Log_Labels[0].Visible         = 1;
  Log_Labels[0].Active          = 1;
  Log_Labels[0].Caption         = 0;
  Log_Labels[0].FontName        = Tahoma12x16_Regular;
  Log_Labels[0].Font_Color      = 0xFFFF;
  Log_Labels[0].VerticalText    = 0;
  Log_Labels[0].OnUpPtr         = 0;
  Log_Labels[0].OnDownPtr       = 0;
  Log_Labels[0].OnClickPtr      = 0;
  Log_Labels[0].OnPressPtr      = 0;
  
  // NOTE - all the other Log_Labels are duplicated from this one in InitializeErrorLogScreen - CM 11/7/17
  
  config_fill_value_label.OwnerScreen     = &Config;
  config_fill_value_label.Order           = 0;
  config_fill_value_label.Left            = 110;
  config_fill_value_label.Top             = 90;
  config_fill_value_label.Width           = 86;
  config_fill_value_label.Height          = 47;
  config_fill_value_label.Visible         = 1;
  config_fill_value_label.Active          = 1;
  config_fill_value_label.Caption         = config_fill_value_label_Caption;
  config_fill_value_label.FontName        = Tahoma34x42_Regular;
  config_fill_value_label.Font_Color      = 0xFFE0;
  config_fill_value_label.VerticalText    = 0;
  config_fill_value_label.OnUpPtr         = 0;
  config_fill_value_label.OnDownPtr       = 0;
  config_fill_value_label.OnClickPtr      = 0;
  config_fill_value_label.OnPressPtr      = 0;

  config_drain_value_label.OwnerScreen     = &Config;
  config_drain_value_label.Order           = 1;
  config_drain_value_label.Left            = 110;
  config_drain_value_label.Top             = 22;
  config_drain_value_label.Width           = 86;
  config_drain_value_label.Height          = 47;
  config_drain_value_label.Visible         = 1;
  config_drain_value_label.Active          = 1;
  config_drain_value_label.Caption         = config_drain_value_label_Caption;
  config_drain_value_label.FontName        = Tahoma34x42_Regular;
  config_drain_value_label.Font_Color      = 0xFFE0;
  config_drain_value_label.VerticalText    = 0;
  config_drain_value_label.OnUpPtr         = 0;
  config_drain_value_label.OnDownPtr       = 0;
  config_drain_value_label.OnClickPtr      = 0;
  config_drain_value_label.OnPressPtr      = 0;

  config_fill_button.OwnerScreen     = &Config;
  config_fill_button.Order           = 2;
  config_fill_button.Left            = 8;
  config_fill_button.Top             = 84;
  config_fill_button.Width           = 96;
  config_fill_button.Height          = 54;
  config_fill_button.Pen_Width       = 1;
  config_fill_button.Pen_Color       = 0x0000;
  config_fill_button.Visible         = 1;
  config_fill_button.Active          = 1;
  config_fill_button.Transparent     = 1;
  config_fill_button.Caption         = config_fill_button_Caption;
  config_fill_button.TextAlign       = _taCenter;
  config_fill_button.TextAlignVertical= _tavMiddle;
  config_fill_button.FontName        = Tahoma21x25_Regular;
  config_fill_button.PressColEnabled = 1;
  config_fill_button.Font_Color      = 0x0000;
  config_fill_button.VerticalText    = 0;
  config_fill_button.Gradient        = 0;
  config_fill_button.Gradient_Orientation = 0;
  config_fill_button.Gradient_Start_Color = 0xFFFF;
  config_fill_button.Gradient_End_Color = 0xC618;
  config_fill_button.Color           = 0xF81F;
  config_fill_button.Press_Color     = 0xE71C;
  config_fill_button.Corner_Radius   = 5;
  config_fill_button.OnUpPtr         = 0;
  config_fill_button.OnDownPtr       = 0;
  config_fill_button.OnClickPtr      = config_fill_buttonClick;
  config_fill_button.OnPressPtr      = 0;

  config_drain_button.OwnerScreen     = &Config;
  config_drain_button.Order           = 3;
  config_drain_button.Left            = 8;
  config_drain_button.Top             = 18;
  config_drain_button.Width           = 96;
  config_drain_button.Height          = 54;
  config_drain_button.Pen_Width       = 1;
  config_drain_button.Pen_Color       = 0x0000;
  config_drain_button.Visible         = 1;
  config_drain_button.Active          = 1;
  config_drain_button.Transparent     = 1;
  config_drain_button.Caption         = config_drain_button_Caption;
  config_drain_button.TextAlign       = _taCenter;
  config_drain_button.TextAlignVertical= _tavMiddle;
  config_drain_button.FontName        = Tahoma21x25_Regular;
  config_drain_button.PressColEnabled = 1;
  config_drain_button.Font_Color      = 0x0000;
  config_drain_button.VerticalText    = 0;
  config_drain_button.Gradient        = 0;
  config_drain_button.Gradient_Orientation = 0;
  config_drain_button.Gradient_Start_Color = 0xFFFF;
  config_drain_button.Gradient_End_Color = 0xC618;
  config_drain_button.Color           = 0xF81F;
  config_drain_button.Press_Color     = 0xE71C;
  config_drain_button.Corner_Radius   = 5;
  config_drain_button.OnUpPtr         = 0;
  config_drain_button.OnDownPtr       = 0;
  config_drain_button.OnClickPtr      = config_drain_buttonClick;
  config_drain_button.OnPressPtr      = 0;
  

  config_confirm_button.OwnerScreen     = &Config;
  config_confirm_button.Order           = 4;
  config_confirm_button.Left            = 222;
  config_confirm_button.Top             = 157;
  config_confirm_button.Width           = 95;
  config_confirm_button.Height          = 77;
  config_confirm_button.Pen_Width       = 2;
  config_confirm_button.Pen_Color       = 0xFFFF;
  config_confirm_button.Visible         = 1;
  config_confirm_button.Active          = 1;
  config_confirm_button.Transparent     = 1;
  config_confirm_button.Caption         = config_confirm_button_Caption;
  config_confirm_button.TextAlign       = _taCenter;
  config_confirm_button.TextAlignVertical= _tavMiddle;
  config_confirm_button.FontName        = Tahoma42x52_Regular;
  config_confirm_button.PressColEnabled = 1;
  config_confirm_button.Font_Color      = 0x0000;
  config_confirm_button.VerticalText    = 0;
  config_confirm_button.Gradient        = 0;
  config_confirm_button.Gradient_Orientation = 0;
  config_confirm_button.Gradient_Start_Color = 0xFFFF;
  config_confirm_button.Gradient_End_Color = 0xC618;
  config_confirm_button.Color           = 0x0400;
  config_confirm_button.Press_Color     = 0xE71C;
  config_confirm_button.Corner_Radius   = 5;
  config_confirm_button.OnUpPtr         = 0;
  config_confirm_button.OnDownPtr       = 0;
  config_confirm_button.OnClickPtr      = config_confirm_buttonClick;
  config_confirm_button.OnPressPtr      = 0;

  config_ret_button.OwnerScreen     = &Config;
  config_ret_button.Order           = 5;
  config_ret_button.Left            = 222;
  config_ret_button.Top             = 4;
  config_ret_button.Width           = 95;
  config_ret_button.Height          = 77;
  config_ret_button.Pen_Width       = 2;
  config_ret_button.Pen_Color       = 0xFFFF;
  config_ret_button.Visible         = 1;
  config_ret_button.Active          = 1;
  config_ret_button.Transparent     = 1;
  config_ret_button.Caption         = config_ret_button_Caption;
  config_ret_button.TextAlign       = _taCenter;
  config_ret_button.TextAlignVertical= _tavMiddle;
  config_ret_button.FontName        = Tahoma42x52_Regular;
  config_ret_button.PressColEnabled = 1;
  config_ret_button.Font_Color      = 0x0000;
  config_ret_button.VerticalText    = 0;
  config_ret_button.Gradient        = 0;
  config_ret_button.Gradient_Orientation = 0;
  config_ret_button.Gradient_Start_Color = 0xFFFF;
  config_ret_button.Gradient_End_Color = 0xC618;
  config_ret_button.Color           = 0xF800;
  config_ret_button.Press_Color     = 0xE71C;
  config_ret_button.Corner_Radius   = 5;
  config_ret_button.OnUpPtr         = 0;
  config_ret_button.OnDownPtr       = 0;
  config_ret_button.OnClickPtr      = config_ret_buttonClick;
  config_ret_button.OnPressPtr      = 0;

  Image3.OwnerScreen     = &Config;
  Image3.Order           = 6;
  Image3.Left            = 250;
  Image3.Top             = 21;
  Image3.Width           = 40;
  Image3.Height          = 42;
  Image3.Picture_Type    = 0;
  Image3.Picture_Ratio   = 1;
  Image3.Picture_Name    = returnsarrow_bmp;
  Image3.Visible         = 1;
  Image3.Active          = 0;
  Image3.OnUpPtr         = 0;
  Image3.OnDownPtr       = 0;
  Image3.OnClickPtr      = 0;
  Image3.OnPressPtr      = 0;
}

static char IsInsideObject (unsigned int X, unsigned int Y, unsigned int Left, unsigned int Top, unsigned int Width, unsigned int Height) { // static
  if ( (Left<= X) && (Left+ Width - 1 >= X) &&
       (Top <= Y)  && (Top + Height - 1 >= Y) )
    return 1;
  else
    return 0;
}


#define GetButton(index)              CurrentScreen->Buttons[index]
#define GetRoundButton(index)         CurrentScreen->Buttons_Round[index]
#define GetCRoundButton(index)        CurrentScreen->CButtons_Round[index]
#define GetLabel(index)               CurrentScreen->Labels[index]
#define GetImage(index)               CurrentScreen->Images[index]
#define GetCImage(index)              CurrentScreen->CImages[index]
#define GetCircle(index)              CurrentScreen->Circles[index]
#define GetBox(index)                 CurrentScreen->Boxes[index]
#define GetLine(index)                CurrentScreen->Lines[index]
#define GetCheckBox(index)            CurrentScreen->CheckBoxes[index]

void DrawButton(TButton *Abutton) {
unsigned int ALeft, ATop;
  if (CurrentScreen == AButton->OwnerScreen && Abutton->Visible != 0) {
    if (object_pressed == 1) {
      object_pressed = 0;
      TFT_Set_Brush(Abutton->Transparent, Abutton->Press_Color, Abutton->Gradient, Abutton->Gradient_Orientation, Abutton->Gradient_End_Color, Abutton->Gradient_Start_Color);
    }
    else {
      TFT_Set_Brush(Abutton->Transparent, Abutton->Color, Abutton->Gradient, Abutton->Gradient_Orientation, Abutton->Gradient_Start_Color, Abutton->Gradient_End_Color);
    }
    TFT_Set_Pen(Abutton->Pen_Color, Abutton->Pen_Width);
    TFT_Rectangle(Abutton->Left, Abutton->Top, Abutton->Left + Abutton->Width - 1, Abutton->Top + Abutton->Height - 1);
    if (Abutton->VerticalText != 0)
      TFT_Set_Font(Abutton->FontName, Abutton->Font_Color, FO_VERTICAL_COLUMN);
    else
      TFT_Set_Font(Abutton->FontName, Abutton->Font_Color, FO_HORIZONTAL);
    TFT_Write_Text_Return_Pos(Abutton->Caption, Abutton->Left, Abutton->Top);
    if (Abutton->TextAlign == _taLeft)
      ALeft = Abutton->Left + 4;
    else if (Abutton->TextAlign == _taCenter)
      ALeft = Abutton->Left + (Abutton->Width - caption_length) / 2;
    else if (Abutton->TextAlign == _taRight)
      ALeft = Abutton->Left + Abutton->Width - caption_length - 4;

    if (Abutton->TextAlignVertical == _tavTop)
      ATop = Abutton->Top + 4;
    else if (Abutton->TextAlignVertical == _tavMiddle)
      ATop = Abutton->Top + ((Abutton->Height - caption_height) / 2);
    else if (Abutton->TextAlignVertical == _tavBottom)
      ATop = Abutton->Top + (Abutton->Height - caption_height - 4);

    TFT_Write_Text(Abutton->Caption, ALeft, ATop);
  }
}

void DrawRoundButton(TButton_Round *Around_button) {
unsigned int ALeft, ATop;
  if (CurrentScreen == Around_button->OwnerScreen && Around_button->Visible != 0) {
    if (object_pressed == 1) {
      object_pressed = 0;
      TFT_Set_Brush(Around_button->Transparent, Around_button->Press_Color, Around_button->Gradient, Around_button->Gradient_Orientation,
                    Around_button->Gradient_End_Color, Around_button->Gradient_Start_Color);
    }
    else {
      TFT_Set_Brush(Around_button->Transparent, Around_button->Color, Around_button->Gradient, Around_button->Gradient_Orientation,
                    Around_button->Gradient_Start_Color, Around_button->Gradient_End_Color);
    }
    TFT_Set_Pen(Around_button->Pen_Color, Around_button->Pen_Width);
    TFT_Rectangle_Round_Edges(Around_button->Left + 1, Around_button->Top + 1,
      Around_button->Left + Around_button->Width - 2,
      Around_button->Top + Around_button->Height - 2, Around_button->Corner_Radius);
    if (Around_button->VerticalText != 0)
      TFT_Set_Font(Around_button->FontName, Around_button->Font_Color, FO_VERTICAL_COLUMN);
    else
      TFT_Set_Font(Around_button->FontName, Around_button->Font_Color, FO_HORIZONTAL);
    TFT_Write_Text_Return_Pos(Around_button->Caption, Around_button->Left, Around_button->Top);
    if (Around_button->TextAlign == _taLeft)
      ALeft = Around_button->Left + 4;
    else if (Around_button->TextAlign == _taCenter)
      ALeft = Around_button->Left + (Around_button->Width - caption_length) / 2;
    else if (Around_button->TextAlign == _taRight)
      ALeft = Around_button->Left + Around_button->Width - caption_length - 4;

    if (Around_button->TextAlignVertical == _tavTop)
      ATop = Around_button->Top + 3;
    else if (Around_button->TextAlignVertical == _tavMiddle)
      ATop = Around_button->Top + (Around_button->Height - caption_height) / 2;
    else if (Around_button->TextAlignVertical == _tavBottom)
      ATop  = Around_button->Top + Around_button->Height - caption_height - 4;

    TFT_Write_Text(Around_button->Caption, ALeft, ATop);
  }
}

void DrawCRoundButton(TCButton_Round *Around_button) {
unsigned int ALeft, ATop;
  if (CurrentScreen == Around_button->OwnerScreen && Around_button->Visible != 0) {
    if (object_pressed == 1) {
      object_pressed = 0;
      TFT_Set_Brush(Around_button->Transparent, Around_button->Press_Color, Around_button->Gradient, Around_button->Gradient_Orientation,
                    Around_button->Gradient_End_Color, Around_button->Gradient_Start_Color);
    }
    else {
      TFT_Set_Brush(Around_button->Transparent, Around_button->Color, Around_button->Gradient, Around_button->Gradient_Orientation,
                    Around_button->Gradient_Start_Color, Around_button->Gradient_End_Color);
    }
    TFT_Set_Pen(Around_button->Pen_Color, Around_button->Pen_Width);
    TFT_Rectangle_Round_Edges(Around_button->Left + 1, Around_button->Top + 1,
      Around_button->Left + Around_button->Width - 2,
      Around_button->Top + Around_button->Height - 2, Around_button->Corner_Radius);
    if (Around_button->VerticalText != 0)
      TFT_Set_Font(Around_button->FontName, Around_button->Font_Color, FO_VERTICAL_COLUMN);
    else
      TFT_Set_Font(Around_button->FontName, Around_button->Font_Color, FO_HORIZONTAL);
    TFT_Write_Text_Return_Pos(Around_button->Caption, Around_button->Left, Around_button->Top);
    if (Around_button->TextAlign == _taLeft)
      ALeft = Around_button->Left + 4;
    else if (Around_button->TextAlign == _taCenter)
      ALeft = Around_button->Left + (Around_button->Width - caption_length) / 2;
    else if (Around_button->TextAlign == _taRight)
      ALeft = Around_button->Left + Around_button->Width - caption_length - 4;

    if (Around_button->TextAlignVertical == _tavTop)
      ATop = Around_button->Top + 3;
    else if (Around_button->TextAlignVertical == _tavMiddle)
      ATop = Around_button->Top + (Around_button->Height - caption_height) / 2;
    else if (Around_button->TextAlignVertical == _tavBottom)
      ATop  = Around_button->Top + Around_button->Height - caption_height - 4;

    TFT_Write_Text(Around_button->Caption, ALeft, ATop);
  }
}

void DrawLabel(TLabel *ALabel) {
  if (CurrentScreen == ALabel->OwnerScreen && ALabel->Visible != 0) {
    if (ALabel->VerticalText != 0)
      TFT_Set_Font(ALabel->FontName, ALabel->Font_Color, FO_VERTICAL_COLUMN);
    else
      TFT_Set_Font(ALabel->FontName, ALabel->Font_Color, FO_HORIZONTAL);
    TFT_Write_Text(ALabel->Caption, ALabel->Left, ALabel->Top);
  }
}

void DrawImage(TImage *AImage) {
  if (CurrentScreen == AImage->OwnerScreen && AImage->Visible != 0) {
    if (AImage->Picture_Type == 0)
      TFT_Image(AImage->Left, AImage->Top, AImage->Picture_Name, AImage->Picture_Ratio);
    if (AImage->Picture_Type == 1)
      TFT_Image_Jpeg(AImage->Left, AImage->Top, AImage->Picture_Name);
  }
}

void DrawCImage(TCImage *AImage) {
  if (CurrentScreen == AImage->OwnerScreen && AImage->Visible != 0) {
    if (AImage->Picture_Type == 0)
      TFT_Image(AImage->Left, AImage->Top, AImage->Picture_Name, AImage->Picture_Ratio);
    if (AImage->Picture_Type == 1)
      TFT_Image_Jpeg(AImage->Left, AImage->Top, AImage->Picture_Name);
  }
}

void DrawCircle(TCircle *ACircle) {
  if (CurrentScreen == ACircle->OwnerScreen && ACircle->Visible != 0) {
    if (object_pressed == 1) {
      object_pressed = 0;
      TFT_Set_Brush(ACircle->Transparent, ACircle->Press_Color, ACircle->Gradient, ACircle->Gradient_Orientation,
                    ACircle->Gradient_End_Color, ACircle->Gradient_Start_Color);
    }
    else {
      TFT_Set_Brush(ACircle->Transparent, ACircle->Color, ACircle->Gradient, ACircle->Gradient_Orientation,
                    ACircle->Gradient_Start_Color, ACircle->Gradient_End_Color);
    }
    TFT_Set_Pen(ACircle->Pen_Color, ACircle->Pen_Width);
    TFT_Circle(ACircle->Left + ACircle->Radius,
               ACircle->Top + ACircle->Radius,
               ACircle->Radius);
  }
}

void DrawBox(TBox *ABox) {
  if (CurrentScreen == ABox->OwnerScreen &&  ABox->Visible != 0) {
    if (object_pressed == 1) {
      object_pressed = 0;
      TFT_Set_Brush(ABox->Transparent, ABox->Press_Color, ABox->Gradient, ABox->Gradient_Orientation, ABox->Gradient_End_Color, ABox->Gradient_Start_Color);
    }
    else {
      TFT_Set_Brush(ABox->Transparent, ABox->Color, ABox->Gradient, ABox->Gradient_Orientation, ABox->Gradient_Start_Color, ABox->Gradient_End_Color);
    }
    TFT_Set_Pen(ABox->Pen_Color, ABox->Pen_Width);
    TFT_Rectangle(ABox->Left, ABox->Top, ABox->Left + ABox->Width - 1, ABox->Top + ABox->Height - 1);
  }
}

void DrawLine(TLine *Aline) {
  if (CurrentScreen == ALine->OwnerScreen && Aline->Visible != 0) {
    TFT_Set_Pen(Aline->Color, Aline->Pen_Width);
    TFT_Line(Aline->First_Point_X, Aline->First_Point_Y, Aline->Second_Point_X, Aline->Second_Point_Y);
  }
}

void DrawCheckBox(TCheckBox *ACheckBox) {
  if (CurrentScreen == ACheckBox->OwnerScreen && ACheckBox->Visible != 0) {
    if (object_pressed == 1) {
      object_pressed = 0;
      TFT_Set_Brush(ACheckBox->Transparent, ACheckBox->Press_Color, ACheckBox->Gradient, ACheckBox->Gradient_Orientation, ACheckBox->Gradient_End_Color, ACheckBox->Gradient_Start_Color);
    }
    else {
      TFT_Set_Brush(ACheckBox->Transparent, ACheckBox->Color, ACheckBox->Gradient, ACheckBox->Gradient_Orientation, ACheckBox->Gradient_Start_Color, ACheckBox->Gradient_End_Color);
    }
    TFT_Set_Pen(ACheckBox->Pen_Color, ACheckBox->Pen_Width);
    TFT_Set_Font(ACheckBox->FontName, ACheckBox->Font_Color, FO_HORIZONTAL);
    if (ACheckBox->TextAlign == _taLeft) {
      if (ACheckBox->Rounded != 0)
        TFT_Rectangle_Round_Edges(ACheckBox->Left, ACheckBox->Top, ACheckBox->Left + ACheckBox->Height, ACheckBox->Top + ACheckBox->Height - 1, ACheckBox->Corner_Radius);
      else
        TFT_Rectangle(ACheckBox->Left, ACheckBox->Top, ACheckBox->Left + ACheckBox->Height, ACheckBox->Top + ACheckBox->Height - 1);
      if (ACheckBox->Checked != 0) {
        TFT_Set_Pen(ACheckBox->Pen_Color, ACheckBox->Height / 8);
        TFT_Line(ACheckBox->Left  + ACheckBox->Height / 5 + 1,
                               ACheckBox->Top   + ACheckBox->Height / 2 + 1,
                               ACheckBox->Left  + ACheckBox->Height / 2 - 1,
                               ACheckBox->Top   + ACheckBox->Height - ACheckBox->Height / 5 - 1);
        TFT_Line(ACheckBox->Left  + ACheckBox->Height / 2 - ACheckBox->Pen_Width + 1,
                               ACheckBox->Top   + ACheckBox->Height -  ACheckBox->Height / 5 - 1,
                               ACheckBox->Left  + ACheckBox->Height - ACheckBox->Height / 5 - 1,
                               ACheckBox->Top   + ACheckBox->Height / 5 + 1);
      }
      TFT_Write_Text_Return_Pos(ACheckBox->Caption, ACheckBox->Left + ACheckBox->Width / 4 + 3, ACheckBox->Top);
      TFT_Write_Text(ACheckBox->Caption, ACheckBox->Left + ACheckBox->Height + 3, (ACheckBox->Top + ((ACheckBox->Height - caption_height) / 2)));
    }
    else if (ACheckBox->TextAlign == _taRight) {
      if (ACheckBox->Rounded != 0)
        TFT_Rectangle_Round_Edges(ACheckBox->Left + ACheckBox->Width - ACheckBox->Height  , ACheckBox->Top, ACheckBox->Left + ACheckBox->Width, ACheckBox->Top + ACheckBox->Height - 1, ACheckBox->Corner_Radius);
      else
        TFT_Rectangle(ACheckBox->Left + ACheckBox->Width - ACheckBox->Height  , ACheckBox->Top, ACheckBox->Left + ACheckBox->Width, ACheckBox->Top + ACheckBox->Height - 1);
      if (ACheckBox->Checked != 0) {
        TFT_Set_Pen(ACheckBox->Pen_Color, ACheckBox->Height / 8);
        TFT_Line(ACheckBox->Left  + ACheckBox->Width - ACheckBox->Height + ACheckBox->Height / 5 + 1,
                               ACheckBox->Top +  ACheckBox->Height / 2 + 1,
                               ACheckBox->Left + ACheckBox->Width  - ACheckBox->Height /2 - 1,
                               ACheckBox->Top   + ACheckBox->Height - ACheckBox->Height / 5 - 1);
        TFT_Line(ACheckBox->Left + ACheckBox->Width  - ACheckBox->Height /2 + 1,
                               ACheckBox->Top   + ACheckBox->Height -  ACheckBox->Height / 5 - 1,
                               ACheckBox->Left + ACheckBox->Width  - ACheckBox->Height / 5 - 1,
                               ACheckBox->Top   + ACheckBox->Height / 5 + 1);
      }
      TFT_Write_Text_Return_Pos(ACheckBox->Caption, ACheckBox->Left + 3, ACheckBox->Top);
      TFT_Write_Text(ACheckBox->Caption, ACheckBox->Left + 3, ACheckBox->Top + (ACheckBox->Height - caption_height) / 2);
    }
  }
}

void DrawScreen(TScreen *aScreen) {
 unsigned int order;
  unsigned short button_idx;
  TButton *local_button;
  unsigned short round_button_idx;
  TButton_Round *local_round_button;
  unsigned short round_cbutton_idx;
  TCButton_Round *local_round_cbutton;
  unsigned short label_idx;
  TLabel *local_label;
  unsigned short image_idx;
  TImage *local_image;
  unsigned short cimage_idx;
  TCImage *local_cimage;
  unsigned short circle_idx;
  TCircle *local_circle;
  unsigned short box_idx;
  TBox *local_box;
  unsigned short line_idx;
  TLine *local_line;
  unsigned short checkbox_idx;
  TCheckBox *local_checkbox;
  char save_bled;

  object_pressed = 0;
  order = 0;
  button_idx = 0;
  round_button_idx = 0;
  round_cbutton_idx = 0;
  label_idx = 0;
  image_idx = 0;
  cimage_idx = 0;
  circle_idx = 0;
  box_idx = 0;
  line_idx = 0;
  checkbox_idx = 0;
  CurrentScreen = aScreen;

  if ((display_width != CurrentScreen->Width) || (display_height != CurrentScreen->Height)) {
    save_bled = TFT_BLED;
    TFT_BLED           = 0;
    TFT_Init_ILI9341_8bit(CurrentScreen->Width, CurrentScreen->Height);
    TP_TFT_Init(CurrentScreen->Width, CurrentScreen->Height, 8, 9);                                  // Initialize touch panel
    TP_TFT_Set_ADC_Threshold(ADC_THRESHOLD);                              // Set touch panel ADC threshold
    TFT_Fill_Screen(CurrentScreen->Color);
    display_width = CurrentScreen->Width;
    display_height = CurrentScreen->Height;
    TFT_BLED           = save_bled;
  }
  else
    TFT_Fill_Screen(CurrentScreen->Color);


  while (order < CurrentScreen->ObjectsCount) {
    if (button_idx < CurrentScreen->ButtonsCount) {
      local_button = GetButton(button_idx);
      if (order == local_button->Order) {
        button_idx++;
        order++;
        DrawButton(local_button);
      }
    }

    if (round_button_idx < CurrentScreen->Buttons_RoundCount) {
      local_round_button = GetRoundButton(round_button_idx);
      if (order == local_round_button->Order) {
        round_button_idx++;
        order++;
        DrawRoundButton(local_round_button);
      }
    }

    if (round_cbutton_idx < CurrentScreen->CButtons_RoundCount) {
      local_round_cbutton = GetCRoundButton(round_cbutton_idx);
      if (order == local_round_cbutton->Order) {
        round_cbutton_idx++;
        order++;
        DrawCRoundButton(local_round_cbutton);
      }
    }

    if (label_idx < CurrentScreen->LabelsCount) {
      local_label = GetLabel(label_idx);
      if (order == local_label->Order) {
        label_idx++;
        order++;
        DrawLabel(local_label);
      }
    }

    if (circle_idx < CurrentScreen->CirclesCount) {
      local_circle = GetCircle(circle_idx);
      if (order == local_circle->Order) {
        circle_idx++;
        order++;
        DrawCircle(local_circle);
      }
    }

    if (box_idx < CurrentScreen->BoxesCount) {
      local_box = GetBox(box_idx);
      if (order == local_box->Order) {
        box_idx++;
        order++;
        DrawBox(local_box);
      }
    }

    if (line_idx < CurrentScreen->LinesCount) {
      local_line = GetLine(line_idx);
      if (order == local_line->Order) {
        line_idx++;
        order++;
        DrawLine(local_line);
      }
    }

    if (image_idx < CurrentScreen->ImagesCount) {
      local_image = GetImage(image_idx);
      if (order == local_image->Order) {
        image_idx++;
        order++;
        DrawImage(local_image);
      }
    }

    if (cimage_idx < CurrentScreen->CImagesCount) {
      local_cimage = GetCImage(cimage_idx);
      if (order == local_cimage->Order) {
        cimage_idx++;
        order++;
        DrawCImage(local_cimage);
      }
    }

    if (checkbox_idx < CurrentScreen->CheckBoxesCount) {
      local_checkbox = GetCheckBox(checkbox_idx);
      if (order == local_checkbox->Order) {
        checkbox_idx++;
        order++;
        DrawCheckBox(local_checkbox);
      }
    }

  }
}

void Get_Object(unsigned int X, unsigned int Y) {
  button_order        = -1;
  round_button_order  = -1;
  round_cbutton_order = -1;
  label_order         = -1;
  image_order         = -1;
  cimage_order        = -1;
  circle_order        = -1;
  box_order           = -1;
  checkbox_order      = -1;
  //  Buttons
  for ( _object_count = 0 ; _object_count < CurrentScreen->ButtonsCount ; _object_count++ ) {
    local_button = GetButton(_object_count);
    if (local_button->Active != 0) {
      if (IsInsideObject(X, Y, local_button->Left, local_button->Top,
                         local_button->Width, local_button->Height) == 1) {
        button_order = local_button->Order;
        exec_button = local_button;
      }
    }
  }

  //  Buttons with Round Edges
  for ( _object_count = 0 ; _object_count < CurrentScreen->Buttons_RoundCount ; _object_count++ ) {
    local_round_button = GetRoundButton(_object_count);
    if (local_round_button->Active != 0) {
      if (IsInsideObject(X, Y, local_round_button->Left, local_round_button->Top,
                         local_round_button->Width, local_round_button->Height) == 1) {
        round_button_order = local_round_button->Order;
        exec_round_button = local_round_button;
      }
    }
  }

  //  CButtons with Round Edges
  for ( _object_count = 0 ; _object_count < CurrentScreen->CButtons_RoundCount ; _object_count++ ) {
    local_round_cbutton = GetCRoundButton(_object_count);
    if (local_round_cbutton->Active != 0) {
      if (IsInsideObject(X, Y, local_round_cbutton->Left, local_round_cbutton->Top,
                         local_round_cbutton->Width, local_round_cbutton->Height) == 1) {
        round_cbutton_order = local_round_cbutton->Order;
        exec_round_cbutton = local_round_cbutton;
      }
    }
  }

  //  Labels
  for ( _object_count = 0 ; _object_count < CurrentScreen->LabelsCount ; _object_count++ ) {
    local_label = GetLabel(_object_count);
    if (local_label->Active != 0) {
      if (IsInsideObject(X, Y, local_label->Left, local_label->Top,
                         local_label->Width, local_label->Height) == 1) {
        label_order = local_label->Order;
        exec_label = local_label;
      }
    }
  }

  //  Images
  for ( _object_count = 0 ; _object_count < CurrentScreen->ImagesCount ; _object_count++ ) {
    local_image = GetImage(_object_count);
    if (local_image->Active != 0) {
      if (IsInsideObject(X, Y, local_image->Left, local_image->Top,
                         local_image->Width, local_image->Height) == 1) {
        image_order = local_image->Order;
        exec_image = local_image;
      }
    }
  }

  //  CImages
  for ( _object_count = 0 ; _object_count < CurrentScreen->CImagesCount ; _object_count++ ) {
    local_cimage = GetCImage(_object_count);
    if (local_cimage->Active != 0) {
      if (IsInsideObject(X, Y, local_cimage->Left, local_cimage->Top,
                         local_cimage->Width, local_cimage->Height) == 1) {
        cimage_order = local_cimage->Order;
        exec_cimage = local_cimage;
      }
    }
  }

  //  Circles
  for ( _object_count = 0 ; _object_count < CurrentScreen->CirclesCount ; _object_count++ ) {
    local_circle = GetCircle(_object_count);
    if (local_circle->Active != 0) {
      if (IsInsideObject(X, Y, local_circle->Left, local_circle->Top,
                        (local_circle->Radius * 2), (local_circle->Radius * 2)) == 1) {
        circle_order = local_circle->Order;
        exec_circle = local_circle;
      }
    }
  }

  //  Boxes
  for ( _object_count = 0 ; _object_count < CurrentScreen->BoxesCount ; _object_count++ ) {
    local_box = GetBox(_object_count);
    if (local_box->Active != 0) {
      if (IsInsideObject(X, Y, local_box->Left, local_box->Top,
                         local_box->Width, local_box->Height) == 1) {
        box_order = local_box->Order;
        exec_box = local_box;
      }
    }
  }

  // CheckBoxes
  for ( _object_count = 0 ; _object_count < CurrentScreen->CheckBoxesCount ; _object_count++ ) {
    local_checkbox = GetCheckBox(_object_count);
    if (local_checkbox->Active != 0) {
      if (IsInsideObject(X, Y, local_checkbox->Left, local_checkbox->Top,
                         local_checkbox->Width, local_checkbox->Height) == 1) {
        checkbox_order = local_checkbox->Order;
        exec_checkbox = local_checkbox;
      }
    }
  }

  _object_count = -1;
  if (button_order >  _object_count )
    _object_count = button_order;
  if (round_button_order >  _object_count )
    _object_count = round_button_order;
  if (round_cbutton_order >  _object_count )
    _object_count = round_cbutton_order;
  if (label_order >  _object_count )
    _object_count = label_order;
  if (image_order >  _object_count )
    _object_count = image_order;
  if (cimage_order >  _object_count )
    _object_count = cimage_order;
  if (circle_order >  _object_count )
    _object_count = circle_order;
  if (box_order >  _object_count )
    _object_count = box_order;
  if (checkbox_order >  _object_count )
    _object_count = checkbox_order;
}


void Process_TP_Press(unsigned int X, unsigned int Y) {
  exec_button         = 0;
  exec_round_button   = 0;
  exec_round_cbutton  = 0;
  exec_label          = 0;
  exec_image          = 0;
  exec_cimage         = 0;
  exec_circle         = 0;
  exec_box            = 0;
  exec_checkbox       = 0;

  Get_Object(X, Y);

  if (_object_count != -1) {
    if (_object_count == button_order) {
      if (exec_button->Active != 0) {
        if (exec_button->OnPressPtr != 0) {
          exec_button->OnPressPtr();
          return;
        }
      }
    }

    if (_object_count == round_button_order) {
      if (exec_round_button->Active != 0) {
        if (exec_round_button->OnPressPtr != 0) {
          exec_round_button->OnPressPtr();
          return;
        }
      }
    }

    if (_object_count == round_cbutton_order) {
      if (exec_round_cbutton->Active != 0) {
        if (exec_round_cbutton->OnPressPtr != 0) {
          exec_round_cbutton->OnPressPtr();
          return;
        }
      }
    }

    if (_object_count == label_order) {
      if (exec_label->Active != 0) {
        if (exec_label->OnPressPtr != 0) {
          exec_label->OnPressPtr();
          return;
        }
      }
    }

    if (_object_count == image_order) {
      if (exec_image->Active != 0) {
        if (exec_image->OnPressPtr != 0) {
          exec_image->OnPressPtr();
          return;
        }
      }
    }

    if (_object_count == cimage_order) {
      if (exec_cimage->Active != 0) {
        if (exec_cimage->OnPressPtr != 0) {
          exec_cimage->OnPressPtr();
          return;
        }
      }
    }

    if (_object_count == circle_order) {
      if (exec_circle->Active != 0) {
        if (exec_circle->OnPressPtr != 0) {
          exec_circle->OnPressPtr();
          return;
        }
      }
    }

    if (_object_count == box_order) {
      if (exec_box->Active != 0) {
        if (exec_box->OnPressPtr != 0) {
          exec_box->OnPressPtr();
          return;
        }
      }
    }

    if (_object_count == checkbox_order) {
      if (exec_checkbox->Active != 0) {
        if (exec_checkbox->OnPressPtr != 0) {
          exec_checkbox->OnPressPtr();
          return;
        }
      }
    }

  }
}

void Process_TP_Up(unsigned int X, unsigned int Y) {

  switch (PressedObjectType) {
    // Button
    case 0: {
      if (PressedObject != 0) {
        exec_button = (TButton*)PressedObject;
        if ((exec_button->PressColEnabled != 0) && (exec_button->OwnerScreen == CurrentScreen)) {
          DrawButton(exec_button);
        }
        break;
      }
      break;
    }
    // Round Button
    case 1: {
      if (PressedObject != 0) {
        exec_round_button = (TButton_Round*)PressedObject;
        if ((exec_round_button->PressColEnabled != 0) && (exec_round_button->OwnerScreen == CurrentScreen)) {
          DrawRoundButton(exec_round_button);
        }
        break;
      }
      break;
    }
    // Round CButton
    case 9: {
      if (PressedObject != 0) {
        exec_round_cbutton = (TCButton_Round*)PressedObject;
        if ((exec_round_cbutton->PressColEnabled != 0) && (exec_round_cbutton->OwnerScreen == CurrentScreen)) {
          DrawCRoundButton(exec_round_cbutton);
        }
        break;
      }
      break;
    }
    // Circle
    case 4: {
      if (PressedObject != 0) {
        exec_circle = (TCircle*)PressedObject;
        if ((exec_circle->PressColEnabled != 0) && (exec_circle->OwnerScreen == CurrentScreen)) {
          DrawCircle(exec_circle);
        }
        break;
      }
      break;
    }
    // Box
    case 6: {
      if (PressedObject != 0) {
        exec_box = (TBox*)PressedObject;
        if ((exec_box->PressColEnabled != 0) && (exec_box->OwnerScreen == CurrentScreen)) {
          DrawBox(exec_box);
        }
        break;
      }
      break;
    }
    // Check Box
    case 16: {
      if (PressedObject != 0) {
        exec_checkbox = (TCheckBox*)PressedObject;
        break;
      }
      break;
    }
  }

  exec_label          = 0;
  exec_image          = 0;
  exec_cimage          = 0;

  Get_Object(X, Y);


  if (_object_count != -1) {
  // Buttons
    if (_object_count == button_order) {
      if (exec_button->Active != 0) {
        if (exec_button->OnUpPtr != 0)
          exec_button->OnUpPtr();
        if (PressedObject == (TPointer)exec_button)
          if (exec_button->OnClickPtr != 0)
            exec_button->OnClickPtr();
        PressedObject = 0;
        PressedObjectType = -1;
        return;
      }
    }

  // Buttons with Round Edges
    if (_object_count == round_button_order) {
      if (exec_round_button->Active != 0) {
        if (exec_round_button->OnUpPtr != 0)
          exec_round_button->OnUpPtr();
        if (PressedObject == (TPointer)exec_round_button)
          if (exec_round_button->OnClickPtr != 0)
            exec_round_button->OnClickPtr();
        PressedObject = 0;
        PressedObjectType = -1;
        return;
      }
    }

  // CButtons with Round Edges
    if (_object_count == round_cbutton_order) {
      if (exec_round_cbutton->Active != 0) {
        if (exec_round_cbutton->OnUpPtr != 0)
          exec_round_cbutton->OnUpPtr();
        if (PressedObject == (TPointer)exec_round_cbutton)
          if (exec_round_cbutton->OnClickPtr != 0)
            exec_round_cbutton->OnClickPtr();
        PressedObject = 0;
        PressedObjectType = -1;
        return;
      }
    }

  // Labels
    if (_object_count == label_order) {
      if (exec_label->Active != 0) {
        if (exec_label->OnUpPtr != 0)
          exec_label->OnUpPtr();
        if (PressedObject == (TPointer)exec_label)
          if (exec_label->OnClickPtr != 0)
            exec_label->OnClickPtr();
        PressedObject = 0;
        PressedObjectType = -1;
        return;
      }
    }

  // Images
    if (_object_count == image_order) {
      if (exec_image->Active != 0) {
        if (exec_image->OnUpPtr != 0)
          exec_image->OnUpPtr();
        if (PressedObject == (TPointer)exec_image)
          if (exec_image->OnClickPtr != 0)
            exec_image->OnClickPtr();
        PressedObject = 0;
        PressedObjectType = -1;
        return;
      }
    }

  // CImages
    if (_object_count == cimage_order) {
      if (exec_cimage->Active != 0) {
        if (exec_cimage->OnUpPtr != 0)
          exec_cimage->OnUpPtr();
        if (PressedObject == (TPointer)exec_cimage)
          if (exec_cimage->OnClickPtr != 0)
            exec_cimage->OnClickPtr();
        PressedObject = 0;
        PressedObjectType = -1;
        return;
      }
    }

  // Circles
    if (_object_count == circle_order) {
      if (exec_circle->Active != 0) {
        if (exec_circle->OnUpPtr != 0)
          exec_circle->OnUpPtr();
        if (PressedObject == (TPointer)exec_circle)
          if (exec_circle->OnClickPtr != 0)
            exec_circle->OnClickPtr();
        PressedObject = 0;
        PressedObjectType = -1;
        return;
      }
    }

  // Boxes
    if (_object_count == box_order) {
      if (exec_box->Active != 0) {
        if (exec_box->OnUpPtr != 0)
          exec_box->OnUpPtr();
        if (PressedObject == (TPointer)exec_box)
          if (exec_box->OnClickPtr != 0)
            exec_box->OnClickPtr();
        PressedObject = 0;
        PressedObjectType = -1;
        return;
      }
    }

  // CheckBoxes
    if (_object_count == checkbox_order) {
      if (exec_checkbox->Active != 0) {
        if (exec_checkbox->OnUpPtr != 0)
          exec_checkbox->OnUpPtr();
        if (PressedObject == (TPointer)exec_checkbox) {
          if (exec_checkbox->Checked != 0)
            exec_checkbox->Checked = 0;
          else
            exec_checkbox->Checked = 1;
          DrawCheckBox(exec_checkbox);
          if (exec_checkbox->OnClickPtr != 0)
            exec_checkbox->OnClickPtr(exec_checkbox->Bit, exec_checkbox->Checked);
        }
        PressedObject = 0;
        PressedObjectType = -1;
        return;
      }
    }

  }
  PressedObject = 0;
  PressedObjectType = -1;
}

void Process_TP_Down(unsigned int X, unsigned int Y) {

  object_pressed      = 0;
  exec_button         = 0;
  exec_round_button   = 0;
  exec_round_cbutton  = 0;
  exec_label          = 0;
  exec_image          = 0;
  exec_cimage         = 0;
  exec_circle         = 0;
  exec_box            = 0;
  exec_checkbox       = 0;

  Get_Object(X, Y);

  if (_object_count != -1) {
    if (_object_count == button_order) {
      if (exec_button->Active != 0) {
        if (exec_button->PressColEnabled != 0) {
          object_pressed = 1;
          DrawButton(exec_button);
        }
        PressedObject = (TPointer)exec_button;
        PressedObjectType = 0;
        if (exec_button->OnDownPtr != 0) {
          exec_button->OnDownPtr();
          return;
        }
      }
    }

    if (_object_count == round_button_order) {
      if (exec_round_button->Active != 0) {
        if (exec_round_button->PressColEnabled != 0) {
          object_pressed = 1;
          DrawRoundButton(exec_round_button);
        }
        PressedObject = (TPointer)exec_round_button;
        PressedObjectType = 1;
        if (exec_round_button->OnDownPtr != 0) {
          exec_round_button->OnDownPtr();
          return;
        }
      }
    }

    if (_object_count == round_cbutton_order) {
      if (exec_round_cbutton->Active != 0) {
        if (exec_round_cbutton->PressColEnabled != 0) {
          object_pressed = 1;
          DrawCRoundButton(exec_round_cbutton);
        }
        PressedObject = (TPointer)exec_round_cbutton;
        PressedObjectType = 9;
        if (exec_round_cbutton->OnDownPtr != 0) {
          exec_round_cbutton->OnDownPtr();
          return;
        }
      }
    }

    if (_object_count == label_order) {
      if (exec_label->Active != 0) {
        PressedObject = (TPointer)exec_label;
        PressedObjectType = 2;
        if (exec_label->OnDownPtr != 0) {
          exec_label->OnDownPtr();
          return;
        }
      }
    }

    if (_object_count == image_order) {
      if (exec_image->Active != 0) {
        PressedObject = (TPointer)exec_image;
        PressedObjectType = 3;
        if (exec_image->OnDownPtr != 0) {
          exec_image->OnDownPtr();
          return;
        }
      }
    }

    if (_object_count == cimage_order) {
      if (exec_cimage->Active != 0) {
        PressedObject = (TPointer)exec_cimage;
        PressedObjectType = 11;
        if (exec_cimage->OnDownPtr != 0) {
          exec_cimage->OnDownPtr();
          return;
        }
      }
    }

    if (_object_count == circle_order) {
      if (exec_circle->Active != 0) {
        if (exec_circle->PressColEnabled != 0) {
          object_pressed = 1;
          DrawCircle(exec_circle);
        }
        PressedObject = (TPointer)exec_circle;
        PressedObjectType = 4;
        if (exec_circle->OnDownPtr != 0) {
          exec_circle->OnDownPtr();
          return;
        }
      }
    }

    if (_object_count == box_order) {
      if (exec_box->Active != 0) {
        if (exec_box->PressColEnabled != 0) {
          object_pressed = 1;
          DrawBox(exec_box);
        }
        PressedObject = (TPointer)exec_box;
        PressedObjectType = 6;
        if (exec_box->OnDownPtr != 0) {
          exec_box->OnDownPtr();
          return;
        }
      }
    }

    if (_object_count == checkbox_order) {
      if (exec_checkbox->Active != 0) {
        if (exec_checkbox->PressColEnabled != 0) {
          object_pressed = 1;
          DrawCheckBox(exec_checkbox);
        }
        PressedObject = (TPointer)exec_checkbox;
        PressedObjectType = 16;
        if (exec_checkbox->OnDownPtr != 0) {
          exec_checkbox->OnDownPtr();
          return;
        }
      }
    }

  }
}

void Check_TP() {
  if (TP_TFT_Press_Detect()) {
    if (TP_TFT_Get_Coordinates(&Xcoord, &Ycoord) == 0) {
    // After a PRESS is detected read X-Y and convert it to Display dimensions space
      Process_TP_Press(Xcoord, Ycoord);
      if (PenDown == 0) {
        PenDown = 1;
        Process_TP_Down(Xcoord, Ycoord);
      }
    }
  }
  else if (PenDown == 1) {
    PenDown = 0;
    Process_TP_Up(Xcoord, Ycoord);
  }
}

void Init_MCU() {
  GPIO_Digital_Output(&GPIOE_BASE, _GPIO_PINMASK_9);
  TFT_BLED = 1;
  TFT_Set_Default_Mode();
  TP_TFT_Set_Default_Mode();
}


void InitializeErrorLogScreen()
    /************************
    dupliate the error log lines and point each caption to the correct place in memory
    /************************/
    {
    int iErrorLogCounter;
    // the first label already exists as Screen8_Labels[1] and Log_Labels[0]
    
    // point the first caption to the start of the error log buffer
    Log_Labels[0].Caption = &sErrorLog;
    //sprintf(Log_Labels[0].Caption, "This is label 0");
    
    // duplicate the other 9
    for (iErrorLogCounter = 1; iErrorLogCounter < ERRLOG_LINE_N; iErrorLogCounter++)
        {
        //Screen8_Labels[iErrorLogCounter+1] = &Log_Labels[iErrorLogCounter];
        memcpy(&Log_Labels[iErrorLogCounter], &Log_Labels[iErrorLogCounter-1], sizeof(TLabel));
        Log_Labels[iErrorLogCounter].Order++;
        Log_Labels[iErrorLogCounter].Top += 17;
        Log_Labels[iErrorLogCounter].Caption += ERRLOG_LINE_LEN;
        //sprintf(Log_Labels[iErrorLogCounter].Caption, "This is label %d", iErrorLogCounter);
        }
    }

void Start_TP() {
  Init_MCU();

  InitializeTouchPanel();

  //Delay_ms(1000);
  TFT_Fill_Screen(0);
  Calibrate();
  //TFT_Fill_Screen(0);

  InitializeObjects();
  InitializeErrorLogScreen();
  display_width = Diags.Width;
  display_height = Diags.Height;
  DrawScreen(&SplashLand);
}