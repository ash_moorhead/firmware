_StatusHalfSecondsCount:
;Recipe.c,61 :: 		void StatusHalfSecondsCount()
SUB	SP, SP, #4
;Recipe.c,63 :: 		HalfSeconds++;
MOVW	R1, #lo_addr(_HalfSeconds+0)
MOVT	R1, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;Recipe.c,64 :: 		}
L_end_StatusHalfSecondsCount:
ADD	SP, SP, #4
BX	LR
; end of _StatusHalfSecondsCount
_SwitchOnThree:
;Recipe.c,71 :: 		char SwitchOnThree(char bOnOff)
; bOnOff start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
UXTB	R2, R0
; bOnOff end address is: 0 (R0)
; bOnOff start address is: 8 (R2)
;Recipe.c,73 :: 		char bAllOnOff = FALSE;
; bAllOnOff start address is: 0 (R0)
MOVS	R0, #0
;Recipe.c,75 :: 		switch (nDeviceState)
IT	AL
BAL	L_SwitchOnThree0
;Recipe.c,77 :: 		case IMPELLOR_ONOFF:
L_SwitchOnThree2:
;Recipe.c,78 :: 		SetOutput(IMPELLOR, bOnOff);
STRB	R0, [SP, #4]
; bOnOff end address is: 8 (R2)
UXTB	R1, R2
MOVS	R0, #2
BL	_SetOutput+0
LDRB	R0, [SP, #4]
;Recipe.c,79 :: 		delay_ms(300);
MOVW	R7, #40830
MOVT	R7, #213
NOP
NOP
L_SwitchOnThree3:
SUBS	R7, R7, #1
BNE	L_SwitchOnThree3
NOP
NOP
NOP
;Recipe.c,80 :: 		nDeviceState = DRAIN_ONOFF;
MOVS	R2, #3
MOVW	R1, #lo_addr(_nDeviceState+0)
MOVT	R1, #hi_addr(_nDeviceState+0)
STRB	R2, [R1, #0]
;Recipe.c,81 :: 		break;
IT	AL
BAL	L_SwitchOnThree1
;Recipe.c,83 :: 		case DRAIN_ONOFF:
L_SwitchOnThree5:
;Recipe.c,84 :: 		SetOutput(DRAIN_CLOSED, bOnOff);
; bOnOff start address is: 8 (R2)
STRB	R0, [SP, #4]
; bOnOff end address is: 8 (R2)
UXTB	R1, R2
MOVS	R0, #5
BL	_SetOutput+0
LDRB	R0, [SP, #4]
;Recipe.c,85 :: 		delay_ms(300);
MOVW	R7, #40830
MOVT	R7, #213
NOP
NOP
L_SwitchOnThree6:
SUBS	R7, R7, #1
BNE	L_SwitchOnThree6
NOP
NOP
NOP
;Recipe.c,86 :: 		nDeviceState = WATER_IN_ONOFF;
MOVS	R2, #2
MOVW	R1, #lo_addr(_nDeviceState+0)
MOVT	R1, #hi_addr(_nDeviceState+0)
STRB	R2, [R1, #0]
;Recipe.c,88 :: 		break;
; bAllOnOff end address is: 0 (R0)
IT	AL
BAL	L_SwitchOnThree1
;Recipe.c,90 :: 		case WATER_IN_ONOFF:
L_SwitchOnThree8:
;Recipe.c,91 :: 		SetOutput(WATER_IN, bOnOff);
; bOnOff start address is: 8 (R2)
UXTB	R1, R2
; bOnOff end address is: 8 (R2)
MOVS	R0, #4
BL	_SetOutput+0
;Recipe.c,92 :: 		delay_ms(300);
MOVW	R7, #40830
MOVT	R7, #213
NOP
NOP
L_SwitchOnThree9:
SUBS	R7, R7, #1
BNE	L_SwitchOnThree9
NOP
NOP
NOP
;Recipe.c,93 :: 		nDeviceState = IMPELLOR_ONOFF;
MOVS	R2, #1
MOVW	R1, #lo_addr(_nDeviceState+0)
MOVT	R1, #hi_addr(_nDeviceState+0)
STRB	R2, [R1, #0]
;Recipe.c,94 :: 		bAllOnOff = TRUE;
; bAllOnOff start address is: 0 (R0)
MOVS	R0, #1
;Recipe.c,95 :: 		break;
IT	AL
BAL	L_SwitchOnThree1
;Recipe.c,97 :: 		}
L_SwitchOnThree0:
; bOnOff start address is: 8 (R2)
MOVW	R1, #lo_addr(_nDeviceState+0)
MOVT	R1, #hi_addr(_nDeviceState+0)
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	EQ
BEQ	L_SwitchOnThree2
MOVW	R1, #lo_addr(_nDeviceState+0)
MOVT	R1, #hi_addr(_nDeviceState+0)
LDRB	R1, [R1, #0]
CMP	R1, #3
IT	EQ
BEQ	L_SwitchOnThree5
MOVW	R1, #lo_addr(_nDeviceState+0)
MOVT	R1, #hi_addr(_nDeviceState+0)
LDRB	R1, [R1, #0]
CMP	R1, #2
IT	EQ
BEQ	L_SwitchOnThree8
; bOnOff end address is: 8 (R2)
; bAllOnOff end address is: 0 (R0)
L_SwitchOnThree1:
;Recipe.c,99 :: 		return  bAllOnOff;
; bAllOnOff start address is: 0 (R0)
; bAllOnOff end address is: 0 (R0)
;Recipe.c,100 :: 		}
L_end_SwitchOnThree:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _SwitchOnThree
_SetTextLabel:
;Recipe.c,102 :: 		void SetTextLabel(TLabel label, char *msg){
SUB	SP, SP, #12
STR	LR, [SP, #0]
STR	R0, [SP, #8]
;Recipe.c,108 :: 		unsigned int nOldColour = label.Font_Color;
LDRH	R1, [SP, #36]
STRH	R1, [SP, #4]
;Recipe.c,109 :: 		label.Font_Color = CL_BLACK;
MOVW	R1, #0
STRH	R1, [SP, #36]
;Recipe.c,110 :: 		DrawLabel(&label);
ADD	R1, SP, #12
MOV	R0, R1
BL	_DrawLabel+0
;Recipe.c,111 :: 		strcpy(label.Caption, msg);
LDR	R1, [SP, #8]
LDR	R0, [SP, #28]
BL	_strcpy+0
;Recipe.c,112 :: 		label.Font_Color = nOldColour;
LDRH	R1, [SP, #4]
STRH	R1, [SP, #36]
;Recipe.c,113 :: 		DrawLabel(&label);
ADD	R1, SP, #12
MOV	R0, R1
BL	_DrawLabel+0
;Recipe.c,114 :: 		}
L_end_SetTextLabel:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _SetTextLabel
_SetStatus:
;Recipe.c,116 :: 		void SetStatus(char *sCaption, char bError)
; bError start address is: 4 (R1)
; sCaption start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; bError end address is: 4 (R1)
; sCaption end address is: 0 (R0)
; sCaption start address is: 0 (R0)
; bError start address is: 4 (R1)
;Recipe.c,118 :: 		Label2.Font_Color = bError? CL_RED : 0x07FF;
CMP	R1, #0
IT	EQ
BEQ	L_SetStatus11
; bError end address is: 4 (R1)
; ?FLOC___SetStatus?T8 start address is: 4 (R1)
MOVW	R1, #63488
; ?FLOC___SetStatus?T8 end address is: 4 (R1)
IT	AL
BAL	L_SetStatus12
L_SetStatus11:
; ?FLOC___SetStatus?T8 start address is: 4 (R1)
MOVW	R1, #2047
; ?FLOC___SetStatus?T8 end address is: 4 (R1)
L_SetStatus12:
; ?FLOC___SetStatus?T8 start address is: 4 (R1)
MOVW	R2, #lo_addr(_Label2+24)
MOVT	R2, #hi_addr(_Label2+24)
STRH	R1, [R2, #0]
; ?FLOC___SetStatus?T8 end address is: 4 (R1)
;Recipe.c,119 :: 		SetTextLabel(Label2, sCaption);
MOVW	R2, #lo_addr(_Label2+0)
MOVT	R2, #hi_addr(_Label2+0)
; sCaption end address is: 0 (R0)
SUB	SP, SP, #48
MOV	R12, R2
ADD	R11, SP, #0
ADD	R10, R11, #48
BL	___CC2DW+0
BL	_SetTextLabel+0
ADD	SP, SP, #48
;Recipe.c,122 :: 		Label1.Font_Color = Label2.Font_Color;
MOVW	R2, #lo_addr(_Label2+24)
MOVT	R2, #hi_addr(_Label2+24)
LDRH	R3, [R2, #0]
MOVW	R2, #lo_addr(_Label1+24)
MOVT	R2, #hi_addr(_Label1+24)
STRH	R3, [R2, #0]
;Recipe.c,123 :: 		DrawLabel(&Label1);
MOVW	R0, #lo_addr(_Label1+0)
MOVT	R0, #hi_addr(_Label1+0)
BL	_DrawLabel+0
;Recipe.c,124 :: 		}
L_end_SetStatus:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _SetStatus
_SetFloatLabel:
;Recipe.c,127 :: 		void SetFloatLabel(TLabel label, char *format, float fNew) {
SUB	SP, SP, #16
STR	LR, [SP, #0]
STR	R0, [SP, #8]
VSTR	#1, S0, [SP, #12]
;Recipe.c,128 :: 		unsigned int nOldColour = label.Font_Color;
LDRH	R1, [SP, #40]
STRH	R1, [SP, #4]
;Recipe.c,129 :: 		label.Font_Color = CL_BLACK;
MOVW	R1, #0
STRH	R1, [SP, #40]
;Recipe.c,130 :: 		DrawLabel(&label);
ADD	R1, SP, #16
MOV	R0, R1
BL	_DrawLabel+0
;Recipe.c,131 :: 		sprintf(label.Caption, format, fNew);
LDR	R2, [SP, #32]
VLDR	#1, S0, [SP, #12]
LDR	R1, [SP, #8]
VPUSH	#0, (S0)
PUSH	(R1)
PUSH	(R2)
BL	_sprintf+0
ADD	SP, SP, #12
;Recipe.c,132 :: 		label.Font_Color = nOldColour;
LDRH	R1, [SP, #4]
STRH	R1, [SP, #40]
;Recipe.c,133 :: 		DrawLabel(&label);
ADD	R1, SP, #16
MOV	R0, R1
BL	_DrawLabel+0
;Recipe.c,134 :: 		}
L_end_SetFloatLabel:
LDR	LR, [SP, #0]
ADD	SP, SP, #16
BX	LR
; end of _SetFloatLabel
_StateName:
;Recipe.c,137 :: 		char *StateName()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Recipe.c,139 :: 		switch (nState)
IT	AL
BAL	L_StateName13
;Recipe.c,141 :: 		case PREWASH_FILL :      return "PREWASH_FILL";
L_StateName15:
MOVW	R0, #lo_addr(?lstr1_Recipe+0)
MOVT	R0, #hi_addr(?lstr1_Recipe+0)
IT	AL
BAL	L_end_StateName
;Recipe.c,142 :: 		case PREWASH_HEAT :      return "PREWASH_HEAT";
L_StateName16:
MOVW	R0, #lo_addr(?lstr2_Recipe+0)
MOVT	R0, #hi_addr(?lstr2_Recipe+0)
IT	AL
BAL	L_end_StateName
;Recipe.c,143 :: 		case PREWASH_DRAIN :     return "PREWASH_DRAIN";
L_StateName17:
MOVW	R0, #lo_addr(?lstr3_Recipe+0)
MOVT	R0, #hi_addr(?lstr3_Recipe+0)
IT	AL
BAL	L_end_StateName
;Recipe.c,144 :: 		case PRECOOL_FILL :      return "PRECOOL_FILL";
L_StateName18:
MOVW	R0, #lo_addr(?lstr4_Recipe+0)
MOVT	R0, #hi_addr(?lstr4_Recipe+0)
IT	AL
BAL	L_end_StateName
;Recipe.c,145 :: 		case SANTIZ_WAIT  :      return "SANTIZ_WAIT";
L_StateName19:
MOVW	R0, #lo_addr(?lstr5_Recipe+0)
MOVT	R0, #hi_addr(?lstr5_Recipe+0)
IT	AL
BAL	L_end_StateName
;Recipe.c,146 :: 		case SANTIZ_HEAT :       return "SANTIZ_HEAT";
L_StateName20:
MOVW	R0, #lo_addr(?lstr6_Recipe+0)
MOVT	R0, #hi_addr(?lstr6_Recipe+0)
IT	AL
BAL	L_end_StateName
;Recipe.c,147 :: 		case SANTIZ_FILL :       return "SANTIZ_FILL";
L_StateName21:
MOVW	R0, #lo_addr(?lstr7_Recipe+0)
MOVT	R0, #hi_addr(?lstr7_Recipe+0)
IT	AL
BAL	L_end_StateName
;Recipe.c,148 :: 		case POSTCOOL_FILL :     return "POSTCOOL_FILL";
L_StateName22:
MOVW	R0, #lo_addr(?lstr8_Recipe+0)
MOVT	R0, #hi_addr(?lstr8_Recipe+0)
IT	AL
BAL	L_end_StateName
;Recipe.c,149 :: 		case SANTIZ_DRAIN :      return "SANTIZ_DRAIN";
L_StateName23:
MOVW	R0, #lo_addr(?lstr9_Recipe+0)
MOVT	R0, #hi_addr(?lstr9_Recipe+0)
IT	AL
BAL	L_end_StateName
;Recipe.c,150 :: 		default:
L_StateName24:
;Recipe.c,151 :: 		sprintf(sStateNum, "%d", nState);
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R2, [R0, #0]
MOVW	R1, #lo_addr(?lstr_10_Recipe+0)
MOVT	R1, #hi_addr(?lstr_10_Recipe+0)
MOVW	R0, #lo_addr(_sStateNum+0)
MOVT	R0, #hi_addr(_sStateNum+0)
PUSH	(R2)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #12
;Recipe.c,152 :: 		return sStateNum;  // only the above states are likely to result in an error, otherwise just report state number.
MOVW	R0, #lo_addr(_sStateNum+0)
MOVT	R0, #hi_addr(_sStateNum+0)
IT	AL
BAL	L_end_StateName
;Recipe.c,153 :: 		}
L_StateName13:
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #1
IT	EQ
BEQ	L_StateName15
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #2
IT	EQ
BEQ	L_StateName16
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #3
IT	EQ
BEQ	L_StateName17
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #5
IT	EQ
BEQ	L_StateName18
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #12
IT	EQ
BEQ	L_StateName19
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #11
IT	EQ
BEQ	L_StateName20
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #9
IT	EQ
BEQ	L_StateName21
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #15
IT	EQ
BEQ	L_StateName22
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #13
IT	EQ
BEQ	L_StateName23
IT	AL
BAL	L_StateName24
;Recipe.c,154 :: 		}
L_end_StateName:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _StateName
_ErrorLog_NewError:
;Recipe.c,174 :: 		void ErrorLog_NewError(char *errMsg, char bIncrementCount)
SUB	SP, SP, #112
STR	LR, [SP, #0]
STR	R0, [SP, #104]
STRB	R1, [SP, #108]
;Recipe.c,180 :: 		if (bIncrementCount)
LDRB	R2, [SP, #108]
CMP	R2, #0
IT	EQ
BEQ	L_ErrorLog_NewError25
;Recipe.c,183 :: 		fCyclesFail += 1;
MOVW	R2, #lo_addr(_fCyclesFail+0)
MOVT	R2, #hi_addr(_fCyclesFail+0)
VLDR	#1, S1, [R2, #0]
VMOV.F32	S0, #1
VADD.F32	S0, S1, S0
VSTR	#1, S0, [R2, #0]
;Recipe.c,184 :: 		sprintf(CyclesFail, "%6.0f", fCyclesFail);
VMOV.F32	S0, S0
MOVW	R3, #lo_addr(?lstr_11_Recipe+0)
MOVT	R3, #hi_addr(?lstr_11_Recipe+0)
MOVW	R2, #lo_addr(_CyclesFail+0)
MOVT	R2, #hi_addr(_CyclesFail+0)
VPUSH	#0, (S0)
PUSH	(R3)
PUSH	(R2)
BL	_sprintf+0
ADD	SP, SP, #12
;Recipe.c,185 :: 		SetStatus(errMsg, STATUS_ERROR);
MOVS	R1, #1
LDR	R0, [SP, #104]
BL	_SetStatus+0
;Recipe.c,186 :: 		}
L_ErrorLog_NewError25:
;Recipe.c,191 :: 		iErrorLogOffset = ERRLOG_TOTAL_LEN - ERRLOG_LINE_LEN;
; iErrorLogOffset start address is: 0 (R0)
MOVW	R0, #450
; iErrorLogOffset end address is: 0 (R0)
;Recipe.c,192 :: 		while ( iErrorLogOffset )
L_ErrorLog_NewError26:
; iErrorLogOffset start address is: 0 (R0)
CMP	R0, #0
IT	EQ
BEQ	L_ErrorLog_NewError27
;Recipe.c,194 :: 		sErrorLog[iErrorLogOffset+ERRLOG_LINE_LEN-1] = sErrorLog[iErrorLogOffset-1];
ADDW	R2, R0, #50
UXTH	R2, R2
SUBS	R3, R2, #1
UXTH	R3, R3
MOVW	R2, #lo_addr(_sErrorLog+0)
MOVT	R2, #hi_addr(_sErrorLog+0)
ADDS	R4, R2, R3
SUBS	R3, R0, #1
UXTH	R3, R3
MOVW	R2, #lo_addr(_sErrorLog+0)
MOVT	R2, #hi_addr(_sErrorLog+0)
ADDS	R2, R2, R3
LDRB	R2, [R2, #0]
STRB	R2, [R4, #0]
;Recipe.c,195 :: 		iErrorLogOffset--;
SUBS	R2, R0, #1
; iErrorLogOffset end address is: 0 (R0)
; iErrorLogOffset start address is: 4 (R1)
UXTH	R1, R2
;Recipe.c,196 :: 		}
UXTH	R0, R1
; iErrorLogOffset end address is: 4 (R1)
IT	AL
BAL	L_ErrorLog_NewError26
L_ErrorLog_NewError27:
;Recipe.c,201 :: 		memset(sErrorLogTemp, 0, 100);
ADD	R2, SP, #4
MOVS	R1, #0
MOV	R0, R2
MOVS	R2, #100
SXTH	R2, R2
BL	_memset+0
;Recipe.c,202 :: 		sprintf(sErrorLogTemp, "%.0f / %.0f, %s", fCyclesComplete, fCyclesFail, errMsg);
MOVW	R2, #lo_addr(_fCyclesFail+0)
MOVT	R2, #hi_addr(_fCyclesFail+0)
VLDR	#1, S1, [R2, #0]
MOVW	R2, #lo_addr(_fCyclesComplete+0)
MOVT	R2, #hi_addr(_fCyclesComplete+0)
VLDR	#1, S0, [R2, #0]
MOVW	R4, #lo_addr(?lstr_12_Recipe+0)
MOVT	R4, #hi_addr(?lstr_12_Recipe+0)
ADD	R3, SP, #4
LDR	R2, [SP, #104]
PUSH	(R2)
VPUSH	#0, (S1)
VPUSH	#0, (S0)
PUSH	(R4)
PUSH	(R3)
BL	_sprintf+0
ADD	SP, SP, #20
;Recipe.c,203 :: 		if (bIncrementCount)
LDRB	R2, [SP, #108]
CMP	R2, #0
IT	EQ
BEQ	L_ErrorLog_NewError28
;Recipe.c,205 :: 		strcat(sErrorLogTemp, ", ");
MOVW	R3, #lo_addr(?lstr13_Recipe+0)
MOVT	R3, #hi_addr(?lstr13_Recipe+0)
ADD	R2, SP, #4
MOV	R1, R3
MOV	R0, R2
BL	_strcat+0
;Recipe.c,206 :: 		strcat(sErrorLogTemp, StateName());
BL	_StateName+0
ADD	R2, SP, #4
MOV	R1, R0
MOV	R0, R2
BL	_strcat+0
;Recipe.c,207 :: 		}
L_ErrorLog_NewError28:
;Recipe.c,208 :: 		memcpy(sErrorLog, sErrorLogTemp, ERRLOG_LINE_LEN);
ADD	R2, SP, #4
MOV	R1, R2
MOVS	R2, #50
SXTH	R2, R2
MOVW	R0, #lo_addr(_sErrorLog+0)
MOVT	R0, #hi_addr(_sErrorLog+0)
BL	_memcpy+0
;Recipe.c,210 :: 		sErrorLog[ERRLOG_LINE_LEN-1] = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_sErrorLog+49)
MOVT	R2, #hi_addr(_sErrorLog+49)
STRB	R3, [R2, #0]
;Recipe.c,213 :: 		UpDateMemory();
BL	_UpDateMemory+0
;Recipe.c,214 :: 		}
L_end_ErrorLog_NewError:
LDR	LR, [SP, #0]
ADD	SP, SP, #112
BX	LR
; end of _ErrorLog_NewError
_CheckFillingToLong:
;Recipe.c,216 :: 		void CheckFillingToLong()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Recipe.c,218 :: 		if (130 < HalfSeconds)
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
CMP	R0, #130
IT	LS
BLS	L_CheckFillingToLong29
;Recipe.c,220 :: 		ErrorLog_NewError(msgCycleFailFill, STATUS_ERROR);
MOVS	R1, #1
MOVW	R0, #lo_addr(_msgCycleFailFill+0)
MOVT	R0, #hi_addr(_msgCycleFailFill+0)
BL	_ErrorLog_NewError+0
;Recipe.c,221 :: 		nState = ERR_PRE_HOLDING_STATE;
MOVS	R1, #100
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,222 :: 		}
L_CheckFillingToLong29:
;Recipe.c,223 :: 		}
L_end_CheckFillingToLong:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _CheckFillingToLong
_FillWasher:
;Recipe.c,224 :: 		void FillWasher(void)
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Recipe.c,226 :: 		if (CurrentScreen==&_main) SetStatus(msgFilling, STATUS_NORMAL);
MOVW	R0, #lo_addr(_CurrentScreen+0)
MOVT	R0, #hi_addr(_CurrentScreen+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(__main+0)
MOVT	R0, #hi_addr(__main+0)
CMP	R1, R0
IT	NE
BNE	L_FillWasher30
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgFilling+0)
MOVT	R0, #hi_addr(_msgFilling+0)
BL	_SetStatus+0
L_FillWasher30:
;Recipe.c,227 :: 		SetOutput(DRAIN_CLOSED, TRUE);
MOVS	R1, #1
MOVS	R0, #5
BL	_SetOutput+0
;Recipe.c,228 :: 		delay_ms(SWITCH_DELAY);
MOVW	R7, #15827
MOVT	R7, #14
NOP
NOP
L_FillWasher31:
SUBS	R7, R7, #1
BNE	L_FillWasher31
NOP
NOP
NOP
NOP
;Recipe.c,229 :: 		SetOutput(WATER_IN, TRUE);
MOVS	R1, #1
MOVS	R0, #4
BL	_SetOutput+0
;Recipe.c,230 :: 		}
L_end_FillWasher:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _FillWasher
_ResetRecipe:
;Recipe.c,231 :: 		void ResetRecipe()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Recipe.c,233 :: 		nState = WAITING_START;
MOVS	R1, #0
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,234 :: 		SetStatus(msgWaitingStart, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgWaitingStart+0)
MOVT	R0, #hi_addr(_msgWaitingStart+0)
BL	_SetStatus+0
;Recipe.c,235 :: 		}
L_end_ResetRecipe:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _ResetRecipe
_Recipe_State:
;Recipe.c,237 :: 		void Recipe_State()
SUB	SP, SP, #8
STR	LR, [SP, #0]
;Recipe.c,249 :: 		if(nState != nStateOld){
MOVW	R0, #lo_addr(_nStateOld+0)
MOVT	R0, #hi_addr(_nStateOld+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, R1
IT	EQ
BEQ	L_Recipe_State33
;Recipe.c,251 :: 		DBP_bit = 1;
MOVS	R0, #1
SXTB	R0, R0
MOVW	R3, #lo_addr(DBP_bit+0)
MOVT	R3, #hi_addr(DBP_bit+0)
STR	R0, [R3, #0]
;Recipe.c,252 :: 		RTC_BKP0R = nState;
MOVW	R2, #lo_addr(_nState+0)
MOVT	R2, #hi_addr(_nState+0)
LDRB	R1, [R2, #0]
MOVW	R0, #lo_addr(RTC_BKP0R+0)
MOVT	R0, #hi_addr(RTC_BKP0R+0)
STR	R1, [R0, #0]
;Recipe.c,253 :: 		DBP_bit = 0;    //disable write access after written (prevent heat spike etc. corruption)
MOVS	R0, #0
SXTB	R0, R0
STR	R0, [R3, #0]
;Recipe.c,254 :: 		nStateOld = nState;
MOV	R0, R2
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_nStateOld+0)
MOVT	R0, #hi_addr(_nStateOld+0)
STRB	R1, [R0, #0]
;Recipe.c,255 :: 		}
L_Recipe_State33:
;Recipe.c,258 :: 		switch(nState)
IT	AL
BAL	L_Recipe_State34
;Recipe.c,260 :: 		case WAITING_START:
L_Recipe_State36:
;Recipe.c,261 :: 		if (InputOn(START_BTN) && InputOn(LID_SHUT))
MOVS	R0, #1
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L__Recipe_State183
MOVS	R0, #0
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L__Recipe_State182
L__Recipe_State181:
;Recipe.c,263 :: 		fSetTemp = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,264 :: 		nState = PREWASH_FILL;
MOVS	R1, #1
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,265 :: 		SetOutput(CYCLE_RUN_LIGHT, TRUE);
MOVS	R1, #1
MOVS	R0, #0
BL	_SetOutput+0
;Recipe.c,266 :: 		SetOutput(LID_UNLOCK, FALSE);    // locked
MOVS	R1, #0
MOVS	R0, #3
BL	_SetOutput+0
;Recipe.c,267 :: 		FillWasher();
BL	_FillWasher+0
;Recipe.c,268 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,269 :: 		}
IT	AL
BAL	L_Recipe_State40
;Recipe.c,261 :: 		if (InputOn(START_BTN) && InputOn(LID_SHUT))
L__Recipe_State183:
L__Recipe_State182:
;Recipe.c,270 :: 		else if (InputOn(LID_OPEN_BTN) && InputOn(LID_SHUT) && (GetCurrentTemp() < 43))
MOVS	R0, #2
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L__Recipe_State186
MOVS	R0, #0
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L__Recipe_State185
BL	_GetCurrentTemp+0
MOVW	R0, #0
MOVT	R0, #16940
VMOV	S1, R0
VCMPE.F32	S0, S1
VMRS	#60, FPSCR
IT	GE
BGE	L__Recipe_State184
L__Recipe_State180:
;Recipe.c,271 :: 		SetOutput(LID_UNLOCK, TRUE);    // unlocked
MOVS	R1, #1
MOVS	R0, #3
BL	_SetOutput+0
IT	AL
BAL	L_Recipe_State44
;Recipe.c,270 :: 		else if (InputOn(LID_OPEN_BTN) && InputOn(LID_SHUT) && (GetCurrentTemp() < 43))
L__Recipe_State186:
L__Recipe_State185:
L__Recipe_State184:
;Recipe.c,272 :: 		else if (!InputOn(LID_OPEN_BTN) && !InputOn(LID_SHUT))
MOVS	R0, #2
BL	_InputOn+0
CMP	R0, #0
IT	NE
BNE	L__Recipe_State188
MOVS	R0, #0
BL	_InputOn+0
CMP	R0, #0
IT	NE
BNE	L__Recipe_State187
L__Recipe_State179:
;Recipe.c,273 :: 		SetOutput(LID_UNLOCK, FALSE);  // locked       // is this necessary?
MOVS	R1, #0
MOVS	R0, #3
BL	_SetOutput+0
;Recipe.c,272 :: 		else if (!InputOn(LID_OPEN_BTN) && !InputOn(LID_SHUT))
L__Recipe_State188:
L__Recipe_State187:
;Recipe.c,273 :: 		SetOutput(LID_UNLOCK, FALSE);  // locked       // is this necessary?
L_Recipe_State44:
L_Recipe_State40:
;Recipe.c,274 :: 		if(20 == HalfSeconds)
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
CMP	R0, #20
IT	NE
BNE	L_Recipe_State48
;Recipe.c,276 :: 		HalfSeconds++;
MOVW	R1, #lo_addr(_HalfSeconds+0)
MOVT	R1, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;Recipe.c,277 :: 		sprintf(msgCycles, "Cycles =  %6.0f", fCyclesComplete);
MOVW	R0, #lo_addr(_fCyclesComplete+0)
MOVT	R0, #hi_addr(_fCyclesComplete+0)
VLDR	#1, S0, [R0, #0]
MOVW	R1, #lo_addr(?lstr_14_Recipe+0)
MOVT	R1, #hi_addr(?lstr_14_Recipe+0)
MOVW	R0, #lo_addr(_msgCycles+0)
MOVT	R0, #hi_addr(_msgCycles+0)
VPUSH	#0, (S0)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #12
;Recipe.c,278 :: 		SetStatus(msgCycles, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgCycles+0)
MOVT	R0, #hi_addr(_msgCycles+0)
BL	_SetStatus+0
;Recipe.c,279 :: 		}
IT	AL
BAL	L_Recipe_State49
L_Recipe_State48:
;Recipe.c,280 :: 		else if (24 == HalfSeconds)
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
CMP	R0, #24
IT	NE
BNE	L_Recipe_State50
;Recipe.c,282 :: 		HalfSeconds++;
MOVW	R1, #lo_addr(_HalfSeconds+0)
MOVT	R1, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;Recipe.c,283 :: 		sprintf(msgCycles, "Failed =  %6.0f", fCyclesFail);
MOVW	R0, #lo_addr(_fCyclesFail+0)
MOVT	R0, #hi_addr(_fCyclesFail+0)
VLDR	#1, S0, [R0, #0]
MOVW	R1, #lo_addr(?lstr_15_Recipe+0)
MOVT	R1, #hi_addr(?lstr_15_Recipe+0)
MOVW	R0, #lo_addr(_msgCycles+0)
MOVT	R0, #hi_addr(_msgCycles+0)
VPUSH	#0, (S0)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #12
;Recipe.c,284 :: 		SetStatus(msgCycles, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgCycles+0)
MOVT	R0, #hi_addr(_msgCycles+0)
BL	_SetStatus+0
;Recipe.c,285 :: 		}
IT	AL
BAL	L_Recipe_State51
L_Recipe_State50:
;Recipe.c,286 :: 		else if (28 < HalfSeconds)
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
CMP	R0, #28
IT	LS
BLS	L_Recipe_State52
;Recipe.c,288 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,289 :: 		SetStatus(msgWaitingStart, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgWaitingStart+0)
MOVT	R0, #hi_addr(_msgWaitingStart+0)
BL	_SetStatus+0
;Recipe.c,290 :: 		}
L_Recipe_State52:
L_Recipe_State51:
L_Recipe_State49:
;Recipe.c,291 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,293 :: 		case PREWASH_FILL:
L_Recipe_State53:
;Recipe.c,294 :: 		if (InputOn(WATER_LEVEL))
MOVS	R0, #4
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State54
;Recipe.c,296 :: 		SetOutput(WATER_IN, FALSE);
MOVS	R1, #0
MOVS	R0, #4
BL	_SetOutput+0
;Recipe.c,297 :: 		delay_ms(SWITCH_DELAY);
MOVW	R7, #15827
MOVT	R7, #14
NOP
NOP
L_Recipe_State55:
SUBS	R7, R7, #1
BNE	L_Recipe_State55
NOP
NOP
NOP
NOP
;Recipe.c,298 :: 		SetOutput(IMPELLOR, TRUE);
MOVS	R1, #1
MOVS	R0, #2
BL	_SetOutput+0
;Recipe.c,300 :: 		fSetTemp = PREWASH_TEMP + 0.9;
MOVW	R0, #39322
MOVT	R0, #17011
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,301 :: 		sprintf(msgHeating, HEATING_TO, PREWASH_TEMP);
MOVS	R2, #60
SXTB	R2, R2
MOVW	R1, #lo_addr(?lstr_16_Recipe+0)
MOVT	R1, #hi_addr(?lstr_16_Recipe+0)
MOVW	R0, #lo_addr(_msgHeating+0)
MOVT	R0, #hi_addr(_msgHeating+0)
PUSH	(R2)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #12
;Recipe.c,302 :: 		SetStatus(msgHeating, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgHeating+0)
MOVT	R0, #hi_addr(_msgHeating+0)
BL	_SetStatus+0
;Recipe.c,303 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,304 :: 		nState = PREWASH_HEAT;
MOVS	R1, #2
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,305 :: 		}
IT	AL
BAL	L_Recipe_State57
L_Recipe_State54:
;Recipe.c,308 :: 		CheckFillingToLong();  // 130 HalfSeconds timeout
BL	_CheckFillingToLong+0
L_Recipe_State57:
;Recipe.c,309 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,311 :: 		case PREWASH_HEAT:
L_Recipe_State58:
;Recipe.c,313 :: 		if (bAtTemp)
MOVW	R0, #lo_addr(_bAtTemp+0)
MOVT	R0, #hi_addr(_bAtTemp+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State59
;Recipe.c,316 :: 		nState = PREWASH_DRAIN;
MOVS	R1, #3
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,317 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,318 :: 		SetStatus(msgDraining, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgDraining+0)
MOVT	R0, #hi_addr(_msgDraining+0)
BL	_SetStatus+0
;Recipe.c,319 :: 		SetOutput(DRAIN_CLOSED, FALSE);
MOVS	R1, #0
MOVS	R0, #5
BL	_SetOutput+0
;Recipe.c,320 :: 		delay_ms(SWITCH_DELAY);
MOVW	R7, #15827
MOVT	R7, #14
NOP
NOP
L_Recipe_State60:
SUBS	R7, R7, #1
BNE	L_Recipe_State60
NOP
NOP
NOP
NOP
;Recipe.c,321 :: 		SetOutput(IMPELLOR, FALSE);
MOVS	R1, #0
MOVS	R0, #2
BL	_SetOutput+0
;Recipe.c,322 :: 		fSetTemp = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,323 :: 		}
IT	AL
BAL	L_Recipe_State62
L_Recipe_State59:
;Recipe.c,326 :: 		if (HEATINGTIMEOUT < HalfSeconds)   // 1800 HalfSeconds = 15 min
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
CMP	R0, #1800
IT	LS
BLS	L_Recipe_State63
;Recipe.c,328 :: 		ErrorLog_NewError(msgCycleFailHeat, STATUS_ERROR);
MOVS	R1, #1
MOVW	R0, #lo_addr(_msgCycleFailHeat+0)
MOVT	R0, #hi_addr(_msgCycleFailHeat+0)
BL	_ErrorLog_NewError+0
;Recipe.c,329 :: 		nState = ERR_PRE_HOLDING_STATE;
MOVS	R1, #100
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,330 :: 		}
L_Recipe_State63:
;Recipe.c,331 :: 		}
L_Recipe_State62:
;Recipe.c,334 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,336 :: 		case PREWASH_DRAIN:
L_Recipe_State64:
;Recipe.c,339 :: 		if ((fDrainTime.fValue*2) < HalfSeconds)
MOVW	R0, #lo_addr(_fDrainTime+4)
MOVT	R0, #hi_addr(_fDrainTime+4)
VLDR	#1, S1, [R0, #0]
VMOV.F32	S0, #2
VMUL.F32	S1, S1, S0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
VMOV	S0, R0
VCVT.F32	#0, S0, S0
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	GE
BGE	L_Recipe_State65
;Recipe.c,341 :: 		if (InputOn(WATER_LEVEL))
MOVS	R0, #4
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State66
;Recipe.c,343 :: 		ErrorLog_NewError(msgCycleFailDrain, STATUS_ERROR);
MOVS	R1, #1
MOVW	R0, #lo_addr(_msgCycleFailDrain+0)
MOVT	R0, #hi_addr(_msgCycleFailDrain+0)
BL	_ErrorLog_NewError+0
;Recipe.c,344 :: 		nState = ERR_PRE_HOLDING_STATE;
MOVS	R1, #100
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,345 :: 		}
IT	AL
BAL	L_Recipe_State67
L_Recipe_State66:
;Recipe.c,348 :: 		SetStatus(msgFilling, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgFilling+0)
MOVT	R0, #hi_addr(_msgFilling+0)
BL	_SetStatus+0
;Recipe.c,349 :: 		nState = PRECOOL_INIT;
MOVS	R1, #4
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,350 :: 		}
L_Recipe_State67:
;Recipe.c,351 :: 		}
L_Recipe_State65:
;Recipe.c,352 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,354 :: 		case PRECOOL_INIT:
L_Recipe_State68:
;Recipe.c,356 :: 		if (SwitchOnThree(TRUE))
MOVS	R0, #1
BL	_SwitchOnThree+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State69
;Recipe.c,358 :: 		nState = PRECOOL_FILL;
MOVS	R1, #5
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,359 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,360 :: 		}
L_Recipe_State69:
;Recipe.c,361 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,363 :: 		case PRECOOL_FILL:
L_Recipe_State70:
;Recipe.c,364 :: 		if (InputOn(WATER_LEVEL))
MOVS	R0, #4
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State71
;Recipe.c,366 :: 		SetOutput(WATER_IN, FALSE);
MOVS	R1, #0
MOVS	R0, #4
BL	_SetOutput+0
;Recipe.c,367 :: 		SetStatus(msgCooling, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgCooling+0)
MOVT	R0, #hi_addr(_msgCooling+0)
BL	_SetStatus+0
;Recipe.c,368 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,369 :: 		nState = PRECOOL_WAIT;
MOVS	R1, #6
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,370 :: 		}
IT	AL
BAL	L_Recipe_State72
L_Recipe_State71:
;Recipe.c,372 :: 		CheckFillingToLong();  // 130 HalfSeconds timeout
BL	_CheckFillingToLong+0
L_Recipe_State72:
;Recipe.c,374 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,376 :: 		case PRECOOL_WAIT:
L_Recipe_State73:
;Recipe.c,377 :: 		if (120 < HalfSeconds)     // was 340 HalfSeconds, but cooling is not effective after about 1 min.
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
CMP	R0, #120
IT	LS
BLS	L_Recipe_State74
;Recipe.c,379 :: 		if (SwitchOnThree(FALSE))  // turn off impeller, drain, turn off water (already off)
MOVS	R0, #0
BL	_SwitchOnThree+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State75
;Recipe.c,381 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,382 :: 		SetStatus(msgDraining, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgDraining+0)
MOVT	R0, #hi_addr(_msgDraining+0)
BL	_SetStatus+0
;Recipe.c,383 :: 		nState = PRECOOL_DRAIN;
MOVS	R1, #7
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,384 :: 		}
L_Recipe_State75:
;Recipe.c,385 :: 		}
L_Recipe_State74:
;Recipe.c,387 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,389 :: 		case PRECOOL_DRAIN:
L_Recipe_State76:
;Recipe.c,390 :: 		if ((fDrainTime.fValue*2) < HalfSeconds)
MOVW	R0, #lo_addr(_fDrainTime+4)
MOVT	R0, #hi_addr(_fDrainTime+4)
VLDR	#1, S1, [R0, #0]
VMOV.F32	S0, #2
VMUL.F32	S1, S1, S0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
VMOV	S0, R0
VCVT.F32	#0, S0, S0
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	GE
BGE	L_Recipe_State77
;Recipe.c,392 :: 		if (InputOn(WATER_LEVEL))
MOVS	R0, #4
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State78
;Recipe.c,394 :: 		ErrorLog_NewError(msgCycleFailDrain, STATUS_ERROR);
MOVS	R1, #1
MOVW	R0, #lo_addr(_msgCycleFailDrain+0)
MOVT	R0, #hi_addr(_msgCycleFailDrain+0)
BL	_ErrorLog_NewError+0
;Recipe.c,395 :: 		nState = ERR_PRE_HOLDING_STATE;
MOVS	R1, #100
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,396 :: 		}
IT	AL
BAL	L_Recipe_State79
L_Recipe_State78:
;Recipe.c,398 :: 		nState = SANTIZ_INIT;
MOVS	R1, #8
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
L_Recipe_State79:
;Recipe.c,399 :: 		}
L_Recipe_State77:
;Recipe.c,401 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,403 :: 		case SANTIZ_INIT:
L_Recipe_State80:
;Recipe.c,404 :: 		nState = SANTIZ_FILL;
MOVS	R1, #9
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,405 :: 		nSanCycle = 1;  // moved here, better to reset before first cycle rather than after successful cycle - CM 4/7/17
MOVS	R1, #1
MOVW	R0, #lo_addr(_nSanCycle+0)
MOVT	R0, #hi_addr(_nSanCycle+0)
STRB	R1, [R0, #0]
;Recipe.c,406 :: 		FillWasher();  // stop drain, turn water on, set "Filling" message on screen
BL	_FillWasher+0
;Recipe.c,407 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,408 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,410 :: 		case SANTIZ_FILL:
L_Recipe_State81:
;Recipe.c,411 :: 		if (InputOn(WATER_LEVEL))
MOVS	R0, #4
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State82
;Recipe.c,413 :: 		nState = SANTIZ_FILL_EXTRA;
MOVS	R1, #10
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,414 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,415 :: 		}
IT	AL
BAL	L_Recipe_State83
L_Recipe_State82:
;Recipe.c,417 :: 		CheckFillingToLong(); // 130 HalfSeconds timeout
BL	_CheckFillingToLong+0
L_Recipe_State83:
;Recipe.c,418 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,420 :: 		case SANTIZ_FILL_EXTRA:
L_Recipe_State84:
;Recipe.c,422 :: 		if (InputOn(WATER_LEVEL) && ((fExtraFillTime.fValue*2) < HalfSeconds))
MOVS	R0, #4
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L__Recipe_State190
MOVW	R0, #lo_addr(_fExtraFillTime+4)
MOVT	R0, #hi_addr(_fExtraFillTime+4)
VLDR	#1, S1, [R0, #0]
VMOV.F32	S0, #2
VMUL.F32	S1, S1, S0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
VMOV	S0, R0
VCVT.F32	#0, S0, S0
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	GE
BGE	L__Recipe_State189
L__Recipe_State178:
;Recipe.c,424 :: 		SetOutput(WATER_IN, FALSE);
MOVS	R1, #0
MOVS	R0, #4
BL	_SetOutput+0
;Recipe.c,425 :: 		delay_ms(SWITCH_DELAY);
MOVW	R7, #15827
MOVT	R7, #14
NOP
NOP
L_Recipe_State88:
SUBS	R7, R7, #1
BNE	L_Recipe_State88
NOP
NOP
NOP
NOP
;Recipe.c,426 :: 		SetOutput(IMPELLOR, TRUE);
MOVS	R1, #1
MOVS	R0, #2
BL	_SetOutput+0
;Recipe.c,428 :: 		fSetTemp = SANITIZING_TEMP + 0.9;
MOVW	R0, #52429
MOVT	R0, #17067
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,429 :: 		nState = SANTIZ_HEAT;
MOVS	R1, #11
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,430 :: 		sprintf(msgHeating, HEATING_TO, SANITIZING_TEMP);
MOVS	R2, #85
SXTB	R2, R2
MOVW	R1, #lo_addr(?lstr_17_Recipe+0)
MOVT	R1, #hi_addr(?lstr_17_Recipe+0)
MOVW	R0, #lo_addr(_msgHeating+0)
MOVT	R0, #hi_addr(_msgHeating+0)
PUSH	(R2)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #12
;Recipe.c,431 :: 		SetStatus(msgHeating, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgHeating+0)
MOVT	R0, #hi_addr(_msgHeating+0)
BL	_SetStatus+0
;Recipe.c,432 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,422 :: 		if (InputOn(WATER_LEVEL) && ((fExtraFillTime.fValue*2) < HalfSeconds))
L__Recipe_State190:
L__Recipe_State189:
;Recipe.c,434 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,436 :: 		case SANTIZ_HEAT:
L_Recipe_State90:
;Recipe.c,438 :: 		if (bAtTemp)
MOVW	R0, #lo_addr(_bAtTemp+0)
MOVT	R0, #hi_addr(_bAtTemp+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State91
;Recipe.c,440 :: 		nState = SANTIZ_WAIT;
MOVS	R1, #12
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,441 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,442 :: 		sprintf(msgSanitizing, "Sanitizing  %1d", nSanCycle);
MOVW	R0, #lo_addr(_nSanCycle+0)
MOVT	R0, #hi_addr(_nSanCycle+0)
LDRB	R2, [R0, #0]
MOVW	R1, #lo_addr(?lstr_18_Recipe+0)
MOVT	R1, #hi_addr(?lstr_18_Recipe+0)
MOVW	R0, #lo_addr(_msgSanitizing+0)
MOVT	R0, #hi_addr(_msgSanitizing+0)
PUSH	(R2)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #12
;Recipe.c,443 :: 		SetStatus(msgSanitizing, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgSanitizing+0)
MOVT	R0, #hi_addr(_msgSanitizing+0)
BL	_SetStatus+0
;Recipe.c,444 :: 		}
IT	AL
BAL	L_Recipe_State92
L_Recipe_State91:
;Recipe.c,445 :: 		else if (HEATINGTIMEOUT < HalfSeconds)    // 1800 HalfSeconds = 15 min
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
CMP	R0, #1800
IT	LS
BLS	L_Recipe_State93
;Recipe.c,447 :: 		ErrorLog_NewError(msgCycleFailHeat, STATUS_ERROR);
MOVS	R1, #1
MOVW	R0, #lo_addr(_msgCycleFailHeat+0)
MOVT	R0, #hi_addr(_msgCycleFailHeat+0)
BL	_ErrorLog_NewError+0
;Recipe.c,448 :: 		nState = ERR_PRE_HOLDING_STATE;
MOVS	R1, #100
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,449 :: 		}
L_Recipe_State93:
L_Recipe_State92:
;Recipe.c,450 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,452 :: 		case SANTIZ_WAIT:
L_Recipe_State94:
;Recipe.c,454 :: 		if (259 < HalfSeconds)     //239
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R1, [R0, #0]
MOVW	R0, #259
CMP	R1, R0
IT	LS
BLS	L_Recipe_State95
;Recipe.c,458 :: 		nState = SANTIZ_DRAIN;
MOVS	R1, #13
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,459 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,460 :: 		SetStatus(msgDraining, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgDraining+0)
MOVT	R0, #hi_addr(_msgDraining+0)
BL	_SetStatus+0
;Recipe.c,461 :: 		fSetTemp = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,462 :: 		SetOutput(DRAIN_CLOSED, FALSE);
MOVS	R1, #0
MOVS	R0, #5
BL	_SetOutput+0
;Recipe.c,463 :: 		SetOutput(IMPELLOR, FALSE);
MOVS	R1, #0
MOVS	R0, #2
BL	_SetOutput+0
;Recipe.c,464 :: 		}
IT	AL
BAL	L_Recipe_State96
L_Recipe_State95:
;Recipe.c,472 :: 		if (GetDisplayTemp() < 85)
BL	_GetDisplayTemp+0
MOVW	R0, #0
MOVT	R0, #17066
VMOV	S1, R0
VCMPE.F32	S0, S1
VMRS	#60, FPSCR
IT	GE
BGE	L_Recipe_State97
;Recipe.c,475 :: 		nSanCycle++;
MOVW	R1, #lo_addr(_nSanCycle+0)
MOVT	R1, #hi_addr(_nSanCycle+0)
STR	R1, [SP, #4]
LDRB	R0, [R1, #0]
ADDS	R0, R0, #1
STRB	R0, [R1, #0]
;Recipe.c,476 :: 		sprintf(msgTestHeating, "Temp Fail %2.1f", GetDisplayTemp()); // was GetCurrentTemp() - CM
BL	_GetDisplayTemp+0
MOVW	R1, #lo_addr(?lstr_19_Recipe+0)
MOVT	R1, #hi_addr(?lstr_19_Recipe+0)
MOVW	R0, #lo_addr(_msgTestHeating+0)
MOVT	R0, #hi_addr(_msgTestHeating+0)
VPUSH	#0, (S0)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #12
;Recipe.c,477 :: 		SetStatus(msgTestHeating, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgTestHeating+0)
MOVT	R0, #hi_addr(_msgTestHeating+0)
BL	_SetStatus+0
;Recipe.c,478 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,479 :: 		if (4 <  nSanCycle)  // failed to heat  after 4
LDR	R0, [SP, #4]
LDRB	R0, [R0, #0]
CMP	R0, #4
IT	LS
BLS	L_Recipe_State98
;Recipe.c,481 :: 		ErrorLog_NewError(msgTestHeating, STATUS_ERROR);
MOVS	R1, #1
MOVW	R0, #lo_addr(_msgTestHeating+0)
MOVT	R0, #hi_addr(_msgTestHeating+0)
BL	_ErrorLog_NewError+0
;Recipe.c,482 :: 		nState = ERR_PRE_HOLDING_STATE;
MOVS	R1, #100
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,483 :: 		}
IT	AL
BAL	L_Recipe_State99
L_Recipe_State98:
;Recipe.c,485 :: 		nState = SANTIZ_HEAT;
MOVS	R1, #11
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
L_Recipe_State99:
;Recipe.c,486 :: 		}
L_Recipe_State97:
;Recipe.c,487 :: 		}
L_Recipe_State96:
;Recipe.c,488 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,491 :: 		case SANTIZ_DRAIN:  // 13
L_Recipe_State100:
;Recipe.c,492 :: 		if (((fDrainTime.fValue*2) + 2*(fExtraFillTime.fValue*2)) < HalfSeconds) // add twice extra fill delay to drain additional water, CM 5/7/17
MOVW	R0, #lo_addr(_fDrainTime+4)
MOVT	R0, #hi_addr(_fDrainTime+4)
VLDR	#1, S1, [R0, #0]
VMOV.F32	S0, #2
VMUL.F32	S2, S1, S0
MOVW	R0, #lo_addr(_fExtraFillTime+4)
MOVT	R0, #hi_addr(_fExtraFillTime+4)
VLDR	#1, S1, [R0, #0]
VMOV.F32	S0, #2
VMUL.F32	S1, S1, S0
VMOV.F32	S0, #2
VMUL.F32	S0, S0, S1
VADD.F32	S1, S2, S0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
VMOV	S0, R0
VCVT.F32	#0, S0, S0
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	GE
BGE	L_Recipe_State101
;Recipe.c,494 :: 		if (InputOn(WATER_LEVEL))
MOVS	R0, #4
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State102
;Recipe.c,496 :: 		ErrorLog_NewError(msgCycleFailDrain, STATUS_ERROR);
MOVS	R1, #1
MOVW	R0, #lo_addr(_msgCycleFailDrain+0)
MOVT	R0, #hi_addr(_msgCycleFailDrain+0)
BL	_ErrorLog_NewError+0
;Recipe.c,497 :: 		nState = ERR_PRE_HOLDING_STATE;
MOVS	R1, #100
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,498 :: 		}
IT	AL
BAL	L_Recipe_State103
L_Recipe_State102:
;Recipe.c,500 :: 		nState = POSTCOOL_INIT;
MOVS	R1, #14
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
L_Recipe_State103:
;Recipe.c,501 :: 		}
L_Recipe_State101:
;Recipe.c,502 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,504 :: 		case POSTCOOL_INIT:  // 14
L_Recipe_State104:
;Recipe.c,505 :: 		if (SwitchOnThree(TRUE))    // water in, impeller on, drain closed
MOVS	R0, #1
BL	_SwitchOnThree+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State105
;Recipe.c,507 :: 		nState = POSTCOOL_FILL;
MOVS	R1, #15
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,508 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,509 :: 		if (!bErrorAfterCooling)
MOVW	R0, #lo_addr(_bErrorAfterCooling+0)
MOVT	R0, #hi_addr(_bErrorAfterCooling+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	NE
BNE	L_Recipe_State106
;Recipe.c,510 :: 		SetStatus(msgFilling, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgFilling+0)
MOVT	R0, #hi_addr(_msgFilling+0)
BL	_SetStatus+0
L_Recipe_State106:
;Recipe.c,511 :: 		}
L_Recipe_State105:
;Recipe.c,512 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,514 :: 		case POSTCOOL_FILL:  // 15
L_Recipe_State107:
;Recipe.c,515 :: 		if (InputOn(WATER_LEVEL) || (GetCurrentTemp() <= SAFE_COOL_TEMP))
MOVS	R0, #4
BL	_InputOn+0
CMP	R0, #0
IT	NE
BNE	L__Recipe_State192
BL	_GetCurrentTemp+0
MOVW	R0, #0
MOVT	R0, #16908
VMOV	S1, R0
VCMPE.F32	S0, S1
VMRS	#60, FPSCR
IT	LE
BLE	L__Recipe_State191
IT	AL
BAL	L_Recipe_State110
L__Recipe_State192:
L__Recipe_State191:
;Recipe.c,517 :: 		nState = POSTCOOL_FILL_EXT;
MOVS	R1, #16
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,518 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,519 :: 		}
IT	AL
BAL	L_Recipe_State111
L_Recipe_State110:
;Recipe.c,521 :: 		CheckFillingToLong();
BL	_CheckFillingToLong+0
L_Recipe_State111:
;Recipe.c,523 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,525 :: 		case POSTCOOL_FILL_EXT:  //16
L_Recipe_State112:
;Recipe.c,526 :: 		if ( (fExtraFillTime.fValue*2) < HalfSeconds || (GetCurrentTemp() <= SAFE_COOL_TEMP)) // add extra water for faster cooling - CM 12/7/17
MOVW	R0, #lo_addr(_fExtraFillTime+4)
MOVT	R0, #hi_addr(_fExtraFillTime+4)
VLDR	#1, S1, [R0, #0]
VMOV.F32	S0, #2
VMUL.F32	S1, S1, S0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
VMOV	S0, R0
VCVT.F32	#0, S0, S0
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	LT
BLT	L__Recipe_State194
BL	_GetCurrentTemp+0
MOVW	R0, #0
MOVT	R0, #16908
VMOV	S1, R0
VCMPE.F32	S0, S1
VMRS	#60, FPSCR
IT	LE
BLE	L__Recipe_State193
IT	AL
BAL	L_Recipe_State115
L__Recipe_State194:
L__Recipe_State193:
;Recipe.c,528 :: 		SetOutput(WATER_IN, FALSE);
MOVS	R1, #0
MOVS	R0, #4
BL	_SetOutput+0
;Recipe.c,529 :: 		if (!bErrorAfterCooling)
MOVW	R0, #lo_addr(_bErrorAfterCooling+0)
MOVT	R0, #hi_addr(_bErrorAfterCooling+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	NE
BNE	L_Recipe_State116
;Recipe.c,530 :: 		SetStatus(msgCooling, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgCooling+0)
MOVT	R0, #hi_addr(_msgCooling+0)
BL	_SetStatus+0
L_Recipe_State116:
;Recipe.c,531 :: 		nState = POSTCOOL_WAIT;
MOVS	R1, #17
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,532 :: 		}
L_Recipe_State115:
;Recipe.c,533 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,535 :: 		case POSTCOOL_WAIT:  // 17
L_Recipe_State117:
;Recipe.c,538 :: 		if ((340 < HalfSeconds) || (GetCurrentTemp() <= SAFE_COOL_TEMP))
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
CMP	R0, #340
IT	HI
BHI	L__Recipe_State196
BL	_GetCurrentTemp+0
MOVW	R0, #0
MOVT	R0, #16908
VMOV	S1, R0
VCMPE.F32	S0, S1
VMRS	#60, FPSCR
IT	LE
BLE	L__Recipe_State195
IT	AL
BAL	L_Recipe_State120
L__Recipe_State196:
L__Recipe_State195:
;Recipe.c,540 :: 		if (SwitchOnThree(FALSE)) // water off, impeller off, drain open
MOVS	R0, #0
BL	_SwitchOnThree+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State121
;Recipe.c,542 :: 		if (GetCurrentTemp() < SAFE_COOL_TEMP)
BL	_GetCurrentTemp+0
MOVW	R0, #0
MOVT	R0, #16908
VMOV	S1, R0
VCMPE.F32	S0, S1
VMRS	#60, FPSCR
IT	GE
BGE	L_Recipe_State122
;Recipe.c,544 :: 		if (bErrorAfterCooling)
MOVW	R0, #lo_addr(_bErrorAfterCooling+0)
MOVT	R0, #hi_addr(_bErrorAfterCooling+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State123
;Recipe.c,546 :: 		nState = ERR_HOLDING_STATE;
MOVS	R1, #101
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,547 :: 		}
IT	AL
BAL	L_Recipe_State124
L_Recipe_State123:
;Recipe.c,550 :: 		nState = WAIT_UNLOCK;
MOVS	R1, #18
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,551 :: 		delay_ms(SWITCH_DELAY);
MOVW	R7, #15827
MOVT	R7, #14
NOP
NOP
L_Recipe_State125:
SUBS	R7, R7, #1
BNE	L_Recipe_State125
NOP
NOP
NOP
NOP
;Recipe.c,552 :: 		SetOutput(CYCLE_COMPLETE_LIGHT, TRUE);
MOVS	R1, #1
MOVS	R0, #1
BL	_SetOutput+0
;Recipe.c,553 :: 		fCyclesComplete += 1;
MOVW	R0, #lo_addr(_fCyclesComplete+0)
MOVT	R0, #hi_addr(_fCyclesComplete+0)
VLDR	#1, S1, [R0, #0]
VMOV.F32	S0, #1
VADD.F32	S0, S1, S0
VSTR	#1, S0, [R0, #0]
;Recipe.c,554 :: 		sprintf(CyclesComplete, "%6.0f", fCyclesComplete);
VMOV.F32	S0, S0
MOVW	R1, #lo_addr(?lstr_20_Recipe+0)
MOVT	R1, #hi_addr(?lstr_20_Recipe+0)
MOVW	R0, #lo_addr(_CyclesComplete+0)
MOVT	R0, #hi_addr(_CyclesComplete+0)
VPUSH	#0, (S0)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #12
;Recipe.c,555 :: 		UpDateMemory();
BL	_UpDateMemory+0
;Recipe.c,556 :: 		SetStatus(msgCycleCom, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgCycleCom+0)
MOVT	R0, #hi_addr(_msgCycleCom+0)
BL	_SetStatus+0
;Recipe.c,557 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,558 :: 		}
L_Recipe_State124:
;Recipe.c,559 :: 		}
IT	AL
BAL	L_Recipe_State127
L_Recipe_State122:
;Recipe.c,562 :: 		nState = SANTIZ_DRAIN;
MOVS	R1, #13
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,563 :: 		if (!bErrorAfterCooling)
MOVW	R0, #lo_addr(_bErrorAfterCooling+0)
MOVT	R0, #hi_addr(_bErrorAfterCooling+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	NE
BNE	L_Recipe_State128
;Recipe.c,564 :: 		SetStatus(msgDraining, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgDraining+0)
MOVT	R0, #hi_addr(_msgDraining+0)
BL	_SetStatus+0
L_Recipe_State128:
;Recipe.c,565 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,566 :: 		}
L_Recipe_State127:
;Recipe.c,567 :: 		}
L_Recipe_State121:
;Recipe.c,568 :: 		}
L_Recipe_State120:
;Recipe.c,570 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,572 :: 		case WAIT_UNLOCK: // 14 "Cycle Complete"
L_Recipe_State129:
;Recipe.c,573 :: 		if (InputOn(LID_OPEN_BTN))
MOVS	R0, #2
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State130
;Recipe.c,575 :: 		SetOutput(CYCLE_RUN_LIGHT, FALSE);
MOVS	R1, #0
MOVS	R0, #0
BL	_SetOutput+0
;Recipe.c,576 :: 		SetOutput(CYCLE_COMPLETE_LIGHT, FALSE);
MOVS	R1, #0
MOVS	R0, #1
BL	_SetOutput+0
;Recipe.c,577 :: 		SetOutput(LID_UNLOCK, TRUE);
MOVS	R1, #1
MOVS	R0, #3
BL	_SetOutput+0
;Recipe.c,578 :: 		nState = WAITING_START;
MOVS	R1, #0
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,579 :: 		SetStatus(msgWaitingStart, STATUS_NORMAL);
MOVS	R1, #0
MOVW	R0, #lo_addr(_msgWaitingStart+0)
MOVT	R0, #hi_addr(_msgWaitingStart+0)
BL	_SetStatus+0
;Recipe.c,580 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,581 :: 		}
L_Recipe_State130:
;Recipe.c,594 :: 		}
L_Recipe_State131:
;Recipe.c,595 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,599 :: 		case ERR_PRE_HOLDING_STATE:
L_Recipe_State133:
;Recipe.c,600 :: 		fSetTemp = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,601 :: 		if (SwitchOnThree(FALSE))           // turn off water & impeller, open drain
MOVS	R0, #0
BL	_SwitchOnThree+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State134
;Recipe.c,603 :: 		if (bErrorAfterCooling)
MOVW	R0, #lo_addr(_bErrorAfterCooling+0)
MOVT	R0, #hi_addr(_bErrorAfterCooling+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State135
;Recipe.c,606 :: 		nState = ERR_HOLDING_STATE;
MOVS	R1, #101
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,607 :: 		}
IT	AL
BAL	L_Recipe_State136
L_Recipe_State135:
;Recipe.c,612 :: 		bErrorAfterCooling = TRUE;
MOVS	R1, #1
MOVW	R0, #lo_addr(_bErrorAfterCooling+0)
MOVT	R0, #hi_addr(_bErrorAfterCooling+0)
STRB	R1, [R0, #0]
;Recipe.c,615 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,616 :: 		nState = SANTIZ_DRAIN;
MOVS	R1, #13
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,617 :: 		}
L_Recipe_State136:
;Recipe.c,618 :: 		}
L_Recipe_State134:
;Recipe.c,620 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,622 :: 		case ERR_HOLDING_STATE:
L_Recipe_State137:
;Recipe.c,624 :: 		if (InputOn(START_BTN) && InputOn(LID_SHUT) && !InputOn(ELEMENT_FAULT) && !InputOn(HEATER_MAIN_ON) && !bGlobalTempAlarm)
MOVS	R0, #1
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L__Recipe_State201
MOVS	R0, #0
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L__Recipe_State200
MOVS	R0, #5
BL	_InputOn+0
CMP	R0, #0
IT	NE
BNE	L__Recipe_State199
MOVS	R0, #6
BL	_InputOn+0
CMP	R0, #0
IT	NE
BNE	L__Recipe_State198
MOVW	R0, #lo_addr(_bGlobalTempAlarm+0)
MOVT	R0, #hi_addr(_bGlobalTempAlarm+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	NE
BNE	L__Recipe_State197
L__Recipe_State174:
;Recipe.c,626 :: 		fSetTemp = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,627 :: 		bErrorAfterCooling = FALSE;
MOVS	R1, #0
MOVW	R0, #lo_addr(_bErrorAfterCooling+0)
MOVT	R0, #hi_addr(_bErrorAfterCooling+0)
STRB	R1, [R0, #0]
;Recipe.c,628 :: 		nState = PREWASH_FILL;
MOVS	R1, #1
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,629 :: 		SetOutput(CYCLE_RUN_LIGHT, TRUE);
MOVS	R1, #1
MOVS	R0, #0
BL	_SetOutput+0
;Recipe.c,630 :: 		SetOutput(LID_UNLOCK, FALSE);
MOVS	R1, #0
MOVS	R0, #3
BL	_SetOutput+0
;Recipe.c,631 :: 		FillWasher();
BL	_FillWasher+0
;Recipe.c,632 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,624 :: 		if (InputOn(START_BTN) && InputOn(LID_SHUT) && !InputOn(ELEMENT_FAULT) && !InputOn(HEATER_MAIN_ON) && !bGlobalTempAlarm)
L__Recipe_State201:
L__Recipe_State200:
L__Recipe_State199:
L__Recipe_State198:
L__Recipe_State197:
;Recipe.c,635 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,638 :: 		case CAL_START:
L_Recipe_State141:
;Recipe.c,640 :: 		if(!InputOn(WATER_LEVEL)){
MOVS	R0, #4
BL	_InputOn+0
CMP	R0, #0
IT	NE
BNE	L_Recipe_State142
;Recipe.c,641 :: 		fillWasher();
BL	_FillWasher+0
;Recipe.c,642 :: 		SetTextLabel(cal_message_label, msgFilling);
MOVW	R0, #lo_addr(_cal_message_label+0)
MOVT	R0, #hi_addr(_cal_message_label+0)
SUB	SP, SP, #48
MOV	R12, R0
ADD	R11, SP, #0
ADD	R10, R11, #48
BL	___CC2DW+0
MOVW	R0, #lo_addr(_msgFilling+0)
MOVT	R0, #hi_addr(_msgFilling+0)
BL	_SetTextLabel+0
ADD	SP, SP, #48
;Recipe.c,643 :: 		}
L_Recipe_State142:
;Recipe.c,644 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,645 :: 		nState = CAL_FILL;
MOVS	R1, #201
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,646 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,648 :: 		case CAL_FILL:
L_Recipe_State143:
;Recipe.c,649 :: 		if (InputOn(WATER_LEVEL))
MOVS	R0, #4
BL	_InputOn+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State144
;Recipe.c,652 :: 		SetOutput(WATER_IN, FALSE);
MOVS	R1, #0
MOVS	R0, #4
BL	_SetOutput+0
;Recipe.c,653 :: 		delay_ms(SWITCH_DELAY);
MOVW	R7, #15827
MOVT	R7, #14
NOP
NOP
L_Recipe_State145:
SUBS	R7, R7, #1
BNE	L_Recipe_State145
NOP
NOP
NOP
NOP
;Recipe.c,654 :: 		SetOutput(IMPELLOR, TRUE);
MOVS	R1, #1
MOVS	R0, #2
BL	_SetOutput+0
;Recipe.c,656 :: 		fSetTemp = CAL_TEMP_LOW;
MOVW	R0, #0
MOVT	R0, #16916
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,657 :: 		sprintf(msgCalHeating, HEATING_TO, CAL_TEMP_LOW);
MOVS	R2, #37
SXTB	R2, R2
MOVW	R1, #lo_addr(?lstr_21_Recipe+0)
MOVT	R1, #hi_addr(?lstr_21_Recipe+0)
MOVW	R0, #lo_addr(_msgCalHeating+0)
MOVT	R0, #hi_addr(_msgCalHeating+0)
PUSH	(R2)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #12
;Recipe.c,658 :: 		SetTextLabel(cal_message_label, msgCalHeating);
MOVW	R0, #lo_addr(_cal_message_label+0)
MOVT	R0, #hi_addr(_cal_message_label+0)
SUB	SP, SP, #48
MOV	R12, R0
ADD	R11, SP, #0
ADD	R10, R11, #48
BL	___CC2DW+0
MOVW	R0, #lo_addr(_msgCalHeating+0)
MOVT	R0, #hi_addr(_msgCalHeating+0)
BL	_SetTextLabel+0
ADD	SP, SP, #48
;Recipe.c,661 :: 		cal_temp_label.Visible = TRUE;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_temp_label+27)
MOVT	R0, #hi_addr(_cal_temp_label+27)
STRB	R1, [R0, #0]
;Recipe.c,662 :: 		fLastTempDisplayed = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fLastTempDisplayed+0)
MOVT	R0, #hi_addr(_fLastTempDisplayed+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,664 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,665 :: 		nState = CAL_HOLD_TEMP_LOW;
MOVS	R1, #202
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,666 :: 		}
IT	AL
BAL	L_Recipe_State147
L_Recipe_State144:
;Recipe.c,669 :: 		CheckFillingToLong(); // 130 HalfSeconds timeout
BL	_CheckFillingToLong+0
L_Recipe_State147:
;Recipe.c,670 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,672 :: 		case CAL_HOLD_TEMP_LOW:
L_Recipe_State148:
;Recipe.c,675 :: 		fCurrentTemp = GetDisplayTemp();
BL	_GetDisplayTemp+0
MOVW	R0, #lo_addr(_fCurrentTemp+0)
MOVT	R0, #hi_addr(_fCurrentTemp+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,676 :: 		if (fLastTempDisplayed != fCurrentTemp){
MOVW	R0, #lo_addr(_fLastTempDisplayed+0)
MOVT	R0, #hi_addr(_fLastTempDisplayed+0)
VLDR	#1, S1, [R0, #0]
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	EQ
BEQ	L_Recipe_State149
;Recipe.c,677 :: 		SetFloatLabel(cal_temp_label, "%3.1f", fCurrentTemp);
MOVW	R0, #lo_addr(_fCurrentTemp+0)
MOVT	R0, #hi_addr(_fCurrentTemp+0)
VLDR	#1, S0, [R0, #0]
MOVW	R1, #lo_addr(?lstr22_Recipe+0)
MOVT	R1, #hi_addr(?lstr22_Recipe+0)
MOVW	R0, #lo_addr(_cal_temp_label+0)
MOVT	R0, #hi_addr(_cal_temp_label+0)
SUB	SP, SP, #48
MOV	R12, R0
ADD	R11, SP, #0
ADD	R10, R11, #48
BL	___CC2DW+0
MOV	R0, R1
BL	_SetFloatLabel+0
ADD	SP, SP, #48
;Recipe.c,678 :: 		fLastTempDisplayed = fCurrentTemp;
MOVW	R0, #lo_addr(_fCurrentTemp+0)
MOVT	R0, #hi_addr(_fCurrentTemp+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fLastTempDisplayed+0)
MOVT	R0, #hi_addr(_fLastTempDisplayed+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,679 :: 		}
L_Recipe_State149:
;Recipe.c,681 :: 		if (bAtTemp && HalfSeconds > 4) {
MOVW	R0, #lo_addr(_bAtTemp+0)
MOVT	R0, #hi_addr(_bAtTemp+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L__Recipe_State203
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
CMP	R0, #4
IT	LS
BLS	L__Recipe_State202
L__Recipe_State173:
;Recipe.c,683 :: 		fMeasuredLow.fValue = 0.0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fMeasuredLow+4)
MOVT	R0, #hi_addr(_fMeasuredLow+4)
VSTR	#1, S0, [R0, #0]
;Recipe.c,684 :: 		Display_Keyboard(msgEnterCalTemp, &fMeasuredLow);
MOVW	R1, #lo_addr(_fMeasuredLow+0)
MOVT	R1, #hi_addr(_fMeasuredLow+0)
MOVW	R0, #lo_addr(_msgEnterCalTemp+0)
MOVT	R0, #hi_addr(_msgEnterCalTemp+0)
BL	_Display_Keyboard+0
;Recipe.c,685 :: 		nState = CAL_ENTER_LOW_TEMP;
MOVS	R1, #203
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,686 :: 		} else if (HEATINGTIMEOUT < HalfSeconds) {   // 1800 HalfSeconds = 15 min
IT	AL
BAL	L_Recipe_State153
;Recipe.c,681 :: 		if (bAtTemp && HalfSeconds > 4) {
L__Recipe_State203:
L__Recipe_State202:
;Recipe.c,686 :: 		} else if (HEATINGTIMEOUT < HalfSeconds) {   // 1800 HalfSeconds = 15 min
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
CMP	R0, #1800
IT	LS
BLS	L_Recipe_State154
;Recipe.c,687 :: 		ErrorLog_NewError(msgCycleFailHeat, STATUS_ERROR);
MOVS	R1, #1
MOVW	R0, #lo_addr(_msgCycleFailHeat+0)
MOVT	R0, #hi_addr(_msgCycleFailHeat+0)
BL	_ErrorLog_NewError+0
;Recipe.c,688 :: 		nState = ERR_PRE_HOLDING_STATE;
MOVS	R1, #100
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,689 :: 		}
L_Recipe_State154:
L_Recipe_State153:
;Recipe.c,691 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,693 :: 		case CAL_ENTER_LOW_TEMP:
L_Recipe_State155:
;Recipe.c,694 :: 		if (CurrentScreen!=&Keypad) {
MOVW	R0, #lo_addr(_CurrentScreen+0)
MOVT	R0, #hi_addr(_CurrentScreen+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_Keypad+0)
MOVT	R0, #hi_addr(_Keypad+0)
CMP	R1, R0
IT	EQ
BEQ	L_Recipe_State156
;Recipe.c,696 :: 		if (fMeasuredLow.fValue != 0.0){
MOVW	R0, #lo_addr(_fMeasuredLow+4)
MOVT	R0, #hi_addr(_fMeasuredLow+4)
VLDR	#1, S0, [R0, #0]
VCMPE.F32	S0, #0
VMRS	#60, FPSCR
IT	EQ
BEQ	L_Recipe_State157
;Recipe.c,697 :: 		fSetTemp = CAL_TEMP_HIGH;
MOVW	R0, #0
MOVT	R0, #17008
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,698 :: 		sprintf(msgCalHeating, HEATING_TO, CAL_TEMP_HIGH);
MOVS	R2, #60
SXTB	R2, R2
MOVW	R1, #lo_addr(?lstr_23_Recipe+0)
MOVT	R1, #hi_addr(?lstr_23_Recipe+0)
MOVW	R0, #lo_addr(_msgCalHeating+0)
MOVT	R0, #hi_addr(_msgCalHeating+0)
PUSH	(R2)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #12
;Recipe.c,699 :: 		SetTextLabel(cal_message_label, msgCalHeating);
MOVW	R0, #lo_addr(_cal_message_label+0)
MOVT	R0, #hi_addr(_cal_message_label+0)
SUB	SP, SP, #48
MOV	R12, R0
ADD	R11, SP, #0
ADD	R10, R11, #48
BL	___CC2DW+0
MOVW	R0, #lo_addr(_msgCalHeating+0)
MOVT	R0, #hi_addr(_msgCalHeating+0)
BL	_SetTextLabel+0
ADD	SP, SP, #48
;Recipe.c,700 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,701 :: 		nState = CAL_HOLD_TEMP_HIGH;
MOVS	R1, #204
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,702 :: 		} else {
IT	AL
BAL	L_Recipe_State158
L_Recipe_State157:
;Recipe.c,703 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,704 :: 		nState = CAL_HOLD_TEMP_LOW;
MOVS	R1, #202
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,705 :: 		}
L_Recipe_State158:
;Recipe.c,706 :: 		}
L_Recipe_State156:
;Recipe.c,707 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,709 :: 		case CAL_HOLD_TEMP_HIGH:
L_Recipe_State159:
;Recipe.c,711 :: 		fCurrentTemp = GetDisplayTemp();
BL	_GetDisplayTemp+0
MOVW	R0, #lo_addr(_fCurrentTemp+0)
MOVT	R0, #hi_addr(_fCurrentTemp+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,712 :: 		if (fLastTempDisplayed != fCurrentTemp){
MOVW	R0, #lo_addr(_fLastTempDisplayed+0)
MOVT	R0, #hi_addr(_fLastTempDisplayed+0)
VLDR	#1, S1, [R0, #0]
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	EQ
BEQ	L_Recipe_State160
;Recipe.c,713 :: 		SetFloatLabel(cal_temp_label, "%3.1f", fCurrentTemp);
MOVW	R0, #lo_addr(_fCurrentTemp+0)
MOVT	R0, #hi_addr(_fCurrentTemp+0)
VLDR	#1, S0, [R0, #0]
MOVW	R1, #lo_addr(?lstr24_Recipe+0)
MOVT	R1, #hi_addr(?lstr24_Recipe+0)
MOVW	R0, #lo_addr(_cal_temp_label+0)
MOVT	R0, #hi_addr(_cal_temp_label+0)
SUB	SP, SP, #48
MOV	R12, R0
ADD	R11, SP, #0
ADD	R10, R11, #48
BL	___CC2DW+0
MOV	R0, R1
BL	_SetFloatLabel+0
ADD	SP, SP, #48
;Recipe.c,714 :: 		fLastTempDisplayed = fCurrentTemp;
MOVW	R0, #lo_addr(_fCurrentTemp+0)
MOVT	R0, #hi_addr(_fCurrentTemp+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fLastTempDisplayed+0)
MOVT	R0, #hi_addr(_fLastTempDisplayed+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,715 :: 		}
L_Recipe_State160:
;Recipe.c,717 :: 		if (bAtTemp && HalfSeconds > 4) {
MOVW	R0, #lo_addr(_bAtTemp+0)
MOVT	R0, #hi_addr(_bAtTemp+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L__Recipe_State205
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
CMP	R0, #4
IT	LS
BLS	L__Recipe_State204
L__Recipe_State172:
;Recipe.c,719 :: 		fMeasuredHigh.fValue = 0.0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fMeasuredHigh+4)
MOVT	R0, #hi_addr(_fMeasuredHigh+4)
VSTR	#1, S0, [R0, #0]
;Recipe.c,720 :: 		Display_Keyboard(msgEnterCalTemp, &fMeasuredHigh);
MOVW	R1, #lo_addr(_fMeasuredHigh+0)
MOVT	R1, #hi_addr(_fMeasuredHigh+0)
MOVW	R0, #lo_addr(_msgEnterCalTemp+0)
MOVT	R0, #hi_addr(_msgEnterCalTemp+0)
BL	_Display_Keyboard+0
;Recipe.c,721 :: 		nState = CAL_ENTER_HIGH_TEMP;
MOVS	R1, #205
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,722 :: 		} else if (HEATINGTIMEOUT < HalfSeconds) {   // 1800 HalfSeconds = 15 min
IT	AL
BAL	L_Recipe_State164
;Recipe.c,717 :: 		if (bAtTemp && HalfSeconds > 4) {
L__Recipe_State205:
L__Recipe_State204:
;Recipe.c,722 :: 		} else if (HEATINGTIMEOUT < HalfSeconds) {   // 1800 HalfSeconds = 15 min
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
LDRH	R0, [R0, #0]
CMP	R0, #1800
IT	LS
BLS	L_Recipe_State165
;Recipe.c,723 :: 		ErrorLog_NewError(msgCycleFailHeat, STATUS_ERROR);
MOVS	R1, #1
MOVW	R0, #lo_addr(_msgCycleFailHeat+0)
MOVT	R0, #hi_addr(_msgCycleFailHeat+0)
BL	_ErrorLog_NewError+0
;Recipe.c,724 :: 		nState = ERR_PRE_HOLDING_STATE;
MOVS	R1, #100
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,725 :: 		}
L_Recipe_State165:
L_Recipe_State164:
;Recipe.c,727 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,729 :: 		case CAL_ENTER_HIGH_TEMP:
L_Recipe_State166:
;Recipe.c,730 :: 		if (CurrentScreen!=&Keypad) {
MOVW	R0, #lo_addr(_CurrentScreen+0)
MOVT	R0, #hi_addr(_CurrentScreen+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_Keypad+0)
MOVT	R0, #hi_addr(_Keypad+0)
CMP	R1, R0
IT	EQ
BEQ	L_Recipe_State167
;Recipe.c,732 :: 		if (fMeasuredHigh.fValue != 0.0) {
MOVW	R0, #lo_addr(_fMeasuredHigh+4)
MOVT	R0, #hi_addr(_fMeasuredHigh+4)
VLDR	#1, S0, [R0, #0]
VCMPE.F32	S0, #0
VMRS	#60, FPSCR
IT	EQ
BEQ	L_Recipe_State168
;Recipe.c,734 :: 		float fCalFactorNew = (fMeasuredHigh.fvalue - fMeasuredLow.fvalue)/(CAL_TEMP_HIGH-CAL_TEMP_LOW);
MOVW	R0, #lo_addr(_fMeasuredLow+4)
MOVT	R0, #hi_addr(_fMeasuredLow+4)
VLDR	#1, S1, [R0, #0]
MOVW	R1, #lo_addr(_fMeasuredHigh+4)
MOVT	R1, #hi_addr(_fMeasuredHigh+4)
VLDR	#1, S0, [R1, #0]
VSUB.F32	S1, S0, S1
VMOV.F32	S0, #23
VDIV.F32	S2, S1, S0
;Recipe.c,735 :: 		float fCalOffsetNew =  fMeasuredHigh.fvalue - CAL_TEMP_HIGH*fCalFactorNew;
MOVW	R0, #0
MOVT	R0, #17008
VMOV	S0, R0
VMUL.F32	S1, S0, S2
MOV	R0, R1
VLDR	#1, S0, [R0, #0]
VSUB.F32	S1, S0, S1
;Recipe.c,738 :: 		fCalOffsetNew = (fCalFactorNew*fCal_Offset.fValue) + fCalOffsetNew;
MOVW	R1, #lo_addr(_fCal_offset+4)
MOVT	R1, #hi_addr(_fCal_offset+4)
VLDR	#1, S0, [R1, #0]
VMUL.F32	S0, S2, S0
VADD.F32	S1, S0, S1
;Recipe.c,739 :: 		fCalFactorNew = fCal_factor.fValue * fCalFactorNew;
MOVW	R0, #lo_addr(_fCal_factor+4)
MOVT	R0, #hi_addr(_fCal_factor+4)
VLDR	#1, S0, [R0, #0]
VMUL.F32	S0, S0, S2
;Recipe.c,740 :: 		fCal_Offset.fValue = fCalOffsetNew;
VSTR	#1, S1, [R1, #0]
;Recipe.c,741 :: 		fCal_factor.fValue = fCalFactorNew;
VSTR	#1, S0, [R0, #0]
;Recipe.c,744 :: 		set_and_format_value(&fCal_offset, fCal_Offset.fValue );
VMOV.F32	S0, S1
MOVW	R0, #lo_addr(_fCal_offset+0)
MOVT	R0, #hi_addr(_fCal_offset+0)
BL	_set_and_format_value+0
;Recipe.c,745 :: 		set_and_format_value(&fCal_factor, fCal_factor.fValue );
MOVW	R0, #lo_addr(_fCal_factor+4)
MOVT	R0, #hi_addr(_fCal_factor+4)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fCal_factor+0)
MOVT	R0, #hi_addr(_fCal_factor+0)
BL	_set_and_format_value+0
;Recipe.c,746 :: 		UpdateMemory();
BL	_UpDateMemory+0
;Recipe.c,748 :: 		CalibrationScreenVisible(TRUE);
MOVS	R0, #1
BL	_CalibrationScreenVisible+0
;Recipe.c,749 :: 		cal_temp_label.Visible = FALSE;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_temp_label+27)
MOVT	R0, #hi_addr(_cal_temp_label+27)
STRB	R1, [R0, #0]
;Recipe.c,751 :: 		cal_start_button.Caption = cal_start_button_caption;
MOVW	R1, #lo_addr(_cal_start_button_Caption+0)
MOVT	R1, #hi_addr(_cal_start_button_Caption+0)
MOVW	R0, #lo_addr(_cal_start_button+24)
MOVT	R0, #hi_addr(_cal_start_button+24)
STR	R1, [R0, #0]
;Recipe.c,752 :: 		DrawScreen(&Calibration);
MOVW	R0, #lo_addr(_Calibration+0)
MOVT	R0, #hi_addr(_Calibration+0)
BL	_DrawScreen+0
;Recipe.c,754 :: 		nState = CAL_STOP;
MOVS	R1, #206
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,755 :: 		} else {
IT	AL
BAL	L_Recipe_State169
L_Recipe_State168:
;Recipe.c,756 :: 		HalfSeconds = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_HalfSeconds+0)
MOVT	R0, #hi_addr(_HalfSeconds+0)
STRH	R1, [R0, #0]
;Recipe.c,757 :: 		nState = CAL_HOLD_TEMP_HIGH;
MOVS	R1, #204
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Recipe.c,758 :: 		}
L_Recipe_State169:
;Recipe.c,759 :: 		}
L_Recipe_State167:
;Recipe.c,760 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,762 :: 		case CAL_STOP:
L_Recipe_State170:
;Recipe.c,763 :: 		fSetTemp = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;Recipe.c,764 :: 		if (SwitchOnThree(FALSE))           // turn off water & impeller, open drain
MOVS	R0, #0
BL	_SwitchOnThree+0
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State171
;Recipe.c,765 :: 		nState = WAITING_START;
MOVS	R1, #0
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
L_Recipe_State171:
;Recipe.c,767 :: 		break;
IT	AL
BAL	L_Recipe_State35
;Recipe.c,770 :: 		}
L_Recipe_State34:
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_Recipe_State36
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #1
IT	EQ
BEQ	L_Recipe_State53
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #2
IT	EQ
BEQ	L_Recipe_State58
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #3
IT	EQ
BEQ	L_Recipe_State64
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #4
IT	EQ
BEQ	L_Recipe_State68
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #5
IT	EQ
BEQ	L_Recipe_State70
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #6
IT	EQ
BEQ	L_Recipe_State73
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #7
IT	EQ
BEQ	L_Recipe_State76
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #8
IT	EQ
BEQ	L_Recipe_State80
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #9
IT	EQ
BEQ	L_Recipe_State81
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #10
IT	EQ
BEQ	L_Recipe_State84
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #11
IT	EQ
BEQ	L_Recipe_State90
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #12
IT	EQ
BEQ	L_Recipe_State94
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #13
IT	EQ
BEQ	L_Recipe_State100
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #14
IT	EQ
BEQ	L_Recipe_State104
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #15
IT	EQ
BEQ	L_Recipe_State107
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #16
IT	EQ
BEQ	L_Recipe_State112
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #17
IT	EQ
BEQ	L_Recipe_State117
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #18
IT	EQ
BEQ	L_Recipe_State129
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #100
IT	EQ
BEQ	L_Recipe_State133
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #101
IT	EQ
BEQ	L_Recipe_State137
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #200
IT	EQ
BEQ	L_Recipe_State141
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #201
IT	EQ
BEQ	L_Recipe_State143
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #202
IT	EQ
BEQ	L_Recipe_State148
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #203
IT	EQ
BEQ	L_Recipe_State155
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #204
IT	EQ
BEQ	L_Recipe_State159
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #205
IT	EQ
BEQ	L_Recipe_State166
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #206
IT	EQ
BEQ	L_Recipe_State170
L_Recipe_State35:
;Recipe.c,771 :: 		}
L_end_Recipe_State:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _Recipe_State
Recipe____?ag:
SUB	SP, SP, #4
L_end_Recipe___?ag:
ADD	SP, SP, #4
BX	LR
; end of Recipe____?ag
