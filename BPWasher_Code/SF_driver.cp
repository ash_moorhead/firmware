#line 1 "C:/Software/BPWasher_Code/SF_driver.c"
#line 1 "c:/users/public/documents/mikroelektronika/mikroc pro for arm/include/built_in.h"
#line 1 "c:/software/bpwasher_code/sf_driver.h"
#line 27 "c:/software/bpwasher_code/sf_driver.h"
extern sfr sbit CS_Serial_Flash_bit;


static const unsigned short _SERIAL_FLASH_CMD_RDID = 0x9F;
static const unsigned short _SERIAL_FLASH_CMD_READ = 0x03;
static const unsigned short _SERIAL_FLASH_CMD_WRITE = 0x02;
static const unsigned short _SERIAL_FLASH_CMD_WREN = 0x06;
static const unsigned short _SERIAL_FLASH_CMD_RDSR = 0x05;
static const unsigned short _SERIAL_FLASH_CMD_ERASE = 0xC7;
static const unsigned short _SERIAL_FLASH_CMD_EWSR = 0x06;
static const unsigned short _SERIAL_FLASH_CMD_WRSR = 0x01;
static const unsigned short _SERIAL_FLASH_CMD_SER = 0xD8;


void SerialFlash_init();
void SerialFlash_WriteEnable();
unsigned char SerialFlash_IsWriteBusy();
void SerialFlash_WriteByte(unsigned char _data, unsigned long address);
void SerialFlash_WriteWord(unsigned int _data, unsigned long address);
unsigned char SerialFlash_ReadID(void);
unsigned char SerialFlash_ReadByte(unsigned long address);
unsigned int SerialFlash_ReadWord(unsigned long address);
unsigned char SerialFlash_WriteArray(unsigned long address, unsigned char* pData, unsigned int nCount);
void SerialFlash_ReadArray(unsigned long address, unsigned char* pData, unsigned int nCount);
void SerialFlash_ChipErase(void);
void SerialFlash_ResetWriteProtection();
void SerialFlash_SectorErase(unsigned long address);
#line 32 "C:/Software/BPWasher_Code/SF_driver.c"
void SerialFlash_init(){
 CS_Serial_Flash_bit = 1;
}
#line 43 "C:/Software/BPWasher_Code/SF_driver.c"
void SerialFlash_WriteEnable(){
 CS_Serial_Flash_bit = 0;
 SPI_Wr_Ptr(_SERIAL_FLASH_CMD_WREN);
 CS_Serial_Flash_bit = 1;
}
#line 56 "C:/Software/BPWasher_Code/SF_driver.c"
unsigned char SerialFlash_IsWriteBusy(){
 unsigned char temp;

 CS_Serial_Flash_bit = 0;
 SPI_Wr_Ptr(_SERIAL_FLASH_CMD_RDSR);
 temp = SPI_Rd_Ptr(0);
 CS_Serial_Flash_bit = 1;

 return (temp&0x01);
}
#line 74 "C:/Software/BPWasher_Code/SF_driver.c"
void SerialFlash_WriteByte(unsigned char _data, unsigned long address){
 SerialFlash_WriteEnable();

 CS_Serial_Flash_bit = 0;
 SPI_Wr_Ptr(_SERIAL_FLASH_CMD_WRITE);
 SPI_Wr_Ptr( ((char *)&address)[2] );
 SPI_Wr_Ptr( ((char *)&address)[1] );
 SPI_Wr_Ptr( ((char *)&address)[0] );
 SPI_Wr_Ptr(_data);
 CS_Serial_Flash_bit = 1;


 while(SerialFlash_isWriteBusy());
}
#line 96 "C:/Software/BPWasher_Code/SF_driver.c"
void SerialFlash_WriteWord(unsigned int _data, unsigned long address){
 SerialFlash_WriteByte( ((char *)&_data)[1] ,address);
 SerialFlash_WriteByte( ((char *)&_data)[0] ,address+1);
}
#line 108 "C:/Software/BPWasher_Code/SF_driver.c"
unsigned char SerialFlash_ReadID(void){
 unsigned char temp;

 CS_Serial_Flash_bit = 0;
 SPI_Wr_Ptr(_SERIAL_FLASH_CMD_RDID);
 temp = SPI_Rd_Ptr(0);
 CS_Serial_Flash_bit = 1;

 return temp;
}
#line 126 "C:/Software/BPWasher_Code/SF_driver.c"
unsigned char SerialFlash_ReadByte(unsigned long address){
 unsigned char temp;

 CS_Serial_Flash_bit = 0;

 SPI_Wr_Ptr(_SERIAL_FLASH_CMD_READ);
 SPI_Wr_Ptr( ((char *)&address)[2] );
 SPI_Wr_Ptr( ((char *)&address)[1] );
 SPI_Wr_Ptr( ((char *)&address)[0] );
 temp = SPI_Rd_Ptr(0);

 CS_Serial_Flash_bit = 1;
 return temp;
}
#line 148 "C:/Software/BPWasher_Code/SF_driver.c"
unsigned int SerialFlash_ReadWord(unsigned long address){
 unsigned int temp;

  ((char *)&temp)[1]  = SerialFlash_ReadByte(address);
  ((char *)&temp)[0]  = SerialFlash_ReadByte(address+1);

 return temp;
}
#line 165 "C:/Software/BPWasher_Code/SF_driver.c"
unsigned char SerialFlash_WriteArray(unsigned long address, unsigned char* pData, unsigned int nCount){
 unsigned long addr;
 unsigned char* pD;
 unsigned int counter;

 addr = address;
 pD = pData;



 for(counter = 0; counter < nCount; counter++){
 SerialFlash_WriteByte(*pD++, addr++);
 }




 for (counter=0; counter < nCount; counter++){
 if (*pData != SerialFlash_ReadByte(address))
 return 0;
 pData++;
 address++;
 }

 return 1;
}
#line 200 "C:/Software/BPWasher_Code/SF_driver.c"
void SerialFlash_ReadArray(unsigned long address, unsigned char* pData, unsigned int nCount){
 CS_Serial_Flash_bit = 0;
 SPI_Wr_Ptr(_SERIAL_FLASH_CMD_READ);
 SPI_Wr_Ptr( ((char *)&address)[2] );
 SPI_Wr_Ptr( ((char *)&address)[1] );
 SPI_Wr_Ptr( ((char *)&address)[0] );

 while(nCount--){
 *pData++ = SPI_Rd_Ptr(0);
 }
 CS_Serial_Flash_bit = 1;
}
#line 220 "C:/Software/BPWasher_Code/SF_driver.c"
void SerialFlash_ChipErase(void){

 SerialFlash_WriteEnable();

 CS_Serial_Flash_bit = 0;
 SPI_Wr_Ptr(_SERIAL_FLASH_CMD_ERASE);
 CS_Serial_Flash_bit = 1;


 while(SerialFlash_IsWriteBusy());
}
#line 239 "C:/Software/BPWasher_Code/SF_driver.c"
void SerialFlash_ResetWriteProtection(){

 CS_Serial_Flash_bit = 0;
 SPI_Wr_Ptr(_SERIAL_FLASH_CMD_EWSR);
 CS_Serial_Flash_bit = 1;

 CS_Serial_Flash_bit = 0;
 SPI_Wr_Ptr(_SERIAL_FLASH_CMD_EWSR);
 SPI_Wr_Ptr(0);
 CS_Serial_Flash_bit = 1;
}
#line 258 "C:/Software/BPWasher_Code/SF_driver.c"
void SerialFlash_SectorErase(unsigned long address){

 SerialFlash_WriteEnable();

 CS_Serial_Flash_bit = 0;
 SPI_Wr_Ptr(_SERIAL_FLASH_CMD_SER);
 SPI_Wr_Ptr( ((char *)&address)[2] );
 SPI_Wr_Ptr( ((char *)&address)[1] );
 SPI_Wr_Ptr( ((char *)&address)[0] );
 CS_Serial_Flash_bit = 1;


 while(SerialFlash_IsWriteBusy());
}
