#include "BPWasher_objects.h"
#include "BPWasher_resources.h"
#include "built_in.h"
#include "Constants.h"
#include "SF_driver.h"

#define HEATINGTIMEOUT 1800     // 900 seconds = 15 min
#define SAFE_COOL_TEMP 35       // reduced safe cool temp to 35 degrees to prolong cooling cycle and top SSR from overheating.

char msgWaitingStart[] = "Waiting Start";
char msgFilling[] = "Filling";
char msgEnterCalTemp[] = "Enter measured water temp";
char msgDraining[] = "Draining";
#define HEATING_TO  "Heating to %d"
char msgHeating[] = HEATING_TO;
char msgCalHeating[] = HEATING_TO;
char msgSanitizing[] ="Sanitizing 12";
char msgCooling[] ="Cooling";
char msgCycleCom[] ="Cycle Complete";
char msgTestHeating[] = "Test Heating to 123456789";
char msgWaitingTestStart[] = "Test Waiting Start";
char msgCycles[] = "Cycles = 123456789";
char msgCycleFailHeat[] = "Failed to Reach Temp";
char msgCycleFailFill[] = "Failed to Fill";
char msgCycleFailDrain[] = "Failed to Drain";
char msgElementFault[] = "Element Fault - Call M&E";
char nSanCycle = 1;

//Calibration properties
stParams fMeasuredHigh = {
         0,
         0,
         100,
         1,
         &Calibration,
         NULL,
         1
        };
stParams fMeasuredLow = {
         0,
         0,
         100,
         1,
         &Calibration,
         NULL,
         1
        };
        
float fLastTempDisplayed;
float fCurrentTemp;
#define CAL_TEMP_LOW    37
#define CAL_TEMP_HIGH   60
#define PREWASH_TEMP    60
#define SANITIZING_TEMP 85    // 2 minutes at 85 degrees is defined as sanitized.

unsigned int HalfSeconds = 0;
char nState = WAITING_START;
char bErrorAfterCooling = FALSE;

void StatusHalfSecondsCount()
    {
    HalfSeconds++;
    }
#define IMPELLOR_ONOFF 1
#define WATER_IN_ONOFF 2
#define DRAIN_ONOFF 3


char nDeviceState =  IMPELLOR_ONOFF;
char SwitchOnThree(char bOnOff)
    {
     char bAllOnOff = FALSE;

     switch (nDeviceState)
        {
        case IMPELLOR_ONOFF:
            SetOutput(IMPELLOR, bOnOff);
            delay_ms(300);
            nDeviceState = DRAIN_ONOFF;
        break;

        case DRAIN_ONOFF:
             SetOutput(DRAIN_CLOSED, bOnOff);
             delay_ms(300);
            nDeviceState = WATER_IN_ONOFF;

        break;

        case WATER_IN_ONOFF:
             SetOutput(WATER_IN, bOnOff);
             delay_ms(300);
            nDeviceState = IMPELLOR_ONOFF;
            bAllOnOff = TRUE;
        break;

        }

    return  bAllOnOff;
    }

void SetTextLabel(TLabel label, char *msg){
     // Clears label by drawing with previous caption in black then drawing new in old colour
    // This needs the previous caption still at label.Caption
    // In some instances the sCaption buffer is the same address as previously
    // but with different text, so the only way to remember the old caption
    // is to keep our own copy, hence using strcpy(label.Caption, msg) instead of label.Caption = msg
     unsigned int nOldColour = label.Font_Color;
     label.Font_Color = CL_BLACK;
     DrawLabel(&label);
     strcpy(label.Caption, msg);
     label.Font_Color = nOldColour;
     DrawLabel(&label);
}

void SetStatus(char *sCaption, char bError)
    {
    Label2.Font_Color = bError? CL_RED : 0x07FF;
    SetTextLabel(Label2, sCaption);
    
    // set colour of "STATUS:" label
    Label1.Font_Color = Label2.Font_Color;
    DrawLabel(&Label1);
    }

    
void SetFloatLabel(TLabel label, char *format, float fNew) {
     unsigned int nOldColour = label.Font_Color;
     label.Font_Color = CL_BLACK;
     DrawLabel(&label);
     sprintf(label.Caption, format, fNew);
     label.Font_Color = nOldColour;
     DrawLabel(&label);
}

char sStateNum[4];
char *StateName()
    {
    switch (nState)
        {
        case PREWASH_FILL :      return "PREWASH_FILL";
        case PREWASH_HEAT :      return "PREWASH_HEAT";
        case PREWASH_DRAIN :     return "PREWASH_DRAIN";
        case PRECOOL_FILL :      return "PRECOOL_FILL";
        case SANTIZ_WAIT  :      return "SANTIZ_WAIT";
        case SANTIZ_HEAT :       return "SANTIZ_HEAT";
        case SANTIZ_FILL :       return "SANTIZ_FILL";
        case POSTCOOL_FILL :     return "POSTCOOL_FILL";
        case SANTIZ_DRAIN :      return "SANTIZ_DRAIN";
        default:
            sprintf(sStateNum, "%d", nState);
            return sStateNum;  // only the above states are likely to result in an error, otherwise just report state number.
        }
    }
/**************************
ErrorLog - Record the error in Flash memory - CM 10/07/17
errMsg will be one of
       "Failed to Reach Temp"
       "Failed to Fill"
       "Temp Fail  %2.1f"
       "Version x.xx installed"
errState will be one of
         "PREWASH_HEAT"
         "SANTIZ_HEAT"
         "SANTIZ_WAIT"
         "PREWASH_FILL"
         "PRECOOL_FILL"
         "SANTIZ_FILL"
         "POSTCOOL_FILL"
50 bytes long, including trailing zero gives:
9999 / 9999, Failed to Reach Temp, START_HEATING
times 10 lines = 500 bytes
**************************/
void ErrorLog_NewError(char *errMsg, char bIncrementCount)
    {

    unsigned int iErrorLogOffset;
    char sErrorLogTemp[100];
    
    if (bIncrementCount)
       {
       // update fault count
       fCyclesFail += 1;
       sprintf(CyclesFail, "%6.0f", fCyclesFail);
       SetStatus(errMsg, STATUS_ERROR);
       }
    
    // shift all error logs along the buffer
    // most recent error is at start of log, then every ERRLOG_LINE_LEN bytes
    // NOTE, can't use memcpy because we're copying ahead into the same buffer
    iErrorLogOffset = ERRLOG_TOTAL_LEN - ERRLOG_LINE_LEN;
    while ( iErrorLogOffset )
          {
          sErrorLog[iErrorLogOffset+ERRLOG_LINE_LEN-1] = sErrorLog[iErrorLogOffset-1];
          iErrorLogOffset--;
          }
          
    // copy new error log to start of buffer
    // NOTE, snprintf isn't available and we don't know possible error log line length,
    // so copy into a longer buffer first and then copy maximum of ERRLOG_LINE_LEN into start of buffer
    memset(sErrorLogTemp, 0, 100);
    sprintf(sErrorLogTemp, "%.0f / %.0f, %s", fCyclesComplete, fCyclesFail, errMsg);
    if (bIncrementCount)
       {
       strcat(sErrorLogTemp, ", ");
       strcat(sErrorLogTemp, StateName());
       }
    memcpy(sErrorLog, sErrorLogTemp, ERRLOG_LINE_LEN);
    // put a trailing zero at very end just incase temp string was longer than maximum.
    sErrorLog[ERRLOG_LINE_LEN-1] = 0;
    
    // save new error log and error count
    UpDateMemory();
    }

void CheckFillingToLong()
    {
    if (130 < HalfSeconds)
       {
       ErrorLog_NewError(msgCycleFailFill, STATUS_ERROR);
       nState = ERR_PRE_HOLDING_STATE;
       }
    }
void FillWasher(void)
    {
    if (CurrentScreen==&_main) SetStatus(msgFilling, STATUS_NORMAL);
    SetOutput(DRAIN_CLOSED, TRUE);
    delay_ms(SWITCH_DELAY);
    SetOutput(WATER_IN, TRUE);
    }
void ResetRecipe()
    {
     nState = WAITING_START;
     SetStatus(msgWaitingStart, STATUS_NORMAL);
    }

void Recipe_State()
    {
        /*
    if (InputOn(ELEMENT_FAULT) && nState!=ERR_PRE_HOLDING_STATE && nState!=ERR_HOLDING_STATE )
       {
       // element has overheated
       // no need to turn outputs off as power will already have been cut to everything apart from the controller
       
       ErrorLog_NewError(msgElementFault, STATUS_ERROR);
       nState=ERR_HOLDING_STATE;
       }
        */
    if(nState != RTC_STORAGE_NSTATE){
      //write state to RTC backup registers when we change state
      DBP_bit = 1;    // enable write access to RTC Backup registers
      RTC_STORAGE_NSTATE = nState;
      DBP_bit = 0;    //disable write access after written (prevent heat spike etc. corruption)
    }
    
    switch(nState)
        {
         case WAITING_START:
              if (InputOn(START_BTN) && InputOn(LID_SHUT))
                  {
                  fSetTemp = 0;
                  nState = PREWASH_FILL;
                  SetOutput(CYCLE_RUN_LIGHT, TRUE);
                  SetOutput(LID_UNLOCK, FALSE);    // locked
                  FillWasher();
                  HalfSeconds = 0;
                  }
              else if (InputOn(LID_OPEN_BTN) && InputOn(LID_SHUT) && (GetCurrentTemp() < 43))
                   SetOutput(LID_UNLOCK, TRUE);    // unlocked
              else if (!InputOn(LID_OPEN_BTN) && !InputOn(LID_SHUT))
                    SetOutput(LID_UNLOCK, FALSE);  // locked       // is this necessary?
              if(20 == HalfSeconds)
                  {
                  HalfSeconds++;
                  sprintf(msgCycles, "Cycles =  %6.0f", fCyclesComplete);
                  SetStatus(msgCycles, STATUS_NORMAL);
                  }
              else if (24 == HalfSeconds)
                  {
                  HalfSeconds++;
                  sprintf(msgCycles, "Failed =  %6.0f", fCyclesFail);
                  SetStatus(msgCycles, STATUS_NORMAL);
                  }
              else if (28 < HalfSeconds)
                  {
                  HalfSeconds = 0;
                  SetStatus(msgWaitingStart, STATUS_NORMAL);
                  }
            break;

         case PREWASH_FILL:
             if (InputOn(WATER_LEVEL))
               {
                SetOutput(WATER_IN, FALSE);
                delay_ms(SWITCH_DELAY);
                SetOutput(IMPELLOR, TRUE);
                //set temp to 60
                fSetTemp = PREWASH_TEMP + 0.9;
                sprintf(msgHeating, HEATING_TO, PREWASH_TEMP);
                SetStatus(msgHeating, STATUS_NORMAL);
                HalfSeconds = 0;
                nState = PREWASH_HEAT;
               }
             // Failed to fill
             else
               CheckFillingToLong();  // 130 HalfSeconds timeout
             break;

         case PREWASH_HEAT:
            //If at temp 60 empty
            if (bAtTemp)
                {
              //delay_ms(2000);
              nState = PREWASH_DRAIN;
              HalfSeconds = 0;
              SetStatus(msgDraining, STATUS_NORMAL);
              SetOutput(DRAIN_CLOSED, FALSE);
              delay_ms(SWITCH_DELAY);
              SetOutput(IMPELLOR, FALSE);
              fSetTemp = 0;
               }
            else
                {
                 if (HEATINGTIMEOUT < HalfSeconds)   // 1800 HalfSeconds = 15 min
                     {
                     ErrorLog_NewError(msgCycleFailHeat, STATUS_ERROR);
                     nState = ERR_PRE_HOLDING_STATE;
                     }
                }
                //set temp to 0
                // Failed to heat
              break;
              
         case PREWASH_DRAIN:
             //Wait for one minute to empty
              //delay_ms(2000);
              if ((fDrainTime.fValue*2) < HalfSeconds)
                 {
                 if (InputOn(WATER_LEVEL))
                    {
                    ErrorLog_NewError(msgCycleFailDrain, STATUS_ERROR);
                    nState = ERR_PRE_HOLDING_STATE;
                    }
                 else
                    {
                    SetStatus(msgFilling, STATUS_NORMAL);
                    nState = PRECOOL_INIT;
                    }
                 }
              break;

         case PRECOOL_INIT:

              if (SwitchOnThree(TRUE))
                {
                nState = PRECOOL_FILL;
                HalfSeconds = 0;
                }
               break;

         case PRECOOL_FILL:
              if (InputOn(WATER_LEVEL))
                  {
                  SetOutput(WATER_IN, FALSE);
                  SetStatus(msgCooling, STATUS_NORMAL);
                  HalfSeconds = 0;
                  nState = PRECOOL_WAIT;
                  }
              else
                  CheckFillingToLong();  // 130 HalfSeconds timeout

              break;

         case PRECOOL_WAIT:
              if (120 < HalfSeconds)     // was 340 HalfSeconds, but cooling is not effective after about 1 min.
                  {
                  if (SwitchOnThree(FALSE))  // turn off impeller, drain, turn off water (already off)
                      {
                      HalfSeconds = 0;
                      SetStatus(msgDraining, STATUS_NORMAL);
                      nState = PRECOOL_DRAIN;
                      }
                  }

              break;

         case PRECOOL_DRAIN:
               if ((fDrainTime.fValue*2) < HalfSeconds)
                  {
                  if (InputOn(WATER_LEVEL))
                     {
                     ErrorLog_NewError(msgCycleFailDrain, STATUS_ERROR);
                     nState = ERR_PRE_HOLDING_STATE;
                     }
                  else
                     nState = SANTIZ_INIT;
                  }

               break;

         case SANTIZ_INIT:
               nState = SANTIZ_FILL;
               nSanCycle = 1;  // moved here, better to reset before first cycle rather than after successful cycle - CM 4/7/17
               FillWasher();  // stop drain, turn water on, set "Filling" message on screen
               HalfSeconds = 0;
               break;

          case SANTIZ_FILL:
               if (InputOn(WATER_LEVEL))
                  {
                  nState = SANTIZ_FILL_EXTRA;
                  HalfSeconds = 0;
                  }
               else
                  CheckFillingToLong(); // 130 HalfSeconds timeout
             break;

          case SANTIZ_FILL_EXTRA:
               //  Fill for an extra 5 seconds to ensure there's enough water over the element - CM 5/07/17
               if (InputOn(WATER_LEVEL) && ((fExtraFillTime.fValue*2) < HalfSeconds))
                  {
                  SetOutput(WATER_IN, FALSE);
                  delay_ms(SWITCH_DELAY);
                  SetOutput(IMPELLOR, TRUE);
                  //set temp to 80
                  fSetTemp = SANITIZING_TEMP + 0.9;
                  nState = SANTIZ_HEAT;
                  sprintf(msgHeating, HEATING_TO, SANITIZING_TEMP);
                  SetStatus(msgHeating, STATUS_NORMAL);
                  HalfSeconds = 0;
                  }
             break;

          case SANTIZ_HEAT:
              //delay_ms(2000);
              if (bAtTemp)
                  {
                  nState = SANTIZ_WAIT;
                  HalfSeconds = 0;
                  sprintf(msgSanitizing, "Sanitizing  %1d", nSanCycle);
                  SetStatus(msgSanitizing, STATUS_NORMAL);
                  }
              else if (HEATINGTIMEOUT < HalfSeconds)    // 1800 HalfSeconds = 15 min
                  {
                  ErrorLog_NewError(msgCycleFailHeat, STATUS_ERROR);
                  nState = ERR_PRE_HOLDING_STATE;
                  }
              break;

          case SANTIZ_WAIT:
              // wait 2 mins
             if (259 < HalfSeconds)     //239
                {
                // sanitizing cycle was successful - CM
               // delay_ms(10000);
                nState = SANTIZ_DRAIN;
                HalfSeconds = 0;
                SetStatus(msgDraining, STATUS_NORMAL);
                fSetTemp = 0;
                SetOutput(DRAIN_CLOSED, FALSE);
                SetOutput(IMPELLOR, FALSE);
                }
            else
                {
                // was GetCurrentTemp() but that is instant whereas bAtTemp is an average over last 15 readings, so if temp drops below 85
                // it will move on to the next attempt, but the next attempt will be started straight away because bAtTemp will still be TRUE.
                // Changed to GetDisplayTemp which is also an average of the last 15 readings and which bAtTemp is based on.
                // Can't use bAtTemp directly because temperature setpoint is higher than 85 so would cause too many unecessary failures.
                // CM 5/07/17
                if (GetDisplayTemp() < 85)
                    {
                    // this sanitizing cycle was unsuccessful, temp dropped below 85 - CM
                    nSanCycle++;
                    sprintf(msgTestHeating, "Temp Fail %2.1f", GetDisplayTemp()); // was GetCurrentTemp() - CM
                    SetStatus(msgTestHeating, STATUS_NORMAL);
                    HalfSeconds = 0;
                    if (4 <  nSanCycle)  // failed to heat  after 4
                       {
                       ErrorLog_NewError(msgTestHeating, STATUS_ERROR);
                       nState = ERR_PRE_HOLDING_STATE;
                       }
                    else
                       nState = SANTIZ_HEAT;
                    }
                }
              break;


          case SANTIZ_DRAIN:  // 13
              if (((fDrainTime.fValue*2) + 2*(fExtraFillTime.fValue*2)) < HalfSeconds) // add twice extra fill delay to drain additional water, CM 5/7/17
                 {
                 if (InputOn(WATER_LEVEL))
                    {
                    ErrorLog_NewError(msgCycleFailDrain, STATUS_ERROR);
                    nState = ERR_PRE_HOLDING_STATE;
                    }
                 else
                    nState = POSTCOOL_INIT;
                 }
              break;

          case POSTCOOL_INIT:  // 14
              if (SwitchOnThree(TRUE))    // water in, impeller on, drain closed
                  {
                  nState = POSTCOOL_FILL;
                  HalfSeconds = 0;
                  if (!bErrorAfterCooling)
                     SetStatus(msgFilling, STATUS_NORMAL);
                  }
              break;

         case POSTCOOL_FILL:  // 15
             if (InputOn(WATER_LEVEL) || (GetCurrentTemp() <= SAFE_COOL_TEMP))
                {
                nState = POSTCOOL_FILL_EXT;
                HalfSeconds = 0;
                }
             else
                CheckFillingToLong();

             break;
            
         case POSTCOOL_FILL_EXT:  //16
             if ( (fExtraFillTime.fValue*2) < HalfSeconds || (GetCurrentTemp() <= SAFE_COOL_TEMP)) // add extra water for faster cooling - CM 12/7/17
                {
                SetOutput(WATER_IN, FALSE);
                if (!bErrorAfterCooling)
                   SetStatus(msgCooling, STATUS_NORMAL);
                nState = POSTCOOL_WAIT;
                }
             break;

         case POSTCOOL_WAIT:  // 17
             // was 340 HalfSeconds. use only 1 min reduce cooling time - CM 12/7/17
             // put back to 340 Half Seconds and reduced cooling temp to 35 degrees to prolong cooling cycle to help keep SSR cool.
             if ((340 < HalfSeconds) || (GetCurrentTemp() <= SAFE_COOL_TEMP))
                {
                if (SwitchOnThree(FALSE)) // water off, impeller off, drain open
                   {
                   if (GetCurrentTemp() < SAFE_COOL_TEMP)
                      {
                      if (bErrorAfterCooling)
                         {
                         nState = ERR_HOLDING_STATE;
                         }
                      else
                         {
                         nState = WAIT_UNLOCK;
                         delay_ms(SWITCH_DELAY);
                         SetOutput(CYCLE_COMPLETE_LIGHT, TRUE);
                         fCyclesComplete += 1;
                         sprintf(CyclesComplete, "%6.0f", fCyclesComplete);
                         UpDateMemory();
                         SetStatus(msgCycleCom, STATUS_NORMAL);
                         HalfSeconds = 0;
                         }
                      }
                    else
                      {
                      nState = SANTIZ_DRAIN;
                      if (!bErrorAfterCooling)
                         SetStatus(msgDraining, STATUS_NORMAL);
                      HalfSeconds = 0;
                      }
                   }
                }

            break;

         case WAIT_UNLOCK: // 14 "Cycle Complete"
              if (InputOn(LID_OPEN_BTN))
                {
                SetOutput(CYCLE_RUN_LIGHT, FALSE);
                SetOutput(CYCLE_COMPLETE_LIGHT, FALSE);
                SetOutput(LID_UNLOCK, TRUE);
                nState = WAITING_START;
                SetStatus(msgWaitingStart, STATUS_NORMAL);
                HalfSeconds = 0;
                }
              if (AUTO_RESTART)
                {
                if (599 < HalfSeconds)
                    {
                    SetOutput(CYCLE_COMPLETE_LIGHT, FALSE);
                    fSetTemp = 0;
                    nState = PREWASH_FILL;
                    SetOutput(CYCLE_RUN_LIGHT, TRUE);
                    SetOutput(LID_UNLOCK, FALSE);
                    FillWasher();
                    HalfSeconds = 0;
                    }
                 }
             break;

 //Error

        case ERR_PRE_HOLDING_STATE:
                fSetTemp = 0;
                if (SwitchOnThree(FALSE))           // turn off water & impeller, open drain
                   {
                   if (bErrorAfterCooling)
                      {
                      // if there was another error while bErrorAfterCooling is already true, jump straight to ERR_HOLDING_STATE, CM 06/08/2017
                      nState = ERR_HOLDING_STATE;
                      }
                   else
                      {
                      // set flag so we can't come back here again, don't display 'filling/cooling' messages,
                      // and after we've finished cooling jump strait to ERR_HOLDING_STATE, CM 05/08/2017
                      bErrorAfterCooling = TRUE;
                      // keep draining/filling/cooling until safe to open, to give time for SSR to cool down
                      // and make next cycle safe to start without thermal shock, CM 05/08/2017
                      HalfSeconds = 0;
                      nState = SANTIZ_DRAIN;
                      }
                   }

            break;

        case ERR_HOLDING_STATE:
             // don't allow restart if bGlobalTempAlarm is true, CM 05/08/2017
            if (InputOn(START_BTN) && InputOn(LID_SHUT) && !InputOn(ELEMENT_FAULT) && !InputOn(HEATER_MAIN_ON) && !bGlobalTempAlarm)
                {
                fSetTemp = 0;
                bErrorAfterCooling = FALSE;
                nState = PREWASH_FILL;
                SetOutput(CYCLE_RUN_LIGHT, TRUE);
                SetOutput(LID_UNLOCK, FALSE);
                FillWasher();
                HalfSeconds = 0;
                }

            break;
  //Calibration:

          case CAL_START:
               // If tub is empty start filling
               if(!InputOn(WATER_LEVEL)){
                  fillWasher();
                  SetTextLabel(cal_message_label, msgFilling);
               }
               HalfSeconds = 0;
               nState = CAL_FILL;
             break;
  
          case CAL_FILL:
               if (InputOn(WATER_LEVEL))
                  {
                  //TODO: add a delay before turning water off? (fill over element?)
                  SetOutput(WATER_IN, FALSE);
                  delay_ms(SWITCH_DELAY);
                  SetOutput(IMPELLOR, TRUE);
                   
                  fSetTemp = CAL_TEMP_LOW;
                  sprintf(msgCalHeating, HEATING_TO, CAL_TEMP_LOW);
                  SetTextLabel(cal_message_label, msgCalHeating);
                   
                  //show temperature label
                  cal_temp_label.Visible = TRUE;
                  fLastTempDisplayed = 0;

                  HalfSeconds = 0; 
                  nState = CAL_HOLD_TEMP_LOW;
                  }
               else
               // Failed to fill
                  CheckFillingToLong(); // 130 HalfSeconds timeout
             break;

          case CAL_HOLD_TEMP_LOW:

                // Update temperature display Label
                fCurrentTemp = GetDisplayTemp();
                if (fLastTempDisplayed != fCurrentTemp){
                   SetFloatLabel(cal_temp_label, "%3.1f", fCurrentTemp);
                   fLastTempDisplayed = fCurrentTemp;
                }
                
                if (bAtTemp && HalfSeconds > 4) {
                   // Take user input of measured temp
                   fMeasuredLow.fValue = 0.0;
                   Display_Keyboard(msgEnterCalTemp, &fMeasuredLow);
                   nState = CAL_ENTER_LOW_TEMP;
                } else if (HEATINGTIMEOUT < HalfSeconds) {   // 1800 HalfSeconds = 15 min
                   ErrorLog_NewError(msgCycleFailHeat, STATUS_ERROR);
                   nState = ERR_PRE_HOLDING_STATE;
                }

                break;

         case CAL_ENTER_LOW_TEMP:
              if (CurrentScreen!=&Keypad) {
                //Waits for Display_Keyboard to execute
                if (fMeasuredLow.fValue != 0.0){
                    fSetTemp = CAL_TEMP_HIGH;
                    sprintf(msgCalHeating, HEATING_TO, CAL_TEMP_HIGH);
                    SetTextLabel(cal_message_label, msgCalHeating);
                    HalfSeconds = 0;
                    nState = CAL_HOLD_TEMP_HIGH;
                 } else {
                    HalfSeconds = 0;
                    nState = CAL_HOLD_TEMP_LOW;
                 }
              }
              break;
              
         case CAL_HOLD_TEMP_HIGH:
                // Update temperature display Label
                fCurrentTemp = GetDisplayTemp();
                if (fLastTempDisplayed != fCurrentTemp){
                   SetFloatLabel(cal_temp_label, "%3.1f", fCurrentTemp);
                   fLastTempDisplayed = fCurrentTemp;
                }
                
                if (bAtTemp && HalfSeconds > 4) {
                   // Take user input of measured temp
                   fMeasuredHigh.fValue = 0.0;
                   Display_Keyboard(msgEnterCalTemp, &fMeasuredHigh);
                   nState = CAL_ENTER_HIGH_TEMP;
                } else if (HEATINGTIMEOUT < HalfSeconds) {   // 1800 HalfSeconds = 15 min
                   ErrorLog_NewError(msgCycleFailHeat, STATUS_ERROR);
                   nState = ERR_PRE_HOLDING_STATE;
                }

                break;
                
         case CAL_ENTER_HIGH_TEMP:
              if (CurrentScreen!=&Keypad) {
                //Waits for Display_Keyboard to execute
                if (fMeasuredHigh.fValue != 0.0) {
                    // Calculate Factor and Offset for correcting error accumulated since previous calibration
                    float fCalFactorNew = (fMeasuredHigh.fvalue - fMeasuredLow.fvalue)/(CAL_TEMP_HIGH-CAL_TEMP_LOW);
                    float fCalOffsetNew =  fMeasuredHigh.fvalue - CAL_TEMP_HIGH*fCalFactorNew;

                    // Combine current Calibration factors with previous Calibration and update
                    fCalOffsetNew = (fCalFactorNew*fCal_Offset.fValue) + fCalOffsetNew;
                    fCalFactorNew = fCal_factor.fValue * fCalFactorNew;
                    fCal_Offset.fValue = fCalOffsetNew;
                    fCal_factor.fValue = fCalFactorNew;

                    // Update and display the Calibration constants
                    set_and_format_value(&fCal_offset, fCal_Offset.fValue );
                    set_and_format_value(&fCal_factor, fCal_factor.fValue );
                    UpdateMemory();

                    CalibrationScreenVisible(TRUE);
                    cal_temp_label.Visible = FALSE;

                    cal_start_button.Caption = cal_start_button_caption;
                    DrawScreen(&Calibration);

                    nState = CAL_STOP;
                 } else {
                    HalfSeconds = 0;
                    nState = CAL_HOLD_TEMP_HIGH;
                 }
              }
              break;

         case CAL_STOP:
                fSetTemp = 0;
                if (SwitchOnThree(FALSE))           // turn off water & impeller, open drain
                    nState = WAITING_START;
                    
                break;


        }
    }