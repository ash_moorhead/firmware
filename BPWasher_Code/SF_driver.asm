_SerialFlash_init:
;SF_driver.c,32 :: 		void SerialFlash_init(){
SUB	SP, SP, #4
;SF_driver.c,33 :: 		CS_Serial_Flash_bit = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R0, #hi_addr(CS_Serial_Flash_bit+0)
STR	R1, [R0, #0]
;SF_driver.c,34 :: 		}
L_end_SerialFlash_init:
ADD	SP, SP, #4
BX	LR
; end of _SerialFlash_init
_SerialFlash_WriteEnable:
;SF_driver.c,43 :: 		void SerialFlash_WriteEnable(){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;SF_driver.c,44 :: 		CS_Serial_Flash_bit = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R0, #hi_addr(CS_Serial_Flash_bit+0)
STR	R1, [R0, #0]
;SF_driver.c,45 :: 		SPI_Wr_Ptr(_SERIAL_FLASH_CMD_WREN);
MOVS	R0, #6
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,46 :: 		CS_Serial_Flash_bit = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R0, #hi_addr(CS_Serial_Flash_bit+0)
STR	R1, [R0, #0]
;SF_driver.c,47 :: 		}
L_end_SerialFlash_WriteEnable:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _SerialFlash_WriteEnable
_SerialFlash_IsWriteBusy:
;SF_driver.c,56 :: 		unsigned char SerialFlash_IsWriteBusy(){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;SF_driver.c,59 :: 		CS_Serial_Flash_bit = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R0, #hi_addr(CS_Serial_Flash_bit+0)
STR	R1, [R0, #0]
;SF_driver.c,60 :: 		SPI_Wr_Ptr(_SERIAL_FLASH_CMD_RDSR);
MOVS	R0, #5
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,61 :: 		temp = SPI_Rd_Ptr(0);
MOVS	R0, #0
MOVW	R4, #lo_addr(_SPI_Rd_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Rd_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,62 :: 		CS_Serial_Flash_bit = 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R1, #hi_addr(CS_Serial_Flash_bit+0)
STR	R2, [R1, #0]
;SF_driver.c,64 :: 		return (temp&0x01);
UXTB	R0, R0
AND	R0, R0, #1
;SF_driver.c,65 :: 		}
L_end_SerialFlash_IsWriteBusy:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _SerialFlash_IsWriteBusy
_SerialFlash_WriteByte:
;SF_driver.c,74 :: 		void SerialFlash_WriteByte(unsigned char _data, unsigned long address){
SUB	SP, SP, #12
STR	LR, [SP, #0]
STRB	R0, [SP, #4]
STR	R1, [SP, #8]
;SF_driver.c,75 :: 		SerialFlash_WriteEnable();
BL	_SerialFlash_WriteEnable+0
;SF_driver.c,77 :: 		CS_Serial_Flash_bit = 0;
MOVS	R3, #0
SXTB	R3, R3
MOVW	R2, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R2, #hi_addr(CS_Serial_Flash_bit+0)
STR	R3, [R2, #0]
;SF_driver.c,78 :: 		SPI_Wr_Ptr(_SERIAL_FLASH_CMD_WRITE);
MOVS	R0, #2
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,79 :: 		SPI_Wr_Ptr(Higher(address));
ADD	R2, SP, #8
ADDS	R2, R2, #2
LDRB	R2, [R2, #0]
UXTB	R4, R2
UXTH	R0, R4
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,80 :: 		SPI_Wr_Ptr(Hi(address));
ADD	R2, SP, #8
ADDS	R2, R2, #1
LDRB	R2, [R2, #0]
UXTB	R4, R2
UXTH	R0, R4
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,81 :: 		SPI_Wr_Ptr(Lo(address));
ADD	R2, SP, #8
LDRB	R2, [R2, #0]
UXTB	R4, R2
UXTH	R0, R4
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,82 :: 		SPI_Wr_Ptr(_data);
LDRB	R0, [SP, #4]
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,83 :: 		CS_Serial_Flash_bit = 1;
MOVS	R3, #1
SXTB	R3, R3
MOVW	R2, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R2, #hi_addr(CS_Serial_Flash_bit+0)
STR	R3, [R2, #0]
;SF_driver.c,86 :: 		while(SerialFlash_isWriteBusy());
L_SerialFlash_WriteByte0:
BL	_SerialFlash_IsWriteBusy+0
CMP	R0, #0
IT	EQ
BEQ	L_SerialFlash_WriteByte1
IT	AL
BAL	L_SerialFlash_WriteByte0
L_SerialFlash_WriteByte1:
;SF_driver.c,87 :: 		}
L_end_SerialFlash_WriteByte:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _SerialFlash_WriteByte
_SerialFlash_WriteWord:
;SF_driver.c,96 :: 		void SerialFlash_WriteWord(unsigned int _data, unsigned long address){
; address start address is: 4 (R1)
SUB	SP, SP, #12
STR	LR, [SP, #0]
STRH	R0, [SP, #8]
MOV	R0, R1
; address end address is: 4 (R1)
; address start address is: 0 (R0)
;SF_driver.c,97 :: 		SerialFlash_WriteByte(Hi(_data),address);
ADD	R2, SP, #8
ADDS	R2, R2, #1
LDRB	R2, [R2, #0]
STR	R0, [SP, #4]
MOV	R1, R0
UXTB	R0, R2
BL	_SerialFlash_WriteByte+0
LDR	R0, [SP, #4]
;SF_driver.c,98 :: 		SerialFlash_WriteByte(Lo(_data),address+1);
ADDS	R3, R0, #1
; address end address is: 0 (R0)
ADD	R2, SP, #8
LDRB	R2, [R2, #0]
MOV	R1, R3
UXTB	R0, R2
BL	_SerialFlash_WriteByte+0
;SF_driver.c,99 :: 		}
L_end_SerialFlash_WriteWord:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _SerialFlash_WriteWord
_SerialFlash_ReadID:
;SF_driver.c,108 :: 		unsigned char SerialFlash_ReadID(void){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;SF_driver.c,111 :: 		CS_Serial_Flash_bit = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R0, #hi_addr(CS_Serial_Flash_bit+0)
STR	R1, [R0, #0]
;SF_driver.c,112 :: 		SPI_Wr_Ptr(_SERIAL_FLASH_CMD_RDID);
MOVS	R0, #159
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,113 :: 		temp = SPI_Rd_Ptr(0);
MOVS	R0, #0
MOVW	R4, #lo_addr(_SPI_Rd_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Rd_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,114 :: 		CS_Serial_Flash_bit = 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R1, #hi_addr(CS_Serial_Flash_bit+0)
STR	R2, [R1, #0]
;SF_driver.c,116 :: 		return temp;
UXTB	R0, R0
;SF_driver.c,117 :: 		}
L_end_SerialFlash_ReadID:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _SerialFlash_ReadID
_SerialFlash_ReadByte:
;SF_driver.c,126 :: 		unsigned char SerialFlash_ReadByte(unsigned long address){
SUB	SP, SP, #8
STR	LR, [SP, #0]
STR	R0, [SP, #4]
;SF_driver.c,129 :: 		CS_Serial_Flash_bit = 0;
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R1, #hi_addr(CS_Serial_Flash_bit+0)
STR	R2, [R1, #0]
;SF_driver.c,131 :: 		SPI_Wr_Ptr(_SERIAL_FLASH_CMD_READ);
MOVS	R0, #3
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,132 :: 		SPI_Wr_Ptr(Higher(address));
ADD	R1, SP, #4
ADDS	R1, R1, #2
LDRB	R1, [R1, #0]
UXTB	R4, R1
UXTH	R0, R4
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,133 :: 		SPI_Wr_Ptr(Hi(address));
ADD	R1, SP, #4
ADDS	R1, R1, #1
LDRB	R1, [R1, #0]
UXTB	R4, R1
UXTH	R0, R4
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,134 :: 		SPI_Wr_Ptr(Lo(address));
ADD	R1, SP, #4
LDRB	R1, [R1, #0]
UXTB	R4, R1
UXTH	R0, R4
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,135 :: 		temp = SPI_Rd_Ptr(0);
MOVS	R0, #0
MOVW	R4, #lo_addr(_SPI_Rd_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Rd_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,137 :: 		CS_Serial_Flash_bit = 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R1, #hi_addr(CS_Serial_Flash_bit+0)
STR	R2, [R1, #0]
;SF_driver.c,138 :: 		return temp;
UXTB	R0, R0
;SF_driver.c,139 :: 		}
L_end_SerialFlash_ReadByte:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _SerialFlash_ReadByte
_SerialFlash_ReadWord:
;SF_driver.c,148 :: 		unsigned int SerialFlash_ReadWord(unsigned long address){
; address start address is: 0 (R0)
SUB	SP, SP, #20
STR	LR, [SP, #0]
MOV	R2, R0
; address end address is: 0 (R0)
; address start address is: 8 (R2)
;SF_driver.c,151 :: 		Hi(temp) = SerialFlash_ReadByte(address);
ADD	R1, SP, #8
STR	R1, [SP, #16]
ADDS	R1, R1, #1
STR	R1, [SP, #12]
STR	R2, [SP, #4]
MOV	R0, R2
BL	_SerialFlash_ReadByte+0
LDR	R2, [SP, #4]
LDR	R1, [SP, #12]
STRB	R0, [R1, #0]
;SF_driver.c,152 :: 		Lo(temp) = SerialFlash_ReadByte(address+1);
LDR	R1, [SP, #16]
STR	R1, [SP, #12]
ADDS	R1, R2, #1
; address end address is: 8 (R2)
MOV	R0, R1
BL	_SerialFlash_ReadByte+0
LDR	R1, [SP, #12]
STRB	R0, [R1, #0]
;SF_driver.c,154 :: 		return temp;
LDRH	R0, [SP, #8]
;SF_driver.c,155 :: 		}
L_end_SerialFlash_ReadWord:
LDR	LR, [SP, #0]
ADD	SP, SP, #20
BX	LR
; end of _SerialFlash_ReadWord
_SerialFlash_WriteArray:
;SF_driver.c,165 :: 		unsigned char SerialFlash_WriteArray(unsigned long address, unsigned char* pData, unsigned int nCount){
SUB	SP, SP, #40
STR	LR, [SP, #0]
STR	R0, [SP, #16]
STR	R1, [SP, #20]
STRH	R2, [SP, #24]
;SF_driver.c,170 :: 		addr = address;
LDR	R3, [SP, #16]
STR	R3, [SP, #4]
;SF_driver.c,171 :: 		pD   = pData;
LDR	R3, [SP, #20]
STR	R3, [SP, #8]
;SF_driver.c,175 :: 		for(counter = 0; counter < nCount; counter++){
MOVS	R3, #0
STRH	R3, [SP, #12]
L_SerialFlash_WriteArray2:
LDRH	R4, [SP, #24]
LDRH	R3, [SP, #12]
CMP	R3, R4
IT	CS
BCS	L_SerialFlash_WriteArray3
;SF_driver.c,176 :: 		SerialFlash_WriteByte(*pD++, addr++);
LDR	R3, [SP, #8]
LDRB	R3, [R3, #0]
LDR	R1, [SP, #4]
UXTB	R0, R3
BL	_SerialFlash_WriteByte+0
LDR	R3, [SP, #8]
ADDS	R3, R3, #1
STR	R3, [SP, #8]
LDR	R3, [SP, #4]
ADDS	R3, R3, #1
STR	R3, [SP, #4]
;SF_driver.c,175 :: 		for(counter = 0; counter < nCount; counter++){
LDRH	R3, [SP, #12]
ADDS	R3, R3, #1
STRH	R3, [SP, #12]
;SF_driver.c,177 :: 		}
IT	AL
BAL	L_SerialFlash_WriteArray2
L_SerialFlash_WriteArray3:
;SF_driver.c,182 :: 		for (counter=0; counter < nCount; counter++){
MOVS	R3, #0
STRH	R3, [SP, #12]
L_SerialFlash_WriteArray5:
LDRH	R4, [SP, #24]
LDRH	R3, [SP, #12]
CMP	R3, R4
IT	CS
BCS	L_SerialFlash_WriteArray6
;SF_driver.c,183 :: 		if (*pData != SerialFlash_ReadByte(address))
LDR	R3, [SP, #20]
LDRB	R3, [R3, #0]
STRB	R3, [SP, #36]
LDR	R0, [SP, #16]
BL	_SerialFlash_ReadByte+0
LDRB	R3, [SP, #36]
CMP	R3, R0
IT	EQ
BEQ	L_SerialFlash_WriteArray8
;SF_driver.c,184 :: 		return 0;
MOVS	R0, #0
IT	AL
BAL	L_end_SerialFlash_WriteArray
L_SerialFlash_WriteArray8:
;SF_driver.c,185 :: 		pData++;
LDR	R3, [SP, #20]
ADDS	R3, R3, #1
STR	R3, [SP, #20]
;SF_driver.c,186 :: 		address++;
LDR	R3, [SP, #16]
ADDS	R3, R3, #1
STR	R3, [SP, #16]
;SF_driver.c,182 :: 		for (counter=0; counter < nCount; counter++){
LDRH	R3, [SP, #12]
ADDS	R3, R3, #1
STRH	R3, [SP, #12]
;SF_driver.c,187 :: 		}
IT	AL
BAL	L_SerialFlash_WriteArray5
L_SerialFlash_WriteArray6:
;SF_driver.c,189 :: 		return 1;
MOVS	R0, #1
;SF_driver.c,190 :: 		}
L_end_SerialFlash_WriteArray:
LDR	LR, [SP, #0]
ADD	SP, SP, #40
BX	LR
; end of _SerialFlash_WriteArray
_SerialFlash_ReadArray:
;SF_driver.c,200 :: 		void SerialFlash_ReadArray(unsigned long address, unsigned char* pData, unsigned int nCount){
SUB	SP, SP, #16
STR	LR, [SP, #0]
STR	R0, [SP, #4]
STR	R1, [SP, #8]
STRH	R2, [SP, #12]
;SF_driver.c,201 :: 		CS_Serial_Flash_bit = 0;
MOVS	R4, #0
SXTB	R4, R4
MOVW	R3, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R3, #hi_addr(CS_Serial_Flash_bit+0)
STR	R4, [R3, #0]
;SF_driver.c,202 :: 		SPI_Wr_Ptr(_SERIAL_FLASH_CMD_READ);
MOVS	R0, #3
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,203 :: 		SPI_Wr_Ptr(Higher(address));
ADD	R3, SP, #4
ADDS	R3, R3, #2
LDRB	R3, [R3, #0]
UXTB	R4, R3
UXTH	R0, R4
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,204 :: 		SPI_Wr_Ptr(Hi(address));
ADD	R3, SP, #4
ADDS	R3, R3, #1
LDRB	R3, [R3, #0]
UXTB	R4, R3
UXTH	R0, R4
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,205 :: 		SPI_Wr_Ptr(Lo(address));
ADD	R3, SP, #4
LDRB	R3, [R3, #0]
UXTB	R4, R3
UXTH	R0, R4
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,207 :: 		while(nCount--){
L_SerialFlash_ReadArray9:
LDRH	R4, [SP, #12]
LDRH	R3, [SP, #12]
SUBS	R3, R3, #1
STRH	R3, [SP, #12]
CMP	R4, #0
IT	EQ
BEQ	L_SerialFlash_ReadArray10
;SF_driver.c,208 :: 		*pData++ = SPI_Rd_Ptr(0);
MOVS	R0, #0
MOVW	R4, #lo_addr(_SPI_Rd_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Rd_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
LDR	R3, [SP, #8]
STRB	R0, [R3, #0]
LDR	R3, [SP, #8]
ADDS	R3, R3, #1
STR	R3, [SP, #8]
;SF_driver.c,209 :: 		}
IT	AL
BAL	L_SerialFlash_ReadArray9
L_SerialFlash_ReadArray10:
;SF_driver.c,210 :: 		CS_Serial_Flash_bit = 1;
MOVS	R4, #1
SXTB	R4, R4
MOVW	R3, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R3, #hi_addr(CS_Serial_Flash_bit+0)
STR	R4, [R3, #0]
;SF_driver.c,211 :: 		}
L_end_SerialFlash_ReadArray:
LDR	LR, [SP, #0]
ADD	SP, SP, #16
BX	LR
; end of _SerialFlash_ReadArray
_SerialFlash_ChipErase:
;SF_driver.c,220 :: 		void SerialFlash_ChipErase(void){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;SF_driver.c,222 :: 		SerialFlash_WriteEnable();
BL	_SerialFlash_WriteEnable+0
;SF_driver.c,224 :: 		CS_Serial_Flash_bit = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R0, #hi_addr(CS_Serial_Flash_bit+0)
STR	R1, [R0, #0]
;SF_driver.c,225 :: 		SPI_Wr_Ptr(_SERIAL_FLASH_CMD_ERASE);
MOVS	R0, #199
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,226 :: 		CS_Serial_Flash_bit = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R0, #hi_addr(CS_Serial_Flash_bit+0)
STR	R1, [R0, #0]
;SF_driver.c,229 :: 		while(SerialFlash_IsWriteBusy());
L_SerialFlash_ChipErase11:
BL	_SerialFlash_IsWriteBusy+0
CMP	R0, #0
IT	EQ
BEQ	L_SerialFlash_ChipErase12
IT	AL
BAL	L_SerialFlash_ChipErase11
L_SerialFlash_ChipErase12:
;SF_driver.c,230 :: 		}
L_end_SerialFlash_ChipErase:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _SerialFlash_ChipErase
_SerialFlash_ResetWriteProtection:
;SF_driver.c,239 :: 		void SerialFlash_ResetWriteProtection(){
SUB	SP, SP, #12
STR	LR, [SP, #0]
;SF_driver.c,241 :: 		CS_Serial_Flash_bit = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R0, #hi_addr(CS_Serial_Flash_bit+0)
STR	R0, [SP, #8]
STR	R1, [R0, #0]
;SF_driver.c,242 :: 		SPI_Wr_Ptr(_SERIAL_FLASH_CMD_EWSR);
MOVS	R0, #6
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
STR	R4, [SP, #4]
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,243 :: 		CS_Serial_Flash_bit = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R0, #hi_addr(CS_Serial_Flash_bit+0)
STR	R1, [R0, #0]
;SF_driver.c,245 :: 		CS_Serial_Flash_bit = 0;
MOVS	R1, #0
SXTB	R1, R1
LDR	R0, [SP, #8]
STR	R1, [R0, #0]
;SF_driver.c,246 :: 		SPI_Wr_Ptr(_SERIAL_FLASH_CMD_EWSR);
MOVS	R0, #6
LDR	R4, [SP, #4]
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,247 :: 		SPI_Wr_Ptr(0);
MOVS	R0, #0
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,248 :: 		CS_Serial_Flash_bit = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R0, #hi_addr(CS_Serial_Flash_bit+0)
STR	R1, [R0, #0]
;SF_driver.c,249 :: 		}
L_end_SerialFlash_ResetWriteProtection:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _SerialFlash_ResetWriteProtection
_SerialFlash_SectorErase:
;SF_driver.c,258 :: 		void SerialFlash_SectorErase(unsigned long address){
SUB	SP, SP, #8
STR	LR, [SP, #0]
STR	R0, [SP, #4]
;SF_driver.c,260 :: 		SerialFlash_WriteEnable();
BL	_SerialFlash_WriteEnable+0
;SF_driver.c,262 :: 		CS_Serial_Flash_bit = 0;
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R1, #hi_addr(CS_Serial_Flash_bit+0)
STR	R2, [R1, #0]
;SF_driver.c,263 :: 		SPI_Wr_Ptr(_SERIAL_FLASH_CMD_SER);
MOVS	R0, #216
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,264 :: 		SPI_Wr_Ptr(Higher(address));
ADD	R1, SP, #4
ADDS	R1, R1, #2
LDRB	R1, [R1, #0]
UXTB	R4, R1
UXTH	R0, R4
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,265 :: 		SPI_Wr_Ptr(Hi(address));
ADD	R1, SP, #4
ADDS	R1, R1, #1
LDRB	R1, [R1, #0]
UXTB	R4, R1
UXTH	R0, R4
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,266 :: 		SPI_Wr_Ptr(Lo(address));
ADD	R1, SP, #4
LDRB	R1, [R1, #0]
UXTB	R4, R1
UXTH	R0, R4
MOVW	R4, #lo_addr(_SPI_Wr_Ptr+0)
MOVT	R4, #hi_addr(_SPI_Wr_Ptr+0)
LDR	R4, [R4, #0]
BLX	R4
;SF_driver.c,267 :: 		CS_Serial_Flash_bit = 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(CS_Serial_Flash_bit+0)
MOVT	R1, #hi_addr(CS_Serial_Flash_bit+0)
STR	R2, [R1, #0]
;SF_driver.c,270 :: 		while(SerialFlash_IsWriteBusy());
L_SerialFlash_SectorErase13:
BL	_SerialFlash_IsWriteBusy+0
CMP	R0, #0
IT	EQ
BEQ	L_SerialFlash_SectorErase14
IT	AL
BAL	L_SerialFlash_SectorErase13
L_SerialFlash_SectorErase14:
;SF_driver.c,271 :: 		}
L_end_SerialFlash_SectorErase:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _SerialFlash_SectorErase
