_Init_ADC:
;BPWasher_driver.c,64 :: 		void Init_ADC() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_driver.c,65 :: 		ADC_Set_Input_Channel(_ADC_CHANNEL_8 | _ADC_CHANNEL_9);
MOVW	R0, #768
BL	_ADC_Set_Input_Channel+0
;BPWasher_driver.c,66 :: 		ADC1_Init();
BL	_ADC1_Init+0
;BPWasher_driver.c,67 :: 		Delay_ms(100);
MOVW	R7, #13609
MOVT	R7, #71
NOP
NOP
L_Init_ADC0:
SUBS	R7, R7, #1
BNE	L_Init_ADC0
NOP
NOP
;BPWasher_driver.c,68 :: 		}
L_end_Init_ADC:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Init_ADC
BPWasher_driver_InitializeTouchPanel:
;BPWasher_driver.c,69 :: 		static void InitializeTouchPanel() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_driver.c,70 :: 		Init_ADC();
BL	_Init_ADC+0
;BPWasher_driver.c,71 :: 		TFT_Init_ILI9341_8bit(320, 240);
MOVS	R1, #240
MOVW	R0, #320
BL	_TFT_Init_ILI9341_8bit+0
;BPWasher_driver.c,73 :: 		TP_TFT_Init(320, 240, 8, 9);                                  // Initialize touch panel
MOVS	R3, #9
MOVS	R2, #8
MOVS	R1, #240
MOVW	R0, #320
BL	_TP_TFT_Init+0
;BPWasher_driver.c,74 :: 		TP_TFT_Set_ADC_Threshold(ADC_THRESHOLD);                              // Set touch panel ADC threshold
MOVW	R0, #1500
SXTH	R0, R0
BL	_TP_TFT_Set_ADC_Threshold+0
;BPWasher_driver.c,76 :: 		PenDown = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PenDown+0)
MOVT	R0, #hi_addr(_PenDown+0)
STRB	R1, [R0, #0]
;BPWasher_driver.c,77 :: 		PressedObject = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PressedObject+0)
MOVT	R0, #hi_addr(_PressedObject+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,78 :: 		PressedObjectType = -1;
MOVW	R1, #65535
SXTH	R1, R1
MOVW	R0, #lo_addr(_PressedObjectType+0)
MOVT	R0, #hi_addr(_PressedObjectType+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,79 :: 		}
L_end_InitializeTouchPanel:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of BPWasher_driver_InitializeTouchPanel
_Calibrate:
;BPWasher_driver.c,90 :: 		void Calibrate() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_driver.c,92 :: 		x_tp_min = TP_XMIN;
MOVW	R1, #358
MOVW	R0, #lo_addr(_x_tp_min+0)
MOVT	R0, #hi_addr(_x_tp_min+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,93 :: 		x_tp_max = TP_XMAX;
MOVW	R1, #3464
MOVW	R0, #lo_addr(_x_tp_max+0)
MOVT	R0, #hi_addr(_x_tp_max+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,94 :: 		y_tp_min = TP_YMIN;
MOVS	R1, #212
MOVW	R0, #lo_addr(_y_tp_min+0)
MOVT	R0, #hi_addr(_y_tp_min+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,95 :: 		y_tp_max = TP_YMAX;
MOVW	R1, #3678
MOVW	R0, #lo_addr(_y_tp_max+0)
MOVT	R0, #hi_addr(_y_tp_max+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,122 :: 		TP_TFT_Set_Calibration_Consts( x_tp_min, x_tp_max, y_tp_min, y_tp_max);
MOVW	R3, #3678
MOVW	R2, #212
MOVW	R1, #3464
MOVW	R0, #358
BL	_TP_TFT_Set_Calibration_Consts+0
;BPWasher_driver.c,123 :: 		Delay_ms(500);
MOVW	R7, #2515
MOVT	R7, #356
NOP
NOP
L_Calibrate2:
SUBS	R7, R7, #1
BNE	L_Calibrate2
NOP
NOP
NOP
NOP
;BPWasher_driver.c,124 :: 		}
L_end_Calibrate:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Calibrate
BPWasher_driver_InitializeObjects:
;BPWasher_driver.c,699 :: 		static void InitializeObjects() {
SUB	SP, SP, #4
;BPWasher_driver.c,700 :: 		Keypad.Color                     = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Keypad+0)
MOVT	R0, #hi_addr(_Keypad+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,701 :: 		Keypad.Width                     = 320;
MOVW	R1, #320
MOVW	R0, #lo_addr(_Keypad+2)
MOVT	R0, #hi_addr(_Keypad+2)
STRH	R1, [R0, #0]
;BPWasher_driver.c,702 :: 		Keypad.Height                    = 240;
MOVS	R1, #240
MOVW	R0, #lo_addr(_Keypad+4)
MOVT	R0, #hi_addr(_Keypad+4)
STRH	R1, [R0, #0]
;BPWasher_driver.c,703 :: 		Keypad.ButtonsCount              = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Keypad+8)
MOVT	R0, #hi_addr(_Keypad+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,704 :: 		Keypad.Buttons                   = Screen1_Buttons;
MOVW	R1, #lo_addr(_Screen1_Buttons+0)
MOVT	R1, #hi_addr(_Screen1_Buttons+0)
MOVW	R0, #lo_addr(_Keypad+12)
MOVT	R0, #hi_addr(_Keypad+12)
STR	R1, [R0, #0]
;BPWasher_driver.c,705 :: 		Keypad.Buttons_RoundCount        = 14;
MOVS	R1, #14
MOVW	R0, #lo_addr(_Keypad+16)
MOVT	R0, #hi_addr(_Keypad+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,706 :: 		Keypad.Buttons_Round             = Screen1_Buttons_Round;
MOVW	R1, #lo_addr(_Screen1_Buttons_Round+0)
MOVT	R1, #hi_addr(_Screen1_Buttons_Round+0)
MOVW	R0, #lo_addr(_Keypad+20)
MOVT	R0, #hi_addr(_Keypad+20)
STR	R1, [R0, #0]
;BPWasher_driver.c,707 :: 		Keypad.CButtons_RoundCount       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Keypad+24)
MOVT	R0, #hi_addr(_Keypad+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,708 :: 		Keypad.CButtons_Round            = Screen1_CButtons_Round;
MOVW	R1, #lo_addr(_Screen1_CButtons_Round+0)
MOVT	R1, #hi_addr(_Screen1_CButtons_Round+0)
MOVW	R0, #lo_addr(_Keypad+28)
MOVT	R0, #hi_addr(_Keypad+28)
STR	R1, [R0, #0]
;BPWasher_driver.c,709 :: 		Keypad.LabelsCount               = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Keypad+32)
MOVT	R0, #hi_addr(_Keypad+32)
STRH	R1, [R0, #0]
;BPWasher_driver.c,710 :: 		Keypad.Labels                    = Screen1_Labels;
MOVW	R1, #lo_addr(_Screen1_Labels+0)
MOVT	R1, #hi_addr(_Screen1_Labels+0)
MOVW	R0, #lo_addr(_Keypad+36)
MOVT	R0, #hi_addr(_Keypad+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,711 :: 		Keypad.ImagesCount               = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Keypad+40)
MOVT	R0, #hi_addr(_Keypad+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,712 :: 		Keypad.CImagesCount              = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Keypad+48)
MOVT	R0, #hi_addr(_Keypad+48)
STRH	R1, [R0, #0]
;BPWasher_driver.c,713 :: 		Keypad.CImages                   = Screen1_CImages;
MOVW	R1, #lo_addr(_Screen1_CImages+0)
MOVT	R1, #hi_addr(_Screen1_CImages+0)
MOVW	R0, #lo_addr(_Keypad+52)
MOVT	R0, #hi_addr(_Keypad+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,714 :: 		Keypad.CirclesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Keypad+56)
MOVT	R0, #hi_addr(_Keypad+56)
STRH	R1, [R0, #0]
;BPWasher_driver.c,715 :: 		Keypad.BoxesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Keypad+64)
MOVT	R0, #hi_addr(_Keypad+64)
STRH	R1, [R0, #0]
;BPWasher_driver.c,716 :: 		Keypad.LinesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Keypad+72)
MOVT	R0, #hi_addr(_Keypad+72)
STRH	R1, [R0, #0]
;BPWasher_driver.c,717 :: 		Keypad.CheckBoxesCount           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Keypad+80)
MOVT	R0, #hi_addr(_Keypad+80)
STRH	R1, [R0, #0]
;BPWasher_driver.c,718 :: 		Keypad.ObjectsCount              = 18;
MOVS	R1, #18
MOVW	R0, #lo_addr(_Keypad+6)
MOVT	R0, #hi_addr(_Keypad+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,720 :: 		_main.Color                     = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(__main+0)
MOVT	R0, #hi_addr(__main+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,721 :: 		_main.Width                     = 320;
MOVW	R1, #320
MOVW	R0, #lo_addr(__main+2)
MOVT	R0, #hi_addr(__main+2)
STRH	R1, [R0, #0]
;BPWasher_driver.c,722 :: 		_main.Height                    = 240;
MOVS	R1, #240
MOVW	R0, #lo_addr(__main+4)
MOVT	R0, #hi_addr(__main+4)
STRH	R1, [R0, #0]
;BPWasher_driver.c,723 :: 		_main.ButtonsCount              = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(__main+8)
MOVT	R0, #hi_addr(__main+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,724 :: 		_main.Buttons                   = Screen2_Buttons;
MOVW	R1, #lo_addr(_Screen2_Buttons+0)
MOVT	R1, #hi_addr(_Screen2_Buttons+0)
MOVW	R0, #lo_addr(__main+12)
MOVT	R0, #hi_addr(__main+12)
STR	R1, [R0, #0]
;BPWasher_driver.c,725 :: 		_main.Buttons_RoundCount        = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(__main+16)
MOVT	R0, #hi_addr(__main+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,726 :: 		_main.Buttons_Round             = Screen2_Buttons_Round;
MOVW	R1, #lo_addr(_Screen2_Buttons_Round+0)
MOVT	R1, #hi_addr(_Screen2_Buttons_Round+0)
MOVW	R0, #lo_addr(__main+20)
MOVT	R0, #hi_addr(__main+20)
STR	R1, [R0, #0]
;BPWasher_driver.c,727 :: 		_main.CButtons_RoundCount       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(__main+24)
MOVT	R0, #hi_addr(__main+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,728 :: 		_main.LabelsCount               = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(__main+32)
MOVT	R0, #hi_addr(__main+32)
STRH	R1, [R0, #0]
;BPWasher_driver.c,729 :: 		_main.Labels                    = Screen2_Labels;
MOVW	R1, #lo_addr(_Screen2_Labels+0)
MOVT	R1, #hi_addr(_Screen2_Labels+0)
MOVW	R0, #lo_addr(__main+36)
MOVT	R0, #hi_addr(__main+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,730 :: 		_main.ImagesCount               = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(__main+40)
MOVT	R0, #hi_addr(__main+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,731 :: 		_main.Images                    = Screen2_Images;
MOVW	R1, #lo_addr(_Screen2_Images+0)
MOVT	R1, #hi_addr(_Screen2_Images+0)
MOVW	R0, #lo_addr(__main+44)
MOVT	R0, #hi_addr(__main+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,732 :: 		_main.CImagesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(__main+48)
MOVT	R0, #hi_addr(__main+48)
STRH	R1, [R0, #0]
;BPWasher_driver.c,733 :: 		_main.CirclesCount              = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(__main+56)
MOVT	R0, #hi_addr(__main+56)
STRH	R1, [R0, #0]
;BPWasher_driver.c,734 :: 		_main.Circles                   = Screen2_Circles;
MOVW	R1, #lo_addr(_Screen2_Circles+0)
MOVT	R1, #hi_addr(_Screen2_Circles+0)
MOVW	R0, #lo_addr(__main+60)
MOVT	R0, #hi_addr(__main+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,735 :: 		_main.BoxesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(__main+64)
MOVT	R0, #hi_addr(__main+64)
STRH	R1, [R0, #0]
;BPWasher_driver.c,736 :: 		_main.LinesCount                = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(__main+72)
MOVT	R0, #hi_addr(__main+72)
STRH	R1, [R0, #0]
;BPWasher_driver.c,737 :: 		_main.Lines                     = Screen2_Lines;
MOVW	R1, #lo_addr(_Screen2_Lines+0)
MOVT	R1, #hi_addr(_Screen2_Lines+0)
MOVW	R0, #lo_addr(__main+76)
MOVT	R0, #hi_addr(__main+76)
STR	R1, [R0, #0]
;BPWasher_driver.c,738 :: 		_main.CheckBoxesCount           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(__main+80)
MOVT	R0, #hi_addr(__main+80)
STRH	R1, [R0, #0]
;BPWasher_driver.c,739 :: 		_main.ObjectsCount              = 13;
MOVS	R1, #13
MOVW	R0, #lo_addr(__main+6)
MOVT	R0, #hi_addr(__main+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,741 :: 		Settings.Color                     = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings+0)
MOVT	R0, #hi_addr(_Settings+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,742 :: 		Settings.Width                     = 320;
MOVW	R1, #320
MOVW	R0, #lo_addr(_Settings+2)
MOVT	R0, #hi_addr(_Settings+2)
STRH	R1, [R0, #0]
;BPWasher_driver.c,743 :: 		Settings.Height                    = 240;
MOVS	R1, #240
MOVW	R0, #lo_addr(_Settings+4)
MOVT	R0, #hi_addr(_Settings+4)
STRH	R1, [R0, #0]
;BPWasher_driver.c,744 :: 		Settings.ButtonsCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings+8)
MOVT	R0, #hi_addr(_Settings+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,745 :: 		Settings.Buttons_RoundCount        = 6;
MOVS	R1, #6
MOVW	R0, #lo_addr(_Settings+16)
MOVT	R0, #hi_addr(_Settings+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,746 :: 		Settings.Buttons_Round             = Screen3_Buttons_Round;
MOVW	R1, #lo_addr(_Screen3_Buttons_Round+0)
MOVT	R1, #hi_addr(_Screen3_Buttons_Round+0)
MOVW	R0, #lo_addr(_Settings+20)
MOVT	R0, #hi_addr(_Settings+20)
STR	R1, [R0, #0]
;BPWasher_driver.c,747 :: 		Settings.CButtons_RoundCount       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings+24)
MOVT	R0, #hi_addr(_Settings+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,748 :: 		Settings.LabelsCount               = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings+32)
MOVT	R0, #hi_addr(_Settings+32)
STRH	R1, [R0, #0]
;BPWasher_driver.c,749 :: 		Settings.ImagesCount               = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings+40)
MOVT	R0, #hi_addr(_Settings+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,750 :: 		Settings.CImagesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings+48)
MOVT	R0, #hi_addr(_Settings+48)
STRH	R1, [R0, #0]
;BPWasher_driver.c,751 :: 		Settings.CirclesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings+56)
MOVT	R0, #hi_addr(_Settings+56)
STRH	R1, [R0, #0]
;BPWasher_driver.c,752 :: 		Settings.BoxesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings+64)
MOVT	R0, #hi_addr(_Settings+64)
STRH	R1, [R0, #0]
;BPWasher_driver.c,753 :: 		Settings.LinesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings+72)
MOVT	R0, #hi_addr(_Settings+72)
STRH	R1, [R0, #0]
;BPWasher_driver.c,754 :: 		Settings.CheckBoxesCount           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings+80)
MOVT	R0, #hi_addr(_Settings+80)
STRH	R1, [R0, #0]
;BPWasher_driver.c,755 :: 		Settings.ObjectsCount              = 6;
MOVS	R1, #6
MOVW	R0, #lo_addr(_Settings+6)
MOVT	R0, #hi_addr(_Settings+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,757 :: 		PID.Color                     = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID+0)
MOVT	R0, #hi_addr(_PID+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,758 :: 		PID.Width                     = 320;
MOVW	R1, #320
MOVW	R0, #lo_addr(_PID+2)
MOVT	R0, #hi_addr(_PID+2)
STRH	R1, [R0, #0]
;BPWasher_driver.c,759 :: 		PID.Height                    = 240;
MOVS	R1, #240
MOVW	R0, #lo_addr(_PID+4)
MOVT	R0, #hi_addr(_PID+4)
STRH	R1, [R0, #0]
;BPWasher_driver.c,760 :: 		PID.ButtonsCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID+8)
MOVT	R0, #hi_addr(_PID+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,761 :: 		PID.Buttons_RoundCount        = 7;
MOVS	R1, #7
MOVW	R0, #lo_addr(_PID+16)
MOVT	R0, #hi_addr(_PID+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,762 :: 		PID.Buttons_Round             = Screen4_Buttons_Round;
MOVW	R1, #lo_addr(_Screen4_Buttons_Round+0)
MOVT	R1, #hi_addr(_Screen4_Buttons_Round+0)
MOVW	R0, #lo_addr(_PID+20)
MOVT	R0, #hi_addr(_PID+20)
STR	R1, [R0, #0]
;BPWasher_driver.c,763 :: 		PID.CButtons_RoundCount       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID+24)
MOVT	R0, #hi_addr(_PID+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,764 :: 		PID.CButtons_Round            = Screen4_CButtons_Round;
MOVW	R1, #lo_addr(_Screen4_CButtons_Round+0)
MOVT	R1, #hi_addr(_Screen4_CButtons_Round+0)
MOVW	R0, #lo_addr(_PID+28)
MOVT	R0, #hi_addr(_PID+28)
STR	R1, [R0, #0]
;BPWasher_driver.c,765 :: 		PID.LabelsCount               = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID+32)
MOVT	R0, #hi_addr(_PID+32)
STRH	R1, [R0, #0]
;BPWasher_driver.c,766 :: 		PID.ImagesCount               = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID+40)
MOVT	R0, #hi_addr(_PID+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,767 :: 		PID.Images                    = Screen4_Images;
MOVW	R1, #lo_addr(_Screen4_Images+0)
MOVT	R1, #hi_addr(_Screen4_Images+0)
MOVW	R0, #lo_addr(_PID+44)
MOVT	R0, #hi_addr(_PID+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,768 :: 		PID.CImagesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID+48)
MOVT	R0, #hi_addr(_PID+48)
STRH	R1, [R0, #0]
;BPWasher_driver.c,769 :: 		PID.CirclesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID+56)
MOVT	R0, #hi_addr(_PID+56)
STRH	R1, [R0, #0]
;BPWasher_driver.c,770 :: 		PID.BoxesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID+64)
MOVT	R0, #hi_addr(_PID+64)
STRH	R1, [R0, #0]
;BPWasher_driver.c,771 :: 		PID.LinesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID+72)
MOVT	R0, #hi_addr(_PID+72)
STRH	R1, [R0, #0]
;BPWasher_driver.c,772 :: 		PID.CheckBoxesCount           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID+80)
MOVT	R0, #hi_addr(_PID+80)
STRH	R1, [R0, #0]
;BPWasher_driver.c,773 :: 		PID.ObjectsCount              = 9;
MOVS	R1, #9
MOVW	R0, #lo_addr(_PID+6)
MOVT	R0, #hi_addr(_PID+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,775 :: 		Calibration.Color                     = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Calibration+0)
MOVT	R0, #hi_addr(_Calibration+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,776 :: 		Calibration.Width                     = 320;
MOVW	R1, #320
MOVW	R0, #lo_addr(_Calibration+2)
MOVT	R0, #hi_addr(_Calibration+2)
STRH	R1, [R0, #0]
;BPWasher_driver.c,777 :: 		Calibration.Height                    = 240;
MOVS	R1, #240
MOVW	R0, #lo_addr(_Calibration+4)
MOVT	R0, #hi_addr(_Calibration+4)
STRH	R1, [R0, #0]
;BPWasher_driver.c,778 :: 		Calibration.ButtonsCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Calibration+8)
MOVT	R0, #hi_addr(_Calibration+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,779 :: 		Calibration.Buttons_RoundCount        = 6;
MOVS	R1, #6
MOVW	R0, #lo_addr(_Calibration+16)
MOVT	R0, #hi_addr(_Calibration+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,780 :: 		Calibration.Buttons_Round             = Screen5_Buttons_Round;
MOVW	R1, #lo_addr(_Screen5_Buttons_Round+0)
MOVT	R1, #hi_addr(_Screen5_Buttons_Round+0)
MOVW	R0, #lo_addr(_Calibration+20)
MOVT	R0, #hi_addr(_Calibration+20)
STR	R1, [R0, #0]
;BPWasher_driver.c,781 :: 		Calibration.CButtons_RoundCount       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Calibration+24)
MOVT	R0, #hi_addr(_Calibration+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,782 :: 		Calibration.CButtons_Round            = Screen5_CButtons_Round;
MOVW	R1, #lo_addr(_Screen5_CButtons_Round+0)
MOVT	R1, #hi_addr(_Screen5_CButtons_Round+0)
MOVW	R0, #lo_addr(_Calibration+28)
MOVT	R0, #hi_addr(_Calibration+28)
STR	R1, [R0, #0]
;BPWasher_driver.c,783 :: 		Calibration.LabelsCount               = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_Calibration+32)
MOVT	R0, #hi_addr(_Calibration+32)
STRH	R1, [R0, #0]
;BPWasher_driver.c,784 :: 		Calibration.Labels                    = Screen5_Labels;
MOVW	R1, #lo_addr(_Screen5_Labels+0)
MOVT	R1, #hi_addr(_Screen5_Labels+0)
MOVW	R0, #lo_addr(_Calibration+36)
MOVT	R0, #hi_addr(_Calibration+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,785 :: 		Calibration.ImagesCount               = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Calibration+40)
MOVT	R0, #hi_addr(_Calibration+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,786 :: 		Calibration.Images                    = Screen5_Images;
MOVW	R1, #lo_addr(_Screen5_Images+0)
MOVT	R1, #hi_addr(_Screen5_Images+0)
MOVW	R0, #lo_addr(_Calibration+44)
MOVT	R0, #hi_addr(_Calibration+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,787 :: 		Calibration.CImagesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Calibration+48)
MOVT	R0, #hi_addr(_Calibration+48)
STRH	R1, [R0, #0]
;BPWasher_driver.c,788 :: 		Calibration.CirclesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Calibration+56)
MOVT	R0, #hi_addr(_Calibration+56)
STRH	R1, [R0, #0]
;BPWasher_driver.c,789 :: 		Calibration.BoxesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Calibration+64)
MOVT	R0, #hi_addr(_Calibration+64)
STRH	R1, [R0, #0]
;BPWasher_driver.c,790 :: 		Calibration.LinesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Calibration+72)
MOVT	R0, #hi_addr(_Calibration+72)
STRH	R1, [R0, #0]
;BPWasher_driver.c,791 :: 		Calibration.CheckBoxesCount           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Calibration+80)
MOVT	R0, #hi_addr(_Calibration+80)
STRH	R1, [R0, #0]
;BPWasher_driver.c,792 :: 		Calibration.ObjectsCount              = 10;
MOVS	R1, #10
MOVW	R0, #lo_addr(_Calibration+6)
MOVT	R0, #hi_addr(_Calibration+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,794 :: 		Diags.Color                     = 0x041F;
MOVW	R1, #1055
MOVW	R0, #lo_addr(_Diags+0)
MOVT	R0, #hi_addr(_Diags+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,795 :: 		Diags.Width                     = 320;
MOVW	R1, #320
MOVW	R0, #lo_addr(_Diags+2)
MOVT	R0, #hi_addr(_Diags+2)
STRH	R1, [R0, #0]
;BPWasher_driver.c,796 :: 		Diags.Height                    = 240;
MOVS	R1, #240
MOVW	R0, #lo_addr(_Diags+4)
MOVT	R0, #hi_addr(_Diags+4)
STRH	R1, [R0, #0]
;BPWasher_driver.c,797 :: 		Diags.ButtonsCount              = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diags+8)
MOVT	R0, #hi_addr(_Diags+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,798 :: 		Diags.Buttons                   = Screen6_Buttons;
MOVW	R1, #lo_addr(_Screen6_Buttons+0)
MOVT	R1, #hi_addr(_Screen6_Buttons+0)
MOVW	R0, #lo_addr(_Diags+12)
MOVT	R0, #hi_addr(_Diags+12)
STR	R1, [R0, #0]
;BPWasher_driver.c,799 :: 		Diags.Buttons_RoundCount        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diags+16)
MOVT	R0, #hi_addr(_Diags+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,800 :: 		Diags.CButtons_RoundCount       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diags+24)
MOVT	R0, #hi_addr(_Diags+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,801 :: 		Diags.LabelsCount               = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_Diags+32)
MOVT	R0, #hi_addr(_Diags+32)
STRH	R1, [R0, #0]
;BPWasher_driver.c,802 :: 		Diags.Labels                    = Screen6_Labels;
MOVW	R1, #lo_addr(_Screen6_Labels+0)
MOVT	R1, #hi_addr(_Screen6_Labels+0)
MOVW	R0, #lo_addr(_Diags+36)
MOVT	R0, #hi_addr(_Diags+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,803 :: 		Diags.ImagesCount               = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diags+40)
MOVT	R0, #hi_addr(_Diags+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,804 :: 		Diags.CImagesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diags+48)
MOVT	R0, #hi_addr(_Diags+48)
STRH	R1, [R0, #0]
;BPWasher_driver.c,805 :: 		Diags.CirclesCount              = 6;
MOVS	R1, #6
MOVW	R0, #lo_addr(_Diags+56)
MOVT	R0, #hi_addr(_Diags+56)
STRH	R1, [R0, #0]
;BPWasher_driver.c,806 :: 		Diags.Circles                   = Screen6_Circles;
MOVW	R1, #lo_addr(_Screen6_Circles+0)
MOVT	R1, #hi_addr(_Screen6_Circles+0)
MOVW	R0, #lo_addr(_Diags+60)
MOVT	R0, #hi_addr(_Diags+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,807 :: 		Diags.BoxesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diags+64)
MOVT	R0, #hi_addr(_Diags+64)
STRH	R1, [R0, #0]
;BPWasher_driver.c,808 :: 		Diags.LinesCount                = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diags+72)
MOVT	R0, #hi_addr(_Diags+72)
STRH	R1, [R0, #0]
;BPWasher_driver.c,809 :: 		Diags.Lines                     = Screen6_Lines;
MOVW	R1, #lo_addr(_Screen6_Lines+0)
MOVT	R1, #hi_addr(_Screen6_Lines+0)
MOVW	R0, #lo_addr(_Diags+76)
MOVT	R0, #hi_addr(_Diags+76)
STR	R1, [R0, #0]
;BPWasher_driver.c,810 :: 		Diags.CheckBoxesCount           = 6;
MOVS	R1, #6
MOVW	R0, #lo_addr(_Diags+80)
MOVT	R0, #hi_addr(_Diags+80)
STRH	R1, [R0, #0]
;BPWasher_driver.c,811 :: 		Diags.CheckBoxes                = Screen6_CheckBoxes;
MOVW	R1, #lo_addr(_Screen6_CheckBoxes+0)
MOVT	R1, #hi_addr(_Screen6_CheckBoxes+0)
MOVW	R0, #lo_addr(_Diags+84)
MOVT	R0, #hi_addr(_Diags+84)
STR	R1, [R0, #0]
;BPWasher_driver.c,812 :: 		Diags.ObjectsCount              = 22;
MOVS	R1, #22
MOVW	R0, #lo_addr(_Diags+6)
MOVT	R0, #hi_addr(_Diags+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,814 :: 		SplashLand.Color                     = 0x5AEB;
MOVW	R1, #23275
MOVW	R0, #lo_addr(_SplashLand+0)
MOVT	R0, #hi_addr(_SplashLand+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,815 :: 		SplashLand.Width                     = 320;
MOVW	R1, #320
MOVW	R0, #lo_addr(_SplashLand+2)
MOVT	R0, #hi_addr(_SplashLand+2)
STRH	R1, [R0, #0]
;BPWasher_driver.c,816 :: 		SplashLand.Height                    = 240;
MOVS	R1, #240
MOVW	R0, #lo_addr(_SplashLand+4)
MOVT	R0, #hi_addr(_SplashLand+4)
STRH	R1, [R0, #0]
;BPWasher_driver.c,817 :: 		SplashLand.ButtonsCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_SplashLand+8)
MOVT	R0, #hi_addr(_SplashLand+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,818 :: 		SplashLand.Buttons_RoundCount        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_SplashLand+16)
MOVT	R0, #hi_addr(_SplashLand+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,819 :: 		SplashLand.CButtons_RoundCount       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_SplashLand+24)
MOVT	R0, #hi_addr(_SplashLand+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,820 :: 		SplashLand.LabelsCount               = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_SplashLand+32)
MOVT	R0, #hi_addr(_SplashLand+32)
STRH	R1, [R0, #0]
;BPWasher_driver.c,821 :: 		SplashLand.Labels                    = Screen7_Labels;
MOVW	R1, #lo_addr(_Screen7_Labels+0)
MOVT	R1, #hi_addr(_Screen7_Labels+0)
MOVW	R0, #lo_addr(_SplashLand+36)
MOVT	R0, #hi_addr(_SplashLand+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,822 :: 		SplashLand.ImagesCount               = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_SplashLand+40)
MOVT	R0, #hi_addr(_SplashLand+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,823 :: 		SplashLand.Images                    = Screen7_Images;
MOVW	R1, #lo_addr(_Screen7_Images+0)
MOVT	R1, #hi_addr(_Screen7_Images+0)
MOVW	R0, #lo_addr(_SplashLand+44)
MOVT	R0, #hi_addr(_SplashLand+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,824 :: 		SplashLand.CImagesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_SplashLand+48)
MOVT	R0, #hi_addr(_SplashLand+48)
STRH	R1, [R0, #0]
;BPWasher_driver.c,825 :: 		SplashLand.CirclesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_SplashLand+56)
MOVT	R0, #hi_addr(_SplashLand+56)
STRH	R1, [R0, #0]
;BPWasher_driver.c,826 :: 		SplashLand.BoxesCount                = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_SplashLand+64)
MOVT	R0, #hi_addr(_SplashLand+64)
STRH	R1, [R0, #0]
;BPWasher_driver.c,827 :: 		SplashLand.Boxes                     = Screen7_Boxes;
MOVW	R1, #lo_addr(_Screen7_Boxes+0)
MOVT	R1, #hi_addr(_Screen7_Boxes+0)
MOVW	R0, #lo_addr(_SplashLand+68)
MOVT	R0, #hi_addr(_SplashLand+68)
STR	R1, [R0, #0]
;BPWasher_driver.c,828 :: 		SplashLand.LinesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_SplashLand+72)
MOVT	R0, #hi_addr(_SplashLand+72)
STRH	R1, [R0, #0]
;BPWasher_driver.c,829 :: 		SplashLand.CheckBoxesCount           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_SplashLand+80)
MOVT	R0, #hi_addr(_SplashLand+80)
STRH	R1, [R0, #0]
;BPWasher_driver.c,830 :: 		SplashLand.ObjectsCount              = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_SplashLand+6)
MOVT	R0, #hi_addr(_SplashLand+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,832 :: 		ErrorLog.Color                     = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog+0)
MOVT	R0, #hi_addr(_ErrorLog+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,833 :: 		ErrorLog.Width                     = 320;
MOVW	R1, #320
MOVW	R0, #lo_addr(_ErrorLog+2)
MOVT	R0, #hi_addr(_ErrorLog+2)
STRH	R1, [R0, #0]
;BPWasher_driver.c,834 :: 		ErrorLog.Height                    = 240;
MOVS	R1, #240
MOVW	R0, #lo_addr(_ErrorLog+4)
MOVT	R0, #hi_addr(_ErrorLog+4)
STRH	R1, [R0, #0]
;BPWasher_driver.c,835 :: 		ErrorLog.ButtonsCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog+8)
MOVT	R0, #hi_addr(_ErrorLog+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,836 :: 		ErrorLog.Buttons_RoundCount        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ErrorLog+16)
MOVT	R0, #hi_addr(_ErrorLog+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,837 :: 		ErrorLog.Buttons_Round             = Screen8_Buttons_Round;
MOVW	R1, #lo_addr(_Screen8_Buttons_Round+0)
MOVT	R1, #hi_addr(_Screen8_Buttons_Round+0)
MOVW	R0, #lo_addr(_ErrorLog+20)
MOVT	R0, #hi_addr(_ErrorLog+20)
STR	R1, [R0, #0]
;BPWasher_driver.c,838 :: 		ErrorLog.CButtons_RoundCount       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog+24)
MOVT	R0, #hi_addr(_ErrorLog+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,839 :: 		ErrorLog.LabelsCount               = ERRLOG_LINE_N+1;
MOVS	R1, #11
MOVW	R0, #lo_addr(_ErrorLog+32)
MOVT	R0, #hi_addr(_ErrorLog+32)
STRH	R1, [R0, #0]
;BPWasher_driver.c,840 :: 		ErrorLog.Labels                    = Screen8_Labels;
MOVW	R1, #lo_addr(_Screen8_Labels+0)
MOVT	R1, #hi_addr(_Screen8_Labels+0)
MOVW	R0, #lo_addr(_ErrorLog+36)
MOVT	R0, #hi_addr(_ErrorLog+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,841 :: 		ErrorLog.ImagesCount               = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog+40)
MOVT	R0, #hi_addr(_ErrorLog+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,842 :: 		ErrorLog.CImagesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog+48)
MOVT	R0, #hi_addr(_ErrorLog+48)
STRH	R1, [R0, #0]
;BPWasher_driver.c,843 :: 		ErrorLog.CirclesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog+56)
MOVT	R0, #hi_addr(_ErrorLog+56)
STRH	R1, [R0, #0]
;BPWasher_driver.c,844 :: 		ErrorLog.BoxesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog+64)
MOVT	R0, #hi_addr(_ErrorLog+64)
STRH	R1, [R0, #0]
;BPWasher_driver.c,845 :: 		ErrorLog.LinesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog+72)
MOVT	R0, #hi_addr(_ErrorLog+72)
STRH	R1, [R0, #0]
;BPWasher_driver.c,846 :: 		ErrorLog.CheckBoxesCount           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog+80)
MOVT	R0, #hi_addr(_ErrorLog+80)
STRH	R1, [R0, #0]
;BPWasher_driver.c,847 :: 		ErrorLog.ObjectsCount              = ERRLOG_LINE_N+2;
MOVS	R1, #12
MOVW	R0, #lo_addr(_ErrorLog+6)
MOVT	R0, #hi_addr(_ErrorLog+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,849 :: 		Config.Color                     = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Config+0)
MOVT	R0, #hi_addr(_Config+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,850 :: 		Config.Width                     = 320;
MOVW	R1, #320
MOVW	R0, #lo_addr(_Config+2)
MOVT	R0, #hi_addr(_Config+2)
STRH	R1, [R0, #0]
;BPWasher_driver.c,851 :: 		Config.Height                    = 240;
MOVS	R1, #240
MOVW	R0, #lo_addr(_Config+4)
MOVT	R0, #hi_addr(_Config+4)
STRH	R1, [R0, #0]
;BPWasher_driver.c,852 :: 		Config.ButtonsCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Config+8)
MOVT	R0, #hi_addr(_Config+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,853 :: 		Config.Buttons_RoundCount        = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_Config+16)
MOVT	R0, #hi_addr(_Config+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,854 :: 		Config.Buttons_Round             = Screen9_Buttons_Round;
MOVW	R1, #lo_addr(_Screen9_Buttons_Round+0)
MOVT	R1, #hi_addr(_Screen9_Buttons_Round+0)
MOVW	R0, #lo_addr(_Config+20)
MOVT	R0, #hi_addr(_Config+20)
STR	R1, [R0, #0]
;BPWasher_driver.c,855 :: 		Config.CButtons_RoundCount       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Config+24)
MOVT	R0, #hi_addr(_Config+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,856 :: 		Config.LabelsCount               = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_Config+32)
MOVT	R0, #hi_addr(_Config+32)
STRH	R1, [R0, #0]
;BPWasher_driver.c,857 :: 		Config.Labels                    = Screen9_Labels;
MOVW	R1, #lo_addr(_Screen9_Labels+0)
MOVT	R1, #hi_addr(_Screen9_Labels+0)
MOVW	R0, #lo_addr(_Config+36)
MOVT	R0, #hi_addr(_Config+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,858 :: 		Config.ImagesCount               = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Config+40)
MOVT	R0, #hi_addr(_Config+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,859 :: 		Config.Images                    = Screen9_Images;
MOVW	R1, #lo_addr(_Screen9_Images+0)
MOVT	R1, #hi_addr(_Screen9_Images+0)
MOVW	R0, #lo_addr(_Config+44)
MOVT	R0, #hi_addr(_Config+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,860 :: 		Config.CImagesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Config+48)
MOVT	R0, #hi_addr(_Config+48)
STRH	R1, [R0, #0]
;BPWasher_driver.c,861 :: 		Config.CirclesCount              = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Config+56)
MOVT	R0, #hi_addr(_Config+56)
STRH	R1, [R0, #0]
;BPWasher_driver.c,862 :: 		Config.BoxesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Config+64)
MOVT	R0, #hi_addr(_Config+64)
STRH	R1, [R0, #0]
;BPWasher_driver.c,863 :: 		Config.LinesCount                = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Config+72)
MOVT	R0, #hi_addr(_Config+72)
STRH	R1, [R0, #0]
;BPWasher_driver.c,864 :: 		Config.CheckBoxesCount           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Config+80)
MOVT	R0, #hi_addr(_Config+80)
STRH	R1, [R0, #0]
;BPWasher_driver.c,865 :: 		Config.ObjectsCount              = 7;
MOVS	R1, #7
MOVW	R0, #lo_addr(_Config+6)
MOVT	R0, #hi_addr(_Config+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,867 :: 		Key_7.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_7+0)
MOVT	R0, #hi_addr(_Key_7+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,868 :: 		Key_7.Order           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_7+4)
MOVT	R0, #hi_addr(_Key_7+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,869 :: 		Key_7.Left            = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_7+6)
MOVT	R0, #hi_addr(_Key_7+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,870 :: 		Key_7.Top             = 162;
MOVS	R1, #162
MOVW	R0, #lo_addr(_Key_7+8)
MOVT	R0, #hi_addr(_Key_7+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,871 :: 		Key_7.Width           = 80;
MOVS	R1, #80
MOVW	R0, #lo_addr(_Key_7+10)
MOVT	R0, #hi_addr(_Key_7+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,872 :: 		Key_7.Height          = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Key_7+12)
MOVT	R0, #hi_addr(_Key_7+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,873 :: 		Key_7.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_7+14)
MOVT	R0, #hi_addr(_Key_7+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,874 :: 		Key_7.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_7+16)
MOVT	R0, #hi_addr(_Key_7+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,875 :: 		Key_7.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_7+18)
MOVT	R0, #hi_addr(_Key_7+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,876 :: 		Key_7.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_7+19)
MOVT	R0, #hi_addr(_Key_7+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,877 :: 		Key_7.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_7+20)
MOVT	R0, #hi_addr(_Key_7+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,878 :: 		Key_7.Caption         = Key_7_Caption;
MOVW	R1, #lo_addr(_Key_7_Caption+0)
MOVT	R1, #hi_addr(_Key_7_Caption+0)
MOVW	R0, #lo_addr(_Key_7+24)
MOVT	R0, #hi_addr(_Key_7+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,879 :: 		Key_7.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_7+28)
MOVT	R0, #hi_addr(_Key_7+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,880 :: 		Key_7.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_7+29)
MOVT	R0, #hi_addr(_Key_7+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,881 :: 		Key_7.FontName        = Tahoma26x33_Regular;
MOVW	R2, #lo_addr(_Tahoma26x33_Regular+0)
MOVT	R2, #hi_addr(_Tahoma26x33_Regular+0)
MOVW	R0, #lo_addr(_Key_7+32)
MOVT	R0, #hi_addr(_Key_7+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,882 :: 		Key_7.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_7+49)
MOVT	R0, #hi_addr(_Key_7+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,883 :: 		Key_7.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_7+36)
MOVT	R0, #hi_addr(_Key_7+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,884 :: 		Key_7.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_7+38)
MOVT	R0, #hi_addr(_Key_7+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,885 :: 		Key_7.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_7+39)
MOVT	R0, #hi_addr(_Key_7+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,886 :: 		Key_7.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_7+40)
MOVT	R0, #hi_addr(_Key_7+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,887 :: 		Key_7.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_7+42)
MOVT	R0, #hi_addr(_Key_7+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,888 :: 		Key_7.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_7+44)
MOVT	R0, #hi_addr(_Key_7+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,889 :: 		Key_7.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_7+46)
MOVT	R0, #hi_addr(_Key_7+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,890 :: 		Key_7.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_7+50)
MOVT	R0, #hi_addr(_Key_7+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,891 :: 		Key_7.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_7+48)
MOVT	R0, #hi_addr(_Key_7+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,892 :: 		Key_7.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_7+52)
MOVT	R0, #hi_addr(_Key_7+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,893 :: 		Key_7.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_7+56)
MOVT	R0, #hi_addr(_Key_7+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,894 :: 		Key_7.OnClickPtr      = Key_7Click;
MOVW	R1, #lo_addr(_Key_7Click+0)
MOVT	R1, #hi_addr(_Key_7Click+0)
MOVW	R0, #lo_addr(_Key_7+60)
MOVT	R0, #hi_addr(_Key_7+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,895 :: 		Key_7.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_7+64)
MOVT	R0, #hi_addr(_Key_7+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,897 :: 		Key_8.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_8+0)
MOVT	R0, #hi_addr(_Key_8+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,898 :: 		Key_8.Order           = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_8+4)
MOVT	R0, #hi_addr(_Key_8+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,899 :: 		Key_8.Left            = 81;
MOVS	R1, #81
MOVW	R0, #lo_addr(_Key_8+6)
MOVT	R0, #hi_addr(_Key_8+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,900 :: 		Key_8.Top             = 162;
MOVS	R1, #162
MOVW	R0, #lo_addr(_Key_8+8)
MOVT	R0, #hi_addr(_Key_8+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,901 :: 		Key_8.Width           = 80;
MOVS	R1, #80
MOVW	R0, #lo_addr(_Key_8+10)
MOVT	R0, #hi_addr(_Key_8+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,902 :: 		Key_8.Height          = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Key_8+12)
MOVT	R0, #hi_addr(_Key_8+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,903 :: 		Key_8.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_8+14)
MOVT	R0, #hi_addr(_Key_8+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,904 :: 		Key_8.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_8+16)
MOVT	R0, #hi_addr(_Key_8+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,905 :: 		Key_8.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_8+18)
MOVT	R0, #hi_addr(_Key_8+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,906 :: 		Key_8.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_8+19)
MOVT	R0, #hi_addr(_Key_8+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,907 :: 		Key_8.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_8+20)
MOVT	R0, #hi_addr(_Key_8+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,908 :: 		Key_8.Caption         = Key_8_Caption;
MOVW	R1, #lo_addr(_Key_8_Caption+0)
MOVT	R1, #hi_addr(_Key_8_Caption+0)
MOVW	R0, #lo_addr(_Key_8+24)
MOVT	R0, #hi_addr(_Key_8+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,909 :: 		Key_8.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_8+28)
MOVT	R0, #hi_addr(_Key_8+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,910 :: 		Key_8.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_8+29)
MOVT	R0, #hi_addr(_Key_8+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,911 :: 		Key_8.FontName        = Tahoma26x33_Regular;
MOVW	R0, #lo_addr(_Key_8+32)
MOVT	R0, #hi_addr(_Key_8+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,912 :: 		Key_8.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_8+49)
MOVT	R0, #hi_addr(_Key_8+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,913 :: 		Key_8.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_8+36)
MOVT	R0, #hi_addr(_Key_8+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,914 :: 		Key_8.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_8+38)
MOVT	R0, #hi_addr(_Key_8+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,915 :: 		Key_8.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_8+39)
MOVT	R0, #hi_addr(_Key_8+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,916 :: 		Key_8.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_8+40)
MOVT	R0, #hi_addr(_Key_8+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,917 :: 		Key_8.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_8+42)
MOVT	R0, #hi_addr(_Key_8+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,918 :: 		Key_8.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_8+44)
MOVT	R0, #hi_addr(_Key_8+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,919 :: 		Key_8.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_8+46)
MOVT	R0, #hi_addr(_Key_8+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,920 :: 		Key_8.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_8+50)
MOVT	R0, #hi_addr(_Key_8+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,921 :: 		Key_8.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_8+48)
MOVT	R0, #hi_addr(_Key_8+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,922 :: 		Key_8.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_8+52)
MOVT	R0, #hi_addr(_Key_8+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,923 :: 		Key_8.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_8+56)
MOVT	R0, #hi_addr(_Key_8+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,924 :: 		Key_8.OnClickPtr      = Key_8Click;
MOVW	R1, #lo_addr(_Key_8Click+0)
MOVT	R1, #hi_addr(_Key_8Click+0)
MOVW	R0, #lo_addr(_Key_8+60)
MOVT	R0, #hi_addr(_Key_8+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,925 :: 		Key_8.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_8+64)
MOVT	R0, #hi_addr(_Key_8+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,927 :: 		Key_9.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_9+0)
MOVT	R0, #hi_addr(_Key_9+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,928 :: 		Key_9.Order           = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_Key_9+4)
MOVT	R0, #hi_addr(_Key_9+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,929 :: 		Key_9.Left            = 161;
MOVS	R1, #161
MOVW	R0, #lo_addr(_Key_9+6)
MOVT	R0, #hi_addr(_Key_9+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,930 :: 		Key_9.Top             = 162;
MOVS	R1, #162
MOVW	R0, #lo_addr(_Key_9+8)
MOVT	R0, #hi_addr(_Key_9+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,931 :: 		Key_9.Width           = 80;
MOVS	R1, #80
MOVW	R0, #lo_addr(_Key_9+10)
MOVT	R0, #hi_addr(_Key_9+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,932 :: 		Key_9.Height          = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Key_9+12)
MOVT	R0, #hi_addr(_Key_9+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,933 :: 		Key_9.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_9+14)
MOVT	R0, #hi_addr(_Key_9+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,934 :: 		Key_9.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_9+16)
MOVT	R0, #hi_addr(_Key_9+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,935 :: 		Key_9.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_9+18)
MOVT	R0, #hi_addr(_Key_9+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,936 :: 		Key_9.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_9+19)
MOVT	R0, #hi_addr(_Key_9+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,937 :: 		Key_9.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_9+20)
MOVT	R0, #hi_addr(_Key_9+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,938 :: 		Key_9.Caption         = Key_9_Caption;
MOVW	R1, #lo_addr(_Key_9_Caption+0)
MOVT	R1, #hi_addr(_Key_9_Caption+0)
MOVW	R0, #lo_addr(_Key_9+24)
MOVT	R0, #hi_addr(_Key_9+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,939 :: 		Key_9.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_9+28)
MOVT	R0, #hi_addr(_Key_9+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,940 :: 		Key_9.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_9+29)
MOVT	R0, #hi_addr(_Key_9+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,941 :: 		Key_9.FontName        = Tahoma26x33_Regular;
MOVW	R0, #lo_addr(_Key_9+32)
MOVT	R0, #hi_addr(_Key_9+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,942 :: 		Key_9.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_9+49)
MOVT	R0, #hi_addr(_Key_9+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,943 :: 		Key_9.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_9+36)
MOVT	R0, #hi_addr(_Key_9+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,944 :: 		Key_9.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_9+38)
MOVT	R0, #hi_addr(_Key_9+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,945 :: 		Key_9.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_9+39)
MOVT	R0, #hi_addr(_Key_9+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,946 :: 		Key_9.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_9+40)
MOVT	R0, #hi_addr(_Key_9+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,947 :: 		Key_9.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_9+42)
MOVT	R0, #hi_addr(_Key_9+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,948 :: 		Key_9.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_9+44)
MOVT	R0, #hi_addr(_Key_9+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,949 :: 		Key_9.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_9+46)
MOVT	R0, #hi_addr(_Key_9+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,950 :: 		Key_9.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_9+50)
MOVT	R0, #hi_addr(_Key_9+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,951 :: 		Key_9.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_9+48)
MOVT	R0, #hi_addr(_Key_9+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,952 :: 		Key_9.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_9+52)
MOVT	R0, #hi_addr(_Key_9+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,953 :: 		Key_9.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_9+56)
MOVT	R0, #hi_addr(_Key_9+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,954 :: 		Key_9.OnClickPtr      = Key_9Click;
MOVW	R1, #lo_addr(_Key_9Click+0)
MOVT	R1, #hi_addr(_Key_9Click+0)
MOVW	R0, #lo_addr(_Key_9+60)
MOVT	R0, #hi_addr(_Key_9+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,955 :: 		Key_9.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_9+64)
MOVT	R0, #hi_addr(_Key_9+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,957 :: 		Key_clear.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_clear+0)
MOVT	R0, #hi_addr(_Key_clear+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,958 :: 		Key_clear.Order           = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_clear+4)
MOVT	R0, #hi_addr(_Key_clear+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,959 :: 		Key_clear.Left            = 243;
MOVS	R1, #243
MOVW	R0, #lo_addr(_Key_clear+6)
MOVT	R0, #hi_addr(_Key_clear+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,960 :: 		Key_clear.Top             = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_clear+8)
MOVT	R0, #hi_addr(_Key_clear+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,961 :: 		Key_clear.Width           = 78;
MOVS	R1, #78
MOVW	R0, #lo_addr(_Key_clear+10)
MOVT	R0, #hi_addr(_Key_clear+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,962 :: 		Key_clear.Height          = 52;
MOVS	R1, #52
MOVW	R0, #lo_addr(_Key_clear+12)
MOVT	R0, #hi_addr(_Key_clear+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,963 :: 		Key_clear.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_clear+14)
MOVT	R0, #hi_addr(_Key_clear+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,964 :: 		Key_clear.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_clear+16)
MOVT	R0, #hi_addr(_Key_clear+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,965 :: 		Key_clear.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_clear+18)
MOVT	R0, #hi_addr(_Key_clear+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,966 :: 		Key_clear.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_clear+19)
MOVT	R0, #hi_addr(_Key_clear+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,967 :: 		Key_clear.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_clear+20)
MOVT	R0, #hi_addr(_Key_clear+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,968 :: 		Key_clear.Caption         = Key_clear_Caption;
MOVW	R1, #lo_addr(_Key_clear_Caption+0)
MOVT	R1, #hi_addr(_Key_clear_Caption+0)
MOVW	R0, #lo_addr(_Key_clear+24)
MOVT	R0, #hi_addr(_Key_clear+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,969 :: 		Key_clear.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_clear+28)
MOVT	R0, #hi_addr(_Key_clear+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,970 :: 		Key_clear.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_clear+29)
MOVT	R0, #hi_addr(_Key_clear+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,971 :: 		Key_clear.FontName        = Tahoma44x45_Bold;
MOVW	R1, #lo_addr(_Tahoma44x45_Bold+0)
MOVT	R1, #hi_addr(_Tahoma44x45_Bold+0)
MOVW	R0, #lo_addr(_Key_clear+32)
MOVT	R0, #hi_addr(_Key_clear+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,972 :: 		Key_clear.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_clear+49)
MOVT	R0, #hi_addr(_Key_clear+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,973 :: 		Key_clear.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Key_clear+36)
MOVT	R0, #hi_addr(_Key_clear+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,974 :: 		Key_clear.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_clear+38)
MOVT	R0, #hi_addr(_Key_clear+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,975 :: 		Key_clear.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_clear+39)
MOVT	R0, #hi_addr(_Key_clear+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,976 :: 		Key_clear.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_clear+40)
MOVT	R0, #hi_addr(_Key_clear+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,977 :: 		Key_clear.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_clear+42)
MOVT	R0, #hi_addr(_Key_clear+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,978 :: 		Key_clear.Gradient_End_Color = 0xF800;
MOVW	R1, #63488
MOVW	R0, #lo_addr(_Key_clear+44)
MOVT	R0, #hi_addr(_Key_clear+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,979 :: 		Key_clear.Color           = 0xF800;
MOVW	R1, #63488
MOVW	R0, #lo_addr(_Key_clear+46)
MOVT	R0, #hi_addr(_Key_clear+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,980 :: 		Key_clear.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_clear+50)
MOVT	R0, #hi_addr(_Key_clear+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,981 :: 		Key_clear.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_clear+48)
MOVT	R0, #hi_addr(_Key_clear+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,982 :: 		Key_clear.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_clear+52)
MOVT	R0, #hi_addr(_Key_clear+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,983 :: 		Key_clear.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_clear+56)
MOVT	R0, #hi_addr(_Key_clear+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,984 :: 		Key_clear.OnClickPtr      = Key_clearClick;
MOVW	R1, #lo_addr(_Key_clearClick+0)
MOVT	R1, #hi_addr(_Key_clearClick+0)
MOVW	R0, #lo_addr(_Key_clear+60)
MOVT	R0, #hi_addr(_Key_clear+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,985 :: 		Key_clear.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_clear+64)
MOVT	R0, #hi_addr(_Key_clear+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,987 :: 		Key_4.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_4+0)
MOVT	R0, #hi_addr(_Key_4+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,988 :: 		Key_4.Order           = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_Key_4+4)
MOVT	R0, #hi_addr(_Key_4+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,989 :: 		Key_4.Left            = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_4+6)
MOVT	R0, #hi_addr(_Key_4+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,990 :: 		Key_4.Top             = 123;
MOVS	R1, #123
MOVW	R0, #lo_addr(_Key_4+8)
MOVT	R0, #hi_addr(_Key_4+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,991 :: 		Key_4.Width           = 80;
MOVS	R1, #80
MOVW	R0, #lo_addr(_Key_4+10)
MOVT	R0, #hi_addr(_Key_4+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,992 :: 		Key_4.Height          = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Key_4+12)
MOVT	R0, #hi_addr(_Key_4+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,993 :: 		Key_4.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_4+14)
MOVT	R0, #hi_addr(_Key_4+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,994 :: 		Key_4.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_4+16)
MOVT	R0, #hi_addr(_Key_4+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,995 :: 		Key_4.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_4+18)
MOVT	R0, #hi_addr(_Key_4+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,996 :: 		Key_4.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_4+19)
MOVT	R0, #hi_addr(_Key_4+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,997 :: 		Key_4.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_4+20)
MOVT	R0, #hi_addr(_Key_4+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,998 :: 		Key_4.Caption         = Key_4_Caption;
MOVW	R1, #lo_addr(_Key_4_Caption+0)
MOVT	R1, #hi_addr(_Key_4_Caption+0)
MOVW	R0, #lo_addr(_Key_4+24)
MOVT	R0, #hi_addr(_Key_4+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,999 :: 		Key_4.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_4+28)
MOVT	R0, #hi_addr(_Key_4+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1000 :: 		Key_4.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_4+29)
MOVT	R0, #hi_addr(_Key_4+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1001 :: 		Key_4.FontName        = Tahoma26x33_Regular;
MOVW	R0, #lo_addr(_Key_4+32)
MOVT	R0, #hi_addr(_Key_4+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,1002 :: 		Key_4.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_4+49)
MOVT	R0, #hi_addr(_Key_4+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1003 :: 		Key_4.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_4+36)
MOVT	R0, #hi_addr(_Key_4+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1004 :: 		Key_4.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_4+38)
MOVT	R0, #hi_addr(_Key_4+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1005 :: 		Key_4.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_4+39)
MOVT	R0, #hi_addr(_Key_4+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1006 :: 		Key_4.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_4+40)
MOVT	R0, #hi_addr(_Key_4+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1007 :: 		Key_4.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_4+42)
MOVT	R0, #hi_addr(_Key_4+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1008 :: 		Key_4.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_4+44)
MOVT	R0, #hi_addr(_Key_4+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1009 :: 		Key_4.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_4+46)
MOVT	R0, #hi_addr(_Key_4+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1010 :: 		Key_4.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_4+50)
MOVT	R0, #hi_addr(_Key_4+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1011 :: 		Key_4.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_4+48)
MOVT	R0, #hi_addr(_Key_4+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1012 :: 		Key_4.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_4+52)
MOVT	R0, #hi_addr(_Key_4+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1013 :: 		Key_4.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_4+56)
MOVT	R0, #hi_addr(_Key_4+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1014 :: 		Key_4.OnClickPtr      = Key_4Click;
MOVW	R1, #lo_addr(_Key_4Click+0)
MOVT	R1, #hi_addr(_Key_4Click+0)
MOVW	R0, #lo_addr(_Key_4+60)
MOVT	R0, #hi_addr(_Key_4+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1015 :: 		Key_4.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_4+64)
MOVT	R0, #hi_addr(_Key_4+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1017 :: 		Key_5.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_5+0)
MOVT	R0, #hi_addr(_Key_5+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1018 :: 		Key_5.Order           = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_Key_5+4)
MOVT	R0, #hi_addr(_Key_5+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1019 :: 		Key_5.Left            = 81;
MOVS	R1, #81
MOVW	R0, #lo_addr(_Key_5+6)
MOVT	R0, #hi_addr(_Key_5+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1020 :: 		Key_5.Top             = 123;
MOVS	R1, #123
MOVW	R0, #lo_addr(_Key_5+8)
MOVT	R0, #hi_addr(_Key_5+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1021 :: 		Key_5.Width           = 80;
MOVS	R1, #80
MOVW	R0, #lo_addr(_Key_5+10)
MOVT	R0, #hi_addr(_Key_5+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1022 :: 		Key_5.Height          = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Key_5+12)
MOVT	R0, #hi_addr(_Key_5+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1023 :: 		Key_5.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_5+14)
MOVT	R0, #hi_addr(_Key_5+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1024 :: 		Key_5.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_5+16)
MOVT	R0, #hi_addr(_Key_5+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1025 :: 		Key_5.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_5+18)
MOVT	R0, #hi_addr(_Key_5+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1026 :: 		Key_5.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_5+19)
MOVT	R0, #hi_addr(_Key_5+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1027 :: 		Key_5.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_5+20)
MOVT	R0, #hi_addr(_Key_5+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1028 :: 		Key_5.Caption         = Key_5_Caption;
MOVW	R1, #lo_addr(_Key_5_Caption+0)
MOVT	R1, #hi_addr(_Key_5_Caption+0)
MOVW	R0, #lo_addr(_Key_5+24)
MOVT	R0, #hi_addr(_Key_5+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1029 :: 		Key_5.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_5+28)
MOVT	R0, #hi_addr(_Key_5+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1030 :: 		Key_5.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_5+29)
MOVT	R0, #hi_addr(_Key_5+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1031 :: 		Key_5.FontName        = Tahoma26x33_Regular;
MOVW	R0, #lo_addr(_Key_5+32)
MOVT	R0, #hi_addr(_Key_5+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,1032 :: 		Key_5.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_5+49)
MOVT	R0, #hi_addr(_Key_5+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1033 :: 		Key_5.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_5+36)
MOVT	R0, #hi_addr(_Key_5+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1034 :: 		Key_5.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_5+38)
MOVT	R0, #hi_addr(_Key_5+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1035 :: 		Key_5.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_5+39)
MOVT	R0, #hi_addr(_Key_5+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1036 :: 		Key_5.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_5+40)
MOVT	R0, #hi_addr(_Key_5+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1037 :: 		Key_5.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_5+42)
MOVT	R0, #hi_addr(_Key_5+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1038 :: 		Key_5.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_5+44)
MOVT	R0, #hi_addr(_Key_5+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1039 :: 		Key_5.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_5+46)
MOVT	R0, #hi_addr(_Key_5+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1040 :: 		Key_5.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_5+50)
MOVT	R0, #hi_addr(_Key_5+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1041 :: 		Key_5.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_5+48)
MOVT	R0, #hi_addr(_Key_5+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1042 :: 		Key_5.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_5+52)
MOVT	R0, #hi_addr(_Key_5+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1043 :: 		Key_5.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_5+56)
MOVT	R0, #hi_addr(_Key_5+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1044 :: 		Key_5.OnClickPtr      = Key_5Click;
MOVW	R1, #lo_addr(_Key_5Click+0)
MOVT	R1, #hi_addr(_Key_5Click+0)
MOVW	R0, #lo_addr(_Key_5+60)
MOVT	R0, #hi_addr(_Key_5+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1045 :: 		Key_5.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_5+64)
MOVT	R0, #hi_addr(_Key_5+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1047 :: 		Key_6.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_6+0)
MOVT	R0, #hi_addr(_Key_6+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1048 :: 		Key_6.Order           = 6;
MOVS	R1, #6
MOVW	R0, #lo_addr(_Key_6+4)
MOVT	R0, #hi_addr(_Key_6+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1049 :: 		Key_6.Left            = 161;
MOVS	R1, #161
MOVW	R0, #lo_addr(_Key_6+6)
MOVT	R0, #hi_addr(_Key_6+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1050 :: 		Key_6.Top             = 123;
MOVS	R1, #123
MOVW	R0, #lo_addr(_Key_6+8)
MOVT	R0, #hi_addr(_Key_6+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1051 :: 		Key_6.Width           = 80;
MOVS	R1, #80
MOVW	R0, #lo_addr(_Key_6+10)
MOVT	R0, #hi_addr(_Key_6+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1052 :: 		Key_6.Height          = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Key_6+12)
MOVT	R0, #hi_addr(_Key_6+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1053 :: 		Key_6.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_6+14)
MOVT	R0, #hi_addr(_Key_6+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1054 :: 		Key_6.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_6+16)
MOVT	R0, #hi_addr(_Key_6+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1055 :: 		Key_6.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_6+18)
MOVT	R0, #hi_addr(_Key_6+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1056 :: 		Key_6.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_6+19)
MOVT	R0, #hi_addr(_Key_6+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1057 :: 		Key_6.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_6+20)
MOVT	R0, #hi_addr(_Key_6+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1058 :: 		Key_6.Caption         = Key_6_Caption;
MOVW	R1, #lo_addr(_Key_6_Caption+0)
MOVT	R1, #hi_addr(_Key_6_Caption+0)
MOVW	R0, #lo_addr(_Key_6+24)
MOVT	R0, #hi_addr(_Key_6+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1059 :: 		Key_6.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_6+28)
MOVT	R0, #hi_addr(_Key_6+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1060 :: 		Key_6.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_6+29)
MOVT	R0, #hi_addr(_Key_6+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1061 :: 		Key_6.FontName        = Tahoma26x33_Regular;
MOVW	R0, #lo_addr(_Key_6+32)
MOVT	R0, #hi_addr(_Key_6+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,1062 :: 		Key_6.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_6+49)
MOVT	R0, #hi_addr(_Key_6+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1063 :: 		Key_6.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_6+36)
MOVT	R0, #hi_addr(_Key_6+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1064 :: 		Key_6.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_6+38)
MOVT	R0, #hi_addr(_Key_6+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1065 :: 		Key_6.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_6+39)
MOVT	R0, #hi_addr(_Key_6+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1066 :: 		Key_6.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_6+40)
MOVT	R0, #hi_addr(_Key_6+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1067 :: 		Key_6.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_6+42)
MOVT	R0, #hi_addr(_Key_6+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1068 :: 		Key_6.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_6+44)
MOVT	R0, #hi_addr(_Key_6+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1069 :: 		Key_6.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_6+46)
MOVT	R0, #hi_addr(_Key_6+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1070 :: 		Key_6.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_6+50)
MOVT	R0, #hi_addr(_Key_6+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1071 :: 		Key_6.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_6+48)
MOVT	R0, #hi_addr(_Key_6+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1072 :: 		Key_6.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_6+52)
MOVT	R0, #hi_addr(_Key_6+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1073 :: 		Key_6.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_6+56)
MOVT	R0, #hi_addr(_Key_6+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1074 :: 		Key_6.OnClickPtr      = Key_6Click;
MOVW	R1, #lo_addr(_Key_6Click+0)
MOVT	R1, #hi_addr(_Key_6Click+0)
MOVW	R0, #lo_addr(_Key_6+60)
MOVT	R0, #hi_addr(_Key_6+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1075 :: 		Key_6.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_6+64)
MOVT	R0, #hi_addr(_Key_6+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1077 :: 		Key_1.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_1+0)
MOVT	R0, #hi_addr(_Key_1+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1078 :: 		Key_1.Order           = 7;
MOVS	R1, #7
MOVW	R0, #lo_addr(_Key_1+4)
MOVT	R0, #hi_addr(_Key_1+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1079 :: 		Key_1.Left            = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_Key_1+6)
MOVT	R0, #hi_addr(_Key_1+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1080 :: 		Key_1.Top             = 84;
MOVS	R1, #84
MOVW	R0, #lo_addr(_Key_1+8)
MOVT	R0, #hi_addr(_Key_1+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1081 :: 		Key_1.Width           = 80;
MOVS	R1, #80
MOVW	R0, #lo_addr(_Key_1+10)
MOVT	R0, #hi_addr(_Key_1+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1082 :: 		Key_1.Height          = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Key_1+12)
MOVT	R0, #hi_addr(_Key_1+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1083 :: 		Key_1.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_1+14)
MOVT	R0, #hi_addr(_Key_1+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1084 :: 		Key_1.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_1+16)
MOVT	R0, #hi_addr(_Key_1+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1085 :: 		Key_1.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_1+18)
MOVT	R0, #hi_addr(_Key_1+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1086 :: 		Key_1.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_1+19)
MOVT	R0, #hi_addr(_Key_1+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1087 :: 		Key_1.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_1+20)
MOVT	R0, #hi_addr(_Key_1+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1088 :: 		Key_1.Caption         = Key_1_Caption;
MOVW	R1, #lo_addr(_Key_1_Caption+0)
MOVT	R1, #hi_addr(_Key_1_Caption+0)
MOVW	R0, #lo_addr(_Key_1+24)
MOVT	R0, #hi_addr(_Key_1+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1089 :: 		Key_1.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_1+28)
MOVT	R0, #hi_addr(_Key_1+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1090 :: 		Key_1.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_1+29)
MOVT	R0, #hi_addr(_Key_1+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1091 :: 		Key_1.FontName        = Tahoma26x33_Regular;
MOVW	R0, #lo_addr(_Key_1+32)
MOVT	R0, #hi_addr(_Key_1+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,1092 :: 		Key_1.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_1+49)
MOVT	R0, #hi_addr(_Key_1+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1093 :: 		Key_1.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_1+36)
MOVT	R0, #hi_addr(_Key_1+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1094 :: 		Key_1.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_1+38)
MOVT	R0, #hi_addr(_Key_1+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1095 :: 		Key_1.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_1+39)
MOVT	R0, #hi_addr(_Key_1+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1096 :: 		Key_1.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_1+40)
MOVT	R0, #hi_addr(_Key_1+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1097 :: 		Key_1.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_1+42)
MOVT	R0, #hi_addr(_Key_1+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1098 :: 		Key_1.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_1+44)
MOVT	R0, #hi_addr(_Key_1+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1099 :: 		Key_1.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_1+46)
MOVT	R0, #hi_addr(_Key_1+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1100 :: 		Key_1.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_1+50)
MOVT	R0, #hi_addr(_Key_1+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1101 :: 		Key_1.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_1+48)
MOVT	R0, #hi_addr(_Key_1+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1102 :: 		Key_1.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_1+52)
MOVT	R0, #hi_addr(_Key_1+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1103 :: 		Key_1.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_1+56)
MOVT	R0, #hi_addr(_Key_1+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1104 :: 		Key_1.OnClickPtr      = Key_1click;
MOVW	R1, #lo_addr(_Key_1click+0)
MOVT	R1, #hi_addr(_Key_1click+0)
MOVW	R0, #lo_addr(_Key_1+60)
MOVT	R0, #hi_addr(_Key_1+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1105 :: 		Key_1.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_1+64)
MOVT	R0, #hi_addr(_Key_1+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1107 :: 		Key_2.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_2+0)
MOVT	R0, #hi_addr(_Key_2+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1108 :: 		Key_2.Order           = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_Key_2+4)
MOVT	R0, #hi_addr(_Key_2+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1109 :: 		Key_2.Left            = 81;
MOVS	R1, #81
MOVW	R0, #lo_addr(_Key_2+6)
MOVT	R0, #hi_addr(_Key_2+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1110 :: 		Key_2.Top             = 84;
MOVS	R1, #84
MOVW	R0, #lo_addr(_Key_2+8)
MOVT	R0, #hi_addr(_Key_2+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1111 :: 		Key_2.Width           = 80;
MOVS	R1, #80
MOVW	R0, #lo_addr(_Key_2+10)
MOVT	R0, #hi_addr(_Key_2+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1112 :: 		Key_2.Height          = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Key_2+12)
MOVT	R0, #hi_addr(_Key_2+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1113 :: 		Key_2.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_2+14)
MOVT	R0, #hi_addr(_Key_2+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1114 :: 		Key_2.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_2+16)
MOVT	R0, #hi_addr(_Key_2+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1115 :: 		Key_2.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_2+18)
MOVT	R0, #hi_addr(_Key_2+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1116 :: 		Key_2.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_2+19)
MOVT	R0, #hi_addr(_Key_2+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1117 :: 		Key_2.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_2+20)
MOVT	R0, #hi_addr(_Key_2+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1118 :: 		Key_2.Caption         = Key_2_Caption;
MOVW	R1, #lo_addr(_Key_2_Caption+0)
MOVT	R1, #hi_addr(_Key_2_Caption+0)
MOVW	R0, #lo_addr(_Key_2+24)
MOVT	R0, #hi_addr(_Key_2+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1119 :: 		Key_2.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_2+28)
MOVT	R0, #hi_addr(_Key_2+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1120 :: 		Key_2.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_2+29)
MOVT	R0, #hi_addr(_Key_2+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1121 :: 		Key_2.FontName        = Tahoma26x33_Regular;
MOVW	R0, #lo_addr(_Key_2+32)
MOVT	R0, #hi_addr(_Key_2+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,1122 :: 		Key_2.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_2+49)
MOVT	R0, #hi_addr(_Key_2+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1123 :: 		Key_2.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_2+36)
MOVT	R0, #hi_addr(_Key_2+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1124 :: 		Key_2.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_2+38)
MOVT	R0, #hi_addr(_Key_2+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1125 :: 		Key_2.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_2+39)
MOVT	R0, #hi_addr(_Key_2+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1126 :: 		Key_2.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_2+40)
MOVT	R0, #hi_addr(_Key_2+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1127 :: 		Key_2.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_2+42)
MOVT	R0, #hi_addr(_Key_2+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1128 :: 		Key_2.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_2+44)
MOVT	R0, #hi_addr(_Key_2+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1129 :: 		Key_2.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_2+46)
MOVT	R0, #hi_addr(_Key_2+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1130 :: 		Key_2.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_2+50)
MOVT	R0, #hi_addr(_Key_2+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1131 :: 		Key_2.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_2+48)
MOVT	R0, #hi_addr(_Key_2+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1132 :: 		Key_2.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_2+52)
MOVT	R0, #hi_addr(_Key_2+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1133 :: 		Key_2.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_2+56)
MOVT	R0, #hi_addr(_Key_2+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1134 :: 		Key_2.OnClickPtr      = Key_2Click;
MOVW	R1, #lo_addr(_Key_2Click+0)
MOVT	R1, #hi_addr(_Key_2Click+0)
MOVW	R0, #lo_addr(_Key_2+60)
MOVT	R0, #hi_addr(_Key_2+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1135 :: 		Key_2.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_2+64)
MOVT	R0, #hi_addr(_Key_2+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1137 :: 		Key_3.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_3+0)
MOVT	R0, #hi_addr(_Key_3+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1138 :: 		Key_3.Order           = 9;
MOVS	R1, #9
MOVW	R0, #lo_addr(_Key_3+4)
MOVT	R0, #hi_addr(_Key_3+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1139 :: 		Key_3.Left            = 161;
MOVS	R1, #161
MOVW	R0, #lo_addr(_Key_3+6)
MOVT	R0, #hi_addr(_Key_3+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1140 :: 		Key_3.Top             = 84;
MOVS	R1, #84
MOVW	R0, #lo_addr(_Key_3+8)
MOVT	R0, #hi_addr(_Key_3+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1141 :: 		Key_3.Width           = 80;
MOVS	R1, #80
MOVW	R0, #lo_addr(_Key_3+10)
MOVT	R0, #hi_addr(_Key_3+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1142 :: 		Key_3.Height          = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Key_3+12)
MOVT	R0, #hi_addr(_Key_3+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1143 :: 		Key_3.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_3+14)
MOVT	R0, #hi_addr(_Key_3+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1144 :: 		Key_3.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_3+16)
MOVT	R0, #hi_addr(_Key_3+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1145 :: 		Key_3.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_3+18)
MOVT	R0, #hi_addr(_Key_3+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1146 :: 		Key_3.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_3+19)
MOVT	R0, #hi_addr(_Key_3+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1147 :: 		Key_3.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_3+20)
MOVT	R0, #hi_addr(_Key_3+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1148 :: 		Key_3.Caption         = Key_3_Caption;
MOVW	R1, #lo_addr(_Key_3_Caption+0)
MOVT	R1, #hi_addr(_Key_3_Caption+0)
MOVW	R0, #lo_addr(_Key_3+24)
MOVT	R0, #hi_addr(_Key_3+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1149 :: 		Key_3.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_3+28)
MOVT	R0, #hi_addr(_Key_3+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1150 :: 		Key_3.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_3+29)
MOVT	R0, #hi_addr(_Key_3+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1151 :: 		Key_3.FontName        = Tahoma26x33_Regular;
MOVW	R0, #lo_addr(_Key_3+32)
MOVT	R0, #hi_addr(_Key_3+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,1152 :: 		Key_3.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_3+49)
MOVT	R0, #hi_addr(_Key_3+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1153 :: 		Key_3.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_3+36)
MOVT	R0, #hi_addr(_Key_3+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1154 :: 		Key_3.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_3+38)
MOVT	R0, #hi_addr(_Key_3+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1155 :: 		Key_3.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_3+39)
MOVT	R0, #hi_addr(_Key_3+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1156 :: 		Key_3.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_3+40)
MOVT	R0, #hi_addr(_Key_3+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1157 :: 		Key_3.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_3+42)
MOVT	R0, #hi_addr(_Key_3+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1158 :: 		Key_3.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_3+44)
MOVT	R0, #hi_addr(_Key_3+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1159 :: 		Key_3.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_3+46)
MOVT	R0, #hi_addr(_Key_3+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1160 :: 		Key_3.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_3+50)
MOVT	R0, #hi_addr(_Key_3+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1161 :: 		Key_3.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_3+48)
MOVT	R0, #hi_addr(_Key_3+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1162 :: 		Key_3.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_3+52)
MOVT	R0, #hi_addr(_Key_3+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1163 :: 		Key_3.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_3+56)
MOVT	R0, #hi_addr(_Key_3+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1164 :: 		Key_3.OnClickPtr      = Key_3Click;
MOVW	R1, #lo_addr(_Key_3Click+0)
MOVT	R1, #hi_addr(_Key_3Click+0)
MOVW	R0, #lo_addr(_Key_3+60)
MOVT	R0, #hi_addr(_Key_3+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1165 :: 		Key_3.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_3+64)
MOVT	R0, #hi_addr(_Key_3+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1167 :: 		Key_dp.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_dp+0)
MOVT	R0, #hi_addr(_Key_dp+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1168 :: 		Key_dp.Order           = 10;
MOVS	R1, #10
MOVW	R0, #lo_addr(_Key_dp+4)
MOVT	R0, #hi_addr(_Key_dp+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1169 :: 		Key_dp.Left            = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_dp+6)
MOVT	R0, #hi_addr(_Key_dp+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1170 :: 		Key_dp.Top             = 201;
MOVS	R1, #201
MOVW	R0, #lo_addr(_Key_dp+8)
MOVT	R0, #hi_addr(_Key_dp+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1171 :: 		Key_dp.Width           = 80;
MOVS	R1, #80
MOVW	R0, #lo_addr(_Key_dp+10)
MOVT	R0, #hi_addr(_Key_dp+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1172 :: 		Key_dp.Height          = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Key_dp+12)
MOVT	R0, #hi_addr(_Key_dp+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1173 :: 		Key_dp.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_dp+14)
MOVT	R0, #hi_addr(_Key_dp+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1174 :: 		Key_dp.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_dp+16)
MOVT	R0, #hi_addr(_Key_dp+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1175 :: 		Key_dp.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_dp+18)
MOVT	R0, #hi_addr(_Key_dp+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1176 :: 		Key_dp.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_dp+19)
MOVT	R0, #hi_addr(_Key_dp+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1177 :: 		Key_dp.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_dp+20)
MOVT	R0, #hi_addr(_Key_dp+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1178 :: 		Key_dp.Caption         = Key_dp_Caption;
MOVW	R1, #lo_addr(_Key_dp_Caption+0)
MOVT	R1, #hi_addr(_Key_dp_Caption+0)
MOVW	R0, #lo_addr(_Key_dp+24)
MOVT	R0, #hi_addr(_Key_dp+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1179 :: 		Key_dp.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_dp+28)
MOVT	R0, #hi_addr(_Key_dp+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1180 :: 		Key_dp.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_dp+29)
MOVT	R0, #hi_addr(_Key_dp+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1181 :: 		Key_dp.FontName        = Tahoma26x33_Regular;
MOVW	R0, #lo_addr(_Key_dp+32)
MOVT	R0, #hi_addr(_Key_dp+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,1182 :: 		Key_dp.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_dp+49)
MOVT	R0, #hi_addr(_Key_dp+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1183 :: 		Key_dp.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_dp+36)
MOVT	R0, #hi_addr(_Key_dp+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1184 :: 		Key_dp.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_dp+38)
MOVT	R0, #hi_addr(_Key_dp+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1185 :: 		Key_dp.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_dp+39)
MOVT	R0, #hi_addr(_Key_dp+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1186 :: 		Key_dp.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_dp+40)
MOVT	R0, #hi_addr(_Key_dp+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1187 :: 		Key_dp.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_dp+42)
MOVT	R0, #hi_addr(_Key_dp+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1188 :: 		Key_dp.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_dp+44)
MOVT	R0, #hi_addr(_Key_dp+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1189 :: 		Key_dp.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_dp+46)
MOVT	R0, #hi_addr(_Key_dp+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1190 :: 		Key_dp.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_dp+50)
MOVT	R0, #hi_addr(_Key_dp+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1191 :: 		Key_dp.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_dp+48)
MOVT	R0, #hi_addr(_Key_dp+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1192 :: 		Key_dp.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_dp+52)
MOVT	R0, #hi_addr(_Key_dp+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1193 :: 		Key_dp.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_dp+56)
MOVT	R0, #hi_addr(_Key_dp+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1194 :: 		Key_dp.OnClickPtr      = Key_dpClick;
MOVW	R1, #lo_addr(_Key_dpClick+0)
MOVT	R1, #hi_addr(_Key_dpClick+0)
MOVW	R0, #lo_addr(_Key_dp+60)
MOVT	R0, #hi_addr(_Key_dp+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1195 :: 		Key_dp.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_dp+64)
MOVT	R0, #hi_addr(_Key_dp+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1197 :: 		Key_0.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_0+0)
MOVT	R0, #hi_addr(_Key_0+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1198 :: 		Key_0.Order           = 11;
MOVS	R1, #11
MOVW	R0, #lo_addr(_Key_0+4)
MOVT	R0, #hi_addr(_Key_0+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1199 :: 		Key_0.Left            = 81;
MOVS	R1, #81
MOVW	R0, #lo_addr(_Key_0+6)
MOVT	R0, #hi_addr(_Key_0+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1200 :: 		Key_0.Top             = 201;
MOVS	R1, #201
MOVW	R0, #lo_addr(_Key_0+8)
MOVT	R0, #hi_addr(_Key_0+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1201 :: 		Key_0.Width           = 80;
MOVS	R1, #80
MOVW	R0, #lo_addr(_Key_0+10)
MOVT	R0, #hi_addr(_Key_0+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1202 :: 		Key_0.Height          = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Key_0+12)
MOVT	R0, #hi_addr(_Key_0+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1203 :: 		Key_0.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_0+14)
MOVT	R0, #hi_addr(_Key_0+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1204 :: 		Key_0.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_0+16)
MOVT	R0, #hi_addr(_Key_0+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1205 :: 		Key_0.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_0+18)
MOVT	R0, #hi_addr(_Key_0+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1206 :: 		Key_0.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_0+19)
MOVT	R0, #hi_addr(_Key_0+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1207 :: 		Key_0.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_0+20)
MOVT	R0, #hi_addr(_Key_0+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1208 :: 		Key_0.Caption         = Key_0_Caption;
MOVW	R1, #lo_addr(_Key_0_Caption+0)
MOVT	R1, #hi_addr(_Key_0_Caption+0)
MOVW	R0, #lo_addr(_Key_0+24)
MOVT	R0, #hi_addr(_Key_0+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1209 :: 		Key_0.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_0+28)
MOVT	R0, #hi_addr(_Key_0+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1210 :: 		Key_0.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_0+29)
MOVT	R0, #hi_addr(_Key_0+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1211 :: 		Key_0.FontName        = Tahoma26x33_Regular;
MOVW	R0, #lo_addr(_Key_0+32)
MOVT	R0, #hi_addr(_Key_0+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,1212 :: 		Key_0.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_0+49)
MOVT	R0, #hi_addr(_Key_0+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1213 :: 		Key_0.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_0+36)
MOVT	R0, #hi_addr(_Key_0+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1214 :: 		Key_0.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_0+38)
MOVT	R0, #hi_addr(_Key_0+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1215 :: 		Key_0.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_0+39)
MOVT	R0, #hi_addr(_Key_0+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1216 :: 		Key_0.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_0+40)
MOVT	R0, #hi_addr(_Key_0+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1217 :: 		Key_0.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_0+42)
MOVT	R0, #hi_addr(_Key_0+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1218 :: 		Key_0.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_0+44)
MOVT	R0, #hi_addr(_Key_0+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1219 :: 		Key_0.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Key_0+46)
MOVT	R0, #hi_addr(_Key_0+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1220 :: 		Key_0.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_0+50)
MOVT	R0, #hi_addr(_Key_0+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1221 :: 		Key_0.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_0+48)
MOVT	R0, #hi_addr(_Key_0+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1222 :: 		Key_0.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_0+52)
MOVT	R0, #hi_addr(_Key_0+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1223 :: 		Key_0.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_0+56)
MOVT	R0, #hi_addr(_Key_0+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1224 :: 		Key_0.OnClickPtr      = Key_0Click;
MOVW	R1, #lo_addr(_Key_0Click+0)
MOVT	R1, #hi_addr(_Key_0Click+0)
MOVW	R0, #lo_addr(_Key_0+60)
MOVT	R0, #hi_addr(_Key_0+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1225 :: 		Key_0.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_0+64)
MOVT	R0, #hi_addr(_Key_0+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1227 :: 		Key_enter.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_Key_enter+0)
MOVT	R0, #hi_addr(_Key_enter+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1228 :: 		Key_enter.Order           = 12;
MOVS	R1, #12
MOVW	R0, #lo_addr(_Key_enter+4)
MOVT	R0, #hi_addr(_Key_enter+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1229 :: 		Key_enter.Left            = 243;
MOVS	R1, #243
MOVW	R0, #lo_addr(_Key_enter+6)
MOVT	R0, #hi_addr(_Key_enter+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1230 :: 		Key_enter.Top             = 85;
MOVS	R1, #85
MOVW	R0, #lo_addr(_Key_enter+8)
MOVT	R0, #hi_addr(_Key_enter+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1231 :: 		Key_enter.Width           = 79;
MOVS	R1, #79
MOVW	R0, #lo_addr(_Key_enter+10)
MOVT	R0, #hi_addr(_Key_enter+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1232 :: 		Key_enter.Height          = 154;
MOVS	R1, #154
MOVW	R0, #lo_addr(_Key_enter+12)
MOVT	R0, #hi_addr(_Key_enter+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1233 :: 		Key_enter.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_enter+14)
MOVT	R0, #hi_addr(_Key_enter+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1234 :: 		Key_enter.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_enter+16)
MOVT	R0, #hi_addr(_Key_enter+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1235 :: 		Key_enter.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_enter+18)
MOVT	R0, #hi_addr(_Key_enter+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1236 :: 		Key_enter.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_enter+19)
MOVT	R0, #hi_addr(_Key_enter+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1237 :: 		Key_enter.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_enter+20)
MOVT	R0, #hi_addr(_Key_enter+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1238 :: 		Key_enter.Caption         = Key_enter_Caption;
MOVW	R1, #lo_addr(_Key_enter_Caption+0)
MOVT	R1, #hi_addr(_Key_enter_Caption+0)
MOVW	R0, #lo_addr(_Key_enter+24)
MOVT	R0, #hi_addr(_Key_enter+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1239 :: 		Key_enter.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_enter+28)
MOVT	R0, #hi_addr(_Key_enter+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1240 :: 		Key_enter.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_enter+29)
MOVT	R0, #hi_addr(_Key_enter+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1241 :: 		Key_enter.FontName        = Tahoma29x29_Bold;
MOVW	R1, #lo_addr(_Tahoma29x29_Bold+0)
MOVT	R1, #hi_addr(_Tahoma29x29_Bold+0)
MOVW	R0, #lo_addr(_Key_enter+32)
MOVT	R0, #hi_addr(_Key_enter+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1242 :: 		Key_enter.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Key_enter+49)
MOVT	R0, #hi_addr(_Key_enter+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1243 :: 		Key_enter.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_enter+36)
MOVT	R0, #hi_addr(_Key_enter+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1244 :: 		Key_enter.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_enter+38)
MOVT	R0, #hi_addr(_Key_enter+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1245 :: 		Key_enter.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_enter+39)
MOVT	R0, #hi_addr(_Key_enter+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1246 :: 		Key_enter.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_enter+40)
MOVT	R0, #hi_addr(_Key_enter+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1247 :: 		Key_enter.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_Key_enter+42)
MOVT	R0, #hi_addr(_Key_enter+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1248 :: 		Key_enter.Gradient_End_Color = 0x0400;
MOVW	R1, #1024
MOVW	R0, #lo_addr(_Key_enter+44)
MOVT	R0, #hi_addr(_Key_enter+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1249 :: 		Key_enter.Color           = 0x0400;
MOVW	R1, #1024
MOVW	R0, #lo_addr(_Key_enter+46)
MOVT	R0, #hi_addr(_Key_enter+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1250 :: 		Key_enter.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Key_enter+50)
MOVT	R0, #hi_addr(_Key_enter+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1251 :: 		Key_enter.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Key_enter+48)
MOVT	R0, #hi_addr(_Key_enter+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1252 :: 		Key_enter.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_enter+52)
MOVT	R0, #hi_addr(_Key_enter+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1253 :: 		Key_enter.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_enter+56)
MOVT	R0, #hi_addr(_Key_enter+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1254 :: 		Key_enter.OnClickPtr      = Key_enterClick;
MOVW	R1, #lo_addr(_Key_enterClick+0)
MOVT	R1, #hi_addr(_Key_enterClick+0)
MOVW	R0, #lo_addr(_Key_enter+60)
MOVT	R0, #hi_addr(_Key_enter+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1255 :: 		Key_enter.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Key_enter+64)
MOVT	R0, #hi_addr(_Key_enter+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1257 :: 		kp_display.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_kp_display+0)
MOVT	R0, #hi_addr(_kp_display+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1258 :: 		kp_display.Order           = 14;
MOVS	R1, #14
MOVW	R0, #lo_addr(_kp_display+4)
MOVT	R0, #hi_addr(_kp_display+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1259 :: 		kp_display.Left            = 81;
MOVS	R1, #81
MOVW	R0, #lo_addr(_kp_display+6)
MOVT	R0, #hi_addr(_kp_display+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1260 :: 		kp_display.Top             = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_kp_display+8)
MOVT	R0, #hi_addr(_kp_display+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1261 :: 		kp_display.Width           = 160;
MOVS	R1, #160
MOVW	R0, #lo_addr(_kp_display+10)
MOVT	R0, #hi_addr(_kp_display+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1262 :: 		kp_display.Height          = 52;
MOVS	R1, #52
MOVW	R0, #lo_addr(_kp_display+12)
MOVT	R0, #hi_addr(_kp_display+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1263 :: 		kp_display.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_kp_display+14)
MOVT	R0, #hi_addr(_kp_display+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1264 :: 		kp_display.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_kp_display+16)
MOVT	R0, #hi_addr(_kp_display+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1265 :: 		kp_display.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_kp_display+18)
MOVT	R0, #hi_addr(_kp_display+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1266 :: 		kp_display.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_kp_display+19)
MOVT	R0, #hi_addr(_kp_display+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1267 :: 		kp_display.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_kp_display+20)
MOVT	R0, #hi_addr(_kp_display+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1268 :: 		kp_display.Caption         = kp_display_Caption;
MOVW	R1, #lo_addr(_kp_display_Caption+0)
MOVT	R1, #hi_addr(_kp_display_Caption+0)
MOVW	R0, #lo_addr(_kp_display+24)
MOVT	R0, #hi_addr(_kp_display+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1269 :: 		kp_display.TextAlign       = _taRight;
MOVS	R1, #2
MOVW	R0, #lo_addr(_kp_display+28)
MOVT	R0, #hi_addr(_kp_display+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1270 :: 		kp_display.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_kp_display+29)
MOVT	R0, #hi_addr(_kp_display+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1271 :: 		kp_display.FontName        = Tahoma34x42_Regular;
MOVW	R9, #lo_addr(_Tahoma34x42_Regular+0)
MOVT	R9, #hi_addr(_Tahoma34x42_Regular+0)
MOVW	R0, #lo_addr(_kp_display+32)
MOVT	R0, #hi_addr(_kp_display+32)
STR	R9, [R0, #0]
;BPWasher_driver.c,1272 :: 		kp_display.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_kp_display+48)
MOVT	R0, #hi_addr(_kp_display+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1273 :: 		kp_display.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_kp_display+36)
MOVT	R0, #hi_addr(_kp_display+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1274 :: 		kp_display.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_kp_display+38)
MOVT	R0, #hi_addr(_kp_display+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1275 :: 		kp_display.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_kp_display+39)
MOVT	R0, #hi_addr(_kp_display+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1276 :: 		kp_display.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_kp_display+40)
MOVT	R0, #hi_addr(_kp_display+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1277 :: 		kp_display.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_kp_display+42)
MOVT	R0, #hi_addr(_kp_display+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1278 :: 		kp_display.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_kp_display+44)
MOVT	R0, #hi_addr(_kp_display+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1279 :: 		kp_display.Color           = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_kp_display+46)
MOVT	R0, #hi_addr(_kp_display+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1280 :: 		kp_display.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_kp_display+50)
MOVT	R0, #hi_addr(_kp_display+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1281 :: 		kp_display.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_kp_display+52)
MOVT	R0, #hi_addr(_kp_display+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1282 :: 		kp_display.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_kp_display+56)
MOVT	R0, #hi_addr(_kp_display+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1283 :: 		kp_display.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_kp_display+60)
MOVT	R0, #hi_addr(_kp_display+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1284 :: 		kp_display.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_kp_display+64)
MOVT	R0, #hi_addr(_kp_display+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1286 :: 		pos_neg.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_pos_neg+0)
MOVT	R0, #hi_addr(_pos_neg+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1287 :: 		pos_neg.Order           = 16;
MOVS	R1, #16
MOVW	R0, #lo_addr(_pos_neg+4)
MOVT	R0, #hi_addr(_pos_neg+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1288 :: 		pos_neg.Left            = 161;
MOVS	R1, #161
MOVW	R0, #lo_addr(_pos_neg+6)
MOVT	R0, #hi_addr(_pos_neg+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1289 :: 		pos_neg.Top             = 201;
MOVS	R1, #201
MOVW	R0, #lo_addr(_pos_neg+8)
MOVT	R0, #hi_addr(_pos_neg+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1290 :: 		pos_neg.Width           = 80;
MOVS	R1, #80
MOVW	R0, #lo_addr(_pos_neg+10)
MOVT	R0, #hi_addr(_pos_neg+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1291 :: 		pos_neg.Height          = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_pos_neg+12)
MOVT	R0, #hi_addr(_pos_neg+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1292 :: 		pos_neg.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_pos_neg+14)
MOVT	R0, #hi_addr(_pos_neg+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1293 :: 		pos_neg.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_pos_neg+16)
MOVT	R0, #hi_addr(_pos_neg+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1294 :: 		pos_neg.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_pos_neg+18)
MOVT	R0, #hi_addr(_pos_neg+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1295 :: 		pos_neg.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_pos_neg+19)
MOVT	R0, #hi_addr(_pos_neg+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1296 :: 		pos_neg.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_pos_neg+20)
MOVT	R0, #hi_addr(_pos_neg+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1297 :: 		pos_neg.Caption         = pos_neg_Caption;
MOVW	R1, #lo_addr(_pos_neg_Caption+0)
MOVT	R1, #hi_addr(_pos_neg_Caption+0)
MOVW	R0, #lo_addr(_pos_neg+24)
MOVT	R0, #hi_addr(_pos_neg+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1298 :: 		pos_neg.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_pos_neg+28)
MOVT	R0, #hi_addr(_pos_neg+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1299 :: 		pos_neg.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_pos_neg+29)
MOVT	R0, #hi_addr(_pos_neg+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1300 :: 		pos_neg.FontName        = Tahoma26x33_Regular;
MOVW	R0, #lo_addr(_pos_neg+32)
MOVT	R0, #hi_addr(_pos_neg+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,1301 :: 		pos_neg.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_pos_neg+49)
MOVT	R0, #hi_addr(_pos_neg+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1302 :: 		pos_neg.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_pos_neg+36)
MOVT	R0, #hi_addr(_pos_neg+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1303 :: 		pos_neg.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_pos_neg+38)
MOVT	R0, #hi_addr(_pos_neg+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1304 :: 		pos_neg.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_pos_neg+39)
MOVT	R0, #hi_addr(_pos_neg+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1305 :: 		pos_neg.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_pos_neg+40)
MOVT	R0, #hi_addr(_pos_neg+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1306 :: 		pos_neg.Gradient_Start_Color = 0x4A69;
MOVW	R1, #19049
MOVW	R0, #lo_addr(_pos_neg+42)
MOVT	R0, #hi_addr(_pos_neg+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1307 :: 		pos_neg.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_pos_neg+44)
MOVT	R0, #hi_addr(_pos_neg+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1308 :: 		pos_neg.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_pos_neg+46)
MOVT	R0, #hi_addr(_pos_neg+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1309 :: 		pos_neg.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_pos_neg+50)
MOVT	R0, #hi_addr(_pos_neg+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1310 :: 		pos_neg.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_pos_neg+48)
MOVT	R0, #hi_addr(_pos_neg+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1311 :: 		pos_neg.OnUpPtr         = pos_negClick;
MOVW	R1, #lo_addr(_pos_negClick+0)
MOVT	R1, #hi_addr(_pos_negClick+0)
MOVW	R0, #lo_addr(_pos_neg+52)
MOVT	R0, #hi_addr(_pos_neg+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1312 :: 		pos_neg.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_pos_neg+56)
MOVT	R0, #hi_addr(_pos_neg+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1313 :: 		pos_neg.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_pos_neg+60)
MOVT	R0, #hi_addr(_pos_neg+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1314 :: 		pos_neg.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_pos_neg+64)
MOVT	R0, #hi_addr(_pos_neg+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1316 :: 		lblKeyboardTitle.OwnerScreen     = &Keypad;
MOVW	R1, #lo_addr(_Keypad+0)
MOVT	R1, #hi_addr(_Keypad+0)
MOVW	R0, #lo_addr(_lblKeyboardTitle+0)
MOVT	R0, #hi_addr(_lblKeyboardTitle+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1317 :: 		lblKeyboardTitle.Order           = 17;  // need to keep orders in sync
MOVS	R1, #17
MOVW	R0, #lo_addr(_lblKeyboardTitle+4)
MOVT	R0, #hi_addr(_lblKeyboardTitle+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1318 :: 		lblKeyboardTitle.Left            = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_lblKeyboardTitle+6)
MOVT	R0, #hi_addr(_lblKeyboardTitle+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1319 :: 		lblKeyboardTitle.Top             = 53;
MOVS	R1, #53
MOVW	R0, #lo_addr(_lblKeyboardTitle+8)
MOVT	R0, #hi_addr(_lblKeyboardTitle+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1320 :: 		lblKeyboardTitle.Width           = 180;
MOVS	R1, #180
MOVW	R0, #lo_addr(_lblKeyboardTitle+10)
MOVT	R0, #hi_addr(_lblKeyboardTitle+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1321 :: 		lblKeyboardTitle.Height          = 32;
MOVS	R1, #32
MOVW	R0, #lo_addr(_lblKeyboardTitle+12)
MOVT	R0, #hi_addr(_lblKeyboardTitle+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1322 :: 		lblKeyboardTitle.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_lblKeyboardTitle+27)
MOVT	R0, #hi_addr(_lblKeyboardTitle+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1323 :: 		lblKeyboardTitle.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_lblKeyboardTitle+28)
MOVT	R0, #hi_addr(_lblKeyboardTitle+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1324 :: 		lblKeyboardTitle.Caption         = 0;     // to be replaced with pointer to string later
MOVS	R1, #0
MOVW	R0, #lo_addr(_lblKeyboardTitle+16)
MOVT	R0, #hi_addr(_lblKeyboardTitle+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,1325 :: 		lblKeyboardTitle.FontName        = Tahoma23x29_Regular;
MOVW	R1, #lo_addr(_Tahoma23x29_Regular+0)
MOVT	R1, #hi_addr(_Tahoma23x29_Regular+0)
MOVW	R0, #lo_addr(_lblKeyboardTitle+20)
MOVT	R0, #hi_addr(_lblKeyboardTitle+20)
STR	R1, [R0, #0]
;BPWasher_driver.c,1326 :: 		lblKeyboardTitle.Font_Color      = 0xFFE0;
MOVW	R1, #65504
MOVW	R0, #lo_addr(_lblKeyboardTitle+24)
MOVT	R0, #hi_addr(_lblKeyboardTitle+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1327 :: 		lblKeyboardTitle.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_lblKeyboardTitle+26)
MOVT	R0, #hi_addr(_lblKeyboardTitle+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1328 :: 		lblKeyboardTitle.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_lblKeyboardTitle+32)
MOVT	R0, #hi_addr(_lblKeyboardTitle+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1329 :: 		lblKeyboardTitle.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_lblKeyboardTitle+36)
MOVT	R0, #hi_addr(_lblKeyboardTitle+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,1330 :: 		lblKeyboardTitle.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_lblKeyboardTitle+40)
MOVT	R0, #hi_addr(_lblKeyboardTitle+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,1331 :: 		lblKeyboardTitle.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_lblKeyboardTitle+44)
MOVT	R0, #hi_addr(_lblKeyboardTitle+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,1333 :: 		Diagram3_Label1.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_Diagram3_Label1+0)
MOVT	R0, #hi_addr(_Diagram3_Label1+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1334 :: 		Diagram3_Label1.Order           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_Label1+4)
MOVT	R0, #hi_addr(_Diagram3_Label1+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1335 :: 		Diagram3_Label1.Left            = 285;
MOVW	R1, #285
MOVW	R0, #lo_addr(_Diagram3_Label1+6)
MOVT	R0, #hi_addr(_Diagram3_Label1+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1336 :: 		Diagram3_Label1.Top             = 71;
MOVS	R1, #71
MOVW	R0, #lo_addr(_Diagram3_Label1+8)
MOVT	R0, #hi_addr(_Diagram3_Label1+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1337 :: 		Diagram3_Label1.Width           = 21;
MOVS	R1, #21
MOVW	R0, #lo_addr(_Diagram3_Label1+10)
MOVT	R0, #hi_addr(_Diagram3_Label1+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1338 :: 		Diagram3_Label1.Height          = 25;
MOVS	R1, #25
MOVW	R0, #lo_addr(_Diagram3_Label1+12)
MOVT	R0, #hi_addr(_Diagram3_Label1+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1339 :: 		Diagram3_Label1.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diagram3_Label1+27)
MOVT	R0, #hi_addr(_Diagram3_Label1+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1340 :: 		Diagram3_Label1.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diagram3_Label1+28)
MOVT	R0, #hi_addr(_Diagram3_Label1+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1341 :: 		Diagram3_Label1.Caption         = Diagram3_Label1_Caption;
MOVW	R1, #lo_addr(_Diagram3_Label1_Caption+0)
MOVT	R1, #hi_addr(_Diagram3_Label1_Caption+0)
MOVW	R0, #lo_addr(_Diagram3_Label1+16)
MOVT	R0, #hi_addr(_Diagram3_Label1+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,1342 :: 		Diagram3_Label1.FontName        = Tahoma19x23_Regular;
MOVW	R8, #lo_addr(_Tahoma19x23_Regular+0)
MOVT	R8, #hi_addr(_Tahoma19x23_Regular+0)
MOVW	R0, #lo_addr(_Diagram3_Label1+20)
MOVT	R0, #hi_addr(_Diagram3_Label1+20)
STR	R8, [R0, #0]
;BPWasher_driver.c,1343 :: 		Diagram3_Label1.Font_Color      = 0xFFE0;
MOVW	R1, #65504
MOVW	R0, #lo_addr(_Diagram3_Label1+24)
MOVT	R0, #hi_addr(_Diagram3_Label1+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1344 :: 		Diagram3_Label1.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_Label1+26)
MOVT	R0, #hi_addr(_Diagram3_Label1+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1345 :: 		Diagram3_Label1.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_Label1+32)
MOVT	R0, #hi_addr(_Diagram3_Label1+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1346 :: 		Diagram3_Label1.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_Label1+36)
MOVT	R0, #hi_addr(_Diagram3_Label1+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,1347 :: 		Diagram3_Label1.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_Label1+40)
MOVT	R0, #hi_addr(_Diagram3_Label1+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,1348 :: 		Diagram3_Label1.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_Label1+44)
MOVT	R0, #hi_addr(_Diagram3_Label1+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,1350 :: 		indicator.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_indicator+0)
MOVT	R0, #hi_addr(_indicator+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1351 :: 		indicator.Order           = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_indicator+4)
MOVT	R0, #hi_addr(_indicator+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1352 :: 		indicator.Left            = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_indicator+6)
MOVT	R0, #hi_addr(_indicator+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1353 :: 		indicator.Top             = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_indicator+8)
MOVT	R0, #hi_addr(_indicator+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1354 :: 		indicator.Radius          = 17;
MOVS	R1, #17
MOVW	R0, #lo_addr(_indicator+10)
MOVT	R0, #hi_addr(_indicator+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1355 :: 		indicator.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_indicator+12)
MOVT	R0, #hi_addr(_indicator+12)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1356 :: 		indicator.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_indicator+14)
MOVT	R0, #hi_addr(_indicator+14)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1357 :: 		indicator.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_indicator+16)
MOVT	R0, #hi_addr(_indicator+16)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1358 :: 		indicator.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_indicator+17)
MOVT	R0, #hi_addr(_indicator+17)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1359 :: 		indicator.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_indicator+18)
MOVT	R0, #hi_addr(_indicator+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1360 :: 		indicator.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_indicator+19)
MOVT	R0, #hi_addr(_indicator+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1361 :: 		indicator.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_indicator+20)
MOVT	R0, #hi_addr(_indicator+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1362 :: 		indicator.Gradient_Start_Color = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_indicator+22)
MOVT	R0, #hi_addr(_indicator+22)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1363 :: 		indicator.Gradient_End_Color = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_indicator+24)
MOVT	R0, #hi_addr(_indicator+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1364 :: 		indicator.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_indicator+26)
MOVT	R0, #hi_addr(_indicator+26)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1365 :: 		indicator.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_indicator+28)
MOVT	R0, #hi_addr(_indicator+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1366 :: 		indicator.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_indicator+30)
MOVT	R0, #hi_addr(_indicator+30)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1367 :: 		indicator.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_indicator+32)
MOVT	R0, #hi_addr(_indicator+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1368 :: 		indicator.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_indicator+36)
MOVT	R0, #hi_addr(_indicator+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,1369 :: 		indicator.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_indicator+40)
MOVT	R0, #hi_addr(_indicator+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,1370 :: 		indicator.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_indicator+44)
MOVT	R0, #hi_addr(_indicator+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,1372 :: 		Screen_PV.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_Screen_PV+0)
MOVT	R0, #hi_addr(_Screen_PV+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1373 :: 		Screen_PV.Order           = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_Screen_PV+4)
MOVT	R0, #hi_addr(_Screen_PV+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1374 :: 		Screen_PV.Left            = 29;
MOVS	R1, #29
MOVW	R0, #lo_addr(_Screen_PV+6)
MOVT	R0, #hi_addr(_Screen_PV+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1375 :: 		Screen_PV.Top             = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+8)
MOVT	R0, #hi_addr(_Screen_PV+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1376 :: 		Screen_PV.Width           = 239;
MOVS	R1, #239
MOVW	R0, #lo_addr(_Screen_PV+10)
MOVT	R0, #hi_addr(_Screen_PV+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1377 :: 		Screen_PV.Height          = 115;
MOVS	R1, #115
MOVW	R0, #lo_addr(_Screen_PV+12)
MOVT	R0, #hi_addr(_Screen_PV+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1378 :: 		Screen_PV.Pen_Width       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+14)
MOVT	R0, #hi_addr(_Screen_PV+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1379 :: 		Screen_PV.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+16)
MOVT	R0, #hi_addr(_Screen_PV+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1380 :: 		Screen_PV.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Screen_PV+18)
MOVT	R0, #hi_addr(_Screen_PV+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1381 :: 		Screen_PV.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+19)
MOVT	R0, #hi_addr(_Screen_PV+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1382 :: 		Screen_PV.Transparent     = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+20)
MOVT	R0, #hi_addr(_Screen_PV+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1383 :: 		Screen_PV.Caption         = Screen_PV_Caption;
MOVW	R1, #lo_addr(_Screen_PV_Caption+0)
MOVT	R1, #hi_addr(_Screen_PV_Caption+0)
MOVW	R0, #lo_addr(_Screen_PV+24)
MOVT	R0, #hi_addr(_Screen_PV+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1384 :: 		Screen_PV.TextAlign       = _taRight;
MOVS	R1, #2
MOVW	R0, #lo_addr(_Screen_PV+28)
MOVT	R0, #hi_addr(_Screen_PV+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1385 :: 		Screen_PV.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Screen_PV+29)
MOVT	R0, #hi_addr(_Screen_PV+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1386 :: 		Screen_PV.FontName        = Tahoma83x103_Regular;
MOVW	R1, #lo_addr(_Tahoma83x103_Regular+0)
MOVT	R1, #hi_addr(_Tahoma83x103_Regular+0)
MOVW	R0, #lo_addr(_Screen_PV+32)
MOVT	R0, #hi_addr(_Screen_PV+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1387 :: 		Screen_PV.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Screen_PV+48)
MOVT	R0, #hi_addr(_Screen_PV+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1388 :: 		Screen_PV.Font_Color      = 0x2986;
MOVW	R1, #10630
MOVW	R0, #lo_addr(_Screen_PV+36)
MOVT	R0, #hi_addr(_Screen_PV+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1389 :: 		Screen_PV.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+38)
MOVT	R0, #hi_addr(_Screen_PV+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1390 :: 		Screen_PV.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+39)
MOVT	R0, #hi_addr(_Screen_PV+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1391 :: 		Screen_PV.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+40)
MOVT	R0, #hi_addr(_Screen_PV+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1392 :: 		Screen_PV.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Screen_PV+42)
MOVT	R0, #hi_addr(_Screen_PV+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1393 :: 		Screen_PV.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Screen_PV+44)
MOVT	R0, #hi_addr(_Screen_PV+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1394 :: 		Screen_PV.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Screen_PV+46)
MOVT	R0, #hi_addr(_Screen_PV+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1395 :: 		Screen_PV.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Screen_PV+50)
MOVT	R0, #hi_addr(_Screen_PV+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1396 :: 		Screen_PV.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+52)
MOVT	R0, #hi_addr(_Screen_PV+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1397 :: 		Screen_PV.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+56)
MOVT	R0, #hi_addr(_Screen_PV+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1398 :: 		Screen_PV.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+60)
MOVT	R0, #hi_addr(_Screen_PV+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1399 :: 		Screen_PV.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+64)
MOVT	R0, #hi_addr(_Screen_PV+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1401 :: 		Line2.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_Line2+0)
MOVT	R0, #hi_addr(_Line2+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1402 :: 		Line2.Order           = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Line2+4)
MOVT	R0, #hi_addr(_Line2+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1403 :: 		Line2.First_Point_X   = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Line2+6)
MOVT	R0, #hi_addr(_Line2+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1404 :: 		Line2.First_Point_Y   = 160;
MOVS	R1, #160
MOVW	R0, #lo_addr(_Line2+8)
MOVT	R0, #hi_addr(_Line2+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1405 :: 		Line2.Second_Point_X  = 321;
MOVW	R1, #321
MOVW	R0, #lo_addr(_Line2+10)
MOVT	R0, #hi_addr(_Line2+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1406 :: 		Line2.Second_Point_Y  = 160;
MOVS	R1, #160
MOVW	R0, #lo_addr(_Line2+12)
MOVT	R0, #hi_addr(_Line2+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1407 :: 		Line2.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Line2+15)
MOVT	R0, #hi_addr(_Line2+15)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1408 :: 		Line2.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Line2+14)
MOVT	R0, #hi_addr(_Line2+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1409 :: 		Line2.Color           = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Line2+16)
MOVT	R0, #hi_addr(_Line2+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1411 :: 		Main_Screen_settings.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_Main_Screen_settings+0)
MOVT	R0, #hi_addr(_Main_Screen_settings+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1412 :: 		Main_Screen_settings.Order           = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_Main_Screen_settings+4)
MOVT	R0, #hi_addr(_Main_Screen_settings+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1413 :: 		Main_Screen_settings.Left            = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_Screen_settings+6)
MOVT	R0, #hi_addr(_Main_Screen_settings+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1414 :: 		Main_Screen_settings.Top             = 176;
MOVS	R1, #176
MOVW	R0, #lo_addr(_Main_Screen_settings+8)
MOVT	R0, #hi_addr(_Main_Screen_settings+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1415 :: 		Main_Screen_settings.Width           = 103;
MOVS	R1, #103
MOVW	R0, #lo_addr(_Main_Screen_settings+10)
MOVT	R0, #hi_addr(_Main_Screen_settings+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1416 :: 		Main_Screen_settings.Height          = 64;
MOVS	R1, #64
MOVW	R0, #lo_addr(_Main_Screen_settings+12)
MOVT	R0, #hi_addr(_Main_Screen_settings+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1417 :: 		Main_Screen_settings.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_Screen_settings+14)
MOVT	R0, #hi_addr(_Main_Screen_settings+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1418 :: 		Main_Screen_settings.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_settings+16)
MOVT	R0, #hi_addr(_Main_Screen_settings+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1419 :: 		Main_Screen_settings.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_Screen_settings+18)
MOVT	R0, #hi_addr(_Main_Screen_settings+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1420 :: 		Main_Screen_settings.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_Screen_settings+19)
MOVT	R0, #hi_addr(_Main_Screen_settings+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1421 :: 		Main_Screen_settings.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_Screen_settings+20)
MOVT	R0, #hi_addr(_Main_Screen_settings+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1422 :: 		Main_Screen_settings.Caption         = Main_Screen_settings_Caption;
MOVW	R1, #lo_addr(_Main_Screen_settings_Caption+0)
MOVT	R1, #hi_addr(_Main_Screen_settings_Caption+0)
MOVW	R0, #lo_addr(_Main_Screen_settings+24)
MOVT	R0, #hi_addr(_Main_Screen_settings+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1423 :: 		Main_Screen_settings.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_Screen_settings+28)
MOVT	R0, #hi_addr(_Main_Screen_settings+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1424 :: 		Main_Screen_settings.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_Screen_settings+29)
MOVT	R0, #hi_addr(_Main_Screen_settings+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1425 :: 		Main_Screen_settings.FontName        = Tahoma11x13_Regular;
MOVW	R1, #lo_addr(_Tahoma11x13_Regular+0)
MOVT	R1, #hi_addr(_Tahoma11x13_Regular+0)
MOVW	R0, #lo_addr(_Main_Screen_settings+32)
MOVT	R0, #hi_addr(_Main_Screen_settings+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1426 :: 		Main_Screen_settings.PressColEnabled = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_settings+49)
MOVT	R0, #hi_addr(_Main_Screen_settings+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1427 :: 		Main_Screen_settings.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_settings+36)
MOVT	R0, #hi_addr(_Main_Screen_settings+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1428 :: 		Main_Screen_settings.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_settings+38)
MOVT	R0, #hi_addr(_Main_Screen_settings+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1429 :: 		Main_Screen_settings.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_settings+39)
MOVT	R0, #hi_addr(_Main_Screen_settings+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1430 :: 		Main_Screen_settings.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_settings+40)
MOVT	R0, #hi_addr(_Main_Screen_settings+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1431 :: 		Main_Screen_settings.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Main_Screen_settings+42)
MOVT	R0, #hi_addr(_Main_Screen_settings+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1432 :: 		Main_Screen_settings.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Main_Screen_settings+44)
MOVT	R0, #hi_addr(_Main_Screen_settings+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1433 :: 		Main_Screen_settings.Color           = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Main_Screen_settings+46)
MOVT	R0, #hi_addr(_Main_Screen_settings+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1434 :: 		Main_Screen_settings.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Main_Screen_settings+50)
MOVT	R0, #hi_addr(_Main_Screen_settings+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1435 :: 		Main_Screen_settings.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_Main_Screen_settings+48)
MOVT	R0, #hi_addr(_Main_Screen_settings+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1436 :: 		Main_Screen_settings.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_settings+52)
MOVT	R0, #hi_addr(_Main_Screen_settings+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1437 :: 		Main_Screen_settings.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_settings+56)
MOVT	R0, #hi_addr(_Main_Screen_settings+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1438 :: 		Main_Screen_settings.OnClickPtr      = Main_Screen_settingsClick;
MOVW	R1, #lo_addr(_Main_Screen_settingsClick+0)
MOVT	R1, #hi_addr(_Main_Screen_settingsClick+0)
MOVW	R0, #lo_addr(_Main_Screen_settings+60)
MOVT	R0, #hi_addr(_Main_Screen_settings+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1439 :: 		Main_Screen_settings.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_settings+64)
MOVT	R0, #hi_addr(_Main_Screen_settings+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1441 :: 		Main_screen_settings_image.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_Main_screen_settings_image+0)
MOVT	R0, #hi_addr(_Main_screen_settings_image+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1442 :: 		Main_screen_settings_image.Order           = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_Main_screen_settings_image+4)
MOVT	R0, #hi_addr(_Main_screen_settings_image+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1443 :: 		Main_screen_settings_image.Left            = 33;
MOVS	R1, #33
MOVW	R0, #lo_addr(_Main_screen_settings_image+6)
MOVT	R0, #hi_addr(_Main_screen_settings_image+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1444 :: 		Main_screen_settings_image.Top             = 187;
MOVS	R1, #187
MOVW	R0, #lo_addr(_Main_screen_settings_image+8)
MOVT	R0, #hi_addr(_Main_screen_settings_image+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1445 :: 		Main_screen_settings_image.Width           = 40;
MOVS	R1, #40
MOVW	R0, #lo_addr(_Main_screen_settings_image+10)
MOVT	R0, #hi_addr(_Main_screen_settings_image+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1446 :: 		Main_screen_settings_image.Height          = 39;
MOVS	R1, #39
MOVW	R0, #lo_addr(_Main_screen_settings_image+12)
MOVT	R0, #hi_addr(_Main_screen_settings_image+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1447 :: 		Main_screen_settings_image.Picture_Type    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_screen_settings_image+22)
MOVT	R0, #hi_addr(_Main_screen_settings_image+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1448 :: 		Main_screen_settings_image.Picture_Ratio   = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_screen_settings_image+23)
MOVT	R0, #hi_addr(_Main_screen_settings_image+23)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1449 :: 		Main_screen_settings_image.Picture_Name    = Settings_bmp;
MOVW	R1, #lo_addr(_Settings_bmp+0)
MOVT	R1, #hi_addr(_Settings_bmp+0)
MOVW	R0, #lo_addr(_Main_screen_settings_image+16)
MOVT	R0, #hi_addr(_Main_screen_settings_image+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,1450 :: 		Main_screen_settings_image.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_screen_settings_image+20)
MOVT	R0, #hi_addr(_Main_screen_settings_image+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1451 :: 		Main_screen_settings_image.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_screen_settings_image+21)
MOVT	R0, #hi_addr(_Main_screen_settings_image+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1452 :: 		Main_screen_settings_image.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_screen_settings_image+24)
MOVT	R0, #hi_addr(_Main_screen_settings_image+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1453 :: 		Main_screen_settings_image.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_screen_settings_image+28)
MOVT	R0, #hi_addr(_Main_screen_settings_image+28)
STR	R1, [R0, #0]
;BPWasher_driver.c,1454 :: 		Main_screen_settings_image.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_screen_settings_image+32)
MOVT	R0, #hi_addr(_Main_screen_settings_image+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1455 :: 		Main_screen_settings_image.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_screen_settings_image+36)
MOVT	R0, #hi_addr(_Main_screen_settings_image+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,1457 :: 		Line1.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_Line1+0)
MOVT	R0, #hi_addr(_Line1+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1458 :: 		Line1.Order           = 6;
MOVS	R1, #6
MOVW	R0, #lo_addr(_Line1+4)
MOVT	R0, #hi_addr(_Line1+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1459 :: 		Line1.First_Point_X   = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Line1+6)
MOVT	R0, #hi_addr(_Line1+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1460 :: 		Line1.First_Point_Y   = 100;
MOVS	R1, #100
MOVW	R0, #lo_addr(_Line1+8)
MOVT	R0, #hi_addr(_Line1+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1461 :: 		Line1.Second_Point_X  = 320;
MOVW	R1, #320
MOVW	R0, #lo_addr(_Line1+10)
MOVT	R0, #hi_addr(_Line1+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1462 :: 		Line1.Second_Point_Y  = 100;
MOVS	R1, #100
MOVW	R0, #lo_addr(_Line1+12)
MOVT	R0, #hi_addr(_Line1+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1463 :: 		Line1.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Line1+15)
MOVT	R0, #hi_addr(_Line1+15)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1464 :: 		Line1.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Line1+14)
MOVT	R0, #hi_addr(_Line1+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1465 :: 		Line1.Color           = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Line1+16)
MOVT	R0, #hi_addr(_Line1+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1467 :: 		Main_Screen_start.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_Main_Screen_start+0)
MOVT	R0, #hi_addr(_Main_Screen_start+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1468 :: 		Main_Screen_start.Order           = 7;
MOVS	R1, #7
MOVW	R0, #lo_addr(_Main_Screen_start+4)
MOVT	R0, #hi_addr(_Main_Screen_start+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1469 :: 		Main_Screen_start.Left            = 216;
MOVS	R1, #216
MOVW	R0, #lo_addr(_Main_Screen_start+6)
MOVT	R0, #hi_addr(_Main_Screen_start+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1470 :: 		Main_Screen_start.Top             = 178;
MOVS	R1, #178
MOVW	R0, #lo_addr(_Main_Screen_start+8)
MOVT	R0, #hi_addr(_Main_Screen_start+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1471 :: 		Main_Screen_start.Width           = 103;
MOVS	R1, #103
MOVW	R0, #lo_addr(_Main_Screen_start+10)
MOVT	R0, #hi_addr(_Main_Screen_start+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1472 :: 		Main_Screen_start.Height          = 61;
MOVS	R1, #61
MOVW	R0, #lo_addr(_Main_Screen_start+12)
MOVT	R0, #hi_addr(_Main_Screen_start+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1473 :: 		Main_Screen_start.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_Screen_start+14)
MOVT	R0, #hi_addr(_Main_Screen_start+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1474 :: 		Main_Screen_start.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_start+16)
MOVT	R0, #hi_addr(_Main_Screen_start+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1475 :: 		Main_Screen_start.Visible         = 0;  // was 1 - CM 14/7/17
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_start+18)
MOVT	R0, #hi_addr(_Main_Screen_start+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1476 :: 		Main_Screen_start.Active          = 0;  // was 1 - CM 04/8/17
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_start+19)
MOVT	R0, #hi_addr(_Main_Screen_start+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1477 :: 		Main_Screen_start.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_Screen_start+20)
MOVT	R0, #hi_addr(_Main_Screen_start+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1478 :: 		Main_Screen_start.Caption         = Main_Screen_start_Caption;
MOVW	R1, #lo_addr(_Main_Screen_start_Caption+0)
MOVT	R1, #hi_addr(_Main_Screen_start_Caption+0)
MOVW	R0, #lo_addr(_Main_Screen_start+24)
MOVT	R0, #hi_addr(_Main_Screen_start+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1479 :: 		Main_Screen_start.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_Screen_start+28)
MOVT	R0, #hi_addr(_Main_Screen_start+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1480 :: 		Main_Screen_start.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_Screen_start+29)
MOVT	R0, #hi_addr(_Main_Screen_start+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1481 :: 		Main_Screen_start.FontName        = Tahoma21x25_Regular;
MOVW	R7, #lo_addr(_Tahoma21x25_Regular+0)
MOVT	R7, #hi_addr(_Tahoma21x25_Regular+0)
MOVW	R0, #lo_addr(_Main_Screen_start+32)
MOVT	R0, #hi_addr(_Main_Screen_start+32)
STR	R7, [R0, #0]
;BPWasher_driver.c,1482 :: 		Main_Screen_start.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Main_Screen_start+49)
MOVT	R0, #hi_addr(_Main_Screen_start+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1483 :: 		Main_Screen_start.Font_Color      = CL_Black;
MOVW	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_start+36)
MOVT	R0, #hi_addr(_Main_Screen_start+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1484 :: 		Main_Screen_start.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_start+38)
MOVT	R0, #hi_addr(_Main_Screen_start+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1485 :: 		Main_Screen_start.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_start+39)
MOVT	R0, #hi_addr(_Main_Screen_start+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1486 :: 		Main_Screen_start.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_start+40)
MOVT	R0, #hi_addr(_Main_Screen_start+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1487 :: 		Main_Screen_start.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Main_Screen_start+42)
MOVT	R0, #hi_addr(_Main_Screen_start+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1488 :: 		Main_Screen_start.Gradient_End_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Main_Screen_start+44)
MOVT	R0, #hi_addr(_Main_Screen_start+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1489 :: 		Main_Screen_start.Color           = CL_Red;      //0xFFFF
MOVW	R1, #63488
MOVW	R0, #lo_addr(_Main_Screen_start+46)
MOVT	R0, #hi_addr(_Main_Screen_start+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1490 :: 		Main_Screen_start.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Main_Screen_start+50)
MOVT	R0, #hi_addr(_Main_Screen_start+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1491 :: 		Main_Screen_start.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_Main_Screen_start+48)
MOVT	R0, #hi_addr(_Main_Screen_start+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1492 :: 		Main_Screen_start.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_start+52)
MOVT	R0, #hi_addr(_Main_Screen_start+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1493 :: 		Main_Screen_start.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_start+56)
MOVT	R0, #hi_addr(_Main_Screen_start+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1494 :: 		Main_Screen_start.OnClickPtr      = Main_Screen_startClick;
MOVW	R1, #lo_addr(_Main_Screen_startClick+0)
MOVT	R1, #hi_addr(_Main_Screen_startClick+0)
MOVW	R0, #lo_addr(_Main_Screen_start+60)
MOVT	R0, #hi_addr(_Main_Screen_start+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1495 :: 		Main_Screen_start.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Main_Screen_start+64)
MOVT	R0, #hi_addr(_Main_Screen_start+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1497 :: 		start_stop.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_start_stop+0)
MOVT	R0, #hi_addr(_start_stop+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1498 :: 		start_stop.Order           = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_start_stop+4)
MOVT	R0, #hi_addr(_start_stop+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1499 :: 		start_stop.Left            = 151; //246
MOVS	R1, #151
MOVW	R0, #lo_addr(_start_stop+6)
MOVT	R0, #hi_addr(_start_stop+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1500 :: 		start_stop.Top             = 186;  //186
MOVS	R1, #186
MOVW	R0, #lo_addr(_start_stop+8)
MOVT	R0, #hi_addr(_start_stop+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1501 :: 		start_stop.Width           = 44;
MOVS	R1, #44
MOVW	R0, #lo_addr(_start_stop+10)
MOVT	R0, #hi_addr(_start_stop+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1502 :: 		start_stop.Height          = 46;
MOVS	R1, #46
MOVW	R0, #lo_addr(_start_stop+12)
MOVT	R0, #hi_addr(_start_stop+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1503 :: 		start_stop.Picture_Type    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_start_stop+22)
MOVT	R0, #hi_addr(_start_stop+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1504 :: 		start_stop.Picture_Ratio   = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_start_stop+23)
MOVT	R0, #hi_addr(_start_stop+23)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1505 :: 		start_stop.Picture_Name    = powergreen_bmp;
MOVW	R1, #lo_addr(_powergreen_bmp+0)
MOVT	R1, #hi_addr(_powergreen_bmp+0)
MOVW	R0, #lo_addr(_start_stop+16)
MOVT	R0, #hi_addr(_start_stop+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,1506 :: 		start_stop.Visible         = 0;  //1
MOVS	R1, #0
MOVW	R0, #lo_addr(_start_stop+20)
MOVT	R0, #hi_addr(_start_stop+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1507 :: 		start_stop.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_start_stop+21)
MOVT	R0, #hi_addr(_start_stop+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1508 :: 		start_stop.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_start_stop+24)
MOVT	R0, #hi_addr(_start_stop+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1509 :: 		start_stop.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_start_stop+28)
MOVT	R0, #hi_addr(_start_stop+28)
STR	R1, [R0, #0]
;BPWasher_driver.c,1510 :: 		start_stop.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_start_stop+32)
MOVT	R0, #hi_addr(_start_stop+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1511 :: 		start_stop.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_start_stop+36)
MOVT	R0, #hi_addr(_start_stop+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,1513 :: 		Diagram3_thermred.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_Diagram3_thermred+0)
MOVT	R0, #hi_addr(_Diagram3_thermred+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1514 :: 		Diagram3_thermred.Order           = 9;
MOVS	R1, #9
MOVW	R0, #lo_addr(_Diagram3_thermred+4)
MOVT	R0, #hi_addr(_Diagram3_thermred+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1515 :: 		Diagram3_thermred.Left            = 280;
MOVW	R1, #280
MOVW	R0, #lo_addr(_Diagram3_thermred+6)
MOVT	R0, #hi_addr(_Diagram3_thermred+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1516 :: 		Diagram3_thermred.Top             = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diagram3_thermred+8)
MOVT	R0, #hi_addr(_Diagram3_thermred+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1517 :: 		Diagram3_thermred.Width           = 40;
MOVS	R1, #40
MOVW	R0, #lo_addr(_Diagram3_thermred+10)
MOVT	R0, #hi_addr(_Diagram3_thermred+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1518 :: 		Diagram3_thermred.Height          = 40;
MOVS	R1, #40
MOVW	R0, #lo_addr(_Diagram3_thermred+12)
MOVT	R0, #hi_addr(_Diagram3_thermred+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1519 :: 		Diagram3_thermred.Picture_Type    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_thermred+22)
MOVT	R0, #hi_addr(_Diagram3_thermred+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1520 :: 		Diagram3_thermred.Picture_Ratio   = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diagram3_thermred+23)
MOVT	R0, #hi_addr(_Diagram3_thermred+23)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1521 :: 		Diagram3_thermred.Picture_Name    = Thermometer_red_bmp;
MOVW	R1, #lo_addr(_Thermometer_red_bmp+0)
MOVT	R1, #hi_addr(_Thermometer_red_bmp+0)
MOVW	R0, #lo_addr(_Diagram3_thermred+16)
MOVT	R0, #hi_addr(_Diagram3_thermred+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,1522 :: 		Diagram3_thermred.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diagram3_thermred+20)
MOVT	R0, #hi_addr(_Diagram3_thermred+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1523 :: 		Diagram3_thermred.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diagram3_thermred+21)
MOVT	R0, #hi_addr(_Diagram3_thermred+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1524 :: 		Diagram3_thermred.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_thermred+24)
MOVT	R0, #hi_addr(_Diagram3_thermred+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1525 :: 		Diagram3_thermred.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_thermred+28)
MOVT	R0, #hi_addr(_Diagram3_thermred+28)
STR	R1, [R0, #0]
;BPWasher_driver.c,1526 :: 		Diagram3_thermred.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_thermred+32)
MOVT	R0, #hi_addr(_Diagram3_thermred+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1527 :: 		Diagram3_thermred.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_thermred+36)
MOVT	R0, #hi_addr(_Diagram3_thermred+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,1529 :: 		Diagram3_thermBlack.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_Diagram3_thermBlack+0)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1530 :: 		Diagram3_thermBlack.Order           = 10;
MOVS	R1, #10
MOVW	R0, #lo_addr(_Diagram3_thermBlack+4)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1531 :: 		Diagram3_thermBlack.Left            = 280;
MOVW	R1, #280
MOVW	R0, #lo_addr(_Diagram3_thermBlack+6)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1532 :: 		Diagram3_thermBlack.Top             = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diagram3_thermBlack+8)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1533 :: 		Diagram3_thermBlack.Width           = 40;
MOVS	R1, #40
MOVW	R0, #lo_addr(_Diagram3_thermBlack+10)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1534 :: 		Diagram3_thermBlack.Height          = 40;
MOVS	R1, #40
MOVW	R0, #lo_addr(_Diagram3_thermBlack+12)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1535 :: 		Diagram3_thermBlack.Picture_Type    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_thermBlack+22)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1536 :: 		Diagram3_thermBlack.Picture_Ratio   = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diagram3_thermBlack+23)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+23)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1537 :: 		Diagram3_thermBlack.Picture_Name    = Thermometer_black_bmp;
MOVW	R1, #lo_addr(_Thermometer_black_bmp+0)
MOVT	R1, #hi_addr(_Thermometer_black_bmp+0)
MOVW	R0, #lo_addr(_Diagram3_thermBlack+16)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,1538 :: 		Diagram3_thermBlack.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diagram3_thermBlack+20)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1539 :: 		Diagram3_thermBlack.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Diagram3_thermBlack+21)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1540 :: 		Diagram3_thermBlack.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_thermBlack+24)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1541 :: 		Diagram3_thermBlack.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_thermBlack+28)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+28)
STR	R1, [R0, #0]
;BPWasher_driver.c,1542 :: 		Diagram3_thermBlack.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_thermBlack+32)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1543 :: 		Diagram3_thermBlack.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Diagram3_thermBlack+36)
MOVT	R0, #hi_addr(_Diagram3_thermBlack+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,1545 :: 		Label1.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_Label1+0)
MOVT	R0, #hi_addr(_Label1+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1546 :: 		Label1.Order           = 11;
MOVS	R1, #11
MOVW	R0, #lo_addr(_Label1+4)
MOVT	R0, #hi_addr(_Label1+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1547 :: 		Label1.Left            = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label1+6)
MOVT	R0, #hi_addr(_Label1+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1548 :: 		Label1.Top             = 117;
MOVS	R1, #117
MOVW	R0, #lo_addr(_Label1+8)
MOVT	R0, #hi_addr(_Label1+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1549 :: 		Label1.Width           = 84;
MOVS	R1, #84
MOVW	R0, #lo_addr(_Label1+10)
MOVT	R0, #hi_addr(_Label1+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1550 :: 		Label1.Height          = 28;
MOVS	R1, #28
MOVW	R0, #lo_addr(_Label1+12)
MOVT	R0, #hi_addr(_Label1+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1551 :: 		Label1.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label1+27)
MOVT	R0, #hi_addr(_Label1+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1552 :: 		Label1.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label1+28)
MOVT	R0, #hi_addr(_Label1+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1553 :: 		Label1.Caption         = Label1_Caption;
MOVW	R1, #lo_addr(_Label1_Caption+0)
MOVT	R1, #hi_addr(_Label1_Caption+0)
MOVW	R0, #lo_addr(_Label1+16)
MOVT	R0, #hi_addr(_Label1+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,1554 :: 		Label1.FontName        = Tahoma21x25_Regular;
MOVW	R0, #lo_addr(_Label1+20)
MOVT	R0, #hi_addr(_Label1+20)
STR	R7, [R0, #0]
;BPWasher_driver.c,1555 :: 		Label1.Font_Color      = 0x07FF;
MOVW	R1, #2047
MOVW	R0, #lo_addr(_Label1+24)
MOVT	R0, #hi_addr(_Label1+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1556 :: 		Label1.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label1+26)
MOVT	R0, #hi_addr(_Label1+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1557 :: 		Label1.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label1+32)
MOVT	R0, #hi_addr(_Label1+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1558 :: 		Label1.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label1+36)
MOVT	R0, #hi_addr(_Label1+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,1559 :: 		Label1.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label1+40)
MOVT	R0, #hi_addr(_Label1+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,1560 :: 		Label1.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label1+44)
MOVT	R0, #hi_addr(_Label1+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,1562 :: 		Label2.OwnerScreen     = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_Label2+0)
MOVT	R0, #hi_addr(_Label2+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1563 :: 		Label2.Order           = 12;
MOVS	R1, #12
MOVW	R0, #lo_addr(_Label2+4)
MOVT	R0, #hi_addr(_Label2+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1564 :: 		Label2.Left            = 90;
MOVS	R1, #90
MOVW	R0, #lo_addr(_Label2+6)
MOVT	R0, #hi_addr(_Label2+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1565 :: 		Label2.Top             = 117;
MOVS	R1, #117
MOVW	R0, #lo_addr(_Label2+8)
MOVT	R0, #hi_addr(_Label2+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1566 :: 		Label2.Width           = 236;
MOVS	R1, #236
MOVW	R0, #lo_addr(_Label2+10)
MOVT	R0, #hi_addr(_Label2+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1567 :: 		Label2.Height          = 28;
MOVS	R1, #28
MOVW	R0, #lo_addr(_Label2+12)
MOVT	R0, #hi_addr(_Label2+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1568 :: 		Label2.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label2+27)
MOVT	R0, #hi_addr(_Label2+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1569 :: 		Label2.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label2+28)
MOVT	R0, #hi_addr(_Label2+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1570 :: 		Label2.Caption         = Label2_Caption;
MOVW	R1, #lo_addr(_Label2_Caption+0)
MOVT	R1, #hi_addr(_Label2_Caption+0)
MOVW	R0, #lo_addr(_Label2+16)
MOVT	R0, #hi_addr(_Label2+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,1571 :: 		Label2.FontName        = Tahoma21x25_Regular;
MOVW	R0, #lo_addr(_Label2+20)
MOVT	R0, #hi_addr(_Label2+20)
STR	R7, [R0, #0]
;BPWasher_driver.c,1572 :: 		Label2.Font_Color      = 0x07FF;
MOVW	R1, #2047
MOVW	R0, #lo_addr(_Label2+24)
MOVT	R0, #hi_addr(_Label2+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1573 :: 		Label2.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label2+26)
MOVT	R0, #hi_addr(_Label2+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1574 :: 		Label2.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label2+32)
MOVT	R0, #hi_addr(_Label2+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1575 :: 		Label2.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label2+36)
MOVT	R0, #hi_addr(_Label2+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,1576 :: 		Label2.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label2+40)
MOVT	R0, #hi_addr(_Label2+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,1577 :: 		Label2.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label2+44)
MOVT	R0, #hi_addr(_Label2+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,1579 :: 		Settings_temp.OwnerScreen     = &Settings;
MOVW	R1, #lo_addr(_Settings+0)
MOVT	R1, #hi_addr(_Settings+0)
MOVW	R0, #lo_addr(_Settings_temp+0)
MOVT	R0, #hi_addr(_Settings_temp+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1580 :: 		Settings_temp.Order           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_temp+4)
MOVT	R0, #hi_addr(_Settings_temp+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1581 :: 		Settings_temp.Left            = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_temp+6)
MOVT	R0, #hi_addr(_Settings_temp+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1582 :: 		Settings_temp.Top             = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_temp+8)
MOVT	R0, #hi_addr(_Settings_temp+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1583 :: 		Settings_temp.Width           = 106;
MOVS	R1, #106
MOVW	R0, #lo_addr(_Settings_temp+10)
MOVT	R0, #hi_addr(_Settings_temp+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1584 :: 		Settings_temp.Height          = 120;
MOVS	R1, #120
MOVW	R0, #lo_addr(_Settings_temp+12)
MOVT	R0, #hi_addr(_Settings_temp+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1585 :: 		Settings_temp.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_temp+14)
MOVT	R0, #hi_addr(_Settings_temp+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1586 :: 		Settings_temp.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_temp+16)
MOVT	R0, #hi_addr(_Settings_temp+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1587 :: 		Settings_temp.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_temp+18)
MOVT	R0, #hi_addr(_Settings_temp+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1588 :: 		Settings_temp.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_temp+19)
MOVT	R0, #hi_addr(_Settings_temp+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1589 :: 		Settings_temp.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_temp+20)
MOVT	R0, #hi_addr(_Settings_temp+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1590 :: 		Settings_temp.Caption         = Settings_temp_Caption;
MOVW	R1, #lo_addr(_Settings_temp_Caption+0)
MOVT	R1, #hi_addr(_Settings_temp_Caption+0)
MOVW	R0, #lo_addr(_Settings_temp+24)
MOVT	R0, #hi_addr(_Settings_temp+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1591 :: 		Settings_temp.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_temp+28)
MOVT	R0, #hi_addr(_Settings_temp+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1592 :: 		Settings_temp.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_temp+29)
MOVT	R0, #hi_addr(_Settings_temp+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1593 :: 		Settings_temp.FontName        = Tahoma34x42_Regular;
MOVW	R0, #lo_addr(_Settings_temp+32)
MOVT	R0, #hi_addr(_Settings_temp+32)
STR	R9, [R0, #0]
;BPWasher_driver.c,1594 :: 		Settings_temp.PressColEnabled = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_temp+49)
MOVT	R0, #hi_addr(_Settings_temp+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1595 :: 		Settings_temp.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_temp+36)
MOVT	R0, #hi_addr(_Settings_temp+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1596 :: 		Settings_temp.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_temp+38)
MOVT	R0, #hi_addr(_Settings_temp+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1597 :: 		Settings_temp.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_temp+39)
MOVT	R0, #hi_addr(_Settings_temp+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1598 :: 		Settings_temp.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_temp+40)
MOVT	R0, #hi_addr(_Settings_temp+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1599 :: 		Settings_temp.Gradient_Start_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Settings_temp+42)
MOVT	R0, #hi_addr(_Settings_temp+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1600 :: 		Settings_temp.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Settings_temp+44)
MOVT	R0, #hi_addr(_Settings_temp+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1601 :: 		Settings_temp.Color           = 0xFC00;
MOVW	R1, #64512
MOVW	R0, #lo_addr(_Settings_temp+46)
MOVT	R0, #hi_addr(_Settings_temp+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1602 :: 		Settings_temp.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Settings_temp+50)
MOVT	R0, #hi_addr(_Settings_temp+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1603 :: 		Settings_temp.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_Settings_temp+48)
MOVT	R0, #hi_addr(_Settings_temp+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1604 :: 		Settings_temp.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_temp+52)
MOVT	R0, #hi_addr(_Settings_temp+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1605 :: 		Settings_temp.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_temp+56)
MOVT	R0, #hi_addr(_Settings_temp+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1606 :: 		Settings_temp.OnClickPtr      = Settings_LogClick;
MOVW	R1, #lo_addr(_Settings_LogClick+0)
MOVT	R1, #hi_addr(_Settings_LogClick+0)
MOVW	R0, #lo_addr(_Settings_temp+60)
MOVT	R0, #hi_addr(_Settings_temp+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1607 :: 		Settings_temp.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_temp+64)
MOVT	R0, #hi_addr(_Settings_temp+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1609 :: 		Settings_PID.OwnerScreen     = &Settings;
MOVW	R1, #lo_addr(_Settings+0)
MOVT	R1, #hi_addr(_Settings+0)
MOVW	R0, #lo_addr(_Settings_PID+0)
MOVT	R0, #hi_addr(_Settings_PID+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1610 :: 		Settings_PID.Order           = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_PID+4)
MOVT	R0, #hi_addr(_Settings_PID+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1611 :: 		Settings_PID.Left            = 107;
MOVS	R1, #107
MOVW	R0, #lo_addr(_Settings_PID+6)
MOVT	R0, #hi_addr(_Settings_PID+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1612 :: 		Settings_PID.Top             = 121;
MOVS	R1, #121
MOVW	R0, #lo_addr(_Settings_PID+8)
MOVT	R0, #hi_addr(_Settings_PID+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1613 :: 		Settings_PID.Width           = 106;
MOVS	R1, #106
MOVW	R0, #lo_addr(_Settings_PID+10)
MOVT	R0, #hi_addr(_Settings_PID+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1614 :: 		Settings_PID.Height          = 120;
MOVS	R1, #120
MOVW	R0, #lo_addr(_Settings_PID+12)
MOVT	R0, #hi_addr(_Settings_PID+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1615 :: 		Settings_PID.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_PID+14)
MOVT	R0, #hi_addr(_Settings_PID+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1616 :: 		Settings_PID.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_PID+16)
MOVT	R0, #hi_addr(_Settings_PID+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1617 :: 		Settings_PID.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_PID+18)
MOVT	R0, #hi_addr(_Settings_PID+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1618 :: 		Settings_PID.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_PID+19)
MOVT	R0, #hi_addr(_Settings_PID+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1619 :: 		Settings_PID.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_PID+20)
MOVT	R0, #hi_addr(_Settings_PID+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1620 :: 		Settings_PID.Caption         = Settings_PID_Caption;
MOVW	R1, #lo_addr(_Settings_PID_Caption+0)
MOVT	R1, #hi_addr(_Settings_PID_Caption+0)
MOVW	R0, #lo_addr(_Settings_PID+24)
MOVT	R0, #hi_addr(_Settings_PID+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1621 :: 		Settings_PID.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_PID+28)
MOVT	R0, #hi_addr(_Settings_PID+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1622 :: 		Settings_PID.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_PID+29)
MOVT	R0, #hi_addr(_Settings_PID+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1623 :: 		Settings_PID.FontName        = Tahoma34x42_Regular;
MOVW	R0, #lo_addr(_Settings_PID+32)
MOVT	R0, #hi_addr(_Settings_PID+32)
STR	R9, [R0, #0]
;BPWasher_driver.c,1624 :: 		Settings_PID.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_PID+49)
MOVT	R0, #hi_addr(_Settings_PID+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1625 :: 		Settings_PID.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_PID+36)
MOVT	R0, #hi_addr(_Settings_PID+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1626 :: 		Settings_PID.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_PID+38)
MOVT	R0, #hi_addr(_Settings_PID+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1627 :: 		Settings_PID.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_PID+39)
MOVT	R0, #hi_addr(_Settings_PID+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1628 :: 		Settings_PID.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_PID+40)
MOVT	R0, #hi_addr(_Settings_PID+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1629 :: 		Settings_PID.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Settings_PID+42)
MOVT	R0, #hi_addr(_Settings_PID+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1630 :: 		Settings_PID.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Settings_PID+44)
MOVT	R0, #hi_addr(_Settings_PID+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1631 :: 		Settings_PID.Color           = 0x801F;
MOVW	R1, #32799
MOVW	R0, #lo_addr(_Settings_PID+46)
MOVT	R0, #hi_addr(_Settings_PID+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1632 :: 		Settings_PID.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Settings_PID+50)
MOVT	R0, #hi_addr(_Settings_PID+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1633 :: 		Settings_PID.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_Settings_PID+48)
MOVT	R0, #hi_addr(_Settings_PID+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1634 :: 		Settings_PID.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_PID+52)
MOVT	R0, #hi_addr(_Settings_PID+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1635 :: 		Settings_PID.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_PID+56)
MOVT	R0, #hi_addr(_Settings_PID+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1636 :: 		Settings_PID.OnClickPtr      = Settings_PIDClick;
MOVW	R1, #lo_addr(_Settings_PIDClick+0)
MOVT	R1, #hi_addr(_Settings_PIDClick+0)
MOVW	R0, #lo_addr(_Settings_PID+60)
MOVT	R0, #hi_addr(_Settings_PID+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1637 :: 		Settings_PID.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_PID+64)
MOVT	R0, #hi_addr(_Settings_PID+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1639 :: 		Settings_CAL.OwnerScreen     = &Settings;
MOVW	R1, #lo_addr(_Settings+0)
MOVT	R1, #hi_addr(_Settings+0)
MOVW	R0, #lo_addr(_Settings_CAL+0)
MOVT	R0, #hi_addr(_Settings_CAL+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1640 :: 		Settings_CAL.Order           = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_Settings_CAL+4)
MOVT	R0, #hi_addr(_Settings_CAL+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1641 :: 		Settings_CAL.Left            = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_CAL+6)
MOVT	R0, #hi_addr(_Settings_CAL+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1642 :: 		Settings_CAL.Top             = 121;
MOVS	R1, #121
MOVW	R0, #lo_addr(_Settings_CAL+8)
MOVT	R0, #hi_addr(_Settings_CAL+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1643 :: 		Settings_CAL.Width           = 106;
MOVS	R1, #106
MOVW	R0, #lo_addr(_Settings_CAL+10)
MOVT	R0, #hi_addr(_Settings_CAL+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1644 :: 		Settings_CAL.Height          = 120;
MOVS	R1, #120
MOVW	R0, #lo_addr(_Settings_CAL+12)
MOVT	R0, #hi_addr(_Settings_CAL+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1645 :: 		Settings_CAL.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_CAL+14)
MOVT	R0, #hi_addr(_Settings_CAL+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1646 :: 		Settings_CAL.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_CAL+16)
MOVT	R0, #hi_addr(_Settings_CAL+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1647 :: 		Settings_CAL.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_CAL+18)
MOVT	R0, #hi_addr(_Settings_CAL+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1648 :: 		Settings_CAL.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_CAL+19)
MOVT	R0, #hi_addr(_Settings_CAL+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1649 :: 		Settings_CAL.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_CAL+20)
MOVT	R0, #hi_addr(_Settings_CAL+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1650 :: 		Settings_CAL.Caption         = Settings_CAL_Caption;
MOVW	R1, #lo_addr(_Settings_CAL_Caption+0)
MOVT	R1, #hi_addr(_Settings_CAL_Caption+0)
MOVW	R0, #lo_addr(_Settings_CAL+24)
MOVT	R0, #hi_addr(_Settings_CAL+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1651 :: 		Settings_CAL.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_CAL+28)
MOVT	R0, #hi_addr(_Settings_CAL+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1652 :: 		Settings_CAL.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_CAL+29)
MOVT	R0, #hi_addr(_Settings_CAL+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1653 :: 		Settings_CAL.FontName        = Tahoma34x42_Regular;
MOVW	R0, #lo_addr(_Settings_CAL+32)
MOVT	R0, #hi_addr(_Settings_CAL+32)
STR	R9, [R0, #0]
;BPWasher_driver.c,1654 :: 		Settings_CAL.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_CAL+49)
MOVT	R0, #hi_addr(_Settings_CAL+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1655 :: 		Settings_CAL.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_CAL+36)
MOVT	R0, #hi_addr(_Settings_CAL+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1656 :: 		Settings_CAL.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_CAL+38)
MOVT	R0, #hi_addr(_Settings_CAL+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1657 :: 		Settings_CAL.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_CAL+39)
MOVT	R0, #hi_addr(_Settings_CAL+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1658 :: 		Settings_CAL.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_CAL+40)
MOVT	R0, #hi_addr(_Settings_CAL+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1659 :: 		Settings_CAL.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Settings_CAL+42)
MOVT	R0, #hi_addr(_Settings_CAL+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1660 :: 		Settings_CAL.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Settings_CAL+44)
MOVT	R0, #hi_addr(_Settings_CAL+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1661 :: 		Settings_CAL.Color           = 0x0400;
MOVW	R1, #1024
MOVW	R0, #lo_addr(_Settings_CAL+46)
MOVT	R0, #hi_addr(_Settings_CAL+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1662 :: 		Settings_CAL.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Settings_CAL+50)
MOVT	R0, #hi_addr(_Settings_CAL+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1663 :: 		Settings_CAL.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_Settings_CAL+48)
MOVT	R0, #hi_addr(_Settings_CAL+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1664 :: 		Settings_CAL.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_CAL+52)
MOVT	R0, #hi_addr(_Settings_CAL+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1665 :: 		Settings_CAL.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_CAL+56)
MOVT	R0, #hi_addr(_Settings_CAL+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1666 :: 		Settings_CAL.OnClickPtr      = Settings_CALClick;
MOVW	R1, #lo_addr(_Settings_CALClick+0)
MOVT	R1, #hi_addr(_Settings_CALClick+0)
MOVW	R0, #lo_addr(_Settings_CAL+60)
MOVT	R0, #hi_addr(_Settings_CAL+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1667 :: 		Settings_CAL.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_CAL+64)
MOVT	R0, #hi_addr(_Settings_CAL+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1669 :: 		Settings_Exit.OwnerScreen     = &Settings;
MOVW	R1, #lo_addr(_Settings+0)
MOVT	R1, #hi_addr(_Settings+0)
MOVW	R0, #lo_addr(_Settings_Exit+0)
MOVT	R0, #hi_addr(_Settings_Exit+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1670 :: 		Settings_Exit.Order           = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Settings_Exit+4)
MOVT	R0, #hi_addr(_Settings_Exit+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1671 :: 		Settings_Exit.Left            = 214;
MOVS	R1, #214
MOVW	R0, #lo_addr(_Settings_Exit+6)
MOVT	R0, #hi_addr(_Settings_Exit+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1672 :: 		Settings_Exit.Top             = 121;
MOVS	R1, #121
MOVW	R0, #lo_addr(_Settings_Exit+8)
MOVT	R0, #hi_addr(_Settings_Exit+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1673 :: 		Settings_Exit.Width           = 106;
MOVS	R1, #106
MOVW	R0, #lo_addr(_Settings_Exit+10)
MOVT	R0, #hi_addr(_Settings_Exit+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1674 :: 		Settings_Exit.Height          = 120;
MOVS	R1, #120
MOVW	R0, #lo_addr(_Settings_Exit+12)
MOVT	R0, #hi_addr(_Settings_Exit+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1675 :: 		Settings_Exit.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_Exit+14)
MOVT	R0, #hi_addr(_Settings_Exit+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1676 :: 		Settings_Exit.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_Exit+16)
MOVT	R0, #hi_addr(_Settings_Exit+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1677 :: 		Settings_Exit.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_Exit+18)
MOVT	R0, #hi_addr(_Settings_Exit+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1678 :: 		Settings_Exit.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_Exit+19)
MOVT	R0, #hi_addr(_Settings_Exit+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1679 :: 		Settings_Exit.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_Exit+20)
MOVT	R0, #hi_addr(_Settings_Exit+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1680 :: 		Settings_Exit.Caption         = Settings_Exit_Caption;
MOVW	R1, #lo_addr(_Settings_Exit_Caption+0)
MOVT	R1, #hi_addr(_Settings_Exit_Caption+0)
MOVW	R0, #lo_addr(_Settings_Exit+24)
MOVT	R0, #hi_addr(_Settings_Exit+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1681 :: 		Settings_Exit.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_Exit+28)
MOVT	R0, #hi_addr(_Settings_Exit+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1682 :: 		Settings_Exit.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_Exit+29)
MOVT	R0, #hi_addr(_Settings_Exit+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1683 :: 		Settings_Exit.FontName        = Tahoma34x42_Regular;
MOVW	R0, #lo_addr(_Settings_Exit+32)
MOVT	R0, #hi_addr(_Settings_Exit+32)
STR	R9, [R0, #0]
;BPWasher_driver.c,1684 :: 		Settings_Exit.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_Exit+49)
MOVT	R0, #hi_addr(_Settings_Exit+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1685 :: 		Settings_Exit.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_Exit+36)
MOVT	R0, #hi_addr(_Settings_Exit+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1686 :: 		Settings_Exit.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_Exit+38)
MOVT	R0, #hi_addr(_Settings_Exit+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1687 :: 		Settings_Exit.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_Exit+39)
MOVT	R0, #hi_addr(_Settings_Exit+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1688 :: 		Settings_Exit.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_Exit+40)
MOVT	R0, #hi_addr(_Settings_Exit+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1689 :: 		Settings_Exit.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Settings_Exit+42)
MOVT	R0, #hi_addr(_Settings_Exit+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1690 :: 		Settings_Exit.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Settings_Exit+44)
MOVT	R0, #hi_addr(_Settings_Exit+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1691 :: 		Settings_Exit.Color           = 0xF800;
MOVW	R1, #63488
MOVW	R0, #lo_addr(_Settings_Exit+46)
MOVT	R0, #hi_addr(_Settings_Exit+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1692 :: 		Settings_Exit.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Settings_Exit+50)
MOVT	R0, #hi_addr(_Settings_Exit+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1693 :: 		Settings_Exit.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_Settings_Exit+48)
MOVT	R0, #hi_addr(_Settings_Exit+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1694 :: 		Settings_Exit.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_Exit+52)
MOVT	R0, #hi_addr(_Settings_Exit+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1695 :: 		Settings_Exit.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_Exit+56)
MOVT	R0, #hi_addr(_Settings_Exit+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1696 :: 		Settings_Exit.OnClickPtr      = Settings_ExitClick;
MOVW	R1, #lo_addr(_Settings_ExitClick+0)
MOVT	R1, #hi_addr(_Settings_ExitClick+0)
MOVW	R0, #lo_addr(_Settings_Exit+60)
MOVT	R0, #hi_addr(_Settings_Exit+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1697 :: 		Settings_Exit.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_Exit+64)
MOVT	R0, #hi_addr(_Settings_Exit+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1699 :: 		ToolBox.OwnerScreen     = &Settings;
MOVW	R1, #lo_addr(_Settings+0)
MOVT	R1, #hi_addr(_Settings+0)
MOVW	R0, #lo_addr(_ToolBox+0)
MOVT	R0, #hi_addr(_ToolBox+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1700 :: 		ToolBox.Order           = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_ToolBox+4)
MOVT	R0, #hi_addr(_ToolBox+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1701 :: 		ToolBox.Left            = 214;
MOVS	R1, #214
MOVW	R0, #lo_addr(_ToolBox+6)
MOVT	R0, #hi_addr(_ToolBox+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1702 :: 		ToolBox.Top             = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ToolBox+8)
MOVT	R0, #hi_addr(_ToolBox+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1703 :: 		ToolBox.Width           = 106;
MOVS	R1, #106
MOVW	R0, #lo_addr(_ToolBox+10)
MOVT	R0, #hi_addr(_ToolBox+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1704 :: 		ToolBox.Height          = 120;
MOVS	R1, #120
MOVW	R0, #lo_addr(_ToolBox+12)
MOVT	R0, #hi_addr(_ToolBox+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1705 :: 		ToolBox.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ToolBox+14)
MOVT	R0, #hi_addr(_ToolBox+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1706 :: 		ToolBox.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ToolBox+16)
MOVT	R0, #hi_addr(_ToolBox+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1707 :: 		ToolBox.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ToolBox+18)
MOVT	R0, #hi_addr(_ToolBox+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1708 :: 		ToolBox.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ToolBox+19)
MOVT	R0, #hi_addr(_ToolBox+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1709 :: 		ToolBox.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ToolBox+20)
MOVT	R0, #hi_addr(_ToolBox+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1710 :: 		ToolBox.Caption         = ToolBox_Caption;
MOVW	R1, #lo_addr(_ToolBox_Caption+0)
MOVT	R1, #hi_addr(_ToolBox_Caption+0)
MOVW	R0, #lo_addr(_ToolBox+24)
MOVT	R0, #hi_addr(_ToolBox+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1711 :: 		ToolBox.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ToolBox+28)
MOVT	R0, #hi_addr(_ToolBox+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1712 :: 		ToolBox.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ToolBox+29)
MOVT	R0, #hi_addr(_ToolBox+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1713 :: 		ToolBox.FontName        = Tahoma34x42_Regular;
MOVW	R0, #lo_addr(_ToolBox+32)
MOVT	R0, #hi_addr(_ToolBox+32)
STR	R9, [R0, #0]
;BPWasher_driver.c,1714 :: 		ToolBox.PressColEnabled = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ToolBox+49)
MOVT	R0, #hi_addr(_ToolBox+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1715 :: 		ToolBox.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ToolBox+36)
MOVT	R0, #hi_addr(_ToolBox+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1716 :: 		ToolBox.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ToolBox+38)
MOVT	R0, #hi_addr(_ToolBox+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1717 :: 		ToolBox.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ToolBox+39)
MOVT	R0, #hi_addr(_ToolBox+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1718 :: 		ToolBox.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ToolBox+40)
MOVT	R0, #hi_addr(_ToolBox+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1719 :: 		ToolBox.Gradient_Start_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_ToolBox+42)
MOVT	R0, #hi_addr(_ToolBox+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1720 :: 		ToolBox.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_ToolBox+44)
MOVT	R0, #hi_addr(_ToolBox+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1721 :: 		ToolBox.Color           = 0x041F;
MOVW	R1, #1055
MOVW	R0, #lo_addr(_ToolBox+46)
MOVT	R0, #hi_addr(_ToolBox+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1722 :: 		ToolBox.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_ToolBox+50)
MOVT	R0, #hi_addr(_ToolBox+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1723 :: 		ToolBox.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_ToolBox+48)
MOVT	R0, #hi_addr(_ToolBox+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1724 :: 		ToolBox.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ToolBox+52)
MOVT	R0, #hi_addr(_ToolBox+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1725 :: 		ToolBox.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ToolBox+56)
MOVT	R0, #hi_addr(_ToolBox+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1726 :: 		ToolBox.OnClickPtr      = Diag_Clicked;
MOVW	R1, #lo_addr(_Diag_Clicked+0)
MOVT	R1, #hi_addr(_Diag_Clicked+0)
MOVW	R0, #lo_addr(_ToolBox+60)
MOVT	R0, #hi_addr(_ToolBox+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1727 :: 		ToolBox.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ToolBox+64)
MOVT	R0, #hi_addr(_ToolBox+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1729 :: 		Settings_drain.OwnerScreen     = &Settings;
MOVW	R1, #lo_addr(_Settings+0)
MOVT	R1, #hi_addr(_Settings+0)
MOVW	R0, #lo_addr(_Settings_drain+0)
MOVT	R0, #hi_addr(_Settings_drain+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1730 :: 		Settings_drain.Order           = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_Settings_drain+4)
MOVT	R0, #hi_addr(_Settings_drain+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1731 :: 		Settings_drain.Left            = 107;
MOVS	R1, #107
MOVW	R0, #lo_addr(_Settings_drain+6)
MOVT	R0, #hi_addr(_Settings_drain+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1732 :: 		Settings_drain.Top             = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_drain+8)
MOVT	R0, #hi_addr(_Settings_drain+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1733 :: 		Settings_drain.Width           = 106;
MOVS	R1, #106
MOVW	R0, #lo_addr(_Settings_drain+10)
MOVT	R0, #hi_addr(_Settings_drain+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1734 :: 		Settings_drain.Height          = 120;
MOVS	R1, #120
MOVW	R0, #lo_addr(_Settings_drain+12)
MOVT	R0, #hi_addr(_Settings_drain+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1735 :: 		Settings_drain.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_drain+14)
MOVT	R0, #hi_addr(_Settings_drain+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1736 :: 		Settings_drain.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_drain+16)
MOVT	R0, #hi_addr(_Settings_drain+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1737 :: 		Settings_drain.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_drain+18)
MOVT	R0, #hi_addr(_Settings_drain+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1738 :: 		Settings_drain.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_drain+19)
MOVT	R0, #hi_addr(_Settings_drain+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1739 :: 		Settings_drain.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_drain+20)
MOVT	R0, #hi_addr(_Settings_drain+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1740 :: 		Settings_drain.Caption         = Settings_drain_Caption;
MOVW	R1, #lo_addr(_Settings_drain_Caption+0)
MOVT	R1, #hi_addr(_Settings_drain_Caption+0)
MOVW	R0, #lo_addr(_Settings_drain+24)
MOVT	R0, #hi_addr(_Settings_drain+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1741 :: 		Settings_drain.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_drain+28)
MOVT	R0, #hi_addr(_Settings_drain+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1742 :: 		Settings_drain.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_drain+29)
MOVT	R0, #hi_addr(_Settings_drain+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1743 :: 		Settings_drain.FontName        = Tahoma26x33_Regular;
MOVW	R0, #lo_addr(_Settings_drain+32)
MOVT	R0, #hi_addr(_Settings_drain+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,1744 :: 		Settings_drain.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Settings_drain+49)
MOVT	R0, #hi_addr(_Settings_drain+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1745 :: 		Settings_drain.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_drain+36)
MOVT	R0, #hi_addr(_Settings_drain+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1746 :: 		Settings_drain.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_drain+38)
MOVT	R0, #hi_addr(_Settings_drain+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1747 :: 		Settings_drain.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_drain+39)
MOVT	R0, #hi_addr(_Settings_drain+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1748 :: 		Settings_drain.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_drain+40)
MOVT	R0, #hi_addr(_Settings_drain+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1749 :: 		Settings_drain.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Settings_drain+42)
MOVT	R0, #hi_addr(_Settings_drain+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1750 :: 		Settings_drain.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Settings_drain+44)
MOVT	R0, #hi_addr(_Settings_drain+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1751 :: 		Settings_drain.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Settings_drain+46)
MOVT	R0, #hi_addr(_Settings_drain+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1752 :: 		Settings_drain.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Settings_drain+50)
MOVT	R0, #hi_addr(_Settings_drain+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1753 :: 		Settings_drain.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_Settings_drain+48)
MOVT	R0, #hi_addr(_Settings_drain+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1754 :: 		Settings_drain.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_drain+52)
MOVT	R0, #hi_addr(_Settings_drain+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1755 :: 		Settings_drain.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_drain+56)
MOVT	R0, #hi_addr(_Settings_drain+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1756 :: 		Settings_drain.OnClickPtr      = Config_Clicked;
MOVW	R1, #lo_addr(_Config_Clicked+0)
MOVT	R1, #hi_addr(_Config_Clicked+0)
MOVW	R0, #lo_addr(_Settings_drain+60)
MOVT	R0, #hi_addr(_Settings_drain+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1757 :: 		Settings_drain.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Settings_drain+64)
MOVT	R0, #hi_addr(_Settings_drain+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1759 :: 		P_value.OwnerScreen     = &PID;
MOVW	R1, #lo_addr(_PID+0)
MOVT	R1, #hi_addr(_PID+0)
MOVW	R0, #lo_addr(_P_value+0)
MOVT	R0, #hi_addr(_P_value+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1760 :: 		P_value.Order           = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_P_value+4)
MOVT	R0, #hi_addr(_P_value+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1761 :: 		P_value.Left            = 86; //112;
MOVS	R1, #86
MOVW	R0, #lo_addr(_P_value+6)
MOVT	R0, #hi_addr(_P_value+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1762 :: 		P_value.Top             = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_P_value+8)
MOVT	R0, #hi_addr(_P_value+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1763 :: 		P_value.Width           = 149; //101;
MOVS	R1, #149
MOVW	R0, #lo_addr(_P_value+10)
MOVT	R0, #hi_addr(_P_value+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1764 :: 		P_value.Height          = 77;
MOVS	R1, #77
MOVW	R0, #lo_addr(_P_value+12)
MOVT	R0, #hi_addr(_P_value+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1765 :: 		P_value.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_P_value+14)
MOVT	R0, #hi_addr(_P_value+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1766 :: 		P_value.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_P_value+16)
MOVT	R0, #hi_addr(_P_value+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1767 :: 		P_value.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_P_value+18)
MOVT	R0, #hi_addr(_P_value+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1768 :: 		P_value.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_P_value+19)
MOVT	R0, #hi_addr(_P_value+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1769 :: 		P_value.Transparent     = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_P_value+20)
MOVT	R0, #hi_addr(_P_value+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1770 :: 		P_value.Caption         = P_value_Caption;
MOVW	R1, #lo_addr(_P_value_Caption+0)
MOVT	R1, #hi_addr(_P_value_Caption+0)
MOVW	R0, #lo_addr(_P_value+24)
MOVT	R0, #hi_addr(_P_value+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1771 :: 		P_value.TextAlign       = _taRight;
MOVS	R1, #2
MOVW	R0, #lo_addr(_P_value+28)
MOVT	R0, #hi_addr(_P_value+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1772 :: 		P_value.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_P_value+29)
MOVT	R0, #hi_addr(_P_value+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1773 :: 		P_value.FontName        = Tahoma55x68_Regular;
MOVW	R3, #lo_addr(_Tahoma55x68_Regular+0)
MOVT	R3, #hi_addr(_Tahoma55x68_Regular+0)
MOVW	R0, #lo_addr(_P_value+32)
MOVT	R0, #hi_addr(_P_value+32)
STR	R3, [R0, #0]
;BPWasher_driver.c,1774 :: 		P_value.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_P_value+49)
MOVT	R0, #hi_addr(_P_value+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1775 :: 		P_value.Font_Color      = 0xFFE0;
MOVW	R1, #65504
MOVW	R0, #lo_addr(_P_value+36)
MOVT	R0, #hi_addr(_P_value+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1776 :: 		P_value.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_P_value+38)
MOVT	R0, #hi_addr(_P_value+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1777 :: 		P_value.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_P_value+39)
MOVT	R0, #hi_addr(_P_value+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1778 :: 		P_value.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_P_value+40)
MOVT	R0, #hi_addr(_P_value+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1779 :: 		P_value.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_P_value+42)
MOVT	R0, #hi_addr(_P_value+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1780 :: 		P_value.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_P_value+44)
MOVT	R0, #hi_addr(_P_value+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1781 :: 		P_value.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_P_value+46)
MOVT	R0, #hi_addr(_P_value+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1782 :: 		P_value.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_P_value+50)
MOVT	R0, #hi_addr(_P_value+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1783 :: 		P_value.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_P_value+48)
MOVT	R0, #hi_addr(_P_value+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1784 :: 		P_value.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_P_value+52)
MOVT	R0, #hi_addr(_P_value+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1785 :: 		P_value.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_P_value+56)
MOVT	R0, #hi_addr(_P_value+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1786 :: 		P_value.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_P_value+60)
MOVT	R0, #hi_addr(_P_value+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1787 :: 		P_value.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_P_value+64)
MOVT	R0, #hi_addr(_P_value+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1789 :: 		I_value.OwnerScreen     = &PID;
MOVW	R1, #lo_addr(_PID+0)
MOVT	R1, #hi_addr(_PID+0)
MOVW	R0, #lo_addr(_I_value+0)
MOVT	R0, #hi_addr(_I_value+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1790 :: 		I_value.Order           = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_I_value+4)
MOVT	R0, #hi_addr(_I_value+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1791 :: 		I_value.Left            = 86; //125;
MOVS	R1, #86
MOVW	R0, #lo_addr(_I_value+6)
MOVT	R0, #hi_addr(_I_value+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1792 :: 		I_value.Top             = 84;
MOVS	R1, #84
MOVW	R0, #lo_addr(_I_value+8)
MOVT	R0, #hi_addr(_I_value+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1793 :: 		I_value.Width           = 149; //101;
MOVS	R1, #149
MOVW	R0, #lo_addr(_I_value+10)
MOVT	R0, #hi_addr(_I_value+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1794 :: 		I_value.Height          = 77;
MOVS	R1, #77
MOVW	R0, #lo_addr(_I_value+12)
MOVT	R0, #hi_addr(_I_value+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1795 :: 		I_value.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_I_value+14)
MOVT	R0, #hi_addr(_I_value+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1796 :: 		I_value.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_value+16)
MOVT	R0, #hi_addr(_I_value+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1797 :: 		I_value.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_I_value+18)
MOVT	R0, #hi_addr(_I_value+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1798 :: 		I_value.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_value+19)
MOVT	R0, #hi_addr(_I_value+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1799 :: 		I_value.Transparent     = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_value+20)
MOVT	R0, #hi_addr(_I_value+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1800 :: 		I_value.Caption         = I_value_Caption;
MOVW	R1, #lo_addr(_I_value_Caption+0)
MOVT	R1, #hi_addr(_I_value_Caption+0)
MOVW	R0, #lo_addr(_I_value+24)
MOVT	R0, #hi_addr(_I_value+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1801 :: 		I_value.TextAlign       = _taRight;
MOVS	R1, #2
MOVW	R0, #lo_addr(_I_value+28)
MOVT	R0, #hi_addr(_I_value+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1802 :: 		I_value.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_I_value+29)
MOVT	R0, #hi_addr(_I_value+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1803 :: 		I_value.FontName        = Tahoma55x68_Regular;
MOVW	R0, #lo_addr(_I_value+32)
MOVT	R0, #hi_addr(_I_value+32)
STR	R3, [R0, #0]
;BPWasher_driver.c,1804 :: 		I_value.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_I_value+49)
MOVT	R0, #hi_addr(_I_value+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1805 :: 		I_value.Font_Color      = 0xFFE0;
MOVW	R1, #65504
MOVW	R0, #lo_addr(_I_value+36)
MOVT	R0, #hi_addr(_I_value+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1806 :: 		I_value.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_value+38)
MOVT	R0, #hi_addr(_I_value+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1807 :: 		I_value.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_value+39)
MOVT	R0, #hi_addr(_I_value+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1808 :: 		I_value.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_value+40)
MOVT	R0, #hi_addr(_I_value+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1809 :: 		I_value.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_I_value+42)
MOVT	R0, #hi_addr(_I_value+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1810 :: 		I_value.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_I_value+44)
MOVT	R0, #hi_addr(_I_value+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1811 :: 		I_value.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_I_value+46)
MOVT	R0, #hi_addr(_I_value+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1812 :: 		I_value.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_I_value+50)
MOVT	R0, #hi_addr(_I_value+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1813 :: 		I_value.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_I_value+48)
MOVT	R0, #hi_addr(_I_value+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1814 :: 		I_value.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_value+52)
MOVT	R0, #hi_addr(_I_value+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1815 :: 		I_value.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_value+56)
MOVT	R0, #hi_addr(_I_value+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1816 :: 		I_value.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_value+60)
MOVT	R0, #hi_addr(_I_value+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1817 :: 		I_value.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_value+64)
MOVT	R0, #hi_addr(_I_value+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1819 :: 		D_value.OwnerScreen     = &PID;
MOVW	R1, #lo_addr(_PID+0)
MOVT	R1, #hi_addr(_PID+0)
MOVW	R0, #lo_addr(_D_value+0)
MOVT	R0, #hi_addr(_D_value+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1820 :: 		D_value.Order           = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_D_value+4)
MOVT	R0, #hi_addr(_D_value+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1821 :: 		D_value.Left            = 86; //117;
MOVS	R1, #86
MOVW	R0, #lo_addr(_D_value+6)
MOVT	R0, #hi_addr(_D_value+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1822 :: 		D_value.Top             = 162;
MOVS	R1, #162
MOVW	R0, #lo_addr(_D_value+8)
MOVT	R0, #hi_addr(_D_value+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1823 :: 		D_value.Width           = 149; //118;
MOVS	R1, #149
MOVW	R0, #lo_addr(_D_value+10)
MOVT	R0, #hi_addr(_D_value+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1824 :: 		D_value.Height          = 77;
MOVS	R1, #77
MOVW	R0, #lo_addr(_D_value+12)
MOVT	R0, #hi_addr(_D_value+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1825 :: 		D_value.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_D_value+14)
MOVT	R0, #hi_addr(_D_value+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1826 :: 		D_value.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_value+16)
MOVT	R0, #hi_addr(_D_value+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1827 :: 		D_value.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_D_value+18)
MOVT	R0, #hi_addr(_D_value+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1828 :: 		D_value.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_value+19)
MOVT	R0, #hi_addr(_D_value+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1829 :: 		D_value.Transparent     = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_value+20)
MOVT	R0, #hi_addr(_D_value+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1830 :: 		D_value.Caption         = D_value_Caption;
MOVW	R1, #lo_addr(_D_value_Caption+0)
MOVT	R1, #hi_addr(_D_value_Caption+0)
MOVW	R0, #lo_addr(_D_value+24)
MOVT	R0, #hi_addr(_D_value+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1831 :: 		D_value.TextAlign       = _taRight;
MOVS	R1, #2
MOVW	R0, #lo_addr(_D_value+28)
MOVT	R0, #hi_addr(_D_value+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1832 :: 		D_value.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_D_value+29)
MOVT	R0, #hi_addr(_D_value+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1833 :: 		D_value.FontName        = Tahoma55x68_Regular;
MOVW	R0, #lo_addr(_D_value+32)
MOVT	R0, #hi_addr(_D_value+32)
STR	R3, [R0, #0]
;BPWasher_driver.c,1834 :: 		D_value.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_D_value+49)
MOVT	R0, #hi_addr(_D_value+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1835 :: 		D_value.Font_Color      = 0xFFE0;
MOVW	R1, #65504
MOVW	R0, #lo_addr(_D_value+36)
MOVT	R0, #hi_addr(_D_value+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1836 :: 		D_value.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_value+38)
MOVT	R0, #hi_addr(_D_value+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1837 :: 		D_value.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_value+39)
MOVT	R0, #hi_addr(_D_value+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1838 :: 		D_value.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_value+40)
MOVT	R0, #hi_addr(_D_value+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1839 :: 		D_value.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_D_value+42)
MOVT	R0, #hi_addr(_D_value+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1840 :: 		D_value.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_D_value+44)
MOVT	R0, #hi_addr(_D_value+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1841 :: 		D_value.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_D_value+46)
MOVT	R0, #hi_addr(_D_value+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1842 :: 		D_value.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_D_value+50)
MOVT	R0, #hi_addr(_D_value+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1843 :: 		D_value.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_D_value+48)
MOVT	R0, #hi_addr(_D_value+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1844 :: 		D_value.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_value+52)
MOVT	R0, #hi_addr(_D_value+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1845 :: 		D_value.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_value+56)
MOVT	R0, #hi_addr(_D_value+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1846 :: 		D_value.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_value+60)
MOVT	R0, #hi_addr(_D_value+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1847 :: 		D_value.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_value+64)
MOVT	R0, #hi_addr(_D_value+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1849 :: 		I_button.OwnerScreen     = &PID;
MOVW	R1, #lo_addr(_PID+0)
MOVT	R1, #hi_addr(_PID+0)
MOVW	R0, #lo_addr(_I_button+0)
MOVT	R0, #hi_addr(_I_button+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1850 :: 		I_button.Order           = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_I_button+4)
MOVT	R0, #hi_addr(_I_button+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1851 :: 		I_button.Left            = 7;
MOVS	R1, #7
MOVW	R0, #lo_addr(_I_button+6)
MOVT	R0, #hi_addr(_I_button+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1852 :: 		I_button.Top             = 83;
MOVS	R1, #83
MOVW	R0, #lo_addr(_I_button+8)
MOVT	R0, #hi_addr(_I_button+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1853 :: 		I_button.Width           = 74;
MOVS	R1, #74
MOVW	R0, #lo_addr(_I_button+10)
MOVT	R0, #hi_addr(_I_button+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1854 :: 		I_button.Height          = 70;
MOVS	R1, #70
MOVW	R0, #lo_addr(_I_button+12)
MOVT	R0, #hi_addr(_I_button+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1855 :: 		I_button.Pen_Width       = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_I_button+14)
MOVT	R0, #hi_addr(_I_button+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1856 :: 		I_button.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_I_button+16)
MOVT	R0, #hi_addr(_I_button+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1857 :: 		I_button.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_I_button+18)
MOVT	R0, #hi_addr(_I_button+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1858 :: 		I_button.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_I_button+19)
MOVT	R0, #hi_addr(_I_button+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1859 :: 		I_button.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_I_button+20)
MOVT	R0, #hi_addr(_I_button+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1860 :: 		I_button.Caption         = I_button_Caption;
MOVW	R1, #lo_addr(_I_button_Caption+0)
MOVT	R1, #hi_addr(_I_button_Caption+0)
MOVW	R0, #lo_addr(_I_button+24)
MOVT	R0, #hi_addr(_I_button+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1861 :: 		I_button.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_I_button+28)
MOVT	R0, #hi_addr(_I_button+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1862 :: 		I_button.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_I_button+29)
MOVT	R0, #hi_addr(_I_button+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1863 :: 		I_button.FontName        = Tahoma50x62_Regular;
MOVW	R2, #lo_addr(_Tahoma50x62_Regular+0)
MOVT	R2, #hi_addr(_Tahoma50x62_Regular+0)
MOVW	R0, #lo_addr(_I_button+32)
MOVT	R0, #hi_addr(_I_button+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,1864 :: 		I_button.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_I_button+49)
MOVT	R0, #hi_addr(_I_button+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1865 :: 		I_button.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_button+36)
MOVT	R0, #hi_addr(_I_button+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1866 :: 		I_button.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_button+38)
MOVT	R0, #hi_addr(_I_button+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1867 :: 		I_button.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_button+39)
MOVT	R0, #hi_addr(_I_button+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1868 :: 		I_button.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_button+40)
MOVT	R0, #hi_addr(_I_button+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1869 :: 		I_button.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_I_button+42)
MOVT	R0, #hi_addr(_I_button+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1870 :: 		I_button.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_I_button+44)
MOVT	R0, #hi_addr(_I_button+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1871 :: 		I_button.Color           = 0x87FF;
MOVW	R1, #34815
MOVW	R0, #lo_addr(_I_button+46)
MOVT	R0, #hi_addr(_I_button+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1872 :: 		I_button.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_I_button+50)
MOVT	R0, #hi_addr(_I_button+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1873 :: 		I_button.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_I_button+48)
MOVT	R0, #hi_addr(_I_button+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1874 :: 		I_button.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_button+52)
MOVT	R0, #hi_addr(_I_button+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1875 :: 		I_button.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_button+56)
MOVT	R0, #hi_addr(_I_button+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1876 :: 		I_button.OnClickPtr      = I_buttonClick;
MOVW	R1, #lo_addr(_I_buttonClick+0)
MOVT	R1, #hi_addr(_I_buttonClick+0)
MOVW	R0, #lo_addr(_I_button+60)
MOVT	R0, #hi_addr(_I_button+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1877 :: 		I_button.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_I_button+64)
MOVT	R0, #hi_addr(_I_button+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1879 :: 		D_button.OwnerScreen     = &PID;
MOVW	R1, #lo_addr(_PID+0)
MOVT	R1, #hi_addr(_PID+0)
MOVW	R0, #lo_addr(_D_button+0)
MOVT	R0, #hi_addr(_D_button+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1880 :: 		D_button.Order           = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_D_button+4)
MOVT	R0, #hi_addr(_D_button+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1881 :: 		D_button.Left            = 6;
MOVS	R1, #6
MOVW	R0, #lo_addr(_D_button+6)
MOVT	R0, #hi_addr(_D_button+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1882 :: 		D_button.Top             = 163;
MOVS	R1, #163
MOVW	R0, #lo_addr(_D_button+8)
MOVT	R0, #hi_addr(_D_button+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1883 :: 		D_button.Width           = 74;
MOVS	R1, #74
MOVW	R0, #lo_addr(_D_button+10)
MOVT	R0, #hi_addr(_D_button+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1884 :: 		D_button.Height          = 70;
MOVS	R1, #70
MOVW	R0, #lo_addr(_D_button+12)
MOVT	R0, #hi_addr(_D_button+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1885 :: 		D_button.Pen_Width       = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_D_button+14)
MOVT	R0, #hi_addr(_D_button+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1886 :: 		D_button.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_D_button+16)
MOVT	R0, #hi_addr(_D_button+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1887 :: 		D_button.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_D_button+18)
MOVT	R0, #hi_addr(_D_button+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1888 :: 		D_button.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_D_button+19)
MOVT	R0, #hi_addr(_D_button+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1889 :: 		D_button.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_D_button+20)
MOVT	R0, #hi_addr(_D_button+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1890 :: 		D_button.Caption         = D_button_Caption;
MOVW	R1, #lo_addr(_D_button_Caption+0)
MOVT	R1, #hi_addr(_D_button_Caption+0)
MOVW	R0, #lo_addr(_D_button+24)
MOVT	R0, #hi_addr(_D_button+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1891 :: 		D_button.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_D_button+28)
MOVT	R0, #hi_addr(_D_button+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1892 :: 		D_button.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_D_button+29)
MOVT	R0, #hi_addr(_D_button+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1893 :: 		D_button.FontName        = Tahoma50x62_Regular;
MOVW	R0, #lo_addr(_D_button+32)
MOVT	R0, #hi_addr(_D_button+32)
STR	R2, [R0, #0]
;BPWasher_driver.c,1894 :: 		D_button.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_D_button+49)
MOVT	R0, #hi_addr(_D_button+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1895 :: 		D_button.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_button+36)
MOVT	R0, #hi_addr(_D_button+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1896 :: 		D_button.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_button+38)
MOVT	R0, #hi_addr(_D_button+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1897 :: 		D_button.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_button+39)
MOVT	R0, #hi_addr(_D_button+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1898 :: 		D_button.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_button+40)
MOVT	R0, #hi_addr(_D_button+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1899 :: 		D_button.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_D_button+42)
MOVT	R0, #hi_addr(_D_button+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1900 :: 		D_button.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_D_button+44)
MOVT	R0, #hi_addr(_D_button+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1901 :: 		D_button.Color           = 0xFC08;
MOVW	R1, #64520
MOVW	R0, #lo_addr(_D_button+46)
MOVT	R0, #hi_addr(_D_button+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1902 :: 		D_button.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_D_button+50)
MOVT	R0, #hi_addr(_D_button+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1903 :: 		D_button.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_D_button+48)
MOVT	R0, #hi_addr(_D_button+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1904 :: 		D_button.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_button+52)
MOVT	R0, #hi_addr(_D_button+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1905 :: 		D_button.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_button+56)
MOVT	R0, #hi_addr(_D_button+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1906 :: 		D_button.OnClickPtr      = D_buttonClick;
MOVW	R1, #lo_addr(_D_buttonClick+0)
MOVT	R1, #hi_addr(_D_buttonClick+0)
MOVW	R0, #lo_addr(_D_button+60)
MOVT	R0, #hi_addr(_D_button+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1907 :: 		D_button.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_D_button+64)
MOVT	R0, #hi_addr(_D_button+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1909 :: 		PID_conf_button.OwnerScreen     = &PID;
MOVW	R1, #lo_addr(_PID+0)
MOVT	R1, #hi_addr(_PID+0)
MOVW	R0, #lo_addr(_PID_conf_button+0)
MOVT	R0, #hi_addr(_PID_conf_button+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1910 :: 		PID_conf_button.Order           = 6;
MOVS	R1, #6
MOVW	R0, #lo_addr(_PID_conf_button+4)
MOVT	R0, #hi_addr(_PID_conf_button+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1911 :: 		PID_conf_button.Left            = 239;
MOVS	R1, #239
MOVW	R0, #lo_addr(_PID_conf_button+6)
MOVT	R0, #hi_addr(_PID_conf_button+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1912 :: 		PID_conf_button.Top             = 163;
MOVS	R1, #163
MOVW	R0, #lo_addr(_PID_conf_button+8)
MOVT	R0, #hi_addr(_PID_conf_button+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1913 :: 		PID_conf_button.Width           = 74;
MOVS	R1, #74
MOVW	R0, #lo_addr(_PID_conf_button+10)
MOVT	R0, #hi_addr(_PID_conf_button+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1914 :: 		PID_conf_button.Height          = 70;
MOVS	R1, #70
MOVW	R0, #lo_addr(_PID_conf_button+12)
MOVT	R0, #hi_addr(_PID_conf_button+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1915 :: 		PID_conf_button.Pen_Width       = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_PID_conf_button+14)
MOVT	R0, #hi_addr(_PID_conf_button+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1916 :: 		PID_conf_button.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_PID_conf_button+16)
MOVT	R0, #hi_addr(_PID_conf_button+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1917 :: 		PID_conf_button.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID_conf_button+18)
MOVT	R0, #hi_addr(_PID_conf_button+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1918 :: 		PID_conf_button.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID_conf_button+19)
MOVT	R0, #hi_addr(_PID_conf_button+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1919 :: 		PID_conf_button.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID_conf_button+20)
MOVT	R0, #hi_addr(_PID_conf_button+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1920 :: 		PID_conf_button.Caption         = PID_conf_button_Caption;
MOVW	R1, #lo_addr(_PID_conf_button_Caption+0)
MOVT	R1, #hi_addr(_PID_conf_button_Caption+0)
MOVW	R0, #lo_addr(_PID_conf_button+24)
MOVT	R0, #hi_addr(_PID_conf_button+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1921 :: 		PID_conf_button.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID_conf_button+28)
MOVT	R0, #hi_addr(_PID_conf_button+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1922 :: 		PID_conf_button.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID_conf_button+29)
MOVT	R0, #hi_addr(_PID_conf_button+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1923 :: 		PID_conf_button.FontName        = Tahoma42x52_Regular;
MOVW	R6, #lo_addr(_Tahoma42x52_Regular+0)
MOVT	R6, #hi_addr(_Tahoma42x52_Regular+0)
MOVW	R0, #lo_addr(_PID_conf_button+32)
MOVT	R0, #hi_addr(_PID_conf_button+32)
STR	R6, [R0, #0]
;BPWasher_driver.c,1924 :: 		PID_conf_button.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID_conf_button+49)
MOVT	R0, #hi_addr(_PID_conf_button+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1925 :: 		PID_conf_button.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_conf_button+36)
MOVT	R0, #hi_addr(_PID_conf_button+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1926 :: 		PID_conf_button.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_conf_button+38)
MOVT	R0, #hi_addr(_PID_conf_button+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1927 :: 		PID_conf_button.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_conf_button+39)
MOVT	R0, #hi_addr(_PID_conf_button+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1928 :: 		PID_conf_button.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_conf_button+40)
MOVT	R0, #hi_addr(_PID_conf_button+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1929 :: 		PID_conf_button.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_PID_conf_button+42)
MOVT	R0, #hi_addr(_PID_conf_button+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1930 :: 		PID_conf_button.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_PID_conf_button+44)
MOVT	R0, #hi_addr(_PID_conf_button+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1931 :: 		PID_conf_button.Color           = 0x0400;
MOVW	R1, #1024
MOVW	R0, #lo_addr(_PID_conf_button+46)
MOVT	R0, #hi_addr(_PID_conf_button+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1932 :: 		PID_conf_button.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_PID_conf_button+50)
MOVT	R0, #hi_addr(_PID_conf_button+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1933 :: 		PID_conf_button.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_PID_conf_button+48)
MOVT	R0, #hi_addr(_PID_conf_button+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1934 :: 		PID_conf_button.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_conf_button+52)
MOVT	R0, #hi_addr(_PID_conf_button+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1935 :: 		PID_conf_button.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_conf_button+56)
MOVT	R0, #hi_addr(_PID_conf_button+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1936 :: 		PID_conf_button.OnClickPtr      = PID_conf_buttonClick;
MOVW	R1, #lo_addr(_PID_conf_buttonClick+0)
MOVT	R1, #hi_addr(_PID_conf_buttonClick+0)
MOVW	R0, #lo_addr(_PID_conf_button+60)
MOVT	R0, #hi_addr(_PID_conf_button+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1937 :: 		PID_conf_button.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_conf_button+64)
MOVT	R0, #hi_addr(_PID_conf_button+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1939 :: 		PID_ret_button.OwnerScreen     = &PID;
MOVW	R1, #lo_addr(_PID+0)
MOVT	R1, #hi_addr(_PID+0)
MOVW	R0, #lo_addr(_PID_ret_button+0)
MOVT	R0, #hi_addr(_PID_ret_button+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1940 :: 		PID_ret_button.Order           = 7;
MOVS	R1, #7
MOVW	R0, #lo_addr(_PID_ret_button+4)
MOVT	R0, #hi_addr(_PID_ret_button+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1941 :: 		PID_ret_button.Left            = 239;
MOVS	R1, #239
MOVW	R0, #lo_addr(_PID_ret_button+6)
MOVT	R0, #hi_addr(_PID_ret_button+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1942 :: 		PID_ret_button.Top             = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_PID_ret_button+8)
MOVT	R0, #hi_addr(_PID_ret_button+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1943 :: 		PID_ret_button.Width           = 74;
MOVS	R1, #74
MOVW	R0, #lo_addr(_PID_ret_button+10)
MOVT	R0, #hi_addr(_PID_ret_button+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1944 :: 		PID_ret_button.Height          = 70;
MOVS	R1, #70
MOVW	R0, #lo_addr(_PID_ret_button+12)
MOVT	R0, #hi_addr(_PID_ret_button+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1945 :: 		PID_ret_button.Pen_Width       = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_PID_ret_button+14)
MOVT	R0, #hi_addr(_PID_ret_button+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1946 :: 		PID_ret_button.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_PID_ret_button+16)
MOVT	R0, #hi_addr(_PID_ret_button+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1947 :: 		PID_ret_button.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID_ret_button+18)
MOVT	R0, #hi_addr(_PID_ret_button+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1948 :: 		PID_ret_button.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID_ret_button+19)
MOVT	R0, #hi_addr(_PID_ret_button+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1949 :: 		PID_ret_button.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID_ret_button+20)
MOVT	R0, #hi_addr(_PID_ret_button+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1950 :: 		PID_ret_button.Caption         = PID_ret_button_Caption;
MOVW	R1, #lo_addr(_PID_ret_button_Caption+0)
MOVT	R1, #hi_addr(_PID_ret_button_Caption+0)
MOVW	R0, #lo_addr(_PID_ret_button+24)
MOVT	R0, #hi_addr(_PID_ret_button+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1951 :: 		PID_ret_button.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID_ret_button+28)
MOVT	R0, #hi_addr(_PID_ret_button+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1952 :: 		PID_ret_button.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID_ret_button+29)
MOVT	R0, #hi_addr(_PID_ret_button+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1953 :: 		PID_ret_button.FontName        = Tahoma42x52_Regular;
MOVW	R0, #lo_addr(_PID_ret_button+32)
MOVT	R0, #hi_addr(_PID_ret_button+32)
STR	R6, [R0, #0]
;BPWasher_driver.c,1954 :: 		PID_ret_button.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PID_ret_button+49)
MOVT	R0, #hi_addr(_PID_ret_button+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1955 :: 		PID_ret_button.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_ret_button+36)
MOVT	R0, #hi_addr(_PID_ret_button+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1956 :: 		PID_ret_button.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_ret_button+38)
MOVT	R0, #hi_addr(_PID_ret_button+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1957 :: 		PID_ret_button.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_ret_button+39)
MOVT	R0, #hi_addr(_PID_ret_button+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1958 :: 		PID_ret_button.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_ret_button+40)
MOVT	R0, #hi_addr(_PID_ret_button+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1959 :: 		PID_ret_button.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_PID_ret_button+42)
MOVT	R0, #hi_addr(_PID_ret_button+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1960 :: 		PID_ret_button.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_PID_ret_button+44)
MOVT	R0, #hi_addr(_PID_ret_button+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1961 :: 		PID_ret_button.Color           = 0xF800;
MOVW	R1, #63488
MOVW	R0, #lo_addr(_PID_ret_button+46)
MOVT	R0, #hi_addr(_PID_ret_button+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1962 :: 		PID_ret_button.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_PID_ret_button+50)
MOVT	R0, #hi_addr(_PID_ret_button+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1963 :: 		PID_ret_button.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_PID_ret_button+48)
MOVT	R0, #hi_addr(_PID_ret_button+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1964 :: 		PID_ret_button.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_ret_button+52)
MOVT	R0, #hi_addr(_PID_ret_button+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,1965 :: 		PID_ret_button.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_ret_button+56)
MOVT	R0, #hi_addr(_PID_ret_button+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,1966 :: 		PID_ret_button.OnClickPtr      = PID_ret_buttonClick;   // was PID_conf_buttonClick - CM 10/07/17
MOVW	R1, #lo_addr(_PID_ret_buttonClick+0)
MOVT	R1, #hi_addr(_PID_ret_buttonClick+0)
MOVW	R0, #lo_addr(_PID_ret_button+60)
MOVT	R0, #hi_addr(_PID_ret_button+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,1967 :: 		PID_ret_button.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PID_ret_button+64)
MOVT	R0, #hi_addr(_PID_ret_button+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,1969 :: 		Image9.OwnerScreen     = &PID;
MOVW	R1, #lo_addr(_PID+0)
MOVT	R1, #hi_addr(_PID+0)
MOVW	R0, #lo_addr(_Image9+0)
MOVT	R0, #hi_addr(_Image9+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1970 :: 		Image9.Order           = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_Image9+4)
MOVT	R0, #hi_addr(_Image9+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1971 :: 		Image9.Left            = 257;
MOVW	R1, #257
MOVW	R0, #lo_addr(_Image9+6)
MOVT	R0, #hi_addr(_Image9+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1972 :: 		Image9.Top             = 16;
MOVS	R1, #16
MOVW	R0, #lo_addr(_Image9+8)
MOVT	R0, #hi_addr(_Image9+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1973 :: 		Image9.Width           = 40;
MOVS	R1, #40
MOVW	R0, #lo_addr(_Image9+10)
MOVT	R0, #hi_addr(_Image9+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1974 :: 		Image9.Height          = 42;
MOVS	R1, #42
MOVW	R0, #lo_addr(_Image9+12)
MOVT	R0, #hi_addr(_Image9+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1975 :: 		Image9.Picture_Type    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image9+22)
MOVT	R0, #hi_addr(_Image9+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1976 :: 		Image9.Picture_Ratio   = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image9+23)
MOVT	R0, #hi_addr(_Image9+23)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1977 :: 		Image9.Picture_Name    = returnsarrow_bmp;
MOVW	R5, #lo_addr(_returnsarrow_bmp+0)
MOVT	R5, #hi_addr(_returnsarrow_bmp+0)
MOVW	R0, #lo_addr(_Image9+16)
MOVT	R0, #hi_addr(_Image9+16)
STR	R5, [R0, #0]
;BPWasher_driver.c,1978 :: 		Image9.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image9+20)
MOVT	R0, #hi_addr(_Image9+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1979 :: 		Image9.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image9+21)
MOVT	R0, #hi_addr(_Image9+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1980 :: 		Image9.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image9+24)
MOVT	R0, #hi_addr(_Image9+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1981 :: 		Image9.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image9+28)
MOVT	R0, #hi_addr(_Image9+28)
STR	R1, [R0, #0]
;BPWasher_driver.c,1982 :: 		Image9.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image9+32)
MOVT	R0, #hi_addr(_Image9+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,1983 :: 		Image9.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image9+36)
MOVT	R0, #hi_addr(_Image9+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,1985 :: 		cal_offset_value.OwnerScreen     = &Calibration;
MOVW	R1, #lo_addr(_Calibration+0)
MOVT	R1, #hi_addr(_Calibration+0)
MOVW	R0, #lo_addr(_cal_offset_value+0)
MOVT	R0, #hi_addr(_cal_offset_value+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,1986 :: 		cal_offset_value.Order           = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_offset_value+4)
MOVT	R0, #hi_addr(_cal_offset_value+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1987 :: 		cal_offset_value.Left            = 97; //126; //114
MOVS	R1, #97
MOVW	R0, #lo_addr(_cal_offset_value+6)
MOVT	R0, #hi_addr(_cal_offset_value+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1988 :: 		cal_offset_value.Width           = 266; //101;
MOVW	R1, #266
MOVW	R0, #lo_addr(_cal_offset_value+10)
MOVT	R0, #hi_addr(_cal_offset_value+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1989 :: 		cal_offset_value.TextAlign       = _taLeft; //_taRight;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_offset_value+28)
MOVT	R0, #hi_addr(_cal_offset_value+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1990 :: 		cal_offset_value.Top             = 4; //28;
MOVS	R1, #4
MOVW	R0, #lo_addr(_cal_offset_value+8)
MOVT	R0, #hi_addr(_cal_offset_value+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1991 :: 		cal_offset_value.Height          = 77;
MOVS	R1, #77
MOVW	R0, #lo_addr(_cal_offset_value+12)
MOVT	R0, #hi_addr(_cal_offset_value+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1992 :: 		cal_offset_value.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_offset_value+14)
MOVT	R0, #hi_addr(_cal_offset_value+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1993 :: 		cal_offset_value.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_offset_value+16)
MOVT	R0, #hi_addr(_cal_offset_value+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,1994 :: 		cal_offset_value.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_offset_value+18)
MOVT	R0, #hi_addr(_cal_offset_value+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1995 :: 		cal_offset_value.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_offset_value+19)
MOVT	R0, #hi_addr(_cal_offset_value+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1996 :: 		cal_offset_value.Transparent     = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_offset_value+20)
MOVT	R0, #hi_addr(_cal_offset_value+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1997 :: 		cal_offset_value.Caption         = cal_offset_value_Caption;
MOVW	R1, #lo_addr(_cal_offset_value_Caption+0)
MOVT	R1, #hi_addr(_cal_offset_value_Caption+0)
MOVW	R0, #lo_addr(_cal_offset_value+24)
MOVT	R0, #hi_addr(_cal_offset_value+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,1998 :: 		cal_offset_value.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_offset_value+29)
MOVT	R0, #hi_addr(_cal_offset_value+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,1999 :: 		cal_offset_value.FontName        = Tahoma55x68_Regular;
MOVW	R0, #lo_addr(_cal_offset_value+32)
MOVT	R0, #hi_addr(_cal_offset_value+32)
STR	R3, [R0, #0]
;BPWasher_driver.c,2000 :: 		cal_offset_value.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_offset_value+49)
MOVT	R0, #hi_addr(_cal_offset_value+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2001 :: 		cal_offset_value.Font_Color      = 0xFFE0;
MOVW	R1, #65504
MOVW	R0, #lo_addr(_cal_offset_value+36)
MOVT	R0, #hi_addr(_cal_offset_value+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2002 :: 		cal_offset_value.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_offset_value+38)
MOVT	R0, #hi_addr(_cal_offset_value+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2003 :: 		cal_offset_value.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_offset_value+39)
MOVT	R0, #hi_addr(_cal_offset_value+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2004 :: 		cal_offset_value.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_offset_value+40)
MOVT	R0, #hi_addr(_cal_offset_value+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2005 :: 		cal_offset_value.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_offset_value+42)
MOVT	R0, #hi_addr(_cal_offset_value+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2006 :: 		cal_offset_value.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_cal_offset_value+44)
MOVT	R0, #hi_addr(_cal_offset_value+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2007 :: 		cal_offset_value.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_cal_offset_value+46)
MOVT	R0, #hi_addr(_cal_offset_value+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2008 :: 		cal_offset_value.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_cal_offset_value+50)
MOVT	R0, #hi_addr(_cal_offset_value+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2009 :: 		cal_offset_value.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_cal_offset_value+48)
MOVT	R0, #hi_addr(_cal_offset_value+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2010 :: 		cal_offset_value.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_offset_value+52)
MOVT	R0, #hi_addr(_cal_offset_value+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2011 :: 		cal_offset_value.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_offset_value+56)
MOVT	R0, #hi_addr(_cal_offset_value+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2012 :: 		cal_offset_value.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_offset_value+60)
MOVT	R0, #hi_addr(_cal_offset_value+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2013 :: 		cal_offset_value.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_offset_value+64)
MOVT	R0, #hi_addr(_cal_offset_value+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2015 :: 		cal_factor_value.OwnerScreen     = &Calibration;
MOVW	R1, #lo_addr(_Calibration+0)
MOVT	R1, #hi_addr(_Calibration+0)
MOVW	R0, #lo_addr(_cal_factor_value+0)
MOVT	R0, #hi_addr(_cal_factor_value+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2016 :: 		cal_factor_value.Order           = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_cal_factor_value+4)
MOVT	R0, #hi_addr(_cal_factor_value+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2017 :: 		cal_factor_value.Left            = 97; //126; //114
MOVS	R1, #97
MOVW	R0, #lo_addr(_cal_factor_value+6)
MOVT	R0, #hi_addr(_cal_factor_value+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2018 :: 		cal_factor_value.Width           = 266; //101;
MOVW	R1, #266
MOVW	R0, #lo_addr(_cal_factor_value+10)
MOVT	R0, #hi_addr(_cal_factor_value+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2019 :: 		cal_factor_value.TextAlign       = _taLeft; //_taRight;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_factor_value+28)
MOVT	R0, #hi_addr(_cal_factor_value+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2020 :: 		cal_factor_value.Top             = 90; //115;
MOVS	R1, #90
MOVW	R0, #lo_addr(_cal_factor_value+8)
MOVT	R0, #hi_addr(_cal_factor_value+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2021 :: 		cal_factor_value.Height          = 77;
MOVS	R1, #77
MOVW	R0, #lo_addr(_cal_factor_value+12)
MOVT	R0, #hi_addr(_cal_factor_value+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2022 :: 		cal_factor_value.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_factor_value+14)
MOVT	R0, #hi_addr(_cal_factor_value+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2023 :: 		cal_factor_value.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_factor_value+16)
MOVT	R0, #hi_addr(_cal_factor_value+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2024 :: 		cal_factor_value.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_factor_value+18)
MOVT	R0, #hi_addr(_cal_factor_value+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2025 :: 		cal_factor_value.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_factor_value+19)
MOVT	R0, #hi_addr(_cal_factor_value+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2026 :: 		cal_factor_value.Transparent     = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_factor_value+20)
MOVT	R0, #hi_addr(_cal_factor_value+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2027 :: 		cal_factor_value.Caption         = cal_factor_value_Caption;
MOVW	R1, #lo_addr(_cal_factor_value_Caption+0)
MOVT	R1, #hi_addr(_cal_factor_value_Caption+0)
MOVW	R0, #lo_addr(_cal_factor_value+24)
MOVT	R0, #hi_addr(_cal_factor_value+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2028 :: 		cal_factor_value.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_factor_value+29)
MOVT	R0, #hi_addr(_cal_factor_value+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2029 :: 		cal_factor_value.FontName        = Tahoma55x68_Regular;
MOVW	R0, #lo_addr(_cal_factor_value+32)
MOVT	R0, #hi_addr(_cal_factor_value+32)
STR	R3, [R0, #0]
;BPWasher_driver.c,2030 :: 		cal_factor_value.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_factor_value+49)
MOVT	R0, #hi_addr(_cal_factor_value+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2031 :: 		cal_factor_value.Font_Color      = 0xFFE0;
MOVW	R1, #65504
MOVW	R0, #lo_addr(_cal_factor_value+36)
MOVT	R0, #hi_addr(_cal_factor_value+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2032 :: 		cal_factor_value.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_factor_value+38)
MOVT	R0, #hi_addr(_cal_factor_value+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2033 :: 		cal_factor_value.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_factor_value+39)
MOVT	R0, #hi_addr(_cal_factor_value+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2034 :: 		cal_factor_value.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_factor_value+40)
MOVT	R0, #hi_addr(_cal_factor_value+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2035 :: 		cal_factor_value.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_factor_value+42)
MOVT	R0, #hi_addr(_cal_factor_value+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2036 :: 		cal_factor_value.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_cal_factor_value+44)
MOVT	R0, #hi_addr(_cal_factor_value+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2037 :: 		cal_factor_value.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_cal_factor_value+46)
MOVT	R0, #hi_addr(_cal_factor_value+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2038 :: 		cal_factor_value.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_cal_factor_value+50)
MOVT	R0, #hi_addr(_cal_factor_value+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2039 :: 		cal_factor_value.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_cal_factor_value+48)
MOVT	R0, #hi_addr(_cal_factor_value+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2040 :: 		cal_factor_value.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_factor_value+52)
MOVT	R0, #hi_addr(_cal_factor_value+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2041 :: 		cal_factor_value.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_factor_value+56)
MOVT	R0, #hi_addr(_cal_factor_value+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2042 :: 		cal_factor_value.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_factor_value+60)
MOVT	R0, #hi_addr(_cal_factor_value+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2043 :: 		cal_factor_value.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_factor_value+64)
MOVT	R0, #hi_addr(_cal_factor_value+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2045 :: 		cal_fact_button.OwnerScreen     = &Calibration;
MOVW	R1, #lo_addr(_Calibration+0)
MOVT	R1, #hi_addr(_Calibration+0)
MOVW	R0, #lo_addr(_cal_fact_button+0)
MOVT	R0, #hi_addr(_cal_fact_button+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2046 :: 		cal_fact_button.Order           = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_cal_fact_button+4)
MOVT	R0, #hi_addr(_cal_fact_button+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2047 :: 		cal_fact_button.Left            = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_cal_fact_button+6)
MOVT	R0, #hi_addr(_cal_fact_button+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2048 :: 		cal_fact_button.Top             = 90; //117;
MOVS	R1, #90
MOVW	R0, #lo_addr(_cal_fact_button+8)
MOVT	R0, #hi_addr(_cal_fact_button+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2049 :: 		cal_fact_button.Width           = 95;
MOVS	R1, #95
MOVW	R0, #lo_addr(_cal_fact_button+10)
MOVT	R0, #hi_addr(_cal_fact_button+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2050 :: 		cal_fact_button.Height          = 77;
MOVS	R1, #77
MOVW	R0, #lo_addr(_cal_fact_button+12)
MOVT	R0, #hi_addr(_cal_fact_button+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2051 :: 		cal_fact_button.Pen_Width       = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_cal_fact_button+14)
MOVT	R0, #hi_addr(_cal_fact_button+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2052 :: 		cal_fact_button.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_fact_button+16)
MOVT	R0, #hi_addr(_cal_fact_button+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2053 :: 		cal_fact_button.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_fact_button+18)
MOVT	R0, #hi_addr(_cal_fact_button+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2054 :: 		cal_fact_button.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_fact_button+19)
MOVT	R0, #hi_addr(_cal_fact_button+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2055 :: 		cal_fact_button.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_fact_button+20)
MOVT	R0, #hi_addr(_cal_fact_button+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2056 :: 		cal_fact_button.Caption         = cal_fact_button_Caption;
MOVW	R1, #lo_addr(_cal_fact_button_Caption+0)
MOVT	R1, #hi_addr(_cal_fact_button_Caption+0)
MOVW	R0, #lo_addr(_cal_fact_button+24)
MOVT	R0, #hi_addr(_cal_fact_button+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2057 :: 		cal_fact_button.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_fact_button+28)
MOVT	R0, #hi_addr(_cal_fact_button+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2058 :: 		cal_fact_button.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_fact_button+29)
MOVT	R0, #hi_addr(_cal_fact_button+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2059 :: 		cal_fact_button.FontName        = Tahoma16x19_Regular;
MOVW	R4, #lo_addr(_Tahoma16x19_Regular+0)
MOVT	R4, #hi_addr(_Tahoma16x19_Regular+0)
MOVW	R0, #lo_addr(_cal_fact_button+32)
MOVT	R0, #hi_addr(_cal_fact_button+32)
STR	R4, [R0, #0]
;BPWasher_driver.c,2060 :: 		cal_fact_button.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_fact_button+49)
MOVT	R0, #hi_addr(_cal_fact_button+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2061 :: 		cal_fact_button.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_fact_button+36)
MOVT	R0, #hi_addr(_cal_fact_button+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2062 :: 		cal_fact_button.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_fact_button+38)
MOVT	R0, #hi_addr(_cal_fact_button+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2063 :: 		cal_fact_button.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_fact_button+39)
MOVT	R0, #hi_addr(_cal_fact_button+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2064 :: 		cal_fact_button.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_fact_button+40)
MOVT	R0, #hi_addr(_cal_fact_button+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2065 :: 		cal_fact_button.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_fact_button+42)
MOVT	R0, #hi_addr(_cal_fact_button+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2066 :: 		cal_fact_button.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_cal_fact_button+44)
MOVT	R0, #hi_addr(_cal_fact_button+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2067 :: 		cal_fact_button.Color           = 0x001F;
MOVS	R1, #31
MOVW	R0, #lo_addr(_cal_fact_button+46)
MOVT	R0, #hi_addr(_cal_fact_button+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2068 :: 		cal_fact_button.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_cal_fact_button+50)
MOVT	R0, #hi_addr(_cal_fact_button+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2069 :: 		cal_fact_button.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_cal_fact_button+48)
MOVT	R0, #hi_addr(_cal_fact_button+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2070 :: 		cal_fact_button.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_fact_button+52)
MOVT	R0, #hi_addr(_cal_fact_button+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2071 :: 		cal_fact_button.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_fact_button+56)
MOVT	R0, #hi_addr(_cal_fact_button+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2072 :: 		cal_fact_button.OnClickPtr      = cal_fact_buttonClick;
MOVW	R1, #lo_addr(_cal_fact_buttonClick+0)
MOVT	R1, #hi_addr(_cal_fact_buttonClick+0)
MOVW	R0, #lo_addr(_cal_fact_button+60)
MOVT	R0, #hi_addr(_cal_fact_button+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2073 :: 		cal_fact_button.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_fact_button+64)
MOVT	R0, #hi_addr(_cal_fact_button+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2075 :: 		cal_confirm_button.OwnerScreen     = &Calibration;
MOVW	R1, #lo_addr(_Calibration+0)
MOVT	R1, #hi_addr(_Calibration+0)
MOVW	R0, #lo_addr(_cal_confirm_button+0)
MOVT	R0, #hi_addr(_cal_confirm_button+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2076 :: 		cal_confirm_button.Order           = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_cal_confirm_button+4)
MOVT	R0, #hi_addr(_cal_confirm_button+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2077 :: 		cal_confirm_button.Left            = 227;  //222
MOVS	R1, #227
MOVW	R0, #lo_addr(_cal_confirm_button+6)
MOVT	R0, #hi_addr(_cal_confirm_button+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2078 :: 		cal_confirm_button.Top             = 157;
MOVS	R1, #157
MOVW	R0, #lo_addr(_cal_confirm_button+8)
MOVT	R0, #hi_addr(_cal_confirm_button+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2079 :: 		cal_confirm_button.Width           = 90;  //95
MOVS	R1, #90
MOVW	R0, #lo_addr(_cal_confirm_button+10)
MOVT	R0, #hi_addr(_cal_confirm_button+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2080 :: 		cal_confirm_button.Height          = 77;
MOVS	R1, #77
MOVW	R0, #lo_addr(_cal_confirm_button+12)
MOVT	R0, #hi_addr(_cal_confirm_button+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2081 :: 		cal_confirm_button.Pen_Width       = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_cal_confirm_button+14)
MOVT	R0, #hi_addr(_cal_confirm_button+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2082 :: 		cal_confirm_button.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_confirm_button+16)
MOVT	R0, #hi_addr(_cal_confirm_button+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2083 :: 		cal_confirm_button.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_confirm_button+18)
MOVT	R0, #hi_addr(_cal_confirm_button+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2084 :: 		cal_confirm_button.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_confirm_button+19)
MOVT	R0, #hi_addr(_cal_confirm_button+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2085 :: 		cal_confirm_button.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_confirm_button+20)
MOVT	R0, #hi_addr(_cal_confirm_button+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2086 :: 		cal_confirm_button.Caption         = cal_confirm_button_Caption;
MOVW	R1, #lo_addr(_cal_confirm_button_Caption+0)
MOVT	R1, #hi_addr(_cal_confirm_button_Caption+0)
MOVW	R0, #lo_addr(_cal_confirm_button+24)
MOVT	R0, #hi_addr(_cal_confirm_button+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2087 :: 		cal_confirm_button.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_confirm_button+28)
MOVT	R0, #hi_addr(_cal_confirm_button+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2088 :: 		cal_confirm_button.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_confirm_button+29)
MOVT	R0, #hi_addr(_cal_confirm_button+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2089 :: 		cal_confirm_button.FontName        = Tahoma42x52_Regular;
MOVW	R0, #lo_addr(_cal_confirm_button+32)
MOVT	R0, #hi_addr(_cal_confirm_button+32)
STR	R6, [R0, #0]
;BPWasher_driver.c,2090 :: 		cal_confirm_button.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_confirm_button+49)
MOVT	R0, #hi_addr(_cal_confirm_button+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2091 :: 		cal_confirm_button.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_confirm_button+36)
MOVT	R0, #hi_addr(_cal_confirm_button+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2092 :: 		cal_confirm_button.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_confirm_button+38)
MOVT	R0, #hi_addr(_cal_confirm_button+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2093 :: 		cal_confirm_button.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_confirm_button+39)
MOVT	R0, #hi_addr(_cal_confirm_button+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2094 :: 		cal_confirm_button.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_confirm_button+40)
MOVT	R0, #hi_addr(_cal_confirm_button+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2095 :: 		cal_confirm_button.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_confirm_button+42)
MOVT	R0, #hi_addr(_cal_confirm_button+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2096 :: 		cal_confirm_button.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_cal_confirm_button+44)
MOVT	R0, #hi_addr(_cal_confirm_button+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2097 :: 		cal_confirm_button.Color           = 0x0400;
MOVW	R1, #1024
MOVW	R0, #lo_addr(_cal_confirm_button+46)
MOVT	R0, #hi_addr(_cal_confirm_button+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2098 :: 		cal_confirm_button.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_cal_confirm_button+50)
MOVT	R0, #hi_addr(_cal_confirm_button+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2099 :: 		cal_confirm_button.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_cal_confirm_button+48)
MOVT	R0, #hi_addr(_cal_confirm_button+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2100 :: 		cal_confirm_button.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_confirm_button+52)
MOVT	R0, #hi_addr(_cal_confirm_button+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2101 :: 		cal_confirm_button.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_confirm_button+56)
MOVT	R0, #hi_addr(_cal_confirm_button+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2102 :: 		cal_confirm_button.OnClickPtr      = cal_confirm_buttonClick;
MOVW	R1, #lo_addr(_cal_confirm_buttonClick+0)
MOVT	R1, #hi_addr(_cal_confirm_buttonClick+0)
MOVW	R0, #lo_addr(_cal_confirm_button+60)
MOVT	R0, #hi_addr(_cal_confirm_button+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2103 :: 		cal_confirm_button.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_confirm_button+64)
MOVT	R0, #hi_addr(_cal_confirm_button+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2105 :: 		cal_ret_button.OwnerScreen     = &Calibration;
MOVW	R1, #lo_addr(_Calibration+0)
MOVT	R1, #hi_addr(_Calibration+0)
MOVW	R0, #lo_addr(_cal_ret_button+0)
MOVT	R0, #hi_addr(_cal_ret_button+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2106 :: 		cal_ret_button.Order           = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_cal_ret_button+4)
MOVT	R0, #hi_addr(_cal_ret_button+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2107 :: 		cal_ret_button.Left            = 227;  //222
MOVS	R1, #227
MOVW	R0, #lo_addr(_cal_ret_button+6)
MOVT	R0, #hi_addr(_cal_ret_button+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2108 :: 		cal_ret_button.Top             = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_cal_ret_button+8)
MOVT	R0, #hi_addr(_cal_ret_button+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2109 :: 		cal_ret_button.Width           = 90;  //95
MOVS	R1, #90
MOVW	R0, #lo_addr(_cal_ret_button+10)
MOVT	R0, #hi_addr(_cal_ret_button+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2110 :: 		cal_ret_button.Height          = 77;
MOVS	R1, #77
MOVW	R0, #lo_addr(_cal_ret_button+12)
MOVT	R0, #hi_addr(_cal_ret_button+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2111 :: 		cal_ret_button.Pen_Width       = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_cal_ret_button+14)
MOVT	R0, #hi_addr(_cal_ret_button+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2112 :: 		cal_ret_button.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_ret_button+16)
MOVT	R0, #hi_addr(_cal_ret_button+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2113 :: 		cal_ret_button.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_ret_button+18)
MOVT	R0, #hi_addr(_cal_ret_button+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2114 :: 		cal_ret_button.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_ret_button+19)
MOVT	R0, #hi_addr(_cal_ret_button+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2115 :: 		cal_ret_button.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_ret_button+20)
MOVT	R0, #hi_addr(_cal_ret_button+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2116 :: 		cal_ret_button.Caption         = cal_ret_button_Caption;
MOVW	R1, #lo_addr(_cal_ret_button_Caption+0)
MOVT	R1, #hi_addr(_cal_ret_button_Caption+0)
MOVW	R0, #lo_addr(_cal_ret_button+24)
MOVT	R0, #hi_addr(_cal_ret_button+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2117 :: 		cal_ret_button.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_ret_button+28)
MOVT	R0, #hi_addr(_cal_ret_button+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2118 :: 		cal_ret_button.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_ret_button+29)
MOVT	R0, #hi_addr(_cal_ret_button+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2119 :: 		cal_ret_button.FontName        = Tahoma42x52_Regular;
MOVW	R0, #lo_addr(_cal_ret_button+32)
MOVT	R0, #hi_addr(_cal_ret_button+32)
STR	R6, [R0, #0]
;BPWasher_driver.c,2120 :: 		cal_ret_button.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_ret_button+49)
MOVT	R0, #hi_addr(_cal_ret_button+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2121 :: 		cal_ret_button.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_ret_button+36)
MOVT	R0, #hi_addr(_cal_ret_button+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2122 :: 		cal_ret_button.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_ret_button+38)
MOVT	R0, #hi_addr(_cal_ret_button+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2123 :: 		cal_ret_button.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_ret_button+39)
MOVT	R0, #hi_addr(_cal_ret_button+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2124 :: 		cal_ret_button.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_ret_button+40)
MOVT	R0, #hi_addr(_cal_ret_button+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2125 :: 		cal_ret_button.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_ret_button+42)
MOVT	R0, #hi_addr(_cal_ret_button+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2126 :: 		cal_ret_button.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_cal_ret_button+44)
MOVT	R0, #hi_addr(_cal_ret_button+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2127 :: 		cal_ret_button.Color           = 0xF800;
MOVW	R1, #63488
MOVW	R0, #lo_addr(_cal_ret_button+46)
MOVT	R0, #hi_addr(_cal_ret_button+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2128 :: 		cal_ret_button.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_cal_ret_button+50)
MOVT	R0, #hi_addr(_cal_ret_button+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2129 :: 		cal_ret_button.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_cal_ret_button+48)
MOVT	R0, #hi_addr(_cal_ret_button+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2130 :: 		cal_ret_button.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_ret_button+52)
MOVT	R0, #hi_addr(_cal_ret_button+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2131 :: 		cal_ret_button.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_ret_button+56)
MOVT	R0, #hi_addr(_cal_ret_button+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2132 :: 		cal_ret_button.OnClickPtr      = cal_ret_buttonClick;
MOVW	R1, #lo_addr(_cal_ret_buttonClick+0)
MOVT	R1, #hi_addr(_cal_ret_buttonClick+0)
MOVW	R0, #lo_addr(_cal_ret_button+60)
MOVT	R0, #hi_addr(_cal_ret_button+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2133 :: 		cal_ret_button.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_ret_button+64)
MOVT	R0, #hi_addr(_cal_ret_button+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2135 :: 		Image10.OwnerScreen     = &Calibration;
MOVW	R1, #lo_addr(_Calibration+0)
MOVT	R1, #hi_addr(_Calibration+0)
MOVW	R0, #lo_addr(_Image10+0)
MOVT	R0, #hi_addr(_Image10+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2136 :: 		Image10.Order           = 6;
MOVS	R1, #6
MOVW	R0, #lo_addr(_Image10+4)
MOVT	R0, #hi_addr(_Image10+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2137 :: 		Image10.Left            = 250;
MOVS	R1, #250
MOVW	R0, #lo_addr(_Image10+6)
MOVT	R0, #hi_addr(_Image10+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2138 :: 		Image10.Top             = 21;
MOVS	R1, #21
MOVW	R0, #lo_addr(_Image10+8)
MOVT	R0, #hi_addr(_Image10+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2139 :: 		Image10.Width           = 40;
MOVS	R1, #40
MOVW	R0, #lo_addr(_Image10+10)
MOVT	R0, #hi_addr(_Image10+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2140 :: 		Image10.Height          = 42;
MOVS	R1, #42
MOVW	R0, #lo_addr(_Image10+12)
MOVT	R0, #hi_addr(_Image10+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2141 :: 		Image10.Picture_Type    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image10+22)
MOVT	R0, #hi_addr(_Image10+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2142 :: 		Image10.Picture_Ratio   = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image10+23)
MOVT	R0, #hi_addr(_Image10+23)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2143 :: 		Image10.Picture_Name    = returnsarrow_bmp;
MOVW	R0, #lo_addr(_Image10+16)
MOVT	R0, #hi_addr(_Image10+16)
STR	R5, [R0, #0]
;BPWasher_driver.c,2144 :: 		Image10.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image10+20)
MOVT	R0, #hi_addr(_Image10+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2145 :: 		Image10.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image10+21)
MOVT	R0, #hi_addr(_Image10+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2146 :: 		Image10.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image10+24)
MOVT	R0, #hi_addr(_Image10+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2147 :: 		Image10.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image10+28)
MOVT	R0, #hi_addr(_Image10+28)
STR	R1, [R0, #0]
;BPWasher_driver.c,2148 :: 		Image10.OnClickPtr      = cal_ret_buttonClick;
MOVW	R1, #lo_addr(_cal_ret_buttonClick+0)
MOVT	R1, #hi_addr(_cal_ret_buttonClick+0)
MOVW	R0, #lo_addr(_Image10+32)
MOVT	R0, #hi_addr(_Image10+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2149 :: 		Image10.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image10+36)
MOVT	R0, #hi_addr(_Image10+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2151 :: 		cal_start_button.OwnerScreen     = &Calibration;
MOVW	R1, #lo_addr(_Calibration+0)
MOVT	R1, #hi_addr(_Calibration+0)
MOVW	R0, #lo_addr(_cal_start_button+0)
MOVT	R0, #hi_addr(_cal_start_button+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2152 :: 		cal_start_button.Order           = 7;
MOVS	R1, #7
MOVW	R0, #lo_addr(_cal_start_button+4)
MOVT	R0, #hi_addr(_cal_start_button+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2153 :: 		cal_start_button.Left            = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_cal_start_button+6)
MOVT	R0, #hi_addr(_cal_start_button+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2154 :: 		cal_start_button.Top             = 178;
MOVS	R1, #178
MOVW	R0, #lo_addr(_cal_start_button+8)
MOVT	R0, #hi_addr(_cal_start_button+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2155 :: 		cal_start_button.Width           = 175;
MOVS	R1, #175
MOVW	R0, #lo_addr(_cal_start_button+10)
MOVT	R0, #hi_addr(_cal_start_button+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2156 :: 		cal_start_button.Height          = 53;
MOVS	R1, #53
MOVW	R0, #lo_addr(_cal_start_button+12)
MOVT	R0, #hi_addr(_cal_start_button+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2157 :: 		cal_start_button.Pen_Width       = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_cal_start_button+14)
MOVT	R0, #hi_addr(_cal_start_button+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2158 :: 		cal_start_button.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_start_button+16)
MOVT	R0, #hi_addr(_cal_start_button+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2159 :: 		cal_start_button.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_start_button+18)
MOVT	R0, #hi_addr(_cal_start_button+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2160 :: 		cal_start_button.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_start_button+19)
MOVT	R0, #hi_addr(_cal_start_button+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2161 :: 		cal_start_button.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_start_button+20)
MOVT	R0, #hi_addr(_cal_start_button+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2162 :: 		cal_start_button.Caption         = cal_start_button_Caption;
MOVW	R1, #lo_addr(_cal_start_button_Caption+0)
MOVT	R1, #hi_addr(_cal_start_button_Caption+0)
MOVW	R0, #lo_addr(_cal_start_button+24)
MOVT	R0, #hi_addr(_cal_start_button+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2163 :: 		cal_start_button.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_start_button+28)
MOVT	R0, #hi_addr(_cal_start_button+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2164 :: 		cal_start_button.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_start_button+29)
MOVT	R0, #hi_addr(_cal_start_button+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2165 :: 		cal_start_button.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_cal_start_button+32)
MOVT	R0, #hi_addr(_cal_start_button+32)
STR	R4, [R0, #0]
;BPWasher_driver.c,2166 :: 		cal_start_button.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_start_button+49)
MOVT	R0, #hi_addr(_cal_start_button+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2167 :: 		cal_start_button.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_start_button+36)
MOVT	R0, #hi_addr(_cal_start_button+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2168 :: 		cal_start_button.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_start_button+38)
MOVT	R0, #hi_addr(_cal_start_button+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2169 :: 		cal_start_button.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_start_button+39)
MOVT	R0, #hi_addr(_cal_start_button+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2170 :: 		cal_start_button.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_start_button+40)
MOVT	R0, #hi_addr(_cal_start_button+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2171 :: 		cal_start_button.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_start_button+42)
MOVT	R0, #hi_addr(_cal_start_button+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2172 :: 		cal_start_button.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_cal_start_button+44)
MOVT	R0, #hi_addr(_cal_start_button+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2173 :: 		cal_start_button.Color           = 0xFC00;
MOVW	R1, #64512
MOVW	R0, #lo_addr(_cal_start_button+46)
MOVT	R0, #hi_addr(_cal_start_button+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2174 :: 		cal_start_button.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_cal_start_button+50)
MOVT	R0, #hi_addr(_cal_start_button+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2175 :: 		cal_start_button.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_cal_start_button+48)
MOVT	R0, #hi_addr(_cal_start_button+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2176 :: 		cal_start_button.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_start_button+52)
MOVT	R0, #hi_addr(_cal_start_button+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2177 :: 		cal_start_button.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_start_button+56)
MOVT	R0, #hi_addr(_cal_start_button+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2178 :: 		cal_start_button.OnClickPtr      = cal_start_buttonClick;
MOVW	R1, #lo_addr(_cal_start_buttonClick+0)
MOVT	R1, #hi_addr(_cal_start_buttonClick+0)
MOVW	R0, #lo_addr(_cal_start_button+60)
MOVT	R0, #hi_addr(_cal_start_button+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2179 :: 		cal_start_button.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_start_button+64)
MOVT	R0, #hi_addr(_cal_start_button+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2181 :: 		cal_message_label.OwnerScreen     = &Calibration;
MOVW	R1, #lo_addr(_Calibration+0)
MOVT	R1, #hi_addr(_Calibration+0)
MOVW	R0, #lo_addr(_cal_message_label+0)
MOVT	R0, #hi_addr(_cal_message_label+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2182 :: 		cal_message_label.Order           = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_cal_message_label+4)
MOVT	R0, #hi_addr(_cal_message_label+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2183 :: 		cal_message_label.Left            = 15;
MOVS	R1, #15
MOVW	R0, #lo_addr(_cal_message_label+6)
MOVT	R0, #hi_addr(_cal_message_label+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2184 :: 		cal_message_label.Top             = 84;
MOVS	R1, #84
MOVW	R0, #lo_addr(_cal_message_label+8)
MOVT	R0, #hi_addr(_cal_message_label+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2185 :: 		cal_message_label.Width           = 217;
MOVS	R1, #217
MOVW	R0, #lo_addr(_cal_message_label+10)
MOVT	R0, #hi_addr(_cal_message_label+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2186 :: 		cal_message_label.Height          = 17;
MOVS	R1, #17
MOVW	R0, #lo_addr(_cal_message_label+12)
MOVT	R0, #hi_addr(_cal_message_label+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2187 :: 		cal_message_label.Visible         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_message_label+27)
MOVT	R0, #hi_addr(_cal_message_label+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2188 :: 		cal_message_label.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_cal_message_label+28)
MOVT	R0, #hi_addr(_cal_message_label+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2189 :: 		cal_message_label.Caption         = cal_message_label_Caption;
MOVW	R1, #lo_addr(_cal_message_label_Caption+0)
MOVT	R1, #hi_addr(_cal_message_label_Caption+0)
MOVW	R0, #lo_addr(_cal_message_label+16)
MOVT	R0, #hi_addr(_cal_message_label+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2190 :: 		cal_message_label.FontName        = Tahoma12x16_Regular;
MOVW	R3, #lo_addr(_Tahoma12x16_Regular+0)
MOVT	R3, #hi_addr(_Tahoma12x16_Regular+0)
MOVW	R0, #lo_addr(_cal_message_label+20)
MOVT	R0, #hi_addr(_cal_message_label+20)
STR	R3, [R0, #0]
;BPWasher_driver.c,2191 :: 		cal_message_label.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_message_label+24)
MOVT	R0, #hi_addr(_cal_message_label+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2192 :: 		cal_message_label.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_message_label+26)
MOVT	R0, #hi_addr(_cal_message_label+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2193 :: 		cal_message_label.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_message_label+32)
MOVT	R0, #hi_addr(_cal_message_label+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2194 :: 		cal_message_label.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_message_label+36)
MOVT	R0, #hi_addr(_cal_message_label+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2195 :: 		cal_message_label.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_message_label+40)
MOVT	R0, #hi_addr(_cal_message_label+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2196 :: 		cal_message_label.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_message_label+44)
MOVT	R0, #hi_addr(_cal_message_label+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2198 :: 		cal_temp_label.OwnerScreen     = &Calibration;
MOVW	R1, #lo_addr(_Calibration+0)
MOVT	R1, #hi_addr(_Calibration+0)
MOVW	R0, #lo_addr(_cal_temp_label+0)
MOVT	R0, #hi_addr(_cal_temp_label+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2199 :: 		cal_temp_label.Order           = 9;
MOVS	R1, #9
MOVW	R0, #lo_addr(_cal_temp_label+4)
MOVT	R0, #hi_addr(_cal_temp_label+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2200 :: 		cal_temp_label.Left            = 244;
MOVS	R1, #244
MOVW	R0, #lo_addr(_cal_temp_label+6)
MOVT	R0, #hi_addr(_cal_temp_label+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2201 :: 		cal_temp_label.Top             = 77;
MOVS	R1, #77
MOVW	R0, #lo_addr(_cal_temp_label+8)
MOVT	R0, #hi_addr(_cal_temp_label+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2202 :: 		cal_temp_label.Width           = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_cal_temp_label+10)
MOVT	R0, #hi_addr(_cal_temp_label+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2203 :: 		cal_temp_label.Height          = 25;
MOVS	R1, #25
MOVW	R0, #lo_addr(_cal_temp_label+12)
MOVT	R0, #hi_addr(_cal_temp_label+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2204 :: 		cal_temp_label.Visible         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_temp_label+27)
MOVT	R0, #hi_addr(_cal_temp_label+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2205 :: 		cal_temp_label.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_temp_label+28)
MOVT	R0, #hi_addr(_cal_temp_label+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2206 :: 		cal_temp_label.Caption         = cal_temp_label_Caption;
MOVW	R1, #lo_addr(_cal_temp_label_Caption+0)
MOVT	R1, #hi_addr(_cal_temp_label_Caption+0)
MOVW	R0, #lo_addr(_cal_temp_label+16)
MOVT	R0, #hi_addr(_cal_temp_label+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2207 :: 		cal_temp_label.FontName        = Tahoma19x23_Regular;
MOVW	R0, #lo_addr(_cal_temp_label+20)
MOVT	R0, #hi_addr(_cal_temp_label+20)
STR	R8, [R0, #0]
;BPWasher_driver.c,2208 :: 		cal_temp_label.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_cal_temp_label+24)
MOVT	R0, #hi_addr(_cal_temp_label+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2209 :: 		cal_temp_label.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_temp_label+26)
MOVT	R0, #hi_addr(_cal_temp_label+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2210 :: 		cal_temp_label.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_temp_label+32)
MOVT	R0, #hi_addr(_cal_temp_label+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2211 :: 		cal_temp_label.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_temp_label+36)
MOVT	R0, #hi_addr(_cal_temp_label+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2212 :: 		cal_temp_label.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_temp_label+40)
MOVT	R0, #hi_addr(_cal_temp_label+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2213 :: 		cal_temp_label.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_cal_temp_label+44)
MOVT	R0, #hi_addr(_cal_temp_label+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2215 :: 		Line3.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Line3+0)
MOVT	R0, #hi_addr(_Line3+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2216 :: 		Line3.Order           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Line3+4)
MOVT	R0, #hi_addr(_Line3+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2217 :: 		Line3.First_Point_X   = 160;
MOVS	R1, #160
MOVW	R0, #lo_addr(_Line3+6)
MOVT	R0, #hi_addr(_Line3+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2218 :: 		Line3.First_Point_Y   = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Line3+8)
MOVT	R0, #hi_addr(_Line3+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2219 :: 		Line3.Second_Point_X  = 160;
MOVS	R1, #160
MOVW	R0, #lo_addr(_Line3+10)
MOVT	R0, #hi_addr(_Line3+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2220 :: 		Line3.Second_Point_Y  = 242;
MOVS	R1, #242
MOVW	R0, #lo_addr(_Line3+12)
MOVT	R0, #hi_addr(_Line3+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2221 :: 		Line3.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Line3+15)
MOVT	R0, #hi_addr(_Line3+15)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2222 :: 		Line3.Pen_Width       = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Line3+14)
MOVT	R0, #hi_addr(_Line3+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2223 :: 		Line3.Color           = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_Line3+16)
MOVT	R0, #hi_addr(_Line3+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2225 :: 		Lbl_inputs.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Lbl_inputs+0)
MOVT	R0, #hi_addr(_Lbl_inputs+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2226 :: 		Lbl_inputs.Order           = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Lbl_inputs+4)
MOVT	R0, #hi_addr(_Lbl_inputs+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2227 :: 		Lbl_inputs.Left            = 54;
MOVS	R1, #54
MOVW	R0, #lo_addr(_Lbl_inputs+6)
MOVT	R0, #hi_addr(_Lbl_inputs+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2228 :: 		Lbl_inputs.Top             = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Lbl_inputs+8)
MOVT	R0, #hi_addr(_Lbl_inputs+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2229 :: 		Lbl_inputs.Width           = 72;
MOVS	R1, #72
MOVW	R0, #lo_addr(_Lbl_inputs+10)
MOVT	R0, #hi_addr(_Lbl_inputs+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2230 :: 		Lbl_inputs.Height          = 28;
MOVS	R1, #28
MOVW	R0, #lo_addr(_Lbl_inputs+12)
MOVT	R0, #hi_addr(_Lbl_inputs+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2231 :: 		Lbl_inputs.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Lbl_inputs+27)
MOVT	R0, #hi_addr(_Lbl_inputs+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2232 :: 		Lbl_inputs.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Lbl_inputs+28)
MOVT	R0, #hi_addr(_Lbl_inputs+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2233 :: 		Lbl_inputs.Caption         = Lbl_inputs_Caption;
MOVW	R1, #lo_addr(_Lbl_inputs_Caption+0)
MOVT	R1, #hi_addr(_Lbl_inputs_Caption+0)
MOVW	R0, #lo_addr(_Lbl_inputs+16)
MOVT	R0, #hi_addr(_Lbl_inputs+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2234 :: 		Lbl_inputs.FontName        = Tahoma25x25_Bold;
MOVW	R2, #lo_addr(_Tahoma25x25_Bold+0)
MOVT	R2, #hi_addr(_Tahoma25x25_Bold+0)
MOVW	R0, #lo_addr(_Lbl_inputs+20)
MOVT	R0, #hi_addr(_Lbl_inputs+20)
STR	R2, [R0, #0]
;BPWasher_driver.c,2235 :: 		Lbl_inputs.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Lbl_inputs+24)
MOVT	R0, #hi_addr(_Lbl_inputs+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2236 :: 		Lbl_inputs.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_inputs+26)
MOVT	R0, #hi_addr(_Lbl_inputs+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2237 :: 		Lbl_inputs.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_inputs+32)
MOVT	R0, #hi_addr(_Lbl_inputs+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2238 :: 		Lbl_inputs.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_inputs+36)
MOVT	R0, #hi_addr(_Lbl_inputs+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2239 :: 		Lbl_inputs.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_inputs+40)
MOVT	R0, #hi_addr(_Lbl_inputs+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2240 :: 		Lbl_inputs.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_inputs+44)
MOVT	R0, #hi_addr(_Lbl_inputs+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2242 :: 		Lbl_outputs.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Lbl_outputs+0)
MOVT	R0, #hi_addr(_Lbl_outputs+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2243 :: 		Lbl_outputs.Order           = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_Lbl_outputs+4)
MOVT	R0, #hi_addr(_Lbl_outputs+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2244 :: 		Lbl_outputs.Left            = 206;
MOVS	R1, #206
MOVW	R0, #lo_addr(_Lbl_outputs+6)
MOVT	R0, #hi_addr(_Lbl_outputs+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2245 :: 		Lbl_outputs.Top             = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Lbl_outputs+8)
MOVT	R0, #hi_addr(_Lbl_outputs+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2246 :: 		Lbl_outputs.Width           = 87;
MOVS	R1, #87
MOVW	R0, #lo_addr(_Lbl_outputs+10)
MOVT	R0, #hi_addr(_Lbl_outputs+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2247 :: 		Lbl_outputs.Height          = 28;
MOVS	R1, #28
MOVW	R0, #lo_addr(_Lbl_outputs+12)
MOVT	R0, #hi_addr(_Lbl_outputs+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2248 :: 		Lbl_outputs.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Lbl_outputs+27)
MOVT	R0, #hi_addr(_Lbl_outputs+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2249 :: 		Lbl_outputs.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Lbl_outputs+28)
MOVT	R0, #hi_addr(_Lbl_outputs+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2250 :: 		Lbl_outputs.Caption         = Lbl_outputs_Caption;
MOVW	R1, #lo_addr(_Lbl_outputs_Caption+0)
MOVT	R1, #hi_addr(_Lbl_outputs_Caption+0)
MOVW	R0, #lo_addr(_Lbl_outputs+16)
MOVT	R0, #hi_addr(_Lbl_outputs+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2251 :: 		Lbl_outputs.FontName        = Tahoma25x25_Bold;
MOVW	R0, #lo_addr(_Lbl_outputs+20)
MOVT	R0, #hi_addr(_Lbl_outputs+20)
STR	R2, [R0, #0]
;BPWasher_driver.c,2252 :: 		Lbl_outputs.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Lbl_outputs+24)
MOVT	R0, #hi_addr(_Lbl_outputs+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2253 :: 		Lbl_outputs.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_outputs+26)
MOVT	R0, #hi_addr(_Lbl_outputs+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2254 :: 		Lbl_outputs.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_outputs+32)
MOVT	R0, #hi_addr(_Lbl_outputs+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2255 :: 		Lbl_outputs.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_outputs+36)
MOVT	R0, #hi_addr(_Lbl_outputs+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2256 :: 		Lbl_outputs.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_outputs+40)
MOVT	R0, #hi_addr(_Lbl_outputs+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2257 :: 		Lbl_outputs.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_outputs+44)
MOVT	R0, #hi_addr(_Lbl_outputs+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2259 :: 		Circle1.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Circle1+0)
MOVT	R0, #hi_addr(_Circle1+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2260 :: 		Circle1.Order           = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Circle1+4)
MOVT	R0, #hi_addr(_Circle1+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2261 :: 		Circle1.Left            = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_Circle1+6)
MOVT	R0, #hi_addr(_Circle1+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2262 :: 		Circle1.Top             = 34;
MOVS	R1, #34
MOVW	R0, #lo_addr(_Circle1+8)
MOVT	R0, #hi_addr(_Circle1+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2263 :: 		Circle1.Radius          = 12;
MOVS	R1, #12
MOVW	R0, #lo_addr(_Circle1+10)
MOVT	R0, #hi_addr(_Circle1+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2264 :: 		Circle1.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle1+12)
MOVT	R0, #hi_addr(_Circle1+12)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2265 :: 		Circle1.Pen_Color       = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_Circle1+14)
MOVT	R0, #hi_addr(_Circle1+14)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2266 :: 		Circle1.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle1+16)
MOVT	R0, #hi_addr(_Circle1+16)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2267 :: 		Circle1.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle1+17)
MOVT	R0, #hi_addr(_Circle1+17)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2268 :: 		Circle1.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle1+18)
MOVT	R0, #hi_addr(_Circle1+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2269 :: 		Circle1.Gradient        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle1+19)
MOVT	R0, #hi_addr(_Circle1+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2270 :: 		Circle1.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle1+20)
MOVT	R0, #hi_addr(_Circle1+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2271 :: 		Circle1.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Circle1+22)
MOVT	R0, #hi_addr(_Circle1+22)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2272 :: 		Circle1.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Circle1+24)
MOVT	R0, #hi_addr(_Circle1+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2273 :: 		Circle1.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Circle1+26)
MOVT	R0, #hi_addr(_Circle1+26)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2274 :: 		Circle1.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle1+28)
MOVT	R0, #hi_addr(_Circle1+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2275 :: 		Circle1.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Circle1+30)
MOVT	R0, #hi_addr(_Circle1+30)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2276 :: 		Circle1.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle1+32)
MOVT	R0, #hi_addr(_Circle1+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2277 :: 		Circle1.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle1+36)
MOVT	R0, #hi_addr(_Circle1+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2278 :: 		Circle1.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle1+40)
MOVT	R0, #hi_addr(_Circle1+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2279 :: 		Circle1.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle1+44)
MOVT	R0, #hi_addr(_Circle1+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2281 :: 		Lbl_water.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Lbl_water+0)
MOVT	R0, #hi_addr(_Lbl_water+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2282 :: 		Lbl_water.Order           = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_Lbl_water+4)
MOVT	R0, #hi_addr(_Lbl_water+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2283 :: 		Lbl_water.Left            = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Lbl_water+6)
MOVT	R0, #hi_addr(_Lbl_water+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2284 :: 		Lbl_water.Top             = 35;
MOVS	R1, #35
MOVW	R0, #lo_addr(_Lbl_water+8)
MOVT	R0, #hi_addr(_Lbl_water+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2285 :: 		Lbl_water.Width           = 59;
MOVS	R1, #59
MOVW	R0, #lo_addr(_Lbl_water+10)
MOVT	R0, #hi_addr(_Lbl_water+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2286 :: 		Lbl_water.Height          = 21;
MOVS	R1, #21
MOVW	R0, #lo_addr(_Lbl_water+12)
MOVT	R0, #hi_addr(_Lbl_water+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2287 :: 		Lbl_water.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Lbl_water+27)
MOVT	R0, #hi_addr(_Lbl_water+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2288 :: 		Lbl_water.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Lbl_water+28)
MOVT	R0, #hi_addr(_Lbl_water+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2289 :: 		Lbl_water.Caption         = Lbl_water_Caption;
MOVW	R1, #lo_addr(_Lbl_water_Caption+0)
MOVT	R1, #hi_addr(_Lbl_water_Caption+0)
MOVW	R0, #lo_addr(_Lbl_water+16)
MOVT	R0, #hi_addr(_Lbl_water+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2290 :: 		Lbl_water.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_Lbl_water+20)
MOVT	R0, #hi_addr(_Lbl_water+20)
STR	R4, [R0, #0]
;BPWasher_driver.c,2291 :: 		Lbl_water.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Lbl_water+24)
MOVT	R0, #hi_addr(_Lbl_water+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2292 :: 		Lbl_water.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_water+26)
MOVT	R0, #hi_addr(_Lbl_water+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2293 :: 		Lbl_water.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_water+32)
MOVT	R0, #hi_addr(_Lbl_water+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2294 :: 		Lbl_water.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_water+36)
MOVT	R0, #hi_addr(_Lbl_water+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2295 :: 		Lbl_water.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_water+40)
MOVT	R0, #hi_addr(_Lbl_water+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2296 :: 		Lbl_water.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Lbl_water+44)
MOVT	R0, #hi_addr(_Lbl_water+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2298 :: 		Circle2.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Circle2+0)
MOVT	R0, #hi_addr(_Circle2+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2299 :: 		Circle2.Order           = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_Circle2+4)
MOVT	R0, #hi_addr(_Circle2+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2300 :: 		Circle2.Left            = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_Circle2+6)
MOVT	R0, #hi_addr(_Circle2+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2301 :: 		Circle2.Top             = 67;
MOVS	R1, #67
MOVW	R0, #lo_addr(_Circle2+8)
MOVT	R0, #hi_addr(_Circle2+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2302 :: 		Circle2.Radius          = 12;
MOVS	R1, #12
MOVW	R0, #lo_addr(_Circle2+10)
MOVT	R0, #hi_addr(_Circle2+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2303 :: 		Circle2.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle2+12)
MOVT	R0, #hi_addr(_Circle2+12)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2304 :: 		Circle2.Pen_Color       = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_Circle2+14)
MOVT	R0, #hi_addr(_Circle2+14)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2305 :: 		Circle2.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle2+16)
MOVT	R0, #hi_addr(_Circle2+16)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2306 :: 		Circle2.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle2+17)
MOVT	R0, #hi_addr(_Circle2+17)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2307 :: 		Circle2.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle2+18)
MOVT	R0, #hi_addr(_Circle2+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2308 :: 		Circle2.Gradient        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle2+19)
MOVT	R0, #hi_addr(_Circle2+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2309 :: 		Circle2.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle2+20)
MOVT	R0, #hi_addr(_Circle2+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2310 :: 		Circle2.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Circle2+22)
MOVT	R0, #hi_addr(_Circle2+22)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2311 :: 		Circle2.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Circle2+24)
MOVT	R0, #hi_addr(_Circle2+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2312 :: 		Circle2.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Circle2+26)
MOVT	R0, #hi_addr(_Circle2+26)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2313 :: 		Circle2.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle2+28)
MOVT	R0, #hi_addr(_Circle2+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2314 :: 		Circle2.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Circle2+30)
MOVT	R0, #hi_addr(_Circle2+30)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2315 :: 		Circle2.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle2+32)
MOVT	R0, #hi_addr(_Circle2+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2316 :: 		Circle2.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle2+36)
MOVT	R0, #hi_addr(_Circle2+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2317 :: 		Circle2.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle2+40)
MOVT	R0, #hi_addr(_Circle2+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2318 :: 		Circle2.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle2+44)
MOVT	R0, #hi_addr(_Circle2+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2320 :: 		Label3.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Label3+0)
MOVT	R0, #hi_addr(_Label3+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2321 :: 		Label3.Order           = 6;
MOVS	R1, #6
MOVW	R0, #lo_addr(_Label3+4)
MOVT	R0, #hi_addr(_Label3+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2322 :: 		Label3.Left            = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Label3+6)
MOVT	R0, #hi_addr(_Label3+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2323 :: 		Label3.Top             = 68;
MOVS	R1, #68
MOVW	R0, #lo_addr(_Label3+8)
MOVT	R0, #hi_addr(_Label3+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2324 :: 		Label3.Width           = 35;
MOVS	R1, #35
MOVW	R0, #lo_addr(_Label3+10)
MOVT	R0, #hi_addr(_Label3+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2325 :: 		Label3.Height          = 21;
MOVS	R1, #21
MOVW	R0, #lo_addr(_Label3+12)
MOVT	R0, #hi_addr(_Label3+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2326 :: 		Label3.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label3+27)
MOVT	R0, #hi_addr(_Label3+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2327 :: 		Label3.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label3+28)
MOVT	R0, #hi_addr(_Label3+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2328 :: 		Label3.Caption         = Label3_Caption;
MOVW	R1, #lo_addr(_Label3_Caption+0)
MOVT	R1, #hi_addr(_Label3_Caption+0)
MOVW	R0, #lo_addr(_Label3+16)
MOVT	R0, #hi_addr(_Label3+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2329 :: 		Label3.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_Label3+20)
MOVT	R0, #hi_addr(_Label3+20)
STR	R4, [R0, #0]
;BPWasher_driver.c,2330 :: 		Label3.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Label3+24)
MOVT	R0, #hi_addr(_Label3+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2331 :: 		Label3.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label3+26)
MOVT	R0, #hi_addr(_Label3+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2332 :: 		Label3.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label3+32)
MOVT	R0, #hi_addr(_Label3+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2333 :: 		Label3.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label3+36)
MOVT	R0, #hi_addr(_Label3+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2334 :: 		Label3.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label3+40)
MOVT	R0, #hi_addr(_Label3+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2335 :: 		Label3.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label3+44)
MOVT	R0, #hi_addr(_Label3+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2337 :: 		Circle3.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Circle3+0)
MOVT	R0, #hi_addr(_Circle3+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2338 :: 		Circle3.Order           = 7;
MOVS	R1, #7
MOVW	R0, #lo_addr(_Circle3+4)
MOVT	R0, #hi_addr(_Circle3+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2339 :: 		Circle3.Left            = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_Circle3+6)
MOVT	R0, #hi_addr(_Circle3+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2340 :: 		Circle3.Top             = 101;
MOVS	R1, #101
MOVW	R0, #lo_addr(_Circle3+8)
MOVT	R0, #hi_addr(_Circle3+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2341 :: 		Circle3.Radius          = 12;
MOVS	R1, #12
MOVW	R0, #lo_addr(_Circle3+10)
MOVT	R0, #hi_addr(_Circle3+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2342 :: 		Circle3.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle3+12)
MOVT	R0, #hi_addr(_Circle3+12)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2343 :: 		Circle3.Pen_Color       = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_Circle3+14)
MOVT	R0, #hi_addr(_Circle3+14)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2344 :: 		Circle3.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle3+16)
MOVT	R0, #hi_addr(_Circle3+16)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2345 :: 		Circle3.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle3+17)
MOVT	R0, #hi_addr(_Circle3+17)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2346 :: 		Circle3.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle3+18)
MOVT	R0, #hi_addr(_Circle3+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2347 :: 		Circle3.Gradient        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle3+19)
MOVT	R0, #hi_addr(_Circle3+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2348 :: 		Circle3.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle3+20)
MOVT	R0, #hi_addr(_Circle3+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2349 :: 		Circle3.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Circle3+22)
MOVT	R0, #hi_addr(_Circle3+22)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2350 :: 		Circle3.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Circle3+24)
MOVT	R0, #hi_addr(_Circle3+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2351 :: 		Circle3.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Circle3+26)
MOVT	R0, #hi_addr(_Circle3+26)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2352 :: 		Circle3.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle3+28)
MOVT	R0, #hi_addr(_Circle3+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2353 :: 		Circle3.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Circle3+30)
MOVT	R0, #hi_addr(_Circle3+30)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2354 :: 		Circle3.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle3+32)
MOVT	R0, #hi_addr(_Circle3+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2355 :: 		Circle3.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle3+36)
MOVT	R0, #hi_addr(_Circle3+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2356 :: 		Circle3.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle3+40)
MOVT	R0, #hi_addr(_Circle3+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2357 :: 		Circle3.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle3+44)
MOVT	R0, #hi_addr(_Circle3+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2359 :: 		Label4.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Label4+0)
MOVT	R0, #hi_addr(_Label4+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2360 :: 		Label4.Order           = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_Label4+4)
MOVT	R0, #hi_addr(_Label4+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2361 :: 		Label4.Left            = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Label4+6)
MOVT	R0, #hi_addr(_Label4+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2362 :: 		Label4.Top             = 101;
MOVS	R1, #101
MOVW	R0, #lo_addr(_Label4+8)
MOVT	R0, #hi_addr(_Label4+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2363 :: 		Label4.Width           = 65;
MOVS	R1, #65
MOVW	R0, #lo_addr(_Label4+10)
MOVT	R0, #hi_addr(_Label4+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2364 :: 		Label4.Height          = 21;
MOVS	R1, #21
MOVW	R0, #lo_addr(_Label4+12)
MOVT	R0, #hi_addr(_Label4+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2365 :: 		Label4.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label4+27)
MOVT	R0, #hi_addr(_Label4+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2366 :: 		Label4.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label4+28)
MOVT	R0, #hi_addr(_Label4+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2367 :: 		Label4.Caption         = Label4_Caption;
MOVW	R1, #lo_addr(_Label4_Caption+0)
MOVT	R1, #hi_addr(_Label4_Caption+0)
MOVW	R0, #lo_addr(_Label4+16)
MOVT	R0, #hi_addr(_Label4+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2368 :: 		Label4.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_Label4+20)
MOVT	R0, #hi_addr(_Label4+20)
STR	R4, [R0, #0]
;BPWasher_driver.c,2369 :: 		Label4.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Label4+24)
MOVT	R0, #hi_addr(_Label4+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2370 :: 		Label4.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label4+26)
MOVT	R0, #hi_addr(_Label4+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2371 :: 		Label4.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label4+32)
MOVT	R0, #hi_addr(_Label4+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2372 :: 		Label4.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label4+36)
MOVT	R0, #hi_addr(_Label4+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2373 :: 		Label4.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label4+40)
MOVT	R0, #hi_addr(_Label4+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2374 :: 		Label4.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label4+44)
MOVT	R0, #hi_addr(_Label4+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2376 :: 		Circle4.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Circle4+0)
MOVT	R0, #hi_addr(_Circle4+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2377 :: 		Circle4.Order           = 9;
MOVS	R1, #9
MOVW	R0, #lo_addr(_Circle4+4)
MOVT	R0, #hi_addr(_Circle4+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2378 :: 		Circle4.Left            = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_Circle4+6)
MOVT	R0, #hi_addr(_Circle4+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2379 :: 		Circle4.Top             = 134;
MOVS	R1, #134
MOVW	R0, #lo_addr(_Circle4+8)
MOVT	R0, #hi_addr(_Circle4+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2380 :: 		Circle4.Radius          = 12;
MOVS	R1, #12
MOVW	R0, #lo_addr(_Circle4+10)
MOVT	R0, #hi_addr(_Circle4+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2381 :: 		Circle4.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle4+12)
MOVT	R0, #hi_addr(_Circle4+12)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2382 :: 		Circle4.Pen_Color       = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_Circle4+14)
MOVT	R0, #hi_addr(_Circle4+14)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2383 :: 		Circle4.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle4+16)
MOVT	R0, #hi_addr(_Circle4+16)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2384 :: 		Circle4.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle4+17)
MOVT	R0, #hi_addr(_Circle4+17)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2385 :: 		Circle4.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle4+18)
MOVT	R0, #hi_addr(_Circle4+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2386 :: 		Circle4.Gradient        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle4+19)
MOVT	R0, #hi_addr(_Circle4+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2387 :: 		Circle4.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle4+20)
MOVT	R0, #hi_addr(_Circle4+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2388 :: 		Circle4.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Circle4+22)
MOVT	R0, #hi_addr(_Circle4+22)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2389 :: 		Circle4.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Circle4+24)
MOVT	R0, #hi_addr(_Circle4+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2390 :: 		Circle4.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Circle4+26)
MOVT	R0, #hi_addr(_Circle4+26)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2391 :: 		Circle4.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle4+28)
MOVT	R0, #hi_addr(_Circle4+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2392 :: 		Circle4.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Circle4+30)
MOVT	R0, #hi_addr(_Circle4+30)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2393 :: 		Circle4.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle4+32)
MOVT	R0, #hi_addr(_Circle4+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2394 :: 		Circle4.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle4+36)
MOVT	R0, #hi_addr(_Circle4+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2395 :: 		Circle4.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle4+40)
MOVT	R0, #hi_addr(_Circle4+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2396 :: 		Circle4.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle4+44)
MOVT	R0, #hi_addr(_Circle4+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2398 :: 		Label5.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Label5+0)
MOVT	R0, #hi_addr(_Label5+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2399 :: 		Label5.Order           = 10;
MOVS	R1, #10
MOVW	R0, #lo_addr(_Label5+4)
MOVT	R0, #hi_addr(_Label5+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2400 :: 		Label5.Left            = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Label5+6)
MOVT	R0, #hi_addr(_Label5+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2401 :: 		Label5.Top             = 135;
MOVS	R1, #135
MOVW	R0, #lo_addr(_Label5+8)
MOVT	R0, #hi_addr(_Label5+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2402 :: 		Label5.Width           = 81;
MOVS	R1, #81
MOVW	R0, #lo_addr(_Label5+10)
MOVT	R0, #hi_addr(_Label5+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2403 :: 		Label5.Height          = 21;
MOVS	R1, #21
MOVW	R0, #lo_addr(_Label5+12)
MOVT	R0, #hi_addr(_Label5+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2404 :: 		Label5.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label5+27)
MOVT	R0, #hi_addr(_Label5+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2405 :: 		Label5.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label5+28)
MOVT	R0, #hi_addr(_Label5+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2406 :: 		Label5.Caption         = Label5_Caption;
MOVW	R1, #lo_addr(_Label5_Caption+0)
MOVT	R1, #hi_addr(_Label5_Caption+0)
MOVW	R0, #lo_addr(_Label5+16)
MOVT	R0, #hi_addr(_Label5+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2407 :: 		Label5.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_Label5+20)
MOVT	R0, #hi_addr(_Label5+20)
STR	R4, [R0, #0]
;BPWasher_driver.c,2408 :: 		Label5.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Label5+24)
MOVT	R0, #hi_addr(_Label5+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2409 :: 		Label5.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label5+26)
MOVT	R0, #hi_addr(_Label5+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2410 :: 		Label5.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label5+32)
MOVT	R0, #hi_addr(_Label5+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2411 :: 		Label5.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label5+36)
MOVT	R0, #hi_addr(_Label5+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2412 :: 		Label5.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label5+40)
MOVT	R0, #hi_addr(_Label5+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2413 :: 		Label5.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label5+44)
MOVT	R0, #hi_addr(_Label5+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2415 :: 		Circle5.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Circle5+0)
MOVT	R0, #hi_addr(_Circle5+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2416 :: 		Circle5.Order           = 11;
MOVS	R1, #11
MOVW	R0, #lo_addr(_Circle5+4)
MOVT	R0, #hi_addr(_Circle5+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2417 :: 		Circle5.Left            = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_Circle5+6)
MOVT	R0, #hi_addr(_Circle5+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2418 :: 		Circle5.Top             = 168;
MOVS	R1, #168
MOVW	R0, #lo_addr(_Circle5+8)
MOVT	R0, #hi_addr(_Circle5+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2419 :: 		Circle5.Radius          = 12;
MOVS	R1, #12
MOVW	R0, #lo_addr(_Circle5+10)
MOVT	R0, #hi_addr(_Circle5+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2420 :: 		Circle5.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle5+12)
MOVT	R0, #hi_addr(_Circle5+12)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2421 :: 		Circle5.Pen_Color       = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_Circle5+14)
MOVT	R0, #hi_addr(_Circle5+14)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2422 :: 		Circle5.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle5+16)
MOVT	R0, #hi_addr(_Circle5+16)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2423 :: 		Circle5.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle5+17)
MOVT	R0, #hi_addr(_Circle5+17)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2424 :: 		Circle5.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle5+18)
MOVT	R0, #hi_addr(_Circle5+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2425 :: 		Circle5.Gradient        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle5+19)
MOVT	R0, #hi_addr(_Circle5+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2426 :: 		Circle5.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle5+20)
MOVT	R0, #hi_addr(_Circle5+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2427 :: 		Circle5.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Circle5+22)
MOVT	R0, #hi_addr(_Circle5+22)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2428 :: 		Circle5.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Circle5+24)
MOVT	R0, #hi_addr(_Circle5+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2429 :: 		Circle5.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Circle5+26)
MOVT	R0, #hi_addr(_Circle5+26)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2430 :: 		Circle5.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle5+28)
MOVT	R0, #hi_addr(_Circle5+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2431 :: 		Circle5.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Circle5+30)
MOVT	R0, #hi_addr(_Circle5+30)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2432 :: 		Circle5.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle5+32)
MOVT	R0, #hi_addr(_Circle5+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2433 :: 		Circle5.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle5+36)
MOVT	R0, #hi_addr(_Circle5+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2434 :: 		Circle5.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle5+40)
MOVT	R0, #hi_addr(_Circle5+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2435 :: 		Circle5.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle5+44)
MOVT	R0, #hi_addr(_Circle5+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2437 :: 		Label6.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Label6+0)
MOVT	R0, #hi_addr(_Label6+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2438 :: 		Label6.Order           = 12;
MOVS	R1, #12
MOVW	R0, #lo_addr(_Label6+4)
MOVT	R0, #hi_addr(_Label6+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2439 :: 		Label6.Left            = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Label6+6)
MOVT	R0, #hi_addr(_Label6+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2440 :: 		Label6.Top             = 168;
MOVS	R1, #168
MOVW	R0, #lo_addr(_Label6+8)
MOVT	R0, #hi_addr(_Label6+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2441 :: 		Label6.Width           = 86;
MOVS	R1, #86
MOVW	R0, #lo_addr(_Label6+10)
MOVT	R0, #hi_addr(_Label6+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2442 :: 		Label6.Height          = 21;
MOVS	R1, #21
MOVW	R0, #lo_addr(_Label6+12)
MOVT	R0, #hi_addr(_Label6+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2443 :: 		Label6.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label6+27)
MOVT	R0, #hi_addr(_Label6+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2444 :: 		Label6.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label6+28)
MOVT	R0, #hi_addr(_Label6+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2445 :: 		Label6.Caption         = Label6_Caption;
MOVW	R1, #lo_addr(_Label6_Caption+0)
MOVT	R1, #hi_addr(_Label6_Caption+0)
MOVW	R0, #lo_addr(_Label6+16)
MOVT	R0, #hi_addr(_Label6+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2446 :: 		Label6.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_Label6+20)
MOVT	R0, #hi_addr(_Label6+20)
STR	R4, [R0, #0]
;BPWasher_driver.c,2447 :: 		Label6.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Label6+24)
MOVT	R0, #hi_addr(_Label6+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2448 :: 		Label6.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label6+26)
MOVT	R0, #hi_addr(_Label6+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2449 :: 		Label6.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label6+32)
MOVT	R0, #hi_addr(_Label6+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2450 :: 		Label6.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label6+36)
MOVT	R0, #hi_addr(_Label6+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2451 :: 		Label6.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label6+40)
MOVT	R0, #hi_addr(_Label6+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2452 :: 		Label6.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label6+44)
MOVT	R0, #hi_addr(_Label6+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2454 :: 		Circle6.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Circle6+0)
MOVT	R0, #hi_addr(_Circle6+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2455 :: 		Circle6.Order           = 13;
MOVS	R1, #13
MOVW	R0, #lo_addr(_Circle6+4)
MOVT	R0, #hi_addr(_Circle6+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2456 :: 		Circle6.Left            = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_Circle6+6)
MOVT	R0, #hi_addr(_Circle6+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2457 :: 		Circle6.Top             = 201;
MOVS	R1, #201
MOVW	R0, #lo_addr(_Circle6+8)
MOVT	R0, #hi_addr(_Circle6+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2458 :: 		Circle6.Radius          = 12;
MOVS	R1, #12
MOVW	R0, #lo_addr(_Circle6+10)
MOVT	R0, #hi_addr(_Circle6+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2459 :: 		Circle6.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle6+12)
MOVT	R0, #hi_addr(_Circle6+12)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2460 :: 		Circle6.Pen_Color       = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_Circle6+14)
MOVT	R0, #hi_addr(_Circle6+14)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2461 :: 		Circle6.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle6+16)
MOVT	R0, #hi_addr(_Circle6+16)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2462 :: 		Circle6.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle6+17)
MOVT	R0, #hi_addr(_Circle6+17)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2463 :: 		Circle6.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle6+18)
MOVT	R0, #hi_addr(_Circle6+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2464 :: 		Circle6.Gradient        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle6+19)
MOVT	R0, #hi_addr(_Circle6+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2465 :: 		Circle6.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle6+20)
MOVT	R0, #hi_addr(_Circle6+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2466 :: 		Circle6.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Circle6+22)
MOVT	R0, #hi_addr(_Circle6+22)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2467 :: 		Circle6.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Circle6+24)
MOVT	R0, #hi_addr(_Circle6+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2468 :: 		Circle6.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Circle6+26)
MOVT	R0, #hi_addr(_Circle6+26)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2469 :: 		Circle6.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Circle6+28)
MOVT	R0, #hi_addr(_Circle6+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2470 :: 		Circle6.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Circle6+30)
MOVT	R0, #hi_addr(_Circle6+30)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2471 :: 		Circle6.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle6+32)
MOVT	R0, #hi_addr(_Circle6+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2472 :: 		Circle6.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle6+36)
MOVT	R0, #hi_addr(_Circle6+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2473 :: 		Circle6.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle6+40)
MOVT	R0, #hi_addr(_Circle6+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2474 :: 		Circle6.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Circle6+44)
MOVT	R0, #hi_addr(_Circle6+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2476 :: 		Label7.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Label7+0)
MOVT	R0, #hi_addr(_Label7+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2477 :: 		Label7.Order           = 14;
MOVS	R1, #14
MOVW	R0, #lo_addr(_Label7+4)
MOVT	R0, #hi_addr(_Label7+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2478 :: 		Label7.Left            = 38;
MOVS	R1, #38
MOVW	R0, #lo_addr(_Label7+6)
MOVT	R0, #hi_addr(_Label7+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2479 :: 		Label7.Top             = 201;
MOVS	R1, #201
MOVW	R0, #lo_addr(_Label7+8)
MOVT	R0, #hi_addr(_Label7+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2480 :: 		Label7.Width           = 101;
MOVS	R1, #101
MOVW	R0, #lo_addr(_Label7+10)
MOVT	R0, #hi_addr(_Label7+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2481 :: 		Label7.Height          = 21;
MOVS	R1, #21
MOVW	R0, #lo_addr(_Label7+12)
MOVT	R0, #hi_addr(_Label7+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2482 :: 		Label7.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label7+27)
MOVT	R0, #hi_addr(_Label7+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2483 :: 		Label7.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label7+28)
MOVT	R0, #hi_addr(_Label7+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2484 :: 		Label7.Caption         = Label7_Caption;
MOVW	R1, #lo_addr(_Label7_Caption+0)
MOVT	R1, #hi_addr(_Label7_Caption+0)
MOVW	R0, #lo_addr(_Label7+16)
MOVT	R0, #hi_addr(_Label7+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2485 :: 		Label7.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_Label7+20)
MOVT	R0, #hi_addr(_Label7+20)
STR	R4, [R0, #0]
;BPWasher_driver.c,2486 :: 		Label7.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Label7+24)
MOVT	R0, #hi_addr(_Label7+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2487 :: 		Label7.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label7+26)
MOVT	R0, #hi_addr(_Label7+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2488 :: 		Label7.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label7+32)
MOVT	R0, #hi_addr(_Label7+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2489 :: 		Label7.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label7+36)
MOVT	R0, #hi_addr(_Label7+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2490 :: 		Label7.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label7+40)
MOVT	R0, #hi_addr(_Label7+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2491 :: 		Label7.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label7+44)
MOVT	R0, #hi_addr(_Label7+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2493 :: 		CheckBox1.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_CheckBox1+0)
MOVT	R0, #hi_addr(_CheckBox1+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2494 :: 		CheckBox1.Order           = 15;
MOVS	R1, #15
MOVW	R0, #lo_addr(_CheckBox1+4)
MOVT	R0, #hi_addr(_CheckBox1+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2495 :: 		CheckBox1.Left            = 171;
MOVS	R1, #171
MOVW	R0, #lo_addr(_CheckBox1+6)
MOVT	R0, #hi_addr(_CheckBox1+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2496 :: 		CheckBox1.Top             = 34;
MOVS	R1, #34
MOVW	R0, #lo_addr(_CheckBox1+8)
MOVT	R0, #hi_addr(_CheckBox1+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2497 :: 		CheckBox1.Width           = 105;
MOVS	R1, #105
MOVW	R0, #lo_addr(_CheckBox1+10)
MOVT	R0, #hi_addr(_CheckBox1+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2498 :: 		CheckBox1.Height          = 24;
MOVS	R1, #24
MOVW	R0, #lo_addr(_CheckBox1+12)
MOVT	R0, #hi_addr(_CheckBox1+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2499 :: 		CheckBox1.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox1+14)
MOVT	R0, #hi_addr(_CheckBox1+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2500 :: 		CheckBox1.Pen_Color       = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_CheckBox1+16)
MOVT	R0, #hi_addr(_CheckBox1+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2501 :: 		CheckBox1.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox1+18)
MOVT	R0, #hi_addr(_CheckBox1+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2502 :: 		CheckBox1.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox1+19)
MOVT	R0, #hi_addr(_CheckBox1+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2503 :: 		CheckBox1.Checked         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox1+20)
MOVT	R0, #hi_addr(_CheckBox1+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2504 :: 		CheckBox1.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox1+22)
MOVT	R0, #hi_addr(_CheckBox1+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2505 :: 		CheckBox1.Caption         = CheckBox1_Caption;
MOVW	R1, #lo_addr(_CheckBox1_Caption+0)
MOVT	R1, #hi_addr(_CheckBox1_Caption+0)
MOVW	R0, #lo_addr(_CheckBox1+24)
MOVT	R0, #hi_addr(_CheckBox1+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2506 :: 		CheckBox1.TextAlign       = _taLeft;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox1+28)
MOVT	R0, #hi_addr(_CheckBox1+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2507 :: 		CheckBox1.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_CheckBox1+32)
MOVT	R0, #hi_addr(_CheckBox1+32)
STR	R4, [R0, #0]
;BPWasher_driver.c,2508 :: 		CheckBox1.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox1+48)
MOVT	R0, #hi_addr(_CheckBox1+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2509 :: 		CheckBox1.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_CheckBox1+36)
MOVT	R0, #hi_addr(_CheckBox1+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2510 :: 		CheckBox1.Gradient        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox1+38)
MOVT	R0, #hi_addr(_CheckBox1+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2511 :: 		CheckBox1.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox1+39)
MOVT	R0, #hi_addr(_CheckBox1+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2512 :: 		CheckBox1.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_CheckBox1+40)
MOVT	R0, #hi_addr(_CheckBox1+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2513 :: 		CheckBox1.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_CheckBox1+42)
MOVT	R0, #hi_addr(_CheckBox1+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2514 :: 		CheckBox1.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_CheckBox1+44)
MOVT	R0, #hi_addr(_CheckBox1+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2515 :: 		CheckBox1.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_CheckBox1+50)
MOVT	R0, #hi_addr(_CheckBox1+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2516 :: 		CheckBox1.Rounded         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox1+46)
MOVT	R0, #hi_addr(_CheckBox1+46)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2517 :: 		CheckBox1.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_CheckBox1+47)
MOVT	R0, #hi_addr(_CheckBox1+47)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2518 :: 		CheckBox1.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox1+52)
MOVT	R0, #hi_addr(_CheckBox1+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2519 :: 		CheckBox1.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox1+56)
MOVT	R0, #hi_addr(_CheckBox1+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2520 :: 		CheckBox1.OnClickPtr      = SetOutput;
MOVW	R1, #lo_addr(_SetOutput+0)
MOVT	R1, #hi_addr(_SetOutput+0)
MOVW	R0, #lo_addr(_CheckBox1+60)
MOVT	R0, #hi_addr(_CheckBox1+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2521 :: 		CheckBox1.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox1+64)
MOVT	R0, #hi_addr(_CheckBox1+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2522 :: 		CheckBox1.Bit             = 0x0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox1+21)
MOVT	R0, #hi_addr(_CheckBox1+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2524 :: 		CheckBox2.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_CheckBox2+0)
MOVT	R0, #hi_addr(_CheckBox2+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2525 :: 		CheckBox2.Order           = 16;
MOVS	R1, #16
MOVW	R0, #lo_addr(_CheckBox2+4)
MOVT	R0, #hi_addr(_CheckBox2+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2526 :: 		CheckBox2.Left            = 171;
MOVS	R1, #171
MOVW	R0, #lo_addr(_CheckBox2+6)
MOVT	R0, #hi_addr(_CheckBox2+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2527 :: 		CheckBox2.Top             = 67;
MOVS	R1, #67
MOVW	R0, #lo_addr(_CheckBox2+8)
MOVT	R0, #hi_addr(_CheckBox2+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2528 :: 		CheckBox2.Width           = 145;
MOVS	R1, #145
MOVW	R0, #lo_addr(_CheckBox2+10)
MOVT	R0, #hi_addr(_CheckBox2+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2529 :: 		CheckBox2.Height          = 24;
MOVS	R1, #24
MOVW	R0, #lo_addr(_CheckBox2+12)
MOVT	R0, #hi_addr(_CheckBox2+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2530 :: 		CheckBox2.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox2+14)
MOVT	R0, #hi_addr(_CheckBox2+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2531 :: 		CheckBox2.Pen_Color       = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_CheckBox2+16)
MOVT	R0, #hi_addr(_CheckBox2+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2532 :: 		CheckBox2.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox2+18)
MOVT	R0, #hi_addr(_CheckBox2+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2533 :: 		CheckBox2.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox2+19)
MOVT	R0, #hi_addr(_CheckBox2+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2534 :: 		CheckBox2.Checked         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox2+20)
MOVT	R0, #hi_addr(_CheckBox2+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2535 :: 		CheckBox2.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox2+22)
MOVT	R0, #hi_addr(_CheckBox2+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2536 :: 		CheckBox2.Caption         = CheckBox2_Caption;
MOVW	R1, #lo_addr(_CheckBox2_Caption+0)
MOVT	R1, #hi_addr(_CheckBox2_Caption+0)
MOVW	R0, #lo_addr(_CheckBox2+24)
MOVT	R0, #hi_addr(_CheckBox2+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2537 :: 		CheckBox2.TextAlign       = _taLeft;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox2+28)
MOVT	R0, #hi_addr(_CheckBox2+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2538 :: 		CheckBox2.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_CheckBox2+32)
MOVT	R0, #hi_addr(_CheckBox2+32)
STR	R4, [R0, #0]
;BPWasher_driver.c,2539 :: 		CheckBox2.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox2+48)
MOVT	R0, #hi_addr(_CheckBox2+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2540 :: 		CheckBox2.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_CheckBox2+36)
MOVT	R0, #hi_addr(_CheckBox2+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2541 :: 		CheckBox2.Gradient        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox2+38)
MOVT	R0, #hi_addr(_CheckBox2+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2542 :: 		CheckBox2.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox2+39)
MOVT	R0, #hi_addr(_CheckBox2+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2543 :: 		CheckBox2.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_CheckBox2+40)
MOVT	R0, #hi_addr(_CheckBox2+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2544 :: 		CheckBox2.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_CheckBox2+42)
MOVT	R0, #hi_addr(_CheckBox2+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2545 :: 		CheckBox2.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_CheckBox2+44)
MOVT	R0, #hi_addr(_CheckBox2+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2546 :: 		CheckBox2.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_CheckBox2+50)
MOVT	R0, #hi_addr(_CheckBox2+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2547 :: 		CheckBox2.Rounded         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox2+46)
MOVT	R0, #hi_addr(_CheckBox2+46)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2548 :: 		CheckBox2.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_CheckBox2+47)
MOVT	R0, #hi_addr(_CheckBox2+47)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2549 :: 		CheckBox2.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox2+52)
MOVT	R0, #hi_addr(_CheckBox2+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2550 :: 		CheckBox2.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox2+56)
MOVT	R0, #hi_addr(_CheckBox2+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2551 :: 		CheckBox2.OnClickPtr      = SetOutput;
MOVW	R1, #lo_addr(_SetOutput+0)
MOVT	R1, #hi_addr(_SetOutput+0)
MOVW	R0, #lo_addr(_CheckBox2+60)
MOVT	R0, #hi_addr(_CheckBox2+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2552 :: 		CheckBox2.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox2+64)
MOVT	R0, #hi_addr(_CheckBox2+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2553 :: 		CheckBox2.Bit             = 0x1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox2+21)
MOVT	R0, #hi_addr(_CheckBox2+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2555 :: 		CheckBox3.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_CheckBox3+0)
MOVT	R0, #hi_addr(_CheckBox3+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2556 :: 		CheckBox3.Order           = 17;
MOVS	R1, #17
MOVW	R0, #lo_addr(_CheckBox3+4)
MOVT	R0, #hi_addr(_CheckBox3+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2557 :: 		CheckBox3.Left            = 171;
MOVS	R1, #171
MOVW	R0, #lo_addr(_CheckBox3+6)
MOVT	R0, #hi_addr(_CheckBox3+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2558 :: 		CheckBox3.Top             = 99;
MOVS	R1, #99
MOVW	R0, #lo_addr(_CheckBox3+8)
MOVT	R0, #hi_addr(_CheckBox3+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2559 :: 		CheckBox3.Width           = 93;
MOVS	R1, #93
MOVW	R0, #lo_addr(_CheckBox3+10)
MOVT	R0, #hi_addr(_CheckBox3+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2560 :: 		CheckBox3.Height          = 24;
MOVS	R1, #24
MOVW	R0, #lo_addr(_CheckBox3+12)
MOVT	R0, #hi_addr(_CheckBox3+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2561 :: 		CheckBox3.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox3+14)
MOVT	R0, #hi_addr(_CheckBox3+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2562 :: 		CheckBox3.Pen_Color       = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_CheckBox3+16)
MOVT	R0, #hi_addr(_CheckBox3+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2563 :: 		CheckBox3.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox3+18)
MOVT	R0, #hi_addr(_CheckBox3+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2564 :: 		CheckBox3.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox3+19)
MOVT	R0, #hi_addr(_CheckBox3+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2565 :: 		CheckBox3.Checked         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox3+20)
MOVT	R0, #hi_addr(_CheckBox3+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2566 :: 		CheckBox3.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox3+22)
MOVT	R0, #hi_addr(_CheckBox3+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2567 :: 		CheckBox3.Caption         = CheckBox3_Caption;
MOVW	R1, #lo_addr(_CheckBox3_Caption+0)
MOVT	R1, #hi_addr(_CheckBox3_Caption+0)
MOVW	R0, #lo_addr(_CheckBox3+24)
MOVT	R0, #hi_addr(_CheckBox3+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2568 :: 		CheckBox3.TextAlign       = _taLeft;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox3+28)
MOVT	R0, #hi_addr(_CheckBox3+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2569 :: 		CheckBox3.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_CheckBox3+32)
MOVT	R0, #hi_addr(_CheckBox3+32)
STR	R4, [R0, #0]
;BPWasher_driver.c,2570 :: 		CheckBox3.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox3+48)
MOVT	R0, #hi_addr(_CheckBox3+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2571 :: 		CheckBox3.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_CheckBox3+36)
MOVT	R0, #hi_addr(_CheckBox3+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2572 :: 		CheckBox3.Gradient        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox3+38)
MOVT	R0, #hi_addr(_CheckBox3+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2573 :: 		CheckBox3.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox3+39)
MOVT	R0, #hi_addr(_CheckBox3+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2574 :: 		CheckBox3.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_CheckBox3+40)
MOVT	R0, #hi_addr(_CheckBox3+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2575 :: 		CheckBox3.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_CheckBox3+42)
MOVT	R0, #hi_addr(_CheckBox3+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2576 :: 		CheckBox3.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_CheckBox3+44)
MOVT	R0, #hi_addr(_CheckBox3+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2577 :: 		CheckBox3.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_CheckBox3+50)
MOVT	R0, #hi_addr(_CheckBox3+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2578 :: 		CheckBox3.Rounded         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox3+46)
MOVT	R0, #hi_addr(_CheckBox3+46)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2579 :: 		CheckBox3.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_CheckBox3+47)
MOVT	R0, #hi_addr(_CheckBox3+47)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2580 :: 		CheckBox3.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox3+52)
MOVT	R0, #hi_addr(_CheckBox3+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2581 :: 		CheckBox3.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox3+56)
MOVT	R0, #hi_addr(_CheckBox3+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2582 :: 		CheckBox3.OnClickPtr      = SetOutput;
MOVW	R1, #lo_addr(_SetOutput+0)
MOVT	R1, #hi_addr(_SetOutput+0)
MOVW	R0, #lo_addr(_CheckBox3+60)
MOVT	R0, #hi_addr(_CheckBox3+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2583 :: 		CheckBox3.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox3+64)
MOVT	R0, #hi_addr(_CheckBox3+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2584 :: 		CheckBox3.Bit             = 0x2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_CheckBox3+21)
MOVT	R0, #hi_addr(_CheckBox3+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2586 :: 		CheckBox4.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_CheckBox4+0)
MOVT	R0, #hi_addr(_CheckBox4+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2587 :: 		CheckBox4.Order           = 18;
MOVS	R1, #18
MOVW	R0, #lo_addr(_CheckBox4+4)
MOVT	R0, #hi_addr(_CheckBox4+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2588 :: 		CheckBox4.Left            = 171;
MOVS	R1, #171
MOVW	R0, #lo_addr(_CheckBox4+6)
MOVT	R0, #hi_addr(_CheckBox4+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2589 :: 		CheckBox4.Top             = 132;
MOVS	R1, #132
MOVW	R0, #lo_addr(_CheckBox4+8)
MOVT	R0, #hi_addr(_CheckBox4+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2590 :: 		CheckBox4.Width           = 108;
MOVS	R1, #108
MOVW	R0, #lo_addr(_CheckBox4+10)
MOVT	R0, #hi_addr(_CheckBox4+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2591 :: 		CheckBox4.Height          = 24;
MOVS	R1, #24
MOVW	R0, #lo_addr(_CheckBox4+12)
MOVT	R0, #hi_addr(_CheckBox4+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2592 :: 		CheckBox4.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox4+14)
MOVT	R0, #hi_addr(_CheckBox4+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2593 :: 		CheckBox4.Pen_Color       = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_CheckBox4+16)
MOVT	R0, #hi_addr(_CheckBox4+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2594 :: 		CheckBox4.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox4+18)
MOVT	R0, #hi_addr(_CheckBox4+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2595 :: 		CheckBox4.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox4+19)
MOVT	R0, #hi_addr(_CheckBox4+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2596 :: 		CheckBox4.Checked         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox4+20)
MOVT	R0, #hi_addr(_CheckBox4+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2597 :: 		CheckBox4.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox4+22)
MOVT	R0, #hi_addr(_CheckBox4+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2598 :: 		CheckBox4.Caption         = CheckBox4_Caption;
MOVW	R1, #lo_addr(_CheckBox4_Caption+0)
MOVT	R1, #hi_addr(_CheckBox4_Caption+0)
MOVW	R0, #lo_addr(_CheckBox4+24)
MOVT	R0, #hi_addr(_CheckBox4+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2599 :: 		CheckBox4.TextAlign       = _taLeft;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox4+28)
MOVT	R0, #hi_addr(_CheckBox4+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2600 :: 		CheckBox4.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_CheckBox4+32)
MOVT	R0, #hi_addr(_CheckBox4+32)
STR	R4, [R0, #0]
;BPWasher_driver.c,2601 :: 		CheckBox4.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox4+48)
MOVT	R0, #hi_addr(_CheckBox4+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2602 :: 		CheckBox4.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_CheckBox4+36)
MOVT	R0, #hi_addr(_CheckBox4+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2603 :: 		CheckBox4.Gradient        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox4+38)
MOVT	R0, #hi_addr(_CheckBox4+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2604 :: 		CheckBox4.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox4+39)
MOVT	R0, #hi_addr(_CheckBox4+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2605 :: 		CheckBox4.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_CheckBox4+40)
MOVT	R0, #hi_addr(_CheckBox4+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2606 :: 		CheckBox4.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_CheckBox4+42)
MOVT	R0, #hi_addr(_CheckBox4+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2607 :: 		CheckBox4.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_CheckBox4+44)
MOVT	R0, #hi_addr(_CheckBox4+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2608 :: 		CheckBox4.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_CheckBox4+50)
MOVT	R0, #hi_addr(_CheckBox4+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2609 :: 		CheckBox4.Rounded         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox4+46)
MOVT	R0, #hi_addr(_CheckBox4+46)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2610 :: 		CheckBox4.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_CheckBox4+47)
MOVT	R0, #hi_addr(_CheckBox4+47)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2611 :: 		CheckBox4.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox4+52)
MOVT	R0, #hi_addr(_CheckBox4+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2612 :: 		CheckBox4.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox4+56)
MOVT	R0, #hi_addr(_CheckBox4+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2613 :: 		CheckBox4.OnClickPtr      = SetOutput;
MOVW	R1, #lo_addr(_SetOutput+0)
MOVT	R1, #hi_addr(_SetOutput+0)
MOVW	R0, #lo_addr(_CheckBox4+60)
MOVT	R0, #hi_addr(_CheckBox4+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2614 :: 		CheckBox4.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox4+64)
MOVT	R0, #hi_addr(_CheckBox4+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2615 :: 		CheckBox4.Bit             = 0x3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_CheckBox4+21)
MOVT	R0, #hi_addr(_CheckBox4+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2617 :: 		CheckBox5.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_CheckBox5+0)
MOVT	R0, #hi_addr(_CheckBox5+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2618 :: 		CheckBox5.Order           = 19;
MOVS	R1, #19
MOVW	R0, #lo_addr(_CheckBox5+4)
MOVT	R0, #hi_addr(_CheckBox5+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2619 :: 		CheckBox5.Left            = 171;
MOVS	R1, #171
MOVW	R0, #lo_addr(_CheckBox5+6)
MOVT	R0, #hi_addr(_CheckBox5+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2620 :: 		CheckBox5.Top             = 164;
MOVS	R1, #164
MOVW	R0, #lo_addr(_CheckBox5+8)
MOVT	R0, #hi_addr(_CheckBox5+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2621 :: 		CheckBox5.Width           = 97;
MOVS	R1, #97
MOVW	R0, #lo_addr(_CheckBox5+10)
MOVT	R0, #hi_addr(_CheckBox5+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2622 :: 		CheckBox5.Height          = 24;
MOVS	R1, #24
MOVW	R0, #lo_addr(_CheckBox5+12)
MOVT	R0, #hi_addr(_CheckBox5+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2623 :: 		CheckBox5.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox5+14)
MOVT	R0, #hi_addr(_CheckBox5+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2624 :: 		CheckBox5.Pen_Color       = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_CheckBox5+16)
MOVT	R0, #hi_addr(_CheckBox5+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2625 :: 		CheckBox5.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox5+18)
MOVT	R0, #hi_addr(_CheckBox5+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2626 :: 		CheckBox5.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox5+19)
MOVT	R0, #hi_addr(_CheckBox5+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2627 :: 		CheckBox5.Checked         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox5+20)
MOVT	R0, #hi_addr(_CheckBox5+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2628 :: 		CheckBox5.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox5+22)
MOVT	R0, #hi_addr(_CheckBox5+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2629 :: 		CheckBox5.Caption         = CheckBox5_Caption;
MOVW	R1, #lo_addr(_CheckBox5_Caption+0)
MOVT	R1, #hi_addr(_CheckBox5_Caption+0)
MOVW	R0, #lo_addr(_CheckBox5+24)
MOVT	R0, #hi_addr(_CheckBox5+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2630 :: 		CheckBox5.TextAlign       = _taLeft;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox5+28)
MOVT	R0, #hi_addr(_CheckBox5+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2631 :: 		CheckBox5.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_CheckBox5+32)
MOVT	R0, #hi_addr(_CheckBox5+32)
STR	R4, [R0, #0]
;BPWasher_driver.c,2632 :: 		CheckBox5.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox5+48)
MOVT	R0, #hi_addr(_CheckBox5+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2633 :: 		CheckBox5.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_CheckBox5+36)
MOVT	R0, #hi_addr(_CheckBox5+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2634 :: 		CheckBox5.Gradient        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox5+38)
MOVT	R0, #hi_addr(_CheckBox5+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2635 :: 		CheckBox5.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox5+39)
MOVT	R0, #hi_addr(_CheckBox5+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2636 :: 		CheckBox5.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_CheckBox5+40)
MOVT	R0, #hi_addr(_CheckBox5+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2637 :: 		CheckBox5.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_CheckBox5+42)
MOVT	R0, #hi_addr(_CheckBox5+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2638 :: 		CheckBox5.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_CheckBox5+44)
MOVT	R0, #hi_addr(_CheckBox5+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2639 :: 		CheckBox5.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_CheckBox5+50)
MOVT	R0, #hi_addr(_CheckBox5+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2640 :: 		CheckBox5.Rounded         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox5+46)
MOVT	R0, #hi_addr(_CheckBox5+46)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2641 :: 		CheckBox5.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_CheckBox5+47)
MOVT	R0, #hi_addr(_CheckBox5+47)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2642 :: 		CheckBox5.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox5+52)
MOVT	R0, #hi_addr(_CheckBox5+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2643 :: 		CheckBox5.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox5+56)
MOVT	R0, #hi_addr(_CheckBox5+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2644 :: 		CheckBox5.OnClickPtr      = SetOutput;
MOVW	R1, #lo_addr(_SetOutput+0)
MOVT	R1, #hi_addr(_SetOutput+0)
MOVW	R0, #lo_addr(_CheckBox5+60)
MOVT	R0, #hi_addr(_CheckBox5+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2645 :: 		CheckBox5.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox5+64)
MOVT	R0, #hi_addr(_CheckBox5+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2646 :: 		CheckBox5.Bit             = 0x4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_CheckBox5+21)
MOVT	R0, #hi_addr(_CheckBox5+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2648 :: 		CheckBox6.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_CheckBox6+0)
MOVT	R0, #hi_addr(_CheckBox6+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2649 :: 		CheckBox6.Order           = 20;
MOVS	R1, #20
MOVW	R0, #lo_addr(_CheckBox6+4)
MOVT	R0, #hi_addr(_CheckBox6+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2650 :: 		CheckBox6.Left            = 171;
MOVS	R1, #171
MOVW	R0, #lo_addr(_CheckBox6+6)
MOVT	R0, #hi_addr(_CheckBox6+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2651 :: 		CheckBox6.Top             = 197;
MOVS	R1, #197
MOVW	R0, #lo_addr(_CheckBox6+8)
MOVT	R0, #hi_addr(_CheckBox6+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2652 :: 		CheckBox6.Width           = 72;
MOVS	R1, #72
MOVW	R0, #lo_addr(_CheckBox6+10)
MOVT	R0, #hi_addr(_CheckBox6+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2653 :: 		CheckBox6.Height          = 24;
MOVS	R1, #24
MOVW	R0, #lo_addr(_CheckBox6+12)
MOVT	R0, #hi_addr(_CheckBox6+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2654 :: 		CheckBox6.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox6+14)
MOVT	R0, #hi_addr(_CheckBox6+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2655 :: 		CheckBox6.Pen_Color       = 0x0273;
MOVW	R1, #627
MOVW	R0, #lo_addr(_CheckBox6+16)
MOVT	R0, #hi_addr(_CheckBox6+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2656 :: 		CheckBox6.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox6+18)
MOVT	R0, #hi_addr(_CheckBox6+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2657 :: 		CheckBox6.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox6+19)
MOVT	R0, #hi_addr(_CheckBox6+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2658 :: 		CheckBox6.Checked         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox6+20)
MOVT	R0, #hi_addr(_CheckBox6+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2659 :: 		CheckBox6.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox6+22)
MOVT	R0, #hi_addr(_CheckBox6+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2660 :: 		CheckBox6.Caption         = CheckBox6_Caption;
MOVW	R1, #lo_addr(_CheckBox6_Caption+0)
MOVT	R1, #hi_addr(_CheckBox6_Caption+0)
MOVW	R0, #lo_addr(_CheckBox6+24)
MOVT	R0, #hi_addr(_CheckBox6+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2661 :: 		CheckBox6.TextAlign       = _taLeft;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox6+28)
MOVT	R0, #hi_addr(_CheckBox6+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2662 :: 		CheckBox6.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_CheckBox6+32)
MOVT	R0, #hi_addr(_CheckBox6+32)
STR	R4, [R0, #0]
;BPWasher_driver.c,2663 :: 		CheckBox6.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox6+48)
MOVT	R0, #hi_addr(_CheckBox6+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2664 :: 		CheckBox6.Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_CheckBox6+36)
MOVT	R0, #hi_addr(_CheckBox6+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2665 :: 		CheckBox6.Gradient        = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox6+38)
MOVT	R0, #hi_addr(_CheckBox6+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2666 :: 		CheckBox6.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox6+39)
MOVT	R0, #hi_addr(_CheckBox6+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2667 :: 		CheckBox6.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_CheckBox6+40)
MOVT	R0, #hi_addr(_CheckBox6+40)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2668 :: 		CheckBox6.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_CheckBox6+42)
MOVT	R0, #hi_addr(_CheckBox6+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2669 :: 		CheckBox6.Color           = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_CheckBox6+44)
MOVT	R0, #hi_addr(_CheckBox6+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2670 :: 		CheckBox6.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_CheckBox6+50)
MOVT	R0, #hi_addr(_CheckBox6+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2671 :: 		CheckBox6.Rounded         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_CheckBox6+46)
MOVT	R0, #hi_addr(_CheckBox6+46)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2672 :: 		CheckBox6.Corner_Radius   = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_CheckBox6+47)
MOVT	R0, #hi_addr(_CheckBox6+47)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2673 :: 		CheckBox6.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox6+52)
MOVT	R0, #hi_addr(_CheckBox6+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2674 :: 		CheckBox6.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox6+56)
MOVT	R0, #hi_addr(_CheckBox6+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2675 :: 		CheckBox6.OnClickPtr      = SetOutput;
MOVW	R1, #lo_addr(_SetOutput+0)
MOVT	R1, #hi_addr(_SetOutput+0)
MOVW	R0, #lo_addr(_CheckBox6+60)
MOVT	R0, #hi_addr(_CheckBox6+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2676 :: 		CheckBox6.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CheckBox6+64)
MOVT	R0, #hi_addr(_CheckBox6+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2677 :: 		CheckBox6.Bit             = 0x5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_CheckBox6+21)
MOVT	R0, #hi_addr(_CheckBox6+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2679 :: 		Button1.OwnerScreen     = &Diags;
MOVW	R1, #lo_addr(_Diags+0)
MOVT	R1, #hi_addr(_Diags+0)
MOVW	R0, #lo_addr(_Button1+0)
MOVT	R0, #hi_addr(_Button1+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2680 :: 		Button1.Order           = 21;
MOVS	R1, #21
MOVW	R0, #lo_addr(_Button1+4)
MOVT	R0, #hi_addr(_Button1+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2681 :: 		Button1.Left            = 250;
MOVS	R1, #250
MOVW	R0, #lo_addr(_Button1+6)
MOVT	R0, #hi_addr(_Button1+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2682 :: 		Button1.Top             = 196;
MOVS	R1, #196
MOVW	R0, #lo_addr(_Button1+8)
MOVT	R0, #hi_addr(_Button1+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2683 :: 		Button1.Width           = 59;
MOVS	R1, #59
MOVW	R0, #lo_addr(_Button1+10)
MOVT	R0, #hi_addr(_Button1+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2684 :: 		Button1.Height          = 39;
MOVS	R1, #39
MOVW	R0, #lo_addr(_Button1+12)
MOVT	R0, #hi_addr(_Button1+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2685 :: 		Button1.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Button1+14)
MOVT	R0, #hi_addr(_Button1+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2686 :: 		Button1.Pen_Color       = 0x0010;
MOVS	R1, #16
MOVW	R0, #lo_addr(_Button1+16)
MOVT	R0, #hi_addr(_Button1+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2687 :: 		Button1.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Button1+18)
MOVT	R0, #hi_addr(_Button1+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2688 :: 		Button1.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Button1+19)
MOVT	R0, #hi_addr(_Button1+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2689 :: 		Button1.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Button1+20)
MOVT	R0, #hi_addr(_Button1+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2690 :: 		Button1.Caption         = Button1_Caption;
MOVW	R1, #lo_addr(_Button1_Caption+0)
MOVT	R1, #hi_addr(_Button1_Caption+0)
MOVW	R0, #lo_addr(_Button1+24)
MOVT	R0, #hi_addr(_Button1+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2691 :: 		Button1.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Button1+28)
MOVT	R0, #hi_addr(_Button1+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2692 :: 		Button1.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Button1+29)
MOVT	R0, #hi_addr(_Button1+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2693 :: 		Button1.FontName        = Tahoma16x19_Regular;
MOVW	R0, #lo_addr(_Button1+32)
MOVT	R0, #hi_addr(_Button1+32)
STR	R4, [R0, #0]
;BPWasher_driver.c,2694 :: 		Button1.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Button1+48)
MOVT	R0, #hi_addr(_Button1+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2695 :: 		Button1.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Button1+36)
MOVT	R0, #hi_addr(_Button1+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2696 :: 		Button1.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Button1+38)
MOVT	R0, #hi_addr(_Button1+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2697 :: 		Button1.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Button1+39)
MOVT	R0, #hi_addr(_Button1+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2698 :: 		Button1.Gradient_Orientation = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Button1+40)
MOVT	R0, #hi_addr(_Button1+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2699 :: 		Button1.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Button1+42)
MOVT	R0, #hi_addr(_Button1+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2700 :: 		Button1.Gradient_End_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Button1+44)
MOVT	R0, #hi_addr(_Button1+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2701 :: 		Button1.Color           = 0xFFFF;  //0xC618
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Button1+46)
MOVT	R0, #hi_addr(_Button1+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2702 :: 		Button1.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Button1+50)
MOVT	R0, #hi_addr(_Button1+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2703 :: 		Button1.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Button1+52)
MOVT	R0, #hi_addr(_Button1+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2704 :: 		Button1.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Button1+56)
MOVT	R0, #hi_addr(_Button1+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2705 :: 		Button1.OnClickPtr      = Exit_Pressed;
MOVW	R1, #lo_addr(_Exit_Pressed+0)
MOVT	R1, #hi_addr(_Exit_Pressed+0)
MOVW	R0, #lo_addr(_Button1+60)
MOVT	R0, #hi_addr(_Button1+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2706 :: 		Button1.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Button1+64)
MOVT	R0, #hi_addr(_Button1+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2708 :: 		Image1.OwnerScreen     = &SplashLand;
MOVW	R1, #lo_addr(_SplashLand+0)
MOVT	R1, #hi_addr(_SplashLand+0)
MOVW	R0, #lo_addr(_Image1+0)
MOVT	R0, #hi_addr(_Image1+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2709 :: 		Image1.Order           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image1+4)
MOVT	R0, #hi_addr(_Image1+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2710 :: 		Image1.Left            = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image1+6)
MOVT	R0, #hi_addr(_Image1+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2711 :: 		Image1.Top             = 27;
MOVS	R1, #27
MOVW	R0, #lo_addr(_Image1+8)
MOVT	R0, #hi_addr(_Image1+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2712 :: 		Image1.Width           = 320;
MOVW	R1, #320
MOVW	R0, #lo_addr(_Image1+10)
MOVT	R0, #hi_addr(_Image1+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2713 :: 		Image1.Height          = 214;
MOVS	R1, #214
MOVW	R0, #lo_addr(_Image1+12)
MOVT	R0, #hi_addr(_Image1+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2714 :: 		Image1.Picture_Type    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image1+22)
MOVT	R0, #hi_addr(_Image1+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2715 :: 		Image1.Picture_Ratio   = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image1+23)
MOVT	R0, #hi_addr(_Image1+23)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2716 :: 		Image1.Picture_Name    = waterdripsmallland_bmp;
MOVW	R1, #lo_addr(_waterdripsmallland_bmp+0)
MOVT	R1, #hi_addr(_waterdripsmallland_bmp+0)
MOVW	R0, #lo_addr(_Image1+16)
MOVT	R0, #hi_addr(_Image1+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2717 :: 		Image1.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image1+20)
MOVT	R0, #hi_addr(_Image1+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2718 :: 		Image1.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image1+21)
MOVT	R0, #hi_addr(_Image1+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2719 :: 		Image1.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image1+24)
MOVT	R0, #hi_addr(_Image1+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2720 :: 		Image1.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image1+28)
MOVT	R0, #hi_addr(_Image1+28)
STR	R1, [R0, #0]
;BPWasher_driver.c,2721 :: 		Image1.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image1+32)
MOVT	R0, #hi_addr(_Image1+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2722 :: 		Image1.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image1+36)
MOVT	R0, #hi_addr(_Image1+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2724 :: 		Image2.OwnerScreen     = &SplashLand;
MOVW	R1, #lo_addr(_SplashLand+0)
MOVT	R1, #hi_addr(_SplashLand+0)
MOVW	R0, #lo_addr(_Image2+0)
MOVT	R0, #hi_addr(_Image2+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2725 :: 		Image2.Order           = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image2+4)
MOVT	R0, #hi_addr(_Image2+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2726 :: 		Image2.Left            = 44;
MOVS	R1, #44
MOVW	R0, #lo_addr(_Image2+6)
MOVT	R0, #hi_addr(_Image2+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2727 :: 		Image2.Top             = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image2+8)
MOVT	R0, #hi_addr(_Image2+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2728 :: 		Image2.Width           = 236;
MOVS	R1, #236
MOVW	R0, #lo_addr(_Image2+10)
MOVT	R0, #hi_addr(_Image2+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2729 :: 		Image2.Height          = 33;
MOVS	R1, #33
MOVW	R0, #lo_addr(_Image2+12)
MOVT	R0, #hi_addr(_Image2+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2730 :: 		Image2.Picture_Type    = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image2+22)
MOVT	R0, #hi_addr(_Image2+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2731 :: 		Image2.Picture_Ratio   = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image2+23)
MOVT	R0, #hi_addr(_Image2+23)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2732 :: 		Image2.Picture_Name    = MPBE_Logo_small_jpg;
MOVW	R1, #lo_addr(_MPBE_Logo_small_jpg+0)
MOVT	R1, #hi_addr(_MPBE_Logo_small_jpg+0)
MOVW	R0, #lo_addr(_Image2+16)
MOVT	R0, #hi_addr(_Image2+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2733 :: 		Image2.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image2+20)
MOVT	R0, #hi_addr(_Image2+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2734 :: 		Image2.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image2+21)
MOVT	R0, #hi_addr(_Image2+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2735 :: 		Image2.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image2+24)
MOVT	R0, #hi_addr(_Image2+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2736 :: 		Image2.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image2+28)
MOVT	R0, #hi_addr(_Image2+28)
STR	R1, [R0, #0]
;BPWasher_driver.c,2737 :: 		Image2.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image2+32)
MOVT	R0, #hi_addr(_Image2+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2738 :: 		Image2.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image2+36)
MOVT	R0, #hi_addr(_Image2+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2740 :: 		Box1.OwnerScreen     = &SplashLand;
MOVW	R1, #lo_addr(_SplashLand+0)
MOVT	R1, #hi_addr(_SplashLand+0)
MOVW	R0, #lo_addr(_Box1+0)
MOVT	R0, #hi_addr(_Box1+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2741 :: 		Box1.Order           = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_Box1+4)
MOVT	R0, #hi_addr(_Box1+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2742 :: 		Box1.Left            = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box1+6)
MOVT	R0, #hi_addr(_Box1+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2743 :: 		Box1.Top             = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box1+8)
MOVT	R0, #hi_addr(_Box1+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2744 :: 		Box1.Width           = 44;
MOVS	R1, #44
MOVW	R0, #lo_addr(_Box1+10)
MOVT	R0, #hi_addr(_Box1+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2745 :: 		Box1.Height          = 32;
MOVS	R1, #32
MOVW	R0, #lo_addr(_Box1+12)
MOVT	R0, #hi_addr(_Box1+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2746 :: 		Box1.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Box1+14)
MOVT	R0, #hi_addr(_Box1+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2747 :: 		Box1.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Box1+16)
MOVT	R0, #hi_addr(_Box1+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2748 :: 		Box1.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Box1+18)
MOVT	R0, #hi_addr(_Box1+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2749 :: 		Box1.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Box1+19)
MOVT	R0, #hi_addr(_Box1+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2750 :: 		Box1.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Box1+20)
MOVT	R0, #hi_addr(_Box1+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2751 :: 		Box1.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box1+21)
MOVT	R0, #hi_addr(_Box1+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2752 :: 		Box1.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box1+22)
MOVT	R0, #hi_addr(_Box1+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2753 :: 		Box1.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Box1+24)
MOVT	R0, #hi_addr(_Box1+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2754 :: 		Box1.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Box1+26)
MOVT	R0, #hi_addr(_Box1+26)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2755 :: 		Box1.Color           = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Box1+28)
MOVT	R0, #hi_addr(_Box1+28)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2756 :: 		Box1.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Box1+30)
MOVT	R0, #hi_addr(_Box1+30)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2757 :: 		Box1.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Box1+32)
MOVT	R0, #hi_addr(_Box1+32)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2758 :: 		Box1.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box1+36)
MOVT	R0, #hi_addr(_Box1+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2759 :: 		Box1.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box1+40)
MOVT	R0, #hi_addr(_Box1+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2760 :: 		Box1.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box1+44)
MOVT	R0, #hi_addr(_Box1+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2761 :: 		Box1.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box1+48)
MOVT	R0, #hi_addr(_Box1+48)
STR	R1, [R0, #0]
;BPWasher_driver.c,2763 :: 		Box2.OwnerScreen     = &SplashLand;
MOVW	R1, #lo_addr(_SplashLand+0)
MOVT	R1, #hi_addr(_SplashLand+0)
MOVW	R0, #lo_addr(_Box2+0)
MOVT	R0, #hi_addr(_Box2+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2764 :: 		Box2.Order           = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_Box2+4)
MOVT	R0, #hi_addr(_Box2+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2765 :: 		Box2.Left            = 279;
MOVW	R1, #279
MOVW	R0, #lo_addr(_Box2+6)
MOVT	R0, #hi_addr(_Box2+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2766 :: 		Box2.Top             = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box2+8)
MOVT	R0, #hi_addr(_Box2+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2767 :: 		Box2.Width           = 46;
MOVS	R1, #46
MOVW	R0, #lo_addr(_Box2+10)
MOVT	R0, #hi_addr(_Box2+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2768 :: 		Box2.Height          = 32;
MOVS	R1, #32
MOVW	R0, #lo_addr(_Box2+12)
MOVT	R0, #hi_addr(_Box2+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2769 :: 		Box2.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Box2+14)
MOVT	R0, #hi_addr(_Box2+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2770 :: 		Box2.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Box2+16)
MOVT	R0, #hi_addr(_Box2+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2771 :: 		Box2.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Box2+18)
MOVT	R0, #hi_addr(_Box2+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2772 :: 		Box2.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Box2+19)
MOVT	R0, #hi_addr(_Box2+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2773 :: 		Box2.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Box2+20)
MOVT	R0, #hi_addr(_Box2+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2774 :: 		Box2.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box2+21)
MOVT	R0, #hi_addr(_Box2+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2775 :: 		Box2.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box2+22)
MOVT	R0, #hi_addr(_Box2+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2776 :: 		Box2.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Box2+24)
MOVT	R0, #hi_addr(_Box2+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2777 :: 		Box2.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_Box2+26)
MOVT	R0, #hi_addr(_Box2+26)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2778 :: 		Box2.Color           = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Box2+28)
MOVT	R0, #hi_addr(_Box2+28)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2779 :: 		Box2.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Box2+30)
MOVT	R0, #hi_addr(_Box2+30)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2780 :: 		Box2.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_Box2+32)
MOVT	R0, #hi_addr(_Box2+32)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2781 :: 		Box2.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box2+36)
MOVT	R0, #hi_addr(_Box2+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2782 :: 		Box2.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box2+40)
MOVT	R0, #hi_addr(_Box2+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2783 :: 		Box2.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box2+44)
MOVT	R0, #hi_addr(_Box2+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2784 :: 		Box2.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Box2+48)
MOVT	R0, #hi_addr(_Box2+48)
STR	R1, [R0, #0]
;BPWasher_driver.c,2786 :: 		Label8.OwnerScreen     = &SplashLand;
MOVW	R1, #lo_addr(_SplashLand+0)
MOVT	R1, #hi_addr(_SplashLand+0)
MOVW	R0, #lo_addr(_Label8+0)
MOVT	R0, #hi_addr(_Label8+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2787 :: 		Label8.Order           = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_Label8+4)
MOVT	R0, #hi_addr(_Label8+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2788 :: 		Label8.Left            = 14;
MOVS	R1, #14
MOVW	R0, #lo_addr(_Label8+6)
MOVT	R0, #hi_addr(_Label8+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2789 :: 		Label8.Top             = 207; // was 40, moved to bottom - CM
MOVS	R1, #207
MOVW	R0, #lo_addr(_Label8+8)
MOVT	R0, #hi_addr(_Label8+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2790 :: 		Label8.Width           = 111;
MOVS	R1, #111
MOVW	R0, #lo_addr(_Label8+10)
MOVT	R0, #hi_addr(_Label8+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2791 :: 		Label8.Height          = 25;
MOVS	R1, #25
MOVW	R0, #lo_addr(_Label8+12)
MOVT	R0, #hi_addr(_Label8+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2792 :: 		Label8.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label8+27)
MOVT	R0, #hi_addr(_Label8+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2793 :: 		Label8.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Label8+28)
MOVT	R0, #hi_addr(_Label8+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2794 :: 		Label8.Caption         = Label8_Caption;
MOVW	R1, #lo_addr(_Label8_Caption+0)
MOVT	R1, #hi_addr(_Label8_Caption+0)
MOVW	R0, #lo_addr(_Label8+16)
MOVT	R0, #hi_addr(_Label8+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2795 :: 		Label8.FontName        = Tahoma23x23_Bold;
MOVW	R1, #lo_addr(_Tahoma23x23_Bold+0)
MOVT	R1, #hi_addr(_Tahoma23x23_Bold+0)
MOVW	R0, #lo_addr(_Label8+20)
MOVT	R0, #hi_addr(_Label8+20)
STR	R1, [R0, #0]
;BPWasher_driver.c,2796 :: 		Label8.Font_Color      = 0xFFFF;
MOVW	R0, #65535
MOVW	R1, #lo_addr(_Label8+24)
MOVT	R1, #hi_addr(_Label8+24)
STRH	R0, [R1, #0]
;BPWasher_driver.c,2798 :: 		Label8.Font_Color      = CL_RED; // add warning on splash Screen if no I2C - CM
MOVW	R0, #63488
STRH	R0, [R1, #0]
;BPWasher_driver.c,2800 :: 		Label8.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label8+26)
MOVT	R0, #hi_addr(_Label8+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2801 :: 		Label8.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label8+32)
MOVT	R0, #hi_addr(_Label8+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2802 :: 		Label8.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label8+36)
MOVT	R0, #hi_addr(_Label8+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2803 :: 		Label8.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label8+40)
MOVT	R0, #hi_addr(_Label8+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2804 :: 		Label8.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Label8+44)
MOVT	R0, #hi_addr(_Label8+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2806 :: 		ErrorLog_OK.OwnerScreen     = &ErrorLog;
MOVW	R1, #lo_addr(_ErrorLog+0)
MOVT	R1, #hi_addr(_ErrorLog+0)
MOVW	R0, #lo_addr(_ErrorLog_OK+0)
MOVT	R0, #hi_addr(_ErrorLog_OK+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2807 :: 		ErrorLog_OK.Order           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog_OK+4)
MOVT	R0, #hi_addr(_ErrorLog_OK+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2808 :: 		ErrorLog_OK.Left            = 239;
MOVS	R1, #239
MOVW	R0, #lo_addr(_ErrorLog_OK+6)
MOVT	R0, #hi_addr(_ErrorLog_OK+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2809 :: 		ErrorLog_OK.Top             = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_ErrorLog_OK+8)
MOVT	R0, #hi_addr(_ErrorLog_OK+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2810 :: 		ErrorLog_OK.Width           = 74;
MOVS	R1, #74
MOVW	R0, #lo_addr(_ErrorLog_OK+10)
MOVT	R0, #hi_addr(_ErrorLog_OK+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2811 :: 		ErrorLog_OK.Height          = 59;
MOVS	R1, #59
MOVW	R0, #lo_addr(_ErrorLog_OK+12)
MOVT	R0, #hi_addr(_ErrorLog_OK+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2812 :: 		ErrorLog_OK.Pen_Width       = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_ErrorLog_OK+14)
MOVT	R0, #hi_addr(_ErrorLog_OK+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2813 :: 		ErrorLog_OK.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_ErrorLog_OK+16)
MOVT	R0, #hi_addr(_ErrorLog_OK+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2814 :: 		ErrorLog_OK.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ErrorLog_OK+18)
MOVT	R0, #hi_addr(_ErrorLog_OK+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2815 :: 		ErrorLog_OK.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ErrorLog_OK+19)
MOVT	R0, #hi_addr(_ErrorLog_OK+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2816 :: 		ErrorLog_OK.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ErrorLog_OK+20)
MOVT	R0, #hi_addr(_ErrorLog_OK+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2817 :: 		ErrorLog_OK.Caption         = ErrorLog_OK_Caption;
MOVW	R1, #lo_addr(_ErrorLog_OK_Caption+0)
MOVT	R1, #hi_addr(_ErrorLog_OK_Caption+0)
MOVW	R0, #lo_addr(_ErrorLog_OK+24)
MOVT	R0, #hi_addr(_ErrorLog_OK+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2818 :: 		ErrorLog_OK.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ErrorLog_OK+28)
MOVT	R0, #hi_addr(_ErrorLog_OK+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2819 :: 		ErrorLog_OK.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ErrorLog_OK+29)
MOVT	R0, #hi_addr(_ErrorLog_OK+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2820 :: 		ErrorLog_OK.FontName        = Tahoma42x52_Regular;
MOVW	R0, #lo_addr(_ErrorLog_OK+32)
MOVT	R0, #hi_addr(_ErrorLog_OK+32)
STR	R6, [R0, #0]
;BPWasher_driver.c,2821 :: 		ErrorLog_OK.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ErrorLog_OK+49)
MOVT	R0, #hi_addr(_ErrorLog_OK+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2822 :: 		ErrorLog_OK.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog_OK+36)
MOVT	R0, #hi_addr(_ErrorLog_OK+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2823 :: 		ErrorLog_OK.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog_OK+38)
MOVT	R0, #hi_addr(_ErrorLog_OK+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2824 :: 		ErrorLog_OK.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog_OK+39)
MOVT	R0, #hi_addr(_ErrorLog_OK+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2825 :: 		ErrorLog_OK.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog_OK+40)
MOVT	R0, #hi_addr(_ErrorLog_OK+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2826 :: 		ErrorLog_OK.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_ErrorLog_OK+42)
MOVT	R0, #hi_addr(_ErrorLog_OK+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2827 :: 		ErrorLog_OK.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_ErrorLog_OK+44)
MOVT	R0, #hi_addr(_ErrorLog_OK+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2828 :: 		ErrorLog_OK.Color           = 0x0400;
MOVW	R1, #1024
MOVW	R0, #lo_addr(_ErrorLog_OK+46)
MOVT	R0, #hi_addr(_ErrorLog_OK+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2829 :: 		ErrorLog_OK.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_ErrorLog_OK+50)
MOVT	R0, #hi_addr(_ErrorLog_OK+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2830 :: 		ErrorLog_OK.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_ErrorLog_OK+48)
MOVT	R0, #hi_addr(_ErrorLog_OK+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2831 :: 		ErrorLog_OK.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog_OK+52)
MOVT	R0, #hi_addr(_ErrorLog_OK+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2832 :: 		ErrorLog_OK.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog_OK+56)
MOVT	R0, #hi_addr(_ErrorLog_OK+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2833 :: 		ErrorLog_OK.OnClickPtr      = ErrorLog_OKClick;
MOVW	R1, #lo_addr(_ErrorLog_OKClick+0)
MOVT	R1, #hi_addr(_ErrorLog_OKClick+0)
MOVW	R0, #lo_addr(_ErrorLog_OK+60)
MOVT	R0, #hi_addr(_ErrorLog_OK+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2834 :: 		ErrorLog_OK.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ErrorLog_OK+64)
MOVT	R0, #hi_addr(_ErrorLog_OK+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2836 :: 		Log_Title.OwnerScreen     = &ErrorLog;
MOVW	R1, #lo_addr(_ErrorLog+0)
MOVT	R1, #hi_addr(_ErrorLog+0)
MOVW	R0, #lo_addr(_Log_Title+0)
MOVT	R0, #hi_addr(_Log_Title+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2837 :: 		Log_Title.Order           = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Log_Title+4)
MOVT	R0, #hi_addr(_Log_Title+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2838 :: 		Log_Title.Left            = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Title+6)
MOVT	R0, #hi_addr(_Log_Title+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2839 :: 		Log_Title.Top             = 25;
MOVS	R1, #25
MOVW	R0, #lo_addr(_Log_Title+8)
MOVT	R0, #hi_addr(_Log_Title+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2840 :: 		Log_Title.Width           = 194;
MOVS	R1, #194
MOVW	R0, #lo_addr(_Log_Title+10)
MOVT	R0, #hi_addr(_Log_Title+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2841 :: 		Log_Title.Height          = 17;
MOVS	R1, #17
MOVW	R0, #lo_addr(_Log_Title+12)
MOVT	R0, #hi_addr(_Log_Title+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2842 :: 		Log_Title.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Log_Title+27)
MOVT	R0, #hi_addr(_Log_Title+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2843 :: 		Log_Title.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Log_Title+28)
MOVT	R0, #hi_addr(_Log_Title+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2844 :: 		Log_Title.Caption         = Log_Title_Caption;
MOVW	R1, #lo_addr(_Log_Title_Caption+0)
MOVT	R1, #hi_addr(_Log_Title_Caption+0)
MOVW	R0, #lo_addr(_Log_Title+16)
MOVT	R0, #hi_addr(_Log_Title+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2845 :: 		Log_Title.FontName        = Tahoma12x16_Regular;
MOVW	R0, #lo_addr(_Log_Title+20)
MOVT	R0, #hi_addr(_Log_Title+20)
STR	R3, [R0, #0]
;BPWasher_driver.c,2846 :: 		Log_Title.Font_Color      = 0x07E0;
MOVW	R1, #2016
MOVW	R0, #lo_addr(_Log_Title+24)
MOVT	R0, #hi_addr(_Log_Title+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2847 :: 		Log_Title.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Title+26)
MOVT	R0, #hi_addr(_Log_Title+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2848 :: 		Log_Title.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Title+32)
MOVT	R0, #hi_addr(_Log_Title+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2849 :: 		Log_Title.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Title+36)
MOVT	R0, #hi_addr(_Log_Title+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2850 :: 		Log_Title.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Title+40)
MOVT	R0, #hi_addr(_Log_Title+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2851 :: 		Log_Title.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Title+44)
MOVT	R0, #hi_addr(_Log_Title+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2853 :: 		Log_Labels[0].OwnerScreen     = &ErrorLog;
MOVW	R1, #lo_addr(_ErrorLog+0)
MOVT	R1, #hi_addr(_ErrorLog+0)
MOVW	R0, #lo_addr(_Log_Labels+0)
MOVT	R0, #hi_addr(_Log_Labels+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2854 :: 		Log_Labels[0].Order           = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_Log_Labels+4)
MOVT	R0, #hi_addr(_Log_Labels+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2855 :: 		Log_Labels[0].Left            = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Labels+6)
MOVT	R0, #hi_addr(_Log_Labels+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2856 :: 		Log_Labels[0].Top             = 69;
MOVS	R1, #69
MOVW	R0, #lo_addr(_Log_Labels+8)
MOVT	R0, #hi_addr(_Log_Labels+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2857 :: 		Log_Labels[0].Width           = 352;
MOVW	R1, #352
MOVW	R0, #lo_addr(_Log_Labels+10)
MOVT	R0, #hi_addr(_Log_Labels+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2858 :: 		Log_Labels[0].Height          = 17;
MOVS	R1, #17
MOVW	R0, #lo_addr(_Log_Labels+12)
MOVT	R0, #hi_addr(_Log_Labels+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2859 :: 		Log_Labels[0].Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Log_Labels+27)
MOVT	R0, #hi_addr(_Log_Labels+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2860 :: 		Log_Labels[0].Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Log_Labels+28)
MOVT	R0, #hi_addr(_Log_Labels+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2861 :: 		Log_Labels[0].Caption         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Labels+16)
MOVT	R0, #hi_addr(_Log_Labels+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2862 :: 		Log_Labels[0].FontName        = Tahoma12x16_Regular;
MOVW	R0, #lo_addr(_Log_Labels+20)
MOVT	R0, #hi_addr(_Log_Labels+20)
STR	R3, [R0, #0]
;BPWasher_driver.c,2863 :: 		Log_Labels[0].Font_Color      = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_Log_Labels+24)
MOVT	R0, #hi_addr(_Log_Labels+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2864 :: 		Log_Labels[0].VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Labels+26)
MOVT	R0, #hi_addr(_Log_Labels+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2865 :: 		Log_Labels[0].OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Labels+32)
MOVT	R0, #hi_addr(_Log_Labels+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2866 :: 		Log_Labels[0].OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Labels+36)
MOVT	R0, #hi_addr(_Log_Labels+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2867 :: 		Log_Labels[0].OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Labels+40)
MOVT	R0, #hi_addr(_Log_Labels+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2868 :: 		Log_Labels[0].OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Log_Labels+44)
MOVT	R0, #hi_addr(_Log_Labels+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2872 :: 		config_fill_value_label.OwnerScreen     = &Config;
MOVW	R1, #lo_addr(_Config+0)
MOVT	R1, #hi_addr(_Config+0)
MOVW	R0, #lo_addr(_config_fill_value_label+0)
MOVT	R0, #hi_addr(_config_fill_value_label+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2873 :: 		config_fill_value_label.Order           = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_value_label+4)
MOVT	R0, #hi_addr(_config_fill_value_label+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2874 :: 		config_fill_value_label.Left            = 110;
MOVS	R1, #110
MOVW	R0, #lo_addr(_config_fill_value_label+6)
MOVT	R0, #hi_addr(_config_fill_value_label+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2875 :: 		config_fill_value_label.Top             = 90;
MOVS	R1, #90
MOVW	R0, #lo_addr(_config_fill_value_label+8)
MOVT	R0, #hi_addr(_config_fill_value_label+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2876 :: 		config_fill_value_label.Width           = 86;
MOVS	R1, #86
MOVW	R0, #lo_addr(_config_fill_value_label+10)
MOVT	R0, #hi_addr(_config_fill_value_label+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2877 :: 		config_fill_value_label.Height          = 47;
MOVS	R1, #47
MOVW	R0, #lo_addr(_config_fill_value_label+12)
MOVT	R0, #hi_addr(_config_fill_value_label+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2878 :: 		config_fill_value_label.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_fill_value_label+27)
MOVT	R0, #hi_addr(_config_fill_value_label+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2879 :: 		config_fill_value_label.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_fill_value_label+28)
MOVT	R0, #hi_addr(_config_fill_value_label+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2880 :: 		config_fill_value_label.Caption         = config_fill_value_label_Caption;
MOVW	R1, #lo_addr(_config_fill_value_label_Caption+0)
MOVT	R1, #hi_addr(_config_fill_value_label_Caption+0)
MOVW	R0, #lo_addr(_config_fill_value_label+16)
MOVT	R0, #hi_addr(_config_fill_value_label+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2881 :: 		config_fill_value_label.FontName        = Tahoma34x42_Regular;
MOVW	R0, #lo_addr(_config_fill_value_label+20)
MOVT	R0, #hi_addr(_config_fill_value_label+20)
STR	R9, [R0, #0]
;BPWasher_driver.c,2882 :: 		config_fill_value_label.Font_Color      = 0xFFE0;
MOVW	R1, #65504
MOVW	R0, #lo_addr(_config_fill_value_label+24)
MOVT	R0, #hi_addr(_config_fill_value_label+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2883 :: 		config_fill_value_label.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_value_label+26)
MOVT	R0, #hi_addr(_config_fill_value_label+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2884 :: 		config_fill_value_label.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_value_label+32)
MOVT	R0, #hi_addr(_config_fill_value_label+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2885 :: 		config_fill_value_label.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_value_label+36)
MOVT	R0, #hi_addr(_config_fill_value_label+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2886 :: 		config_fill_value_label.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_value_label+40)
MOVT	R0, #hi_addr(_config_fill_value_label+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2887 :: 		config_fill_value_label.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_value_label+44)
MOVT	R0, #hi_addr(_config_fill_value_label+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2889 :: 		config_drain_value_label.OwnerScreen     = &Config;
MOVW	R1, #lo_addr(_Config+0)
MOVT	R1, #hi_addr(_Config+0)
MOVW	R0, #lo_addr(_config_drain_value_label+0)
MOVT	R0, #hi_addr(_config_drain_value_label+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2890 :: 		config_drain_value_label.Order           = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_drain_value_label+4)
MOVT	R0, #hi_addr(_config_drain_value_label+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2891 :: 		config_drain_value_label.Left            = 110;
MOVS	R1, #110
MOVW	R0, #lo_addr(_config_drain_value_label+6)
MOVT	R0, #hi_addr(_config_drain_value_label+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2892 :: 		config_drain_value_label.Top             = 22;
MOVS	R1, #22
MOVW	R0, #lo_addr(_config_drain_value_label+8)
MOVT	R0, #hi_addr(_config_drain_value_label+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2893 :: 		config_drain_value_label.Width           = 86;
MOVS	R1, #86
MOVW	R0, #lo_addr(_config_drain_value_label+10)
MOVT	R0, #hi_addr(_config_drain_value_label+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2894 :: 		config_drain_value_label.Height          = 47;
MOVS	R1, #47
MOVW	R0, #lo_addr(_config_drain_value_label+12)
MOVT	R0, #hi_addr(_config_drain_value_label+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2895 :: 		config_drain_value_label.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_drain_value_label+27)
MOVT	R0, #hi_addr(_config_drain_value_label+27)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2896 :: 		config_drain_value_label.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_drain_value_label+28)
MOVT	R0, #hi_addr(_config_drain_value_label+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2897 :: 		config_drain_value_label.Caption         = config_drain_value_label_Caption;
MOVW	R1, #lo_addr(_config_drain_value_label_Caption+0)
MOVT	R1, #hi_addr(_config_drain_value_label_Caption+0)
MOVW	R0, #lo_addr(_config_drain_value_label+16)
MOVT	R0, #hi_addr(_config_drain_value_label+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,2898 :: 		config_drain_value_label.FontName        = Tahoma34x42_Regular;
MOVW	R0, #lo_addr(_config_drain_value_label+20)
MOVT	R0, #hi_addr(_config_drain_value_label+20)
STR	R9, [R0, #0]
;BPWasher_driver.c,2899 :: 		config_drain_value_label.Font_Color      = 0xFFE0;
MOVW	R1, #65504
MOVW	R0, #lo_addr(_config_drain_value_label+24)
MOVT	R0, #hi_addr(_config_drain_value_label+24)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2900 :: 		config_drain_value_label.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_value_label+26)
MOVT	R0, #hi_addr(_config_drain_value_label+26)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2901 :: 		config_drain_value_label.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_value_label+32)
MOVT	R0, #hi_addr(_config_drain_value_label+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,2902 :: 		config_drain_value_label.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_value_label+36)
MOVT	R0, #hi_addr(_config_drain_value_label+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,2903 :: 		config_drain_value_label.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_value_label+40)
MOVT	R0, #hi_addr(_config_drain_value_label+40)
STR	R1, [R0, #0]
;BPWasher_driver.c,2904 :: 		config_drain_value_label.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_value_label+44)
MOVT	R0, #hi_addr(_config_drain_value_label+44)
STR	R1, [R0, #0]
;BPWasher_driver.c,2906 :: 		config_fill_button.OwnerScreen     = &Config;
MOVW	R1, #lo_addr(_Config+0)
MOVT	R1, #hi_addr(_Config+0)
MOVW	R0, #lo_addr(_config_fill_button+0)
MOVT	R0, #hi_addr(_config_fill_button+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2907 :: 		config_fill_button.Order           = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_config_fill_button+4)
MOVT	R0, #hi_addr(_config_fill_button+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2908 :: 		config_fill_button.Left            = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_config_fill_button+6)
MOVT	R0, #hi_addr(_config_fill_button+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2909 :: 		config_fill_button.Top             = 84;
MOVS	R1, #84
MOVW	R0, #lo_addr(_config_fill_button+8)
MOVT	R0, #hi_addr(_config_fill_button+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2910 :: 		config_fill_button.Width           = 96;
MOVS	R1, #96
MOVW	R0, #lo_addr(_config_fill_button+10)
MOVT	R0, #hi_addr(_config_fill_button+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2911 :: 		config_fill_button.Height          = 54;
MOVS	R1, #54
MOVW	R0, #lo_addr(_config_fill_button+12)
MOVT	R0, #hi_addr(_config_fill_button+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2912 :: 		config_fill_button.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_fill_button+14)
MOVT	R0, #hi_addr(_config_fill_button+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2913 :: 		config_fill_button.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_button+16)
MOVT	R0, #hi_addr(_config_fill_button+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2914 :: 		config_fill_button.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_fill_button+18)
MOVT	R0, #hi_addr(_config_fill_button+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2915 :: 		config_fill_button.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_fill_button+19)
MOVT	R0, #hi_addr(_config_fill_button+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2916 :: 		config_fill_button.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_fill_button+20)
MOVT	R0, #hi_addr(_config_fill_button+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2917 :: 		config_fill_button.Caption         = config_fill_button_Caption;
MOVW	R1, #lo_addr(_config_fill_button_Caption+0)
MOVT	R1, #hi_addr(_config_fill_button_Caption+0)
MOVW	R0, #lo_addr(_config_fill_button+24)
MOVT	R0, #hi_addr(_config_fill_button+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2918 :: 		config_fill_button.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_fill_button+28)
MOVT	R0, #hi_addr(_config_fill_button+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2919 :: 		config_fill_button.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_fill_button+29)
MOVT	R0, #hi_addr(_config_fill_button+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2920 :: 		config_fill_button.FontName        = Tahoma21x25_Regular;
MOVW	R0, #lo_addr(_config_fill_button+32)
MOVT	R0, #hi_addr(_config_fill_button+32)
STR	R7, [R0, #0]
;BPWasher_driver.c,2921 :: 		config_fill_button.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_fill_button+49)
MOVT	R0, #hi_addr(_config_fill_button+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2922 :: 		config_fill_button.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_button+36)
MOVT	R0, #hi_addr(_config_fill_button+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2923 :: 		config_fill_button.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_button+38)
MOVT	R0, #hi_addr(_config_fill_button+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2924 :: 		config_fill_button.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_button+39)
MOVT	R0, #hi_addr(_config_fill_button+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2925 :: 		config_fill_button.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_button+40)
MOVT	R0, #hi_addr(_config_fill_button+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2926 :: 		config_fill_button.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_config_fill_button+42)
MOVT	R0, #hi_addr(_config_fill_button+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2927 :: 		config_fill_button.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_config_fill_button+44)
MOVT	R0, #hi_addr(_config_fill_button+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2928 :: 		config_fill_button.Color           = 0xF81F;
MOVW	R1, #63519
MOVW	R0, #lo_addr(_config_fill_button+46)
MOVT	R0, #hi_addr(_config_fill_button+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2929 :: 		config_fill_button.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_config_fill_button+50)
MOVT	R0, #hi_addr(_config_fill_button+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2930 :: 		config_fill_button.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_config_fill_button+48)
MOVT	R0, #hi_addr(_config_fill_button+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2931 :: 		config_fill_button.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_button+52)
MOVT	R0, #hi_addr(_config_fill_button+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2932 :: 		config_fill_button.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_button+56)
MOVT	R0, #hi_addr(_config_fill_button+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2933 :: 		config_fill_button.OnClickPtr      = config_fill_buttonClick;
MOVW	R1, #lo_addr(_config_fill_buttonClick+0)
MOVT	R1, #hi_addr(_config_fill_buttonClick+0)
MOVW	R0, #lo_addr(_config_fill_button+60)
MOVT	R0, #hi_addr(_config_fill_button+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2934 :: 		config_fill_button.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_fill_button+64)
MOVT	R0, #hi_addr(_config_fill_button+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2936 :: 		config_drain_button.OwnerScreen     = &Config;
MOVW	R1, #lo_addr(_Config+0)
MOVT	R1, #hi_addr(_Config+0)
MOVW	R0, #lo_addr(_config_drain_button+0)
MOVT	R0, #hi_addr(_config_drain_button+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2937 :: 		config_drain_button.Order           = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_config_drain_button+4)
MOVT	R0, #hi_addr(_config_drain_button+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2938 :: 		config_drain_button.Left            = 8;
MOVS	R1, #8
MOVW	R0, #lo_addr(_config_drain_button+6)
MOVT	R0, #hi_addr(_config_drain_button+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2939 :: 		config_drain_button.Top             = 18;
MOVS	R1, #18
MOVW	R0, #lo_addr(_config_drain_button+8)
MOVT	R0, #hi_addr(_config_drain_button+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2940 :: 		config_drain_button.Width           = 96;
MOVS	R1, #96
MOVW	R0, #lo_addr(_config_drain_button+10)
MOVT	R0, #hi_addr(_config_drain_button+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2941 :: 		config_drain_button.Height          = 54;
MOVS	R1, #54
MOVW	R0, #lo_addr(_config_drain_button+12)
MOVT	R0, #hi_addr(_config_drain_button+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2942 :: 		config_drain_button.Pen_Width       = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_drain_button+14)
MOVT	R0, #hi_addr(_config_drain_button+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2943 :: 		config_drain_button.Pen_Color       = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_button+16)
MOVT	R0, #hi_addr(_config_drain_button+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2944 :: 		config_drain_button.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_drain_button+18)
MOVT	R0, #hi_addr(_config_drain_button+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2945 :: 		config_drain_button.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_drain_button+19)
MOVT	R0, #hi_addr(_config_drain_button+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2946 :: 		config_drain_button.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_drain_button+20)
MOVT	R0, #hi_addr(_config_drain_button+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2947 :: 		config_drain_button.Caption         = config_drain_button_Caption;
MOVW	R1, #lo_addr(_config_drain_button_Caption+0)
MOVT	R1, #hi_addr(_config_drain_button_Caption+0)
MOVW	R0, #lo_addr(_config_drain_button+24)
MOVT	R0, #hi_addr(_config_drain_button+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2948 :: 		config_drain_button.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_drain_button+28)
MOVT	R0, #hi_addr(_config_drain_button+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2949 :: 		config_drain_button.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_drain_button+29)
MOVT	R0, #hi_addr(_config_drain_button+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2950 :: 		config_drain_button.FontName        = Tahoma21x25_Regular;
MOVW	R0, #lo_addr(_config_drain_button+32)
MOVT	R0, #hi_addr(_config_drain_button+32)
STR	R7, [R0, #0]
;BPWasher_driver.c,2951 :: 		config_drain_button.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_drain_button+49)
MOVT	R0, #hi_addr(_config_drain_button+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2952 :: 		config_drain_button.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_button+36)
MOVT	R0, #hi_addr(_config_drain_button+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2953 :: 		config_drain_button.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_button+38)
MOVT	R0, #hi_addr(_config_drain_button+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2954 :: 		config_drain_button.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_button+39)
MOVT	R0, #hi_addr(_config_drain_button+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2955 :: 		config_drain_button.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_button+40)
MOVT	R0, #hi_addr(_config_drain_button+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2956 :: 		config_drain_button.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_config_drain_button+42)
MOVT	R0, #hi_addr(_config_drain_button+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2957 :: 		config_drain_button.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_config_drain_button+44)
MOVT	R0, #hi_addr(_config_drain_button+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2958 :: 		config_drain_button.Color           = 0xF81F;
MOVW	R1, #63519
MOVW	R0, #lo_addr(_config_drain_button+46)
MOVT	R0, #hi_addr(_config_drain_button+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2959 :: 		config_drain_button.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_config_drain_button+50)
MOVT	R0, #hi_addr(_config_drain_button+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2960 :: 		config_drain_button.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_config_drain_button+48)
MOVT	R0, #hi_addr(_config_drain_button+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2961 :: 		config_drain_button.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_button+52)
MOVT	R0, #hi_addr(_config_drain_button+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2962 :: 		config_drain_button.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_button+56)
MOVT	R0, #hi_addr(_config_drain_button+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2963 :: 		config_drain_button.OnClickPtr      = config_drain_buttonClick;
MOVW	R1, #lo_addr(_config_drain_buttonClick+0)
MOVT	R1, #hi_addr(_config_drain_buttonClick+0)
MOVW	R0, #lo_addr(_config_drain_button+60)
MOVT	R0, #hi_addr(_config_drain_button+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2964 :: 		config_drain_button.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_drain_button+64)
MOVT	R0, #hi_addr(_config_drain_button+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2967 :: 		config_confirm_button.OwnerScreen     = &Config;
MOVW	R1, #lo_addr(_Config+0)
MOVT	R1, #hi_addr(_Config+0)
MOVW	R0, #lo_addr(_config_confirm_button+0)
MOVT	R0, #hi_addr(_config_confirm_button+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2968 :: 		config_confirm_button.Order           = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_config_confirm_button+4)
MOVT	R0, #hi_addr(_config_confirm_button+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2969 :: 		config_confirm_button.Left            = 222;
MOVS	R1, #222
MOVW	R0, #lo_addr(_config_confirm_button+6)
MOVT	R0, #hi_addr(_config_confirm_button+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2970 :: 		config_confirm_button.Top             = 157;
MOVS	R1, #157
MOVW	R0, #lo_addr(_config_confirm_button+8)
MOVT	R0, #hi_addr(_config_confirm_button+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2971 :: 		config_confirm_button.Width           = 95;
MOVS	R1, #95
MOVW	R0, #lo_addr(_config_confirm_button+10)
MOVT	R0, #hi_addr(_config_confirm_button+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2972 :: 		config_confirm_button.Height          = 77;
MOVS	R1, #77
MOVW	R0, #lo_addr(_config_confirm_button+12)
MOVT	R0, #hi_addr(_config_confirm_button+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2973 :: 		config_confirm_button.Pen_Width       = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_config_confirm_button+14)
MOVT	R0, #hi_addr(_config_confirm_button+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2974 :: 		config_confirm_button.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_config_confirm_button+16)
MOVT	R0, #hi_addr(_config_confirm_button+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2975 :: 		config_confirm_button.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_confirm_button+18)
MOVT	R0, #hi_addr(_config_confirm_button+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2976 :: 		config_confirm_button.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_confirm_button+19)
MOVT	R0, #hi_addr(_config_confirm_button+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2977 :: 		config_confirm_button.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_confirm_button+20)
MOVT	R0, #hi_addr(_config_confirm_button+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2978 :: 		config_confirm_button.Caption         = config_confirm_button_Caption;
MOVW	R1, #lo_addr(_config_confirm_button_Caption+0)
MOVT	R1, #hi_addr(_config_confirm_button_Caption+0)
MOVW	R0, #lo_addr(_config_confirm_button+24)
MOVT	R0, #hi_addr(_config_confirm_button+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,2979 :: 		config_confirm_button.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_confirm_button+28)
MOVT	R0, #hi_addr(_config_confirm_button+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2980 :: 		config_confirm_button.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_confirm_button+29)
MOVT	R0, #hi_addr(_config_confirm_button+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2981 :: 		config_confirm_button.FontName        = Tahoma42x52_Regular;
MOVW	R0, #lo_addr(_config_confirm_button+32)
MOVT	R0, #hi_addr(_config_confirm_button+32)
STR	R6, [R0, #0]
;BPWasher_driver.c,2982 :: 		config_confirm_button.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_confirm_button+49)
MOVT	R0, #hi_addr(_config_confirm_button+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2983 :: 		config_confirm_button.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_confirm_button+36)
MOVT	R0, #hi_addr(_config_confirm_button+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2984 :: 		config_confirm_button.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_confirm_button+38)
MOVT	R0, #hi_addr(_config_confirm_button+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2985 :: 		config_confirm_button.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_confirm_button+39)
MOVT	R0, #hi_addr(_config_confirm_button+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2986 :: 		config_confirm_button.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_confirm_button+40)
MOVT	R0, #hi_addr(_config_confirm_button+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2987 :: 		config_confirm_button.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_config_confirm_button+42)
MOVT	R0, #hi_addr(_config_confirm_button+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2988 :: 		config_confirm_button.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_config_confirm_button+44)
MOVT	R0, #hi_addr(_config_confirm_button+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2989 :: 		config_confirm_button.Color           = 0x0400;
MOVW	R1, #1024
MOVW	R0, #lo_addr(_config_confirm_button+46)
MOVT	R0, #hi_addr(_config_confirm_button+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2990 :: 		config_confirm_button.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_config_confirm_button+50)
MOVT	R0, #hi_addr(_config_confirm_button+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,2991 :: 		config_confirm_button.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_config_confirm_button+48)
MOVT	R0, #hi_addr(_config_confirm_button+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2992 :: 		config_confirm_button.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_confirm_button+52)
MOVT	R0, #hi_addr(_config_confirm_button+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,2993 :: 		config_confirm_button.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_confirm_button+56)
MOVT	R0, #hi_addr(_config_confirm_button+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,2994 :: 		config_confirm_button.OnClickPtr      = config_confirm_buttonClick;
MOVW	R1, #lo_addr(_config_confirm_buttonClick+0)
MOVT	R1, #hi_addr(_config_confirm_buttonClick+0)
MOVW	R0, #lo_addr(_config_confirm_button+60)
MOVT	R0, #hi_addr(_config_confirm_button+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,2995 :: 		config_confirm_button.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_confirm_button+64)
MOVT	R0, #hi_addr(_config_confirm_button+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,2997 :: 		config_ret_button.OwnerScreen     = &Config;
MOVW	R1, #lo_addr(_Config+0)
MOVT	R1, #hi_addr(_Config+0)
MOVW	R0, #lo_addr(_config_ret_button+0)
MOVT	R0, #hi_addr(_config_ret_button+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,2998 :: 		config_ret_button.Order           = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_config_ret_button+4)
MOVT	R0, #hi_addr(_config_ret_button+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,2999 :: 		config_ret_button.Left            = 222;
MOVS	R1, #222
MOVW	R0, #lo_addr(_config_ret_button+6)
MOVT	R0, #hi_addr(_config_ret_button+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3000 :: 		config_ret_button.Top             = 4;
MOVS	R1, #4
MOVW	R0, #lo_addr(_config_ret_button+8)
MOVT	R0, #hi_addr(_config_ret_button+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3001 :: 		config_ret_button.Width           = 95;
MOVS	R1, #95
MOVW	R0, #lo_addr(_config_ret_button+10)
MOVT	R0, #hi_addr(_config_ret_button+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3002 :: 		config_ret_button.Height          = 77;
MOVS	R1, #77
MOVW	R0, #lo_addr(_config_ret_button+12)
MOVT	R0, #hi_addr(_config_ret_button+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3003 :: 		config_ret_button.Pen_Width       = 2;
MOVS	R1, #2
MOVW	R0, #lo_addr(_config_ret_button+14)
MOVT	R0, #hi_addr(_config_ret_button+14)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3004 :: 		config_ret_button.Pen_Color       = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_config_ret_button+16)
MOVT	R0, #hi_addr(_config_ret_button+16)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3005 :: 		config_ret_button.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_ret_button+18)
MOVT	R0, #hi_addr(_config_ret_button+18)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3006 :: 		config_ret_button.Active          = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_ret_button+19)
MOVT	R0, #hi_addr(_config_ret_button+19)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3007 :: 		config_ret_button.Transparent     = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_ret_button+20)
MOVT	R0, #hi_addr(_config_ret_button+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3008 :: 		config_ret_button.Caption         = config_ret_button_Caption;
MOVW	R1, #lo_addr(_config_ret_button_Caption+0)
MOVT	R1, #hi_addr(_config_ret_button_Caption+0)
MOVW	R0, #lo_addr(_config_ret_button+24)
MOVT	R0, #hi_addr(_config_ret_button+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,3009 :: 		config_ret_button.TextAlign       = _taCenter;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_ret_button+28)
MOVT	R0, #hi_addr(_config_ret_button+28)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3010 :: 		config_ret_button.TextAlignVertical= _tavMiddle;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_ret_button+29)
MOVT	R0, #hi_addr(_config_ret_button+29)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3011 :: 		config_ret_button.FontName        = Tahoma42x52_Regular;
MOVW	R0, #lo_addr(_config_ret_button+32)
MOVT	R0, #hi_addr(_config_ret_button+32)
STR	R6, [R0, #0]
;BPWasher_driver.c,3012 :: 		config_ret_button.PressColEnabled = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_config_ret_button+49)
MOVT	R0, #hi_addr(_config_ret_button+49)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3013 :: 		config_ret_button.Font_Color      = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_ret_button+36)
MOVT	R0, #hi_addr(_config_ret_button+36)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3014 :: 		config_ret_button.VerticalText    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_ret_button+38)
MOVT	R0, #hi_addr(_config_ret_button+38)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3015 :: 		config_ret_button.Gradient        = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_ret_button+39)
MOVT	R0, #hi_addr(_config_ret_button+39)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3016 :: 		config_ret_button.Gradient_Orientation = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_ret_button+40)
MOVT	R0, #hi_addr(_config_ret_button+40)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3017 :: 		config_ret_button.Gradient_Start_Color = 0xFFFF;
MOVW	R1, #65535
MOVW	R0, #lo_addr(_config_ret_button+42)
MOVT	R0, #hi_addr(_config_ret_button+42)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3018 :: 		config_ret_button.Gradient_End_Color = 0xC618;
MOVW	R1, #50712
MOVW	R0, #lo_addr(_config_ret_button+44)
MOVT	R0, #hi_addr(_config_ret_button+44)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3019 :: 		config_ret_button.Color           = 0xF800;
MOVW	R1, #63488
MOVW	R0, #lo_addr(_config_ret_button+46)
MOVT	R0, #hi_addr(_config_ret_button+46)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3020 :: 		config_ret_button.Press_Color     = 0xE71C;
MOVW	R1, #59164
MOVW	R0, #lo_addr(_config_ret_button+50)
MOVT	R0, #hi_addr(_config_ret_button+50)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3021 :: 		config_ret_button.Corner_Radius   = 5;
MOVS	R1, #5
MOVW	R0, #lo_addr(_config_ret_button+48)
MOVT	R0, #hi_addr(_config_ret_button+48)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3022 :: 		config_ret_button.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_ret_button+52)
MOVT	R0, #hi_addr(_config_ret_button+52)
STR	R1, [R0, #0]
;BPWasher_driver.c,3023 :: 		config_ret_button.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_ret_button+56)
MOVT	R0, #hi_addr(_config_ret_button+56)
STR	R1, [R0, #0]
;BPWasher_driver.c,3024 :: 		config_ret_button.OnClickPtr      = config_ret_buttonClick;
MOVW	R1, #lo_addr(_config_ret_buttonClick+0)
MOVT	R1, #hi_addr(_config_ret_buttonClick+0)
MOVW	R0, #lo_addr(_config_ret_button+60)
MOVT	R0, #hi_addr(_config_ret_button+60)
STR	R1, [R0, #0]
;BPWasher_driver.c,3025 :: 		config_ret_button.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_config_ret_button+64)
MOVT	R0, #hi_addr(_config_ret_button+64)
STR	R1, [R0, #0]
;BPWasher_driver.c,3027 :: 		Image3.OwnerScreen     = &Config;
MOVW	R1, #lo_addr(_Config+0)
MOVT	R1, #hi_addr(_Config+0)
MOVW	R0, #lo_addr(_Image3+0)
MOVT	R0, #hi_addr(_Image3+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,3028 :: 		Image3.Order           = 6;
MOVS	R1, #6
MOVW	R0, #lo_addr(_Image3+4)
MOVT	R0, #hi_addr(_Image3+4)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3029 :: 		Image3.Left            = 250;
MOVS	R1, #250
MOVW	R0, #lo_addr(_Image3+6)
MOVT	R0, #hi_addr(_Image3+6)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3030 :: 		Image3.Top             = 21;
MOVS	R1, #21
MOVW	R0, #lo_addr(_Image3+8)
MOVT	R0, #hi_addr(_Image3+8)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3031 :: 		Image3.Width           = 40;
MOVS	R1, #40
MOVW	R0, #lo_addr(_Image3+10)
MOVT	R0, #hi_addr(_Image3+10)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3032 :: 		Image3.Height          = 42;
MOVS	R1, #42
MOVW	R0, #lo_addr(_Image3+12)
MOVT	R0, #hi_addr(_Image3+12)
STRH	R1, [R0, #0]
;BPWasher_driver.c,3033 :: 		Image3.Picture_Type    = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image3+22)
MOVT	R0, #hi_addr(_Image3+22)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3034 :: 		Image3.Picture_Ratio   = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image3+23)
MOVT	R0, #hi_addr(_Image3+23)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3035 :: 		Image3.Picture_Name    = returnsarrow_bmp;
MOVW	R0, #lo_addr(_Image3+16)
MOVT	R0, #hi_addr(_Image3+16)
STR	R5, [R0, #0]
;BPWasher_driver.c,3036 :: 		Image3.Visible         = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_Image3+20)
MOVT	R0, #hi_addr(_Image3+20)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3037 :: 		Image3.Active          = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image3+21)
MOVT	R0, #hi_addr(_Image3+21)
STRB	R1, [R0, #0]
;BPWasher_driver.c,3038 :: 		Image3.OnUpPtr         = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image3+24)
MOVT	R0, #hi_addr(_Image3+24)
STR	R1, [R0, #0]
;BPWasher_driver.c,3039 :: 		Image3.OnDownPtr       = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image3+28)
MOVT	R0, #hi_addr(_Image3+28)
STR	R1, [R0, #0]
;BPWasher_driver.c,3040 :: 		Image3.OnClickPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image3+32)
MOVT	R0, #hi_addr(_Image3+32)
STR	R1, [R0, #0]
;BPWasher_driver.c,3041 :: 		Image3.OnPressPtr      = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Image3+36)
MOVT	R0, #hi_addr(_Image3+36)
STR	R1, [R0, #0]
;BPWasher_driver.c,3042 :: 		}
L_end_InitializeObjects:
ADD	SP, SP, #4
BX	LR
; end of BPWasher_driver_InitializeObjects
BPWasher_driver_IsInsideObject:
;BPWasher_driver.c,3044 :: 		static char IsInsideObject (unsigned int X, unsigned int Y, unsigned int Left, unsigned int Top, unsigned int Width, unsigned int Height) { // static
; Top start address is: 12 (R3)
; Left start address is: 8 (R2)
; Y start address is: 4 (R1)
; X start address is: 0 (R0)
SUB	SP, SP, #4
; Top end address is: 12 (R3)
; Left end address is: 8 (R2)
; Y end address is: 4 (R1)
; X end address is: 0 (R0)
; X start address is: 0 (R0)
; Y start address is: 4 (R1)
; Left start address is: 8 (R2)
; Top start address is: 12 (R3)
; Width start address is: 20 (R5)
LDRH	R5, [SP, #4]
; Height start address is: 24 (R6)
LDRH	R6, [SP, #8]
;BPWasher_driver.c,3045 :: 		if ( (Left<= X) && (Left+ Width - 1 >= X) &&
CMP	R2, R0
IT	HI
BHI	L_BPWasher_driver_IsInsideObject332
ADDS	R4, R2, R5
UXTH	R4, R4
; Left end address is: 8 (R2)
; Width end address is: 20 (R5)
SUBS	R4, R4, #1
UXTH	R4, R4
CMP	R4, R0
IT	CC
BCC	L_BPWasher_driver_IsInsideObject331
; X end address is: 0 (R0)
;BPWasher_driver.c,3046 :: 		(Top <= Y)  && (Top + Height - 1 >= Y) )
CMP	R3, R1
IT	HI
BHI	L_BPWasher_driver_IsInsideObject330
ADDS	R4, R3, R6
UXTH	R4, R4
; Top end address is: 12 (R3)
; Height end address is: 24 (R6)
SUBS	R4, R4, #1
UXTH	R4, R4
CMP	R4, R1
IT	CC
BCC	L_BPWasher_driver_IsInsideObject329
; Y end address is: 4 (R1)
L_BPWasher_driver_IsInsideObject328:
;BPWasher_driver.c,3047 :: 		return 1;
MOVS	R0, #1
IT	AL
BAL	L_end_IsInsideObject
;BPWasher_driver.c,3045 :: 		if ( (Left<= X) && (Left+ Width - 1 >= X) &&
L_BPWasher_driver_IsInsideObject332:
L_BPWasher_driver_IsInsideObject331:
;BPWasher_driver.c,3046 :: 		(Top <= Y)  && (Top + Height - 1 >= Y) )
L_BPWasher_driver_IsInsideObject330:
L_BPWasher_driver_IsInsideObject329:
;BPWasher_driver.c,3049 :: 		return 0;
MOVS	R0, #0
;BPWasher_driver.c,3050 :: 		}
L_end_IsInsideObject:
ADD	SP, SP, #4
BX	LR
; end of BPWasher_driver_IsInsideObject
_DrawButton:
;BPWasher_driver.c,3064 :: 		void DrawButton(TButton *Abutton) {
; Abutton start address is: 0 (R0)
SUB	SP, SP, #12
STR	LR, [SP, #0]
; Abutton end address is: 0 (R0)
; Abutton start address is: 0 (R0)
;BPWasher_driver.c,3066 :: 		if (CurrentScreen == AButton->OwnerScreen && Abutton->Visible != 0) {
LDR	R2, [R0, #0]
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
CMP	R1, R2
IT	NE
BNE	L__DrawButton335
ADDW	R1, R0, #18
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__DrawButton334
L__DrawButton333:
;BPWasher_driver.c,3067 :: 		if (object_pressed == 1) {
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawButton11
;BPWasher_driver.c,3068 :: 		object_pressed = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
STRB	R2, [R1, #0]
;BPWasher_driver.c,3069 :: 		TFT_Set_Brush(Abutton->Transparent, Abutton->Press_Color, Abutton->Gradient, Abutton->Gradient_Orientation, Abutton->Gradient_End_Color, Abutton->Gradient_Start_Color);
ADDW	R1, R0, #42
LDRH	R1, [R1, #0]
UXTH	R6, R1
ADDW	R1, R0, #44
LDRH	R1, [R1, #0]
UXTH	R5, R1
ADDW	R1, R0, #40
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #39
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDW	R1, R0, #50
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
STR	R0, [SP, #4]
UXTB	R0, R1
UXTH	R1, R2
UXTB	R2, R3
UXTB	R3, R4
PUSH	(R6)
PUSH	(R5)
BL	_TFT_Set_Brush+0
ADD	SP, SP, #8
LDR	R0, [SP, #4]
;BPWasher_driver.c,3070 :: 		}
IT	AL
BAL	L_DrawButton12
L_DrawButton11:
;BPWasher_driver.c,3072 :: 		TFT_Set_Brush(Abutton->Transparent, Abutton->Color, Abutton->Gradient, Abutton->Gradient_Orientation, Abutton->Gradient_Start_Color, Abutton->Gradient_End_Color);
ADDW	R1, R0, #44
LDRH	R1, [R1, #0]
UXTH	R6, R1
ADDW	R1, R0, #42
LDRH	R1, [R1, #0]
UXTH	R5, R1
ADDW	R1, R0, #40
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #39
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDW	R1, R0, #46
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
STR	R0, [SP, #4]
UXTB	R0, R1
UXTH	R1, R2
UXTB	R2, R3
UXTB	R3, R4
PUSH	(R6)
PUSH	(R5)
BL	_TFT_Set_Brush+0
ADD	SP, SP, #8
LDR	R0, [SP, #4]
;BPWasher_driver.c,3073 :: 		}
L_DrawButton12:
;BPWasher_driver.c,3074 :: 		TFT_Set_Pen(Abutton->Pen_Color, Abutton->Pen_Width);
ADDW	R1, R0, #14
LDRB	R1, [R1, #0]
UXTB	R2, R1
ADDW	R1, R0, #16
LDRH	R1, [R1, #0]
STR	R0, [SP, #4]
UXTH	R0, R1
UXTB	R1, R2
BL	_TFT_Set_Pen+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3075 :: 		TFT_Rectangle(Abutton->Left, Abutton->Top, Abutton->Left + Abutton->Width - 1, Abutton->Top + Abutton->Height - 1);
ADDW	R1, R0, #8
LDRH	R2, [R1, #0]
ADDW	R1, R0, #12
LDRH	R1, [R1, #0]
ADDS	R1, R2, R1
UXTH	R1, R1
SUBS	R5, R1, #1
ADDS	R1, R0, #6
LDRH	R4, [R1, #0]
ADDW	R1, R0, #10
LDRH	R1, [R1, #0]
ADDS	R1, R4, R1
UXTH	R1, R1
SUBS	R3, R1, #1
SXTH	R2, R2
SXTH	R1, R4
STR	R0, [SP, #4]
SXTH	R0, R1
SXTH	R1, R2
SXTH	R2, R3
SXTH	R3, R5
BL	_TFT_Rectangle+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3076 :: 		if (Abutton->VerticalText != 0)
ADDW	R1, R0, #38
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_DrawButton13
;BPWasher_driver.c,3077 :: 		TFT_Set_Font(Abutton->FontName, Abutton->Font_Color, FO_VERTICAL_COLUMN);
ADDW	R1, R0, #36
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #32
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
MOVS	R2, #1
BL	_TFT_Set_Font+0
LDR	R0, [SP, #4]
IT	AL
BAL	L_DrawButton14
L_DrawButton13:
;BPWasher_driver.c,3079 :: 		TFT_Set_Font(Abutton->FontName, Abutton->Font_Color, FO_HORIZONTAL);
ADDW	R1, R0, #36
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #32
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
MOVS	R2, #0
BL	_TFT_Set_Font+0
LDR	R0, [SP, #4]
L_DrawButton14:
;BPWasher_driver.c,3080 :: 		TFT_Write_Text_Return_Pos(Abutton->Caption, Abutton->Left, Abutton->Top);
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
UXTH	R3, R1
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #24
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
UXTH	R2, R3
BL	_TFT_Write_Text_Return_Pos+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3081 :: 		if (Abutton->TextAlign == _taLeft)
ADDW	R1, R0, #28
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	NE
BNE	L_DrawButton15
;BPWasher_driver.c,3082 :: 		ALeft = Abutton->Left + 4;
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
ADDS	R1, R1, #4
STRH	R1, [SP, #8]
IT	AL
BAL	L_DrawButton16
L_DrawButton15:
;BPWasher_driver.c,3083 :: 		else if (Abutton->TextAlign == _taCenter)
ADDW	R1, R0, #28
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawButton17
;BPWasher_driver.c,3084 :: 		ALeft = Abutton->Left + (Abutton->Width - caption_length) / 2;
ADDS	R1, R0, #6
LDRH	R3, [R1, #0]
ADDW	R1, R0, #10
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_caption_length+0)
MOVT	R1, #hi_addr(_caption_length+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
LSRS	R1, R1, #1
UXTH	R1, R1
ADDS	R1, R3, R1
STRH	R1, [SP, #8]
IT	AL
BAL	L_DrawButton18
L_DrawButton17:
;BPWasher_driver.c,3085 :: 		else if (Abutton->TextAlign == _taRight)
ADDW	R1, R0, #28
LDRB	R1, [R1, #0]
CMP	R1, #2
IT	NE
BNE	L_DrawButton19
;BPWasher_driver.c,3086 :: 		ALeft = Abutton->Left + Abutton->Width - caption_length - 4;
ADDS	R1, R0, #6
LDRH	R2, [R1, #0]
ADDW	R1, R0, #10
LDRH	R1, [R1, #0]
ADDS	R2, R2, R1
UXTH	R2, R2
MOVW	R1, #lo_addr(_caption_length+0)
MOVT	R1, #hi_addr(_caption_length+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
SUBS	R1, R1, #4
STRH	R1, [SP, #8]
L_DrawButton19:
L_DrawButton18:
L_DrawButton16:
;BPWasher_driver.c,3088 :: 		if (Abutton->TextAlignVertical == _tavTop)
ADDW	R1, R0, #29
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	NE
BNE	L_DrawButton20
;BPWasher_driver.c,3089 :: 		ATop = Abutton->Top + 4;
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
ADDS	R1, R1, #4
STRH	R1, [SP, #10]
IT	AL
BAL	L_DrawButton21
L_DrawButton20:
;BPWasher_driver.c,3090 :: 		else if (Abutton->TextAlignVertical == _tavMiddle)
ADDW	R1, R0, #29
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawButton22
;BPWasher_driver.c,3091 :: 		ATop = Abutton->Top + ((Abutton->Height - caption_height) / 2);
ADDW	R1, R0, #8
LDRH	R3, [R1, #0]
ADDW	R1, R0, #12
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_caption_height+0)
MOVT	R1, #hi_addr(_caption_height+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
LSRS	R1, R1, #1
UXTH	R1, R1
ADDS	R1, R3, R1
STRH	R1, [SP, #10]
IT	AL
BAL	L_DrawButton23
L_DrawButton22:
;BPWasher_driver.c,3092 :: 		else if (Abutton->TextAlignVertical == _tavBottom)
ADDW	R1, R0, #29
LDRB	R1, [R1, #0]
CMP	R1, #2
IT	NE
BNE	L_DrawButton24
;BPWasher_driver.c,3093 :: 		ATop = Abutton->Top + (Abutton->Height - caption_height - 4);
ADDW	R1, R0, #8
LDRH	R3, [R1, #0]
ADDW	R1, R0, #12
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_caption_height+0)
MOVT	R1, #hi_addr(_caption_height+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
SUBS	R1, R1, #4
UXTH	R1, R1
ADDS	R1, R3, R1
STRH	R1, [SP, #10]
L_DrawButton24:
L_DrawButton23:
L_DrawButton21:
;BPWasher_driver.c,3095 :: 		TFT_Write_Text(Abutton->Caption, ALeft, ATop);
ADDW	R1, R0, #24
; Abutton end address is: 0 (R0)
LDR	R1, [R1, #0]
LDRH	R2, [SP, #10]
MOV	R0, R1
LDRH	R1, [SP, #8]
BL	_TFT_Write_Text+0
;BPWasher_driver.c,3066 :: 		if (CurrentScreen == AButton->OwnerScreen && Abutton->Visible != 0) {
L__DrawButton335:
L__DrawButton334:
;BPWasher_driver.c,3097 :: 		}
L_end_DrawButton:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _DrawButton
_DrawRoundButton:
;BPWasher_driver.c,3099 :: 		void DrawRoundButton(TButton_Round *Around_button) {
; Around_button start address is: 0 (R0)
SUB	SP, SP, #12
STR	LR, [SP, #0]
; Around_button end address is: 0 (R0)
; Around_button start address is: 0 (R0)
;BPWasher_driver.c,3101 :: 		if (CurrentScreen == Around_button->OwnerScreen && Around_button->Visible != 0) {
LDR	R2, [R0, #0]
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
CMP	R1, R2
IT	NE
BNE	L__DrawRoundButton338
ADDW	R1, R0, #18
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__DrawRoundButton337
L__DrawRoundButton336:
;BPWasher_driver.c,3102 :: 		if (object_pressed == 1) {
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawRoundButton28
;BPWasher_driver.c,3103 :: 		object_pressed = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
STRB	R2, [R1, #0]
;BPWasher_driver.c,3105 :: 		Around_button->Gradient_End_Color, Around_button->Gradient_Start_Color);
ADDW	R1, R0, #42
LDRH	R1, [R1, #0]
UXTH	R6, R1
ADDW	R1, R0, #44
LDRH	R1, [R1, #0]
UXTH	R5, R1
;BPWasher_driver.c,3104 :: 		TFT_Set_Brush(Around_button->Transparent, Around_button->Press_Color, Around_button->Gradient, Around_button->Gradient_Orientation,
ADDW	R1, R0, #40
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #39
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDW	R1, R0, #50
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
STR	R0, [SP, #4]
UXTB	R0, R1
UXTH	R1, R2
UXTB	R2, R3
UXTB	R3, R4
;BPWasher_driver.c,3105 :: 		Around_button->Gradient_End_Color, Around_button->Gradient_Start_Color);
PUSH	(R6)
PUSH	(R5)
BL	_TFT_Set_Brush+0
ADD	SP, SP, #8
;BPWasher_driver.c,3104 :: 		TFT_Set_Brush(Around_button->Transparent, Around_button->Press_Color, Around_button->Gradient, Around_button->Gradient_Orientation,
LDR	R0, [SP, #4]
;BPWasher_driver.c,3106 :: 		}
IT	AL
BAL	L_DrawRoundButton29
L_DrawRoundButton28:
;BPWasher_driver.c,3109 :: 		Around_button->Gradient_Start_Color, Around_button->Gradient_End_Color);
ADDW	R1, R0, #44
LDRH	R1, [R1, #0]
UXTH	R6, R1
ADDW	R1, R0, #42
LDRH	R1, [R1, #0]
UXTH	R5, R1
;BPWasher_driver.c,3108 :: 		TFT_Set_Brush(Around_button->Transparent, Around_button->Color, Around_button->Gradient, Around_button->Gradient_Orientation,
ADDW	R1, R0, #40
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #39
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDW	R1, R0, #46
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
STR	R0, [SP, #4]
UXTB	R0, R1
UXTH	R1, R2
UXTB	R2, R3
UXTB	R3, R4
;BPWasher_driver.c,3109 :: 		Around_button->Gradient_Start_Color, Around_button->Gradient_End_Color);
PUSH	(R6)
PUSH	(R5)
BL	_TFT_Set_Brush+0
ADD	SP, SP, #8
;BPWasher_driver.c,3108 :: 		TFT_Set_Brush(Around_button->Transparent, Around_button->Color, Around_button->Gradient, Around_button->Gradient_Orientation,
LDR	R0, [SP, #4]
;BPWasher_driver.c,3110 :: 		}
L_DrawRoundButton29:
;BPWasher_driver.c,3111 :: 		TFT_Set_Pen(Around_button->Pen_Color, Around_button->Pen_Width);
ADDW	R1, R0, #14
LDRB	R1, [R1, #0]
UXTB	R2, R1
ADDW	R1, R0, #16
LDRH	R1, [R1, #0]
STR	R0, [SP, #4]
UXTH	R0, R1
UXTB	R1, R2
BL	_TFT_Set_Pen+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3114 :: 		Around_button->Top + Around_button->Height - 2, Around_button->Corner_Radius);
ADDW	R1, R0, #48
LDRB	R1, [R1, #0]
UXTB	R6, R1
ADDW	R1, R0, #8
LDRH	R2, [R1, #0]
ADDW	R1, R0, #12
LDRH	R1, [R1, #0]
ADDS	R1, R2, R1
UXTH	R1, R1
SUBS	R5, R1, #2
;BPWasher_driver.c,3113 :: 		Around_button->Left + Around_button->Width - 2,
ADDS	R1, R0, #6
LDRH	R4, [R1, #0]
ADDW	R1, R0, #10
LDRH	R1, [R1, #0]
ADDS	R1, R4, R1
UXTH	R1, R1
SUBS	R3, R1, #2
;BPWasher_driver.c,3112 :: 		TFT_Rectangle_Round_Edges(Around_button->Left + 1, Around_button->Top + 1,
ADDS	R2, R2, #1
ADDS	R1, R4, #1
;BPWasher_driver.c,3114 :: 		Around_button->Top + Around_button->Height - 2, Around_button->Corner_Radius);
STR	R0, [SP, #4]
;BPWasher_driver.c,3112 :: 		TFT_Rectangle_Round_Edges(Around_button->Left + 1, Around_button->Top + 1,
UXTH	R0, R1
UXTH	R1, R2
;BPWasher_driver.c,3113 :: 		Around_button->Left + Around_button->Width - 2,
UXTH	R2, R3
;BPWasher_driver.c,3114 :: 		Around_button->Top + Around_button->Height - 2, Around_button->Corner_Radius);
UXTH	R3, R5
PUSH	(R6)
BL	_TFT_Rectangle_Round_Edges+0
ADD	SP, SP, #4
LDR	R0, [SP, #4]
;BPWasher_driver.c,3115 :: 		if (Around_button->VerticalText != 0)
ADDW	R1, R0, #38
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_DrawRoundButton30
;BPWasher_driver.c,3116 :: 		TFT_Set_Font(Around_button->FontName, Around_button->Font_Color, FO_VERTICAL_COLUMN);
ADDW	R1, R0, #36
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #32
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
MOVS	R2, #1
BL	_TFT_Set_Font+0
LDR	R0, [SP, #4]
IT	AL
BAL	L_DrawRoundButton31
L_DrawRoundButton30:
;BPWasher_driver.c,3118 :: 		TFT_Set_Font(Around_button->FontName, Around_button->Font_Color, FO_HORIZONTAL);
ADDW	R1, R0, #36
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #32
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
MOVS	R2, #0
BL	_TFT_Set_Font+0
LDR	R0, [SP, #4]
L_DrawRoundButton31:
;BPWasher_driver.c,3119 :: 		TFT_Write_Text_Return_Pos(Around_button->Caption, Around_button->Left, Around_button->Top);
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
UXTH	R3, R1
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #24
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
UXTH	R2, R3
BL	_TFT_Write_Text_Return_Pos+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3120 :: 		if (Around_button->TextAlign == _taLeft)
ADDW	R1, R0, #28
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	NE
BNE	L_DrawRoundButton32
;BPWasher_driver.c,3121 :: 		ALeft = Around_button->Left + 4;
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
ADDS	R1, R1, #4
STRH	R1, [SP, #8]
IT	AL
BAL	L_DrawRoundButton33
L_DrawRoundButton32:
;BPWasher_driver.c,3122 :: 		else if (Around_button->TextAlign == _taCenter)
ADDW	R1, R0, #28
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawRoundButton34
;BPWasher_driver.c,3123 :: 		ALeft = Around_button->Left + (Around_button->Width - caption_length) / 2;
ADDS	R1, R0, #6
LDRH	R3, [R1, #0]
ADDW	R1, R0, #10
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_caption_length+0)
MOVT	R1, #hi_addr(_caption_length+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
LSRS	R1, R1, #1
UXTH	R1, R1
ADDS	R1, R3, R1
STRH	R1, [SP, #8]
IT	AL
BAL	L_DrawRoundButton35
L_DrawRoundButton34:
;BPWasher_driver.c,3124 :: 		else if (Around_button->TextAlign == _taRight)
ADDW	R1, R0, #28
LDRB	R1, [R1, #0]
CMP	R1, #2
IT	NE
BNE	L_DrawRoundButton36
;BPWasher_driver.c,3125 :: 		ALeft = Around_button->Left + Around_button->Width - caption_length - 4;
ADDS	R1, R0, #6
LDRH	R2, [R1, #0]
ADDW	R1, R0, #10
LDRH	R1, [R1, #0]
ADDS	R2, R2, R1
UXTH	R2, R2
MOVW	R1, #lo_addr(_caption_length+0)
MOVT	R1, #hi_addr(_caption_length+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
SUBS	R1, R1, #4
STRH	R1, [SP, #8]
L_DrawRoundButton36:
L_DrawRoundButton35:
L_DrawRoundButton33:
;BPWasher_driver.c,3127 :: 		if (Around_button->TextAlignVertical == _tavTop)
ADDW	R1, R0, #29
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	NE
BNE	L_DrawRoundButton37
;BPWasher_driver.c,3128 :: 		ATop = Around_button->Top + 3;
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
ADDS	R1, R1, #3
STRH	R1, [SP, #10]
IT	AL
BAL	L_DrawRoundButton38
L_DrawRoundButton37:
;BPWasher_driver.c,3129 :: 		else if (Around_button->TextAlignVertical == _tavMiddle)
ADDW	R1, R0, #29
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawRoundButton39
;BPWasher_driver.c,3130 :: 		ATop = Around_button->Top + (Around_button->Height - caption_height) / 2;
ADDW	R1, R0, #8
LDRH	R3, [R1, #0]
ADDW	R1, R0, #12
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_caption_height+0)
MOVT	R1, #hi_addr(_caption_height+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
LSRS	R1, R1, #1
UXTH	R1, R1
ADDS	R1, R3, R1
STRH	R1, [SP, #10]
IT	AL
BAL	L_DrawRoundButton40
L_DrawRoundButton39:
;BPWasher_driver.c,3131 :: 		else if (Around_button->TextAlignVertical == _tavBottom)
ADDW	R1, R0, #29
LDRB	R1, [R1, #0]
CMP	R1, #2
IT	NE
BNE	L_DrawRoundButton41
;BPWasher_driver.c,3132 :: 		ATop  = Around_button->Top + Around_button->Height - caption_height - 4;
ADDW	R1, R0, #8
LDRH	R2, [R1, #0]
ADDW	R1, R0, #12
LDRH	R1, [R1, #0]
ADDS	R2, R2, R1
UXTH	R2, R2
MOVW	R1, #lo_addr(_caption_height+0)
MOVT	R1, #hi_addr(_caption_height+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
SUBS	R1, R1, #4
STRH	R1, [SP, #10]
L_DrawRoundButton41:
L_DrawRoundButton40:
L_DrawRoundButton38:
;BPWasher_driver.c,3134 :: 		TFT_Write_Text(Around_button->Caption, ALeft, ATop);
ADDW	R1, R0, #24
; Around_button end address is: 0 (R0)
LDR	R1, [R1, #0]
LDRH	R2, [SP, #10]
MOV	R0, R1
LDRH	R1, [SP, #8]
BL	_TFT_Write_Text+0
;BPWasher_driver.c,3101 :: 		if (CurrentScreen == Around_button->OwnerScreen && Around_button->Visible != 0) {
L__DrawRoundButton338:
L__DrawRoundButton337:
;BPWasher_driver.c,3136 :: 		}
L_end_DrawRoundButton:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _DrawRoundButton
_DrawCRoundButton:
;BPWasher_driver.c,3138 :: 		void DrawCRoundButton(TCButton_Round *Around_button) {
; Around_button start address is: 0 (R0)
SUB	SP, SP, #12
STR	LR, [SP, #0]
; Around_button end address is: 0 (R0)
; Around_button start address is: 0 (R0)
;BPWasher_driver.c,3140 :: 		if (CurrentScreen == Around_button->OwnerScreen && Around_button->Visible != 0) {
LDR	R2, [R0, #0]
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
CMP	R1, R2
IT	NE
BNE	L__DrawCRoundButton341
ADDW	R1, R0, #18
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__DrawCRoundButton340
L__DrawCRoundButton339:
;BPWasher_driver.c,3141 :: 		if (object_pressed == 1) {
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawCRoundButton45
;BPWasher_driver.c,3142 :: 		object_pressed = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
STRB	R2, [R1, #0]
;BPWasher_driver.c,3144 :: 		Around_button->Gradient_End_Color, Around_button->Gradient_Start_Color);
ADDW	R1, R0, #42
LDRH	R1, [R1, #0]
UXTH	R6, R1
ADDW	R1, R0, #44
LDRH	R1, [R1, #0]
UXTH	R5, R1
;BPWasher_driver.c,3143 :: 		TFT_Set_Brush(Around_button->Transparent, Around_button->Press_Color, Around_button->Gradient, Around_button->Gradient_Orientation,
ADDW	R1, R0, #40
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #39
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDW	R1, R0, #50
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
STR	R0, [SP, #4]
UXTB	R0, R1
UXTH	R1, R2
UXTB	R2, R3
UXTB	R3, R4
;BPWasher_driver.c,3144 :: 		Around_button->Gradient_End_Color, Around_button->Gradient_Start_Color);
PUSH	(R6)
PUSH	(R5)
BL	_TFT_Set_Brush+0
ADD	SP, SP, #8
;BPWasher_driver.c,3143 :: 		TFT_Set_Brush(Around_button->Transparent, Around_button->Press_Color, Around_button->Gradient, Around_button->Gradient_Orientation,
LDR	R0, [SP, #4]
;BPWasher_driver.c,3145 :: 		}
IT	AL
BAL	L_DrawCRoundButton46
L_DrawCRoundButton45:
;BPWasher_driver.c,3148 :: 		Around_button->Gradient_Start_Color, Around_button->Gradient_End_Color);
ADDW	R1, R0, #44
LDRH	R1, [R1, #0]
UXTH	R6, R1
ADDW	R1, R0, #42
LDRH	R1, [R1, #0]
UXTH	R5, R1
;BPWasher_driver.c,3147 :: 		TFT_Set_Brush(Around_button->Transparent, Around_button->Color, Around_button->Gradient, Around_button->Gradient_Orientation,
ADDW	R1, R0, #40
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #39
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDW	R1, R0, #46
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
STR	R0, [SP, #4]
UXTB	R0, R1
UXTH	R1, R2
UXTB	R2, R3
UXTB	R3, R4
;BPWasher_driver.c,3148 :: 		Around_button->Gradient_Start_Color, Around_button->Gradient_End_Color);
PUSH	(R6)
PUSH	(R5)
BL	_TFT_Set_Brush+0
ADD	SP, SP, #8
;BPWasher_driver.c,3147 :: 		TFT_Set_Brush(Around_button->Transparent, Around_button->Color, Around_button->Gradient, Around_button->Gradient_Orientation,
LDR	R0, [SP, #4]
;BPWasher_driver.c,3149 :: 		}
L_DrawCRoundButton46:
;BPWasher_driver.c,3150 :: 		TFT_Set_Pen(Around_button->Pen_Color, Around_button->Pen_Width);
ADDW	R1, R0, #14
LDRB	R1, [R1, #0]
UXTB	R2, R1
ADDW	R1, R0, #16
LDRH	R1, [R1, #0]
STR	R0, [SP, #4]
UXTH	R0, R1
UXTB	R1, R2
BL	_TFT_Set_Pen+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3153 :: 		Around_button->Top + Around_button->Height - 2, Around_button->Corner_Radius);
ADDW	R1, R0, #48
LDRB	R1, [R1, #0]
UXTB	R6, R1
ADDW	R1, R0, #8
LDRH	R2, [R1, #0]
ADDW	R1, R0, #12
LDRH	R1, [R1, #0]
ADDS	R1, R2, R1
UXTH	R1, R1
SUBS	R5, R1, #2
;BPWasher_driver.c,3152 :: 		Around_button->Left + Around_button->Width - 2,
ADDS	R1, R0, #6
LDRH	R4, [R1, #0]
ADDW	R1, R0, #10
LDRH	R1, [R1, #0]
ADDS	R1, R4, R1
UXTH	R1, R1
SUBS	R3, R1, #2
;BPWasher_driver.c,3151 :: 		TFT_Rectangle_Round_Edges(Around_button->Left + 1, Around_button->Top + 1,
ADDS	R2, R2, #1
ADDS	R1, R4, #1
;BPWasher_driver.c,3153 :: 		Around_button->Top + Around_button->Height - 2, Around_button->Corner_Radius);
STR	R0, [SP, #4]
;BPWasher_driver.c,3151 :: 		TFT_Rectangle_Round_Edges(Around_button->Left + 1, Around_button->Top + 1,
UXTH	R0, R1
UXTH	R1, R2
;BPWasher_driver.c,3152 :: 		Around_button->Left + Around_button->Width - 2,
UXTH	R2, R3
;BPWasher_driver.c,3153 :: 		Around_button->Top + Around_button->Height - 2, Around_button->Corner_Radius);
UXTH	R3, R5
PUSH	(R6)
BL	_TFT_Rectangle_Round_Edges+0
ADD	SP, SP, #4
LDR	R0, [SP, #4]
;BPWasher_driver.c,3154 :: 		if (Around_button->VerticalText != 0)
ADDW	R1, R0, #38
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_DrawCRoundButton47
;BPWasher_driver.c,3155 :: 		TFT_Set_Font(Around_button->FontName, Around_button->Font_Color, FO_VERTICAL_COLUMN);
ADDW	R1, R0, #36
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #32
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
MOVS	R2, #1
BL	_TFT_Set_Font+0
LDR	R0, [SP, #4]
IT	AL
BAL	L_DrawCRoundButton48
L_DrawCRoundButton47:
;BPWasher_driver.c,3157 :: 		TFT_Set_Font(Around_button->FontName, Around_button->Font_Color, FO_HORIZONTAL);
ADDW	R1, R0, #36
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #32
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
MOVS	R2, #0
BL	_TFT_Set_Font+0
LDR	R0, [SP, #4]
L_DrawCRoundButton48:
;BPWasher_driver.c,3158 :: 		TFT_Write_Text_Return_Pos(Around_button->Caption, Around_button->Left, Around_button->Top);
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
UXTH	R3, R1
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #24
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
UXTH	R2, R3
BL	_TFT_Write_Text_Return_Pos+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3159 :: 		if (Around_button->TextAlign == _taLeft)
ADDW	R1, R0, #28
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	NE
BNE	L_DrawCRoundButton49
;BPWasher_driver.c,3160 :: 		ALeft = Around_button->Left + 4;
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
ADDS	R1, R1, #4
STRH	R1, [SP, #8]
IT	AL
BAL	L_DrawCRoundButton50
L_DrawCRoundButton49:
;BPWasher_driver.c,3161 :: 		else if (Around_button->TextAlign == _taCenter)
ADDW	R1, R0, #28
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawCRoundButton51
;BPWasher_driver.c,3162 :: 		ALeft = Around_button->Left + (Around_button->Width - caption_length) / 2;
ADDS	R1, R0, #6
LDRH	R3, [R1, #0]
ADDW	R1, R0, #10
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_caption_length+0)
MOVT	R1, #hi_addr(_caption_length+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
LSRS	R1, R1, #1
UXTH	R1, R1
ADDS	R1, R3, R1
STRH	R1, [SP, #8]
IT	AL
BAL	L_DrawCRoundButton52
L_DrawCRoundButton51:
;BPWasher_driver.c,3163 :: 		else if (Around_button->TextAlign == _taRight)
ADDW	R1, R0, #28
LDRB	R1, [R1, #0]
CMP	R1, #2
IT	NE
BNE	L_DrawCRoundButton53
;BPWasher_driver.c,3164 :: 		ALeft = Around_button->Left + Around_button->Width - caption_length - 4;
ADDS	R1, R0, #6
LDRH	R2, [R1, #0]
ADDW	R1, R0, #10
LDRH	R1, [R1, #0]
ADDS	R2, R2, R1
UXTH	R2, R2
MOVW	R1, #lo_addr(_caption_length+0)
MOVT	R1, #hi_addr(_caption_length+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
SUBS	R1, R1, #4
STRH	R1, [SP, #8]
L_DrawCRoundButton53:
L_DrawCRoundButton52:
L_DrawCRoundButton50:
;BPWasher_driver.c,3166 :: 		if (Around_button->TextAlignVertical == _tavTop)
ADDW	R1, R0, #29
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	NE
BNE	L_DrawCRoundButton54
;BPWasher_driver.c,3167 :: 		ATop = Around_button->Top + 3;
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
ADDS	R1, R1, #3
STRH	R1, [SP, #10]
IT	AL
BAL	L_DrawCRoundButton55
L_DrawCRoundButton54:
;BPWasher_driver.c,3168 :: 		else if (Around_button->TextAlignVertical == _tavMiddle)
ADDW	R1, R0, #29
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawCRoundButton56
;BPWasher_driver.c,3169 :: 		ATop = Around_button->Top + (Around_button->Height - caption_height) / 2;
ADDW	R1, R0, #8
LDRH	R3, [R1, #0]
ADDW	R1, R0, #12
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_caption_height+0)
MOVT	R1, #hi_addr(_caption_height+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
LSRS	R1, R1, #1
UXTH	R1, R1
ADDS	R1, R3, R1
STRH	R1, [SP, #10]
IT	AL
BAL	L_DrawCRoundButton57
L_DrawCRoundButton56:
;BPWasher_driver.c,3170 :: 		else if (Around_button->TextAlignVertical == _tavBottom)
ADDW	R1, R0, #29
LDRB	R1, [R1, #0]
CMP	R1, #2
IT	NE
BNE	L_DrawCRoundButton58
;BPWasher_driver.c,3171 :: 		ATop  = Around_button->Top + Around_button->Height - caption_height - 4;
ADDW	R1, R0, #8
LDRH	R2, [R1, #0]
ADDW	R1, R0, #12
LDRH	R1, [R1, #0]
ADDS	R2, R2, R1
UXTH	R2, R2
MOVW	R1, #lo_addr(_caption_height+0)
MOVT	R1, #hi_addr(_caption_height+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
SUBS	R1, R1, #4
STRH	R1, [SP, #10]
L_DrawCRoundButton58:
L_DrawCRoundButton57:
L_DrawCRoundButton55:
;BPWasher_driver.c,3173 :: 		TFT_Write_Text(Around_button->Caption, ALeft, ATop);
ADDW	R1, R0, #24
; Around_button end address is: 0 (R0)
LDR	R1, [R1, #0]
LDRH	R2, [SP, #10]
MOV	R0, R1
LDRH	R1, [SP, #8]
BL	_TFT_Write_Text+0
;BPWasher_driver.c,3140 :: 		if (CurrentScreen == Around_button->OwnerScreen && Around_button->Visible != 0) {
L__DrawCRoundButton341:
L__DrawCRoundButton340:
;BPWasher_driver.c,3175 :: 		}
L_end_DrawCRoundButton:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _DrawCRoundButton
_DrawLabel:
;BPWasher_driver.c,3177 :: 		void DrawLabel(TLabel *ALabel) {
; ALabel start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; ALabel end address is: 0 (R0)
; ALabel start address is: 0 (R0)
;BPWasher_driver.c,3178 :: 		if (CurrentScreen == ALabel->OwnerScreen && ALabel->Visible != 0) {
LDR	R2, [R0, #0]
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
CMP	R1, R2
IT	NE
BNE	L__DrawLabel344
ADDW	R1, R0, #27
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__DrawLabel343
L__DrawLabel342:
;BPWasher_driver.c,3179 :: 		if (ALabel->VerticalText != 0)
ADDW	R1, R0, #26
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_DrawLabel62
;BPWasher_driver.c,3180 :: 		TFT_Set_Font(ALabel->FontName, ALabel->Font_Color, FO_VERTICAL_COLUMN);
ADDW	R1, R0, #24
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #20
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
MOVS	R2, #1
BL	_TFT_Set_Font+0
LDR	R0, [SP, #4]
IT	AL
BAL	L_DrawLabel63
L_DrawLabel62:
;BPWasher_driver.c,3182 :: 		TFT_Set_Font(ALabel->FontName, ALabel->Font_Color, FO_HORIZONTAL);
ADDW	R1, R0, #24
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #20
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
MOVS	R2, #0
BL	_TFT_Set_Font+0
LDR	R0, [SP, #4]
L_DrawLabel63:
;BPWasher_driver.c,3183 :: 		TFT_Write_Text(ALabel->Caption, ALabel->Left, ALabel->Top);
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
UXTH	R3, R1
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #16
; ALabel end address is: 0 (R0)
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
UXTH	R2, R3
BL	_TFT_Write_Text+0
;BPWasher_driver.c,3178 :: 		if (CurrentScreen == ALabel->OwnerScreen && ALabel->Visible != 0) {
L__DrawLabel344:
L__DrawLabel343:
;BPWasher_driver.c,3185 :: 		}
L_end_DrawLabel:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _DrawLabel
_DrawImage:
;BPWasher_driver.c,3187 :: 		void DrawImage(TImage *AImage) {
; AImage start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; AImage end address is: 0 (R0)
; AImage start address is: 0 (R0)
;BPWasher_driver.c,3188 :: 		if (CurrentScreen == AImage->OwnerScreen && AImage->Visible != 0) {
LDR	R2, [R0, #0]
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
CMP	R1, R2
IT	NE
BNE	L__DrawImage347
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__DrawImage346
L__DrawImage345:
;BPWasher_driver.c,3189 :: 		if (AImage->Picture_Type == 0)
ADDW	R1, R0, #22
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	NE
BNE	L_DrawImage67
;BPWasher_driver.c,3190 :: 		TFT_Image(AImage->Left, AImage->Top, AImage->Picture_Name, AImage->Picture_Ratio);
ADDW	R1, R0, #23
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #16
LDR	R1, [R1, #0]
MOV	R3, R1
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
STR	R0, [SP, #4]
UXTH	R0, R1
UXTH	R1, R2
MOV	R2, R3
UXTB	R3, R4
BL	_TFT_Image+0
LDR	R0, [SP, #4]
L_DrawImage67:
;BPWasher_driver.c,3191 :: 		if (AImage->Picture_Type == 1)
ADDW	R1, R0, #22
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawImage68
;BPWasher_driver.c,3192 :: 		TFT_Image_Jpeg(AImage->Left, AImage->Top, AImage->Picture_Name);
ADDW	R1, R0, #16
LDR	R1, [R1, #0]
MOV	R3, R1
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDS	R1, R0, #6
; AImage end address is: 0 (R0)
LDRH	R1, [R1, #0]
UXTH	R0, R1
UXTH	R1, R2
MOV	R2, R3
BL	_TFT_Image_Jpeg+0
L_DrawImage68:
;BPWasher_driver.c,3188 :: 		if (CurrentScreen == AImage->OwnerScreen && AImage->Visible != 0) {
L__DrawImage347:
L__DrawImage346:
;BPWasher_driver.c,3194 :: 		}
L_end_DrawImage:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _DrawImage
_DrawCImage:
;BPWasher_driver.c,3196 :: 		void DrawCImage(TCImage *AImage) {
; AImage start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; AImage end address is: 0 (R0)
; AImage start address is: 0 (R0)
;BPWasher_driver.c,3197 :: 		if (CurrentScreen == AImage->OwnerScreen && AImage->Visible != 0) {
LDR	R2, [R0, #0]
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
CMP	R1, R2
IT	NE
BNE	L__DrawCImage350
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__DrawCImage349
L__DrawCImage348:
;BPWasher_driver.c,3198 :: 		if (AImage->Picture_Type == 0)
ADDW	R1, R0, #22
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	NE
BNE	L_DrawCImage72
;BPWasher_driver.c,3199 :: 		TFT_Image(AImage->Left, AImage->Top, AImage->Picture_Name, AImage->Picture_Ratio);
ADDW	R1, R0, #23
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #16
LDR	R1, [R1, #0]
MOV	R3, R1
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
STR	R0, [SP, #4]
UXTH	R0, R1
UXTH	R1, R2
MOV	R2, R3
UXTB	R3, R4
BL	_TFT_Image+0
LDR	R0, [SP, #4]
L_DrawCImage72:
;BPWasher_driver.c,3200 :: 		if (AImage->Picture_Type == 1)
ADDW	R1, R0, #22
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawCImage73
;BPWasher_driver.c,3201 :: 		TFT_Image_Jpeg(AImage->Left, AImage->Top, AImage->Picture_Name);
ADDW	R1, R0, #16
LDR	R1, [R1, #0]
MOV	R3, R1
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDS	R1, R0, #6
; AImage end address is: 0 (R0)
LDRH	R1, [R1, #0]
UXTH	R0, R1
UXTH	R1, R2
MOV	R2, R3
BL	_TFT_Image_Jpeg+0
L_DrawCImage73:
;BPWasher_driver.c,3197 :: 		if (CurrentScreen == AImage->OwnerScreen && AImage->Visible != 0) {
L__DrawCImage350:
L__DrawCImage349:
;BPWasher_driver.c,3203 :: 		}
L_end_DrawCImage:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _DrawCImage
_DrawCircle:
;BPWasher_driver.c,3205 :: 		void DrawCircle(TCircle *ACircle) {
; ACircle start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; ACircle end address is: 0 (R0)
; ACircle start address is: 0 (R0)
;BPWasher_driver.c,3206 :: 		if (CurrentScreen == ACircle->OwnerScreen && ACircle->Visible != 0) {
LDR	R2, [R0, #0]
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
CMP	R1, R2
IT	NE
BNE	L__DrawCircle353
ADDW	R1, R0, #16
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__DrawCircle352
L__DrawCircle351:
;BPWasher_driver.c,3207 :: 		if (object_pressed == 1) {
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawCircle77
;BPWasher_driver.c,3208 :: 		object_pressed = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
STRB	R2, [R1, #0]
;BPWasher_driver.c,3210 :: 		ACircle->Gradient_End_Color, ACircle->Gradient_Start_Color);
ADDW	R1, R0, #22
LDRH	R1, [R1, #0]
UXTH	R6, R1
ADDW	R1, R0, #24
LDRH	R1, [R1, #0]
UXTH	R5, R1
;BPWasher_driver.c,3209 :: 		TFT_Set_Brush(ACircle->Transparent, ACircle->Press_Color, ACircle->Gradient, ACircle->Gradient_Orientation,
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #19
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDW	R1, R0, #30
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #18
LDRB	R1, [R1, #0]
STR	R0, [SP, #4]
UXTB	R0, R1
UXTH	R1, R2
UXTB	R2, R3
UXTB	R3, R4
;BPWasher_driver.c,3210 :: 		ACircle->Gradient_End_Color, ACircle->Gradient_Start_Color);
PUSH	(R6)
PUSH	(R5)
BL	_TFT_Set_Brush+0
ADD	SP, SP, #8
;BPWasher_driver.c,3209 :: 		TFT_Set_Brush(ACircle->Transparent, ACircle->Press_Color, ACircle->Gradient, ACircle->Gradient_Orientation,
LDR	R0, [SP, #4]
;BPWasher_driver.c,3211 :: 		}
IT	AL
BAL	L_DrawCircle78
L_DrawCircle77:
;BPWasher_driver.c,3214 :: 		ACircle->Gradient_Start_Color, ACircle->Gradient_End_Color);
ADDW	R1, R0, #24
LDRH	R1, [R1, #0]
UXTH	R6, R1
ADDW	R1, R0, #22
LDRH	R1, [R1, #0]
UXTH	R5, R1
;BPWasher_driver.c,3213 :: 		TFT_Set_Brush(ACircle->Transparent, ACircle->Color, ACircle->Gradient, ACircle->Gradient_Orientation,
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #19
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDW	R1, R0, #26
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #18
LDRB	R1, [R1, #0]
STR	R0, [SP, #4]
UXTB	R0, R1
UXTH	R1, R2
UXTB	R2, R3
UXTB	R3, R4
;BPWasher_driver.c,3214 :: 		ACircle->Gradient_Start_Color, ACircle->Gradient_End_Color);
PUSH	(R6)
PUSH	(R5)
BL	_TFT_Set_Brush+0
ADD	SP, SP, #8
;BPWasher_driver.c,3213 :: 		TFT_Set_Brush(ACircle->Transparent, ACircle->Color, ACircle->Gradient, ACircle->Gradient_Orientation,
LDR	R0, [SP, #4]
;BPWasher_driver.c,3215 :: 		}
L_DrawCircle78:
;BPWasher_driver.c,3216 :: 		TFT_Set_Pen(ACircle->Pen_Color, ACircle->Pen_Width);
ADDW	R1, R0, #12
LDRB	R1, [R1, #0]
UXTB	R2, R1
ADDW	R1, R0, #14
LDRH	R1, [R1, #0]
STR	R0, [SP, #4]
UXTH	R0, R1
UXTB	R1, R2
BL	_TFT_Set_Pen+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3219 :: 		ACircle->Radius);
ADDW	R1, R0, #10
LDRH	R4, [R1, #0]
SXTH	R3, R4
;BPWasher_driver.c,3218 :: 		ACircle->Top + ACircle->Radius,
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
ADDS	R2, R1, R4
;BPWasher_driver.c,3217 :: 		TFT_Circle(ACircle->Left + ACircle->Radius,
ADDS	R1, R0, #6
; ACircle end address is: 0 (R0)
LDRH	R1, [R1, #0]
ADDS	R1, R1, R4
SXTH	R0, R1
;BPWasher_driver.c,3218 :: 		ACircle->Top + ACircle->Radius,
SXTH	R1, R2
;BPWasher_driver.c,3219 :: 		ACircle->Radius);
SXTH	R2, R3
BL	_TFT_Circle+0
;BPWasher_driver.c,3206 :: 		if (CurrentScreen == ACircle->OwnerScreen && ACircle->Visible != 0) {
L__DrawCircle353:
L__DrawCircle352:
;BPWasher_driver.c,3221 :: 		}
L_end_DrawCircle:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _DrawCircle
_DrawBox:
;BPWasher_driver.c,3223 :: 		void DrawBox(TBox *ABox) {
; ABox start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; ABox end address is: 0 (R0)
; ABox start address is: 0 (R0)
;BPWasher_driver.c,3224 :: 		if (CurrentScreen == ABox->OwnerScreen &&  ABox->Visible != 0) {
LDR	R2, [R0, #0]
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
CMP	R1, R2
IT	NE
BNE	L__DrawBox356
ADDW	R1, R0, #18
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__DrawBox355
L__DrawBox354:
;BPWasher_driver.c,3225 :: 		if (object_pressed == 1) {
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawBox82
;BPWasher_driver.c,3226 :: 		object_pressed = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
STRB	R2, [R1, #0]
;BPWasher_driver.c,3227 :: 		TFT_Set_Brush(ABox->Transparent, ABox->Press_Color, ABox->Gradient, ABox->Gradient_Orientation, ABox->Gradient_End_Color, ABox->Gradient_Start_Color);
ADDW	R1, R0, #24
LDRH	R1, [R1, #0]
UXTH	R6, R1
ADDW	R1, R0, #26
LDRH	R1, [R1, #0]
UXTH	R5, R1
ADDW	R1, R0, #22
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #21
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDW	R1, R0, #32
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
STR	R0, [SP, #4]
UXTB	R0, R1
UXTH	R1, R2
UXTB	R2, R3
UXTB	R3, R4
PUSH	(R6)
PUSH	(R5)
BL	_TFT_Set_Brush+0
ADD	SP, SP, #8
LDR	R0, [SP, #4]
;BPWasher_driver.c,3228 :: 		}
IT	AL
BAL	L_DrawBox83
L_DrawBox82:
;BPWasher_driver.c,3230 :: 		TFT_Set_Brush(ABox->Transparent, ABox->Color, ABox->Gradient, ABox->Gradient_Orientation, ABox->Gradient_Start_Color, ABox->Gradient_End_Color);
ADDW	R1, R0, #26
LDRH	R1, [R1, #0]
UXTH	R6, R1
ADDW	R1, R0, #24
LDRH	R1, [R1, #0]
UXTH	R5, R1
ADDW	R1, R0, #22
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #21
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDW	R1, R0, #28
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
STR	R0, [SP, #4]
UXTB	R0, R1
UXTH	R1, R2
UXTB	R2, R3
UXTB	R3, R4
PUSH	(R6)
PUSH	(R5)
BL	_TFT_Set_Brush+0
ADD	SP, SP, #8
LDR	R0, [SP, #4]
;BPWasher_driver.c,3231 :: 		}
L_DrawBox83:
;BPWasher_driver.c,3232 :: 		TFT_Set_Pen(ABox->Pen_Color, ABox->Pen_Width);
ADDW	R1, R0, #14
LDRB	R1, [R1, #0]
UXTB	R2, R1
ADDW	R1, R0, #16
LDRH	R1, [R1, #0]
STR	R0, [SP, #4]
UXTH	R0, R1
UXTB	R1, R2
BL	_TFT_Set_Pen+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3233 :: 		TFT_Rectangle(ABox->Left, ABox->Top, ABox->Left + ABox->Width - 1, ABox->Top + ABox->Height - 1);
ADDW	R1, R0, #8
LDRH	R2, [R1, #0]
ADDW	R1, R0, #12
LDRH	R1, [R1, #0]
ADDS	R1, R2, R1
UXTH	R1, R1
SUBS	R5, R1, #1
ADDS	R1, R0, #6
LDRH	R4, [R1, #0]
ADDW	R1, R0, #10
; ABox end address is: 0 (R0)
LDRH	R1, [R1, #0]
ADDS	R1, R4, R1
UXTH	R1, R1
SUBS	R3, R1, #1
SXTH	R2, R2
SXTH	R1, R4
SXTH	R0, R1
SXTH	R1, R2
SXTH	R2, R3
SXTH	R3, R5
BL	_TFT_Rectangle+0
;BPWasher_driver.c,3224 :: 		if (CurrentScreen == ABox->OwnerScreen &&  ABox->Visible != 0) {
L__DrawBox356:
L__DrawBox355:
;BPWasher_driver.c,3235 :: 		}
L_end_DrawBox:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _DrawBox
_DrawLine:
;BPWasher_driver.c,3237 :: 		void DrawLine(TLine *Aline) {
; Aline start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
MOV	R5, R0
; Aline end address is: 0 (R0)
; Aline start address is: 20 (R5)
;BPWasher_driver.c,3238 :: 		if (CurrentScreen == ALine->OwnerScreen && Aline->Visible != 0) {
LDR	R2, [R5, #0]
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
CMP	R1, R2
IT	NE
BNE	L__DrawLine359
ADDW	R1, R5, #15
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__DrawLine358
L__DrawLine357:
;BPWasher_driver.c,3239 :: 		TFT_Set_Pen(Aline->Color, Aline->Pen_Width);
ADDW	R1, R5, #14
LDRB	R1, [R1, #0]
UXTB	R2, R1
ADDW	R1, R5, #16
LDRH	R1, [R1, #0]
UXTH	R0, R1
UXTB	R1, R2
BL	_TFT_Set_Pen+0
;BPWasher_driver.c,3240 :: 		TFT_Line(Aline->First_Point_X, Aline->First_Point_Y, Aline->Second_Point_X, Aline->Second_Point_Y);
ADDW	R1, R5, #12
LDRH	R1, [R1, #0]
SXTH	R4, R1
ADDW	R1, R5, #10
LDRH	R1, [R1, #0]
SXTH	R3, R1
ADDW	R1, R5, #8
LDRH	R1, [R1, #0]
SXTH	R2, R1
ADDS	R1, R5, #6
; Aline end address is: 20 (R5)
LDRH	R1, [R1, #0]
SXTH	R1, R1
SXTH	R0, R1
SXTH	R1, R2
SXTH	R2, R3
SXTH	R3, R4
BL	_TFT_Line+0
;BPWasher_driver.c,3238 :: 		if (CurrentScreen == ALine->OwnerScreen && Aline->Visible != 0) {
L__DrawLine359:
L__DrawLine358:
;BPWasher_driver.c,3242 :: 		}
L_end_DrawLine:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _DrawLine
_DrawCheckBox:
;BPWasher_driver.c,3244 :: 		void DrawCheckBox(TCheckBox *ACheckBox) {
; ACheckBox start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; ACheckBox end address is: 0 (R0)
; ACheckBox start address is: 0 (R0)
;BPWasher_driver.c,3245 :: 		if (CurrentScreen == ACheckBox->OwnerScreen && ACheckBox->Visible != 0) {
LDR	R2, [R0, #0]
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
CMP	R1, R2
IT	NE
BNE	L__DrawCheckBox362
ADDW	R1, R0, #18
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__DrawCheckBox361
L__DrawCheckBox360:
;BPWasher_driver.c,3246 :: 		if (object_pressed == 1) {
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	NE
BNE	L_DrawCheckBox90
;BPWasher_driver.c,3247 :: 		object_pressed = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
STRB	R2, [R1, #0]
;BPWasher_driver.c,3248 :: 		TFT_Set_Brush(ACheckBox->Transparent, ACheckBox->Press_Color, ACheckBox->Gradient, ACheckBox->Gradient_Orientation, ACheckBox->Gradient_End_Color, ACheckBox->Gradient_Start_Color);
ADDW	R1, R0, #40
LDRH	R1, [R1, #0]
UXTH	R6, R1
ADDW	R1, R0, #42
LDRH	R1, [R1, #0]
UXTH	R5, R1
ADDW	R1, R0, #39
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #38
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDW	R1, R0, #50
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #22
LDRB	R1, [R1, #0]
STR	R0, [SP, #4]
UXTB	R0, R1
UXTH	R1, R2
UXTB	R2, R3
UXTB	R3, R4
PUSH	(R6)
PUSH	(R5)
BL	_TFT_Set_Brush+0
ADD	SP, SP, #8
LDR	R0, [SP, #4]
;BPWasher_driver.c,3249 :: 		}
IT	AL
BAL	L_DrawCheckBox91
L_DrawCheckBox90:
;BPWasher_driver.c,3251 :: 		TFT_Set_Brush(ACheckBox->Transparent, ACheckBox->Color, ACheckBox->Gradient, ACheckBox->Gradient_Orientation, ACheckBox->Gradient_Start_Color, ACheckBox->Gradient_End_Color);
ADDW	R1, R0, #42
LDRH	R1, [R1, #0]
UXTH	R6, R1
ADDW	R1, R0, #40
LDRH	R1, [R1, #0]
UXTH	R5, R1
ADDW	R1, R0, #39
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDW	R1, R0, #38
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDW	R1, R0, #44
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #22
LDRB	R1, [R1, #0]
STR	R0, [SP, #4]
UXTB	R0, R1
UXTH	R1, R2
UXTB	R2, R3
UXTB	R3, R4
PUSH	(R6)
PUSH	(R5)
BL	_TFT_Set_Brush+0
ADD	SP, SP, #8
LDR	R0, [SP, #4]
;BPWasher_driver.c,3252 :: 		}
L_DrawCheckBox91:
;BPWasher_driver.c,3253 :: 		TFT_Set_Pen(ACheckBox->Pen_Color, ACheckBox->Pen_Width);
ADDW	R1, R0, #14
LDRB	R1, [R1, #0]
UXTB	R2, R1
ADDW	R1, R0, #16
LDRH	R1, [R1, #0]
STR	R0, [SP, #4]
UXTH	R0, R1
UXTB	R1, R2
BL	_TFT_Set_Pen+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3254 :: 		TFT_Set_Font(ACheckBox->FontName, ACheckBox->Font_Color, FO_HORIZONTAL);
ADDW	R1, R0, #36
LDRH	R1, [R1, #0]
UXTH	R2, R1
ADDW	R1, R0, #32
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
MOVS	R2, #0
BL	_TFT_Set_Font+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3255 :: 		if (ACheckBox->TextAlign == _taLeft) {
ADDW	R1, R0, #28
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	NE
BNE	L_DrawCheckBox92
;BPWasher_driver.c,3256 :: 		if (ACheckBox->Rounded != 0)
ADDW	R1, R0, #46
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_DrawCheckBox93
;BPWasher_driver.c,3257 :: 		TFT_Rectangle_Round_Edges(ACheckBox->Left, ACheckBox->Top, ACheckBox->Left + ACheckBox->Height, ACheckBox->Top + ACheckBox->Height - 1, ACheckBox->Corner_Radius);
ADDW	R1, R0, #47
LDRB	R1, [R1, #0]
UXTB	R6, R1
ADDW	R1, R0, #8
LDRH	R5, [R1, #0]
ADDW	R1, R0, #12
LDRH	R2, [R1, #0]
ADDS	R1, R5, R2
UXTH	R1, R1
SUBS	R4, R1, #1
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
ADDS	R3, R1, R2
UXTH	R2, R5
STR	R0, [SP, #4]
UXTH	R0, R1
UXTH	R1, R2
UXTH	R2, R3
UXTH	R3, R4
PUSH	(R6)
BL	_TFT_Rectangle_Round_Edges+0
ADD	SP, SP, #4
LDR	R0, [SP, #4]
IT	AL
BAL	L_DrawCheckBox94
L_DrawCheckBox93:
;BPWasher_driver.c,3259 :: 		TFT_Rectangle(ACheckBox->Left, ACheckBox->Top, ACheckBox->Left + ACheckBox->Height, ACheckBox->Top + ACheckBox->Height - 1);
ADDW	R1, R0, #8
LDRH	R5, [R1, #0]
ADDW	R1, R0, #12
LDRH	R2, [R1, #0]
ADDS	R1, R5, R2
UXTH	R1, R1
SUBS	R4, R1, #1
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
ADDS	R3, R1, R2
SXTH	R2, R5
SXTH	R1, R1
STR	R0, [SP, #4]
SXTH	R0, R1
SXTH	R1, R2
SXTH	R2, R3
SXTH	R3, R4
BL	_TFT_Rectangle+0
LDR	R0, [SP, #4]
L_DrawCheckBox94:
;BPWasher_driver.c,3260 :: 		if (ACheckBox->Checked != 0) {
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_DrawCheckBox95
;BPWasher_driver.c,3261 :: 		TFT_Set_Pen(ACheckBox->Pen_Color, ACheckBox->Height / 8);
ADDW	R1, R0, #12
LDRH	R1, [R1, #0]
LSRS	R2, R1, #3
ADDW	R1, R0, #16
LDRH	R1, [R1, #0]
STR	R0, [SP, #4]
UXTH	R0, R1
UXTB	R1, R2
BL	_TFT_Set_Pen+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3265 :: 		ACheckBox->Top   + ACheckBox->Height - ACheckBox->Height / 5 - 1);
ADDW	R1, R0, #8
LDRH	R7, [R1, #0]
ADDW	R1, R0, #12
LDRH	R3, [R1, #0]
ADDS	R2, R7, R3
UXTH	R2, R2
MOVS	R1, #5
UDIV	R6, R3, R1
UXTH	R6, R6
SUB	R1, R2, R6
UXTH	R1, R1
SUBS	R5, R1, #1
;BPWasher_driver.c,3264 :: 		ACheckBox->Left  + ACheckBox->Height / 2 - 1,
ADDS	R1, R0, #6
LDRH	R4, [R1, #0]
LSRS	R2, R3, #1
UXTH	R2, R2
ADDS	R1, R4, R2
UXTH	R1, R1
SUBS	R3, R1, #1
;BPWasher_driver.c,3263 :: 		ACheckBox->Top   + ACheckBox->Height / 2 + 1,
ADDS	R1, R7, R2
UXTH	R1, R1
ADDS	R2, R1, #1
;BPWasher_driver.c,3262 :: 		TFT_Line(ACheckBox->Left  + ACheckBox->Height / 5 + 1,
ADDS	R1, R4, R6
UXTH	R1, R1
ADDS	R1, R1, #1
;BPWasher_driver.c,3265 :: 		ACheckBox->Top   + ACheckBox->Height - ACheckBox->Height / 5 - 1);
STR	R0, [SP, #4]
;BPWasher_driver.c,3262 :: 		TFT_Line(ACheckBox->Left  + ACheckBox->Height / 5 + 1,
SXTH	R0, R1
;BPWasher_driver.c,3263 :: 		ACheckBox->Top   + ACheckBox->Height / 2 + 1,
SXTH	R1, R2
;BPWasher_driver.c,3264 :: 		ACheckBox->Left  + ACheckBox->Height / 2 - 1,
SXTH	R2, R3
;BPWasher_driver.c,3265 :: 		ACheckBox->Top   + ACheckBox->Height - ACheckBox->Height / 5 - 1);
SXTH	R3, R5
BL	_TFT_Line+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3269 :: 		ACheckBox->Top   + ACheckBox->Height / 5 + 1);
ADDW	R1, R0, #8
LDRH	R7, [R1, #0]
ADDW	R1, R0, #12
LDRH	R6, [R1, #0]
MOVS	R1, #5
UDIV	R3, R6, R1
UXTH	R3, R3
ADDS	R1, R7, R3
UXTH	R1, R1
ADDS	R5, R1, #1
;BPWasher_driver.c,3268 :: 		ACheckBox->Left  + ACheckBox->Height - ACheckBox->Height / 5 - 1,
ADDS	R1, R0, #6
LDRH	R2, [R1, #0]
ADDS	R1, R2, R6
UXTH	R1, R1
SUB	R1, R1, R3
UXTH	R1, R1
SUBS	R4, R1, #1
;BPWasher_driver.c,3267 :: 		ACheckBox->Top   + ACheckBox->Height -  ACheckBox->Height / 5 - 1,
ADDS	R1, R7, R6
UXTH	R1, R1
SUB	R1, R1, R3
UXTH	R1, R1
SUBS	R3, R1, #1
;BPWasher_driver.c,3266 :: 		TFT_Line(ACheckBox->Left  + ACheckBox->Height / 2 - ACheckBox->Pen_Width + 1,
LSRS	R1, R6, #1
UXTH	R1, R1
ADDS	R2, R2, R1
UXTH	R2, R2
ADDW	R1, R0, #14
LDRB	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
ADDS	R1, R1, #1
;BPWasher_driver.c,3269 :: 		ACheckBox->Top   + ACheckBox->Height / 5 + 1);
STR	R0, [SP, #4]
;BPWasher_driver.c,3268 :: 		ACheckBox->Left  + ACheckBox->Height - ACheckBox->Height / 5 - 1,
SXTH	R2, R4
;BPWasher_driver.c,3266 :: 		TFT_Line(ACheckBox->Left  + ACheckBox->Height / 2 - ACheckBox->Pen_Width + 1,
SXTH	R0, R1
;BPWasher_driver.c,3267 :: 		ACheckBox->Top   + ACheckBox->Height -  ACheckBox->Height / 5 - 1,
SXTH	R1, R3
;BPWasher_driver.c,3269 :: 		ACheckBox->Top   + ACheckBox->Height / 5 + 1);
SXTH	R3, R5
BL	_TFT_Line+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3270 :: 		}
L_DrawCheckBox95:
;BPWasher_driver.c,3271 :: 		TFT_Write_Text_Return_Pos(ACheckBox->Caption, ACheckBox->Left + ACheckBox->Width / 4 + 3, ACheckBox->Top);
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
UXTH	R3, R1
ADDS	R1, R0, #6
LDRH	R2, [R1, #0]
ADDW	R1, R0, #10
LDRH	R1, [R1, #0]
LSRS	R1, R1, #2
UXTH	R1, R1
ADDS	R1, R2, R1
UXTH	R1, R1
ADDS	R2, R1, #3
ADDW	R1, R0, #24
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
UXTH	R2, R3
BL	_TFT_Write_Text_Return_Pos+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3272 :: 		TFT_Write_Text(ACheckBox->Caption, ACheckBox->Left + ACheckBox->Height + 3, (ACheckBox->Top + ((ACheckBox->Height - caption_height) / 2)));
ADDW	R1, R0, #8
LDRH	R3, [R1, #0]
ADDW	R1, R0, #12
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_caption_height+0)
MOVT	R1, #hi_addr(_caption_height+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
LSRS	R1, R1, #1
UXTH	R1, R1
ADDS	R3, R3, R1
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
ADDS	R1, R1, R2
UXTH	R1, R1
ADDS	R2, R1, #3
ADDW	R1, R0, #24
; ACheckBox end address is: 0 (R0)
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
UXTH	R2, R3
BL	_TFT_Write_Text+0
;BPWasher_driver.c,3273 :: 		}
IT	AL
BAL	L_DrawCheckBox96
L_DrawCheckBox92:
;BPWasher_driver.c,3274 :: 		else if (ACheckBox->TextAlign == _taRight) {
; ACheckBox start address is: 0 (R0)
ADDW	R1, R0, #28
LDRB	R1, [R1, #0]
CMP	R1, #2
IT	NE
BNE	L_DrawCheckBox97
;BPWasher_driver.c,3275 :: 		if (ACheckBox->Rounded != 0)
ADDW	R1, R0, #46
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_DrawCheckBox98
;BPWasher_driver.c,3276 :: 		TFT_Rectangle_Round_Edges(ACheckBox->Left + ACheckBox->Width - ACheckBox->Height  , ACheckBox->Top, ACheckBox->Left + ACheckBox->Width, ACheckBox->Top + ACheckBox->Height - 1, ACheckBox->Corner_Radius);
ADDW	R1, R0, #47
LDRB	R1, [R1, #0]
UXTB	R7, R1
ADDW	R1, R0, #8
LDRH	R6, [R1, #0]
ADDW	R1, R0, #12
LDRH	R5, [R1, #0]
ADDS	R1, R6, R5
UXTH	R1, R1
SUBS	R4, R1, #1
ADDS	R1, R0, #6
LDRH	R2, [R1, #0]
ADDW	R1, R0, #10
LDRH	R1, [R1, #0]
ADDS	R3, R2, R1
UXTH	R3, R3
UXTH	R2, R6
SUB	R1, R3, R5
STR	R0, [SP, #4]
UXTH	R0, R1
UXTH	R1, R2
UXTH	R2, R3
UXTH	R3, R4
PUSH	(R7)
BL	_TFT_Rectangle_Round_Edges+0
ADD	SP, SP, #4
LDR	R0, [SP, #4]
IT	AL
BAL	L_DrawCheckBox99
L_DrawCheckBox98:
;BPWasher_driver.c,3278 :: 		TFT_Rectangle(ACheckBox->Left + ACheckBox->Width - ACheckBox->Height  , ACheckBox->Top, ACheckBox->Left + ACheckBox->Width, ACheckBox->Top + ACheckBox->Height - 1);
ADDW	R1, R0, #8
LDRH	R6, [R1, #0]
ADDW	R1, R0, #12
LDRH	R5, [R1, #0]
ADDS	R1, R6, R5
UXTH	R1, R1
SUBS	R4, R1, #1
ADDS	R1, R0, #6
LDRH	R2, [R1, #0]
ADDW	R1, R0, #10
LDRH	R1, [R1, #0]
ADDS	R3, R2, R1
UXTH	R3, R3
SXTH	R2, R6
SUB	R1, R3, R5
STR	R0, [SP, #4]
SXTH	R0, R1
SXTH	R1, R2
SXTH	R2, R3
SXTH	R3, R4
BL	_TFT_Rectangle+0
LDR	R0, [SP, #4]
L_DrawCheckBox99:
;BPWasher_driver.c,3279 :: 		if (ACheckBox->Checked != 0) {
ADDW	R1, R0, #20
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_DrawCheckBox100
;BPWasher_driver.c,3280 :: 		TFT_Set_Pen(ACheckBox->Pen_Color, ACheckBox->Height / 8);
ADDW	R1, R0, #12
LDRH	R1, [R1, #0]
LSRS	R2, R1, #3
ADDW	R1, R0, #16
LDRH	R1, [R1, #0]
STR	R0, [SP, #4]
UXTH	R0, R1
UXTB	R1, R2
BL	_TFT_Set_Pen+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3284 :: 		ACheckBox->Top   + ACheckBox->Height - ACheckBox->Height / 5 - 1);
ADDW	R1, R0, #8
LDRH	R8, [R1, #0]
ADDW	R1, R0, #12
LDRH	R7, [R1, #0]
ADD	R2, R8, R7, LSL #0
UXTH	R2, R2
MOVS	R1, #5
UDIV	R6, R7, R1
UXTH	R6, R6
SUB	R1, R2, R6
UXTH	R1, R1
SUBS	R5, R1, #1
;BPWasher_driver.c,3283 :: 		ACheckBox->Left + ACheckBox->Width  - ACheckBox->Height /2 - 1,
ADDS	R1, R0, #6
LDRH	R2, [R1, #0]
ADDW	R1, R0, #10
LDRH	R1, [R1, #0]
ADDS	R4, R2, R1
UXTH	R4, R4
LSRS	R2, R7, #1
UXTH	R2, R2
SUB	R1, R4, R2
UXTH	R1, R1
SUBS	R3, R1, #1
;BPWasher_driver.c,3282 :: 		ACheckBox->Top +  ACheckBox->Height / 2 + 1,
ADD	R1, R8, R2, LSL #0
UXTH	R1, R1
ADDS	R2, R1, #1
;BPWasher_driver.c,3281 :: 		TFT_Line(ACheckBox->Left  + ACheckBox->Width - ACheckBox->Height + ACheckBox->Height / 5 + 1,
SUB	R1, R4, R7
UXTH	R1, R1
ADDS	R1, R1, R6
UXTH	R1, R1
ADDS	R1, R1, #1
;BPWasher_driver.c,3284 :: 		ACheckBox->Top   + ACheckBox->Height - ACheckBox->Height / 5 - 1);
STR	R0, [SP, #4]
;BPWasher_driver.c,3281 :: 		TFT_Line(ACheckBox->Left  + ACheckBox->Width - ACheckBox->Height + ACheckBox->Height / 5 + 1,
SXTH	R0, R1
;BPWasher_driver.c,3282 :: 		ACheckBox->Top +  ACheckBox->Height / 2 + 1,
SXTH	R1, R2
;BPWasher_driver.c,3283 :: 		ACheckBox->Left + ACheckBox->Width  - ACheckBox->Height /2 - 1,
SXTH	R2, R3
;BPWasher_driver.c,3284 :: 		ACheckBox->Top   + ACheckBox->Height - ACheckBox->Height / 5 - 1);
SXTH	R3, R5
BL	_TFT_Line+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3288 :: 		ACheckBox->Top   + ACheckBox->Height / 5 + 1);
ADDW	R1, R0, #8
LDRH	R8, [R1, #0]
ADDW	R1, R0, #12
LDRH	R7, [R1, #0]
MOVS	R1, #5
UDIV	R6, R7, R1
UXTH	R6, R6
ADD	R1, R8, R6, LSL #0
UXTH	R1, R1
ADDS	R5, R1, #1
;BPWasher_driver.c,3287 :: 		ACheckBox->Left + ACheckBox->Width  - ACheckBox->Height / 5 - 1,
ADDS	R1, R0, #6
LDRH	R2, [R1, #0]
ADDW	R1, R0, #10
LDRH	R1, [R1, #0]
ADDS	R4, R2, R1
UXTH	R4, R4
SUB	R1, R4, R6
UXTH	R1, R1
SUBS	R3, R1, #1
;BPWasher_driver.c,3286 :: 		ACheckBox->Top   + ACheckBox->Height -  ACheckBox->Height / 5 - 1,
ADD	R1, R8, R7, LSL #0
UXTH	R1, R1
SUB	R1, R1, R6
UXTH	R1, R1
SUBS	R2, R1, #1
;BPWasher_driver.c,3285 :: 		TFT_Line(ACheckBox->Left + ACheckBox->Width  - ACheckBox->Height /2 + 1,
LSRS	R1, R7, #1
UXTH	R1, R1
SUB	R1, R4, R1
UXTH	R1, R1
ADDS	R1, R1, #1
;BPWasher_driver.c,3288 :: 		ACheckBox->Top   + ACheckBox->Height / 5 + 1);
STR	R0, [SP, #4]
;BPWasher_driver.c,3285 :: 		TFT_Line(ACheckBox->Left + ACheckBox->Width  - ACheckBox->Height /2 + 1,
SXTH	R0, R1
;BPWasher_driver.c,3286 :: 		ACheckBox->Top   + ACheckBox->Height -  ACheckBox->Height / 5 - 1,
SXTH	R1, R2
;BPWasher_driver.c,3287 :: 		ACheckBox->Left + ACheckBox->Width  - ACheckBox->Height / 5 - 1,
SXTH	R2, R3
;BPWasher_driver.c,3288 :: 		ACheckBox->Top   + ACheckBox->Height / 5 + 1);
SXTH	R3, R5
BL	_TFT_Line+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3289 :: 		}
L_DrawCheckBox100:
;BPWasher_driver.c,3290 :: 		TFT_Write_Text_Return_Pos(ACheckBox->Caption, ACheckBox->Left + 3, ACheckBox->Top);
ADDW	R1, R0, #8
LDRH	R1, [R1, #0]
UXTH	R3, R1
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
ADDS	R2, R1, #3
ADDW	R1, R0, #24
LDR	R1, [R1, #0]
STR	R0, [SP, #4]
MOV	R0, R1
UXTH	R1, R2
UXTH	R2, R3
BL	_TFT_Write_Text_Return_Pos+0
LDR	R0, [SP, #4]
;BPWasher_driver.c,3291 :: 		TFT_Write_Text(ACheckBox->Caption, ACheckBox->Left + 3, ACheckBox->Top + (ACheckBox->Height - caption_height) / 2);
ADDW	R1, R0, #8
LDRH	R3, [R1, #0]
ADDW	R1, R0, #12
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_caption_height+0)
MOVT	R1, #hi_addr(_caption_height+0)
LDRH	R1, [R1, #0]
SUB	R1, R2, R1
UXTH	R1, R1
LSRS	R1, R1, #1
UXTH	R1, R1
ADDS	R3, R3, R1
ADDS	R1, R0, #6
LDRH	R1, [R1, #0]
ADDS	R2, R1, #3
ADDW	R1, R0, #24
; ACheckBox end address is: 0 (R0)
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
UXTH	R2, R3
BL	_TFT_Write_Text+0
;BPWasher_driver.c,3292 :: 		}
L_DrawCheckBox97:
L_DrawCheckBox96:
;BPWasher_driver.c,3245 :: 		if (CurrentScreen == ACheckBox->OwnerScreen && ACheckBox->Visible != 0) {
L__DrawCheckBox362:
L__DrawCheckBox361:
;BPWasher_driver.c,3294 :: 		}
L_end_DrawCheckBox:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _DrawCheckBox
_DrawScreen:
;BPWasher_driver.c,3296 :: 		void DrawScreen(TScreen *aScreen) {
; aScreen start address is: 0 (R0)
SUB	SP, SP, #128
STR	LR, [SP, #0]
; aScreen end address is: 0 (R0)
; aScreen start address is: 0 (R0)
;BPWasher_driver.c,3320 :: 		object_pressed = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_object_pressed+0)
MOVT	R1, #hi_addr(_object_pressed+0)
STRB	R2, [R1, #0]
;BPWasher_driver.c,3321 :: 		order = 0;
MOVS	R1, #0
STRH	R1, [SP, #4]
;BPWasher_driver.c,3322 :: 		button_idx = 0;
MOVS	R1, #0
STRB	R1, [SP, #6]
;BPWasher_driver.c,3323 :: 		round_button_idx = 0;
MOVS	R1, #0
STRB	R1, [SP, #12]
;BPWasher_driver.c,3324 :: 		round_cbutton_idx = 0;
MOVS	R1, #0
STRB	R1, [SP, #20]
;BPWasher_driver.c,3325 :: 		label_idx = 0;
MOVS	R1, #0
STRB	R1, [SP, #28]
;BPWasher_driver.c,3326 :: 		image_idx = 0;
MOVS	R1, #0
STRB	R1, [SP, #36]
;BPWasher_driver.c,3327 :: 		cimage_idx = 0;
MOVS	R1, #0
STRB	R1, [SP, #44]
;BPWasher_driver.c,3328 :: 		circle_idx = 0;
MOVS	R1, #0
STRB	R1, [SP, #52]
;BPWasher_driver.c,3329 :: 		box_idx = 0;
MOVS	R1, #0
STRB	R1, [SP, #60]
;BPWasher_driver.c,3330 :: 		line_idx = 0;
MOVS	R1, #0
STRB	R1, [SP, #68]
;BPWasher_driver.c,3331 :: 		checkbox_idx = 0;
MOVS	R1, #0
STRB	R1, [SP, #76]
;BPWasher_driver.c,3332 :: 		CurrentScreen = aScreen;
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
STR	R0, [R1, #0]
;BPWasher_driver.c,3334 :: 		if ((display_width != CurrentScreen->Width) || (display_height != CurrentScreen->Height)) {
ADDS	R1, R0, #2
; aScreen end address is: 0 (R0)
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_display_width+0)
MOVT	R1, #hi_addr(_display_width+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	NE
BNE	L__DrawScreen365
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, R1, #4
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_display_height+0)
MOVT	R1, #hi_addr(_display_height+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	NE
BNE	L__DrawScreen364
IT	AL
BAL	L_DrawScreen103
L__DrawScreen365:
L__DrawScreen364:
;BPWasher_driver.c,3335 :: 		save_bled = TFT_BLED;
MOVW	R2, #lo_addr(GPIOE_ODR+0)
MOVT	R2, #hi_addr(GPIOE_ODR+0)
STR	R2, [SP, #124]
LDR	R1, [R2, #0]
STRB	R1, [SP, #84]
;BPWasher_driver.c,3336 :: 		TFT_BLED           = 0;
MOVS	R1, #0
SXTB	R1, R1
STR	R1, [R2, #0]
;BPWasher_driver.c,3337 :: 		TFT_Init_ILI9341_8bit(CurrentScreen->Width, CurrentScreen->Height);
MOVW	R3, #lo_addr(_CurrentScreen+0)
MOVT	R3, #hi_addr(_CurrentScreen+0)
STR	R3, [SP, #120]
LDR	R1, [R3, #0]
ADDS	R1, R1, #4
LDRH	R1, [R1, #0]
UXTH	R2, R1
MOV	R1, R3
LDR	R1, [R1, #0]
ADDS	R1, R1, #2
LDRH	R1, [R1, #0]
UXTH	R0, R1
UXTH	R1, R2
BL	_TFT_Init_ILI9341_8bit+0
;BPWasher_driver.c,3338 :: 		TP_TFT_Init(CurrentScreen->Width, CurrentScreen->Height, 8, 9);                                  // Initialize touch panel
MOVW	R3, #lo_addr(_CurrentScreen+0)
MOVT	R3, #hi_addr(_CurrentScreen+0)
LDR	R1, [R3, #0]
ADDS	R1, R1, #4
LDRH	R1, [R1, #0]
UXTH	R2, R1
MOV	R1, R3
LDR	R1, [R1, #0]
ADDS	R1, R1, #2
LDRH	R1, [R1, #0]
MOVS	R3, #9
UXTH	R0, R1
UXTH	R1, R2
MOVS	R2, #8
BL	_TP_TFT_Init+0
;BPWasher_driver.c,3339 :: 		TP_TFT_Set_ADC_Threshold(ADC_THRESHOLD);                              // Set touch panel ADC threshold
MOVW	R0, #1500
SXTH	R0, R0
BL	_TP_TFT_Set_ADC_Threshold+0
;BPWasher_driver.c,3340 :: 		TFT_Fill_Screen(CurrentScreen->Color);
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
LDRH	R1, [R1, #0]
UXTH	R0, R1
BL	_TFT_Fill_Screen+0
;BPWasher_driver.c,3341 :: 		display_width = CurrentScreen->Width;
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, R1, #2
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_display_width+0)
MOVT	R1, #hi_addr(_display_width+0)
STRH	R2, [R1, #0]
;BPWasher_driver.c,3342 :: 		display_height = CurrentScreen->Height;
LDR	R1, [SP, #120]
LDR	R1, [R1, #0]
ADDS	R1, R1, #4
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_display_height+0)
MOVT	R1, #hi_addr(_display_height+0)
STRH	R2, [R1, #0]
;BPWasher_driver.c,3343 :: 		TFT_BLED           = save_bled;
LDRB	R2, [SP, #84]
LDR	R1, [SP, #124]
STR	R2, [R1, #0]
;BPWasher_driver.c,3344 :: 		}
IT	AL
BAL	L_DrawScreen104
L_DrawScreen103:
;BPWasher_driver.c,3346 :: 		TFT_Fill_Screen(CurrentScreen->Color);
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
LDRH	R1, [R1, #0]
UXTH	R0, R1
BL	_TFT_Fill_Screen+0
L_DrawScreen104:
;BPWasher_driver.c,3349 :: 		while (order < CurrentScreen->ObjectsCount) {
L_DrawScreen105:
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, R1, #6
LDRH	R2, [R1, #0]
LDRH	R1, [SP, #4]
CMP	R1, R2
IT	CS
BCS	L_DrawScreen106
;BPWasher_driver.c,3350 :: 		if (button_idx < CurrentScreen->ButtonsCount) {
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #8
LDRH	R2, [R1, #0]
LDRB	R1, [SP, #6]
CMP	R1, R2
IT	CS
BCS	L_DrawScreen107
;BPWasher_driver.c,3351 :: 		local_button = GetButton(button_idx);
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #12
LDR	R2, [R1, #0]
LDRB	R1, [SP, #6]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
LDR	R1, [R1, #0]
STR	R1, [SP, #8]
;BPWasher_driver.c,3352 :: 		if (order == local_button->Order) {
ADDS	R1, R1, #4
LDRB	R2, [R1, #0]
LDRH	R1, [SP, #4]
CMP	R1, R2
IT	NE
BNE	L_DrawScreen108
;BPWasher_driver.c,3353 :: 		button_idx++;
LDRB	R1, [SP, #6]
ADDS	R1, R1, #1
STRB	R1, [SP, #6]
;BPWasher_driver.c,3354 :: 		order++;
LDRH	R1, [SP, #4]
ADDS	R1, R1, #1
STRH	R1, [SP, #4]
;BPWasher_driver.c,3355 :: 		DrawButton(local_button);
LDR	R0, [SP, #8]
BL	_DrawButton+0
;BPWasher_driver.c,3356 :: 		}
L_DrawScreen108:
;BPWasher_driver.c,3357 :: 		}
L_DrawScreen107:
;BPWasher_driver.c,3359 :: 		if (round_button_idx < CurrentScreen->Buttons_RoundCount) {
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #16
LDRH	R2, [R1, #0]
LDRB	R1, [SP, #12]
CMP	R1, R2
IT	CS
BCS	L_DrawScreen109
;BPWasher_driver.c,3360 :: 		local_round_button = GetRoundButton(round_button_idx);
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #20
LDR	R2, [R1, #0]
LDRB	R1, [SP, #12]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
LDR	R1, [R1, #0]
STR	R1, [SP, #16]
;BPWasher_driver.c,3361 :: 		if (order == local_round_button->Order) {
ADDS	R1, R1, #4
LDRB	R2, [R1, #0]
LDRH	R1, [SP, #4]
CMP	R1, R2
IT	NE
BNE	L_DrawScreen110
;BPWasher_driver.c,3362 :: 		round_button_idx++;
LDRB	R1, [SP, #12]
ADDS	R1, R1, #1
STRB	R1, [SP, #12]
;BPWasher_driver.c,3363 :: 		order++;
LDRH	R1, [SP, #4]
ADDS	R1, R1, #1
STRH	R1, [SP, #4]
;BPWasher_driver.c,3364 :: 		DrawRoundButton(local_round_button);
LDR	R0, [SP, #16]
BL	_DrawRoundButton+0
;BPWasher_driver.c,3365 :: 		}
L_DrawScreen110:
;BPWasher_driver.c,3366 :: 		}
L_DrawScreen109:
;BPWasher_driver.c,3368 :: 		if (round_cbutton_idx < CurrentScreen->CButtons_RoundCount) {
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #24
LDRH	R2, [R1, #0]
LDRB	R1, [SP, #20]
CMP	R1, R2
IT	CS
BCS	L_DrawScreen111
;BPWasher_driver.c,3369 :: 		local_round_cbutton = GetCRoundButton(round_cbutton_idx);
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #28
LDR	R2, [R1, #0]
LDRB	R1, [SP, #20]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
LDR	R1, [R1, #0]
STR	R1, [SP, #24]
;BPWasher_driver.c,3370 :: 		if (order == local_round_cbutton->Order) {
ADDS	R1, R1, #4
LDRB	R2, [R1, #0]
LDRH	R1, [SP, #4]
CMP	R1, R2
IT	NE
BNE	L_DrawScreen112
;BPWasher_driver.c,3371 :: 		round_cbutton_idx++;
LDRB	R1, [SP, #20]
ADDS	R1, R1, #1
STRB	R1, [SP, #20]
;BPWasher_driver.c,3372 :: 		order++;
LDRH	R1, [SP, #4]
ADDS	R1, R1, #1
STRH	R1, [SP, #4]
;BPWasher_driver.c,3373 :: 		DrawCRoundButton(local_round_cbutton);
LDR	R0, [SP, #24]
BL	_DrawCRoundButton+0
;BPWasher_driver.c,3374 :: 		}
L_DrawScreen112:
;BPWasher_driver.c,3375 :: 		}
L_DrawScreen111:
;BPWasher_driver.c,3377 :: 		if (label_idx < CurrentScreen->LabelsCount) {
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #32
LDRH	R2, [R1, #0]
LDRB	R1, [SP, #28]
CMP	R1, R2
IT	CS
BCS	L_DrawScreen113
;BPWasher_driver.c,3378 :: 		local_label = GetLabel(label_idx);
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #36
LDR	R2, [R1, #0]
LDRB	R1, [SP, #28]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
LDR	R1, [R1, #0]
STR	R1, [SP, #32]
;BPWasher_driver.c,3379 :: 		if (order == local_label->Order) {
ADDS	R1, R1, #4
LDRB	R2, [R1, #0]
LDRH	R1, [SP, #4]
CMP	R1, R2
IT	NE
BNE	L_DrawScreen114
;BPWasher_driver.c,3380 :: 		label_idx++;
LDRB	R1, [SP, #28]
ADDS	R1, R1, #1
STRB	R1, [SP, #28]
;BPWasher_driver.c,3381 :: 		order++;
LDRH	R1, [SP, #4]
ADDS	R1, R1, #1
STRH	R1, [SP, #4]
;BPWasher_driver.c,3382 :: 		DrawLabel(local_label);
LDR	R0, [SP, #32]
BL	_DrawLabel+0
;BPWasher_driver.c,3383 :: 		}
L_DrawScreen114:
;BPWasher_driver.c,3384 :: 		}
L_DrawScreen113:
;BPWasher_driver.c,3386 :: 		if (circle_idx < CurrentScreen->CirclesCount) {
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #56
LDRH	R2, [R1, #0]
LDRB	R1, [SP, #52]
CMP	R1, R2
IT	CS
BCS	L_DrawScreen115
;BPWasher_driver.c,3387 :: 		local_circle = GetCircle(circle_idx);
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #60
LDR	R2, [R1, #0]
LDRB	R1, [SP, #52]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
LDR	R1, [R1, #0]
STR	R1, [SP, #56]
;BPWasher_driver.c,3388 :: 		if (order == local_circle->Order) {
ADDS	R1, R1, #4
LDRB	R2, [R1, #0]
LDRH	R1, [SP, #4]
CMP	R1, R2
IT	NE
BNE	L_DrawScreen116
;BPWasher_driver.c,3389 :: 		circle_idx++;
LDRB	R1, [SP, #52]
ADDS	R1, R1, #1
STRB	R1, [SP, #52]
;BPWasher_driver.c,3390 :: 		order++;
LDRH	R1, [SP, #4]
ADDS	R1, R1, #1
STRH	R1, [SP, #4]
;BPWasher_driver.c,3391 :: 		DrawCircle(local_circle);
LDR	R0, [SP, #56]
BL	_DrawCircle+0
;BPWasher_driver.c,3392 :: 		}
L_DrawScreen116:
;BPWasher_driver.c,3393 :: 		}
L_DrawScreen115:
;BPWasher_driver.c,3395 :: 		if (box_idx < CurrentScreen->BoxesCount) {
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #64
LDRH	R2, [R1, #0]
LDRB	R1, [SP, #60]
CMP	R1, R2
IT	CS
BCS	L_DrawScreen117
;BPWasher_driver.c,3396 :: 		local_box = GetBox(box_idx);
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #68
LDR	R2, [R1, #0]
LDRB	R1, [SP, #60]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
LDR	R1, [R1, #0]
STR	R1, [SP, #64]
;BPWasher_driver.c,3397 :: 		if (order == local_box->Order) {
ADDS	R1, R1, #4
LDRB	R2, [R1, #0]
LDRH	R1, [SP, #4]
CMP	R1, R2
IT	NE
BNE	L_DrawScreen118
;BPWasher_driver.c,3398 :: 		box_idx++;
LDRB	R1, [SP, #60]
ADDS	R1, R1, #1
STRB	R1, [SP, #60]
;BPWasher_driver.c,3399 :: 		order++;
LDRH	R1, [SP, #4]
ADDS	R1, R1, #1
STRH	R1, [SP, #4]
;BPWasher_driver.c,3400 :: 		DrawBox(local_box);
LDR	R0, [SP, #64]
BL	_DrawBox+0
;BPWasher_driver.c,3401 :: 		}
L_DrawScreen118:
;BPWasher_driver.c,3402 :: 		}
L_DrawScreen117:
;BPWasher_driver.c,3404 :: 		if (line_idx < CurrentScreen->LinesCount) {
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #72
LDRH	R2, [R1, #0]
LDRB	R1, [SP, #68]
CMP	R1, R2
IT	CS
BCS	L_DrawScreen119
;BPWasher_driver.c,3405 :: 		local_line = GetLine(line_idx);
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #76
LDR	R2, [R1, #0]
LDRB	R1, [SP, #68]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
LDR	R1, [R1, #0]
STR	R1, [SP, #72]
;BPWasher_driver.c,3406 :: 		if (order == local_line->Order) {
ADDS	R1, R1, #4
LDRB	R2, [R1, #0]
LDRH	R1, [SP, #4]
CMP	R1, R2
IT	NE
BNE	L_DrawScreen120
;BPWasher_driver.c,3407 :: 		line_idx++;
LDRB	R1, [SP, #68]
ADDS	R1, R1, #1
STRB	R1, [SP, #68]
;BPWasher_driver.c,3408 :: 		order++;
LDRH	R1, [SP, #4]
ADDS	R1, R1, #1
STRH	R1, [SP, #4]
;BPWasher_driver.c,3409 :: 		DrawLine(local_line);
LDR	R0, [SP, #72]
BL	_DrawLine+0
;BPWasher_driver.c,3410 :: 		}
L_DrawScreen120:
;BPWasher_driver.c,3411 :: 		}
L_DrawScreen119:
;BPWasher_driver.c,3413 :: 		if (image_idx < CurrentScreen->ImagesCount) {
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #40
LDRH	R2, [R1, #0]
LDRB	R1, [SP, #36]
CMP	R1, R2
IT	CS
BCS	L_DrawScreen121
;BPWasher_driver.c,3414 :: 		local_image = GetImage(image_idx);
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #44
LDR	R2, [R1, #0]
LDRB	R1, [SP, #36]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
LDR	R1, [R1, #0]
STR	R1, [SP, #40]
;BPWasher_driver.c,3415 :: 		if (order == local_image->Order) {
ADDS	R1, R1, #4
LDRB	R2, [R1, #0]
LDRH	R1, [SP, #4]
CMP	R1, R2
IT	NE
BNE	L_DrawScreen122
;BPWasher_driver.c,3416 :: 		image_idx++;
LDRB	R1, [SP, #36]
ADDS	R1, R1, #1
STRB	R1, [SP, #36]
;BPWasher_driver.c,3417 :: 		order++;
LDRH	R1, [SP, #4]
ADDS	R1, R1, #1
STRH	R1, [SP, #4]
;BPWasher_driver.c,3418 :: 		DrawImage(local_image);
LDR	R0, [SP, #40]
BL	_DrawImage+0
;BPWasher_driver.c,3419 :: 		}
L_DrawScreen122:
;BPWasher_driver.c,3420 :: 		}
L_DrawScreen121:
;BPWasher_driver.c,3422 :: 		if (cimage_idx < CurrentScreen->CImagesCount) {
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #48
LDRH	R2, [R1, #0]
LDRB	R1, [SP, #44]
CMP	R1, R2
IT	CS
BCS	L_DrawScreen123
;BPWasher_driver.c,3423 :: 		local_cimage = GetCImage(cimage_idx);
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #52
LDR	R2, [R1, #0]
LDRB	R1, [SP, #44]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
LDR	R1, [R1, #0]
STR	R1, [SP, #48]
;BPWasher_driver.c,3424 :: 		if (order == local_cimage->Order) {
ADDS	R1, R1, #4
LDRB	R2, [R1, #0]
LDRH	R1, [SP, #4]
CMP	R1, R2
IT	NE
BNE	L_DrawScreen124
;BPWasher_driver.c,3425 :: 		cimage_idx++;
LDRB	R1, [SP, #44]
ADDS	R1, R1, #1
STRB	R1, [SP, #44]
;BPWasher_driver.c,3426 :: 		order++;
LDRH	R1, [SP, #4]
ADDS	R1, R1, #1
STRH	R1, [SP, #4]
;BPWasher_driver.c,3427 :: 		DrawCImage(local_cimage);
LDR	R0, [SP, #48]
BL	_DrawCImage+0
;BPWasher_driver.c,3428 :: 		}
L_DrawScreen124:
;BPWasher_driver.c,3429 :: 		}
L_DrawScreen123:
;BPWasher_driver.c,3431 :: 		if (checkbox_idx < CurrentScreen->CheckBoxesCount) {
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #80
LDRH	R2, [R1, #0]
LDRB	R1, [SP, #76]
CMP	R1, R2
IT	CS
BCS	L_DrawScreen125
;BPWasher_driver.c,3432 :: 		local_checkbox = GetCheckBox(checkbox_idx);
MOVW	R1, #lo_addr(_CurrentScreen+0)
MOVT	R1, #hi_addr(_CurrentScreen+0)
LDR	R1, [R1, #0]
ADDS	R1, #84
LDR	R2, [R1, #0]
LDRB	R1, [SP, #76]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
LDR	R1, [R1, #0]
STR	R1, [SP, #80]
;BPWasher_driver.c,3433 :: 		if (order == local_checkbox->Order) {
ADDS	R1, R1, #4
LDRB	R2, [R1, #0]
LDRH	R1, [SP, #4]
CMP	R1, R2
IT	NE
BNE	L_DrawScreen126
;BPWasher_driver.c,3434 :: 		checkbox_idx++;
LDRB	R1, [SP, #76]
ADDS	R1, R1, #1
STRB	R1, [SP, #76]
;BPWasher_driver.c,3435 :: 		order++;
LDRH	R1, [SP, #4]
ADDS	R1, R1, #1
STRH	R1, [SP, #4]
;BPWasher_driver.c,3436 :: 		DrawCheckBox(local_checkbox);
LDR	R0, [SP, #80]
BL	_DrawCheckBox+0
;BPWasher_driver.c,3437 :: 		}
L_DrawScreen126:
;BPWasher_driver.c,3438 :: 		}
L_DrawScreen125:
;BPWasher_driver.c,3440 :: 		}
IT	AL
BAL	L_DrawScreen105
L_DrawScreen106:
;BPWasher_driver.c,3441 :: 		}
L_end_DrawScreen:
LDR	LR, [SP, #0]
ADD	SP, SP, #128
BX	LR
; end of _DrawScreen
_Get_Object:
;BPWasher_driver.c,3443 :: 		void Get_Object(unsigned int X, unsigned int Y) {
; Y start address is: 4 (R1)
; X start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; Y end address is: 4 (R1)
; X end address is: 0 (R0)
; X start address is: 0 (R0)
; Y start address is: 4 (R1)
;BPWasher_driver.c,3444 :: 		button_order        = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_button_order+0)
MOVT	R2, #hi_addr(_button_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3445 :: 		round_button_order  = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_round_button_order+0)
MOVT	R2, #hi_addr(_round_button_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3446 :: 		round_cbutton_order = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_round_cbutton_order+0)
MOVT	R2, #hi_addr(_round_cbutton_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3447 :: 		label_order         = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_label_order+0)
MOVT	R2, #hi_addr(_label_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3448 :: 		image_order         = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_image_order+0)
MOVT	R2, #hi_addr(_image_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3449 :: 		cimage_order        = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_cimage_order+0)
MOVT	R2, #hi_addr(_cimage_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3450 :: 		circle_order        = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_circle_order+0)
MOVT	R2, #hi_addr(_circle_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3451 :: 		box_order           = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_box_order+0)
MOVT	R2, #hi_addr(_box_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3452 :: 		checkbox_order      = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_checkbox_order+0)
MOVT	R2, #hi_addr(_checkbox_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3454 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->ButtonsCount ; _object_count++ ) {
MOVS	R3, #0
SXTH	R3, R3
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
; X end address is: 0 (R0)
; Y end address is: 4 (R1)
UXTH	R8, R0
UXTH	R7, R1
L_Get_Object127:
; Y start address is: 28 (R7)
; X start address is: 32 (R8)
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #8
LDRH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	CS
BCS	L_Get_Object128
;BPWasher_driver.c,3455 :: 		local_button = GetButton(_object_count);
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #12
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
LSLS	R2, R2, #2
ADDS	R2, R3, R2
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_local_button+0)
MOVT	R2, #hi_addr(_local_button+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3456 :: 		if (local_button->Active != 0) {
ADDW	R2, R3, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Get_Object130
;BPWasher_driver.c,3458 :: 		local_button->Width, local_button->Height) == 1) {
MOVW	R6, #lo_addr(_local_button+0)
MOVT	R6, #hi_addr(_local_button+0)
LDR	R2, [R6, #0]
ADDS	R2, #12
LDRH	R2, [R2, #0]
UXTH	R5, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #10
LDRH	R2, [R2, #0]
UXTH	R4, R2
;BPWasher_driver.c,3457 :: 		if (IsInsideObject(X, Y, local_button->Left, local_button->Top,
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #8
LDRH	R2, [R2, #0]
UXTH	R3, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, R2, #6
LDRH	R2, [R2, #0]
UXTH	R1, R7
UXTH	R0, R8
;BPWasher_driver.c,3458 :: 		local_button->Width, local_button->Height) == 1) {
PUSH	(R5)
PUSH	(R4)
BL	BPWasher_driver_IsInsideObject+0
ADD	SP, SP, #8
CMP	R0, #1
IT	NE
BNE	L_Get_Object131
;BPWasher_driver.c,3459 :: 		button_order = local_button->Order;
MOVW	R4, #lo_addr(_local_button+0)
MOVT	R4, #hi_addr(_local_button+0)
LDR	R2, [R4, #0]
ADDS	R2, R2, #4
LDRB	R3, [R2, #0]
MOVW	R2, #lo_addr(_button_order+0)
MOVT	R2, #hi_addr(_button_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3460 :: 		exec_button = local_button;
MOV	R2, R4
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_exec_button+0)
MOVT	R2, #hi_addr(_exec_button+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3461 :: 		}
L_Get_Object131:
;BPWasher_driver.c,3462 :: 		}
L_Get_Object130:
;BPWasher_driver.c,3454 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->ButtonsCount ; _object_count++ ) {
MOVW	R3, #lo_addr(__object_count+0)
MOVT	R3, #hi_addr(__object_count+0)
LDRSH	R2, [R3, #0]
ADDS	R2, R2, #1
STRH	R2, [R3, #0]
;BPWasher_driver.c,3463 :: 		}
IT	AL
BAL	L_Get_Object127
L_Get_Object128:
;BPWasher_driver.c,3466 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->Buttons_RoundCount ; _object_count++ ) {
MOVS	R3, #0
SXTH	R3, R3
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
; Y end address is: 28 (R7)
; X end address is: 32 (R8)
STRH	R8, [SP, #4]
UXTH	R8, R7
LDRH	R7, [SP, #4]
L_Get_Object132:
; X start address is: 28 (R7)
; Y start address is: 32 (R8)
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #16
LDRH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	CS
BCS	L_Get_Object133
;BPWasher_driver.c,3467 :: 		local_round_button = GetRoundButton(_object_count);
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #20
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
LSLS	R2, R2, #2
ADDS	R2, R3, R2
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_local_round_button+0)
MOVT	R2, #hi_addr(_local_round_button+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3468 :: 		if (local_round_button->Active != 0) {
ADDW	R2, R3, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Get_Object135
;BPWasher_driver.c,3470 :: 		local_round_button->Width, local_round_button->Height) == 1) {
MOVW	R6, #lo_addr(_local_round_button+0)
MOVT	R6, #hi_addr(_local_round_button+0)
LDR	R2, [R6, #0]
ADDS	R2, #12
LDRH	R2, [R2, #0]
UXTH	R5, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #10
LDRH	R2, [R2, #0]
UXTH	R4, R2
;BPWasher_driver.c,3469 :: 		if (IsInsideObject(X, Y, local_round_button->Left, local_round_button->Top,
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #8
LDRH	R2, [R2, #0]
UXTH	R3, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, R2, #6
LDRH	R2, [R2, #0]
UXTH	R1, R8
UXTH	R0, R7
;BPWasher_driver.c,3470 :: 		local_round_button->Width, local_round_button->Height) == 1) {
PUSH	(R5)
PUSH	(R4)
BL	BPWasher_driver_IsInsideObject+0
ADD	SP, SP, #8
CMP	R0, #1
IT	NE
BNE	L_Get_Object136
;BPWasher_driver.c,3471 :: 		round_button_order = local_round_button->Order;
MOVW	R4, #lo_addr(_local_round_button+0)
MOVT	R4, #hi_addr(_local_round_button+0)
LDR	R2, [R4, #0]
ADDS	R2, R2, #4
LDRB	R3, [R2, #0]
MOVW	R2, #lo_addr(_round_button_order+0)
MOVT	R2, #hi_addr(_round_button_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3472 :: 		exec_round_button = local_round_button;
MOV	R2, R4
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_exec_round_button+0)
MOVT	R2, #hi_addr(_exec_round_button+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3473 :: 		}
L_Get_Object136:
;BPWasher_driver.c,3474 :: 		}
L_Get_Object135:
;BPWasher_driver.c,3466 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->Buttons_RoundCount ; _object_count++ ) {
MOVW	R3, #lo_addr(__object_count+0)
MOVT	R3, #hi_addr(__object_count+0)
LDRSH	R2, [R3, #0]
ADDS	R2, R2, #1
STRH	R2, [R3, #0]
;BPWasher_driver.c,3475 :: 		}
IT	AL
BAL	L_Get_Object132
L_Get_Object133:
;BPWasher_driver.c,3478 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->CButtons_RoundCount ; _object_count++ ) {
MOVS	R3, #0
SXTH	R3, R3
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
; X end address is: 28 (R7)
; Y end address is: 32 (R8)
STRH	R8, [SP, #4]
UXTH	R8, R7
LDRH	R7, [SP, #4]
L_Get_Object137:
; Y start address is: 28 (R7)
; X start address is: 32 (R8)
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #24
LDRH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	CS
BCS	L_Get_Object138
;BPWasher_driver.c,3479 :: 		local_round_cbutton = GetCRoundButton(_object_count);
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #28
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
LSLS	R2, R2, #2
ADDS	R2, R3, R2
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_local_round_cbutton+0)
MOVT	R2, #hi_addr(_local_round_cbutton+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3480 :: 		if (local_round_cbutton->Active != 0) {
ADDW	R2, R3, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Get_Object140
;BPWasher_driver.c,3482 :: 		local_round_cbutton->Width, local_round_cbutton->Height) == 1) {
MOVW	R6, #lo_addr(_local_round_cbutton+0)
MOVT	R6, #hi_addr(_local_round_cbutton+0)
LDR	R2, [R6, #0]
ADDS	R2, #12
LDRH	R2, [R2, #0]
UXTH	R5, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #10
LDRH	R2, [R2, #0]
UXTH	R4, R2
;BPWasher_driver.c,3481 :: 		if (IsInsideObject(X, Y, local_round_cbutton->Left, local_round_cbutton->Top,
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #8
LDRH	R2, [R2, #0]
UXTH	R3, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, R2, #6
LDRH	R2, [R2, #0]
UXTH	R1, R7
UXTH	R0, R8
;BPWasher_driver.c,3482 :: 		local_round_cbutton->Width, local_round_cbutton->Height) == 1) {
PUSH	(R5)
PUSH	(R4)
BL	BPWasher_driver_IsInsideObject+0
ADD	SP, SP, #8
CMP	R0, #1
IT	NE
BNE	L_Get_Object141
;BPWasher_driver.c,3483 :: 		round_cbutton_order = local_round_cbutton->Order;
MOVW	R4, #lo_addr(_local_round_cbutton+0)
MOVT	R4, #hi_addr(_local_round_cbutton+0)
LDR	R2, [R4, #0]
ADDS	R2, R2, #4
LDRB	R3, [R2, #0]
MOVW	R2, #lo_addr(_round_cbutton_order+0)
MOVT	R2, #hi_addr(_round_cbutton_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3484 :: 		exec_round_cbutton = local_round_cbutton;
MOV	R2, R4
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_exec_round_cbutton+0)
MOVT	R2, #hi_addr(_exec_round_cbutton+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3485 :: 		}
L_Get_Object141:
;BPWasher_driver.c,3486 :: 		}
L_Get_Object140:
;BPWasher_driver.c,3478 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->CButtons_RoundCount ; _object_count++ ) {
MOVW	R3, #lo_addr(__object_count+0)
MOVT	R3, #hi_addr(__object_count+0)
LDRSH	R2, [R3, #0]
ADDS	R2, R2, #1
STRH	R2, [R3, #0]
;BPWasher_driver.c,3487 :: 		}
IT	AL
BAL	L_Get_Object137
L_Get_Object138:
;BPWasher_driver.c,3490 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->LabelsCount ; _object_count++ ) {
MOVS	R3, #0
SXTH	R3, R3
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
; Y end address is: 28 (R7)
; X end address is: 32 (R8)
STRH	R8, [SP, #4]
UXTH	R8, R7
LDRH	R7, [SP, #4]
L_Get_Object142:
; X start address is: 28 (R7)
; Y start address is: 32 (R8)
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #32
LDRH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	CS
BCS	L_Get_Object143
;BPWasher_driver.c,3491 :: 		local_label = GetLabel(_object_count);
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #36
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
LSLS	R2, R2, #2
ADDS	R2, R3, R2
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_local_label+0)
MOVT	R2, #hi_addr(_local_label+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3492 :: 		if (local_label->Active != 0) {
ADDW	R2, R3, #28
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Get_Object145
;BPWasher_driver.c,3494 :: 		local_label->Width, local_label->Height) == 1) {
MOVW	R6, #lo_addr(_local_label+0)
MOVT	R6, #hi_addr(_local_label+0)
LDR	R2, [R6, #0]
ADDS	R2, #12
LDRH	R2, [R2, #0]
UXTH	R5, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #10
LDRH	R2, [R2, #0]
UXTH	R4, R2
;BPWasher_driver.c,3493 :: 		if (IsInsideObject(X, Y, local_label->Left, local_label->Top,
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #8
LDRH	R2, [R2, #0]
UXTH	R3, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, R2, #6
LDRH	R2, [R2, #0]
UXTH	R1, R8
UXTH	R0, R7
;BPWasher_driver.c,3494 :: 		local_label->Width, local_label->Height) == 1) {
PUSH	(R5)
PUSH	(R4)
BL	BPWasher_driver_IsInsideObject+0
ADD	SP, SP, #8
CMP	R0, #1
IT	NE
BNE	L_Get_Object146
;BPWasher_driver.c,3495 :: 		label_order = local_label->Order;
MOVW	R4, #lo_addr(_local_label+0)
MOVT	R4, #hi_addr(_local_label+0)
LDR	R2, [R4, #0]
ADDS	R2, R2, #4
LDRB	R3, [R2, #0]
MOVW	R2, #lo_addr(_label_order+0)
MOVT	R2, #hi_addr(_label_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3496 :: 		exec_label = local_label;
MOV	R2, R4
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_exec_label+0)
MOVT	R2, #hi_addr(_exec_label+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3497 :: 		}
L_Get_Object146:
;BPWasher_driver.c,3498 :: 		}
L_Get_Object145:
;BPWasher_driver.c,3490 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->LabelsCount ; _object_count++ ) {
MOVW	R3, #lo_addr(__object_count+0)
MOVT	R3, #hi_addr(__object_count+0)
LDRSH	R2, [R3, #0]
ADDS	R2, R2, #1
STRH	R2, [R3, #0]
;BPWasher_driver.c,3499 :: 		}
IT	AL
BAL	L_Get_Object142
L_Get_Object143:
;BPWasher_driver.c,3502 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->ImagesCount ; _object_count++ ) {
MOVS	R3, #0
SXTH	R3, R3
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
; X end address is: 28 (R7)
; Y end address is: 32 (R8)
STRH	R8, [SP, #4]
UXTH	R8, R7
LDRH	R7, [SP, #4]
L_Get_Object147:
; Y start address is: 28 (R7)
; X start address is: 32 (R8)
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #40
LDRH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	CS
BCS	L_Get_Object148
;BPWasher_driver.c,3503 :: 		local_image = GetImage(_object_count);
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #44
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
LSLS	R2, R2, #2
ADDS	R2, R3, R2
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_local_image+0)
MOVT	R2, #hi_addr(_local_image+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3504 :: 		if (local_image->Active != 0) {
ADDW	R2, R3, #21
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Get_Object150
;BPWasher_driver.c,3506 :: 		local_image->Width, local_image->Height) == 1) {
MOVW	R6, #lo_addr(_local_image+0)
MOVT	R6, #hi_addr(_local_image+0)
LDR	R2, [R6, #0]
ADDS	R2, #12
LDRH	R2, [R2, #0]
UXTH	R5, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #10
LDRH	R2, [R2, #0]
UXTH	R4, R2
;BPWasher_driver.c,3505 :: 		if (IsInsideObject(X, Y, local_image->Left, local_image->Top,
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #8
LDRH	R2, [R2, #0]
UXTH	R3, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, R2, #6
LDRH	R2, [R2, #0]
UXTH	R1, R7
UXTH	R0, R8
;BPWasher_driver.c,3506 :: 		local_image->Width, local_image->Height) == 1) {
PUSH	(R5)
PUSH	(R4)
BL	BPWasher_driver_IsInsideObject+0
ADD	SP, SP, #8
CMP	R0, #1
IT	NE
BNE	L_Get_Object151
;BPWasher_driver.c,3507 :: 		image_order = local_image->Order;
MOVW	R4, #lo_addr(_local_image+0)
MOVT	R4, #hi_addr(_local_image+0)
LDR	R2, [R4, #0]
ADDS	R2, R2, #4
LDRB	R3, [R2, #0]
MOVW	R2, #lo_addr(_image_order+0)
MOVT	R2, #hi_addr(_image_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3508 :: 		exec_image = local_image;
MOV	R2, R4
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_exec_image+0)
MOVT	R2, #hi_addr(_exec_image+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3509 :: 		}
L_Get_Object151:
;BPWasher_driver.c,3510 :: 		}
L_Get_Object150:
;BPWasher_driver.c,3502 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->ImagesCount ; _object_count++ ) {
MOVW	R3, #lo_addr(__object_count+0)
MOVT	R3, #hi_addr(__object_count+0)
LDRSH	R2, [R3, #0]
ADDS	R2, R2, #1
STRH	R2, [R3, #0]
;BPWasher_driver.c,3511 :: 		}
IT	AL
BAL	L_Get_Object147
L_Get_Object148:
;BPWasher_driver.c,3514 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->CImagesCount ; _object_count++ ) {
MOVS	R3, #0
SXTH	R3, R3
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
; Y end address is: 28 (R7)
; X end address is: 32 (R8)
STRH	R8, [SP, #4]
UXTH	R8, R7
LDRH	R7, [SP, #4]
L_Get_Object152:
; X start address is: 28 (R7)
; Y start address is: 32 (R8)
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #48
LDRH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	CS
BCS	L_Get_Object153
;BPWasher_driver.c,3515 :: 		local_cimage = GetCImage(_object_count);
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #52
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
LSLS	R2, R2, #2
ADDS	R2, R3, R2
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_local_cimage+0)
MOVT	R2, #hi_addr(_local_cimage+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3516 :: 		if (local_cimage->Active != 0) {
ADDW	R2, R3, #21
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Get_Object155
;BPWasher_driver.c,3518 :: 		local_cimage->Width, local_cimage->Height) == 1) {
MOVW	R6, #lo_addr(_local_cimage+0)
MOVT	R6, #hi_addr(_local_cimage+0)
LDR	R2, [R6, #0]
ADDS	R2, #12
LDRH	R2, [R2, #0]
UXTH	R5, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #10
LDRH	R2, [R2, #0]
UXTH	R4, R2
;BPWasher_driver.c,3517 :: 		if (IsInsideObject(X, Y, local_cimage->Left, local_cimage->Top,
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #8
LDRH	R2, [R2, #0]
UXTH	R3, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, R2, #6
LDRH	R2, [R2, #0]
UXTH	R1, R8
UXTH	R0, R7
;BPWasher_driver.c,3518 :: 		local_cimage->Width, local_cimage->Height) == 1) {
PUSH	(R5)
PUSH	(R4)
BL	BPWasher_driver_IsInsideObject+0
ADD	SP, SP, #8
CMP	R0, #1
IT	NE
BNE	L_Get_Object156
;BPWasher_driver.c,3519 :: 		cimage_order = local_cimage->Order;
MOVW	R4, #lo_addr(_local_cimage+0)
MOVT	R4, #hi_addr(_local_cimage+0)
LDR	R2, [R4, #0]
ADDS	R2, R2, #4
LDRB	R3, [R2, #0]
MOVW	R2, #lo_addr(_cimage_order+0)
MOVT	R2, #hi_addr(_cimage_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3520 :: 		exec_cimage = local_cimage;
MOV	R2, R4
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_exec_cimage+0)
MOVT	R2, #hi_addr(_exec_cimage+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3521 :: 		}
L_Get_Object156:
;BPWasher_driver.c,3522 :: 		}
L_Get_Object155:
;BPWasher_driver.c,3514 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->CImagesCount ; _object_count++ ) {
MOVW	R3, #lo_addr(__object_count+0)
MOVT	R3, #hi_addr(__object_count+0)
LDRSH	R2, [R3, #0]
ADDS	R2, R2, #1
STRH	R2, [R3, #0]
;BPWasher_driver.c,3523 :: 		}
IT	AL
BAL	L_Get_Object152
L_Get_Object153:
;BPWasher_driver.c,3526 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->CirclesCount ; _object_count++ ) {
MOVS	R3, #0
SXTH	R3, R3
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
; X end address is: 28 (R7)
; Y end address is: 32 (R8)
L_Get_Object157:
; Y start address is: 32 (R8)
; X start address is: 28 (R7)
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #56
LDRH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	CS
BCS	L_Get_Object158
;BPWasher_driver.c,3527 :: 		local_circle = GetCircle(_object_count);
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #60
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
LSLS	R2, R2, #2
ADDS	R2, R3, R2
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_local_circle+0)
MOVT	R2, #hi_addr(_local_circle+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3528 :: 		if (local_circle->Active != 0) {
ADDW	R2, R3, #17
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Get_Object160
;BPWasher_driver.c,3530 :: 		(local_circle->Radius * 2), (local_circle->Radius * 2)) == 1) {
MOVW	R5, #lo_addr(_local_circle+0)
MOVT	R5, #hi_addr(_local_circle+0)
LDR	R2, [R5, #0]
ADDS	R2, #10
LDRH	R2, [R2, #0]
LSLS	R4, R2, #1
UXTH	R4, R4
;BPWasher_driver.c,3529 :: 		if (IsInsideObject(X, Y, local_circle->Left, local_circle->Top,
MOV	R2, R5
LDR	R2, [R2, #0]
ADDS	R2, #8
LDRH	R2, [R2, #0]
UXTH	R3, R2
MOV	R2, R5
LDR	R2, [R2, #0]
ADDS	R2, R2, #6
LDRH	R2, [R2, #0]
UXTH	R1, R8
UXTH	R0, R7
;BPWasher_driver.c,3530 :: 		(local_circle->Radius * 2), (local_circle->Radius * 2)) == 1) {
PUSH	(R4)
PUSH	(R4)
BL	BPWasher_driver_IsInsideObject+0
ADD	SP, SP, #8
CMP	R0, #1
IT	NE
BNE	L_Get_Object161
;BPWasher_driver.c,3531 :: 		circle_order = local_circle->Order;
MOVW	R4, #lo_addr(_local_circle+0)
MOVT	R4, #hi_addr(_local_circle+0)
LDR	R2, [R4, #0]
ADDS	R2, R2, #4
LDRB	R3, [R2, #0]
MOVW	R2, #lo_addr(_circle_order+0)
MOVT	R2, #hi_addr(_circle_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3532 :: 		exec_circle = local_circle;
MOV	R2, R4
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_exec_circle+0)
MOVT	R2, #hi_addr(_exec_circle+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3533 :: 		}
L_Get_Object161:
;BPWasher_driver.c,3534 :: 		}
L_Get_Object160:
;BPWasher_driver.c,3526 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->CirclesCount ; _object_count++ ) {
MOVW	R3, #lo_addr(__object_count+0)
MOVT	R3, #hi_addr(__object_count+0)
LDRSH	R2, [R3, #0]
ADDS	R2, R2, #1
STRH	R2, [R3, #0]
;BPWasher_driver.c,3535 :: 		}
IT	AL
BAL	L_Get_Object157
L_Get_Object158:
;BPWasher_driver.c,3538 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->BoxesCount ; _object_count++ ) {
MOVS	R3, #0
SXTH	R3, R3
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
; X end address is: 28 (R7)
; Y end address is: 32 (R8)
L_Get_Object162:
; X start address is: 28 (R7)
; Y start address is: 32 (R8)
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #64
LDRH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	CS
BCS	L_Get_Object163
;BPWasher_driver.c,3539 :: 		local_box = GetBox(_object_count);
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #68
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
LSLS	R2, R2, #2
ADDS	R2, R3, R2
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_local_box+0)
MOVT	R2, #hi_addr(_local_box+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3540 :: 		if (local_box->Active != 0) {
ADDW	R2, R3, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Get_Object165
;BPWasher_driver.c,3542 :: 		local_box->Width, local_box->Height) == 1) {
MOVW	R6, #lo_addr(_local_box+0)
MOVT	R6, #hi_addr(_local_box+0)
LDR	R2, [R6, #0]
ADDS	R2, #12
LDRH	R2, [R2, #0]
UXTH	R5, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #10
LDRH	R2, [R2, #0]
UXTH	R4, R2
;BPWasher_driver.c,3541 :: 		if (IsInsideObject(X, Y, local_box->Left, local_box->Top,
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #8
LDRH	R2, [R2, #0]
UXTH	R3, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, R2, #6
LDRH	R2, [R2, #0]
UXTH	R1, R8
UXTH	R0, R7
;BPWasher_driver.c,3542 :: 		local_box->Width, local_box->Height) == 1) {
PUSH	(R5)
PUSH	(R4)
BL	BPWasher_driver_IsInsideObject+0
ADD	SP, SP, #8
CMP	R0, #1
IT	NE
BNE	L_Get_Object166
;BPWasher_driver.c,3543 :: 		box_order = local_box->Order;
MOVW	R4, #lo_addr(_local_box+0)
MOVT	R4, #hi_addr(_local_box+0)
LDR	R2, [R4, #0]
ADDS	R2, R2, #4
LDRB	R3, [R2, #0]
MOVW	R2, #lo_addr(_box_order+0)
MOVT	R2, #hi_addr(_box_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3544 :: 		exec_box = local_box;
MOV	R2, R4
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_exec_box+0)
MOVT	R2, #hi_addr(_exec_box+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3545 :: 		}
L_Get_Object166:
;BPWasher_driver.c,3546 :: 		}
L_Get_Object165:
;BPWasher_driver.c,3538 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->BoxesCount ; _object_count++ ) {
MOVW	R3, #lo_addr(__object_count+0)
MOVT	R3, #hi_addr(__object_count+0)
LDRSH	R2, [R3, #0]
ADDS	R2, R2, #1
STRH	R2, [R3, #0]
;BPWasher_driver.c,3547 :: 		}
IT	AL
BAL	L_Get_Object162
L_Get_Object163:
;BPWasher_driver.c,3550 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->CheckBoxesCount ; _object_count++ ) {
MOVS	R3, #0
SXTH	R3, R3
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
; X end address is: 28 (R7)
; Y end address is: 32 (R8)
STRH	R8, [SP, #4]
UXTH	R8, R7
LDRH	R7, [SP, #4]
L_Get_Object167:
; X start address is: 32 (R8)
; Y start address is: 28 (R7)
; Y start address is: 28 (R7)
; Y end address is: 28 (R7)
; X start address is: 32 (R8)
; X end address is: 32 (R8)
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #80
LDRH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	CS
BCS	L_Get_Object168
; Y end address is: 28 (R7)
; X end address is: 32 (R8)
;BPWasher_driver.c,3551 :: 		local_checkbox = GetCheckBox(_object_count);
; X start address is: 32 (R8)
; Y start address is: 28 (R7)
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
ADDS	R2, #84
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
LSLS	R2, R2, #2
ADDS	R2, R3, R2
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_local_checkbox+0)
MOVT	R2, #hi_addr(_local_checkbox+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3552 :: 		if (local_checkbox->Active != 0) {
ADDW	R2, R3, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Get_Object170
;BPWasher_driver.c,3554 :: 		local_checkbox->Width, local_checkbox->Height) == 1) {
MOVW	R6, #lo_addr(_local_checkbox+0)
MOVT	R6, #hi_addr(_local_checkbox+0)
LDR	R2, [R6, #0]
ADDS	R2, #12
LDRH	R2, [R2, #0]
UXTH	R5, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #10
LDRH	R2, [R2, #0]
UXTH	R4, R2
;BPWasher_driver.c,3553 :: 		if (IsInsideObject(X, Y, local_checkbox->Left, local_checkbox->Top,
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, #8
LDRH	R2, [R2, #0]
UXTH	R3, R2
MOV	R2, R6
LDR	R2, [R2, #0]
ADDS	R2, R2, #6
LDRH	R2, [R2, #0]
UXTH	R1, R7
UXTH	R0, R8
;BPWasher_driver.c,3554 :: 		local_checkbox->Width, local_checkbox->Height) == 1) {
PUSH	(R5)
PUSH	(R4)
BL	BPWasher_driver_IsInsideObject+0
ADD	SP, SP, #8
CMP	R0, #1
IT	NE
BNE	L_Get_Object171
;BPWasher_driver.c,3555 :: 		checkbox_order = local_checkbox->Order;
MOVW	R4, #lo_addr(_local_checkbox+0)
MOVT	R4, #hi_addr(_local_checkbox+0)
LDR	R2, [R4, #0]
ADDS	R2, R2, #4
LDRB	R3, [R2, #0]
MOVW	R2, #lo_addr(_checkbox_order+0)
MOVT	R2, #hi_addr(_checkbox_order+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3556 :: 		exec_checkbox = local_checkbox;
MOV	R2, R4
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3557 :: 		}
L_Get_Object171:
;BPWasher_driver.c,3558 :: 		}
L_Get_Object170:
;BPWasher_driver.c,3550 :: 		for ( _object_count = 0 ; _object_count < CurrentScreen->CheckBoxesCount ; _object_count++ ) {
MOVW	R3, #lo_addr(__object_count+0)
MOVT	R3, #hi_addr(__object_count+0)
LDRSH	R2, [R3, #0]
ADDS	R2, R2, #1
STRH	R2, [R3, #0]
;BPWasher_driver.c,3559 :: 		}
; Y end address is: 28 (R7)
; X end address is: 32 (R8)
IT	AL
BAL	L_Get_Object167
L_Get_Object168:
;BPWasher_driver.c,3561 :: 		_object_count = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3562 :: 		if (button_order >  _object_count )
MOVW	R2, #lo_addr(_button_order+0)
MOVT	R2, #hi_addr(_button_order+0)
LDRSH	R2, [R2, #0]
CMP	R2, #-1
IT	LE
BLE	L_Get_Object172
;BPWasher_driver.c,3563 :: 		_object_count = button_order;
MOVW	R2, #lo_addr(_button_order+0)
MOVT	R2, #hi_addr(_button_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
L_Get_Object172:
;BPWasher_driver.c,3564 :: 		if (round_button_order >  _object_count )
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(_round_button_order+0)
MOVT	R2, #hi_addr(_round_button_order+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	LE
BLE	L_Get_Object173
;BPWasher_driver.c,3565 :: 		_object_count = round_button_order;
MOVW	R2, #lo_addr(_round_button_order+0)
MOVT	R2, #hi_addr(_round_button_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
L_Get_Object173:
;BPWasher_driver.c,3566 :: 		if (round_cbutton_order >  _object_count )
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(_round_cbutton_order+0)
MOVT	R2, #hi_addr(_round_cbutton_order+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	LE
BLE	L_Get_Object174
;BPWasher_driver.c,3567 :: 		_object_count = round_cbutton_order;
MOVW	R2, #lo_addr(_round_cbutton_order+0)
MOVT	R2, #hi_addr(_round_cbutton_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
L_Get_Object174:
;BPWasher_driver.c,3568 :: 		if (label_order >  _object_count )
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(_label_order+0)
MOVT	R2, #hi_addr(_label_order+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	LE
BLE	L_Get_Object175
;BPWasher_driver.c,3569 :: 		_object_count = label_order;
MOVW	R2, #lo_addr(_label_order+0)
MOVT	R2, #hi_addr(_label_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
L_Get_Object175:
;BPWasher_driver.c,3570 :: 		if (image_order >  _object_count )
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(_image_order+0)
MOVT	R2, #hi_addr(_image_order+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	LE
BLE	L_Get_Object176
;BPWasher_driver.c,3571 :: 		_object_count = image_order;
MOVW	R2, #lo_addr(_image_order+0)
MOVT	R2, #hi_addr(_image_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
L_Get_Object176:
;BPWasher_driver.c,3572 :: 		if (cimage_order >  _object_count )
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(_cimage_order+0)
MOVT	R2, #hi_addr(_cimage_order+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	LE
BLE	L_Get_Object177
;BPWasher_driver.c,3573 :: 		_object_count = cimage_order;
MOVW	R2, #lo_addr(_cimage_order+0)
MOVT	R2, #hi_addr(_cimage_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
L_Get_Object177:
;BPWasher_driver.c,3574 :: 		if (circle_order >  _object_count )
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(_circle_order+0)
MOVT	R2, #hi_addr(_circle_order+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	LE
BLE	L_Get_Object178
;BPWasher_driver.c,3575 :: 		_object_count = circle_order;
MOVW	R2, #lo_addr(_circle_order+0)
MOVT	R2, #hi_addr(_circle_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
L_Get_Object178:
;BPWasher_driver.c,3576 :: 		if (box_order >  _object_count )
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(_box_order+0)
MOVT	R2, #hi_addr(_box_order+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	LE
BLE	L_Get_Object179
;BPWasher_driver.c,3577 :: 		_object_count = box_order;
MOVW	R2, #lo_addr(_box_order+0)
MOVT	R2, #hi_addr(_box_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
L_Get_Object179:
;BPWasher_driver.c,3578 :: 		if (checkbox_order >  _object_count )
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(_checkbox_order+0)
MOVT	R2, #hi_addr(_checkbox_order+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	LE
BLE	L_Get_Object180
;BPWasher_driver.c,3579 :: 		_object_count = checkbox_order;
MOVW	R2, #lo_addr(_checkbox_order+0)
MOVT	R2, #hi_addr(_checkbox_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
STRH	R3, [R2, #0]
L_Get_Object180:
;BPWasher_driver.c,3580 :: 		}
L_end_Get_Object:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _Get_Object
_Process_TP_Press:
;BPWasher_driver.c,3583 :: 		void Process_TP_Press(unsigned int X, unsigned int Y) {
; Y start address is: 4 (R1)
; X start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; Y end address is: 4 (R1)
; X end address is: 0 (R0)
; X start address is: 0 (R0)
; Y start address is: 4 (R1)
;BPWasher_driver.c,3584 :: 		exec_button         = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_button+0)
MOVT	R2, #hi_addr(_exec_button+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3585 :: 		exec_round_button   = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_round_button+0)
MOVT	R2, #hi_addr(_exec_round_button+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3586 :: 		exec_round_cbutton  = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_round_cbutton+0)
MOVT	R2, #hi_addr(_exec_round_cbutton+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3587 :: 		exec_label          = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_label+0)
MOVT	R2, #hi_addr(_exec_label+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3588 :: 		exec_image          = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_image+0)
MOVT	R2, #hi_addr(_exec_image+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3589 :: 		exec_cimage         = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_cimage+0)
MOVT	R2, #hi_addr(_exec_cimage+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3590 :: 		exec_circle         = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_circle+0)
MOVT	R2, #hi_addr(_exec_circle+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3591 :: 		exec_box            = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_box+0)
MOVT	R2, #hi_addr(_exec_box+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3592 :: 		exec_checkbox       = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3594 :: 		Get_Object(X, Y);
; Y end address is: 4 (R1)
; X end address is: 0 (R0)
BL	_Get_Object+0
;BPWasher_driver.c,3596 :: 		if (_object_count != -1) {
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, #-1
IT	EQ
BEQ	L_Process_TP_Press181
;BPWasher_driver.c,3597 :: 		if (_object_count == button_order) {
MOVW	R2, #lo_addr(_button_order+0)
MOVT	R2, #hi_addr(_button_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Press182
;BPWasher_driver.c,3598 :: 		if (exec_button->Active != 0) {
MOVW	R2, #lo_addr(_exec_button+0)
MOVT	R2, #hi_addr(_exec_button+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Press183
;BPWasher_driver.c,3599 :: 		if (exec_button->OnPressPtr != 0) {
MOVW	R4, #lo_addr(_exec_button+0)
MOVT	R4, #hi_addr(_exec_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #64
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Press184
;BPWasher_driver.c,3600 :: 		exec_button->OnPressPtr();
MOVW	R4, #lo_addr(_exec_button+0)
MOVT	R4, #hi_addr(_exec_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #64
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3601 :: 		return;
IT	AL
BAL	L_end_Process_TP_Press
;BPWasher_driver.c,3602 :: 		}
L_Process_TP_Press184:
;BPWasher_driver.c,3603 :: 		}
L_Process_TP_Press183:
;BPWasher_driver.c,3604 :: 		}
L_Process_TP_Press182:
;BPWasher_driver.c,3606 :: 		if (_object_count == round_button_order) {
MOVW	R2, #lo_addr(_round_button_order+0)
MOVT	R2, #hi_addr(_round_button_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Press185
;BPWasher_driver.c,3607 :: 		if (exec_round_button->Active != 0) {
MOVW	R2, #lo_addr(_exec_round_button+0)
MOVT	R2, #hi_addr(_exec_round_button+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Press186
;BPWasher_driver.c,3608 :: 		if (exec_round_button->OnPressPtr != 0) {
MOVW	R4, #lo_addr(_exec_round_button+0)
MOVT	R4, #hi_addr(_exec_round_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #64
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Press187
;BPWasher_driver.c,3609 :: 		exec_round_button->OnPressPtr();
MOVW	R4, #lo_addr(_exec_round_button+0)
MOVT	R4, #hi_addr(_exec_round_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #64
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3610 :: 		return;
IT	AL
BAL	L_end_Process_TP_Press
;BPWasher_driver.c,3611 :: 		}
L_Process_TP_Press187:
;BPWasher_driver.c,3612 :: 		}
L_Process_TP_Press186:
;BPWasher_driver.c,3613 :: 		}
L_Process_TP_Press185:
;BPWasher_driver.c,3615 :: 		if (_object_count == round_cbutton_order) {
MOVW	R2, #lo_addr(_round_cbutton_order+0)
MOVT	R2, #hi_addr(_round_cbutton_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Press188
;BPWasher_driver.c,3616 :: 		if (exec_round_cbutton->Active != 0) {
MOVW	R2, #lo_addr(_exec_round_cbutton+0)
MOVT	R2, #hi_addr(_exec_round_cbutton+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Press189
;BPWasher_driver.c,3617 :: 		if (exec_round_cbutton->OnPressPtr != 0) {
MOVW	R4, #lo_addr(_exec_round_cbutton+0)
MOVT	R4, #hi_addr(_exec_round_cbutton+0)
LDR	R4, [R4, #0]
ADDS	R4, #64
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Press190
;BPWasher_driver.c,3618 :: 		exec_round_cbutton->OnPressPtr();
MOVW	R4, #lo_addr(_exec_round_cbutton+0)
MOVT	R4, #hi_addr(_exec_round_cbutton+0)
LDR	R4, [R4, #0]
ADDS	R4, #64
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3619 :: 		return;
IT	AL
BAL	L_end_Process_TP_Press
;BPWasher_driver.c,3620 :: 		}
L_Process_TP_Press190:
;BPWasher_driver.c,3621 :: 		}
L_Process_TP_Press189:
;BPWasher_driver.c,3622 :: 		}
L_Process_TP_Press188:
;BPWasher_driver.c,3624 :: 		if (_object_count == label_order) {
MOVW	R2, #lo_addr(_label_order+0)
MOVT	R2, #hi_addr(_label_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Press191
;BPWasher_driver.c,3625 :: 		if (exec_label->Active != 0) {
MOVW	R2, #lo_addr(_exec_label+0)
MOVT	R2, #hi_addr(_exec_label+0)
LDR	R2, [R2, #0]
ADDS	R2, #28
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Press192
;BPWasher_driver.c,3626 :: 		if (exec_label->OnPressPtr != 0) {
MOVW	R4, #lo_addr(_exec_label+0)
MOVT	R4, #hi_addr(_exec_label+0)
LDR	R4, [R4, #0]
ADDS	R4, #44
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Press193
;BPWasher_driver.c,3627 :: 		exec_label->OnPressPtr();
MOVW	R4, #lo_addr(_exec_label+0)
MOVT	R4, #hi_addr(_exec_label+0)
LDR	R4, [R4, #0]
ADDS	R4, #44
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3628 :: 		return;
IT	AL
BAL	L_end_Process_TP_Press
;BPWasher_driver.c,3629 :: 		}
L_Process_TP_Press193:
;BPWasher_driver.c,3630 :: 		}
L_Process_TP_Press192:
;BPWasher_driver.c,3631 :: 		}
L_Process_TP_Press191:
;BPWasher_driver.c,3633 :: 		if (_object_count == image_order) {
MOVW	R2, #lo_addr(_image_order+0)
MOVT	R2, #hi_addr(_image_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Press194
;BPWasher_driver.c,3634 :: 		if (exec_image->Active != 0) {
MOVW	R2, #lo_addr(_exec_image+0)
MOVT	R2, #hi_addr(_exec_image+0)
LDR	R2, [R2, #0]
ADDS	R2, #21
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Press195
;BPWasher_driver.c,3635 :: 		if (exec_image->OnPressPtr != 0) {
MOVW	R4, #lo_addr(_exec_image+0)
MOVT	R4, #hi_addr(_exec_image+0)
LDR	R4, [R4, #0]
ADDS	R4, #36
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Press196
;BPWasher_driver.c,3636 :: 		exec_image->OnPressPtr();
MOVW	R4, #lo_addr(_exec_image+0)
MOVT	R4, #hi_addr(_exec_image+0)
LDR	R4, [R4, #0]
ADDS	R4, #36
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3637 :: 		return;
IT	AL
BAL	L_end_Process_TP_Press
;BPWasher_driver.c,3638 :: 		}
L_Process_TP_Press196:
;BPWasher_driver.c,3639 :: 		}
L_Process_TP_Press195:
;BPWasher_driver.c,3640 :: 		}
L_Process_TP_Press194:
;BPWasher_driver.c,3642 :: 		if (_object_count == cimage_order) {
MOVW	R2, #lo_addr(_cimage_order+0)
MOVT	R2, #hi_addr(_cimage_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Press197
;BPWasher_driver.c,3643 :: 		if (exec_cimage->Active != 0) {
MOVW	R2, #lo_addr(_exec_cimage+0)
MOVT	R2, #hi_addr(_exec_cimage+0)
LDR	R2, [R2, #0]
ADDS	R2, #21
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Press198
;BPWasher_driver.c,3644 :: 		if (exec_cimage->OnPressPtr != 0) {
MOVW	R4, #lo_addr(_exec_cimage+0)
MOVT	R4, #hi_addr(_exec_cimage+0)
LDR	R4, [R4, #0]
ADDS	R4, #36
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Press199
;BPWasher_driver.c,3645 :: 		exec_cimage->OnPressPtr();
MOVW	R4, #lo_addr(_exec_cimage+0)
MOVT	R4, #hi_addr(_exec_cimage+0)
LDR	R4, [R4, #0]
ADDS	R4, #36
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3646 :: 		return;
IT	AL
BAL	L_end_Process_TP_Press
;BPWasher_driver.c,3647 :: 		}
L_Process_TP_Press199:
;BPWasher_driver.c,3648 :: 		}
L_Process_TP_Press198:
;BPWasher_driver.c,3649 :: 		}
L_Process_TP_Press197:
;BPWasher_driver.c,3651 :: 		if (_object_count == circle_order) {
MOVW	R2, #lo_addr(_circle_order+0)
MOVT	R2, #hi_addr(_circle_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Press200
;BPWasher_driver.c,3652 :: 		if (exec_circle->Active != 0) {
MOVW	R2, #lo_addr(_exec_circle+0)
MOVT	R2, #hi_addr(_exec_circle+0)
LDR	R2, [R2, #0]
ADDS	R2, #17
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Press201
;BPWasher_driver.c,3653 :: 		if (exec_circle->OnPressPtr != 0) {
MOVW	R4, #lo_addr(_exec_circle+0)
MOVT	R4, #hi_addr(_exec_circle+0)
LDR	R4, [R4, #0]
ADDS	R4, #44
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Press202
;BPWasher_driver.c,3654 :: 		exec_circle->OnPressPtr();
MOVW	R4, #lo_addr(_exec_circle+0)
MOVT	R4, #hi_addr(_exec_circle+0)
LDR	R4, [R4, #0]
ADDS	R4, #44
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3655 :: 		return;
IT	AL
BAL	L_end_Process_TP_Press
;BPWasher_driver.c,3656 :: 		}
L_Process_TP_Press202:
;BPWasher_driver.c,3657 :: 		}
L_Process_TP_Press201:
;BPWasher_driver.c,3658 :: 		}
L_Process_TP_Press200:
;BPWasher_driver.c,3660 :: 		if (_object_count == box_order) {
MOVW	R2, #lo_addr(_box_order+0)
MOVT	R2, #hi_addr(_box_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Press203
;BPWasher_driver.c,3661 :: 		if (exec_box->Active != 0) {
MOVW	R2, #lo_addr(_exec_box+0)
MOVT	R2, #hi_addr(_exec_box+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Press204
;BPWasher_driver.c,3662 :: 		if (exec_box->OnPressPtr != 0) {
MOVW	R4, #lo_addr(_exec_box+0)
MOVT	R4, #hi_addr(_exec_box+0)
LDR	R4, [R4, #0]
ADDS	R4, #48
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Press205
;BPWasher_driver.c,3663 :: 		exec_box->OnPressPtr();
MOVW	R4, #lo_addr(_exec_box+0)
MOVT	R4, #hi_addr(_exec_box+0)
LDR	R4, [R4, #0]
ADDS	R4, #48
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3664 :: 		return;
IT	AL
BAL	L_end_Process_TP_Press
;BPWasher_driver.c,3665 :: 		}
L_Process_TP_Press205:
;BPWasher_driver.c,3666 :: 		}
L_Process_TP_Press204:
;BPWasher_driver.c,3667 :: 		}
L_Process_TP_Press203:
;BPWasher_driver.c,3669 :: 		if (_object_count == checkbox_order) {
MOVW	R2, #lo_addr(_checkbox_order+0)
MOVT	R2, #hi_addr(_checkbox_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Press206
;BPWasher_driver.c,3670 :: 		if (exec_checkbox->Active != 0) {
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Press207
;BPWasher_driver.c,3671 :: 		if (exec_checkbox->OnPressPtr != 0) {
MOVW	R4, #lo_addr(_exec_checkbox+0)
MOVT	R4, #hi_addr(_exec_checkbox+0)
LDR	R4, [R4, #0]
ADDS	R4, #64
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Press208
;BPWasher_driver.c,3672 :: 		exec_checkbox->OnPressPtr();
MOVW	R4, #lo_addr(_exec_checkbox+0)
MOVT	R4, #hi_addr(_exec_checkbox+0)
LDR	R4, [R4, #0]
ADDS	R4, #64
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3673 :: 		return;
IT	AL
BAL	L_end_Process_TP_Press
;BPWasher_driver.c,3674 :: 		}
L_Process_TP_Press208:
;BPWasher_driver.c,3675 :: 		}
L_Process_TP_Press207:
;BPWasher_driver.c,3676 :: 		}
L_Process_TP_Press206:
;BPWasher_driver.c,3678 :: 		}
L_Process_TP_Press181:
;BPWasher_driver.c,3679 :: 		}
L_end_Process_TP_Press:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Process_TP_Press
_Process_TP_Up:
;BPWasher_driver.c,3681 :: 		void Process_TP_Up(unsigned int X, unsigned int Y) {
; Y start address is: 4 (R1)
; X start address is: 0 (R0)
SUB	SP, SP, #12
STR	LR, [SP, #0]
; Y end address is: 4 (R1)
; X end address is: 0 (R0)
; X start address is: 0 (R0)
; Y start address is: 4 (R1)
;BPWasher_driver.c,3683 :: 		switch (PressedObjectType) {
IT	AL
BAL	L_Process_TP_Up209
;BPWasher_driver.c,3685 :: 		case 0: {
L_Process_TP_Up211:
;BPWasher_driver.c,3686 :: 		if (PressedObject != 0) {
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up212
;BPWasher_driver.c,3687 :: 		exec_button = (TButton*)PressedObject;
MOVW	R4, #lo_addr(_PressedObject+0)
MOVT	R4, #hi_addr(_PressedObject+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_exec_button+0)
MOVT	R2, #hi_addr(_exec_button+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3688 :: 		if ((exec_button->PressColEnabled != 0) && (exec_button->OwnerScreen == CurrentScreen)) {
MOV	R2, R4
LDR	R2, [R2, #0]
ADDS	R2, #48
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L__Process_TP_Up372
MOVW	R2, #lo_addr(_exec_button+0)
MOVT	R2, #hi_addr(_exec_button+0)
LDR	R2, [R2, #0]
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
CMP	R3, R2
IT	NE
BNE	L__Process_TP_Up371
L__Process_TP_Up370:
;BPWasher_driver.c,3689 :: 		DrawButton(exec_button);
MOVW	R2, #lo_addr(_exec_button+0)
MOVT	R2, #hi_addr(_exec_button+0)
LDR	R2, [R2, #0]
STRH	R1, [SP, #4]
STRH	R0, [SP, #8]
MOV	R0, R2
BL	_DrawButton+0
LDRH	R0, [SP, #8]
LDRH	R1, [SP, #4]
;BPWasher_driver.c,3688 :: 		if ((exec_button->PressColEnabled != 0) && (exec_button->OwnerScreen == CurrentScreen)) {
L__Process_TP_Up372:
L__Process_TP_Up371:
;BPWasher_driver.c,3691 :: 		break;
IT	AL
BAL	L_Process_TP_Up210
;BPWasher_driver.c,3692 :: 		}
L_Process_TP_Up212:
;BPWasher_driver.c,3693 :: 		break;
IT	AL
BAL	L_Process_TP_Up210
;BPWasher_driver.c,3696 :: 		case 1: {
L_Process_TP_Up216:
;BPWasher_driver.c,3697 :: 		if (PressedObject != 0) {
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up217
;BPWasher_driver.c,3698 :: 		exec_round_button = (TButton_Round*)PressedObject;
MOVW	R4, #lo_addr(_PressedObject+0)
MOVT	R4, #hi_addr(_PressedObject+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_exec_round_button+0)
MOVT	R2, #hi_addr(_exec_round_button+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3699 :: 		if ((exec_round_button->PressColEnabled != 0) && (exec_round_button->OwnerScreen == CurrentScreen)) {
MOV	R2, R4
LDR	R2, [R2, #0]
ADDS	R2, #49
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L__Process_TP_Up374
MOVW	R2, #lo_addr(_exec_round_button+0)
MOVT	R2, #hi_addr(_exec_round_button+0)
LDR	R2, [R2, #0]
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
CMP	R3, R2
IT	NE
BNE	L__Process_TP_Up373
L__Process_TP_Up369:
;BPWasher_driver.c,3700 :: 		DrawRoundButton(exec_round_button);
MOVW	R2, #lo_addr(_exec_round_button+0)
MOVT	R2, #hi_addr(_exec_round_button+0)
LDR	R2, [R2, #0]
STRH	R1, [SP, #4]
STRH	R0, [SP, #8]
MOV	R0, R2
BL	_DrawRoundButton+0
LDRH	R0, [SP, #8]
LDRH	R1, [SP, #4]
;BPWasher_driver.c,3699 :: 		if ((exec_round_button->PressColEnabled != 0) && (exec_round_button->OwnerScreen == CurrentScreen)) {
L__Process_TP_Up374:
L__Process_TP_Up373:
;BPWasher_driver.c,3702 :: 		break;
IT	AL
BAL	L_Process_TP_Up210
;BPWasher_driver.c,3703 :: 		}
L_Process_TP_Up217:
;BPWasher_driver.c,3704 :: 		break;
IT	AL
BAL	L_Process_TP_Up210
;BPWasher_driver.c,3707 :: 		case 9: {
L_Process_TP_Up221:
;BPWasher_driver.c,3708 :: 		if (PressedObject != 0) {
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up222
;BPWasher_driver.c,3709 :: 		exec_round_cbutton = (TCButton_Round*)PressedObject;
MOVW	R4, #lo_addr(_PressedObject+0)
MOVT	R4, #hi_addr(_PressedObject+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_exec_round_cbutton+0)
MOVT	R2, #hi_addr(_exec_round_cbutton+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3710 :: 		if ((exec_round_cbutton->PressColEnabled != 0) && (exec_round_cbutton->OwnerScreen == CurrentScreen)) {
MOV	R2, R4
LDR	R2, [R2, #0]
ADDS	R2, #49
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L__Process_TP_Up376
MOVW	R2, #lo_addr(_exec_round_cbutton+0)
MOVT	R2, #hi_addr(_exec_round_cbutton+0)
LDR	R2, [R2, #0]
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
CMP	R3, R2
IT	NE
BNE	L__Process_TP_Up375
L__Process_TP_Up368:
;BPWasher_driver.c,3711 :: 		DrawCRoundButton(exec_round_cbutton);
MOVW	R2, #lo_addr(_exec_round_cbutton+0)
MOVT	R2, #hi_addr(_exec_round_cbutton+0)
LDR	R2, [R2, #0]
STRH	R1, [SP, #4]
STRH	R0, [SP, #8]
MOV	R0, R2
BL	_DrawCRoundButton+0
LDRH	R0, [SP, #8]
LDRH	R1, [SP, #4]
;BPWasher_driver.c,3710 :: 		if ((exec_round_cbutton->PressColEnabled != 0) && (exec_round_cbutton->OwnerScreen == CurrentScreen)) {
L__Process_TP_Up376:
L__Process_TP_Up375:
;BPWasher_driver.c,3713 :: 		break;
IT	AL
BAL	L_Process_TP_Up210
;BPWasher_driver.c,3714 :: 		}
L_Process_TP_Up222:
;BPWasher_driver.c,3715 :: 		break;
IT	AL
BAL	L_Process_TP_Up210
;BPWasher_driver.c,3718 :: 		case 4: {
L_Process_TP_Up226:
;BPWasher_driver.c,3719 :: 		if (PressedObject != 0) {
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up227
;BPWasher_driver.c,3720 :: 		exec_circle = (TCircle*)PressedObject;
MOVW	R4, #lo_addr(_PressedObject+0)
MOVT	R4, #hi_addr(_PressedObject+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_exec_circle+0)
MOVT	R2, #hi_addr(_exec_circle+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3721 :: 		if ((exec_circle->PressColEnabled != 0) && (exec_circle->OwnerScreen == CurrentScreen)) {
MOV	R2, R4
LDR	R2, [R2, #0]
ADDS	R2, #28
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L__Process_TP_Up378
MOVW	R2, #lo_addr(_exec_circle+0)
MOVT	R2, #hi_addr(_exec_circle+0)
LDR	R2, [R2, #0]
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
CMP	R3, R2
IT	NE
BNE	L__Process_TP_Up377
L__Process_TP_Up367:
;BPWasher_driver.c,3722 :: 		DrawCircle(exec_circle);
MOVW	R2, #lo_addr(_exec_circle+0)
MOVT	R2, #hi_addr(_exec_circle+0)
LDR	R2, [R2, #0]
STRH	R1, [SP, #4]
STRH	R0, [SP, #8]
MOV	R0, R2
BL	_DrawCircle+0
LDRH	R0, [SP, #8]
LDRH	R1, [SP, #4]
;BPWasher_driver.c,3721 :: 		if ((exec_circle->PressColEnabled != 0) && (exec_circle->OwnerScreen == CurrentScreen)) {
L__Process_TP_Up378:
L__Process_TP_Up377:
;BPWasher_driver.c,3724 :: 		break;
IT	AL
BAL	L_Process_TP_Up210
;BPWasher_driver.c,3725 :: 		}
L_Process_TP_Up227:
;BPWasher_driver.c,3726 :: 		break;
IT	AL
BAL	L_Process_TP_Up210
;BPWasher_driver.c,3729 :: 		case 6: {
L_Process_TP_Up231:
;BPWasher_driver.c,3730 :: 		if (PressedObject != 0) {
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up232
;BPWasher_driver.c,3731 :: 		exec_box = (TBox*)PressedObject;
MOVW	R4, #lo_addr(_PressedObject+0)
MOVT	R4, #hi_addr(_PressedObject+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_exec_box+0)
MOVT	R2, #hi_addr(_exec_box+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3732 :: 		if ((exec_box->PressColEnabled != 0) && (exec_box->OwnerScreen == CurrentScreen)) {
MOV	R2, R4
LDR	R2, [R2, #0]
ADDS	R2, #30
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L__Process_TP_Up380
MOVW	R2, #lo_addr(_exec_box+0)
MOVT	R2, #hi_addr(_exec_box+0)
LDR	R2, [R2, #0]
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_CurrentScreen+0)
MOVT	R2, #hi_addr(_CurrentScreen+0)
LDR	R2, [R2, #0]
CMP	R3, R2
IT	NE
BNE	L__Process_TP_Up379
L__Process_TP_Up366:
;BPWasher_driver.c,3733 :: 		DrawBox(exec_box);
MOVW	R2, #lo_addr(_exec_box+0)
MOVT	R2, #hi_addr(_exec_box+0)
LDR	R2, [R2, #0]
STRH	R1, [SP, #4]
STRH	R0, [SP, #8]
MOV	R0, R2
BL	_DrawBox+0
LDRH	R0, [SP, #8]
LDRH	R1, [SP, #4]
;BPWasher_driver.c,3732 :: 		if ((exec_box->PressColEnabled != 0) && (exec_box->OwnerScreen == CurrentScreen)) {
L__Process_TP_Up380:
L__Process_TP_Up379:
;BPWasher_driver.c,3735 :: 		break;
IT	AL
BAL	L_Process_TP_Up210
;BPWasher_driver.c,3736 :: 		}
L_Process_TP_Up232:
;BPWasher_driver.c,3737 :: 		break;
IT	AL
BAL	L_Process_TP_Up210
;BPWasher_driver.c,3740 :: 		case 16: {
L_Process_TP_Up236:
;BPWasher_driver.c,3741 :: 		if (PressedObject != 0) {
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up237
;BPWasher_driver.c,3742 :: 		exec_checkbox = (TCheckBox*)PressedObject;
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3743 :: 		break;
IT	AL
BAL	L_Process_TP_Up210
;BPWasher_driver.c,3744 :: 		}
L_Process_TP_Up237:
;BPWasher_driver.c,3745 :: 		break;
IT	AL
BAL	L_Process_TP_Up210
;BPWasher_driver.c,3747 :: 		}
L_Process_TP_Up209:
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
LDRSH	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up211
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
LDRSH	R2, [R2, #0]
CMP	R2, #1
IT	EQ
BEQ	L_Process_TP_Up216
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
LDRSH	R2, [R2, #0]
CMP	R2, #9
IT	EQ
BEQ	L_Process_TP_Up221
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
LDRSH	R2, [R2, #0]
CMP	R2, #4
IT	EQ
BEQ	L_Process_TP_Up226
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
LDRSH	R2, [R2, #0]
CMP	R2, #6
IT	EQ
BEQ	L_Process_TP_Up231
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
LDRSH	R2, [R2, #0]
CMP	R2, #16
IT	EQ
BEQ	L_Process_TP_Up236
L_Process_TP_Up210:
;BPWasher_driver.c,3749 :: 		exec_label          = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_label+0)
MOVT	R2, #hi_addr(_exec_label+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3750 :: 		exec_image          = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_image+0)
MOVT	R2, #hi_addr(_exec_image+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3751 :: 		exec_cimage          = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_cimage+0)
MOVT	R2, #hi_addr(_exec_cimage+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3753 :: 		Get_Object(X, Y);
; Y end address is: 4 (R1)
; X end address is: 0 (R0)
BL	_Get_Object+0
;BPWasher_driver.c,3756 :: 		if (_object_count != -1) {
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, #-1
IT	EQ
BEQ	L_Process_TP_Up238
;BPWasher_driver.c,3758 :: 		if (_object_count == button_order) {
MOVW	R2, #lo_addr(_button_order+0)
MOVT	R2, #hi_addr(_button_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up239
;BPWasher_driver.c,3759 :: 		if (exec_button->Active != 0) {
MOVW	R2, #lo_addr(_exec_button+0)
MOVT	R2, #hi_addr(_exec_button+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up240
;BPWasher_driver.c,3760 :: 		if (exec_button->OnUpPtr != 0)
MOVW	R4, #lo_addr(_exec_button+0)
MOVT	R4, #hi_addr(_exec_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #52
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up241
;BPWasher_driver.c,3761 :: 		exec_button->OnUpPtr();
MOVW	R4, #lo_addr(_exec_button+0)
MOVT	R4, #hi_addr(_exec_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #52
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up241:
;BPWasher_driver.c,3762 :: 		if (PressedObject == (TPointer)exec_button)
MOVW	R2, #lo_addr(_exec_button+0)
MOVT	R2, #hi_addr(_exec_button+0)
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up242
;BPWasher_driver.c,3763 :: 		if (exec_button->OnClickPtr != 0)
MOVW	R4, #lo_addr(_exec_button+0)
MOVT	R4, #hi_addr(_exec_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #60
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up243
;BPWasher_driver.c,3764 :: 		exec_button->OnClickPtr();
MOVW	R4, #lo_addr(_exec_button+0)
MOVT	R4, #hi_addr(_exec_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #60
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up243:
L_Process_TP_Up242:
;BPWasher_driver.c,3765 :: 		PressedObject = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3766 :: 		PressedObjectType = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3767 :: 		return;
IT	AL
BAL	L_end_Process_TP_Up
;BPWasher_driver.c,3768 :: 		}
L_Process_TP_Up240:
;BPWasher_driver.c,3769 :: 		}
L_Process_TP_Up239:
;BPWasher_driver.c,3772 :: 		if (_object_count == round_button_order) {
MOVW	R2, #lo_addr(_round_button_order+0)
MOVT	R2, #hi_addr(_round_button_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up244
;BPWasher_driver.c,3773 :: 		if (exec_round_button->Active != 0) {
MOVW	R2, #lo_addr(_exec_round_button+0)
MOVT	R2, #hi_addr(_exec_round_button+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up245
;BPWasher_driver.c,3774 :: 		if (exec_round_button->OnUpPtr != 0)
MOVW	R4, #lo_addr(_exec_round_button+0)
MOVT	R4, #hi_addr(_exec_round_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #52
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up246
;BPWasher_driver.c,3775 :: 		exec_round_button->OnUpPtr();
MOVW	R4, #lo_addr(_exec_round_button+0)
MOVT	R4, #hi_addr(_exec_round_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #52
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up246:
;BPWasher_driver.c,3776 :: 		if (PressedObject == (TPointer)exec_round_button)
MOVW	R2, #lo_addr(_exec_round_button+0)
MOVT	R2, #hi_addr(_exec_round_button+0)
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up247
;BPWasher_driver.c,3777 :: 		if (exec_round_button->OnClickPtr != 0)
MOVW	R4, #lo_addr(_exec_round_button+0)
MOVT	R4, #hi_addr(_exec_round_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #60
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up248
;BPWasher_driver.c,3778 :: 		exec_round_button->OnClickPtr();
MOVW	R4, #lo_addr(_exec_round_button+0)
MOVT	R4, #hi_addr(_exec_round_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #60
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up248:
L_Process_TP_Up247:
;BPWasher_driver.c,3779 :: 		PressedObject = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3780 :: 		PressedObjectType = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3781 :: 		return;
IT	AL
BAL	L_end_Process_TP_Up
;BPWasher_driver.c,3782 :: 		}
L_Process_TP_Up245:
;BPWasher_driver.c,3783 :: 		}
L_Process_TP_Up244:
;BPWasher_driver.c,3786 :: 		if (_object_count == round_cbutton_order) {
MOVW	R2, #lo_addr(_round_cbutton_order+0)
MOVT	R2, #hi_addr(_round_cbutton_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up249
;BPWasher_driver.c,3787 :: 		if (exec_round_cbutton->Active != 0) {
MOVW	R2, #lo_addr(_exec_round_cbutton+0)
MOVT	R2, #hi_addr(_exec_round_cbutton+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up250
;BPWasher_driver.c,3788 :: 		if (exec_round_cbutton->OnUpPtr != 0)
MOVW	R4, #lo_addr(_exec_round_cbutton+0)
MOVT	R4, #hi_addr(_exec_round_cbutton+0)
LDR	R4, [R4, #0]
ADDS	R4, #52
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up251
;BPWasher_driver.c,3789 :: 		exec_round_cbutton->OnUpPtr();
MOVW	R4, #lo_addr(_exec_round_cbutton+0)
MOVT	R4, #hi_addr(_exec_round_cbutton+0)
LDR	R4, [R4, #0]
ADDS	R4, #52
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up251:
;BPWasher_driver.c,3790 :: 		if (PressedObject == (TPointer)exec_round_cbutton)
MOVW	R2, #lo_addr(_exec_round_cbutton+0)
MOVT	R2, #hi_addr(_exec_round_cbutton+0)
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up252
;BPWasher_driver.c,3791 :: 		if (exec_round_cbutton->OnClickPtr != 0)
MOVW	R4, #lo_addr(_exec_round_cbutton+0)
MOVT	R4, #hi_addr(_exec_round_cbutton+0)
LDR	R4, [R4, #0]
ADDS	R4, #60
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up253
;BPWasher_driver.c,3792 :: 		exec_round_cbutton->OnClickPtr();
MOVW	R4, #lo_addr(_exec_round_cbutton+0)
MOVT	R4, #hi_addr(_exec_round_cbutton+0)
LDR	R4, [R4, #0]
ADDS	R4, #60
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up253:
L_Process_TP_Up252:
;BPWasher_driver.c,3793 :: 		PressedObject = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3794 :: 		PressedObjectType = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3795 :: 		return;
IT	AL
BAL	L_end_Process_TP_Up
;BPWasher_driver.c,3796 :: 		}
L_Process_TP_Up250:
;BPWasher_driver.c,3797 :: 		}
L_Process_TP_Up249:
;BPWasher_driver.c,3800 :: 		if (_object_count == label_order) {
MOVW	R2, #lo_addr(_label_order+0)
MOVT	R2, #hi_addr(_label_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up254
;BPWasher_driver.c,3801 :: 		if (exec_label->Active != 0) {
MOVW	R2, #lo_addr(_exec_label+0)
MOVT	R2, #hi_addr(_exec_label+0)
LDR	R2, [R2, #0]
ADDS	R2, #28
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up255
;BPWasher_driver.c,3802 :: 		if (exec_label->OnUpPtr != 0)
MOVW	R4, #lo_addr(_exec_label+0)
MOVT	R4, #hi_addr(_exec_label+0)
LDR	R4, [R4, #0]
ADDS	R4, #32
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up256
;BPWasher_driver.c,3803 :: 		exec_label->OnUpPtr();
MOVW	R4, #lo_addr(_exec_label+0)
MOVT	R4, #hi_addr(_exec_label+0)
LDR	R4, [R4, #0]
ADDS	R4, #32
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up256:
;BPWasher_driver.c,3804 :: 		if (PressedObject == (TPointer)exec_label)
MOVW	R2, #lo_addr(_exec_label+0)
MOVT	R2, #hi_addr(_exec_label+0)
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up257
;BPWasher_driver.c,3805 :: 		if (exec_label->OnClickPtr != 0)
MOVW	R4, #lo_addr(_exec_label+0)
MOVT	R4, #hi_addr(_exec_label+0)
LDR	R4, [R4, #0]
ADDS	R4, #40
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up258
;BPWasher_driver.c,3806 :: 		exec_label->OnClickPtr();
MOVW	R4, #lo_addr(_exec_label+0)
MOVT	R4, #hi_addr(_exec_label+0)
LDR	R4, [R4, #0]
ADDS	R4, #40
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up258:
L_Process_TP_Up257:
;BPWasher_driver.c,3807 :: 		PressedObject = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3808 :: 		PressedObjectType = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3809 :: 		return;
IT	AL
BAL	L_end_Process_TP_Up
;BPWasher_driver.c,3810 :: 		}
L_Process_TP_Up255:
;BPWasher_driver.c,3811 :: 		}
L_Process_TP_Up254:
;BPWasher_driver.c,3814 :: 		if (_object_count == image_order) {
MOVW	R2, #lo_addr(_image_order+0)
MOVT	R2, #hi_addr(_image_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up259
;BPWasher_driver.c,3815 :: 		if (exec_image->Active != 0) {
MOVW	R2, #lo_addr(_exec_image+0)
MOVT	R2, #hi_addr(_exec_image+0)
LDR	R2, [R2, #0]
ADDS	R2, #21
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up260
;BPWasher_driver.c,3816 :: 		if (exec_image->OnUpPtr != 0)
MOVW	R4, #lo_addr(_exec_image+0)
MOVT	R4, #hi_addr(_exec_image+0)
LDR	R4, [R4, #0]
ADDS	R4, #24
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up261
;BPWasher_driver.c,3817 :: 		exec_image->OnUpPtr();
MOVW	R4, #lo_addr(_exec_image+0)
MOVT	R4, #hi_addr(_exec_image+0)
LDR	R4, [R4, #0]
ADDS	R4, #24
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up261:
;BPWasher_driver.c,3818 :: 		if (PressedObject == (TPointer)exec_image)
MOVW	R2, #lo_addr(_exec_image+0)
MOVT	R2, #hi_addr(_exec_image+0)
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up262
;BPWasher_driver.c,3819 :: 		if (exec_image->OnClickPtr != 0)
MOVW	R4, #lo_addr(_exec_image+0)
MOVT	R4, #hi_addr(_exec_image+0)
LDR	R4, [R4, #0]
ADDS	R4, #32
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up263
;BPWasher_driver.c,3820 :: 		exec_image->OnClickPtr();
MOVW	R4, #lo_addr(_exec_image+0)
MOVT	R4, #hi_addr(_exec_image+0)
LDR	R4, [R4, #0]
ADDS	R4, #32
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up263:
L_Process_TP_Up262:
;BPWasher_driver.c,3821 :: 		PressedObject = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3822 :: 		PressedObjectType = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3823 :: 		return;
IT	AL
BAL	L_end_Process_TP_Up
;BPWasher_driver.c,3824 :: 		}
L_Process_TP_Up260:
;BPWasher_driver.c,3825 :: 		}
L_Process_TP_Up259:
;BPWasher_driver.c,3828 :: 		if (_object_count == cimage_order) {
MOVW	R2, #lo_addr(_cimage_order+0)
MOVT	R2, #hi_addr(_cimage_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up264
;BPWasher_driver.c,3829 :: 		if (exec_cimage->Active != 0) {
MOVW	R2, #lo_addr(_exec_cimage+0)
MOVT	R2, #hi_addr(_exec_cimage+0)
LDR	R2, [R2, #0]
ADDS	R2, #21
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up265
;BPWasher_driver.c,3830 :: 		if (exec_cimage->OnUpPtr != 0)
MOVW	R4, #lo_addr(_exec_cimage+0)
MOVT	R4, #hi_addr(_exec_cimage+0)
LDR	R4, [R4, #0]
ADDS	R4, #24
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up266
;BPWasher_driver.c,3831 :: 		exec_cimage->OnUpPtr();
MOVW	R4, #lo_addr(_exec_cimage+0)
MOVT	R4, #hi_addr(_exec_cimage+0)
LDR	R4, [R4, #0]
ADDS	R4, #24
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up266:
;BPWasher_driver.c,3832 :: 		if (PressedObject == (TPointer)exec_cimage)
MOVW	R2, #lo_addr(_exec_cimage+0)
MOVT	R2, #hi_addr(_exec_cimage+0)
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up267
;BPWasher_driver.c,3833 :: 		if (exec_cimage->OnClickPtr != 0)
MOVW	R4, #lo_addr(_exec_cimage+0)
MOVT	R4, #hi_addr(_exec_cimage+0)
LDR	R4, [R4, #0]
ADDS	R4, #32
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up268
;BPWasher_driver.c,3834 :: 		exec_cimage->OnClickPtr();
MOVW	R4, #lo_addr(_exec_cimage+0)
MOVT	R4, #hi_addr(_exec_cimage+0)
LDR	R4, [R4, #0]
ADDS	R4, #32
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up268:
L_Process_TP_Up267:
;BPWasher_driver.c,3835 :: 		PressedObject = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3836 :: 		PressedObjectType = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3837 :: 		return;
IT	AL
BAL	L_end_Process_TP_Up
;BPWasher_driver.c,3838 :: 		}
L_Process_TP_Up265:
;BPWasher_driver.c,3839 :: 		}
L_Process_TP_Up264:
;BPWasher_driver.c,3842 :: 		if (_object_count == circle_order) {
MOVW	R2, #lo_addr(_circle_order+0)
MOVT	R2, #hi_addr(_circle_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up269
;BPWasher_driver.c,3843 :: 		if (exec_circle->Active != 0) {
MOVW	R2, #lo_addr(_exec_circle+0)
MOVT	R2, #hi_addr(_exec_circle+0)
LDR	R2, [R2, #0]
ADDS	R2, #17
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up270
;BPWasher_driver.c,3844 :: 		if (exec_circle->OnUpPtr != 0)
MOVW	R4, #lo_addr(_exec_circle+0)
MOVT	R4, #hi_addr(_exec_circle+0)
LDR	R4, [R4, #0]
ADDS	R4, #32
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up271
;BPWasher_driver.c,3845 :: 		exec_circle->OnUpPtr();
MOVW	R4, #lo_addr(_exec_circle+0)
MOVT	R4, #hi_addr(_exec_circle+0)
LDR	R4, [R4, #0]
ADDS	R4, #32
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up271:
;BPWasher_driver.c,3846 :: 		if (PressedObject == (TPointer)exec_circle)
MOVW	R2, #lo_addr(_exec_circle+0)
MOVT	R2, #hi_addr(_exec_circle+0)
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up272
;BPWasher_driver.c,3847 :: 		if (exec_circle->OnClickPtr != 0)
MOVW	R4, #lo_addr(_exec_circle+0)
MOVT	R4, #hi_addr(_exec_circle+0)
LDR	R4, [R4, #0]
ADDS	R4, #40
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up273
;BPWasher_driver.c,3848 :: 		exec_circle->OnClickPtr();
MOVW	R4, #lo_addr(_exec_circle+0)
MOVT	R4, #hi_addr(_exec_circle+0)
LDR	R4, [R4, #0]
ADDS	R4, #40
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up273:
L_Process_TP_Up272:
;BPWasher_driver.c,3849 :: 		PressedObject = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3850 :: 		PressedObjectType = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3851 :: 		return;
IT	AL
BAL	L_end_Process_TP_Up
;BPWasher_driver.c,3852 :: 		}
L_Process_TP_Up270:
;BPWasher_driver.c,3853 :: 		}
L_Process_TP_Up269:
;BPWasher_driver.c,3856 :: 		if (_object_count == box_order) {
MOVW	R2, #lo_addr(_box_order+0)
MOVT	R2, #hi_addr(_box_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up274
;BPWasher_driver.c,3857 :: 		if (exec_box->Active != 0) {
MOVW	R2, #lo_addr(_exec_box+0)
MOVT	R2, #hi_addr(_exec_box+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up275
;BPWasher_driver.c,3858 :: 		if (exec_box->OnUpPtr != 0)
MOVW	R4, #lo_addr(_exec_box+0)
MOVT	R4, #hi_addr(_exec_box+0)
LDR	R4, [R4, #0]
ADDS	R4, #36
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up276
;BPWasher_driver.c,3859 :: 		exec_box->OnUpPtr();
MOVW	R4, #lo_addr(_exec_box+0)
MOVT	R4, #hi_addr(_exec_box+0)
LDR	R4, [R4, #0]
ADDS	R4, #36
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up276:
;BPWasher_driver.c,3860 :: 		if (PressedObject == (TPointer)exec_box)
MOVW	R2, #lo_addr(_exec_box+0)
MOVT	R2, #hi_addr(_exec_box+0)
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up277
;BPWasher_driver.c,3861 :: 		if (exec_box->OnClickPtr != 0)
MOVW	R4, #lo_addr(_exec_box+0)
MOVT	R4, #hi_addr(_exec_box+0)
LDR	R4, [R4, #0]
ADDS	R4, #44
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up278
;BPWasher_driver.c,3862 :: 		exec_box->OnClickPtr();
MOVW	R4, #lo_addr(_exec_box+0)
MOVT	R4, #hi_addr(_exec_box+0)
LDR	R4, [R4, #0]
ADDS	R4, #44
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up278:
L_Process_TP_Up277:
;BPWasher_driver.c,3863 :: 		PressedObject = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3864 :: 		PressedObjectType = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3865 :: 		return;
IT	AL
BAL	L_end_Process_TP_Up
;BPWasher_driver.c,3866 :: 		}
L_Process_TP_Up275:
;BPWasher_driver.c,3867 :: 		}
L_Process_TP_Up274:
;BPWasher_driver.c,3870 :: 		if (_object_count == checkbox_order) {
MOVW	R2, #lo_addr(_checkbox_order+0)
MOVT	R2, #hi_addr(_checkbox_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up279
;BPWasher_driver.c,3871 :: 		if (exec_checkbox->Active != 0) {
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up280
;BPWasher_driver.c,3872 :: 		if (exec_checkbox->OnUpPtr != 0)
MOVW	R4, #lo_addr(_exec_checkbox+0)
MOVT	R4, #hi_addr(_exec_checkbox+0)
LDR	R4, [R4, #0]
ADDS	R4, #52
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Up281
;BPWasher_driver.c,3873 :: 		exec_checkbox->OnUpPtr();
MOVW	R4, #lo_addr(_exec_checkbox+0)
MOVT	R4, #hi_addr(_exec_checkbox+0)
LDR	R4, [R4, #0]
ADDS	R4, #52
LDR	R2, [R4, #0]
BLX	R2
L_Process_TP_Up281:
;BPWasher_driver.c,3874 :: 		if (PressedObject == (TPointer)exec_checkbox) {
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
LDR	R3, [R2, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
LDR	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Up282
;BPWasher_driver.c,3875 :: 		if (exec_checkbox->Checked != 0)
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
LDR	R2, [R2, #0]
ADDS	R2, #20
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up283
;BPWasher_driver.c,3876 :: 		exec_checkbox->Checked = 0;
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
LDR	R2, [R2, #0]
ADDW	R3, R2, #20
MOVS	R2, #0
STRB	R2, [R3, #0]
IT	AL
BAL	L_Process_TP_Up284
L_Process_TP_Up283:
;BPWasher_driver.c,3878 :: 		exec_checkbox->Checked = 1;
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
LDR	R2, [R2, #0]
ADDW	R3, R2, #20
MOVS	R2, #1
STRB	R2, [R3, #0]
L_Process_TP_Up284:
;BPWasher_driver.c,3879 :: 		DrawCheckBox(exec_checkbox);
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
LDR	R2, [R2, #0]
MOV	R0, R2
BL	_DrawCheckBox+0
;BPWasher_driver.c,3880 :: 		if (exec_checkbox->OnClickPtr != 0)
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
LDR	R2, [R2, #0]
ADDS	R2, #60
LDR	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Up285
;BPWasher_driver.c,3881 :: 		exec_checkbox->OnClickPtr(exec_checkbox->Bit, exec_checkbox->Checked);
MOVW	R5, #lo_addr(_exec_checkbox+0)
MOVT	R5, #hi_addr(_exec_checkbox+0)
LDR	R2, [R5, #0]
ADDS	R2, #60
LDR	R4, [R2, #0]
MOV	R2, R5
LDR	R2, [R2, #0]
ADDS	R2, #20
LDRB	R2, [R2, #0]
UXTB	R3, R2
MOV	R2, R5
LDR	R2, [R2, #0]
ADDS	R2, #21
LDRB	R2, [R2, #0]
UXTB	R1, R3
UXTB	R0, R2
BLX	R4
L_Process_TP_Up285:
;BPWasher_driver.c,3882 :: 		}
L_Process_TP_Up282:
;BPWasher_driver.c,3883 :: 		PressedObject = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3884 :: 		PressedObjectType = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3885 :: 		return;
IT	AL
BAL	L_end_Process_TP_Up
;BPWasher_driver.c,3886 :: 		}
L_Process_TP_Up280:
;BPWasher_driver.c,3887 :: 		}
L_Process_TP_Up279:
;BPWasher_driver.c,3889 :: 		}
L_Process_TP_Up238:
;BPWasher_driver.c,3890 :: 		PressedObject = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3891 :: 		PressedObjectType = -1;
MOVW	R3, #65535
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3892 :: 		}
L_end_Process_TP_Up:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _Process_TP_Up
_Process_TP_Down:
;BPWasher_driver.c,3894 :: 		void Process_TP_Down(unsigned int X, unsigned int Y) {
; Y start address is: 4 (R1)
; X start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; Y end address is: 4 (R1)
; X end address is: 0 (R0)
; X start address is: 0 (R0)
; Y start address is: 4 (R1)
;BPWasher_driver.c,3896 :: 		object_pressed      = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_object_pressed+0)
MOVT	R2, #hi_addr(_object_pressed+0)
STRB	R3, [R2, #0]
;BPWasher_driver.c,3897 :: 		exec_button         = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_button+0)
MOVT	R2, #hi_addr(_exec_button+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3898 :: 		exec_round_button   = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_round_button+0)
MOVT	R2, #hi_addr(_exec_round_button+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3899 :: 		exec_round_cbutton  = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_round_cbutton+0)
MOVT	R2, #hi_addr(_exec_round_cbutton+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3900 :: 		exec_label          = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_label+0)
MOVT	R2, #hi_addr(_exec_label+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3901 :: 		exec_image          = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_image+0)
MOVT	R2, #hi_addr(_exec_image+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3902 :: 		exec_cimage         = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_cimage+0)
MOVT	R2, #hi_addr(_exec_cimage+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3903 :: 		exec_circle         = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_circle+0)
MOVT	R2, #hi_addr(_exec_circle+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3904 :: 		exec_box            = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_box+0)
MOVT	R2, #hi_addr(_exec_box+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3905 :: 		exec_checkbox       = 0;
MOVS	R3, #0
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3907 :: 		Get_Object(X, Y);
; Y end address is: 4 (R1)
; X end address is: 0 (R0)
BL	_Get_Object+0
;BPWasher_driver.c,3909 :: 		if (_object_count != -1) {
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, #-1
IT	EQ
BEQ	L_Process_TP_Down286
;BPWasher_driver.c,3910 :: 		if (_object_count == button_order) {
MOVW	R2, #lo_addr(_button_order+0)
MOVT	R2, #hi_addr(_button_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Down287
;BPWasher_driver.c,3911 :: 		if (exec_button->Active != 0) {
MOVW	R2, #lo_addr(_exec_button+0)
MOVT	R2, #hi_addr(_exec_button+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down288
;BPWasher_driver.c,3912 :: 		if (exec_button->PressColEnabled != 0) {
MOVW	R2, #lo_addr(_exec_button+0)
MOVT	R2, #hi_addr(_exec_button+0)
LDR	R2, [R2, #0]
ADDS	R2, #48
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down289
;BPWasher_driver.c,3913 :: 		object_pressed = 1;
MOVS	R3, #1
MOVW	R2, #lo_addr(_object_pressed+0)
MOVT	R2, #hi_addr(_object_pressed+0)
STRB	R3, [R2, #0]
;BPWasher_driver.c,3914 :: 		DrawButton(exec_button);
MOVW	R2, #lo_addr(_exec_button+0)
MOVT	R2, #hi_addr(_exec_button+0)
LDR	R2, [R2, #0]
MOV	R0, R2
BL	_DrawButton+0
;BPWasher_driver.c,3915 :: 		}
L_Process_TP_Down289:
;BPWasher_driver.c,3916 :: 		PressedObject = (TPointer)exec_button;
MOVW	R4, #lo_addr(_exec_button+0)
MOVT	R4, #hi_addr(_exec_button+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3917 :: 		PressedObjectType = 0;
MOVS	R3, #0
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3918 :: 		if (exec_button->OnDownPtr != 0) {
LDR	R4, [R4, #0]
ADDS	R4, #56
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Down290
;BPWasher_driver.c,3919 :: 		exec_button->OnDownPtr();
MOVW	R4, #lo_addr(_exec_button+0)
MOVT	R4, #hi_addr(_exec_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #56
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3920 :: 		return;
IT	AL
BAL	L_end_Process_TP_Down
;BPWasher_driver.c,3921 :: 		}
L_Process_TP_Down290:
;BPWasher_driver.c,3922 :: 		}
L_Process_TP_Down288:
;BPWasher_driver.c,3923 :: 		}
L_Process_TP_Down287:
;BPWasher_driver.c,3925 :: 		if (_object_count == round_button_order) {
MOVW	R2, #lo_addr(_round_button_order+0)
MOVT	R2, #hi_addr(_round_button_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Down291
;BPWasher_driver.c,3926 :: 		if (exec_round_button->Active != 0) {
MOVW	R2, #lo_addr(_exec_round_button+0)
MOVT	R2, #hi_addr(_exec_round_button+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down292
;BPWasher_driver.c,3927 :: 		if (exec_round_button->PressColEnabled != 0) {
MOVW	R2, #lo_addr(_exec_round_button+0)
MOVT	R2, #hi_addr(_exec_round_button+0)
LDR	R2, [R2, #0]
ADDS	R2, #49
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down293
;BPWasher_driver.c,3928 :: 		object_pressed = 1;
MOVS	R3, #1
MOVW	R2, #lo_addr(_object_pressed+0)
MOVT	R2, #hi_addr(_object_pressed+0)
STRB	R3, [R2, #0]
;BPWasher_driver.c,3929 :: 		DrawRoundButton(exec_round_button);
MOVW	R2, #lo_addr(_exec_round_button+0)
MOVT	R2, #hi_addr(_exec_round_button+0)
LDR	R2, [R2, #0]
MOV	R0, R2
BL	_DrawRoundButton+0
;BPWasher_driver.c,3930 :: 		}
L_Process_TP_Down293:
;BPWasher_driver.c,3931 :: 		PressedObject = (TPointer)exec_round_button;
MOVW	R4, #lo_addr(_exec_round_button+0)
MOVT	R4, #hi_addr(_exec_round_button+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3932 :: 		PressedObjectType = 1;
MOVS	R3, #1
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3933 :: 		if (exec_round_button->OnDownPtr != 0) {
LDR	R4, [R4, #0]
ADDS	R4, #56
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Down294
;BPWasher_driver.c,3934 :: 		exec_round_button->OnDownPtr();
MOVW	R4, #lo_addr(_exec_round_button+0)
MOVT	R4, #hi_addr(_exec_round_button+0)
LDR	R4, [R4, #0]
ADDS	R4, #56
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3935 :: 		return;
IT	AL
BAL	L_end_Process_TP_Down
;BPWasher_driver.c,3936 :: 		}
L_Process_TP_Down294:
;BPWasher_driver.c,3937 :: 		}
L_Process_TP_Down292:
;BPWasher_driver.c,3938 :: 		}
L_Process_TP_Down291:
;BPWasher_driver.c,3940 :: 		if (_object_count == round_cbutton_order) {
MOVW	R2, #lo_addr(_round_cbutton_order+0)
MOVT	R2, #hi_addr(_round_cbutton_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Down295
;BPWasher_driver.c,3941 :: 		if (exec_round_cbutton->Active != 0) {
MOVW	R2, #lo_addr(_exec_round_cbutton+0)
MOVT	R2, #hi_addr(_exec_round_cbutton+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down296
;BPWasher_driver.c,3942 :: 		if (exec_round_cbutton->PressColEnabled != 0) {
MOVW	R2, #lo_addr(_exec_round_cbutton+0)
MOVT	R2, #hi_addr(_exec_round_cbutton+0)
LDR	R2, [R2, #0]
ADDS	R2, #49
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down297
;BPWasher_driver.c,3943 :: 		object_pressed = 1;
MOVS	R3, #1
MOVW	R2, #lo_addr(_object_pressed+0)
MOVT	R2, #hi_addr(_object_pressed+0)
STRB	R3, [R2, #0]
;BPWasher_driver.c,3944 :: 		DrawCRoundButton(exec_round_cbutton);
MOVW	R2, #lo_addr(_exec_round_cbutton+0)
MOVT	R2, #hi_addr(_exec_round_cbutton+0)
LDR	R2, [R2, #0]
MOV	R0, R2
BL	_DrawCRoundButton+0
;BPWasher_driver.c,3945 :: 		}
L_Process_TP_Down297:
;BPWasher_driver.c,3946 :: 		PressedObject = (TPointer)exec_round_cbutton;
MOVW	R4, #lo_addr(_exec_round_cbutton+0)
MOVT	R4, #hi_addr(_exec_round_cbutton+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3947 :: 		PressedObjectType = 9;
MOVS	R3, #9
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3948 :: 		if (exec_round_cbutton->OnDownPtr != 0) {
LDR	R4, [R4, #0]
ADDS	R4, #56
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Down298
;BPWasher_driver.c,3949 :: 		exec_round_cbutton->OnDownPtr();
MOVW	R4, #lo_addr(_exec_round_cbutton+0)
MOVT	R4, #hi_addr(_exec_round_cbutton+0)
LDR	R4, [R4, #0]
ADDS	R4, #56
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3950 :: 		return;
IT	AL
BAL	L_end_Process_TP_Down
;BPWasher_driver.c,3951 :: 		}
L_Process_TP_Down298:
;BPWasher_driver.c,3952 :: 		}
L_Process_TP_Down296:
;BPWasher_driver.c,3953 :: 		}
L_Process_TP_Down295:
;BPWasher_driver.c,3955 :: 		if (_object_count == label_order) {
MOVW	R2, #lo_addr(_label_order+0)
MOVT	R2, #hi_addr(_label_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Down299
;BPWasher_driver.c,3956 :: 		if (exec_label->Active != 0) {
MOVW	R2, #lo_addr(_exec_label+0)
MOVT	R2, #hi_addr(_exec_label+0)
LDR	R2, [R2, #0]
ADDS	R2, #28
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down300
;BPWasher_driver.c,3957 :: 		PressedObject = (TPointer)exec_label;
MOVW	R4, #lo_addr(_exec_label+0)
MOVT	R4, #hi_addr(_exec_label+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3958 :: 		PressedObjectType = 2;
MOVS	R3, #2
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3959 :: 		if (exec_label->OnDownPtr != 0) {
LDR	R4, [R4, #0]
ADDS	R4, #36
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Down301
;BPWasher_driver.c,3960 :: 		exec_label->OnDownPtr();
MOVW	R4, #lo_addr(_exec_label+0)
MOVT	R4, #hi_addr(_exec_label+0)
LDR	R4, [R4, #0]
ADDS	R4, #36
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3961 :: 		return;
IT	AL
BAL	L_end_Process_TP_Down
;BPWasher_driver.c,3962 :: 		}
L_Process_TP_Down301:
;BPWasher_driver.c,3963 :: 		}
L_Process_TP_Down300:
;BPWasher_driver.c,3964 :: 		}
L_Process_TP_Down299:
;BPWasher_driver.c,3966 :: 		if (_object_count == image_order) {
MOVW	R2, #lo_addr(_image_order+0)
MOVT	R2, #hi_addr(_image_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Down302
;BPWasher_driver.c,3967 :: 		if (exec_image->Active != 0) {
MOVW	R2, #lo_addr(_exec_image+0)
MOVT	R2, #hi_addr(_exec_image+0)
LDR	R2, [R2, #0]
ADDS	R2, #21
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down303
;BPWasher_driver.c,3968 :: 		PressedObject = (TPointer)exec_image;
MOVW	R4, #lo_addr(_exec_image+0)
MOVT	R4, #hi_addr(_exec_image+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3969 :: 		PressedObjectType = 3;
MOVS	R3, #3
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3970 :: 		if (exec_image->OnDownPtr != 0) {
LDR	R4, [R4, #0]
ADDS	R4, #28
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Down304
;BPWasher_driver.c,3971 :: 		exec_image->OnDownPtr();
MOVW	R4, #lo_addr(_exec_image+0)
MOVT	R4, #hi_addr(_exec_image+0)
LDR	R4, [R4, #0]
ADDS	R4, #28
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3972 :: 		return;
IT	AL
BAL	L_end_Process_TP_Down
;BPWasher_driver.c,3973 :: 		}
L_Process_TP_Down304:
;BPWasher_driver.c,3974 :: 		}
L_Process_TP_Down303:
;BPWasher_driver.c,3975 :: 		}
L_Process_TP_Down302:
;BPWasher_driver.c,3977 :: 		if (_object_count == cimage_order) {
MOVW	R2, #lo_addr(_cimage_order+0)
MOVT	R2, #hi_addr(_cimage_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Down305
;BPWasher_driver.c,3978 :: 		if (exec_cimage->Active != 0) {
MOVW	R2, #lo_addr(_exec_cimage+0)
MOVT	R2, #hi_addr(_exec_cimage+0)
LDR	R2, [R2, #0]
ADDS	R2, #21
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down306
;BPWasher_driver.c,3979 :: 		PressedObject = (TPointer)exec_cimage;
MOVW	R4, #lo_addr(_exec_cimage+0)
MOVT	R4, #hi_addr(_exec_cimage+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3980 :: 		PressedObjectType = 11;
MOVS	R3, #11
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3981 :: 		if (exec_cimage->OnDownPtr != 0) {
LDR	R4, [R4, #0]
ADDS	R4, #28
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Down307
;BPWasher_driver.c,3982 :: 		exec_cimage->OnDownPtr();
MOVW	R4, #lo_addr(_exec_cimage+0)
MOVT	R4, #hi_addr(_exec_cimage+0)
LDR	R4, [R4, #0]
ADDS	R4, #28
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3983 :: 		return;
IT	AL
BAL	L_end_Process_TP_Down
;BPWasher_driver.c,3984 :: 		}
L_Process_TP_Down307:
;BPWasher_driver.c,3985 :: 		}
L_Process_TP_Down306:
;BPWasher_driver.c,3986 :: 		}
L_Process_TP_Down305:
;BPWasher_driver.c,3988 :: 		if (_object_count == circle_order) {
MOVW	R2, #lo_addr(_circle_order+0)
MOVT	R2, #hi_addr(_circle_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Down308
;BPWasher_driver.c,3989 :: 		if (exec_circle->Active != 0) {
MOVW	R2, #lo_addr(_exec_circle+0)
MOVT	R2, #hi_addr(_exec_circle+0)
LDR	R2, [R2, #0]
ADDS	R2, #17
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down309
;BPWasher_driver.c,3990 :: 		if (exec_circle->PressColEnabled != 0) {
MOVW	R2, #lo_addr(_exec_circle+0)
MOVT	R2, #hi_addr(_exec_circle+0)
LDR	R2, [R2, #0]
ADDS	R2, #28
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down310
;BPWasher_driver.c,3991 :: 		object_pressed = 1;
MOVS	R3, #1
MOVW	R2, #lo_addr(_object_pressed+0)
MOVT	R2, #hi_addr(_object_pressed+0)
STRB	R3, [R2, #0]
;BPWasher_driver.c,3992 :: 		DrawCircle(exec_circle);
MOVW	R2, #lo_addr(_exec_circle+0)
MOVT	R2, #hi_addr(_exec_circle+0)
LDR	R2, [R2, #0]
MOV	R0, R2
BL	_DrawCircle+0
;BPWasher_driver.c,3993 :: 		}
L_Process_TP_Down310:
;BPWasher_driver.c,3994 :: 		PressedObject = (TPointer)exec_circle;
MOVW	R4, #lo_addr(_exec_circle+0)
MOVT	R4, #hi_addr(_exec_circle+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,3995 :: 		PressedObjectType = 4;
MOVS	R3, #4
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,3996 :: 		if (exec_circle->OnDownPtr != 0) {
LDR	R4, [R4, #0]
ADDS	R4, #36
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Down311
;BPWasher_driver.c,3997 :: 		exec_circle->OnDownPtr();
MOVW	R4, #lo_addr(_exec_circle+0)
MOVT	R4, #hi_addr(_exec_circle+0)
LDR	R4, [R4, #0]
ADDS	R4, #36
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,3998 :: 		return;
IT	AL
BAL	L_end_Process_TP_Down
;BPWasher_driver.c,3999 :: 		}
L_Process_TP_Down311:
;BPWasher_driver.c,4000 :: 		}
L_Process_TP_Down309:
;BPWasher_driver.c,4001 :: 		}
L_Process_TP_Down308:
;BPWasher_driver.c,4003 :: 		if (_object_count == box_order) {
MOVW	R2, #lo_addr(_box_order+0)
MOVT	R2, #hi_addr(_box_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Down312
;BPWasher_driver.c,4004 :: 		if (exec_box->Active != 0) {
MOVW	R2, #lo_addr(_exec_box+0)
MOVT	R2, #hi_addr(_exec_box+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down313
;BPWasher_driver.c,4005 :: 		if (exec_box->PressColEnabled != 0) {
MOVW	R2, #lo_addr(_exec_box+0)
MOVT	R2, #hi_addr(_exec_box+0)
LDR	R2, [R2, #0]
ADDS	R2, #30
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down314
;BPWasher_driver.c,4006 :: 		object_pressed = 1;
MOVS	R3, #1
MOVW	R2, #lo_addr(_object_pressed+0)
MOVT	R2, #hi_addr(_object_pressed+0)
STRB	R3, [R2, #0]
;BPWasher_driver.c,4007 :: 		DrawBox(exec_box);
MOVW	R2, #lo_addr(_exec_box+0)
MOVT	R2, #hi_addr(_exec_box+0)
LDR	R2, [R2, #0]
MOV	R0, R2
BL	_DrawBox+0
;BPWasher_driver.c,4008 :: 		}
L_Process_TP_Down314:
;BPWasher_driver.c,4009 :: 		PressedObject = (TPointer)exec_box;
MOVW	R4, #lo_addr(_exec_box+0)
MOVT	R4, #hi_addr(_exec_box+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,4010 :: 		PressedObjectType = 6;
MOVS	R3, #6
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,4011 :: 		if (exec_box->OnDownPtr != 0) {
LDR	R4, [R4, #0]
ADDS	R4, #40
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Down315
;BPWasher_driver.c,4012 :: 		exec_box->OnDownPtr();
MOVW	R4, #lo_addr(_exec_box+0)
MOVT	R4, #hi_addr(_exec_box+0)
LDR	R4, [R4, #0]
ADDS	R4, #40
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,4013 :: 		return;
IT	AL
BAL	L_end_Process_TP_Down
;BPWasher_driver.c,4014 :: 		}
L_Process_TP_Down315:
;BPWasher_driver.c,4015 :: 		}
L_Process_TP_Down313:
;BPWasher_driver.c,4016 :: 		}
L_Process_TP_Down312:
;BPWasher_driver.c,4018 :: 		if (_object_count == checkbox_order) {
MOVW	R2, #lo_addr(_checkbox_order+0)
MOVT	R2, #hi_addr(_checkbox_order+0)
LDRSH	R3, [R2, #0]
MOVW	R2, #lo_addr(__object_count+0)
MOVT	R2, #hi_addr(__object_count+0)
LDRSH	R2, [R2, #0]
CMP	R2, R3
IT	NE
BNE	L_Process_TP_Down316
;BPWasher_driver.c,4019 :: 		if (exec_checkbox->Active != 0) {
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
LDR	R2, [R2, #0]
ADDS	R2, #19
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down317
;BPWasher_driver.c,4020 :: 		if (exec_checkbox->PressColEnabled != 0) {
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
LDR	R2, [R2, #0]
ADDS	R2, #48
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_Process_TP_Down318
;BPWasher_driver.c,4021 :: 		object_pressed = 1;
MOVS	R3, #1
MOVW	R2, #lo_addr(_object_pressed+0)
MOVT	R2, #hi_addr(_object_pressed+0)
STRB	R3, [R2, #0]
;BPWasher_driver.c,4022 :: 		DrawCheckBox(exec_checkbox);
MOVW	R2, #lo_addr(_exec_checkbox+0)
MOVT	R2, #hi_addr(_exec_checkbox+0)
LDR	R2, [R2, #0]
MOV	R0, R2
BL	_DrawCheckBox+0
;BPWasher_driver.c,4023 :: 		}
L_Process_TP_Down318:
;BPWasher_driver.c,4024 :: 		PressedObject = (TPointer)exec_checkbox;
MOVW	R4, #lo_addr(_exec_checkbox+0)
MOVT	R4, #hi_addr(_exec_checkbox+0)
LDR	R3, [R4, #0]
MOVW	R2, #lo_addr(_PressedObject+0)
MOVT	R2, #hi_addr(_PressedObject+0)
STR	R3, [R2, #0]
;BPWasher_driver.c,4025 :: 		PressedObjectType = 16;
MOVS	R3, #16
SXTH	R3, R3
MOVW	R2, #lo_addr(_PressedObjectType+0)
MOVT	R2, #hi_addr(_PressedObjectType+0)
STRH	R3, [R2, #0]
;BPWasher_driver.c,4026 :: 		if (exec_checkbox->OnDownPtr != 0) {
LDR	R4, [R4, #0]
ADDS	R4, #56
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_Process_TP_Down319
;BPWasher_driver.c,4027 :: 		exec_checkbox->OnDownPtr();
MOVW	R4, #lo_addr(_exec_checkbox+0)
MOVT	R4, #hi_addr(_exec_checkbox+0)
LDR	R4, [R4, #0]
ADDS	R4, #56
LDR	R2, [R4, #0]
BLX	R2
;BPWasher_driver.c,4028 :: 		return;
IT	AL
BAL	L_end_Process_TP_Down
;BPWasher_driver.c,4029 :: 		}
L_Process_TP_Down319:
;BPWasher_driver.c,4030 :: 		}
L_Process_TP_Down317:
;BPWasher_driver.c,4031 :: 		}
L_Process_TP_Down316:
;BPWasher_driver.c,4033 :: 		}
L_Process_TP_Down286:
;BPWasher_driver.c,4034 :: 		}
L_end_Process_TP_Down:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Process_TP_Down
_Check_TP:
;BPWasher_driver.c,4036 :: 		void Check_TP() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_driver.c,4037 :: 		if (TP_TFT_Press_Detect()) {
BL	_TP_TFT_Press_Detect+0
CMP	R0, #0
IT	EQ
BEQ	L_Check_TP320
;BPWasher_driver.c,4038 :: 		if (TP_TFT_Get_Coordinates(&Xcoord, &Ycoord) == 0) {
MOVW	R1, #lo_addr(_Ycoord+0)
MOVT	R1, #hi_addr(_Ycoord+0)
MOVW	R0, #lo_addr(_Xcoord+0)
MOVT	R0, #hi_addr(_Xcoord+0)
BL	_TP_TFT_Get_Coordinates+0
CMP	R0, #0
IT	NE
BNE	L_Check_TP321
;BPWasher_driver.c,4040 :: 		Process_TP_Press(Xcoord, Ycoord);
MOVW	R0, #lo_addr(_Ycoord+0)
MOVT	R0, #hi_addr(_Ycoord+0)
LDRH	R1, [R0, #0]
MOVW	R0, #lo_addr(_Xcoord+0)
MOVT	R0, #hi_addr(_Xcoord+0)
LDRH	R0, [R0, #0]
BL	_Process_TP_Press+0
;BPWasher_driver.c,4041 :: 		if (PenDown == 0) {
MOVW	R0, #lo_addr(_PenDown+0)
MOVT	R0, #hi_addr(_PenDown+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	NE
BNE	L_Check_TP322
;BPWasher_driver.c,4042 :: 		PenDown = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_PenDown+0)
MOVT	R0, #hi_addr(_PenDown+0)
STRB	R1, [R0, #0]
;BPWasher_driver.c,4043 :: 		Process_TP_Down(Xcoord, Ycoord);
MOVW	R0, #lo_addr(_Ycoord+0)
MOVT	R0, #hi_addr(_Ycoord+0)
LDRH	R1, [R0, #0]
MOVW	R0, #lo_addr(_Xcoord+0)
MOVT	R0, #hi_addr(_Xcoord+0)
LDRH	R0, [R0, #0]
BL	_Process_TP_Down+0
;BPWasher_driver.c,4044 :: 		}
L_Check_TP322:
;BPWasher_driver.c,4045 :: 		}
L_Check_TP321:
;BPWasher_driver.c,4046 :: 		}
IT	AL
BAL	L_Check_TP323
L_Check_TP320:
;BPWasher_driver.c,4047 :: 		else if (PenDown == 1) {
MOVW	R0, #lo_addr(_PenDown+0)
MOVT	R0, #hi_addr(_PenDown+0)
LDRB	R0, [R0, #0]
CMP	R0, #1
IT	NE
BNE	L_Check_TP324
;BPWasher_driver.c,4048 :: 		PenDown = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_PenDown+0)
MOVT	R0, #hi_addr(_PenDown+0)
STRB	R1, [R0, #0]
;BPWasher_driver.c,4049 :: 		Process_TP_Up(Xcoord, Ycoord);
MOVW	R0, #lo_addr(_Ycoord+0)
MOVT	R0, #hi_addr(_Ycoord+0)
LDRH	R1, [R0, #0]
MOVW	R0, #lo_addr(_Xcoord+0)
MOVT	R0, #hi_addr(_Xcoord+0)
LDRH	R0, [R0, #0]
BL	_Process_TP_Up+0
;BPWasher_driver.c,4050 :: 		}
L_Check_TP324:
L_Check_TP323:
;BPWasher_driver.c,4051 :: 		}
L_end_Check_TP:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Check_TP
_Init_MCU:
;BPWasher_driver.c,4053 :: 		void Init_MCU() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_driver.c,4054 :: 		GPIO_Digital_Output(&GPIOE_BASE, _GPIO_PINMASK_9);
MOVW	R1, #512
MOVW	R0, #lo_addr(GPIOE_BASE+0)
MOVT	R0, #hi_addr(GPIOE_BASE+0)
BL	_GPIO_Digital_Output+0
;BPWasher_driver.c,4055 :: 		TFT_BLED = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
;BPWasher_driver.c,4056 :: 		TFT_Set_Default_Mode();
BL	_TFT_Set_Default_Mode+0
;BPWasher_driver.c,4057 :: 		TP_TFT_Set_Default_Mode();
BL	_TP_TFT_Set_Default_Mode+0
;BPWasher_driver.c,4058 :: 		}
L_end_Init_MCU:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Init_MCU
_InitializeErrorLogScreen:
;BPWasher_driver.c,4061 :: 		void InitializeErrorLogScreen()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_driver.c,4070 :: 		Log_Labels[0].Caption = &sErrorLog;
MOVW	R1, #lo_addr(_sErrorLog+0)
MOVT	R1, #hi_addr(_sErrorLog+0)
MOVW	R0, #lo_addr(_Log_Labels+16)
MOVT	R0, #hi_addr(_Log_Labels+16)
STR	R1, [R0, #0]
;BPWasher_driver.c,4074 :: 		for (iErrorLogCounter = 1; iErrorLogCounter < ERRLOG_LINE_N; iErrorLogCounter++)
; iErrorLogCounter start address is: 24 (R6)
MOVS	R6, #1
SXTH	R6, R6
; iErrorLogCounter end address is: 24 (R6)
L_InitializeErrorLogScreen325:
; iErrorLogCounter start address is: 24 (R6)
CMP	R6, #10
IT	GE
BGE	L_InitializeErrorLogScreen326
;BPWasher_driver.c,4077 :: 		memcpy(&Log_Labels[iErrorLogCounter], &Log_Labels[iErrorLogCounter-1], sizeof(TLabel));
SUBS	R1, R6, #1
SXTH	R1, R1
MOVS	R0, #48
MULS	R1, R0, R1
MOVW	R0, #lo_addr(_Log_Labels+0)
MOVT	R0, #hi_addr(_Log_Labels+0)
ADDS	R2, R0, R1
MOVS	R0, #48
MUL	R1, R0, R6
MOVW	R0, #lo_addr(_Log_Labels+0)
MOVT	R0, #hi_addr(_Log_Labels+0)
ADDS	R0, R0, R1
MOV	R1, R2
MOVS	R2, #48
SXTH	R2, R2
BL	_memcpy+0
;BPWasher_driver.c,4078 :: 		Log_Labels[iErrorLogCounter].Order++;
MOVS	R0, #48
MUL	R1, R0, R6
MOVW	R0, #lo_addr(_Log_Labels+0)
MOVT	R0, #hi_addr(_Log_Labels+0)
ADDS	R0, R0, R1
ADDS	R1, R0, #4
LDRB	R0, [R1, #0]
ADDS	R0, R0, #1
STRB	R0, [R1, #0]
;BPWasher_driver.c,4079 :: 		Log_Labels[iErrorLogCounter].Top += 17;
MOVS	R0, #48
MUL	R1, R0, R6
MOVW	R0, #lo_addr(_Log_Labels+0)
MOVT	R0, #hi_addr(_Log_Labels+0)
ADDS	R0, R0, R1
ADDW	R1, R0, #8
LDRH	R0, [R1, #0]
ADDS	R0, #17
STRH	R0, [R1, #0]
;BPWasher_driver.c,4080 :: 		Log_Labels[iErrorLogCounter].Caption += ERRLOG_LINE_LEN;
MOVS	R0, #48
MUL	R1, R0, R6
MOVW	R0, #lo_addr(_Log_Labels+0)
MOVT	R0, #hi_addr(_Log_Labels+0)
ADDS	R0, R0, R1
ADDW	R1, R0, #16
LDR	R0, [R1, #0]
ADDS	R0, #50
STR	R0, [R1, #0]
;BPWasher_driver.c,4074 :: 		for (iErrorLogCounter = 1; iErrorLogCounter < ERRLOG_LINE_N; iErrorLogCounter++)
ADDS	R6, R6, #1
SXTH	R6, R6
;BPWasher_driver.c,4082 :: 		}
; iErrorLogCounter end address is: 24 (R6)
IT	AL
BAL	L_InitializeErrorLogScreen325
L_InitializeErrorLogScreen326:
;BPWasher_driver.c,4083 :: 		}
L_end_InitializeErrorLogScreen:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _InitializeErrorLogScreen
_Start_TP:
;BPWasher_driver.c,4085 :: 		void Start_TP() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_driver.c,4086 :: 		Init_MCU();
BL	_Init_MCU+0
;BPWasher_driver.c,4088 :: 		InitializeTouchPanel();
BL	BPWasher_driver_InitializeTouchPanel+0
;BPWasher_driver.c,4091 :: 		TFT_Fill_Screen(0);
MOVS	R0, #0
BL	_TFT_Fill_Screen+0
;BPWasher_driver.c,4092 :: 		Calibrate();
BL	_Calibrate+0
;BPWasher_driver.c,4095 :: 		InitializeObjects();
BL	BPWasher_driver_InitializeObjects+0
;BPWasher_driver.c,4096 :: 		InitializeErrorLogScreen();
BL	_InitializeErrorLogScreen+0
;BPWasher_driver.c,4097 :: 		display_width = Diags.Width;
MOVW	R0, #lo_addr(_Diags+2)
MOVT	R0, #hi_addr(_Diags+2)
LDRH	R1, [R0, #0]
MOVW	R0, #lo_addr(_display_width+0)
MOVT	R0, #hi_addr(_display_width+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,4098 :: 		display_height = Diags.Height;
MOVW	R0, #lo_addr(_Diags+4)
MOVT	R0, #hi_addr(_Diags+4)
LDRH	R1, [R0, #0]
MOVW	R0, #lo_addr(_display_height+0)
MOVT	R0, #hi_addr(_display_height+0)
STRH	R1, [R0, #0]
;BPWasher_driver.c,4099 :: 		DrawScreen(&SplashLand);
MOVW	R0, #lo_addr(_SplashLand+0)
MOVT	R0, #hi_addr(_SplashLand+0)
BL	_DrawScreen+0
;BPWasher_driver.c,4100 :: 		}
L_end_Start_TP:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _Start_TP
BPWasher_driver____?ag:
SUB	SP, SP, #4
L_end_BPWasher_driver___?ag:
ADD	SP, SP, #4
BX	LR
; end of BPWasher_driver____?ag
