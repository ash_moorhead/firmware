_GetCurrentTemp:
;Heater.c,43 :: 		float GetCurrentTemp()
SUB	SP, SP, #4
;Heater.c,45 :: 		return  ActualTemp; // was LastActualTemp;
MOVW	R0, #lo_addr(_ActualTemp+0)
MOVT	R0, #hi_addr(_ActualTemp+0)
VLDR	#1, S0, [R0, #0]
;Heater.c,46 :: 		}
L_end_GetCurrentTemp:
ADD	SP, SP, #4
BX	LR
; end of _GetCurrentTemp
_GetDisplayTemp:
;Heater.c,53 :: 		float GetDisplayTemp()
SUB	SP, SP, #4
;Heater.c,55 :: 		return  fCurrentDisplay;
MOVW	R0, #lo_addr(_fCurrentDisplay+0)
MOVT	R0, #hi_addr(_fCurrentDisplay+0)
VLDR	#1, S0, [R0, #0]
;Heater.c,56 :: 		}
L_end_GetDisplayTemp:
ADD	SP, SP, #4
BX	LR
; end of _GetDisplayTemp
_AddTempError:
;Heater.c,68 :: 		void AddTempError(float TempError)
SUB	SP, SP, #4
; TempError start address is: 0 (R0)
; TempError end address is: 0 (R0)
; TempError start address is: 0 (R0)
;Heater.c,72 :: 		if (++nSeconds >= READING_INTERVAL)
MOVW	R2, #lo_addr(_nSeconds+0)
MOVT	R2, #hi_addr(_nSeconds+0)
LDRB	R1, [R2, #0]
ADDS	R1, R1, #1
UXTB	R1, R1
STRB	R1, [R2, #0]
CMP	R1, #5
IT	CC
BCC	L_AddTempError0
;Heater.c,74 :: 		for (i = NLASTREADINGS - 1; i > 0; i--)
; i start address is: 0 (R0)
MOVS	R0, #24
SXTH	R0, R0
; TempError end address is: 0 (R0)
; i end address is: 0 (R0)
VMOV.F32	S1, S0
L_AddTempError1:
; i start address is: 0 (R0)
; TempError start address is: 4 (R1)
CMP	R0, #0
IT	LE
BLE	L_AddTempError2
;Heater.c,75 :: 		LastReadings[i] = LastReadings[i - 1];
LSLS	R2, R0, #2
MOVW	R1, #lo_addr(_LastReadings+0)
MOVT	R1, #hi_addr(_LastReadings+0)
ADDS	R3, R1, R2
SUBS	R1, R0, #1
SXTH	R1, R1
LSLS	R2, R1, #2
MOVW	R1, #lo_addr(_LastReadings+0)
MOVT	R1, #hi_addr(_LastReadings+0)
ADDS	R1, R1, R2
VLDR	#1, S0, [R1, #0]
VSTR	#1, S0, [R3, #0]
;Heater.c,74 :: 		for (i = NLASTREADINGS - 1; i > 0; i--)
SUBS	R0, R0, #1
SXTH	R0, R0
;Heater.c,75 :: 		LastReadings[i] = LastReadings[i - 1];
; i end address is: 0 (R0)
IT	AL
BAL	L_AddTempError1
L_AddTempError2:
;Heater.c,77 :: 		LastReadings[0] = (TempError / READING_INTERVAL);
VMOV.F32	S0, #5
VDIV.F32	S0, S1, S0
; TempError end address is: 4 (R1)
MOVW	R1, #lo_addr(_LastReadings+0)
MOVT	R1, #hi_addr(_LastReadings+0)
VSTR	#1, S0, [R1, #0]
;Heater.c,79 :: 		nSeconds = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_nSeconds+0)
MOVT	R1, #hi_addr(_nSeconds+0)
STRB	R2, [R1, #0]
;Heater.c,80 :: 		if (++nRecent > NLASTREADINGS)
MOVW	R2, #lo_addr(_nRecent+0)
MOVT	R2, #hi_addr(_nRecent+0)
LDRSH	R1, [R2, #0]
ADDS	R1, R1, #1
SXTH	R1, R1
STRH	R1, [R2, #0]
CMP	R1, #25
IT	LE
BLE	L_AddTempError4
;Heater.c,81 :: 		nRecent = NLASTREADINGS;
MOVS	R2, #25
SXTH	R2, R2
MOVW	R1, #lo_addr(_nRecent+0)
MOVT	R1, #hi_addr(_nRecent+0)
STRH	R2, [R1, #0]
L_AddTempError4:
;Heater.c,82 :: 		}
L_AddTempError0:
;Heater.c,83 :: 		}
L_end_AddTempError:
ADD	SP, SP, #4
BX	LR
; end of _AddTempError
_DisplayTemp:
;Heater.c,93 :: 		void DisplayTemp(void)
SUB	SP, SP, #12
STR	LR, [SP, #0]
;Heater.c,95 :: 		char nIndex = 0;
;Heater.c,96 :: 		Total = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_Total+0)
MOVT	R0, #hi_addr(_Total+0)
VSTR	#1, S0, [R0, #0]
;Heater.c,98 :: 		if (nTempIndex < TEMP_ADV)
MOVW	R0, #lo_addr(_nTempIndex+0)
MOVT	R0, #hi_addr(_nTempIndex+0)
LDRSH	R0, [R0, #0]
CMP	R0, #15
IT	GE
BGE	L_DisplayTemp5
;Heater.c,100 :: 		nTempLedDisplay[nTempIndex] = (ActualTemp);
MOVW	R2, #lo_addr(_nTempIndex+0)
MOVT	R2, #hi_addr(_nTempIndex+0)
LDRSH	R0, [R2, #0]
LSLS	R1, R0, #2
MOVW	R0, #lo_addr(_nTempLedDisplay+0)
MOVT	R0, #hi_addr(_nTempLedDisplay+0)
ADDS	R1, R0, R1
MOVW	R0, #lo_addr(_ActualTemp+0)
MOVT	R0, #hi_addr(_ActualTemp+0)
VLDR	#1, S0, [R0, #0]
VSTR	#1, S0, [R1, #0]
;Heater.c,101 :: 		nTempIndex++;
MOV	R0, R2
LDRSH	R0, [R0, #0]
ADDS	R0, R0, #1
STRH	R0, [R2, #0]
;Heater.c,102 :: 		}
IT	AL
BAL	L_DisplayTemp6
L_DisplayTemp5:
;Heater.c,106 :: 		for (nIndex = 0; nIndex < TEMP_ADV-1; nIndex++)
; nIndex start address is: 12 (R3)
MOVS	R3, #0
; nIndex end address is: 12 (R3)
L_DisplayTemp7:
; nIndex start address is: 12 (R3)
CMP	R3, #14
IT	CS
BCS	L_DisplayTemp8
;Heater.c,107 :: 		nTempLedDisplay[nIndex] = nTempLedDisplay[nIndex + 1];
LSLS	R1, R3, #2
MOVW	R0, #lo_addr(_nTempLedDisplay+0)
MOVT	R0, #hi_addr(_nTempLedDisplay+0)
ADDS	R2, R0, R1
ADDS	R0, R3, #1
SXTH	R0, R0
LSLS	R1, R0, #2
MOVW	R0, #lo_addr(_nTempLedDisplay+0)
MOVT	R0, #hi_addr(_nTempLedDisplay+0)
ADDS	R0, R0, R1
VLDR	#1, S0, [R0, #0]
VSTR	#1, S0, [R2, #0]
;Heater.c,106 :: 		for (nIndex = 0; nIndex < TEMP_ADV-1; nIndex++)
ADDS	R3, R3, #1
UXTB	R3, R3
;Heater.c,107 :: 		nTempLedDisplay[nIndex] = nTempLedDisplay[nIndex + 1];
; nIndex end address is: 12 (R3)
IT	AL
BAL	L_DisplayTemp7
L_DisplayTemp8:
;Heater.c,109 :: 		nTempLedDisplay[TEMP_ADV-1]  = (ActualTemp);
MOVW	R0, #lo_addr(_ActualTemp+0)
MOVT	R0, #hi_addr(_ActualTemp+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_nTempLedDisplay+56)
MOVT	R0, #hi_addr(_nTempLedDisplay+56)
VSTR	#1, S0, [R0, #0]
;Heater.c,110 :: 		}
L_DisplayTemp6:
;Heater.c,112 :: 		for (nIndex = 0; nIndex < (nTempIndex); nIndex++)
; nIndex start address is: 8 (R2)
MOVS	R2, #0
; nIndex end address is: 8 (R2)
L_DisplayTemp10:
; nIndex start address is: 8 (R2)
MOVW	R0, #lo_addr(_nTempIndex+0)
MOVT	R0, #hi_addr(_nTempIndex+0)
LDRSH	R0, [R0, #0]
CMP	R2, R0
IT	GE
BGE	L_DisplayTemp11
;Heater.c,114 :: 		Total +=  nTempLedDisplay[nIndex] ;
LSLS	R1, R2, #2
MOVW	R0, #lo_addr(_nTempLedDisplay+0)
MOVT	R0, #hi_addr(_nTempLedDisplay+0)
ADDS	R0, R0, R1
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_Total+0)
MOVT	R0, #hi_addr(_Total+0)
VLDR	#1, S0, [R0, #0]
VADD.F32	S0, S0, S1
VSTR	#1, S0, [R0, #0]
;Heater.c,112 :: 		for (nIndex = 0; nIndex < (nTempIndex); nIndex++)
ADDS	R2, R2, #1
UXTB	R2, R2
;Heater.c,115 :: 		}
; nIndex end address is: 8 (R2)
IT	AL
BAL	L_DisplayTemp10
L_DisplayTemp11:
;Heater.c,117 :: 		fCurrentDisplay = Total;
MOVW	R2, #lo_addr(_Total+0)
MOVT	R2, #hi_addr(_Total+0)
STR	R2, [SP, #8]
VLDR	#1, S0, [R2, #0]
MOVW	R1, #lo_addr(_fCurrentDisplay+0)
MOVT	R1, #hi_addr(_fCurrentDisplay+0)
VSTR	#1, S0, [R1, #0]
;Heater.c,118 :: 		fCurrentDisplay = fCurrentDisplay / (nTempIndex);
MOVW	R0, #lo_addr(_nTempIndex+0)
MOVT	R0, #hi_addr(_nTempIndex+0)
STR	R0, [SP, #4]
LDRSH	R0, [R0, #0]
VMOV	S1, R0
VCVT.F32	#1, S1, S1
MOV	R0, R2
VLDR	#1, S0, [R0, #0]
VDIV.F32	S1, S0, S1
VSTR	#1, S1, [R1, #0]
;Heater.c,119 :: 		fCurrentDisplay *= 10;
VMOV.F32	S0, #10
VMUL.F32	S0, S1, S0
VSTR	#1, S0, [R1, #0]
;Heater.c,120 :: 		modf (fCurrentDisplay , &fCurrentDisplay);
VMOV.F32	S0, S0
MOVW	R0, #lo_addr(_fCurrentDisplay+0)
MOVT	R0, #hi_addr(_fCurrentDisplay+0)
BL	_modf+0
;Heater.c,121 :: 		fCurrentDisplay /= 10;
MOVW	R0, #lo_addr(_fCurrentDisplay+0)
MOVT	R0, #hi_addr(_fCurrentDisplay+0)
VLDR	#1, S1, [R0, #0]
VMOV.F32	S0, #10
VDIV.F32	S0, S1, S0
VSTR	#1, S0, [R0, #0]
;Heater.c,123 :: 		Total = Total / (nTempIndex);
LDR	R0, [SP, #4]
LDRSH	R0, [R0, #0]
VMOV	S1, R0
VCVT.F32	#1, S1, S1
LDR	R1, [SP, #8]
MOV	R0, R1
VLDR	#1, S0, [R0, #0]
VDIV.F32	S0, S0, S1
VSTR	#1, S0, [R1, #0]
;Heater.c,125 :: 		bAtTemp = FALSE;
MOVS	R1, #0
MOVW	R0, #lo_addr(_bAtTemp+0)
MOVT	R0, #hi_addr(_bAtTemp+0)
STRB	R1, [R0, #0]
;Heater.c,126 :: 		if (fSetTemp)
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
LDR	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_DisplayTemp13
;Heater.c,128 :: 		nTempSetpoint = fSetTemp * 100;
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #0
MOVT	R0, #17096
VMOV	S0, R0
VMUL.F32	S0, S1, S0
VCVT	#1, .F32, S0, S0
VMOV	R2, S0
SXTH	R2, R2
MOVW	R0, #lo_addr(_nTempSetpoint+0)
MOVT	R0, #hi_addr(_nTempSetpoint+0)
STRH	R2, [R0, #0]
;Heater.c,129 :: 		nTempDisplay = fCurrentDisplay *100;
MOVW	R0, #lo_addr(_fCurrentDisplay+0)
MOVT	R0, #hi_addr(_fCurrentDisplay+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #0
MOVT	R0, #17096
VMOV	S0, R0
VMUL.F32	S0, S1, S0
VCVT	#1, .F32, S0, S0
VMOV	R0, S0
SXTH	R0, R0
MOVW	R1, #lo_addr(_nTempDisplay+0)
MOVT	R1, #hi_addr(_nTempDisplay+0)
STRH	R0, [R1, #0]
;Heater.c,130 :: 		nTempDisplay = (nTempSetpoint) - (nTempDisplay );
SUB	R0, R2, R0
SXTH	R0, R0
STRH	R0, [R1, #0]
;Heater.c,131 :: 		if (((nTempDisplay ) < 70) && ((nTempDisplay) > -179))  //20 and -19
CMP	R0, #70
IT	GE
BGE	L__DisplayTemp59
MOVW	R0, #lo_addr(_nTempDisplay+0)
MOVT	R0, #hi_addr(_nTempDisplay+0)
LDRSH	R1, [R0, #0]
MVN	R0, #178
CMP	R1, R0
IT	LE
BLE	L__DisplayTemp58
L__DisplayTemp57:
;Heater.c,132 :: 		bAtTemp = TRUE;
MOVS	R1, #1
MOVW	R0, #lo_addr(_bAtTemp+0)
MOVT	R0, #hi_addr(_bAtTemp+0)
STRB	R1, [R0, #0]
;Heater.c,131 :: 		if (((nTempDisplay ) < 70) && ((nTempDisplay) > -179))  //20 and -19
L__DisplayTemp59:
L__DisplayTemp58:
;Heater.c,134 :: 		}
L_DisplayTemp13:
;Heater.c,136 :: 		}
L_end_DisplayTemp:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _DisplayTemp
_refresh_PV_display:
;Heater.c,141 :: 		void refresh_PV_display()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Heater.c,143 :: 		DisplayTemp(); // this was below the next lines, but I think it should be above - CM 19/7/17
BL	_DisplayTemp+0
;Heater.c,144 :: 		if (fCurrentDisplay == fLastNumberDisplay && Screen_PV.Font_Color == SCREEN_PV_FONT_COLOR)
MOVW	R0, #lo_addr(_fLastNumberDisplay+0)
MOVT	R0, #hi_addr(_fLastNumberDisplay+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_fCurrentDisplay+0)
MOVT	R0, #hi_addr(_fCurrentDisplay+0)
VLDR	#1, S0, [R0, #0]
VCMPE.F32	S0, S1
VMRS	#60, FPSCR
IT	NE
BNE	L__refresh_PV_display62
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VLDR	#1, S0, [R0, #0]
VCMPE.F32	S0, #0
VMRS	#60, FPSCR
IT	LE
BLE	L_refresh_PV_display17
MOVW	R0, #lo_addr(_bAtTemp+0)
MOVT	R0, #hi_addr(_bAtTemp+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_refresh_PV_display19
; ?FLOC___refresh_PV_display?T57 start address is: 4 (R1)
MOVW	R1, #2016
; ?FLOC___refresh_PV_display?T57 end address is: 4 (R1)
IT	AL
BAL	L_refresh_PV_display20
L_refresh_PV_display19:
; ?FLOC___refresh_PV_display?T57 start address is: 4 (R1)
MOVW	R1, #63748
; ?FLOC___refresh_PV_display?T57 end address is: 4 (R1)
L_refresh_PV_display20:
; ?FLOC___refresh_PV_display?T57 start address is: 4 (R1)
; ?FLOC___refresh_PV_display?T58 start address is: 0 (R0)
UXTH	R0, R1
; ?FLOC___refresh_PV_display?T57 end address is: 4 (R1)
UXTH	R1, R0
; ?FLOC___refresh_PV_display?T58 end address is: 0 (R0)
IT	AL
BAL	L_refresh_PV_display18
L_refresh_PV_display17:
; ?FLOC___refresh_PV_display?T58 start address is: 4 (R1)
MOVW	R1, #50712
; ?FLOC___refresh_PV_display?T58 end address is: 4 (R1)
L_refresh_PV_display18:
; ?FLOC___refresh_PV_display?T58 start address is: 4 (R1)
MOVW	R0, #lo_addr(_Screen_PV+36)
MOVT	R0, #hi_addr(_Screen_PV+36)
LDRH	R0, [R0, #0]
CMP	R0, R1
IT	NE
BNE	L__refresh_PV_display61
; ?FLOC___refresh_PV_display?T58 end address is: 4 (R1)
L__refresh_PV_display60:
;Heater.c,145 :: 		return;
IT	AL
BAL	L_end_refresh_PV_display
;Heater.c,144 :: 		if (fCurrentDisplay == fLastNumberDisplay && Screen_PV.Font_Color == SCREEN_PV_FONT_COLOR)
L__refresh_PV_display62:
L__refresh_PV_display61:
;Heater.c,147 :: 		Screen_PV.Font_Color = 0x0000;
MOVS	R1, #0
MOVW	R0, #lo_addr(_Screen_PV+36)
MOVT	R0, #hi_addr(_Screen_PV+36)
STRH	R1, [R0, #0]
;Heater.c,148 :: 		DrawButton(&Screen_PV);
MOVW	R0, #lo_addr(_Screen_PV+0)
MOVT	R0, #hi_addr(_Screen_PV+0)
BL	_DrawButton+0
;Heater.c,150 :: 		sprintf(Screen_PV_caption, "%3.1f", fCurrentDisplay);
MOVW	R0, #lo_addr(_fCurrentDisplay+0)
MOVT	R0, #hi_addr(_fCurrentDisplay+0)
VLDR	#1, S0, [R0, #0]
MOVW	R1, #lo_addr(?lstr_1_Heater+0)
MOVT	R1, #hi_addr(?lstr_1_Heater+0)
MOVW	R0, #lo_addr(_Screen_PV_Caption+0)
MOVT	R0, #hi_addr(_Screen_PV_Caption+0)
VPUSH	#0, (S0)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #12
;Heater.c,151 :: 		Screen_PV.Font_Color = SCREEN_PV_FONT_COLOR;
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VLDR	#1, S0, [R0, #0]
VCMPE.F32	S0, #0
VMRS	#60, FPSCR
IT	LE
BLE	L_refresh_PV_display24
MOVW	R0, #lo_addr(_bAtTemp+0)
MOVT	R0, #hi_addr(_bAtTemp+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_refresh_PV_display26
; ?FLOC___refresh_PV_display?T68 start address is: 4 (R1)
MOVW	R1, #2016
; ?FLOC___refresh_PV_display?T68 end address is: 4 (R1)
IT	AL
BAL	L_refresh_PV_display27
L_refresh_PV_display26:
; ?FLOC___refresh_PV_display?T68 start address is: 4 (R1)
MOVW	R1, #63748
; ?FLOC___refresh_PV_display?T68 end address is: 4 (R1)
L_refresh_PV_display27:
; ?FLOC___refresh_PV_display?T68 start address is: 4 (R1)
; ?FLOC___refresh_PV_display?T69 start address is: 0 (R0)
UXTH	R0, R1
; ?FLOC___refresh_PV_display?T68 end address is: 4 (R1)
UXTH	R1, R0
; ?FLOC___refresh_PV_display?T69 end address is: 0 (R0)
IT	AL
BAL	L_refresh_PV_display25
L_refresh_PV_display24:
; ?FLOC___refresh_PV_display?T69 start address is: 4 (R1)
MOVW	R1, #50712
; ?FLOC___refresh_PV_display?T69 end address is: 4 (R1)
L_refresh_PV_display25:
; ?FLOC___refresh_PV_display?T69 start address is: 4 (R1)
MOVW	R0, #lo_addr(_Screen_PV+36)
MOVT	R0, #hi_addr(_Screen_PV+36)
STRH	R1, [R0, #0]
; ?FLOC___refresh_PV_display?T69 end address is: 4 (R1)
;Heater.c,152 :: 		DrawButton(&Screen_PV);
MOVW	R0, #lo_addr(_Screen_PV+0)
MOVT	R0, #hi_addr(_Screen_PV+0)
BL	_DrawButton+0
;Heater.c,153 :: 		fLastNumberDisplay = fCurrentDisplay;
MOVW	R0, #lo_addr(_fCurrentDisplay+0)
MOVT	R0, #hi_addr(_fCurrentDisplay+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_fLastNumberDisplay+0)
MOVT	R0, #hi_addr(_fLastNumberDisplay+0)
VSTR	#1, S0, [R0, #0]
;Heater.c,154 :: 		}
L_end_refresh_PV_display:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _refresh_PV_display
_GetDerivative:
;Heater.c,161 :: 		float GetDerivative(void)
SUB	SP, SP, #4
;Heater.c,163 :: 		float nRate = 0;
; nRate start address is: 0 (R0)
MOV	R0, #0
VMOV	S0, R0
;Heater.c,166 :: 		if (nRecent > 20)
MOVW	R0, #lo_addr(_nRecent+0)
MOVT	R0, #hi_addr(_nRecent+0)
LDRSH	R0, [R0, #0]
CMP	R0, #20
IT	LE
BLE	L_GetDerivative28
;Heater.c,167 :: 		nPeriod = 20 / READING_INTERVAL;
; nPeriod start address is: 8 (R2)
MOVS	R2, #4
SXTH	R2, R2
; nPeriod end address is: 8 (R2)
IT	AL
BAL	L_GetDerivative29
L_GetDerivative28:
;Heater.c,169 :: 		nPeriod = nRecent - 1;
MOVW	R0, #lo_addr(_nRecent+0)
MOVT	R0, #hi_addr(_nRecent+0)
LDRSH	R0, [R0, #0]
SUBS	R2, R0, #1
SXTH	R2, R2
; nPeriod start address is: 8 (R2)
; nPeriod end address is: 8 (R2)
L_GetDerivative29:
;Heater.c,171 :: 		if (0 < nPeriod)
; nPeriod start address is: 8 (R2)
CMP	R2, #0
IT	LE
BLE	L__GetDerivative63
; nRate end address is: 0 (R0)
;Heater.c,172 :: 		nRate = LastReadings[0] - LastReadings[nPeriod];
LSLS	R1, R2, #2
; nPeriod end address is: 8 (R2)
MOVW	R0, #lo_addr(_LastReadings+0)
MOVT	R0, #hi_addr(_LastReadings+0)
ADDS	R0, R0, R1
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_LastReadings+0)
MOVT	R0, #hi_addr(_LastReadings+0)
VLDR	#1, S0, [R0, #0]
VSUB.F32	S0, S0, S1
; nRate start address is: 4 (R1)
VMOV.F32	S1, S0
; nRate end address is: 4 (R1)
VMOV.F32	S0, S1
IT	AL
BAL	L_GetDerivative30
L__GetDerivative63:
;Heater.c,171 :: 		if (0 < nPeriod)
;Heater.c,172 :: 		nRate = LastReadings[0] - LastReadings[nPeriod];
L_GetDerivative30:
;Heater.c,174 :: 		return nRate;
; nRate start address is: 0 (R0)
; nRate end address is: 0 (R0)
;Heater.c,175 :: 		}
L_end_GetDerivative:
ADD	SP, SP, #4
BX	LR
; end of _GetDerivative
_GetIntegral:
;Heater.c,182 :: 		float GetIntegral(void)
SUB	SP, SP, #4
;Heater.c,187 :: 		nIntegral = 0;
MOV	R0, #0
; nIntegral start address is: 4 (R1)
VMOV	S1, R0
;Heater.c,188 :: 		for (i = 0; i < nRecent; i++)
; i start address is: 8 (R2)
MOVS	R2, #0
SXTH	R2, R2
; nIntegral end address is: 4 (R1)
; i end address is: 8 (R2)
L_GetIntegral31:
; i start address is: 8 (R2)
; nIntegral start address is: 4 (R1)
MOVW	R0, #lo_addr(_nRecent+0)
MOVT	R0, #hi_addr(_nRecent+0)
LDRSH	R0, [R0, #0]
CMP	R2, R0
IT	GE
BGE	L_GetIntegral32
;Heater.c,189 :: 		nIntegral += LastReadings[i];
LSLS	R1, R2, #2
MOVW	R0, #lo_addr(_LastReadings+0)
MOVT	R0, #hi_addr(_LastReadings+0)
ADDS	R0, R0, R1
VLDR	#1, S0, [R0, #0]
VADD.F32	S1, S1, S0
;Heater.c,188 :: 		for (i = 0; i < nRecent; i++)
ADDS	R2, R2, #1
SXTH	R2, R2
;Heater.c,189 :: 		nIntegral += LastReadings[i];
; i end address is: 8 (R2)
IT	AL
BAL	L_GetIntegral31
L_GetIntegral32:
;Heater.c,191 :: 		return nIntegral;
VMOV.F32	S0, S1
; nIntegral end address is: 4 (R1)
;Heater.c,192 :: 		}
L_end_GetIntegral:
ADD	SP, SP, #4
BX	LR
; end of _GetIntegral
_CalculatePowerDemand:
;Heater.c,199 :: 		float CalculatePowerDemand(void)
SUB	SP, SP, #8
STR	LR, [SP, #0]
;Heater.c,201 :: 		float fPowerDemand = 0;
;Heater.c,202 :: 		float TempError = 0;
;Heater.c,206 :: 		ActualTemp =  ADC1_Get_Sample(1);//sample_ad(0, ADC_ADVERAGE);  // get the reading
MOVS	R0, #1
BL	_ADC1_Get_Sample+0
VMOV	S0, R0
VCVT.F32	#0, S0, S0
MOVW	R0, #lo_addr(_ActualTemp+0)
MOVT	R0, #hi_addr(_ActualTemp+0)
STR	R0, [SP, #4]
VSTR	#1, S0, [R0, #0]
;Heater.c,207 :: 		ActualTemp += ADC1_Get_Sample(1);
MOVS	R0, #1
BL	_ADC1_Get_Sample+0
VMOV	S1, R0
VCVT.F32	#0, S1, S1
MOVW	R0, #lo_addr(_ActualTemp+0)
MOVT	R0, #hi_addr(_ActualTemp+0)
VLDR	#1, S0, [R0, #0]
VADD.F32	S0, S0, S1
VSTR	#1, S0, [R0, #0]
;Heater.c,208 :: 		ActualTemp +=  ADC1_Get_Sample(1);
MOVS	R0, #1
BL	_ADC1_Get_Sample+0
VMOV	S1, R0
VCVT.F32	#0, S1, S1
MOVW	R0, #lo_addr(_ActualTemp+0)
MOVT	R0, #hi_addr(_ActualTemp+0)
VLDR	#1, S0, [R0, #0]
VADD.F32	S1, S0, S1
VSTR	#1, S1, [R0, #0]
;Heater.c,209 :: 		ActualTemp /= 3;
VMOV.F32	S0, #3
VDIV.F32	S1, S1, S0
LDR	R1, [SP, #4]
VSTR	#1, S1, [R1, #0]
;Heater.c,210 :: 		ActualTemp *= CONVERT_mV; //convert to mv
MOVW	R0, #26214
MOVT	R0, #14822
VMOV	S0, R0
VMUL.F32	S1, S1, S0
VSTR	#1, S1, [R1, #0]
;Heater.c,213 :: 		ActualTemp = 120 * (ActualTemp / 1.8);
MOVW	R0, #26214
MOVT	R0, #16358
VMOV	S0, R0
VDIV.F32	S1, S1, S0
MOVW	R0, #0
MOVT	R0, #17136
VMOV	S0, R0
VMUL.F32	S1, S0, S1
VSTR	#1, S1, [R1, #0]
;Heater.c,214 :: 		ActualTemp *=  fCal_factor.fvalue;
MOVW	R0, #lo_addr(_fCal_factor+4)
MOVT	R0, #hi_addr(_fCal_factor+4)
VLDR	#1, S0, [R0, #0]
VMUL.F32	S1, S1, S0
VSTR	#1, S1, [R1, #0]
;Heater.c,215 :: 		ActualTemp +=  fCal_offset.fvalue ;
MOVW	R0, #lo_addr(_fCal_offset+4)
MOVT	R0, #hi_addr(_fCal_offset+4)
VLDR	#1, S0, [R0, #0]
VADD.F32	S0, S1, S0
VSTR	#1, S0, [R1, #0]
;Heater.c,217 :: 		ActualTemp = nFudgeTemp;
MOVW	R0, #lo_addr(_nFudgeTemp+0)
MOVT	R0, #hi_addr(_nFudgeTemp+0)
LDRSH	R0, [R0, #0]
VMOV	S1, R0
VCVT.F32	#1, S1, S1
VSTR	#1, S1, [R1, #0]
;Heater.c,220 :: 		if (ActualTemp > 100.0)
MOVW	R0, #0
MOVT	R0, #17096
VMOV	S0, R0
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	LE
BLE	L_CalculatePowerDemand34
;Heater.c,221 :: 		ActualTemp = 100.0;
MOVW	R0, #0
MOVT	R0, #17096
VMOV	S0, R0
MOVW	R0, #lo_addr(_ActualTemp+0)
MOVT	R0, #hi_addr(_ActualTemp+0)
VSTR	#1, S0, [R0, #0]
IT	AL
BAL	L_CalculatePowerDemand35
L_CalculatePowerDemand34:
;Heater.c,222 :: 		else if (ActualTemp < 0)
MOVW	R0, #lo_addr(_ActualTemp+0)
MOVT	R0, #hi_addr(_ActualTemp+0)
VLDR	#1, S0, [R0, #0]
VCMPE.F32	S0, #0
VMRS	#60, FPSCR
IT	GE
BGE	L_CalculatePowerDemand36
;Heater.c,223 :: 		ActualTemp = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_ActualTemp+0)
MOVT	R0, #hi_addr(_ActualTemp+0)
VSTR	#1, S0, [R0, #0]
L_CalculatePowerDemand36:
L_CalculatePowerDemand35:
;Heater.c,225 :: 		if (fSetTemp == 0)             // added by Chris 4/9/17
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VLDR	#1, S0, [R0, #0]
VCMPE.F32	S0, #0
VMRS	#60, FPSCR
IT	NE
BNE	L_CalculatePowerDemand37
;Heater.c,226 :: 		return 0;
MOV	R0, #0
VMOV	S0, R0
IT	AL
BAL	L_end_CalculatePowerDemand
L_CalculatePowerDemand37:
;Heater.c,228 :: 		TempError = fSetTemp - ActualTemp;
MOVW	R0, #lo_addr(_ActualTemp+0)
MOVT	R0, #hi_addr(_ActualTemp+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VLDR	#1, S0, [R0, #0]
VSUB.F32	S0, S0, S1
; TempError start address is: 8 (R2)
VMOV.F32	S2, S0
;Heater.c,229 :: 		AddTempError(TempError);
BL	_AddTempError+0
;Heater.c,231 :: 		fPowerDemand = TempError * fProportional.fvalue
MOVW	R0, #lo_addr(_fProportional+4)
MOVT	R0, #hi_addr(_fProportional+4)
VLDR	#1, S0, [R0, #0]
VMUL.F32	S0, S2, S0
; TempError end address is: 8 (R2)
VSTR	#1, S0, [SP, #4]
;Heater.c,232 :: 		+ GetIntegral() * fIntegral.fvalue
BL	_GetIntegral+0
MOVW	R0, #lo_addr(_fIntegral+4)
MOVT	R0, #hi_addr(_fIntegral+4)
VLDR	#1, S1, [R0, #0]
VMUL.F32	S1, S0, S1
VLDR	#1, S0, [SP, #4]
VADD.F32	S0, S0, S1
VSTR	#1, S0, [SP, #4]
;Heater.c,233 :: 		+ GetDerivative() * fDerivative.fvalue;
BL	_GetDerivative+0
MOVW	R0, #lo_addr(_fDerivative+4)
MOVT	R0, #hi_addr(_fDerivative+4)
VLDR	#1, S1, [R0, #0]
VMUL.F32	S1, S0, S1
VLDR	#1, S0, [SP, #4]
VADD.F32	S1, S0, S1
; fPowerDemand start address is: 8 (R2)
VMOV.F32	S2, S1
;Heater.c,235 :: 		if (fPowerDemand > MAX_POWER)
MOVW	R0, #0
MOVT	R0, #17092
VMOV	S0, R0
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	LE
BLE	L_CalculatePowerDemand38
; fPowerDemand end address is: 8 (R2)
;Heater.c,236 :: 		fPowerDemand = MAX_POWER;                      /* Limit duty cycle */
MOVW	R0, #0
MOVT	R0, #17092
; fPowerDemand start address is: 0 (R0)
VMOV	S0, R0
; fPowerDemand end address is: 0 (R0)
IT	AL
BAL	L_CalculatePowerDemand39
L_CalculatePowerDemand38:
;Heater.c,237 :: 		else if (fPowerDemand < 0)
; fPowerDemand start address is: 8 (R2)
VCMPE.F32	S2, #0
VMRS	#60, FPSCR
IT	GE
BGE	L__CalculatePowerDemand64
; fPowerDemand end address is: 8 (R2)
;Heater.c,238 :: 		fPowerDemand = 0;
MOV	R0, #0
; fPowerDemand start address is: 0 (R0)
VMOV	S0, R0
; fPowerDemand end address is: 0 (R0)
IT	AL
BAL	L_CalculatePowerDemand40
L__CalculatePowerDemand64:
;Heater.c,237 :: 		else if (fPowerDemand < 0)
VMOV.F32	S0, S2
;Heater.c,238 :: 		fPowerDemand = 0;
L_CalculatePowerDemand40:
; fPowerDemand start address is: 0 (R0)
; fPowerDemand end address is: 0 (R0)
L_CalculatePowerDemand39:
;Heater.c,240 :: 		return fPowerDemand;
; fPowerDemand start address is: 0 (R0)
; fPowerDemand end address is: 0 (R0)
;Heater.c,241 :: 		}
L_end_CalculatePowerDemand:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _CalculatePowerDemand
_CheckHeaterAlarm:
;Heater.c,248 :: 		void CheckHeaterAlarm(void)      // this is called every 20ms
SUB	SP, SP, #104
STR	LR, [SP, #0]
;Heater.c,255 :: 		if (SRC_State == HEATER_ON_STATE || fabs(ActualTemp-fSetTemp)<5 || bGlobalTempAlarm)  // ActualTemp was LastActualTemp
MOVW	R0, #lo_addr(_SRC_State+0)
MOVT	R0, #hi_addr(_SRC_State+0)
LDRB	R0, [R0, #0]
CMP	R0, #1
IT	EQ
BEQ	L__CheckHeaterAlarm69
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_ActualTemp+0)
MOVT	R0, #hi_addr(_ActualTemp+0)
VLDR	#1, S0, [R0, #0]
VSUB.F32	S0, S0, S1
BL	_fabs+0
VMOV.F32	S1, #5
VCMPE.F32	S0, S1
VMRS	#60, FPSCR
IT	LT
BLT	L__CheckHeaterAlarm68
MOVW	R0, #lo_addr(_bGlobalTempAlarm+0)
MOVT	R0, #hi_addr(_bGlobalTempAlarm+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	NE
BNE	L__CheckHeaterAlarm67
IT	AL
BAL	L_CheckHeaterAlarm43
L__CheckHeaterAlarm69:
L__CheckHeaterAlarm68:
L__CheckHeaterAlarm67:
;Heater.c,260 :: 		nHeatOffDelay = 0;
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(_nHeatOffDelay+0)
MOVT	R0, #hi_addr(_nHeatOffDelay+0)
STRH	R1, [R0, #0]
;Heater.c,262 :: 		return;
IT	AL
BAL	L_end_CheckHeaterAlarm
;Heater.c,263 :: 		}
L_CheckHeaterAlarm43:
;Heater.c,266 :: 		if (++nHeatOffDelay < HEATER_OFF_CHECK_DELAY)
MOVW	R1, #lo_addr(_nHeatOffDelay+0)
MOVT	R1, #hi_addr(_nHeatOffDelay+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
SXTH	R0, R0
STRH	R0, [R1, #0]
CMP	R0, #200
IT	GE
BGE	L_CheckHeaterAlarm44
;Heater.c,267 :: 		return;
IT	AL
BAL	L_end_CheckHeaterAlarm
L_CheckHeaterAlarm44:
;Heater.c,269 :: 		bGlobalTempAlarm = InputOn(HEATER_MAIN_ON);  // if element has power, raise the alarm
MOVS	R0, #6
BL	_InputOn+0
MOVW	R1, #lo_addr(_bGlobalTempAlarm+0)
MOVT	R1, #hi_addr(_bGlobalTempAlarm+0)
STRB	R0, [R1, #0]
;Heater.c,271 :: 		nHeatOffDelay = bGlobalTempAlarm ? -3000     // if we raise the alarm, don't check again for a while (1 min) (give power time to turn off)
CMP	R0, #0
IT	EQ
BEQ	L_CheckHeaterAlarm45
; ?FLOC___CheckHeaterAlarm?T116 start address is: 4 (R1)
MOVW	R1, #62536
SXTH	R1, R1
;Heater.c,272 :: 		: 0;        // if no problem, reset counter to check again in 4 seconds.
; ?FLOC___CheckHeaterAlarm?T116 end address is: 4 (R1)
IT	AL
BAL	L_CheckHeaterAlarm46
L_CheckHeaterAlarm45:
; ?FLOC___CheckHeaterAlarm?T116 start address is: 4 (R1)
MOVS	R1, #0
SXTH	R1, R1
; ?FLOC___CheckHeaterAlarm?T116 end address is: 4 (R1)
L_CheckHeaterAlarm46:
; ?FLOC___CheckHeaterAlarm?T116 start address is: 4 (R1)
MOVW	R0, #lo_addr(_nHeatOffDelay+0)
MOVT	R0, #hi_addr(_nHeatOffDelay+0)
STRH	R1, [R0, #0]
; ?FLOC___CheckHeaterAlarm?T116 end address is: 4 (R1)
;Heater.c,274 :: 		if (bGlobalTempAlarm && nState!=ERR_HOLDING_STATE)
MOVW	R0, #lo_addr(_bGlobalTempAlarm+0)
MOVT	R0, #hi_addr(_bGlobalTempAlarm+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L__CheckHeaterAlarm71
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
LDRB	R0, [R0, #0]
CMP	R0, #101
IT	EQ
BEQ	L__CheckHeaterAlarm70
L__CheckHeaterAlarm65:
;Heater.c,276 :: 		sprintf(msg, "Elm SSR Short S=%dT=%.0f/%.0f", SRC_State, ActualTemp, fSetTemp);
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_ActualTemp+0)
MOVT	R0, #hi_addr(_ActualTemp+0)
VLDR	#1, S0, [R0, #0]
MOVW	R0, #lo_addr(_SRC_State+0)
MOVT	R0, #hi_addr(_SRC_State+0)
LDRB	R2, [R0, #0]
MOVW	R1, #lo_addr(?lstr_2_Heater+0)
MOVT	R1, #hi_addr(?lstr_2_Heater+0)
ADD	R0, SP, #4
VPUSH	#0, (S1)
VPUSH	#0, (S0)
PUSH	(R2)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #20
;Heater.c,277 :: 		ErrorLog_NewError(msg, STATUS_ERROR);
ADD	R0, SP, #4
MOVS	R1, #1
BL	_ErrorLog_NewError+0
;Heater.c,278 :: 		nState = ERR_PRE_HOLDING_STATE;
MOVS	R1, #100
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;Heater.c,274 :: 		if (bGlobalTempAlarm && nState!=ERR_HOLDING_STATE)
L__CheckHeaterAlarm71:
L__CheckHeaterAlarm70:
;Heater.c,280 :: 		}
L_end_CheckHeaterAlarm:
LDR	LR, [SP, #0]
ADD	SP, SP, #104
BX	LR
; end of _CheckHeaterAlarm
_ServiceHeater:
;Heater.c,290 :: 		void ServiceHeater(void)
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Heater.c,292 :: 		CheckHeaterAlarm();
BL	_CheckHeaterAlarm+0
;Heater.c,293 :: 		++SRC_Ticks;
MOVW	R1, #lo_addr(_SRC_Ticks+0)
MOVT	R1, #hi_addr(_SRC_Ticks+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;Heater.c,295 :: 		if (HEATER_ON_STATE == SRC_State)
MOVW	R0, #lo_addr(_SRC_State+0)
MOVT	R0, #hi_addr(_SRC_State+0)
LDRB	R0, [R0, #0]
CMP	R0, #1
IT	NE
BNE	L_ServiceHeater50
;Heater.c,297 :: 		if (SRC_Ticks >= SRC_Power)
MOVW	R0, #lo_addr(_SRC_Ticks+0)
MOVT	R0, #hi_addr(_SRC_Ticks+0)
LDRSH	R0, [R0, #0]
VMOV	S1, R0
VCVT.F32	#1, S1, S1
MOVW	R0, #lo_addr(_SRC_Power+0)
MOVT	R0, #hi_addr(_SRC_Power+0)
VLDR	#1, S0, [R0, #0]
VCMPE.F32	S1, S0
VMRS	#60, FPSCR
IT	LT
BLT	L_ServiceHeater51
;Heater.c,299 :: 		HEATER_OFF;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOD_ODR+0)
MOVT	R0, #hi_addr(GPIOD_ODR+0)
STR	R1, [R0, #0]
;Heater.c,300 :: 		SRC_State = HEATER_OFF_STATE;
MOVS	R1, #0
MOVW	R0, #lo_addr(_SRC_State+0)
MOVT	R0, #hi_addr(_SRC_State+0)
STRB	R1, [R0, #0]
;Heater.c,301 :: 		heater_off_indicator();
BL	_heater_off_indicator+0
;Heater.c,302 :: 		}
L_ServiceHeater51:
;Heater.c,303 :: 		}
IT	AL
BAL	L_ServiceHeater52
L_ServiceHeater50:
;Heater.c,306 :: 		if (1 == SRC_Ticks)
MOVW	R0, #lo_addr(_SRC_Ticks+0)
MOVT	R0, #hi_addr(_SRC_Ticks+0)
LDRSH	R0, [R0, #0]
CMP	R0, #1
IT	NE
BNE	L_ServiceHeater53
;Heater.c,309 :: 		HEATER_OFF;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOD_ODR+0)
MOVT	R0, #hi_addr(GPIOD_ODR+0)
STR	R1, [R0, #0]
;Heater.c,310 :: 		heater_off_indicator();
BL	_heater_off_indicator+0
;Heater.c,311 :: 		}
IT	AL
BAL	L_ServiceHeater54
L_ServiceHeater53:
;Heater.c,312 :: 		else if (SRC_Ticks >= 10) /* Time to turn on again */
MOVW	R0, #lo_addr(_SRC_Ticks+0)
MOVT	R0, #hi_addr(_SRC_Ticks+0)
LDRSH	R0, [R0, #0]
CMP	R0, #10
IT	LT
BLT	L_ServiceHeater55
;Heater.c,314 :: 		SRC_Power = CalculatePowerDemand();
BL	_CalculatePowerDemand+0
MOVW	R0, #lo_addr(_SRC_Power+0)
MOVT	R0, #hi_addr(_SRC_Power+0)
VSTR	#1, S0, [R0, #0]
;Heater.c,315 :: 		if (0 < SRC_Power)
VCMPE.F32	S0, #0
VMRS	#60, FPSCR
IT	LE
BLE	L_ServiceHeater56
;Heater.c,317 :: 		HEATER_ON;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOD_ODR+0)
MOVT	R0, #hi_addr(GPIOD_ODR+0)
STR	R1, [R0, #0]
;Heater.c,318 :: 		SRC_State = HEATER_ON_STATE;
MOVS	R1, #1
MOVW	R0, #lo_addr(_SRC_State+0)
MOVT	R0, #hi_addr(_SRC_State+0)
STRB	R1, [R0, #0]
;Heater.c,319 :: 		heater_on_indicator();
BL	_heater_on_indicator+0
;Heater.c,320 :: 		}
L_ServiceHeater56:
;Heater.c,322 :: 		SRC_Ticks = 0;
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(_SRC_Ticks+0)
MOVT	R0, #hi_addr(_SRC_Ticks+0)
STRH	R1, [R0, #0]
;Heater.c,323 :: 		}
L_ServiceHeater55:
L_ServiceHeater54:
;Heater.c,324 :: 		}
L_ServiceHeater52:
;Heater.c,325 :: 		}
L_end_ServiceHeater:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _ServiceHeater
