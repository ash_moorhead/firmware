#include "BPWasher_objects.h"
#include "BPWasher_resources.h"
#include "built_in.h"
#include "Constants.h"
#include "SF_driver.h"


#define MAX_INPUT 8
#define MAX_OUTPUT 6
 unsigned char nDeviceAddress = 0x20;
unsigned char nAddress[2];
unsigned char OutputSettings = 0x0;
typedef struct  {
     unsigned char bActive;
     TCircle *TargetCircle;
    } InputValues;
InputValues CurrentInputValues[MAX_INPUT];
TCheckBox *const code OutputCheckboxes[]=
          {
          &CheckBox1,
          &CheckBox2,
          &CheckBox3,
          &CheckBox4,
          &CheckBox5,
          &CheckBox6
          };
          
#define I2C1_PORT_A  0x12
          
void ClearAllOutputs()
    {
     unsigned char WriteAddress[2];
     unsigned char i;

     if (SWITCH_ON_I2C)
        {
        WriteAddress[0] = I2C1_PORT_A;
        WriteAddress[1] = 0x00;
        OutputSettings = 0x0;

        I2C1_Start();
        I2C1_Write(nDeviceAddress, WriteAddress, 2, END_MODE_STOP);
        }
     // update diag screen - CM 13/7/17
     for (i=0; i<MAX_OUTPUT; i++)
          OutputCheckboxes[i]->Checked = 0;
    }
    
void SetOutput(char nOutputToDrive, char CallingChkBox)
    {
    unsigned char nTemp = 0x01;
    unsigned char WriteAddress[2];
    if (SWITCH_ON_I2C)
      {
      if (nOutputToDrive < 7)  // should always be true
          {
          WriteAddress[0] = I2C1_PORT_A;

          nTemp = (nTemp <<  nOutputToDrive);
          if (!CallingChkBox)
              {
              nTemp = ~nTemp;
              OutputSettings = OutputSettings &  nTemp;
              }
          else
            OutputSettings = OutputSettings |  nTemp;

          WriteAddress[1] = OutputSettings;
          I2C1_Start();

          I2C1_Write(nDeviceAddress, WriteAddress, 2, END_MODE_STOP);
          }
      }
      // update the diag screen checkbox
      OutputCheckboxes[nOutputToDrive]->Checked = CallingChkBox;
      DrawCheckbox(OutputCheckboxes[nOutputToDrive]);
    }
void ConfigureInputs(void)
    {
    CurrentInputValues[0].bActive = 0x00;
    CurrentInputValues[0].TargetCircle = &Circle1;

    CurrentInputValues[1].bActive = 0x00;
    CurrentInputValues[1].TargetCircle = &Circle2;

    CurrentInputValues[2].bActive = 0x00;
    CurrentInputValues[2].TargetCircle = &Circle3;

    CurrentInputValues[3].bActive = 0x00;
     CurrentInputValues[3].TargetCircle = &Circle4;

    CurrentInputValues[4].bActive = 0x00;
    CurrentInputValues[4].TargetCircle = &Circle5;

    CurrentInputValues[5].bActive = 0x00;
    CurrentInputValues[5].TargetCircle = 0;

    CurrentInputValues[6].bActive = 0x00;
    CurrentInputValues[6].TargetCircle = &Circle6;

    CurrentInputValues[7].bActive = 0x00;
    CurrentInputValues[7].TargetCircle = 0;

    }
char InputOn (char nInputIndex)
    {
#if SWITCH_ON_I2C
    return  CurrentInputValues[nInputIndex].bActive;
#else
    if (nInputIndex == START_BTN && CurrentInputValues[nInputIndex].bActive)
       {
       // automatically turn off START button during testing because I kept leaving it on and the recipe would restart upon error.
        CurrentInputValues[nInputIndex].bActive = FALSE;
        return TRUE;
       }
    return CurrentInputValues[nInputIndex].bActive;
#endif
    }

void UpdateInputs(char nInputNewValue)
    {
    unsigned char nIndex;
    unsigned char nNewValue;
    nNewValue = ~nInputNewValue;

    for (nIndex = 0; nIndex < MAX_INPUT; nIndex++)
        {

        if (CurrentInputValues[nIndex].bActive != ((nNewValue >> nIndex) & 0x1))
            {
            CurrentInputValues[nIndex].TargetCircle->Gradient_Start_Color = CurrentInputValues[nIndex].bActive ? CL_WHITE : CL_GREEN ;

             if (CurrentInputValues[nIndex].TargetCircle)
                DrawCircle(CurrentInputValues[nIndex].TargetCircle);

             CurrentInputValues[nIndex].bActive = !CurrentInputValues[nIndex].bActive;
            }
        }


    }
    
unsigned char nOldInputsValue = 0x00;
unsigned char buffer[2];
#define I2C_PORT_B  0x13

void GetInputStates()
    {
    nAddress[1] = I2C_PORT_B;
    nAddress[0] = I2C_PORT_B;
    I2C1_Start();
    I2C1_Write(nDeviceAddress, nAddress, 1, END_MODE_RESTART);
    
    I2C1_Read(nDeviceAddress, buffer, 2, END_MODE_STOP);

    if (nOldInputsValue != buffer[0])
           UpdateInputs(buffer[0]);
    nOldInputsValue = buffer[0];
    }