#include "BPWasher_objects.h"
#include "BPWasher_resources.h"
#include "Constants.h"
//--------------------- User code ---------------------//
float fProportionalOld;
float fIntegralOld;
float fDerivativeOld;
float fCal_offsetOld;
float fCal_factorOld;
float fDrainTimeOld;
float fExtraFillTimeOld;

void heater_on_indicator()
     {
     Diagram3_thermBlack.Visible = 0;
     DrawImage(&Diagram3_thermBlack);
     Diagram3_thermred.Visible = 1;
     DrawImage(&Diagram3_thermred);
     }

void heater_off_indicator()
     {
     Diagram3_thermred.Visible = 0;
     DrawImage(&Diagram3_thermred);
     Diagram3_thermBlack.Visible = 1;
     DrawImage(&Diagram3_thermBlack);
     }

//----------------- End of User code ------------------//

// Event Handlers
/******************************************************************************/
/*    KeyPad Key Presses                                                      */
/******************************************************************************/

void Key_0Click()
     {
     String_to_num('0');
     }

void Key_dpClick()
     {
     String_to_num('.');
     }

void Key_1click()
     {
     String_to_num('1');
     }

void Key_2Click()
     {
     String_to_num('2');
     }

void Key_3Click()
     {
     String_to_num('3');
     }

void Key_4Click()
     {
     String_to_num('4');
     }

void Key_5Click() {
     String_to_num('5');
}

void Key_6Click()
     {
     String_to_num('6');
     }

void Key_7Click()
     {
     String_to_num('7');
     }

void Key_8Click()
     {
     String_to_num('8');
     }

void Key_9Click()
     {
     String_to_num('9');
     }

void pos_negClick()
     {
     String_to_num('-');
     }

void Key_clearClick()
     {
     ResetKeypad();
     }
     
void Key_enterClick()
     {
     enter_pressed();
     }

/******************************************************************************/
/*   Main Screen Key Presses                                                  */
/******************************************************************************/

void Main_Screen_settingsClick()
     {
     Display_Keyboard("Enter password", &fPassword);
     }


void Main_Screen_startClick()    // actually this is the STOP button - CM 13/07/17
     {
        fSetTemp = 0;
        ClearAllOutputs();
        ResetRecipe();
        HEATER_OFF;
     }


/******************************************************************************/
/*   Settings Screen Key Presses                                              */
/******************************************************************************/
void Diag_Clicked()
    {
     DrawScreen(&Diags);
    }

void Config_Clicked()
  {
   DrawScreen(&Config);
   fDrainTimeOld = fDrainTime.fValue;
   fExtraFillTimeOld = fExtraFillTime.fValue;
  }

void Settings_LogClick()
     {
     sprintf(Log_Title_Caption, "%.0f / %.0f = Complete / Faults", fCyclesComplete, fCyclesFail);
     DrawScreen(&ErrorLog);
     }
void Settings_PIDClick()
     {
     DrawScreen(&PID);
     // backup old values - CM 10/07/17
     fProportionalOld = fProportional.fValue;
     fIntegralOld = fIntegral.fValue;
     fDerivativeOld = fDerivative.fValue;
     }

void Settings_CALClick()
     {
     DrawScreen(&Calibration);
     // backup old values - CM 10/07/17
     fCal_offsetOld = fCal_offset.fValue;
     fCal_factorOld = fCal_factor.fValue;
     }

void Settings_ExitClick()
     {
     DrawScreen(&_Main);
     }
/******************************************************************************/
/*   PID Screen Key Presses                                                   */
/******************************************************************************/


void Prop_buttonClick()
     {
     Display_Keyboard("Enter P value", &fProportional);
    }

void I_buttonClick()
     {
     Display_Keyboard("Enter I Value", &fIntegral);
     }

void D_buttonClick()
     {
     Display_Keyboard("Enter D Value", &fDerivative);
     }

void PID_conf_buttonClick()
     {
     DrawScreen(&Settings);
     }

void PID_ret_buttonClick()
     {
     // restore previous values - CM 10/07/17
     set_and_format_value(&fProportional, fProportionalOld);
     set_and_format_value(&fIntegral, fIntegralOld);
     set_and_format_value(&fDerivative, fDerivativeOld);
     DrawScreen(&PID);
     UpDateMemory();
     }
/******************************************************************************/
/*   Calibration Screen Key Presses                                           */
/******************************************************************************/

void cal_offset_buttonClick() {
     Display_Keyboard("Enter Cal Offset", &fCal_offset);
}

void cal_fact_buttonClick() {
     Display_Keyboard("Enter Cal Factor", &fCal_factor);
}

void cal_start_buttonClick() {
         
     if (cal_start_button.Caption == cal_start_button_Caption)
     {
     // Toggle UI
        CalibrationScreenVisible(FALSE);
        cal_start_button.Caption = cal_start_button_CaptionStop;
        
     // Begin Calibration Recipe
        nState = CAL_START;
        
     } else {
       // Toggle UI
       CalibrationScreenVisible(TRUE);
       cal_temp_label.Visible = FALSE;
       cal_start_button.Caption = cal_start_button_Caption;
       
       //Exit Calibration routine
       nState = CAL_STOP;
     }
     
      DrawScreen(&Calibration);
}

void CalibrationScreenVisible(char visibility){

       cal_offset_button.Visible = visibility;
       cal_fact_button.Visible = visibility;
       cal_offset_value.Visible = visibility;
       cal_factor_value.Visible = visibility;
       cal_ret_button.Visible = visibility;
       Image10.Visible = visibility;
       cal_confirm_button.Visible = visibility;
       cal_message_label.Visible = !visibility;
       
       cal_offset_button.Active = visibility;
       cal_fact_button.Active = visibility;
       cal_offset_value.Active = visibility;
       cal_factor_value.Active = visibility;
       cal_ret_button.Active = visibility;
       Image10.Active = visibility;
       cal_confirm_button.Active = visibility;
}

void cal_confirm_buttonClick() {
     DrawScreen(&Settings);
}

void cal_ret_buttonClick() {
     // restore previous values - CM 10/07/17
     set_and_format_value(&fCal_offset, fCal_offsetOld);
     set_and_format_value(&fCal_factor, fCal_factorOld);
     DrawScreen(&Calibration);
     UpDateMemory();
}
 /******************************************************************************/
/*   Diags Screen Key Presses                                           */
/******************************************************************************/
void Exit_Pressed()
    {
     DrawScreen(&Settings);
    }
/******************************************************************************/
/*   PID Screen Key Presses                                                   */
/******************************************************************************/
void ErrorLog_OKClick()
    {
    DrawScreen(&Settings);
    }
    
/******************************************************************************/
/*   Config Screen Key Presses                                           */
/******************************************************************************/
void config_drain_buttonClick(){
     Display_Keyboard("Enter drain time", &fDrainTime);
}

void config_fill_buttonClick(){
     Display_Keyboard("Enter extra fill time", &fExtraFillTime);
}

void config_confirm_buttonClick() {
     UpDateMemory();
     DrawScreen(&Settings);
}

void config_ret_buttonClick() {
     set_and_format_value(&fDrainTime, fDrainTimeOld);
     set_and_format_value(&fExtraFillTime, fExtraFillTimeOld);
     UpDateMemory();
     DrawScreen(&Config);
}