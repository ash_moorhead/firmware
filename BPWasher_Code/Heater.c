// Copyright Alex Lowings 2006

#include "BPWasher_objects.h"
#include "BPWasher_resources.h"
#include "built_in.h"
#include "Constants.h"

#define MAX_POWER   98           /* Percent */
#define HEATER_ON_STATE  1
#define HEATER_OFF_STATE  0
#define POWER_FIFTY 49
#define TEMP_ADV 15
#define NLASTREADINGS   25 // 100 orginal 400 for steel; water does not want this history.
#define READING_INTERVAL 5
#define CONVERT_mV /*0.97704*/ 1.8 / 4096
#define MOVING_TIME_OUT 60

float ActualTemp = 20.0;      /* Degrees Centigrade x10 ! */

signed int nRecent = 0;
//float LastActualTemp = 0;  // CM - can't see any reason for this, so using ActualTemp instead.
float fCurrentDisplay = 0;

float LastReadings[NLASTREADINGS];

char bGlobalTempAlarm;

int SRC_Ticks = 0;
float SRC_Power = 0;
unsigned char SRC_State = HEATER_OFF_STATE;

char bAtTemp = FALSE;

#if !SWITCH_ON_I2C
// way of fudging input temperature - CM 3/7/17
signed int nFudgeTemp = 20.0 ;
#endif

/*
GetCurrentTemp()
Returns the last reading from the ADC
*/
 float GetCurrentTemp()
    {
     return  ActualTemp; // was LastActualTemp;
    }


/*
GetDisplayTemp()
Returns the current value on screen, which is the average of the last 15 readings
*/
 float GetDisplayTemp()
    {
     return  fCurrentDisplay;
    }


/*--------------------------------------------------------------------------*/
/* Add this second's reading into the record of the recent history at the
 * beginning. The error is postive if the actual temperature is below the set
 * point. Record the level every READING_INTERVAL. Record the average over the
 * interval;
 */

unsigned char nSeconds = 0;

void AddTempError(float TempError)
     {
     int i;

     if (++nSeconds >= READING_INTERVAL)
        {
        for (i = NLASTREADINGS - 1; i > 0; i--)
            LastReadings[i] = LastReadings[i - 1];
     
         LastReadings[0] = (TempError / READING_INTERVAL);
          
         nSeconds = 0;
         if (++nRecent > NLASTREADINGS)
            nRecent = NLASTREADINGS;
         }
     }

/*--------------------------------------------------------------------------*/

float nTempLedDisplay[TEMP_ADV];  //int
int nTempIndex = 0;
int nTempDisplay = 0;
int nTempSetpoint = 0;
float Total;

void DisplayTemp(void)
    {
    char nIndex = 0;
    Total = 0;
        
    if (nTempIndex < TEMP_ADV)
        {
        nTempLedDisplay[nTempIndex] = (ActualTemp);
        nTempIndex++;
        }
    else
        {
        //Add temp to average list
        for (nIndex = 0; nIndex < TEMP_ADV-1; nIndex++)
            nTempLedDisplay[nIndex] = nTempLedDisplay[nIndex + 1];
            
        nTempLedDisplay[TEMP_ADV-1]  = (ActualTemp);
        }

    for (nIndex = 0; nIndex < (nTempIndex); nIndex++)
        {
        Total +=  nTempLedDisplay[nIndex] ;
        }

    fCurrentDisplay = Total;
    fCurrentDisplay = fCurrentDisplay / (nTempIndex);
    fCurrentDisplay *= 10;
    modf (fCurrentDisplay , &fCurrentDisplay);
    fCurrentDisplay /= 10;
    
    Total = Total / (nTempIndex);
    
    bAtTemp = FALSE;
    if (fSetTemp)
        {
        nTempSetpoint = fSetTemp * 100;
        nTempDisplay = fCurrentDisplay *100;
        nTempDisplay = (nTempSetpoint) - (nTempDisplay );
        if (((nTempDisplay ) < 70) && ((nTempDisplay) > -179))  //20 and -19
           bAtTemp = TRUE;
           
        }

    }
    
float fLastNumberDisplay = -1;
#define  SCREEN_PV_FONT_COLOR  (fSetTemp>0 ? (bAtTemp ? CL_LIME : 0xF904) : 0xC618)

void refresh_PV_display()
     {
     DisplayTemp(); // this was below the next lines, but I think it should be above - CM 19/7/17
     if (fCurrentDisplay == fLastNumberDisplay && Screen_PV.Font_Color == SCREEN_PV_FONT_COLOR)
        return;

     Screen_PV.Font_Color = 0x0000;
     DrawButton(&Screen_PV);
     
     sprintf(Screen_PV_caption, "%3.1f", fCurrentDisplay);
     Screen_PV.Font_Color = SCREEN_PV_FONT_COLOR;
     DrawButton(&Screen_PV);
     fLastNumberDisplay = fCurrentDisplay;
     }

/*--------------------------------------------------------------------------*/
/* Calculate the rate at which the temperature is approaching the set point.
 * Returns 10 * deg.C change over the last 20 seconds.  Note that
 * the rate is positive when the actual temperature is increasing.
 */
float GetDerivative(void)
      {
      float nRate = 0;
      int nPeriod;

      if (nRecent > 20)
         nPeriod = 20 / READING_INTERVAL;
      else
         nPeriod = nRecent - 1;
         
      if (0 < nPeriod)
         nRate = LastReadings[0] - LastReadings[nPeriod];
      
      return nRate;
    }

/*--------------------------------------------------------------------------*/
/* Calculate the integral of the temperature error below the set pointover the
 * last 50 seconds.  Called every second. Returns 10 * deg.C * 500 seconds. The
 * error is postive if the actual temperature is below the set point.
 */
float GetIntegral(void)
      {
      float nIntegral;
      int i;

      nIntegral = 0;
      for (i = 0; i < nRecent; i++)
          nIntegral += LastReadings[i];

      return nIntegral;
      }
        
/*--------------------------------------------------------------------------*/
/* Calculate the power demand for the RMS body heater. "TempError" is positive
 * when the body temperature is below the set point.
 * Limit the final value to 98%.
 */
float CalculatePowerDemand(void)
    {
    float fPowerDemand = 0;
    float TempError = 0;
    
    /* Read the actual temperature (x100) */
    // GPIOB_ODR ^= (1<<13);
    ActualTemp =  ADC1_Get_Sample(1);//sample_ad(0, ADC_ADVERAGE);  // get the reading
    ActualTemp += ADC1_Get_Sample(1);
    ActualTemp +=  ADC1_Get_Sample(1);
    ActualTemp /= 3;
    ActualTemp *= CONVERT_mV; //convert to mv

    //ActualTemp /= 20;    // convert from mv to degrees
    ActualTemp = 120 * (ActualTemp / 1.8);
    ActualTemp *=  fCal_factor.fvalue;
    ActualTemp +=  fCal_offset.fvalue ;
#if !SWITCH_ON_I2C
    ActualTemp = nFudgeTemp;
#endif
         
    if (ActualTemp > 100.0)
       ActualTemp = 100.0;
    else if (ActualTemp < 0)
       ActualTemp = 0;

    if (fSetTemp == 0)             // added by Chris 4/9/17
       return 0;
       
    TempError = fSetTemp - ActualTemp;
    AddTempError(TempError);

    fPowerDemand = TempError * fProportional.fvalue
                 + GetIntegral() * fIntegral.fvalue
                 + GetDerivative() * fDerivative.fvalue;

    if (fPowerDemand > MAX_POWER)
       fPowerDemand = MAX_POWER;                      /* Limit duty cycle */
    else if (fPowerDemand < 0)
       fPowerDemand = 0;
       
    return fPowerDemand;
    }

/*--------------------------------------------------------------------------*/
/*Check That the elememt is off when well tell it */
#define HEATER_OFF_CHECK_DELAY 200    // about 4 seconds
int nHeatOffDelay = 0;

void CheckHeaterAlarm(void)      // this is called every 20ms
     {
     char msg[100];
     
     // don't ever allow reset of bGlobalTempAlarm if it has ever been set to true, need to power off/on again, CM 05/08/2017
     // TODO remember bGlobalTempAlarm in EEPROM so it can't be reset with power off/on, and add UI in settings screen to reset it, CM 08/05/2017

     if (SRC_State == HEATER_ON_STATE || fabs(ActualTemp-fSetTemp)<5 || bGlobalTempAlarm)  // ActualTemp was LastActualTemp
        {
        // if we're close to set temp then the PID might be quickly turning the element on/off which
        // due to inductance delays might make it appear to be on after we've told it to be off.
        // So wait until temp is at least 5 degrees out then PID should be permanently on or off.
        nHeatOffDelay = 0;
        // bGlobalTempAlarm = FALSE;       // CM removed ability to recover - need to turn off and on again.
        return;
        }

     // if we're not supposed to be heating, wait for about 4 seconds before checking element
     if (++nHeatOffDelay < HEATER_OFF_CHECK_DELAY)
        return;

     bGlobalTempAlarm = InputOn(HEATER_MAIN_ON);  // if element has power, raise the alarm
     // following line is now irrelevent since can't get here again if bGlobalTempAlarm is set to true, CM 05/08/17
     nHeatOffDelay = bGlobalTempAlarm ? -3000     // if we raise the alarm, don't check again for a while (1 min) (give power time to turn off)
                                      : 0;        // if no problem, reset counter to check again in 4 seconds.

     if (bGlobalTempAlarm && nState!=ERR_HOLDING_STATE)
        {
        sprintf(msg, "Elm SSR Short S=%dT=%.0f/%.0f", SRC_State, ActualTemp, fSetTemp);
        ErrorLog_NewError(msg, STATUS_ERROR);
        nState = ERR_PRE_HOLDING_STATE;
        }
     }

/*--------------------------------------------------------------------------*/
/* Service the Source heater. Aim for the set point with inputs from the
 * PRT and the time.  Work on a 1s timebase, calls to this routine occurring
 * every 10ms. LED is driven ON at
 * the start of each time period and off when the heater goes off (or after 1
 * tick if zero power demand).
 */

void ServiceHeater(void)
     {
     CheckHeaterAlarm();
     ++SRC_Ticks;

     if (HEATER_ON_STATE == SRC_State)
        {
        if (SRC_Ticks >= SRC_Power)
            {
            HEATER_OFF;
            SRC_State = HEATER_OFF_STATE;
            heater_off_indicator();
            }
        }
     else
        {
        if (1 == SRC_Ticks)
            {
            //heater off
            HEATER_OFF;
            heater_off_indicator();
            }
        else if (SRC_Ticks >= 10) /* Time to turn on again */
            {
            SRC_Power = CalculatePowerDemand();
            if (0 < SRC_Power)
               {
               HEATER_ON;
               SRC_State = HEATER_ON_STATE;
               heater_on_indicator();
               }
               
            SRC_Ticks = 0;
            }
        }
}