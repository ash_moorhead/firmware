_ClearAllOutputs:
;I_O.c,30 :: 		void ClearAllOutputs()
SUB	SP, SP, #8
;I_O.c,43 :: 		}
L_ClearAllOutputs0:
;I_O.c,45 :: 		for (i=0; i<MAX_OUTPUT; i++)
; i start address is: 8 (R2)
MOVS	R2, #0
; i end address is: 8 (R2)
L_ClearAllOutputs1:
; i start address is: 8 (R2)
CMP	R2, #6
IT	CS
BCS	L_ClearAllOutputs2
;I_O.c,46 :: 		OutputCheckboxes[i]->Checked = 0;
LSLS	R1, R2, #2
MOVW	R0, #lo_addr(_OutputCheckboxes+0)
MOVT	R0, #hi_addr(_OutputCheckboxes+0)
ADDS	R0, R0, R1
LDR	R0, [R0, #0]
ADDW	R1, R0, #20
MOVS	R0, #0
STRB	R0, [R1, #0]
;I_O.c,45 :: 		for (i=0; i<MAX_OUTPUT; i++)
ADDS	R2, R2, #1
UXTB	R2, R2
;I_O.c,46 :: 		OutputCheckboxes[i]->Checked = 0;
; i end address is: 8 (R2)
IT	AL
BAL	L_ClearAllOutputs1
L_ClearAllOutputs2:
;I_O.c,47 :: 		}
L_end_ClearAllOutputs:
ADD	SP, SP, #8
BX	LR
; end of _ClearAllOutputs
_SetOutput:
;I_O.c,49 :: 		void SetOutput(char nOutputToDrive, char CallingChkBox)
; CallingChkBox start address is: 4 (R1)
; nOutputToDrive start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; CallingChkBox end address is: 4 (R1)
; nOutputToDrive end address is: 0 (R0)
; nOutputToDrive start address is: 0 (R0)
; CallingChkBox start address is: 4 (R1)
;I_O.c,51 :: 		unsigned char nTemp = 0x01;
;I_O.c,73 :: 		}
L_SetOutput4:
;I_O.c,75 :: 		OutputCheckboxes[nOutputToDrive]->Checked = CallingChkBox;
LSLS	R3, R0, #2
MOVW	R2, #lo_addr(_OutputCheckboxes+0)
MOVT	R2, #hi_addr(_OutputCheckboxes+0)
ADDS	R2, R2, R3
LDR	R2, [R2, #0]
ADDS	R2, #20
STRB	R1, [R2, #0]
; CallingChkBox end address is: 4 (R1)
;I_O.c,76 :: 		DrawCheckbox(OutputCheckboxes[nOutputToDrive]);
LSLS	R3, R0, #2
; nOutputToDrive end address is: 0 (R0)
MOVW	R2, #lo_addr(_OutputCheckboxes+0)
MOVT	R2, #hi_addr(_OutputCheckboxes+0)
ADDS	R2, R2, R3
LDR	R2, [R2, #0]
MOV	R0, R2
BL	_DrawCheckBox+0
;I_O.c,77 :: 		}
L_end_SetOutput:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _SetOutput
_ConfigureInputs:
;I_O.c,78 :: 		void ConfigureInputs(void)
SUB	SP, SP, #4
;I_O.c,80 :: 		CurrentInputValues[0].bActive = 0x00;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CurrentInputValues+0)
MOVT	R0, #hi_addr(_CurrentInputValues+0)
STRB	R1, [R0, #0]
;I_O.c,81 :: 		CurrentInputValues[0].TargetCircle = &Circle1;
MOVW	R1, #lo_addr(_Circle1+0)
MOVT	R1, #hi_addr(_Circle1+0)
MOVW	R0, #lo_addr(_CurrentInputValues+4)
MOVT	R0, #hi_addr(_CurrentInputValues+4)
STR	R1, [R0, #0]
;I_O.c,83 :: 		CurrentInputValues[1].bActive = 0x00;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CurrentInputValues+8)
MOVT	R0, #hi_addr(_CurrentInputValues+8)
STRB	R1, [R0, #0]
;I_O.c,84 :: 		CurrentInputValues[1].TargetCircle = &Circle2;
MOVW	R1, #lo_addr(_Circle2+0)
MOVT	R1, #hi_addr(_Circle2+0)
MOVW	R0, #lo_addr(_CurrentInputValues+12)
MOVT	R0, #hi_addr(_CurrentInputValues+12)
STR	R1, [R0, #0]
;I_O.c,86 :: 		CurrentInputValues[2].bActive = 0x00;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CurrentInputValues+16)
MOVT	R0, #hi_addr(_CurrentInputValues+16)
STRB	R1, [R0, #0]
;I_O.c,87 :: 		CurrentInputValues[2].TargetCircle = &Circle3;
MOVW	R1, #lo_addr(_Circle3+0)
MOVT	R1, #hi_addr(_Circle3+0)
MOVW	R0, #lo_addr(_CurrentInputValues+20)
MOVT	R0, #hi_addr(_CurrentInputValues+20)
STR	R1, [R0, #0]
;I_O.c,89 :: 		CurrentInputValues[3].bActive = 0x00;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CurrentInputValues+24)
MOVT	R0, #hi_addr(_CurrentInputValues+24)
STRB	R1, [R0, #0]
;I_O.c,90 :: 		CurrentInputValues[3].TargetCircle = &Circle4;
MOVW	R1, #lo_addr(_Circle4+0)
MOVT	R1, #hi_addr(_Circle4+0)
MOVW	R0, #lo_addr(_CurrentInputValues+28)
MOVT	R0, #hi_addr(_CurrentInputValues+28)
STR	R1, [R0, #0]
;I_O.c,92 :: 		CurrentInputValues[4].bActive = 0x00;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CurrentInputValues+32)
MOVT	R0, #hi_addr(_CurrentInputValues+32)
STRB	R1, [R0, #0]
;I_O.c,93 :: 		CurrentInputValues[4].TargetCircle = &Circle5;
MOVW	R1, #lo_addr(_Circle5+0)
MOVT	R1, #hi_addr(_Circle5+0)
MOVW	R0, #lo_addr(_CurrentInputValues+36)
MOVT	R0, #hi_addr(_CurrentInputValues+36)
STR	R1, [R0, #0]
;I_O.c,95 :: 		CurrentInputValues[5].bActive = 0x00;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CurrentInputValues+40)
MOVT	R0, #hi_addr(_CurrentInputValues+40)
STRB	R1, [R0, #0]
;I_O.c,96 :: 		CurrentInputValues[5].TargetCircle = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CurrentInputValues+44)
MOVT	R0, #hi_addr(_CurrentInputValues+44)
STR	R1, [R0, #0]
;I_O.c,98 :: 		CurrentInputValues[6].bActive = 0x00;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CurrentInputValues+48)
MOVT	R0, #hi_addr(_CurrentInputValues+48)
STRB	R1, [R0, #0]
;I_O.c,99 :: 		CurrentInputValues[6].TargetCircle = &Circle6;
MOVW	R1, #lo_addr(_Circle6+0)
MOVT	R1, #hi_addr(_Circle6+0)
MOVW	R0, #lo_addr(_CurrentInputValues+52)
MOVT	R0, #hi_addr(_CurrentInputValues+52)
STR	R1, [R0, #0]
;I_O.c,101 :: 		CurrentInputValues[7].bActive = 0x00;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CurrentInputValues+56)
MOVT	R0, #hi_addr(_CurrentInputValues+56)
STRB	R1, [R0, #0]
;I_O.c,102 :: 		CurrentInputValues[7].TargetCircle = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_CurrentInputValues+60)
MOVT	R0, #hi_addr(_CurrentInputValues+60)
STR	R1, [R0, #0]
;I_O.c,104 :: 		}
L_end_ConfigureInputs:
ADD	SP, SP, #4
BX	LR
; end of _ConfigureInputs
_InputOn:
;I_O.c,105 :: 		char InputOn (char nInputIndex)
; nInputIndex start address is: 0 (R0)
SUB	SP, SP, #4
; nInputIndex end address is: 0 (R0)
; nInputIndex start address is: 0 (R0)
;I_O.c,110 :: 		if (nInputIndex == START_BTN && CurrentInputValues[nInputIndex].bActive)
CMP	R0, #1
IT	NE
BNE	L__InputOn21
LSLS	R2, R0, #3
MOVW	R1, #lo_addr(_CurrentInputValues+0)
MOVT	R1, #hi_addr(_CurrentInputValues+0)
ADDS	R1, R1, R2
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L__InputOn20
L__InputOn19:
;I_O.c,113 :: 		CurrentInputValues[nInputIndex].bActive = FALSE;
LSLS	R2, R0, #3
; nInputIndex end address is: 0 (R0)
MOVW	R1, #lo_addr(_CurrentInputValues+0)
MOVT	R1, #hi_addr(_CurrentInputValues+0)
ADDS	R2, R1, R2
MOVS	R1, #0
STRB	R1, [R2, #0]
;I_O.c,114 :: 		return TRUE;
MOVS	R0, #1
IT	AL
BAL	L_end_InputOn
;I_O.c,110 :: 		if (nInputIndex == START_BTN && CurrentInputValues[nInputIndex].bActive)
L__InputOn21:
; nInputIndex start address is: 0 (R0)
L__InputOn20:
;I_O.c,116 :: 		return CurrentInputValues[nInputIndex].bActive;
LSLS	R2, R0, #3
; nInputIndex end address is: 0 (R0)
MOVW	R1, #lo_addr(_CurrentInputValues+0)
MOVT	R1, #hi_addr(_CurrentInputValues+0)
ADDS	R1, R1, R2
LDRB	R1, [R1, #0]
UXTB	R0, R1
;I_O.c,118 :: 		}
L_end_InputOn:
ADD	SP, SP, #4
BX	LR
; end of _InputOn
_UpdateInputs:
;I_O.c,120 :: 		void UpdateInputs(char nInputNewValue)
; nInputNewValue start address is: 0 (R0)
SUB	SP, SP, #12
STR	LR, [SP, #0]
; nInputNewValue end address is: 0 (R0)
; nInputNewValue start address is: 0 (R0)
;I_O.c,124 :: 		nNewValue = ~nInputNewValue;
MVN	R0, R0
UXTB	R0, R0
; nInputNewValue end address is: 0 (R0)
; nNewValue start address is: 0 (R0)
;I_O.c,126 :: 		for (nIndex = 0; nIndex < MAX_INPUT; nIndex++)
; nIndex start address is: 16 (R4)
MOVS	R4, #0
; nIndex end address is: 16 (R4)
L_UpdateInputs11:
; nIndex start address is: 16 (R4)
; nNewValue start address is: 0 (R0)
; nNewValue end address is: 0 (R0)
CMP	R4, #8
IT	CS
BCS	L_UpdateInputs12
; nNewValue end address is: 0 (R0)
;I_O.c,129 :: 		if (CurrentInputValues[nIndex].bActive != ((nNewValue >> nIndex) & 0x1))
; nNewValue start address is: 0 (R0)
LSLS	R2, R4, #3
MOVW	R1, #lo_addr(_CurrentInputValues+0)
MOVT	R1, #hi_addr(_CurrentInputValues+0)
ADDS	R1, R1, R2
LDRB	R2, [R1, #0]
LSR	R1, R0, R4
UXTB	R1, R1
AND	R1, R1, #1
UXTB	R1, R1
CMP	R2, R1
IT	EQ
BEQ	L_UpdateInputs14
;I_O.c,131 :: 		CurrentInputValues[nIndex].TargetCircle->Gradient_Start_Color = CurrentInputValues[nIndex].bActive ? CL_WHITE : CL_GREEN ;
LSLS	R2, R4, #3
MOVW	R1, #lo_addr(_CurrentInputValues+0)
MOVT	R1, #hi_addr(_CurrentInputValues+0)
ADDS	R3, R1, R2
ADDS	R1, R3, #4
LDR	R1, [R1, #0]
ADDW	R2, R1, #22
LDRB	R1, [R3, #0]
CMP	R1, #0
IT	EQ
BEQ	L_UpdateInputs15
MOVW	R1, #65535
STRH	R1, [SP, #8]
IT	AL
BAL	L_UpdateInputs16
L_UpdateInputs15:
MOVW	R1, #1024
STRH	R1, [SP, #8]
L_UpdateInputs16:
LDRH	R1, [SP, #8]
STRH	R1, [R2, #0]
;I_O.c,133 :: 		if (CurrentInputValues[nIndex].TargetCircle)
LSLS	R2, R4, #3
MOVW	R1, #lo_addr(_CurrentInputValues+0)
MOVT	R1, #hi_addr(_CurrentInputValues+0)
ADDS	R1, R1, R2
ADDS	R1, R1, #4
LDR	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_UpdateInputs17
;I_O.c,134 :: 		DrawCircle(CurrentInputValues[nIndex].TargetCircle);
LSLS	R2, R4, #3
MOVW	R1, #lo_addr(_CurrentInputValues+0)
MOVT	R1, #hi_addr(_CurrentInputValues+0)
ADDS	R1, R1, R2
ADDS	R1, R1, #4
LDR	R1, [R1, #0]
STRB	R0, [SP, #4]
STRB	R4, [SP, #5]
MOV	R0, R1
BL	_DrawCircle+0
LDRB	R4, [SP, #5]
LDRB	R0, [SP, #4]
L_UpdateInputs17:
;I_O.c,136 :: 		CurrentInputValues[nIndex].bActive = !CurrentInputValues[nIndex].bActive;
LSLS	R2, R4, #3
MOVW	R1, #lo_addr(_CurrentInputValues+0)
MOVT	R1, #hi_addr(_CurrentInputValues+0)
ADDS	R2, R1, R2
LDRB	R1, [R2, #0]
CMP	R1, #0
MOVW	R1, #0
BNE	L__UpdateInputs27
MOVS	R1, #1
L__UpdateInputs27:
STRB	R1, [R2, #0]
;I_O.c,137 :: 		}
L_UpdateInputs14:
;I_O.c,126 :: 		for (nIndex = 0; nIndex < MAX_INPUT; nIndex++)
ADDS	R4, R4, #1
UXTB	R4, R4
;I_O.c,138 :: 		}
; nNewValue end address is: 0 (R0)
; nIndex end address is: 16 (R4)
IT	AL
BAL	L_UpdateInputs11
L_UpdateInputs12:
;I_O.c,141 :: 		}
L_end_UpdateInputs:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _UpdateInputs
_GetInputStates:
;I_O.c,147 :: 		void GetInputStates()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;I_O.c,149 :: 		nAddress[1] = I2C_PORT_B;
MOVS	R1, #19
MOVW	R0, #lo_addr(_nAddress+1)
MOVT	R0, #hi_addr(_nAddress+1)
STRB	R1, [R0, #0]
;I_O.c,150 :: 		nAddress[0] = I2C_PORT_B;
MOVS	R1, #19
MOVW	R0, #lo_addr(_nAddress+0)
MOVT	R0, #hi_addr(_nAddress+0)
STRB	R1, [R0, #0]
;I_O.c,151 :: 		I2C1_Start();
BL	_I2C1_Start+0
;I_O.c,152 :: 		I2C1_Write(nDeviceAddress, nAddress, 1, END_MODE_RESTART);
MOVW	R0, #lo_addr(_nDeviceAddress+0)
MOVT	R0, #hi_addr(_nDeviceAddress+0)
LDRB	R0, [R0, #0]
MOVW	R3, #0
MOVS	R2, #1
MOVW	R1, #lo_addr(_nAddress+0)
MOVT	R1, #hi_addr(_nAddress+0)
BL	_I2C1_Write+0
;I_O.c,154 :: 		I2C1_Read(nDeviceAddress, buffer, 2, END_MODE_STOP);
MOVW	R0, #lo_addr(_nDeviceAddress+0)
MOVT	R0, #hi_addr(_nDeviceAddress+0)
LDRB	R0, [R0, #0]
MOVW	R3, #1
MOVS	R2, #2
MOVW	R1, #lo_addr(_buffer+0)
MOVT	R1, #hi_addr(_buffer+0)
BL	_I2C1_Read+0
;I_O.c,156 :: 		if (nOldInputsValue != buffer[0])
MOVW	R0, #lo_addr(_buffer+0)
MOVT	R0, #hi_addr(_buffer+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_nOldInputsValue+0)
MOVT	R0, #hi_addr(_nOldInputsValue+0)
LDRB	R0, [R0, #0]
CMP	R0, R1
IT	EQ
BEQ	L_GetInputStates18
;I_O.c,157 :: 		UpdateInputs(buffer[0]);
MOVW	R0, #lo_addr(_buffer+0)
MOVT	R0, #hi_addr(_buffer+0)
LDRB	R0, [R0, #0]
BL	_UpdateInputs+0
L_GetInputStates18:
;I_O.c,158 :: 		nOldInputsValue = buffer[0];
MOVW	R0, #lo_addr(_buffer+0)
MOVT	R0, #hi_addr(_buffer+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_nOldInputsValue+0)
MOVT	R0, #hi_addr(_nOldInputsValue+0)
STRB	R1, [R0, #0]
;I_O.c,159 :: 		}
L_end_GetInputStates:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _GetInputStates
I_O____?ag:
SUB	SP, SP, #4
L_end_I_O___?ag:
ADD	SP, SP, #4
BX	LR
; end of I_O____?ag
