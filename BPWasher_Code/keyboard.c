// Display keyboard on STM32
// TCU project
// Created by Stefan James on 06/01/2016

#include "BPWasher_objects.h"
#include "BPWasher_resources.h"
#include "built_in.h"
#include "Constants.h"
#include "SF_driver.h"

#define LABEL_FONT_COLOUR 0xFFE0
#define ERROR "error"
#define IS_ERROR (*kp_display_Caption == 'e')

char bDecimalPlacePressed;
char bNumberOfDecPlaces;
char nMaxNumberOfDecimalPlaces;

stParams *ValueToSet;
float fNew_Value = 0.0;
float fMin, fMax;
char *sTitle;
char formatString[8];

void set_and_format_value(stParams *StpValue, float fValue)
     {
     StpValue->fvalue = fValue;
     if( (StpValue->DisplayToUpdate == config_drain_value_label_Caption) || (StpValue->DisplayToUpdate == config_fill_value_label_Caption))
       {
        strcpy(formatString,"%.0f s");
       } else
       {
        sprintf(formatString,"%%3.%df", StpValue->nDecimalPlaces);
       }
     sprintf(StpValue->DisplayToUpdate, formatString, fValue);
     }

void error(char *msg)
     {
     ResetKeypad();
     strcpy(&kp_display_Caption, ERROR);
     DrawButton(&kp_display);
     
     lblKeyboardTitle.Font_Color = CL_BLACK;
     DrawLabel(&lblKeyboardTitle);
     lblKeyboardTitle.Font_Color = LABEL_FONT_COLOUR;
     lblKeyboardTitle.Caption = msg;
     DrawLabel(&lblKeyboardTitle);
     }

void enter_pressed()
    {
    if (ValueToSet == &fMeasuredHigh && (fNew_Value < 58 || fNew_Value > 62)) {
       error("Unexpected value, contact MPBE");
    }
    if (ValueToSet == &fMeasuredLow && (fNew_Value < 35 || fNew_Value > 39)) {
       error("Unexpected value, contact MPBE");
    }
    if (ValueToSet == &fPassword)
      {
      if (fNew_Value == 6776)
         DrawScreen(&settings);
      else
          error("Incorrect password");
      } 
    else if (!IS_ERROR)
      {
        // update previous screen
        set_and_format_value(ValueToSet, fNew_Value);
        
        // save new value
        UpDateMemory();

        // return to previous screen
        ret_prev_screen();
      }
    }

void ret_prev_screen()
     {
     DrawScreen(ValueToSet->CallingDisplay);
     }
  
void ResetKeypad()
    {
    *kp_display.Caption = 0;
    fNew_Value = 0;
    bDecimalPlacePressed = FALSE;
    bNumberOfDecPlaces = 0;
    // remove previous title - could be an error message
    lblKeyboardTitle.Font_Color = CL_BLACK;
    DrawLabel(&lblKeyboardTitle);
    // draw new title
    lblKeyboardTitle.Font_Color = LABEL_FONT_COLOUR;
    lblKeyboardTitle.Caption = sTitle;
    DrawLabel(&lblKeyboardTitle);
    DrawButton(&kp_display);
    }

/******************************************************************************/
void Display_Keyboard(char *caption, stParams *StpValue)
     {
     ValueToSet =  StpValue;
     nMaxNumberOfDecimalPlaces  = ValueToSet->nDecimalPlaces;
     fMin = ValueToSet->fValueMin;
     fMax = ValueToSet->fValueMax;
     pos_neg.Visible = fMin < 0 ? 1 : 0;
     Key_dp.Visible = nMaxNumberOfDecimalPlaces ? 1 : 0;

     sTitle = caption; // store a copy so it can be repainted when clearing an error
     ResetKeypad();

     DrawScreen(&Keypad);
    }

/******************************************************************************/




/******************************************************************************/
void String_to_num(char nNumber)
     {
     char sTempNumber[10];

     if (IS_ERROR)  // this is true when the caption shows an error state
        ResetKeypad();

     if (nNumber == '-')
        {
        if (*kp_display_Caption == '-')
           strcpy(/*dest*/ kp_display_Caption, /*source*/ kp_display_Caption+1); // remove leading '-'
        else
           {
           sprintf(sTempNumber, "-%s", kp_display_Caption);
           strcpy(kp_display_Caption, sTempNumber);
           }
        nNumber = NULL;
        }

     if (nNumber == '.' && bDecimalPlacePressed)
        return;
     else if (nNumber == '.')
        bDecimalPlacePressed = TRUE;
     else if (bDecimalPlacePressed && nNumber)
        if (++bNumberOfDecPlaces > nMaxNumberOfDecimalPlaces)
           {
           error("Too many decimal places");
           return;
           }

     if (nNumber)
        {
        sTempNumber[0] = nNumber;
        sTempNumber[1] = NULL;

        strcat(kp_display_Caption, sTempNumber); // concatenate next entered digit to display caption
        }

     fNew_Value = atof(kp_display_Caption);  // convert entered value from string to number
     
     if (strlen(kp_display_Caption) > 5)
        error("Too long");
     else if (fNew_Value > fMax)
        error("Too high");
     else if (fNew_Value < fMin)
        error("Too low");
     else
        DrawButton(&kp_display);
     }