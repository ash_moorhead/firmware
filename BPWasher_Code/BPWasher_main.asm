_UpDateMemory:
;BPWasher_main.c,128 :: 		void UpDateMemory()
SUB	SP, SP, #16
STR	LR, [SP, #0]
;BPWasher_main.c,130 :: 		unsigned char nChar = MAGIC_NUMBER;
MOVS	R0, #170
STRB	R0, [SP, #6]
;BPWasher_main.c,131 :: 		unsigned int build = BUILD;
MOVW	R0, #lo_addr(?lstr1_BPWasher_main+0)
MOVT	R0, #hi_addr(?lstr1_BPWasher_main+0)
BL	_atoi+0
MOVW	R1, #1000
SXTH	R1, R1
MULS	R0, R1, R0
STRH	R0, [SP, #12]
MOVW	R0, #lo_addr(?lstr2_BPWasher_main+0)
MOVT	R0, #hi_addr(?lstr2_BPWasher_main+0)
BL	_atoi+0
LDRSH	R1, [SP, #12]
ADDS	R0, R1, R0
STRH	R0, [SP, #4]
;BPWasher_main.c,132 :: 		SerialFlash_SectorErase(0x00000); // each sector is 256 pages of 256 bytes = 64 kByte = 512 kBit
MOVS	R0, #0
BL	_SerialFlash_SectorErase+0
;BPWasher_main.c,133 :: 		SerialFlash_WriteByte(nChar, MEMORY_ACTIVE);
MOVS	R1, #0
LDRB	R0, [SP, #6]
BL	_SerialFlash_WriteByte+0
;BPWasher_main.c,136 :: 		SerialFlash_WriteArray(P_MEMORY, &P_value_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_P_value_Caption+0)
MOVT	R1, #hi_addr(_P_value_Caption+0)
MOVS	R0, #8
BL	_SerialFlash_WriteArray+0
;BPWasher_main.c,137 :: 		SerialFlash_WriteArray(I_MEMORY, &I_value_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_I_value_Caption+0)
MOVT	R1, #hi_addr(_I_value_Caption+0)
MOVS	R0, #15
BL	_SerialFlash_WriteArray+0
;BPWasher_main.c,138 :: 		SerialFlash_WriteArray(D_MEMORY, &D_value_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_D_value_Caption+0)
MOVT	R1, #hi_addr(_D_value_Caption+0)
MOVS	R0, #22
BL	_SerialFlash_WriteArray+0
;BPWasher_main.c,139 :: 		SerialFlash_WriteArray(FACTOR_MEMORY, &cal_factor_value_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_cal_factor_value_Caption+0)
MOVT	R1, #hi_addr(_cal_factor_value_Caption+0)
MOVS	R0, #36
BL	_SerialFlash_WriteArray+0
;BPWasher_main.c,140 :: 		SerialFlash_WriteArray(OFFSET_MEMORY, &cal_offset_value_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_cal_offset_value_Caption+0)
MOVT	R1, #hi_addr(_cal_offset_value_Caption+0)
MOVS	R0, #29
BL	_SerialFlash_WriteArray+0
;BPWasher_main.c,141 :: 		SerialFlash_WriteArray(CYCLE_COMPLETE, &CyclesComplete, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_CyclesComplete+0)
MOVT	R1, #hi_addr(_CyclesComplete+0)
MOVS	R0, #43
BL	_SerialFlash_WriteArray+0
;BPWasher_main.c,142 :: 		SerialFlash_WriteArray(CYCLE_FAILED, &CyclesFail, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_CyclesFail+0)
MOVT	R1, #hi_addr(_CyclesFail+0)
MOVS	R0, #50
BL	_SerialFlash_WriteArray+0
;BPWasher_main.c,143 :: 		SerialFlash_WriteArray(FILL_MEMORY, &config_fill_value_label_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_config_fill_value_label_Caption+0)
MOVT	R1, #hi_addr(_config_fill_value_label_Caption+0)
MOVW	R0, #559
BL	_SerialFlash_WriteArray+0
;BPWasher_main.c,144 :: 		SerialFlash_WriteArray(DRAIN_MEMORY, &config_drain_value_label_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_config_drain_value_label_Caption+0)
MOVT	R1, #hi_addr(_config_drain_value_label_Caption+0)
MOVW	R0, #566
BL	_SerialFlash_WriteArray+0
;BPWasher_main.c,146 :: 		SerialFlash_WriteWord(build, BUILD_MEMORY);
MOVS	R1, #57
LDRH	R0, [SP, #4]
BL	_SerialFlash_WriteWord+0
;BPWasher_main.c,147 :: 		SerialFlash_WriteArray(ERRLOG_MEMORY, &sErrorLog, ERRLOG_TOTAL_LEN);
MOVW	R2, #500
MOVW	R1, #lo_addr(_sErrorLog+0)
MOVT	R1, #hi_addr(_sErrorLog+0)
MOVS	R0, #59
BL	_SerialFlash_WriteArray+0
;BPWasher_main.c,148 :: 		}
L_end_UpDateMemory:
LDR	LR, [SP, #0]
ADD	SP, SP, #16
BX	LR
; end of _UpDateMemory
_init_variables:
;BPWasher_main.c,150 :: 		void init_variables()
SUB	SP, SP, #16
STR	LR, [SP, #0]
;BPWasher_main.c,154 :: 		SerialFlashID = SerialFlash_ReadID();
BL	_SerialFlash_ReadID+0
MOVW	R1, #lo_addr(_SerialFlashID+0)
MOVT	R1, #hi_addr(_SerialFlashID+0)
STRB	R0, [R1, #0]
;BPWasher_main.c,155 :: 		nMemoryActive = SerialFlash_ReadByte(MEMORY_ACTIVE);
MOVS	R0, #0
BL	_SerialFlash_ReadByte+0
MOVW	R1, #lo_addr(_nMemoryActive+0)
MOVT	R1, #hi_addr(_nMemoryActive+0)
STRB	R0, [R1, #0]
;BPWasher_main.c,156 :: 		if ( nMemoryActive == MAGIC_NUMBER)
CMP	R0, #170
IT	NE
BNE	L_init_variables0
;BPWasher_main.c,159 :: 		SerialFlash_ReadArray(P_MEMORY, &P_value_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_P_value_Caption+0)
MOVT	R1, #hi_addr(_P_value_Caption+0)
MOVS	R0, #8
BL	_SerialFlash_ReadArray+0
;BPWasher_main.c,160 :: 		SerialFlash_ReadArray(I_MEMORY, &I_value_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_I_value_Caption+0)
MOVT	R1, #hi_addr(_I_value_Caption+0)
MOVS	R0, #15
BL	_SerialFlash_ReadArray+0
;BPWasher_main.c,161 :: 		SerialFlash_ReadArray(D_MEMORY, &D_value_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_D_value_Caption+0)
MOVT	R1, #hi_addr(_D_value_Caption+0)
MOVS	R0, #22
BL	_SerialFlash_ReadArray+0
;BPWasher_main.c,162 :: 		SerialFlash_ReadArray(FACTOR_MEMORY, &cal_factor_value_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_cal_factor_value_Caption+0)
MOVT	R1, #hi_addr(_cal_factor_value_Caption+0)
MOVS	R0, #36
BL	_SerialFlash_ReadArray+0
;BPWasher_main.c,163 :: 		SerialFlash_ReadArray(OFFSET_MEMORY, &cal_offset_value_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_cal_offset_value_Caption+0)
MOVT	R1, #hi_addr(_cal_offset_value_Caption+0)
MOVS	R0, #29
BL	_SerialFlash_ReadArray+0
;BPWasher_main.c,164 :: 		SerialFlash_ReadArray(CYCLE_COMPLETE, &CyclesComplete, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_CyclesComplete+0)
MOVT	R1, #hi_addr(_CyclesComplete+0)
MOVS	R0, #43
BL	_SerialFlash_ReadArray+0
;BPWasher_main.c,165 :: 		SerialFlash_ReadArray(CYCLE_FAILED, &CyclesFail, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_CyclesFail+0)
MOVT	R1, #hi_addr(_CyclesFail+0)
MOVS	R0, #50
BL	_SerialFlash_ReadArray+0
;BPWasher_main.c,166 :: 		stored_memory_build = SerialFlash_ReadWord(BUILD_MEMORY);
MOVS	R0, #57
BL	_SerialFlash_ReadWord+0
STRH	R0, [SP, #4]
;BPWasher_main.c,170 :: 		}
IT	AL
BAL	L_init_variables1
L_init_variables0:
;BPWasher_main.c,172 :: 		stored_memory_build = 0;
MOVS	R0, #0
STRH	R0, [SP, #4]
L_init_variables1:
;BPWasher_main.c,174 :: 		fSetTemp = 85;
MOVW	R0, #0
MOVT	R0, #17066
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,176 :: 		fProportional.fvalue = atof(P_value_Caption);
MOVW	R0, #lo_addr(_P_value_Caption+0)
MOVT	R0, #hi_addr(_P_value_Caption+0)
BL	_atof+0
MOVW	R0, #lo_addr(_fProportional+4)
MOVT	R0, #hi_addr(_fProportional+4)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,177 :: 		fProportional.fValueMax = 500;
MOVW	R0, #0
MOVT	R0, #17402
VMOV	S0, R0
MOVW	R0, #lo_addr(_fProportional+8)
MOVT	R0, #hi_addr(_fProportional+8)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,178 :: 		fProportional.fValueMin = 1;
VMOV.F32	S0, #1
MOVW	R0, #lo_addr(_fProportional+12)
MOVT	R0, #hi_addr(_fProportional+12)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,179 :: 		fProportional.CallingDisplay = &PID;
MOVW	R1, #lo_addr(_PID+0)
MOVT	R1, #hi_addr(_PID+0)
MOVW	R0, #lo_addr(_fProportional+16)
MOVT	R0, #hi_addr(_fProportional+16)
STR	R1, [R0, #0]
;BPWasher_main.c,180 :: 		fProportional.DisplayToUpdate = P_value_Caption;
MOVW	R1, #lo_addr(_P_value_Caption+0)
MOVT	R1, #hi_addr(_P_value_Caption+0)
MOVW	R0, #lo_addr(_fProportional+20)
MOVT	R0, #hi_addr(_fProportional+20)
STR	R1, [R0, #0]
;BPWasher_main.c,181 :: 		fProportional.nDecimalPlaces = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_fProportional+24)
MOVT	R0, #hi_addr(_fProportional+24)
STRB	R1, [R0, #0]
;BPWasher_main.c,183 :: 		fIntegral.fvalue = atof(I_value_Caption);
MOVW	R0, #lo_addr(_I_value_Caption+0)
MOVT	R0, #hi_addr(_I_value_Caption+0)
BL	_atof+0
MOVW	R0, #lo_addr(_fIntegral+4)
MOVT	R0, #hi_addr(_fIntegral+4)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,184 :: 		fIntegral.fValueMax = 500;
MOVW	R0, #0
MOVT	R0, #17402
VMOV	S0, R0
MOVW	R0, #lo_addr(_fIntegral+8)
MOVT	R0, #hi_addr(_fIntegral+8)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,185 :: 		fIntegral.fValueMin = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fIntegral+12)
MOVT	R0, #hi_addr(_fIntegral+12)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,186 :: 		fIntegral.CallingDisplay = &PID;
MOVW	R1, #lo_addr(_PID+0)
MOVT	R1, #hi_addr(_PID+0)
MOVW	R0, #lo_addr(_fIntegral+16)
MOVT	R0, #hi_addr(_fIntegral+16)
STR	R1, [R0, #0]
;BPWasher_main.c,187 :: 		fIntegral.DisplayToUpdate = I_value_Caption;
MOVW	R1, #lo_addr(_I_value_Caption+0)
MOVT	R1, #hi_addr(_I_value_Caption+0)
MOVW	R0, #lo_addr(_fIntegral+20)
MOVT	R0, #hi_addr(_fIntegral+20)
STR	R1, [R0, #0]
;BPWasher_main.c,188 :: 		fIntegral.nDecimalPlaces = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_fIntegral+24)
MOVT	R0, #hi_addr(_fIntegral+24)
STRB	R1, [R0, #0]
;BPWasher_main.c,190 :: 		fDerivative.fvalue = atof(D_value_Caption);
MOVW	R0, #lo_addr(_D_value_Caption+0)
MOVT	R0, #hi_addr(_D_value_Caption+0)
BL	_atof+0
MOVW	R0, #lo_addr(_fDerivative+4)
MOVT	R0, #hi_addr(_fDerivative+4)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,191 :: 		fDerivative.fValueMax = 500;
MOVW	R0, #0
MOVT	R0, #17402
VMOV	S0, R0
MOVW	R0, #lo_addr(_fDerivative+8)
MOVT	R0, #hi_addr(_fDerivative+8)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,192 :: 		fDerivative.fValueMin = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fDerivative+12)
MOVT	R0, #hi_addr(_fDerivative+12)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,193 :: 		fDerivative.CallingDisplay = &PID;
MOVW	R1, #lo_addr(_PID+0)
MOVT	R1, #hi_addr(_PID+0)
MOVW	R0, #lo_addr(_fDerivative+16)
MOVT	R0, #hi_addr(_fDerivative+16)
STR	R1, [R0, #0]
;BPWasher_main.c,194 :: 		fDerivative.DisplayToUpdate = D_value_Caption;
MOVW	R1, #lo_addr(_D_value_Caption+0)
MOVT	R1, #hi_addr(_D_value_Caption+0)
MOVW	R0, #lo_addr(_fDerivative+20)
MOVT	R0, #hi_addr(_fDerivative+20)
STR	R1, [R0, #0]
;BPWasher_main.c,195 :: 		fDerivative.nDecimalPlaces = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(_fDerivative+24)
MOVT	R0, #hi_addr(_fDerivative+24)
STRB	R1, [R0, #0]
;BPWasher_main.c,197 :: 		fCal_offset.fvalue = atof(cal_offset_value_Caption);
MOVW	R0, #lo_addr(_cal_offset_value_Caption+0)
MOVT	R0, #hi_addr(_cal_offset_value_Caption+0)
BL	_atof+0
MOVW	R0, #lo_addr(_fCal_offset+4)
MOVT	R0, #hi_addr(_fCal_offset+4)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,198 :: 		fCal_offset.fValueMax = 100;
MOVW	R0, #0
MOVT	R0, #17096
VMOV	S0, R0
MOVW	R0, #lo_addr(_fCal_offset+8)
MOVT	R0, #hi_addr(_fCal_offset+8)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,199 :: 		fCal_offset.fValueMin = -100;
MOVW	R0, #0
MOVT	R0, #49864
VMOV	S0, R0
MOVW	R0, #lo_addr(_fCal_offset+12)
MOVT	R0, #hi_addr(_fCal_offset+12)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,200 :: 		fCal_offset.CallingDisplay = &Calibration;
MOVW	R1, #lo_addr(_Calibration+0)
MOVT	R1, #hi_addr(_Calibration+0)
MOVW	R0, #lo_addr(_fCal_offset+16)
MOVT	R0, #hi_addr(_fCal_offset+16)
STR	R1, [R0, #0]
;BPWasher_main.c,201 :: 		fCal_offset.DisplayToUpdate = cal_offset_value_Caption;
MOVW	R1, #lo_addr(_cal_offset_value_Caption+0)
MOVT	R1, #hi_addr(_cal_offset_value_Caption+0)
MOVW	R0, #lo_addr(_fCal_offset+20)
MOVT	R0, #hi_addr(_fCal_offset+20)
STR	R1, [R0, #0]
;BPWasher_main.c,202 :: 		fCal_offset.nDecimalPlaces = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_fCal_offset+24)
MOVT	R0, #hi_addr(_fCal_offset+24)
STRB	R1, [R0, #0]
;BPWasher_main.c,204 :: 		fCal_factor.fvalue = atof(cal_factor_value_Caption);
MOVW	R0, #lo_addr(_cal_factor_value_Caption+0)
MOVT	R0, #hi_addr(_cal_factor_value_Caption+0)
BL	_atof+0
MOVW	R0, #lo_addr(_fCal_factor+4)
MOVT	R0, #hi_addr(_fCal_factor+4)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,205 :: 		fCal_factor.fValueMax = 100;
MOVW	R0, #0
MOVT	R0, #17096
VMOV	S0, R0
MOVW	R0, #lo_addr(_fCal_factor+8)
MOVT	R0, #hi_addr(_fCal_factor+8)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,206 :: 		fCal_factor.fValueMin = -100;
MOVW	R0, #0
MOVT	R0, #49864
VMOV	S0, R0
MOVW	R0, #lo_addr(_fCal_factor+12)
MOVT	R0, #hi_addr(_fCal_factor+12)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,207 :: 		fCal_factor.CallingDisplay = &Calibration;
MOVW	R1, #lo_addr(_Calibration+0)
MOVT	R1, #hi_addr(_Calibration+0)
MOVW	R0, #lo_addr(_fCal_factor+16)
MOVT	R0, #hi_addr(_fCal_factor+16)
STR	R1, [R0, #0]
;BPWasher_main.c,208 :: 		fCal_factor.DisplayToUpdate = cal_factor_value_Caption;
MOVW	R1, #lo_addr(_cal_factor_value_Caption+0)
MOVT	R1, #hi_addr(_cal_factor_value_Caption+0)
MOVW	R0, #lo_addr(_fCal_factor+20)
MOVT	R0, #hi_addr(_fCal_factor+20)
STR	R1, [R0, #0]
;BPWasher_main.c,209 :: 		fCal_factor.nDecimalPlaces = 3;
MOVS	R1, #3
MOVW	R0, #lo_addr(_fCal_factor+24)
MOVT	R0, #hi_addr(_fCal_factor+24)
STRB	R1, [R0, #0]
;BPWasher_main.c,211 :: 		fCyclesComplete = atof(CyclesComplete);
MOVW	R0, #lo_addr(_CyclesComplete+0)
MOVT	R0, #hi_addr(_CyclesComplete+0)
BL	_atof+0
MOVW	R0, #lo_addr(_fCyclesComplete+0)
MOVT	R0, #hi_addr(_fCyclesComplete+0)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,212 :: 		fCyclesFail =   atof(CyclesFail);
MOVW	R0, #lo_addr(_CyclesFail+0)
MOVT	R0, #hi_addr(_CyclesFail+0)
BL	_atof+0
MOVW	R0, #lo_addr(_fCyclesFail+0)
MOVT	R0, #hi_addr(_fCyclesFail+0)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,214 :: 		fPassword.fvalue = 0;              // was 6776 which allowed access to settings without entering any password! - CM 14/07/17
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fPassword+4)
MOVT	R0, #hi_addr(_fPassword+4)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,215 :: 		fPassword.fValueMax = 99999;
MOVW	R0, #20352
MOVT	R0, #18371
VMOV	S0, R0
MOVW	R0, #lo_addr(_fPassword+8)
MOVT	R0, #hi_addr(_fPassword+8)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,216 :: 		fPassword.fValueMin = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fPassword+12)
MOVT	R0, #hi_addr(_fPassword+12)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,217 :: 		fPassword.CallingDisplay = &_main;
MOVW	R1, #lo_addr(__main+0)
MOVT	R1, #hi_addr(__main+0)
MOVW	R0, #lo_addr(_fPassword+16)
MOVT	R0, #hi_addr(_fPassword+16)
STR	R1, [R0, #0]
;BPWasher_main.c,218 :: 		fPassword.DisplayToUpdate = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_fPassword+20)
MOVT	R0, #hi_addr(_fPassword+20)
STR	R1, [R0, #0]
;BPWasher_main.c,219 :: 		fPassword.nDecimalPlaces = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_fPassword+24)
MOVT	R0, #hi_addr(_fPassword+24)
STRB	R1, [R0, #0]
;BPWasher_main.c,222 :: 		if ( nMemoryActive == MAGIC_NUMBER && stored_memory_build >= 1 )
MOVW	R0, #lo_addr(_nMemoryActive+0)
MOVT	R0, #hi_addr(_nMemoryActive+0)
LDRB	R0, [R0, #0]
CMP	R0, #170
IT	NE
BNE	L__init_variables38
LDRH	R0, [SP, #4]
CMP	R0, #1
IT	CC
BCC	L__init_variables37
L__init_variables36:
;BPWasher_main.c,224 :: 		SerialFlash_ReadArray(ERRLOG_MEMORY, &sErrorLog, ERRLOG_TOTAL_LEN);
MOVW	R2, #500
MOVW	R1, #lo_addr(_sErrorLog+0)
MOVT	R1, #hi_addr(_sErrorLog+0)
MOVS	R0, #59
BL	_SerialFlash_ReadArray+0
;BPWasher_main.c,225 :: 		}
IT	AL
BAL	L_init_variables5
;BPWasher_main.c,222 :: 		if ( nMemoryActive == MAGIC_NUMBER && stored_memory_build >= 1 )
L__init_variables38:
L__init_variables37:
;BPWasher_main.c,229 :: 		memset(sErrorLog, 0, ERRLOG_TOTAL_LEN);
MOVW	R2, #500
SXTH	R2, R2
MOVS	R1, #0
MOVW	R0, #lo_addr(_sErrorLog+0)
MOVT	R0, #hi_addr(_sErrorLog+0)
BL	_memset+0
;BPWasher_main.c,230 :: 		}
L_init_variables5:
;BPWasher_main.c,233 :: 		if ( nMemoryActive == MAGIC_NUMBER && stored_memory_build >= 1004 )
MOVW	R0, #lo_addr(_nMemoryActive+0)
MOVT	R0, #hi_addr(_nMemoryActive+0)
LDRB	R0, [R0, #0]
CMP	R0, #170
IT	NE
BNE	L__init_variables40
LDRH	R0, [SP, #4]
CMP	R0, #1004
IT	CC
BCC	L__init_variables39
L__init_variables35:
;BPWasher_main.c,235 :: 		SerialFlash_ReadArray(FILL_MEMORY, &config_fill_value_label_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_config_fill_value_label_Caption+0)
MOVT	R1, #hi_addr(_config_fill_value_label_Caption+0)
MOVW	R0, #559
BL	_SerialFlash_ReadArray+0
;BPWasher_main.c,236 :: 		SerialFlash_ReadArray(DRAIN_MEMORY, &config_drain_value_label_Caption, CAPTION_SIZE);
MOVS	R2, #7
MOVW	R1, #lo_addr(_config_drain_value_label_Caption+0)
MOVT	R1, #hi_addr(_config_drain_value_label_Caption+0)
MOVW	R0, #566
BL	_SerialFlash_ReadArray+0
;BPWasher_main.c,237 :: 		}
IT	AL
BAL	L_init_variables9
;BPWasher_main.c,233 :: 		if ( nMemoryActive == MAGIC_NUMBER && stored_memory_build >= 1004 )
L__init_variables40:
L__init_variables39:
;BPWasher_main.c,241 :: 		sprintf(config_fill_value_label_Caption, "5s");
MOVW	R1, #lo_addr(?lstr_3_BPWasher_main+0)
MOVT	R1, #hi_addr(?lstr_3_BPWasher_main+0)
MOVW	R0, #lo_addr(_config_fill_value_label_Caption+0)
MOVT	R0, #hi_addr(_config_fill_value_label_Caption+0)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #8
;BPWasher_main.c,242 :: 		sprintf(config_drain_value_label_Caption, "60s");
MOVW	R1, #lo_addr(?lstr_4_BPWasher_main+0)
MOVT	R1, #hi_addr(?lstr_4_BPWasher_main+0)
MOVW	R0, #lo_addr(_config_drain_value_label_Caption+0)
MOVT	R0, #hi_addr(_config_drain_value_label_Caption+0)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #8
;BPWasher_main.c,243 :: 		}
L_init_variables9:
;BPWasher_main.c,245 :: 		fExtraFillTime.fvalue = atof(config_fill_value_label_Caption);
MOVW	R0, #lo_addr(_config_fill_value_label_Caption+0)
MOVT	R0, #hi_addr(_config_fill_value_label_Caption+0)
BL	_atof+0
MOVW	R0, #lo_addr(_fExtraFillTime+4)
MOVT	R0, #hi_addr(_fExtraFillTime+4)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,246 :: 		fExtraFillTime.fValueMax = 999;
MOVW	R0, #49152
MOVT	R0, #17529
VMOV	S0, R0
MOVW	R0, #lo_addr(_fExtraFillTime+8)
MOVT	R0, #hi_addr(_fExtraFillTime+8)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,247 :: 		fExtraFillTime.fValueMin = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fExtraFillTime+12)
MOVT	R0, #hi_addr(_fExtraFillTime+12)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,248 :: 		fExtraFillTime.CallingDisplay = &Config;
MOVW	R1, #lo_addr(_Config+0)
MOVT	R1, #hi_addr(_Config+0)
MOVW	R0, #lo_addr(_fExtraFillTime+16)
MOVT	R0, #hi_addr(_fExtraFillTime+16)
STR	R1, [R0, #0]
;BPWasher_main.c,249 :: 		fExtraFillTime.DisplayToUpdate = config_fill_value_label_Caption;
MOVW	R1, #lo_addr(_config_fill_value_label_Caption+0)
MOVT	R1, #hi_addr(_config_fill_value_label_Caption+0)
MOVW	R0, #lo_addr(_fExtraFillTime+20)
MOVT	R0, #hi_addr(_fExtraFillTime+20)
STR	R1, [R0, #0]
;BPWasher_main.c,250 :: 		fExtraFillTime.nDecimalPlaces = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_fExtraFillTime+24)
MOVT	R0, #hi_addr(_fExtraFillTime+24)
STRB	R1, [R0, #0]
;BPWasher_main.c,252 :: 		fDrainTime.fvalue = atof(config_drain_value_label_Caption);
MOVW	R0, #lo_addr(_config_drain_value_label_Caption+0)
MOVT	R0, #hi_addr(_config_drain_value_label_Caption+0)
BL	_atof+0
MOVW	R0, #lo_addr(_fDrainTime+4)
MOVT	R0, #hi_addr(_fDrainTime+4)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,253 :: 		fDrainTime.fValueMax = 999;
MOVW	R0, #49152
MOVT	R0, #17529
VMOV	S0, R0
MOVW	R0, #lo_addr(_fDrainTime+8)
MOVT	R0, #hi_addr(_fDrainTime+8)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,254 :: 		fDrainTime.fValueMin = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fDrainTime+12)
MOVT	R0, #hi_addr(_fDrainTime+12)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,255 :: 		fDrainTime.CallingDisplay = &Config;
MOVW	R1, #lo_addr(_Config+0)
MOVT	R1, #hi_addr(_Config+0)
MOVW	R0, #lo_addr(_fDrainTime+16)
MOVT	R0, #hi_addr(_fDrainTime+16)
STR	R1, [R0, #0]
;BPWasher_main.c,256 :: 		fDrainTime.DisplayToUpdate = config_drain_value_label_Caption;
MOVW	R1, #lo_addr(_config_drain_value_label_Caption+0)
MOVT	R1, #hi_addr(_config_drain_value_label_Caption+0)
MOVW	R0, #lo_addr(_fDrainTime+20)
MOVT	R0, #hi_addr(_fDrainTime+20)
STR	R1, [R0, #0]
;BPWasher_main.c,257 :: 		fDrainTime.nDecimalPlaces = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(_fDrainTime+24)
MOVT	R0, #hi_addr(_fDrainTime+24)
STRB	R1, [R0, #0]
;BPWasher_main.c,260 :: 		if (stored_memory_build != BUILD )
MOVW	R0, #lo_addr(?lstr5_BPWasher_main+0)
MOVT	R0, #hi_addr(?lstr5_BPWasher_main+0)
BL	_atoi+0
MOVW	R1, #1000
SXTH	R1, R1
MULS	R0, R1, R0
STRH	R0, [SP, #12]
MOVW	R0, #lo_addr(?lstr6_BPWasher_main+0)
MOVT	R0, #hi_addr(?lstr6_BPWasher_main+0)
BL	_atoi+0
LDRSH	R1, [SP, #12]
ADDS	R1, R1, R0
SXTH	R1, R1
LDRH	R0, [SP, #4]
CMP	R0, R1
IT	EQ
BEQ	L_init_variables10
;BPWasher_main.c,262 :: 		ErrorLog_NewError("Version " VERSION " installed", STATUS_NORMAL);
MOVW	R0, #lo_addr(?lstr7_BPWasher_main+0)
MOVT	R0, #hi_addr(?lstr7_BPWasher_main+0)
MOVS	R1, #0
BL	_ErrorLog_NewError+0
;BPWasher_main.c,263 :: 		}
L_init_variables10:
;BPWasher_main.c,265 :: 		fSetTemp = 0;
MOV	R0, #0
VMOV	S0, R0
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VSTR	#1, S0, [R0, #0]
;BPWasher_main.c,267 :: 		}
L_end_init_variables:
LDR	LR, [SP, #0]
ADD	SP, SP, #16
BX	LR
; end of _init_variables
_SF_Start:
;BPWasher_main.c,281 :: 		void SF_Start()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_main.c,286 :: 		&_GPIO_MODULE_SPI3_PC10_11_12);
MOVW	R2, #lo_addr(__GPIO_MODULE_SPI3_PC10_11_12+0)
MOVT	R2, #hi_addr(__GPIO_MODULE_SPI3_PC10_11_12+0)
;BPWasher_main.c,285 :: 		_SPI_MSB_FIRST | _SPI_SS_DISABLE | _SPI_SSM_ENABLE | _SPI_SSI_1,
MOVW	R1, #772
;BPWasher_main.c,283 :: 		SPI3_Init_Advanced(_SPI_FPCLK_DIV2, _SPI_MASTER  | _SPI_8_BIT |
MOVS	R0, #0
;BPWasher_main.c,286 :: 		&_GPIO_MODULE_SPI3_PC10_11_12);
BL	_SPI3_Init_Advanced+0
;BPWasher_main.c,291 :: 		SerialFlash_init();
BL	_SerialFlash_init+0
;BPWasher_main.c,292 :: 		SerialFlash_WriteEnable();
BL	_SerialFlash_WriteEnable+0
;BPWasher_main.c,293 :: 		Delay_ms(500);
MOVW	R7, #2515
MOVT	R7, #356
NOP
NOP
L_SF_Start11:
SUBS	R7, R7, #1
BNE	L_SF_Start11
NOP
NOP
NOP
NOP
;BPWasher_main.c,294 :: 		}
L_end_SF_Start:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _SF_Start
_init_Ports:
;BPWasher_main.c,296 :: 		void init_Ports()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_main.c,299 :: 		GPIO_Config(&GPIOD_BASE, _GPIO_PINMASK_7, _GPIO_CFG_DIGITAL_OUTPUT);
MOVW	R2, #20
MOVT	R2, #8
MOVW	R1, #128
MOVW	R0, #lo_addr(GPIOD_BASE+0)
MOVT	R0, #hi_addr(GPIOD_BASE+0)
BL	_GPIO_Config+0
;BPWasher_main.c,300 :: 		GPIO_Digital_Output(&GPIOB_BASE, _GPIO_PINMASK_11);
MOVW	R1, #2048
MOVW	R0, #lo_addr(GPIOB_BASE+0)
MOVT	R0, #hi_addr(GPIOB_BASE+0)
BL	_GPIO_Digital_Output+0
;BPWasher_main.c,301 :: 		GPIO_Digital_Output(&GPIOB_BASE, _GPIO_PINMASK_12);
MOVW	R1, #4096
MOVW	R0, #lo_addr(GPIOB_BASE+0)
MOVT	R0, #hi_addr(GPIOB_BASE+0)
BL	_GPIO_Digital_Output+0
;BPWasher_main.c,302 :: 		GPIO_Digital_Output(&GPIOB_BASE, _GPIO_PINMASK_13);
MOVW	R1, #8192
MOVW	R0, #lo_addr(GPIOB_BASE+0)
MOVT	R0, #hi_addr(GPIOB_BASE+0)
BL	_GPIO_Digital_Output+0
;BPWasher_main.c,303 :: 		GPIO_Digital_Output(&GPIOB_BASE, _GPIO_PINMASK_14);
MOVW	R1, #16384
MOVW	R0, #lo_addr(GPIOB_BASE+0)
MOVT	R0, #hi_addr(GPIOB_BASE+0)
BL	_GPIO_Digital_Output+0
;BPWasher_main.c,304 :: 		GPIO_Digital_Input(&GPIOB_BASE, _GPIO_PINMASK_15);
MOVW	R1, #32768
MOVW	R0, #lo_addr(GPIOB_BASE+0)
MOVT	R0, #hi_addr(GPIOB_BASE+0)
BL	_GPIO_Digital_Input+0
;BPWasher_main.c,305 :: 		GPIO_Digital_Input(&GPIOD_BASE, _GPIO_PINMASK_0);
MOVW	R1, #1
MOVW	R0, #lo_addr(GPIOD_BASE+0)
MOVT	R0, #hi_addr(GPIOD_BASE+0)
BL	_GPIO_Digital_Input+0
;BPWasher_main.c,306 :: 		HEATER_OFF;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOD_ODR+0)
MOVT	R0, #hi_addr(GPIOD_ODR+0)
STR	R1, [R0, #0]
;BPWasher_main.c,307 :: 		SF_Start();
BL	_SF_Start+0
;BPWasher_main.c,308 :: 		}
L_end_init_Ports:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _init_Ports
_flash_status:
;BPWasher_main.c,313 :: 		void flash_status()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_main.c,317 :: 		indicator.Color = (indicator.Color== CL_BLACK) ? (fSetTemp>0 ? CL_LIME : 0xC618) : CL_BLACK ;
MOVW	R0, #lo_addr(_indicator+26)
MOVT	R0, #hi_addr(_indicator+26)
LDRH	R0, [R0, #0]
CMP	R0, #0
IT	NE
BNE	L_flash_status13
MOVW	R0, #lo_addr(_fSetTemp+0)
MOVT	R0, #hi_addr(_fSetTemp+0)
VLDR	#1, S0, [R0, #0]
VCMPE.F32	S0, #0
VMRS	#60, FPSCR
IT	LE
BLE	L_flash_status15
; ?FLOC___flash_status?T87 start address is: 4 (R1)
MOVW	R1, #2016
; ?FLOC___flash_status?T87 end address is: 4 (R1)
IT	AL
BAL	L_flash_status16
L_flash_status15:
; ?FLOC___flash_status?T87 start address is: 4 (R1)
MOVW	R1, #50712
; ?FLOC___flash_status?T87 end address is: 4 (R1)
L_flash_status16:
; ?FLOC___flash_status?T87 start address is: 4 (R1)
; ?FLOC___flash_status?T88 start address is: 0 (R0)
UXTH	R0, R1
; ?FLOC___flash_status?T87 end address is: 4 (R1)
UXTH	R1, R0
; ?FLOC___flash_status?T88 end address is: 0 (R0)
IT	AL
BAL	L_flash_status14
L_flash_status13:
; ?FLOC___flash_status?T88 start address is: 4 (R1)
MOVW	R1, #0
; ?FLOC___flash_status?T88 end address is: 4 (R1)
L_flash_status14:
; ?FLOC___flash_status?T88 start address is: 4 (R1)
MOVW	R0, #lo_addr(_indicator+26)
MOVT	R0, #hi_addr(_indicator+26)
STRH	R1, [R0, #0]
; ?FLOC___flash_status?T88 end address is: 4 (R1)
;BPWasher_main.c,318 :: 		DrawCircle(&indicator);
MOVW	R0, #lo_addr(_indicator+0)
MOVT	R0, #hi_addr(_indicator+0)
BL	_DrawCircle+0
;BPWasher_main.c,319 :: 		}
L_end_flash_status:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _flash_status
_InitTimer2:
;BPWasher_main.c,326 :: 		void InitTimer2()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;BPWasher_main.c,328 :: 		RCC_APB1ENR.TIM2EN = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(RCC_APB1ENR+0)
MOVT	R0, #hi_addr(RCC_APB1ENR+0)
STR	R1, [R0, #0]
;BPWasher_main.c,329 :: 		TIM2_CR1.CEN = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(TIM2_CR1+0)
MOVT	R0, #hi_addr(TIM2_CR1+0)
STR	R1, [R0, #0]
;BPWasher_main.c,330 :: 		TIM2_PSC = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(TIM2_PSC+0)
MOVT	R0, #hi_addr(TIM2_PSC+0)
STR	R1, [R0, #0]
;BPWasher_main.c,331 :: 		TIM2_ARR = 34760;
MOVW	R1, #34760
MOVW	R0, #lo_addr(TIM2_ARR+0)
MOVT	R0, #hi_addr(TIM2_ARR+0)
STR	R1, [R0, #0]
;BPWasher_main.c,332 :: 		NVIC_IntEnable(IVT_INT_TIM2);
MOVW	R0, #44
BL	_NVIC_IntEnable+0
;BPWasher_main.c,333 :: 		TIM2_DIER.UIE = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(TIM2_DIER+0)
MOVT	R0, #hi_addr(TIM2_DIER+0)
STR	R1, [R0, #0]
;BPWasher_main.c,334 :: 		TIM2_CR1.CEN = 1;
MOVW	R0, #lo_addr(TIM2_CR1+0)
MOVT	R0, #hi_addr(TIM2_CR1+0)
STR	R1, [R0, #0]
;BPWasher_main.c,335 :: 		}
L_end_InitTimer2:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _InitTimer2
_Timer2_interrupt:
;BPWasher_main.c,344 :: 		void Timer2_interrupt() iv IVT_INT_TIM2
;BPWasher_main.c,346 :: 		TIM2_SR.UIF = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(TIM2_SR+0)
MOVT	R0, #hi_addr(TIM2_SR+0)
STR	R1, [R0, #0]
;BPWasher_main.c,348 :: 		nADC_count++;
MOVW	R1, #lo_addr(_nADC_count+0)
MOVT	R1, #hi_addr(_nADC_count+0)
LDRSH	R0, [R1, #0]
ADDS	R2, R0, #1
SXTH	R2, R2
STRH	R2, [R1, #0]
;BPWasher_main.c,349 :: 		nSec_count++;
MOVW	R1, #lo_addr(_nSec_count+0)
MOVT	R1, #hi_addr(_nSec_count+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;BPWasher_main.c,350 :: 		nRefresh_count++;
MOVW	R1, #lo_addr(_nRefresh_count+0)
MOVT	R1, #hi_addr(_nRefresh_count+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;BPWasher_main.c,352 :: 		if (20 <= nADC_count)
CMP	R2, #20
IT	LT
BLT	L_Timer2_interrupt17
;BPWasher_main.c,354 :: 		bADC = TRUE;
MOVS	R1, #1
MOVW	R0, #lo_addr(_bADC+0)
MOVT	R0, #hi_addr(_bADC+0)
STRB	R1, [R0, #0]
;BPWasher_main.c,355 :: 		nADC_count = 0;
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(_nADC_count+0)
MOVT	R0, #hi_addr(_nADC_count+0)
STRH	R1, [R0, #0]
;BPWasher_main.c,356 :: 		}
L_Timer2_interrupt17:
;BPWasher_main.c,358 :: 		if (100 <= nRefresh_count)
MOVW	R0, #lo_addr(_nRefresh_count+0)
MOVT	R0, #hi_addr(_nRefresh_count+0)
LDRSH	R0, [R0, #0]
CMP	R0, #100
IT	LT
BLT	L_Timer2_interrupt18
;BPWasher_main.c,360 :: 		bRefresh = TRUE;
MOVS	R1, #1
MOVW	R0, #lo_addr(_bRefresh+0)
MOVT	R0, #hi_addr(_bRefresh+0)
STRB	R1, [R0, #0]
;BPWasher_main.c,361 :: 		nRefresh_count = 0;
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(_nRefresh_count+0)
MOVT	R0, #hi_addr(_nRefresh_count+0)
STRH	R1, [R0, #0]
;BPWasher_main.c,362 :: 		}
L_Timer2_interrupt18:
;BPWasher_main.c,365 :: 		if (500 <= nSec_count) // check half second
MOVW	R0, #lo_addr(_nSec_count+0)
MOVT	R0, #hi_addr(_nSec_count+0)
LDRSH	R0, [R0, #0]
CMP	R0, #500
IT	LT
BLT	L_Timer2_interrupt19
;BPWasher_main.c,367 :: 		bFLASH = TRUE;
MOVS	R1, #1
MOVW	R0, #lo_addr(_bFlash+0)
MOVT	R0, #hi_addr(_bFlash+0)
STRB	R1, [R0, #0]
;BPWasher_main.c,368 :: 		nSec_count = 0;
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(_nSec_count+0)
MOVT	R0, #hi_addr(_nSec_count+0)
STRH	R1, [R0, #0]
;BPWasher_main.c,369 :: 		}
L_Timer2_interrupt19:
;BPWasher_main.c,370 :: 		}
L_end_Timer2_interrupt:
BX	LR
; end of _Timer2_interrupt
_main:
;BPWasher_main.c,374 :: 		void main() {
SUB	SP, SP, #112
;BPWasher_main.c,375 :: 		char bCPU_Mon = FALSE;
MOVS	R0, #0
STRB	R0, [SP, #108]
;BPWasher_main.c,380 :: 		PWREN_bit = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(PWREN_bit+0)
MOVT	R0, #hi_addr(PWREN_bit+0)
STR	R1, [R0, #0]
;BPWasher_main.c,382 :: 		Start_TP();
BL	_Start_TP+0
;BPWasher_main.c,383 :: 		init_Ports();
BL	_init_Ports+0
;BPWasher_main.c,384 :: 		init_variables();
BL	_init_variables+0
;BPWasher_main.c,385 :: 		InitTimer2();
BL	_InitTimer2+0
;BPWasher_main.c,386 :: 		GPIO_Digital_Output(&GPIOD_BASE, _GPIO_PINMASK_10 | _GPIO_PINMASK_11 | _GPIO_PINMASK_1);
MOVW	R1, #3074
MOVW	R0, #lo_addr(GPIOD_BASE+0)
MOVT	R0, #hi_addr(GPIOD_BASE+0)
BL	_GPIO_Digital_Output+0
;BPWasher_main.c,387 :: 		HEATER_OFF;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOD_ODR+0)
MOVT	R0, #hi_addr(GPIOD_ODR+0)
STR	R1, [R0, #0]
;BPWasher_main.c,388 :: 		ADC_Set_Input_Channel(_ADC_CHANNEL_1);
MOVW	R0, #2
BL	_ADC_Set_Input_Channel+0
;BPWasher_main.c,391 :: 		IWDG_KR = 0x5555;    // enable write access
MOVW	R1, #21845
MOVW	R0, #lo_addr(IWDG_KR+0)
MOVT	R0, #hi_addr(IWDG_KR+0)
STR	R1, [R0, #0]
;BPWasher_main.c,392 :: 		IWDG_PR = 0x0004;    // divider /64
MOVS	R1, #4
MOVW	R0, #lo_addr(IWDG_PR+0)
MOVT	R0, #hi_addr(IWDG_PR+0)
STR	R1, [R0, #0]
;BPWasher_main.c,393 :: 		IWDG_KR = 0x5555;    // enable write access
MOVW	R1, #21845
MOVW	R0, #lo_addr(IWDG_KR+0)
MOVT	R0, #hi_addr(IWDG_KR+0)
STR	R1, [R0, #0]
;BPWasher_main.c,394 :: 		IWDG_RLR = 0xFFFF;   // 8192 ms
MOVW	R1, #65535
MOVW	R0, #lo_addr(IWDG_RLR+0)
MOVT	R0, #hi_addr(IWDG_RLR+0)
STR	R1, [R0, #0]
;BPWasher_main.c,395 :: 		IWDG_KR = 0xCCCC;    // turn on watchdog - CM 30/10/2017
MOVW	R1, #52428
MOVW	R0, #lo_addr(IWDG_KR+0)
MOVT	R0, #hi_addr(IWDG_KR+0)
STR	R1, [R0, #0]
;BPWasher_main.c,397 :: 		I2C1_Init();
BL	_I2C1_Init+0
;BPWasher_main.c,398 :: 		ConfigureInputs();
BL	_ConfigureInputs+0
;BPWasher_main.c,399 :: 		nTest[0] =0x00;
ADD	R1, SP, #104
MOVS	R0, #0
STRB	R0, [R1, #0]
;BPWasher_main.c,400 :: 		nTest[1] =0x00;
ADDS	R1, R1, #1
MOVS	R0, #0
STRB	R0, [R1, #0]
;BPWasher_main.c,406 :: 		}
L_main20:
;BPWasher_main.c,408 :: 		ResetRecipe();
BL	_ResetRecipe+0
;BPWasher_main.c,409 :: 		Delay_ms(2000); // delay for splashscreen was 3000, now delay includes loading memory, etc - CM
MOVW	R7, #10067
MOVT	R7, #1424
NOP
NOP
L_main21:
SUBS	R7, R7, #1
BNE	L_main21
NOP
NOP
NOP
NOP
;BPWasher_main.c,410 :: 		DrawScreen(&_main);
MOVW	R0, #lo_addr(__main+0)
MOVT	R0, #hi_addr(__main+0)
BL	_DrawScreen+0
;BPWasher_main.c,414 :: 		if(WDGRSTF_bit){
MOVW	R1, #lo_addr(WDGRSTF_bit+0)
MOVT	R1, #hi_addr(WDGRSTF_bit+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	EQ
BEQ	L_main23
;BPWasher_main.c,418 :: 		stored_state = RTC_BKP0R; // register which stores the state before reboot
MOVW	R0, #lo_addr(RTC_BKP0R+0)
MOVT	R0, #hi_addr(RTC_BKP0R+0)
LDR	R0, [R0, #0]
STRH	R0, [SP, #106]
;BPWasher_main.c,419 :: 		sprintf(msgWatchdogReset, "Watchdog Reset Occured, %d, %.0f", stored_state, getCurrentTemp());
BL	_GetCurrentTemp+0
LDRH	R2, [SP, #106]
MOVW	R1, #lo_addr(?lstr_8_BPWasher_main+0)
MOVT	R1, #hi_addr(?lstr_8_BPWasher_main+0)
ADD	R0, SP, #4
VPUSH	#0, (S0)
PUSH	(R2)
PUSH	(R1)
PUSH	(R0)
BL	_sprintf+0
ADD	SP, SP, #16
;BPWasher_main.c,422 :: 		if(stored_state != 0){
LDRH	R0, [SP, #106]
CMP	R0, #0
IT	EQ
BEQ	L_main24
;BPWasher_main.c,423 :: 		nState = ERR_PRE_HOLDING_STATE;
MOVS	R1, #100
MOVW	R0, #lo_addr(_nState+0)
MOVT	R0, #hi_addr(_nState+0)
STRB	R1, [R0, #0]
;BPWasher_main.c,424 :: 		}
L_main24:
;BPWasher_main.c,426 :: 		ErrorLog_NewError(&msgWatchdogReset, STATUS_ERROR);
ADD	R0, SP, #4
MOVS	R1, #1
BL	_ErrorLog_NewError+0
;BPWasher_main.c,428 :: 		RMVF_bit = 1; // clear reset flags
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(RMVF_bit+0)
MOVT	R0, #hi_addr(RMVF_bit+0)
STR	R1, [R0, #0]
;BPWasher_main.c,429 :: 		}
L_main23:
;BPWasher_main.c,431 :: 		while (1) {
L_main25:
;BPWasher_main.c,433 :: 		if(KickDog){
MOVW	R0, #lo_addr(_KickDog+0)
MOVT	R0, #hi_addr(_KickDog+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_main27
;BPWasher_main.c,434 :: 		IWDG_KR = 0xAAAA;   //Kick the dog
MOVW	R1, #43690
MOVW	R0, #lo_addr(IWDG_KR+0)
MOVT	R0, #hi_addr(IWDG_KR+0)
STR	R1, [R0, #0]
;BPWasher_main.c,435 :: 		}
L_main27:
;BPWasher_main.c,437 :: 		Check_TP();
BL	_Check_TP+0
;BPWasher_main.c,440 :: 		GetInputStates();
L_main28:
;BPWasher_main.c,442 :: 		Recipe_State();
BL	_Recipe_State+0
;BPWasher_main.c,444 :: 		if (bADC)
MOVW	R0, #lo_addr(_bADC+0)
MOVT	R0, #hi_addr(_bADC+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_main29
;BPWasher_main.c,446 :: 		if (!bGlobalTempAlarm)
MOVW	R0, #lo_addr(_bGlobalTempAlarm+0)
MOVT	R0, #hi_addr(_bGlobalTempAlarm+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	NE
BNE	L_main30
;BPWasher_main.c,448 :: 		if (bCPU_Mon)
LDRB	R0, [SP, #108]
CMP	R0, #0
IT	EQ
BEQ	L_main31
;BPWasher_main.c,449 :: 		CPU_MON_OFF
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOD_ODR+0)
MOVT	R0, #hi_addr(GPIOD_ODR+0)
STR	R1, [R0, #0]
IT	AL
BAL	L_main32
L_main31:
;BPWasher_main.c,451 :: 		CPU_MON_ON
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOD_ODR+0)
MOVT	R0, #hi_addr(GPIOD_ODR+0)
STR	R1, [R0, #0]
L_main32:
;BPWasher_main.c,453 :: 		bCPU_Mon = !bCPU_Mon;
LDRB	R0, [SP, #108]
CMP	R0, #0
MOVW	R0, #0
BNE	L__main49
MOVS	R0, #1
L__main49:
STRB	R0, [SP, #108]
;BPWasher_main.c,454 :: 		}
L_main30:
;BPWasher_main.c,456 :: 		ServiceHeater();
BL	_ServiceHeater+0
;BPWasher_main.c,457 :: 		bADC = FALSE;
MOVS	R1, #0
MOVW	R0, #lo_addr(_bADC+0)
MOVT	R0, #hi_addr(_bADC+0)
STRB	R1, [R0, #0]
;BPWasher_main.c,458 :: 		}
L_main29:
;BPWasher_main.c,460 :: 		if (bRefresh)
MOVW	R0, #lo_addr(_bRefresh+0)
MOVT	R0, #hi_addr(_bRefresh+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_main33
;BPWasher_main.c,462 :: 		refresh_PV_display();
BL	_refresh_PV_display+0
;BPWasher_main.c,463 :: 		bRefresh = FALSE;
MOVS	R1, #0
MOVW	R0, #lo_addr(_bRefresh+0)
MOVT	R0, #hi_addr(_bRefresh+0)
STRB	R1, [R0, #0]
;BPWasher_main.c,464 :: 		}
L_main33:
;BPWasher_main.c,466 :: 		if (bFlash)
MOVW	R0, #lo_addr(_bFlash+0)
MOVT	R0, #hi_addr(_bFlash+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_main34
;BPWasher_main.c,468 :: 		StatusHalfSecondsCount();
BL	_StatusHalfSecondsCount+0
;BPWasher_main.c,469 :: 		flash_status();
BL	_flash_status+0
;BPWasher_main.c,470 :: 		bFLASH = FALSE;
MOVS	R1, #0
MOVW	R0, #lo_addr(_bFlash+0)
MOVT	R0, #hi_addr(_bFlash+0)
STRB	R1, [R0, #0]
;BPWasher_main.c,471 :: 		}
L_main34:
;BPWasher_main.c,472 :: 		}
IT	AL
BAL	L_main25
;BPWasher_main.c,473 :: 		}
L_end_main:
L__main_end_loop:
B	L__main_end_loop
; end of _main
