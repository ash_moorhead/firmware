/*
   _____             _____                 
  / ____|           / ____|                
 | (___   ___ _ __ | (___  _   _ _ __ ___  
  \___ \ / _ \ '_ \ \___ \| | | | '_ ` _ \ 
  ____) |  __/ | | |____) | |_| | | | | | |
 |_____/ \___|_| |_|_____/ \__,_|_| |_| |_|
                                           
                                           
	Description:	Interface header for the D7SA0001 siesmic sensor


*/



#ifndef D7SHEADER
#define D7SHEADER
#include <stdint.h>
#include "lora_sensum.h"

/********************************************************************
 *Public Definitions                                                *
 ********************************************************************/
	typedef enum
	{
		Normal = 0x00,
		Normal_not_in_standby   = 0x01, 
		Initial_installation = 0x02,
		Offset_acquisition = 0x03, 
		Self_diagnostic = 0x04,
		Unknown = 0x05,
	}D7S_mode;
	
	typedef enum
	{
		YZ = 0x00,
		XZ   = 0x01, 
		XY = 0x02,
		Auto_switch = 0x03, 
		Switch_at_installation = 0x04
	}Ctrl_Axis;

 /********************************************************************
 *Public Function Prototypes                                         *
 ********************************************************************/
void D7S_init ( void );
void INT1_IRQ( void );
void INT2_IRQ( void );
void wake_timeout( void );
void initialise_timeout( void );
void D7S_uplink( void );
void Uplink_new ( void );
void D7S_clear( void );
void Init_interrupts( void );
int8_t D7S_read_byte( uint8_t, uint8_t );
int16_t D7S_get_PGA  (uint8_t, uint8_t );
int16_t D7S_get_SI  (uint8_t, uint8_t );
void run_initialisation( void );
void D7S_write_byte( uint8_t, uint8_t, uint8_t	);
int8_t get_current_state( void );
void D7S_cli(int argc, char *argv[]);
void D7S_cli_status( void );
void D7S_cli_help( void );
uint8_t self_test_passed( void );
void continuously_Get_D7S_PGA( void ); 
void d7s_save_config( void );
void d7s_load_config( void );
 /********************************************************************
 *Global Variables                                                  *
 ********************************************************************/
extern volatile uint8_t  wake_via_d7s;



#endif //D7SHEADER


