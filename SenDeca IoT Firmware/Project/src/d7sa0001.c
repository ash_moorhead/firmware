/*
   _____             _____                 
  / ____|           / ____|                
 | (___   ___ _ __ | (___  _   _ _ __ ___  
  \___ \ / _ \ '_ \ \___ \| | | | '_ ` _ \ 
  ____) |  __/ | | |____) | |_| | | | | | |
 |_____/ \___|_| |_|_____/ \__,_|_| |_| |_|
                                           
                                           
	Description:	I2C register level driver for LoRa only implementation project
								Implements I2C flash external periphheral.


*/


#include "i2c1.h"
#include "d7sa0001.h"

#include "stm32l0xx.h"                  // Device header

#include "hw.h"
#include "flash_map.h"

#include "debug_uart.h"

#include "lora_sensum.h"
#include "radio_common.h"

#include "watchdog.h" 									// so the watchdog can be manually reset for continuous reading of D7S
#include "delays.h"

#ifdef DISABLE_D7S_DEBUG
	#define dbg_print(...)
#else
	#define dbg_print(...) Debug_printf(__VA_ARGS__)
#endif

#define D7S_ADDR 0x55

#define STATE_H  				0x10
#define STATE_L  				0x00
#define AXIS_STATE_H  	0x10
#define AXIS_STATE_L  	0x01
#define EVENT_H  				0x10
#define EVENT_L  				0x02
#define MODE_H  				0x10
#define MODE_L  				0x03
#define CTRL_H  				0x10
#define CTRL_L  				0x04
#define CLEAR_H					0x10
#define CLEAR_L					0x05

// instantaneous values
#define MAIN_SI_H				0x20
#define MAIN_SI_L				0x00
#define MAIN_PGA_H			0x20
#define MAIN_PGA_L			0x02

// SI ranked quake results
#define M1_MAIN_SI_H			0x35
#define M1_MAIN_SI_L			0x08
#define M1_MAIN_PGA_H			0x35
#define M1_MAIN_PGA_L			0x0A

// latest readings (updated after quake)
#define N1_MAIN_SI_H			0x30
#define N1_MAIN_SI_L			0x08
#define N1_MAIN_PGA_H			0x30
#define N1_MAIN_PGA_L			0x0A
#define N2_MAIN_PGA_H			0x31
#define N2_MAIN_PGA_L			0x0A
#define N2_MAIN_SI_H			0x31
#define N2_MAIN_SI_L			0x08
#define N3_MAIN_PGA_H			0x32
#define N3_MAIN_PGA_L			0x0A
#define N3_MAIN_SI_H			0x32
#define N3_MAIN_SI_L			0x08

// wake flag for interrupts and timer
volatile uint8_t 		wake_via_d7s = 0;
volatile uint8_t 		wake_via_int1 = 0;
volatile uint8_t 		timeout = 0;
// initialise interrupt 2 flag
static bool    			INT2_enabled = false;
static bool    			Threshold_low = false;
static bool 				INITIALISED = false;
static uint8_t 			offsets_called = 0;
static TimerEvent_t processing_timer;
static TimerEvent_t initialise_timer;
static uint32_t 		check_interval_ms = 10* 1000; // set at 10 seconds



void D7S_init()
{

		if(!i2c1_init())
			{
				dbg_print("Could not initialise I2C!");
			}
		else
			{	
				// Run initialisation only once (called 3 times when power on)
					if (!INITIALISED)
					{
						// set ctrl_axis register to 4 (switch axis at installation) and default threshold to High
						D7S_write_byte(CTRL_H, CTRL_L, Switch_at_installation << 4 );
						
						// If current state is not normal cannot change modes
						if (get_current_state() != Normal){
							// happens if it is trying to calculate an earthquake when initialised. need to wait.
							Debug_printf("Waiting to return to Normal mode to run initial installation\r\n");
							
							TimerInit(&initialise_timer, initialise_timeout);
							TimerSetValue(&initialise_timer, 2*60*1000); // if not read in 2 minutes sensor/I2C issue
							TimerStart(&initialise_timer);
							
							while((get_current_state() != Normal) && !timeout)
							{
									reset_watchdog();
									delay_timeout_ms(1000);
									Debug_printf(".");
							}
							// switch to unconfigured mode to prevent another timeout.
							if (timeout)
							{
								device_mode = DEVICE_UNCONFIGURED;
								update_device_behaviour();
								save_global_config_page();
								return;
							}
							Debug_printf("\r\n");
							TimerStop(&initialise_timer);
						}
					
						run_initialisation();
						
						D7S_clear();
						INITIALISED = true;
					}
					
					/* needs to be called flash is loaded else initialised value(false) is always used  
						( third init call through update_device_behaviour) */
					Init_interrupts();
	
			}
		
}
/*OLD: for reference only
void D7S_uplink ( void )
{
	D7S_payload_t payload = {0};
	
	// If still processing, skip send and send when INT2 goes low
	if (get_current_state() != Normal)
	{
		dbg_print("processing\r\n");
		
		//reset the check timer if INT2 disabled and scheduled wake or INT1 (should be impossible) arrives here
		if(!INT2_enabled)
		{
			TimerReset(&processing_timer);
		}
		return;
	}
	
	if(!INT2_enabled)
	{
		TimerStop(&processing_timer); 
	}

	// read Biggest SI value and PGA
	payload.members.SI = D7S_get_SI(M1_MAIN_SI_H, M1_MAIN_SI_L);
	payload.members.PGA = D7S_get_PGA(M1_MAIN_PGA_H, M1_MAIN_PGA_L);
	
	payload.members.status = self_test_passed();
	
	payload.members.sys_voltage = fourBit_battery_calculation();
	payload.members.pkt_type    = packet_type_data;
	dbg_print("SI: %d, PGA: %d\r\n", payload.members.SI, payload.members.PGA);
	Uplink(payload.payload, D7S_SIZE);
	
	D7S_clear();
	
}*/


void Uplink_new ( void )
{
	D7S_payload_t payload = {0};
	
	// if still processing
	if (get_current_state() != Normal)
	{	
		//send instantly unless woken by timer
		if (!processing_timer.IsRunning)
		{
			// read instant SI value and PGA
			payload.members.SI = D7S_get_SI(MAIN_SI_H, MAIN_SI_L);
			//register is raw 
			payload.members.PGA = D7S_get_PGA(MAIN_PGA_H, MAIN_PGA_L);
			
			// skip this for speed
			payload.members.status = 1;
			payload.members.threshold = Threshold_low;
			payload.members.critical = wake_via_int1;
			
			payload.members.sys_voltage = fourBit_battery_calculation();
			payload.members.pkt_type    = packet_type_data;
			
			Uplink(payload.payload, D7S_SIZE);
			wake_via_int1 = 0;
		}
		
		// if int2 disabled start timer	
		if(!INT2_enabled)
		{
			TimerReset(&processing_timer);
			return;
		}
	}
	else
	{
		// if int2 disabled stop timer	
		if(!INT2_enabled)
		{
			TimerStop(&processing_timer);
			processing_timer.IsRunning = false;
		}
	
		// read Biggest SI value and PGA
		payload.members.SI = D7S_get_SI(M1_MAIN_SI_H, M1_MAIN_SI_L);
		// devide by 10 to get to the same as the raw register
		payload.members.PGA = D7S_get_PGA(M1_MAIN_PGA_H, M1_MAIN_PGA_L) /10;	
	
		// only send on scheduled wakeup
		payload.members.status = self_test_passed();
		payload.members.critical = wake_via_int1;
		payload.members.threshold = Threshold_low;
		
		payload.members.sys_voltage = fourBit_battery_calculation();
		payload.members.pkt_type    = packet_type_data;
		dbg_print("SI: %d, PGA: %d\r\n", payload.members.SI, payload.members.PGA);
		
		Uplink(payload.payload, D7S_SIZE);
		
		D7S_clear();
	}
	
}

void run_initialisation()
{	
		// change to initial installation mode of sensor
		Debug_printf("Initialising sensor");
		D7S_write_byte(MODE_H, MODE_L, Initial_installation);
	
		// wait for initial installation mode to finish
		while((get_current_state() != Normal))
		{
			reset_watchdog();
			delay_timeout_ms(100);
			Debug_printf(".");
		}
		Debug_printf("\r\n");
}
void Init_interrupts()
{
	GPIO_InitTypeDef GPIO_InitStruct;
	
	GPIO_InitStruct.Mode      = GPIO_MODE_IT_FALLING; 
	// both INT lines are resting high without this. but neither IRQ responds if this is NOPULL. 
	GPIO_InitStruct.Pull  		= GPIO_PULLUP;  
	GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_LOW;

	HW_GPIO_Init(D7S_INT1_PORT, D7S_INT1_PIN, &GPIO_InitStruct);	
	HW_GPIO_SetIrq(D7S_INT1_PORT, D7S_INT1_PIN  ,3,INT1_IRQ);

	if (INT2_enabled)
	{
		dbg_print("INT2 Enabled\r\n");

		GPIO_InitStruct.Mode      = GPIO_MODE_IT_RISING;
		
		HW_GPIO_Init(D7S_INT2_PORT, D7S_INT2_PIN, &GPIO_InitStruct);	
		HW_GPIO_SetIrq(D7S_INT2_PORT, D7S_INT2_PIN  ,3,INT2_IRQ);
	}
	else
	{
		// init timer for waiting for processing to finish before uplink
		// only needed if INT2 doesn't handle it already
		TimerInit(&processing_timer, wake_timeout);
		TimerSetValue(&processing_timer, check_interval_ms);
	}
}
void INT1_IRQ()
{
	dbg_print("INT1\r\n");
	wake_via_d7s = 1;
	wake_via_int1 = 1;
}
void wake_timeout()
{
	wake_via_d7s = 1;
}
void initialise_timeout()
{
	timeout = true;
	TimerStop(&initialise_timer);
}
void INT2_IRQ()
{
		dbg_print("INT2\r\n");
	// ignore if from offset acquisition(unused) or self-diagnostic
	if (offsets_called)
	{
		offsets_called = 0;
		dbg_print(" cancelled");

		return;
	}
	if (INT2_enabled)
	{
		wake_via_d7s = 1;
	}

}
int8_t get_current_state()
{
	/* Return Unknown (-1) when NACK recieved. means registers are being updated 
		and that we need to keep waiting*/
	int8_t state = D7S_read_byte(STATE_H, STATE_L);
	if (state == -1)
	{
		return Unknown;
	}
	return ((D7S_mode)(state & 0x07));
}

void D7S_clear( void )
{
	//clears earthquake registers
	D7S_write_byte(CLEAR_H, CLEAR_L, 0x01);
	
	dbg_print("clearing Quake registers\r\n");
}
int16_t D7S_get_PGA(uint8_t ADDR_H, uint8_t ADDR_L)
{
		uint8_t data_to_send[2];
    uint8_t data_send_length = 2;
    uint8_t data_to_read[2] = {0};
    uint8_t data_read_length = 2;
		
   
    data_to_send[0] = ADDR_H;
    data_to_send[1] = ADDR_L;
  
				
		i2c1_send(D7S_ADDR, data_to_send, data_send_length, 0);
		i2c1_receive(D7S_ADDR, data_to_read, data_read_length,1);
		reset_watchdog();
       
		return data_to_read[0] << 8 | data_to_read[1];
		
	}

int16_t D7S_get_SI(uint8_t ADDR_H, uint8_t ADDR_L)
{
		uint8_t data_to_send[2];
    uint8_t data_send_length = 2;
    uint8_t data_to_read[2] = {0};
    uint8_t data_read_length = 2;
		
   
    data_to_send[0] = ADDR_H;
    data_to_send[1] = ADDR_L;
				
		i2c1_send(D7S_ADDR, data_to_send, data_send_length, 0);
		i2c1_receive(D7S_ADDR, data_to_read, data_read_length,1);
		reset_watchdog();
        
		return (data_to_read[0] << 8 | data_to_read[1]);		
		
}
int8_t D7S_read_byte(uint8_t reg_h, uint8_t reg_l)	
	{
	
		uint8_t data_to_send[10];
		uint8_t data_send_length = 2;
		uint8_t data_to_read[10] = {0};
		uint8_t data_read_length = 1;
		int success;
		
		data_to_send[0] = reg_h;
		data_to_send[1] = reg_l;
		
		i2c1_send(D7S_ADDR, data_to_send, data_send_length, 0);
		success = i2c1_receive_feedback(D7S_ADDR, data_to_read, data_read_length,1);
		/* when registers are updated at the end of processing of initial installation 
				mode etc. NACK's are sent. */ 
		if (!success)
		{
			return -1;
		}
		
		return data_to_read[0];
	}	

void D7S_write_byte(uint8_t reg_h, uint8_t reg_l, uint8_t val)	
	{
		// For setting register value like CLEAR reg, requires this type of write.
		
		uint8_t data_to_send[10];
		uint8_t data_send_length = 3;
		
		data_to_send[0] = reg_h;
		data_to_send[1] = reg_l;
		data_to_send[2] = val;
		
		i2c1_send(D7S_ADDR, data_to_send, data_send_length, 1);
		
	}
uint8_t self_test_passed( void )
{
	uint8_t pass;
	
	// cancels INT2 from running diagnostic
	/* TODO: possibility of skipping an earthquake if starts
		while INT2 is still low from diagnostic*/
	offsets_called = 1;	
	D7S_write_byte(MODE_H, MODE_L, Self_diagnostic);
	Debug_printf("Running diagnostic");
	
	// wait for diagnostic to complete
	while(get_current_state() != Normal)
		{
			reset_watchdog();
			delay_timeout_ms(500);
			Debug_printf(".");
		}
	Debug_printf("\r\n");
	
	pass = !((D7S_read_byte(EVENT_H, EVENT_L) & 0x04) >> 2); 

	return pass;
}
void d7s_save_config()
{
	d7s_config_page_layout_t config = {0};
	
	config.members.INT2_enabled    = INT2_enabled           ;
	config.members.threshold_low   = Threshold_low           ;
	
	save_extra_config_page(config.raw_bytes, device_specific_page_1);
}

void d7s_load_config()
{
	d7s_config_page_layout_t config = {0};
	load_extra_config_page(config.raw_bytes, device_specific_page_1);
	
	INT2_enabled                   = config.members.INT2_enabled        ;      
	Threshold_low                   = config.members.threshold_low       ;      	

}
void D7S_cli(int argc, char *argv[])
{
	
	if(argc < 1)
	{
		D7S_cli_help();
		return;
	}
	
	if(!strcmp(argv[0], "pga"))
	{
		Debug_printf("Last 3 stored PGA's: %d, %d, %d \r\n", D7S_get_PGA(N1_MAIN_PGA_H, M1_MAIN_PGA_L), D7S_get_PGA(N2_MAIN_PGA_H, N2_MAIN_PGA_L), D7S_get_PGA(N3_MAIN_PGA_H, N3_MAIN_PGA_L)); 
		return;
	}
	if(!strcmp(argv[0], "si"))
	{
		Debug_printf("Last 3 stored SI's: %d, %d, %d \r\n", D7S_get_SI(N1_MAIN_SI_H, M1_MAIN_SI_L), D7S_get_SI(N2_MAIN_SI_H, N2_MAIN_SI_L), D7S_get_SI(N3_MAIN_SI_H, N3_MAIN_SI_L)); 
		return;
	}
	if(!strcmp(argv[0], "test"))
	{
		if (self_test_passed())
		{
			Debug_printf("diagnostic passed!\r\n");
		}
		else
		{
			Debug_printf("self diagnostic failed!\r\n");
		}
		return;
	}
	
	if(!strcmp(argv[0], "continuous"))
	{
		Debug_printf("Continuous instantaneous PGA, reset to exit.");
		continuously_Get_D7S_PGA();
		return;
	}
	if(!strcmp(argv[0], "ready"))
	{
		if (get_current_state() == Normal)
		{
			Debug_printf("sensor is ready\r\n");
		} 
		else 
		{
			Debug_printf("sensor is not ready\r\n");
		}
		return;
	}
	if(!strcmp(argv[0], "clear"))
	{
		D7S_clear();
		return;
	}
	if(!strcmp(argv[0], "events"))
	{
		if(argc < 2)
		{
			D7S_cli_help();
		}
		else if(!strcmp(argv[1], "all"))
		{
			INT2_enabled = true;
			d7s_save_config();
			Init_interrupts();
			Debug_printf("events: all\r\n");
		}
		else if (!strcmp(argv[1], "critical"))
		{
			INT2_enabled = false;
			d7s_save_config();
			Init_interrupts();
			Debug_printf("events: critical\r\n");
		}
		else {

			D7S_cli_help();
		}

		return;
	}
	if(!strcmp(argv[0], "threshold"))
	{
		if(argc < 2)
		{
			D7S_cli_help();
		}
		else if(!strcmp(argv[1], "high"))
		{
			D7S_write_byte(CTRL_H, CTRL_L,  0x04 << 3);
			Threshold_low = 0;
			d7s_save_config();
			Debug_printf("threshold: high\r\n");
		}
		else if (!strcmp(argv[1], "low"))
		{
			D7S_write_byte(CTRL_H, CTRL_L,  0x09 << 3);
			Threshold_low = 1;
			d7s_save_config();
			Debug_printf("threshold: low\r\n");
		}
		else
		{
			D7S_cli_help();
		}
		
		return;
	}	
	
	if(!strcmp(argv[0], "show"))
	{
		D7S_cli_status();
		return;
	}
	if(argc < 2)
	{
		D7S_cli_help();
		return;
	}
	
	return;
	
}

void D7S_cli_help( void )
{
//	Debug_printf("continuous : Continuous instantaneous PGA\r\n");
	Debug_printf("Usage: device events [all | critical] \r\n");
	Debug_printf("\tsets reporting of events\r\n");
	Debug_printf("Usage: device threshold [low | high] \r\n");
	Debug_printf("\tsets the device's threshold");
	
	D7S_cli_status();
}
void D7S_cli_status( void )
{
	Debug_printf("\r\nCurrent:\r\n");
	if (Threshold_low)
	{
		Debug_printf("\tthreshold: low\r\n");
	}
	else
	{
		Debug_printf("\tthreshold: high\r\n");
	}
	
	if (INT2_enabled)
	{
		Debug_printf("\tevents: all\r\n");
	}
	else
	{
		Debug_printf("\tevents: critical\r\n");
	}
}
//	Debug_printf("pga : 3 latest stored PGA values\r\n");
//	Debug_printf("test: Run self diagnostic routine\r\n");
//	Debug_printf("ready: is sensor ready or still calculating quake values\r\n");
//	Debug_printf("si : 3 latest stored SI values\r\n");
//	Debug_printf("clear : clears earthquake registers\r\n");
//	Debug_printf("threshold : clears earthquake registers\r\n");

// only used from CLI continuous call.
void continuously_Get_D7S_PGA()
{
    uint8_t data_to_send[2];
    uint8_t data_send_length = 2;
    uint8_t data_to_read[2] = {0};
    uint8_t data_read_length = 2;
    int16_t temp;
		
   
    data_to_send[0] = 0x20;
    data_to_send[1] = 0x02;
   
    //set the i2c peripheral to the default state
    if(!i2c1_init())
    {
        //if we could not initialise the I2C, then we cannot continue.
    }
    else
    {
        while ( true)
			{
				
        i2c1_send(D7S_ADDR, data_to_send, data_send_length, 0);
        i2c1_receive(D7S_ADDR, data_to_read, data_read_length,1);
        reset_watchdog();
        temp = data_to_read[0] << 8 | data_to_read[1];
        //at this point we know we have valid data
        Debug_printf("REG:%d \r\n",temp);
        delay_timeout_ms(300); // updates every 320ms
			}
    }
		
	}
